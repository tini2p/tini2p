/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_OUTBOUND_GATEWAY_H_
#define SRC_TUNNELS_OUTBOUND_GATEWAY_H_

#include "src/tunnels/hop.h"
#include "src/tunnels/processor.h"

namespace tini2p
{
namespace tunnels
{
template <class TLayer>
class OutboundGateway : public tunnels::Processor<TLayer, tunnels::OutboundGatewayProcessor>
{
 public:
  using layer_t = TLayer;  //< Tunnel layer encryption trait alias
  using base_t = tunnels::Processor<layer_t, tunnels::OutboundGatewayProcessor>;
  using aes_layer_t = crypto::TunnelAES;  //< AES tunnel layer encryption
  using chacha_layer_t = crypto::TunnelChaCha;  //< ChaCha tunnel layer encryption
  using info_t = data::Info;  //< RouterInfo trait alias
  using lease_t = data::Lease2;  //< Lease2 trait alias
  using tunnel_id_t = typename base_t::tunnel_id_t;  //< Tunnel ID trait alias
  using tunnel_message_t = typename base_t::tunnel_message_t;  //< Tunnel message trait alias
  using key_info_t = typename base_t::key_info_t;  //< Build key information trait alias

  /// @brief Initializing ctor, creates OutboundGateway from a container of hop RouterInfos
  /// @detail Hop RouterInfo's should be selected from the outbound tunnel pool of available routers
  /// @param info Shared pointer to the local RouterInfo
  /// @param reply_lease Lease for the reply tunnel gateway
  /// @param infos Collection of shared pointers to RouterInfos for the hops in the outbound tunnel
  OutboundGateway(info_t::shared_ptr info, lease_t reply_lease, const std::vector<info_t::shared_ptr>& infos)
      : base_t(info, std::forward<lease_t>(reply_lease), infos, base_t::BloomFlag::Enable)
  {
  }

  decltype(auto) EncryptRecord(typename base_t::record_buffer_t&) = delete;
  decltype(auto) DecryptRecord(typename base_t::record_buffer_t&) = delete;
  decltype(auto) EncryptRecord(typename base_t::record_buffer_t&, const std::uint8_t&, const std::uint8_t&) = delete;
  decltype(auto) DecryptRecord(typename base_t::record_buffer_t&, const std::uint8_t&, const std::uint8_t&) = delete;
  decltype(auto) Encrypt(tunnel_message_t&) = delete;
  decltype(auto) Decrypt(tunnel_message_t&) = delete;
  decltype(auto) DecryptAEAD(tunnel_message_t&) = delete;
  decltype(auto) ExtractI2NPFragments(tunnel_message_t&) = delete;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_OUTBOUND_GATEWAY_H_
