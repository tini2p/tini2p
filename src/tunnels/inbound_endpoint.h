/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_INBOUND_ENDPOINT_H_
#define SRC_TUNNELS_INBOUND_ENDPOINT_H_

#include "src/tunnels/processor.h"

namespace tini2p
{
namespace tunnels
{
/// @class InboundEndpoint
/// @brief For processing inbound tunnel messages for an InboundEndpoint
template <class TLayer>
class InboundEndpoint : public tunnels::Processor<TLayer, tunnels::InboundEndpointProcessor>
{
 public:
  using layer_t = TLayer;
  using base_t = tunnels::Processor<layer_t, tunnels::InboundEndpointProcessor>;
  using aes_layer_t = crypto::TunnelAES;  //< AES layer encryption trait alias
  using chacha_layer_t = crypto::TunnelChaCha;  //< ChaCha layer encryption trait alias
  using info_t = data::Info;  //< Router info trait alias
  using lease_t = data::Lease2;  //< Lease2 trait alias
  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using tunnel_id_t = typename base_t::tunnel_id_t;  //< Tunnel ID trait alias
  using to_hash_t = typename base_t::to_hash_t;  //< To hash trait alias
  using build_request_t = typename base_t::build_request_t;  //< BuildRequestRecord trait alias
  using build_reply_t = typename base_t::build_reply_t;  //< BuildRequestRecord trait alias
  using record_buffer_t = typename base_t::record_buffer_t;  //< Build record buffer trait alias
  using tunnel_message_t = typename base_t::tunnel_message_t;  //< Tunnel message trait alias

  /// @brief Initializing ctor, creates an tunnel InboundEndpoint
  /// @detail Caller responsible for setting layer encryption keys before en/decrypting tunnel messages
  /// @param info Shared pointer to the RouterInfo for this endpoint
  /// @param reply_lease Lease for the InboundGateway
  InboundEndpoint(info_t::shared_ptr info, lease_t reply_lease, const std::vector<info_t::shared_ptr>& infos)
      : base_t(info, std::forward<lease_t>(reply_lease), infos, base_t::BloomFlag::Enable)
  {
  }

  // Delete unused functions by an InboundEndpoint
  decltype(auto)
  CreateTunnelMessages(const typename base_t::i2np_block_t::buffer_t&, to_hash_t, std::optional<tunnel_id_t>) = delete;

  decltype(auto) EncryptRecord(typename base_t::record_buffer_t&) = delete;
  decltype(auto) DecryptRecord(typename base_t::record_buffer_t&) = delete;
  decltype(auto) EncryptRecord(typename base_t::record_buffer_t&, const std::uint8_t&, const std::uint8_t&) = delete;
  decltype(auto) DecryptRecord(typename base_t::record_buffer_t&, const std::uint8_t&, const std::uint8_t&) = delete;
  decltype(auto) Encrypt(tunnel_message_t&) = delete;
  decltype(auto) Decrypt(tunnel_message_t&) = delete;
  decltype(auto) EncryptAEAD(tunnel_message_t&) = delete;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_INBOUND_ENDPOINT_H_
