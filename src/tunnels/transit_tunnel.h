/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_TRANSIT_TUNNEL_H_
#define SRC_TUNNELS_TRANSIT_TUNNEL_H_

#include "src/tunnels/hop.h"
#include "src/tunnels/inbound_gateway.h"
#include "src/tunnels/outbound_endpoint.h"

namespace tini2p
{
namespace tunnels
{
template <class TLayer>
class TransitTunnel
{
 public:
  enum
  {
    Expiration = 630,  //< in seconds, regular tunnel expiration (10 min.) plus network skew 
  };

  using layer_t = TLayer;  //< Layer encryption trait alias
  using aes_layer_t = crypto::TunnelAES;  //< AES layer encryption trait alias
  using chacha_layer_t = crypto::TunnelChaCha;  //< ChaCha layer encryption trait alias
  using sym_t = typename layer_t::sym_t;  //< Symmetric encryption trait alias
  using aes_t = crypto::AES;  //< AES encryption trait alias
  using chacha_t = crypto::ChaCha20;  //< ChaCha20 encryption trait alias
  using info_t = data::Info;  //< RouterInfo trait alias
  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using build_request_t = data::BuildRequestRecord;  //< BuildRequestRecord trait alias
  using ident_hash_t = build_request_t::ident_hash_t;  //< Creator Identity hash trait alias
  using build_reply_t = data::BuildResponseRecord<sym_t>;  //< BuildResponseRecord trait alias
  using build_message_t = data::TunnelBuildMessage;  //< TunnelBuildMessage trait alias
  using tunnel_message_t = data::TunnelMessage<layer_t>;  //< TunnelMessage trait alias
  using aes_tunnel_message_t =  data::TunnelMessage<aes_layer_t>;  //< AES TunnelMessage trait alias
  using chacha_tunnel_message_t =  data::TunnelMessage<chacha_layer_t>;  //< AES TunnelMessage trait alias
  using i2np_block_t = data::I2NPBlock;  //< I2NP message block trait alias
  using i2np_buffer_t = i2np_block_t::buffer_t;  //< I2NP message buffer trait alias
  using hop_t = tunnels::Hop<layer_t>;  //< Hop trait alias
  using ibgw_t = tunnels::InboundGateway<layer_t>;  //< InboundGateway trait alias
  using obep_t = tunnels::OutboundEndpoint<layer_t>;  //< OutboundEndpoint trait alias
  using proc_v = std::variant<hop_t, ibgw_t, obep_t>;  //< Processor variant trait alias
  using creation_time_t = std::uint32_t;  //< Creation time trait alias
  using expiration_t = std::uint32_t;  //< Expiration time trait alias

  /// @brief Fully-initializing ctor, create a TransitTunnel from an incoming BuildRequestRecord
  /// @param local_info Local RouterInfo that received the message
  /// @param record BuildRequestRecord requesting tunnel participation
  TransitTunnel(std::unique_ptr<layer_t> layer, build_request_t& record)
      : creation_(tini2p::time::now_s()), expiration_(creation_ + tini2p::under_cast(Expiration))
  {
    const exception::Exception ex{"Tunnel: TransitTunnel", __func__};

    if (layer == nullptr)
      ex.throw_ex<std::invalid_argument>("null layer encryption.");

    auto& record_buf = record.buffer();
    const auto& flags = record.flags();

    creator_ident_ = record.local_ident_hash();
    next_ident_ = record.next_ident_hash();

    if (flags == build_request_t::flags_t::ChaChaHop || flags == build_request_t::flags_t::ChaChaOBEP)
      layer->key_info().receive_key(record.receive_key());

    // Accept the tunnel
    build_reply_t reply;
    auto& reply_buf = reply.buffer();
    layer->EncryptReplyRecord(reply.buffer());
    record_buf = reply_buf;

    // Create the tunnel processor from decrypted record parameters
    // Note: processor variant emplace call requires "template" keyword because of dependent types
    if (flags == build_request_t::flags_t::Hop || flags == build_request_t::flags_t::ChaChaHop)
      proc_.template emplace<hop_t>(record.tunnel_id(), record.next_tunnel_id(), std::move(layer));
    else if (flags == build_request_t::flags_t::IBGW || flags == build_request_t::flags_t::ChaChaIBGW)
      proc_.template emplace<ibgw_t>(record.tunnel_id(), record.next_tunnel_id(), std::move(layer));
    else if (flags == build_request_t::flags_t::OBEP || flags == build_request_t::flags_t::ChaChaOBEP)
      proc_.template emplace<obep_t>(record.tunnel_id(), record.next_tunnel_id(), std::move(layer));
    else
      ex.throw_ex<std::logic_error>("invalid build request flags.");
  }

  /// @brief Handle an outbound AES TunnelMessage
  /// @detail If tunnel processor is an OutboundEndpoint, returns a collection of completed I2NP message buffers
  /// @param message TunnelMessage to process
  /// @throws Logic error if the tunnel processor is not a Hop or OutboundEndpoint
  /// @return Optional pointer to a collection of I2NP message buffers
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  decltype(auto) HandleTunnelMessage(aes_tunnel_message_t& message)
  {
    const exception::Exception ex{"Tunnel: TransitTunnel", __func__};

    check_hop_obep(proc_, ex);

    // Check and add the nonces to the Bloom
    std::visit([&message, &ex](auto& p) { p.check_bloom_filter(message.iv(), ex); }, proc_);

    std::vector<std::pair<std::optional<ident_hash_t>, data::I2NPMessage>> i2np_msgs;
    if (std::holds_alternative<hop_t>(proc_))
      std::get<hop_t>(proc_).Encrypt(message);
    else
      {
        auto& obep = std::get<obep_t>(proc_);
        obep.Encrypt(message);
        message.deserialize();
        i2np_msgs = obep.ExtractI2NPFragments(message);
      }

    return i2np_msgs;
  }

  /// @brief Handle an outbound AES TunnelMessage
  /// @detail If tunnel processor is an OutboundEndpoint, returns a collection of completed I2NP message buffers
  /// @param message TunnelMessage to process
  /// @throws Logic error if the tunnel processor is not a Hop or OutboundEndpoint
  /// @return Optional pointer to a collection of I2NP message buffers
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  decltype(auto) HandleTunnelMessage(chacha_tunnel_message_t& message)
  {
    const exception::Exception ex{"Tunnel: TransitTunnel", __func__};

    check_hop_obep(proc_, ex);

    // Check and add the nonces to the Bloom
    std::visit([&message, &ex](auto& p) { p.check_bloom_filter(message.nonces(), ex); }, proc_);

    std::vector<std::pair<std::optional<ident_hash_t>, data::I2NPMessage>> i2np_msgs;
    if (std::holds_alternative<hop_t>(proc_))
      {
        auto& hop = std::get<hop_t>(proc_);

        hop.DecryptAEAD(message);
        hop.Encrypt(message);
        hop.EncryptAEAD(message);
      }
    else
      {
        auto& obep = std::get<obep_t>(proc_);
        obep.DecryptAEAD(message);
        obep.Encrypt(message);
        message.deserialize();
        i2np_msgs = obep.ExtractI2NPFragments(message);
      }

    return i2np_msgs;
  }

  /// @brief Handle inbound I2NP message as the InboundGateway
  /// @param i2np_buf I2NP message buffer to process
  /// @throws Logic error if tunnel processor is not an InboundGateway
  /// @throws Runtime error if tunnel creator Identity hash is null
  /// @return Encrypted tunnel message(s) to send to the next InboundTunnel hop
  decltype(auto) HandleI2NPMessage(const i2np_buffer_t& i2np_buf)
  {
    const exception::Exception ex{"Tunnel: TransitTunnel", __func__};

    check_ibgw(proc_, ex);

    auto messages = std::get<ibgw_t>(proc_).CreateTunnelMessages(i2np_buf, std::nullopt, std::nullopt);
    EncryptMessages(messages, ex);

    return messages;
  }

  /// @brief Get a const reference to the tunnel processor
  const proc_v& processor() const noexcept
  {
    return proc_;
  }

  /// @brief Get a non-const reference to the tunnel processor
  proc_v& processor() noexcept
  {
    return proc_;
  }

  /// @brief Get the creation time of the TransitTunnel
  /// @detail Time is in seconds since epoch
  const creation_time_t& creation() const noexcept
  {
    return creation_;
  }

  /// @brief Get the expiration time of the TransitTunnel
  /// @detail Time is in seconds since epoch
  const expiration_t& expiration() const noexcept
  {
    return expiration_;
  }

  /// @brief Get a const reference to the TransitTunnel tunnel ID
  tunnel_id_t tunnel_id() const
  {
    return std::visit([](const auto& p){ return p.tunnel_id(); }, proc_);
  }

  /// @brief Get a const reference to the next Hop's tunnel ID
  tunnel_id_t next_tunnel_id() const
  {
    return std::visit([](const auto& p){ return p.next_id(); }, proc_);
  }

  /// @brief Get a const reference to the tunnel creator's Identity hash
  const ident_hash_t& creator_ident() const noexcept
  {
    return creator_ident_;
  }

  /// @brief Get a const reference to the next Hop's Identity hash
  const ident_hash_t& next_ident() const noexcept
  {
    return next_ident_;
  }

 private:
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  void EncryptMessages(std::vector<aes_tunnel_message_t>& messages, const exception::Exception& ex)
  {
    check_ibgw(proc_, ex);

    auto& ibgw = std::get<ibgw_t>(proc_);
    for (auto& message : messages)
      ibgw.Encrypt(message);
  }

  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  void EncryptMessages(std::vector<chacha_tunnel_message_t>& messages, const exception::Exception& ex)
  {
    check_ibgw(proc_, ex);

    auto& ibgw = std::get<ibgw_t>(proc_);
    for (auto& message : messages)
      {
        ibgw.Encrypt(message);
        ibgw.EncryptAEAD(message);
      }
  }

  void check_hop_obep(const proc_v& proc, const exception::Exception& ex) const
  {
    if (!std::holds_alternative<hop_t>(proc) && !std::holds_alternative<obep_t>(proc))
      ex.throw_ex<std::logic_error>("invalid tunnel processor.");
  }

  void check_ibgw(const proc_v& proc, const exception::Exception& ex) const
  {
    if (!std::holds_alternative<ibgw_t>(proc_))
      ex.throw_ex<std::logic_error>("invalid tunnel processor.");
  }

  creation_time_t creation_;
  expiration_t expiration_;
  ident_hash_t creator_ident_, next_ident_;
  proc_v proc_;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_TRANSIT_TUNNEL_H_
