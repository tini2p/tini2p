/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_POOL_H_
#define SRC_TUNNELS_POOL_H_

#include <numeric>

#include "src/crypto/blake.h"
#include "src/crypto/rand.h"

#include "src/data/i2np/data.h"
#include "src/data/i2np/tunnel_gateway.h"

#include "src/tunnels/tunnel.h"
#include "src/tunnels/profiler.h"
#include "src/tunnels/zero_hop.h"

namespace tini2p
{
namespace tunnels
{
/// @brief Context for test message checksum
static const crypto::Blake2b::context_t TEST_CHECKSUM_CTX(std::string("TestMessageCheck"));

/// @struct TestInfo
/// @brief Information for tunnel test messages
struct TestInfo
{
  enum
  {
    Expiration = 30,  //< in seconds, time past creation when test expires
  };

  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using peer_id_t = std::uint32_t;  //< Peer ID trait alias
  using peer_ids_t = std::vector<peer_id_t>;  //< Peer ID collection trait alias
  using creation_t = std::uint32_t;  //< Creation time trait alias
  using to_hash_t = data::Identity::hash_t;  //< To hash trait alias
  using checksum_t = crypto::Blake2b::digest_t;  //< Checksum trait alias

  tunnel_id_t creator_tunnel_id;
  tunnel_id_t to_tunnel_id;
  to_hash_t to_hash;
  message_id_t message_id;
  creation_t creation;
  checksum_t checksum;
  peer_ids_t peers;
};

/// @class Pool
/// @brief Class for managing inbound and outbound tunnels
template <class TTunnel>
class Pool
{
 public:
  // TODO(tini2p): change adjustable limits to config options
  enum
  {
    CheckProfilerTimeout = 45,  //< in seconds, update peer profiler
    CheckTunnelTimeout = 5,  //< in seconds, check for expired tunnels
    MaxZeroHops = 10,  //< maximum number of ZeroHop tunnels
    TunnelsLimit = 32,  //< upper limit for established tunnels, adjust as necessary
    ExploratoryLimit = 16,  //< upper limit for building tunnels, adjust as necessary
    MinTestLen = 32,  //< minimum length for a test message
    MaxTestLen = 528,  //< maximum length for a test message
  };

  using blake_t = crypto::Blake2b;  //< Blake2b trait alias
  using info_t = data::Info;  //< RouterInfo trait alias
  using i2np_block_t = data::I2NPBlock;  //< I2NP block trait alias
  using i2np_data_t = data::Data;  //< I2NP Data trait alias
  using i2np_gateway_t = data::TunnelGateway;  //< I2NP TunnelGateway trait alias
  using i2np_buffer_t = i2np_block_t::buffer_t;  //< I2NP buffer trait alias
  using lease_t = data::Lease2;  //< Lease2 trait alias
  using tunnel_t = TTunnel;  //< Tunnel trait alias
  using aes_inbound_t = tunnels::InboundTunnel<crypto::TunnelAES>; //< AES InboundTunnel trait alias
  using aes_outbound_t = tunnels::OutboundTunnel<crypto::TunnelAES>;  //< AES OutboundTunnel trait alias
  using chacha_inbound_t = tunnels::InboundTunnel<crypto::TunnelChaCha>; //< ChaCha InboundTunnel trait alias
  using chacha_outbound_t = tunnels::OutboundTunnel<crypto::TunnelChaCha>;  //< ChaCha OutboundTunnel trait alias
  using tunnels_t = std::vector<tunnel_t>;  //< Tunnel collection trait alias
  using zero_hop_t = tunnels::ZeroHop<typename tunnel_t::creator_t::layer_t>;  //< Zero-hop tunnel trait alias
  using zero_hops_t = std::vector<zero_hop_t>;  //< Zero-hop tunnel collection trait alias
  using tunnel_id_t = typename tunnel_t::creator_t::tunnel_id_t;  //< Tunnel ID trait alias
  using message_id_t = typename tunnel_t::creator_t::message_id_t;  //< Message ID trait alias
  using to_hash_t = typename tunnel_t::creator_t::to_hash_t;  //< To hash trait alias
  using build_message_t = typename tunnel_t::creator_t::build_message_t;  //< TunnelBuildMessage trait alias
  using reply_message_t = typename tunnel_t::creator_t::reply_message_t;  //< TunnelBuildReply trait alias
  using tunnel_message_t = typename tunnel_t::creator_t::tunnel_message_t;  //< TunnelMessage trait alias
  using profiler_t = tunnels::Profiler;  //< Profiler trait alias
  using test_info_t = tunnels::TestInfo;  //< TestInfo trait alias
  using tests_t = std::vector<test_info_t>;  //< TestInfo collection trait alias
  using io_context_t = asio::io_context;  //< IO context trait alias
  using timer_t = asio::steady_timer;  //< Timer trait alias
  using error_c = asio::error_code;  //< ASIO error code trait alias

  /// @brief Default ctor, creates an empty tunnel Pool
  Pool() : tunnels_(), building_(), zero_hops_(), test_key_(blake_t::create_key()), profiler_ctx_(), tunnels_ctx_()
  {
    RunProfilerTimer();
    RunTunnelsTimer();
  }

  /// @brief Dtor, stops the timer for checking expired tunnels
  ~Pool()
  {
    StopProfilerTimer();
    StopTunnelsTimer();
  }

  /// @brief Delete copy-ctor to make Pool move-only
  Pool(const Pool&) = delete;

  /// @brief Delete copy-assignment operator to make Pool move-only
  void operator=(const Pool&) = delete;

  /// @brief Add an building tunnel to the Pool
  /// @param info Local RouterInfo for the tunnel creator (InboundEndpoint or OutboundGateway)
  /// @param reply_lease Lease for the reply tunnel gateway
  /// @param hop_infos RouterInfos for the hops in this tunnel
  /// @return VariableTunnelBuildMessage to send to the first tunnel hop
  data::I2NPMessage AddTunnel(info_t::shared_ptr info, lease_t reply_lease, const std::vector<info_t::shared_ptr>& hop_infos)
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};
    
    std::scoped_lock sgd(building_mutex_);

    const auto& explore_limit = tini2p::under_cast(ExploratoryLimit);

    if (building_.size() == explore_limit)
      ex.throw_ex<std::logic_error>("building tunnel limit reached: " + std::to_string(explore_limit));

    building_.emplace_back(
        std::forward<info_t::shared_ptr>(info), std::forward<lease_t>(reply_lease), hop_infos);

    auto& tunnel = building_.back();

    // add tunnel peers to the profiler
    // wait to update peer speed until receiving the build reply
    profiler_.add_peers(tunnel.peer_hashes());

    return tunnel.CreateBuildMessage();
  }

  /// @brief Handle a TunnelBuildReply message for an building Tunnel
  /// @detail For inbound building tunnels, the reply gateway is the tunnel's InboundGateway.
  ///     For outbound expoloratory tunnels, the reply gateway is either an InboundGateway or ZeroHop gateway.
  /// @param i2np_block I2NP block containing a VariableTunnelBuildReply message
  Pool& HandleBuildReply(data::I2NPMessage i2np_msg)
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    std::scoped_lock sgd(building_mutex_);

    const auto& msg_id = i2np_msg.message_id();
    const auto exp_begin = building_.begin();
    const auto exp_end = building_.end();

    auto exp_it = std::find_if(exp_begin, exp_end, [&msg_id](const auto& t) { return t.build_message_id() == msg_id; });

    if (exp_it == exp_end)
      ex.throw_ex<std::runtime_error>("no building tunnel found for build message ID: " + std::to_string(msg_id));

    // update bytes transferred, account for the original TunnelBuildMessage
    exp_it->add_bytes_transferred(i2np_msg.size() * 2);

    const auto& peer_hashes = exp_it->peer_hashes();
    if (exp_it->HandleBuildReply(std::forward<data::I2NPMessage>(i2np_msg)) == 1)
      {  // Tunnel accepted, move to estbalished tunnels
        std::scoped_lock tgd(tunnels_mutex_);

        const auto& tunnel_limit = tini2p::under_cast(TunnelsLimit);
        if (tunnels_.size() == tunnel_limit)
          {
            building_.erase(exp_it);
            ex.throw_ex<std::logic_error>("established tunnels limit reached: " + std::to_string(tunnel_limit));
          }

        profiler_.add_peer_build_success(peer_hashes);  // add a build success for the tunnel peers

        exp_it->creation_time(time::now_s());  // update the creation time
        tunnels_.emplace_back(std::move(*exp_it));
      }
    else
      {  // update the speed, and add a build fail for the tunnel peers
        profiler_.update_peer_speed(peer_hashes, exp_it->bytes_transferred(), time::now_s() - exp_it->creation_time());
        profiler_.add_peer_build_fail(peer_hashes);
      }

    // Whether accepted or rejected, remove the building tunnel from the Pool
    building_.erase(exp_it);

    return *this;
  }

  /// @brief Handles tunnel message received for an established InboundTunnel
  /// @param message TunnelMessage to decrypt and process
  /// @return Collection of completed I2NP message buffers, if any
  template <typename T = tunnel_t,
      typename = std::enable_if_t<std::is_same<T, aes_inbound_t>::value || std::is_same<T, chacha_inbound_t>::value>>
  decltype(auto) HandleTunnelMessage(tunnel_message_t& message)
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    tunnel_id_t tunnel_id(0);
    tini2p::read_bytes(message.buffer().data(), tunnel_id, tini2p::Endian::Big);

    std::scoped_lock sgd(tunnels_mutex_);

    auto& tunnel = get_tunnel(tunnel_id, ex);
    auto& creator = tunnel.creator();

    if (std::is_same<T, chacha_inbound_t>::value)
      creator.DecryptAEAD(message);

    // Iteratively decrypt the tunnel messages to recover the original
    creator.IterativeDecryption(message);
    message.deserialize();

    // update the bytes transferred through the tunnel
    tunnel.add_bytes_transferred(message.size());

    // Extract the I2NP message fragments from the tunnel message, returning any completed I2NP message buffers
    auto i2np_msgs = creator.ExtractI2NPFragments(message);

    // Check for any test messages
    if (!i2np_msgs.empty())
      {
        std::scoped_lock sgd(tests_mutex_);

        for (const auto& msg : i2np_msgs)
          {
            if (has_test_info(msg.second.message_id()))
              ValidateTest(msg.second, ex);
          }
      }

    return i2np_msgs;
  }

  /// @brief Handle an outbound I2NP message
  /// @param out_tunnel_id Tunnel ID of the OutboundTunnel to send the I2NP message through
  /// @param i2np_buf I2NP message buffer to convert to tunnel messages
  /// @param to_hash Identity hash of the InboundGateway for the message Destination
  /// @param to_tunnel_id Tunnel ID of the InboundGateway for the message Destination, null for Router delivery
  /// @return Collection of tunnel messages to send through the OutboundTunnel
  template <typename T = tunnel_t,
      typename = std::enable_if_t<std::is_same<T, aes_outbound_t>::value || std::is_same<T, chacha_outbound_t>::value>>
  decltype(auto) HandleI2NPMessage(
      const tunnel_id_t& out_id,
      const i2np_buffer_t& i2np_buf,
      to_hash_t to_hash,
      tunnel_id_t to_tunnel_id = 0)
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    std::scoped_lock sgd(tunnels_mutex_);

    return HandleI2NPMessage(
        get_tunnel(out_id, ex), i2np_buf, std::forward<to_hash_t>(to_hash), std::forward<tunnel_id_t>(to_tunnel_id));
  }

  /// @brief Handle an outbound I2NP message through a random OutboundTunnel
  /// @param i2np_buf I2NP message buffer to convert to tunnel messages
  /// @param to_hash Identity hash of the InboundGateway for the message Destination
  /// @param to_tunnel_id Tunnel ID of the InboundGateway for the message Destination, null for Router delivery
  /// @return Collection of tunnel messages to send through the OutboundTunnel
  template <typename T = tunnel_t,
      typename = std::enable_if_t<std::is_same<T, aes_outbound_t>::value || std::is_same<T, chacha_outbound_t>::value>>
  decltype(auto) HandleI2NPMessage(const i2np_buffer_t& i2np_buf, to_hash_t to_hash, tunnel_id_t to_tunnel_id = 0)
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    std::scoped_lock sgd(tunnels_mutex_);

    if (tunnels_.empty())
      ex.throw_ex<std::runtime_error>("no outbound tunnels.");

    return HandleI2NPMessage(
        tunnels_.at(crypto::RandInRange(0, tunnels_.size() - 1)),
        i2np_buf,
        std::forward<to_hash_t>(to_hash),
        std::forward<tunnel_id_t>(to_tunnel_id));
  }

  /// @brief Create a test message for measuring tunnel health
  /// @return Pair of test information, and an I2NP block with test message data
  decltype(auto) CreateTestMessage()
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};
    tunnel_id_t tunnel_id, to_tunnel_id(0);
    test_info_t::to_hash_t to_hash;

    std::shared_lock ts(tunnels_mutex_, std::defer_lock);
    std::scoped_lock sgd(ts);

    const auto is_inbound =
        std::is_same<tunnel_t, aes_inbound_t>::value || std::is_same<tunnel_t, chacha_inbound_t>::value;

    if (tunnels_.empty())
      {
        std::string direction(is_inbound ? "inbound" : "outbound");
        ex.throw_ex<std::runtime_error>("No " + direction + " tunnels available.");
      }

      // select a random tunnel to test
      const auto& tunnel = tunnels_.at(crypto::RandInRange(0, tunnels_.size() - 1));
      const auto& creator = tunnel.creator();
      tunnel_id = creator.tunnel_id();

      if (is_inbound)
        {  // select the IBGW to receive the test message
          const auto& reply_lease = creator.reply_lease();
          to_hash = reply_lease.tunnel_gateway();
          to_tunnel_id = reply_lease.tunnel_id();
        }
      else
        {  // select a ZeroHop tunnel for the return path
          std::shared_lock zs(zero_hop_mutex_, std::defer_lock);
          std::scoped_lock zgd(zs);

          if (zero_hops_.empty())
            ex.throw_ex<std::runtime_error>("no zero-hop tunnels available.");

          const auto& zero_hop = zero_hops_.at(crypto::RandInRange(0, zero_hops_.size()));
          to_hash = zero_hop.info().identity().hash();
          to_tunnel_id = zero_hop.tunnel_id();
        }

    const auto& message_id = crypto::RandInRange(1, 0xFFFFFFFF);

    data::I2NPMessage data_msg(
        data::I2NPMessage::Type::Data, message_id, i2np_data_t(crypto::RandInRange(MinTestLen, MaxTestLen)).buffer());

    // Calculate Blake2b-256 checksum of the Data block
    blake_t::digest_t checksum;
    blake_t::Hash(checksum, data_msg.buffer(), test_key_, TEST_CHECKSUM_CTX);

    // Create TestInfo for tracking the test
    test_info_t test_info{tunnel_id,
                          to_tunnel_id,
                          std::move(to_hash),
                          message_id,
                          time::now_s(),
                          std::move(checksum),
                          profiler_.get_peer_ids(tunnel.peer_hashes())};

    {  // lock tests
      std::scoped_lock sgd(tests_mutex_);
      tests_.emplace_back(test_info);
    }  // end-tests-lock

    return std::make_pair<test_info_t, data::I2NPMessage>(std::move(test_info), std::move(data_msg));
  }

  /// @brief Handle test message reply
  Pool& HandleTestReply(const data::I2NPMessage& message)
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    std::scoped_lock sgd(tests_mutex_);

    ValidateTest(message, ex);

    return *this;
  }

  /// @brief Refill ZeroHop tunnels to the maximum
  /// @param info RouterInfo for the local router
  Pool& CreateZeroHopTunnels(info_t::shared_ptr info)
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    if (info == nullptr)
      ex.throw_ex<std::invalid_argument>("null RouterInfo");

    std::scoped_lock sgd(zero_hop_mutex_);

    const auto& zero_len = zero_hops_.size();
    const auto& max_zero_len = tini2p::under_cast(MaxZeroHops);

    for (std::size_t idx = 0; idx < max_zero_len - zero_len; ++idx)
      zero_hops_.emplace_back(info, lease_t());

    return *this;
  }

  /// @brief Get whether the collection of zero-hop tunnels is empty
  std::uint8_t empty_zero_hops()
  {
    std::shared_lock zs(zero_hop_mutex_, std::defer_lock);
    std::scoped_lock sgd(zs);

    return static_cast<std::uint8_t>(zero_hops_.empty());
  }

  /// @brief Get a random zero-hop tunnel
  zero_hop_t& zero_hop()
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    std::shared_lock zs(zero_hop_mutex_, std::defer_lock);
    std::scoped_lock sgd(zs);

    if (zero_hops_.empty())
      ex.throw_ex<std::runtime_error>("no zero-hop tunnels");

    return zero_hops_.at(crypto::RandInRange(0, zero_hops_.size() - 1));
  }

  /// @brief Get a const reference to the established tunnels in the Pool
  const tunnels_t& tunnels() const noexcept
  {
    return tunnels_;
  }

  /// @brief Get a non-const reference to a tunnel with the given tunnel ID
  tunnel_t& tunnel(const tunnel_id_t& id)
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    std::shared_lock tfs(tunnels_mutex_, std::defer_lock);
    std::scoped_lock sgd(tfs);

    return get_tunnel(id, ex);
  }

  /// @brief Get a non-const reference to a random tunnel
  /// @throws Runtime error if there are no tunnels
  tunnel_t& tunnel()
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    std::shared_lock tfs(tunnels_mutex_, std::defer_lock);
    std::scoped_lock sgd(tfs);

    if (tunnels_.empty())
      ex.throw_ex<std::runtime_error>("no established tunnels");

    return tunnels_.at(crypto::RandInRange(0, tunnels_.size() - 1));
  }

  /// @brief Get whether the Pool has an established tunnel with the given tunnel ID
  std::uint8_t has_tunnel(const tunnel_id_t& tunnel_id)
  {
    std::shared_lock ts(tunnels_mutex_, std::defer_lock);
    std::scoped_lock sgd(ts);

    const auto tun_beg = tunnels_.begin();
    const auto tun_end = tunnels_.end();

    return static_cast<std::uint8_t>(
        std::find_if(tun_beg, tun_end, [&tunnel_id](const auto& t) { return t.tunnel_id() == tunnel_id; }) != tun_end);
  }

  /// @brief Get the number of tunnels in the pool
  std::size_t tunnels_size()
  {
    std::shared_lock ts(tunnels_mutex_, std::defer_lock);
    std::scoped_lock sgd(ts);

    return tunnels_.size();
  }

  /// @brief Get tunnel hop gateway hashes and tunnel IDs
  /// @detail Useful for creating LeaseSets from inbound tunnels, invalid for outbound tunnels
  std::vector<std::pair<info_t::identity_t::hash_t, tunnel_id_t>> get_gateway_hashes_and_ids(const std::uint8_t& num)
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    std::shared_lock ts(tunnels_mutex_, std::defer_lock);
    std::scoped_lock sgd(ts);

    if (tunnels_.empty())
      ex.throw_ex<std::runtime_error>("no tunnels");

    const auto& num_tunnels = tunnels_.size();
    const auto& ret_num = num_tunnels < num ? num_tunnels : num;

    // get a list of indexes
    std::vector<std::size_t> indexes(num_tunnels);
    std::iota(indexes.begin(), indexes.end(), 0);

    // randomize the indexes
    crypto::RandGenerator rng;
    std::shuffle(indexes.begin(), indexes.end(), rng);

    // resize for the number of return hashes and tunnel IDs
    indexes.resize(ret_num);

    std::vector<std::pair<info_t::identity_t::hash_t, tunnel_id_t>> ret_hashes;

    for (const auto& index : indexes)
      {
        const auto& first_hop = tunnels_.at(index).creator().first_hop();
        ret_hashes.emplace_back(std::make_pair(first_hop.info().identity().hash(), first_hop.tunnel_id()));
      }

    return ret_hashes;
  }

  /// @brief Get whether the pool has established tunnels
  std::uint8_t has_tunnels()
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    std::shared_lock tfs(tunnels_mutex_, std::defer_lock);
    std::scoped_lock sgd(tfs);

    return static_cast<std::uint8_t>(!tunnels_.empty());
  }

  /// @brief Get a const reference to the building tunnels in the Pool
  const tunnels_t& building_tunnels() const noexcept
  {
    return building_;
  }

  /// @brief Get whether the Pool has an building tunnel with the given message ID
  /// @detail Useful for processing VariableTunnelBuildReply messages
  std::uint8_t has_building_tunnel(const message_id_t& msg_id)
  {
    std::shared_lock efs(building_mutex_, std::defer_lock);
    std::scoped_lock sgd(efs);

    const auto exp_beg = building_.begin();
    const auto exp_end = building_.end();

    return static_cast<std::uint8_t>(
        std::find_if(exp_beg, exp_end, [&msg_id](const auto& e) { return e.build_message_id() == msg_id; }) != exp_end);
  }

  /// @brief Get a non-const reference to an building tunnel with the given tunnel ID
  tunnel_t& building_tunnel(const tunnel_id_t& id)
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    std::shared_lock efs(building_mutex_, std::defer_lock);
    std::scoped_lock sgd(efs);

    return get_building_tunnel(id, ex);
  }

  /// @brief Get whether the Pool has a test message with the given message ID
  /// @return One if the test is found, zero otherwise
  std::uint8_t has_test(const std::uint32_t& msg_id)
  {
    std::shared_lock tfs(tests_mutex_, std::defer_lock);
    std::scoped_lock sgd(tfs);

    return has_test_info(msg_id);
  }

  /// @brief Get a non-const reference to the peer profiler
  profiler_t& profiler() noexcept
  {
    return profiler_;
  }

 private:
  template <typename T = tunnel_t,
      typename = std::enable_if_t<std::is_same<T, aes_outbound_t>::value || std::is_same<T, chacha_outbound_t>::value>>
  decltype(auto)
  HandleI2NPMessage(tunnel_t& tunnel, const i2np_buffer_t& i2np_buf, to_hash_t to_hash, tunnel_id_t to_tunnel_id)
  {
    auto& creator = tunnel.creator();

    // Create tunnel messages from the I2NP buffer
    auto messages = creator.CreateTunnelMessages(
        i2np_buf,
        std::forward<to_hash_t>(to_hash),
        to_tunnel_id == 0 ? std::nullopt : std::optional<tunnel_id_t>(std::forward<tunnel_id_t>(to_tunnel_id)));

    // Iteratively decrypt the tunnel messages for the outbound hops
    std::size_t message_len(0);
    for (auto& message : messages)
      {
        creator.IterativeDecryption(message);

        // If ChaCha layer encryption, AEAD encrypt for the first hop
        if (std::is_same<T, chacha_outbound_t>::value)
          creator.EncryptAEAD(message);

        message_len += message.size();
      }

    // update the bytes transferred through the tunnel
    tunnel.add_bytes_transferred(message_len);

    return std::make_pair(tunnel.peer_hashes().front(), messages);
  }

  void ValidateTest(const data::I2NPMessage& message, const exception::Exception& ex)
  {
    check_test_message_type(message.type(), ex);

    auto test_it = get_test_info(message.message_id(), ex);
    blake_t::digest_t checksum;
    blake_t::Hash(checksum, message.buffer(), test_key_, TEST_CHECKSUM_CTX);

    if (checksum != test_it->checksum)
      {
        profiler_.add_peer_test_fail(test_it->peers);
        const auto exp_checksum = std::move(test_it->checksum);
        tests_.erase(test_it);

        ex.throw_ex<std::logic_error>(
            "checksum: " + tini2p::bin_to_hex(checksum, ex)
            + " does not match expected: " + tini2p::bin_to_hex(exp_checksum, ex));
      }

    profiler_.add_peer_test_success(test_it->peers);
    tests_.erase(test_it);
  }

  std::uint8_t has_test_info(const std::uint32_t& msg_id) const
  {
    const auto tests_end = tests_.end();

    return static_cast<std::uint8_t>(
        std::find_if(tests_.begin(), tests_end, [&msg_id](const auto& t) { return t.message_id == msg_id; })
        != tests_end);
  }

  decltype(auto) get_test_info(const std::uint32_t& msg_id, const exception::Exception& ex) const
  {
    const auto tests_end = tests_.end();

    auto test_it = std::find_if(tests_.begin(), tests_end, [&msg_id](const auto& t) { return t.message_id == msg_id; });

    if (test_it == tests_end)
      ex.throw_ex<std::invalid_argument>("no test info found for message ID: " + std::to_string(msg_id));

    return test_it;
  }

  void check_test_message_type(const data::I2NPHeader::Type& msg_type, const exception::Exception& ex) const
  {
    if (msg_type != data::I2NPHeader::Type::Data)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid I2NP message type: " + std::to_string(tini2p::under_cast(msg_type))
            + ", expected Data message: " + std::to_string(tini2p::under_cast(data::I2NPHeader::Type::Data)));
      }
  }

  void RunProfilerTimer()
  {
    profiler_timer_ = std::make_unique<timer_t>(
        profiler_ctx_, std::chrono::seconds(tini2p::under_cast(CheckProfilerTimeout)));
    profiler_timer_->async_wait([this](const auto& ec) { UpdateProfiler(ec); });
    profiler_timer_thread_ = std::make_unique<std::thread>([this]() { profiler_ctx_.run(); });
  }

  void RunTunnelsTimer()
  {
    tunnels_timer_ = std::make_unique<timer_t>(tunnels_ctx_, std::chrono::seconds(tini2p::under_cast(CheckTunnelTimeout)));
    tunnels_timer_->async_wait([this](const auto& ec) { HandleExpiredTunnels(ec); });
    tunnels_timer_thread_ = std::make_unique<std::thread>([this]() { tunnels_ctx_.run(); });
  }

  void StopProfilerTimer()
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    check_timer(profiler_timer_, ex);

    try
      {
        profiler_ctx_.stop();
        profiler_timer_->cancel();

        if (profiler_timer_thread_ != nullptr)
          {
            profiler_timer_thread_->join();
            profiler_timer_thread_.reset();
          }
      }
    catch(const std::exception& e)
      {
        ex.throw_ex<std::runtime_error>("timer cancellation error: " + std::string(e.what()));
      }
  }

  void StopTunnelsTimer()
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    check_timer(tunnels_timer_, ex);

    try
      {
        tunnels_ctx_.stop();
        tunnels_timer_->cancel();

        if (tunnels_timer_thread_ != nullptr)
          {
            tunnels_timer_thread_->join();
            tunnels_timer_thread_.reset();
          }
      }
    catch(const std::exception& e)
      {
        ex.throw_ex<std::runtime_error>("timer cancellation error: " + std::string(e.what()));
      }
  }

  void UpdateProfiler(const error_c& ec)
  {
    if (ec != asio::error::operation_aborted)
      {
        const auto& profiler_timeout = tini2p::under_cast(CheckProfilerTimeout);

        {  // lock established and building tunnels
          std::scoped_lock(tunnels_mutex_, building_mutex_);

          for (auto& tunnel : tunnels_)
            {
              profiler_.update_peer_speed(tunnel.peer_hashes(), tunnel.bytes_transferred(), profiler_timeout);
              tunnel.clear_bytes_transferred();
            }

          for (auto& tunnel : building_)
            {
              profiler_.update_peer_speed(tunnel.peer_hashes(), tunnel.bytes_transferred(), profiler_timeout);
              tunnel.clear_bytes_transferred();
            }
        }  // end-building-established-tunnels-lock

        profiler_.update_peer_groups();

        // reset the timer
        profiler_timer_->expires_after(std::chrono::seconds(tini2p::under_cast(CheckProfilerTimeout)));
        profiler_timer_->async_wait([this](const auto& ec) { UpdateProfiler(ec); });
      }
    else
      StopProfilerTimer();
  }

  void HandleExpiredTunnels(const error_c& ec)
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    check_timer(tunnels_timer_, ex);

    if (ec != asio::error::operation_aborted)
      {
        {  // lock established and building tunnels
          std::scoped_lock sgd(building_mutex_, tunnels_mutex_, tests_mutex_, zero_hop_mutex_);

          const auto now = tini2p::time::now_s();
          const auto is_expired = [&now](const auto& t) { return t.expiration_time() <= now; };

          // check for expired/dropped tests
          CheckExpiredTests();

          // erase expired established tunnels
          const auto tun_end = tunnels_.end();
          tunnels_.erase(std::remove_if(tunnels_.begin(), tun_end, is_expired), tun_end);

          // erase expired building tunnels
          const auto exp_end = building_.end();
          building_.erase(std::remove_if(building_.begin(), exp_end, is_expired), exp_end);

          // erase expired ZeroHop tunnels
          const auto zero_end = zero_hops_.end();
          zero_hops_.erase(std::remove_if(zero_hops_.begin(), zero_end, is_expired), zero_end);
        }  // end-established-and-building-tunnels-lock

        // reset the timer
        tunnels_timer_->expires_after(std::chrono::seconds(tini2p::under_cast(CheckTunnelTimeout)));
        tunnels_timer_->async_wait([this](const auto& ec) { HandleExpiredTunnels(ec); });
      }
    else
      StopTunnelsTimer();
  }

  // caller must lock tunnels mutex
  void CheckExpiredTests()
  {
    const exception::Exception ex{"Tunnel: Pool", __func__};

    const auto& test_expiration = tini2p::under_cast(test_info_t::Expiration);
    const auto& now = time::now_s();
    const auto tests_end = tests_.end();

    // find expired tests
    auto remove_it = std::remove_if(tests_.begin(), tests_end, [&now, &test_expiration](const auto& t) {
      return t.creation + test_expiration < now;
    });

    // for all expired tests, add dropped test to the profiler
    for (auto exp_it = remove_it; exp_it != tests_end; ++exp_it)
      profiler_.add_peer_test_drop(exp_it->peers);

    // erase expired tests
    tests_.erase(remove_it, tests_end);
  }

  tunnel_t& get_tunnel(const tunnel_id_t& id, const exception::Exception& ex)
  {
    const auto tunnels_end = tunnels_.end();

    auto tun_it =
        std::find_if(tunnels_.begin(), tunnels_end, [&id](const auto& t) { return t.creator().tunnel_id() == id; });

    if (tun_it == tunnels_end)
      ex.throw_ex<std::runtime_error>("no tunnel found for tunnel ID: " + std::to_string(id));

    return *tun_it;
  }

  tunnel_t& get_building_tunnel(const tunnel_id_t& id, const exception::Exception& ex)
  {
    const auto building_end = building_.end();

    auto tun_it = std::find_if(
        building_.begin(), building_end, [&id](const auto& t) { return t.creator().tunnel_id() == id; });

    if (tun_it == building_end)
      ex.throw_ex<std::runtime_error>("no tunnel found for tunnel ID: " + std::to_string(id));

    return *tun_it;
  }

  void check_timer(const std::unique_ptr<timer_t>& timer, const exception::Exception& ex) const
  {
    if (tunnels_timer_ == nullptr)
      ex.throw_ex<std::runtime_error>("null timer.");
  }

  tunnels_t tunnels_;
  std::shared_mutex tunnels_mutex_;

  tunnels_t building_;
  std::shared_mutex building_mutex_;

  zero_hops_t zero_hops_;
  std::shared_mutex zero_hop_mutex_;

  tests_t tests_;
  std::shared_mutex tests_mutex_;

  blake_t::key_t test_key_;

  io_context_t profiler_ctx_, tunnels_ctx_;
  std::unique_ptr<std::thread> profiler_timer_thread_, tunnels_timer_thread_;
  std::unique_ptr<timer_t> profiler_timer_, tunnels_timer_;

  profiler_t profiler_;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_POOL_H_
