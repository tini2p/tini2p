/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_PROFILER_H_
#define SRC_TUNNELS_PROFILER_H_

#include <deque>
#include <mutex>
#include <shared_mutex>

#include "src/bytes.h"

#include "src/crypto/rand.h"

#include "src/tunnels/peer_profile.h"

namespace tini2p
{
namespace tunnels
{
/// @struct GroupPeer
/// @brief For tracking peers in fast, high capacity, and standard groups
struct GroupPeer
{
  using peer_id_t = std::uint32_t;  //< Peer ID trait alias
  using speed_t = std::uint32_t;  //< Speed trait alias
  using capacity_t = std::uint32_t;  //< Capacity trait alias

  GroupPeer() : peer_id(0), speed(0), capacity(0) {}

  GroupPeer(peer_id_t id) : peer_id(std::forward<peer_id_t>(id)), speed(0), capacity(0) {}

  std::uint32_t peer_id;
  std::uint32_t speed;
  std::uint32_t capacity;
};

/// @class Profiler
/// @brief Class for managing peer profiles
class Profiler
{
 public:
  enum : std::uint8_t
  {
    MaxFastPeers = 30,
    MaxHighCapPeers = 75,
  };

  using peer_id_t = std::uint32_t;  //< Peer profile ID trait alias
  using peer_ids_t = std::vector<std::uint32_t>;  //< Peer profile ID collection trait alias
  using peer_profile_t = tunnels::PeerProfile;  //< PeerProfile trait alias
  using profiles_t = std::deque<peer_profile_t>;  //< Profile collection trait alias
  using peer_hash_t = peer_profile_t::ident_hash_t;  //< Peer hash trait alias
  using peer_hashes_t = std::vector<peer_hash_t>;  //< Peer hash collection trait alias
  using group_peer_t = GroupPeer;  //< Group peer trait alias
  using fast_peers_t = std::vector<group_peer_t>;  //< Fast peers trait alias
  using high_cap_peers_t = std::deque<group_peer_t>;  //< High capacity peers trait alias
  using standard_peers_t = std::deque<group_peer_t>;  //< Standard peers trait alias

  /// @brief Default ctor, create an empty Profiler
  Profiler() : profiles_() {}

  /// @brief Add a peer for the given Identity hash
  Profiler& add_peer(peer_hash_t peer_hash)
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    check_peer(peer_hash, ex);

    std::scoped_lock sgd(profiles_mutex_, standard_mutex_);

    check_additional_peer(profiles_, peer_hash, ex);
    profiles_.emplace_back(std::forward<peer_hash_t>(peer_hash));

    standard_peers_.emplace_back(profiles_.back().peer_id());

    return *this;
  }

  /// @brief Add a list of peers
  /// @detail Ignores duplicate peer hashes, only adding new peers
  /// @param peer_hashes Peer Identity hashes
  Profiler& add_peers(peer_hashes_t peer_hashes)
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    if (peer_hashes.empty())
      ex.throw_ex<std::invalid_argument>("null peer hashes.");

    for (const auto& hash : peer_hashes)
      check_peer(hash, ex);

    std::scoped_lock sgd(profiles_mutex_, standard_mutex_);

    for (auto& hash : peer_hashes)
      {
        if (std::find_if(profiles_.begin(), profiles_.end(), [&hash](const auto& p) { return p.ident_hash() == hash; })
            == profiles_.end())
          {
            profiles_.emplace_back(std::forward<peer_hash_t>(hash));
            standard_peers_.emplace_back(profiles_.back().peer_id());
          }
      }

    return *this;
  }

  /// @brief Get the peer IDs for a list of peer Identity hashes
  peer_ids_t get_peer_ids(const peer_hashes_t& peer_hashes)
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    std::shared_lock ps(profiles_mutex_, std::defer_lock);
    std::scoped_lock sgd(ps);

    check_peers(peer_hashes, ex);

    peer_ids_t peer_ids;

    for (const auto& peer : peer_hashes)
      peer_ids.emplace_back(find_profile(peer, ex).peer_id());

    return peer_ids;
  }

  /// @brief Update transfer speed for a list of peers
  /// @tparam TPeers Peers collection type
  /// @param peers Peers to update (IDs or Identity hashes)
  /// @param bytes Bytes transferred in the sample interval
  /// @param secs Speed sample interval (in seconds)
  template <class TPeers>
  Profiler& update_peer_speed(
      const TPeers& peers,
      const peer_profile_t::bytes_t& bytes,
      const peer_profile_t::secs_t& secs)
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    check_time(secs, ex);

    std::scoped_lock sgd(profiles_mutex_);

    check_peers(peers, ex);

    for (const auto& peer : peers)
      find_profile(peer, ex).speed(bytes, secs);

    return *this;
  }

  /// @brief Add successful build for a list of peers
  /// @tparam TPeers Peers collection type
  /// @param peers Peers to update
  template <class TPeers>
  Profiler& add_peer_build_success(const TPeers& peers)
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    std::scoped_lock sgd(profiles_mutex_);

    check_peers(peers, ex);

    for (const auto& peer : peers)
      find_profile(peer, ex).add_build_success();

    return *this;
  }

  /// @brief Add failed build for a list of peers
  /// @tparam TPeers Peers collection type
  /// @param peers Peers to update
  template <class TPeers>
  Profiler& add_peer_build_fail(const TPeers& peers)
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    std::scoped_lock sgd(profiles_mutex_);

    check_peers(peers, ex);

    for (const auto& peer : peers)
      find_profile(peer, ex).add_build_fail();

    return *this;
  }

  /// @brief Add dropped build for a list of peers
  /// @tparam TPeers Peers collection type
  /// @param peers Peers to update
  template <class TPeers>
  Profiler& add_peer_build_drop(const TPeers& peers)
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    std::scoped_lock sgd(profiles_mutex_);

    check_peers(peers, ex);

    for (const auto& peer : peers)
      find_profile(peer, ex).add_build_drop();

    return *this;
  }

  /// @brief Add successful test for a list of peers
  /// @tparam TPeers Peers collection type
  /// @param peers Peers to update
  template <class TPeers>
  Profiler& add_peer_test_success(const TPeers& peers)
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    std::scoped_lock sgd(profiles_mutex_);

    check_peers(peers, ex);

    for (const auto& peer : peers)
      find_profile(peer, ex).add_test_success();

    return *this;
  }

  /// @brief Add failed test for a list of peers
  /// @tparam TPeers Peers collection type
  /// @param peers Peers to update
  template <class TPeers>
  Profiler& add_peer_test_fail(const TPeers& peers)
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    std::scoped_lock sgd(profiles_mutex_);

    check_peers(peers, ex);

    for (const auto& peer : peers)
      find_profile(peer, ex).add_test_fail();

    return *this;
  }

  /// @brief Add dropped test for a list of peers
  /// @tparam TPeers Peers collection type
  /// @param peers Peers to update
  template <class TPeers>
  Profiler& add_peer_test_drop(const TPeers& peers)
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    std::scoped_lock sgd(profiles_mutex_);

    check_peers(peers, ex);

    for (const auto& peer : peers)
      find_profile(peer, ex).add_test_drop();

    return *this;
  }

  /// @brief Get a const reference to a peer profile
  /// @tparam Peer identifier type
  /// @param peer Peer identifier (ID or Identity hash)
  template <class TPeer>
  const peer_profile_t& find_profile(const TPeer& peer)
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    check_peer(peer, ex);

    std::shared_lock ps(profiles_mutex_, std::defer_lock);
    std::scoped_lock sgd(ps);

    return find_profile(peer, ex);
  }

  /// @brief Update peer profile fast and high-capacity groups
  Profiler& update_peer_groups()
  {
    std::scoped_lock sgd(profiles_mutex_, fast_mutex_, high_cap_mutex_);

    const auto& median_capacity = calculate_median_capacity();
    const auto& median_speed = calculate_median_speed();

    fast_peers_.clear();
    high_cap_peers_.clear();

    for (const auto& profile : profiles_)
      {
        const auto& peer_id = profile.peer_id();
        const auto& peer_capacity = profile.capacity();
        const auto& peer_speed = profile.speed();

        if(add_high_cap_peer(high_cap_peers_, peer_id, peer_capacity, median_capacity, peer_speed))
          add_fast_peer(fast_peers_, peer_id, peer_speed, median_speed, peer_capacity);
      }

    return *this;
  }

  /// @brief Get a peer from the fast group
  const peer_hash_t& get_fast_peer()
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    std::shared_lock fs(fast_mutex_, std::defer_lock);
    std::shared_lock ps(profiles_mutex_, std::defer_lock);
    std::scoped_lock sgd(fs, ps);

    if (fast_peers_.empty())
      ex.throw_ex<std::logic_error>("no fast peers.");

    const auto& fast_id = fast_peers_.at(crypto::RandInRange(0, fast_peers_.size() - 1)).peer_id;

    return find_profile(fast_id, ex).ident_hash();
  }

  /// @brief Get a peer from the high capacity group
  const peer_hash_t& get_high_cap_peer()
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    std::shared_lock fs(high_cap_mutex_, std::defer_lock);
    std::shared_lock ps(profiles_mutex_, std::defer_lock);
    std::scoped_lock sgd(fs, ps);

    if (high_cap_peers_.empty())
      ex.throw_ex<std::logic_error>("no high capacity peers.");

    const auto& high_id = high_cap_peers_.at(crypto::RandInRange(0, high_cap_peers_.size() - 1)).peer_id;

    return find_profile(high_id, ex).ident_hash();
  }

  /// @brief Get a peer from the standard group
  const peer_hash_t& get_standard_peer()
  {
    const exception::Exception ex{"Tunnel: Profiler", __func__};

    std::shared_lock fs(standard_mutex_, std::defer_lock);
    std::shared_lock ps(profiles_mutex_, std::defer_lock);
    std::scoped_lock sgd(fs, ps);

    if (standard_peers_.empty())
      ex.throw_ex<std::logic_error>("no standard peers.");

    const auto& std_id = standard_peers_.at(crypto::RandInRange(0, standard_peers_.size() - 1)).peer_id;

    return find_profile(std_id, ex).ident_hash();
  }

 private:
  const peer_profile_t& find_profile(const peer_hash_t& peer_hash, const exception::Exception& ex) const
  {
    const auto prof_end = profiles_.end();

    auto it =
        std::find_if(profiles_.begin(), prof_end, [&peer_hash](const auto& p) { return p.ident_hash() == peer_hash; });

    if (it == prof_end)
      ex.throw_ex<std::runtime_error>("no profile found for peer hash: " + tini2p::bin_to_hex(peer_hash, ex));

    return *it;
  }

  peer_profile_t& find_profile(const peer_hash_t& peer_hash, const exception::Exception& ex)
  {
    const auto prof_end = profiles_.end();

    auto it =
        std::find_if(profiles_.begin(), prof_end, [&peer_hash](const auto& p) { return p.ident_hash() == peer_hash; });

    if (it == prof_end)
      ex.throw_ex<std::runtime_error>("no profile found for peer hash: " + tini2p::bin_to_hex(peer_hash, ex));

    return *it;
  }

  const peer_profile_t& find_profile(const peer_id_t& peer_id, const exception::Exception& ex) const
  {
    const auto prof_end = profiles_.end();

    auto it =
        std::find_if(profiles_.begin(), prof_end, [&peer_id](const auto& p) { return p.peer_id() == peer_id; });

    if (it == prof_end)
      ex.throw_ex<std::runtime_error>("no profile found for peer ID: " + std::to_string(peer_id));

    return *it;
  }

  peer_profile_t& find_profile(const peer_id_t& peer_id, const exception::Exception& ex)
  {
    const auto prof_end = profiles_.end();

    auto it =
        std::find_if(profiles_.begin(), prof_end, [&peer_id](const auto& p) { return p.peer_id() == peer_id; });

    if (it == prof_end)
      ex.throw_ex<std::runtime_error>("no profile found for peer ID: " + std::to_string(peer_id));

    return *it;
  }

  peer_profile_t::capacity_t calculate_median_capacity()
  {
    std::sort(
        profiles_.begin(), profiles_.end(), [](const auto& pl, const auto& pr) { return pl.capacity() < pr.capacity(); });

    const auto median_idx = (profiles_.size() / 2);

    return profiles_.empty() ? 0 : profiles_.at(median_idx).capacity();
  }

  peer_profile_t::speed_t calculate_median_speed()
  {
    std::sort(
        profiles_.begin(), profiles_.end(), [](const auto& pl, const auto& pr) { return pl.speed() < pr.speed(); });

    const auto median_idx = (profiles_.size() / 2);

    return profiles_.empty() ? 0 : profiles_.at(median_idx).speed();
  }

  std::uint8_t add_high_cap_peer(
      high_cap_peers_t& high_cap_peers,
      const peer_profile_t::peer_id_t& peer_id,
      const peer_profile_t::capacity_t& peer_capacity,
      const peer_profile_t::capacity_t& median_capacity,
      const peer_profile_t::speed_t& peer_speed)
  {
    std::uint8_t added(0);

    if (peer_capacity >= median_capacity)
      {
        if (high_cap_peers.size() < tini2p::under_cast(MaxHighCapPeers))
          {
            high_cap_peers.emplace_back(peer_id);
            added = 1;
          }
        else
          {
            auto it = std::min_element(high_cap_peers.begin(), high_cap_peers.end(), [](const auto& pl, const auto& pr) {
              return pl.capacity < pr.capacity;
            });

            if (peer_capacity > it->capacity || (peer_capacity == it->capacity && peer_speed > it->speed))
              {
                high_cap_peers.erase(it);
                high_cap_peers.emplace_back(peer_id);
                added = 1;
              }
          }

        if (added)
          {
            auto& new_peer = high_cap_peers.back();
            new_peer.capacity = peer_capacity;
            new_peer.speed = peer_speed;
          }
      }

    return added;
  }

  void add_fast_peer(
      fast_peers_t& fast_peers,
      const peer_profile_t::peer_id_t& peer_id,
      const peer_profile_t::speed_t& peer_speed,
      const peer_profile_t::speed_t& median_speed,
      const peer_profile_t::capacity_t& peer_capacity)
  {
    std::uint8_t added(0);

    if (peer_speed >= median_speed)
      {
        if (fast_peers.size() < tini2p::under_cast(MaxFastPeers))
          {
            fast_peers.emplace_back(peer_id);
            added = 1;
          }
        else
          {
            auto it = std::min_element(fast_peers.begin(), fast_peers.end(), [](const auto& pl, const auto& pr) {
              return pl.speed < pr.speed;
            });

            if (peer_speed > it->speed || (peer_speed == it->speed && peer_capacity > it->capacity))
              {
                fast_peers.erase(it);
                fast_peers.emplace_back(peer_id);
                added = 1;
              }
          }

        if (added)
          {
            auto& new_peer = fast_peers.back();
            new_peer.capacity = peer_capacity;
            new_peer.speed = peer_speed;
          }
      }
  }

  void check_peer(const peer_profile_t::ident_hash_t& peer_hash, const exception::Exception& ex) const
  {
    if (peer_hash.is_zero())
      ex.throw_ex<std::logic_error>("null peer hash.");
  }

  void check_peer(const peer_id_t& peer_id, const exception::Exception& ex) const
  {
    const auto& min_peer_id = tini2p::under_cast(peer_profile_t::MinPeerID);
    const auto& max_peer_id = tini2p::under_cast(peer_profile_t::MaxPeerID);

    if (peer_id < min_peer_id || peer_id > max_peer_id)
      {
        ex.throw_ex<std::logic_error>(
            "invalid peer ID: " + std::to_string(peer_id) + ", min: " + std::to_string(min_peer_id)
            + ", max: " + std::to_string(max_peer_id));
      }
  }

  void check_peers(const peer_hashes_t& peer_hashes, const exception::Exception& ex) const
  {
    for (const auto& hash : peer_hashes)
      {
        check_peer(hash, ex);
        find_profile(hash, ex);
      }
  }

  void check_peers(const peer_ids_t& peer_ids, const exception::Exception& ex) const
  {
    for (const auto& id : peer_ids)
      find_profile(id, ex);
  }

  void check_additional_peer(const profiles_t& profiles, const peer_hash_t& peer_hash, const exception::Exception& ex)
      const
  {
    const auto prof_end = profiles.end();

    if (std::find_if(profiles.begin(), prof_end, [&peer_hash](const auto& p) { return p.ident_hash() == peer_hash; })
        != prof_end)
      ex.throw_ex<std::logic_error>("duplicate peer hash: " + tini2p::bin_to_hex(peer_hash, ex));
  }

  void check_time(const peer_profile_t::secs_t& secs, const exception::Exception& ex) const
  {
    const auto& min_time = tini2p::under_cast(peer_profile_t::MinTime);

    if (secs < min_time)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid speed sampling time: " + std::to_string(secs) + ", min time: " + std::to_string(min_time));
      }
  }

  profiles_t profiles_;
  std::shared_mutex profiles_mutex_;

  fast_peers_t fast_peers_;
  std::shared_mutex fast_mutex_;

  high_cap_peers_t high_cap_peers_;
  std::shared_mutex high_cap_mutex_;

  standard_peers_t standard_peers_;
  std::shared_mutex standard_mutex_;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_PROFILER_H_
