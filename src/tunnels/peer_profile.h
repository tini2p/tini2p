/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_PEER_PROFILE_H_
#define SRC_TUNNELS_PEER_PROFILE_H_

#include "src/crypto/rand.h"

#include "src/data/router/identity.h"

namespace tini2p
{
namespace tunnels
{
/// @class PeerProfile
/// @brief Class for profiling tunnel peers
class PeerProfile
{
 public:
  /// @brief Weights for capacity calculation
  enum struct Weight : std::uint8_t
  {
    Min = 0,  //< minimum weight to preven underflow
    Success = 1,  //< weight success the same as failure
    Fail = 1,  //< weight failure the same as success
    Drop = 2,  //< weight drops more heavily
  };

  enum
  {
    Scale = 100,  //< scaling factor for capacity
    DefaultTime = 45,  //< in seconds, default time for speed sampling
    MinTime = 1,  //< in seconds
    MinPeerID = 1,  //< minimum peer ID
    MaxPeerID = 4294967295,  //< maximum peer ID (uint32_max)
  };

  using peer_id_t = std::uint32_t;  //< Peer ID trait alias
  using ident_hash_t = data::Identity::hash_t;  //< RouterIdentity hash trait alias
  using speed_t = std::uint32_t;  //< Speed trait alias
  using bytes_t = std::uint32_t;  //< Bytes count trait alias
  using secs_t = std::uint16_t;  //< Seconds trait alias
  using capacity_t = std::uint32_t;  //< Capacity trait alias
  using rate_t = std::uint32_t;  //< Rate trait alias
  using weight_t = std::uint32_t;  //< Weight trait alias

  /// @brief Default ctor, create empty PeerProfile, for using with std containers
  PeerProfile()
      : peer_id_(0),
        ident_(),
        speed_(0),
        capacity_(0),
        build_success_(0),
        build_fail_(0),
        build_drop_(0),
        test_success_(0),
        test_fail_(0),
        test_drop_(0),
        weight_(0)
  {
  }

  /// @brief Fully initializing ctor, create a PeerProfile for peer Identity
  /// @param ident RouterIdentity hash for the peer
  explicit PeerProfile(ident_hash_t ident)
      : peer_id_(create_peer_id()),
        ident_(std::forward<ident_hash_t>(ident)),
        speed_(0),
        capacity_(0),
        build_success_(0),
        build_fail_(0),
        build_drop_(0),
        test_success_(0),
        test_fail_(0),
        test_drop_(0),
        weight_(0)
  {
    const exception::Exception ex{"Tunnel: PeerProfile", __func__};

    check_ident_hash(ident_, ex);
  }

  /// @brief Get a const reference to the peer ID
  const peer_id_t& peer_id() const noexcept
  {
    return peer_id_;
  }

  /// @brief Get a const reference to the peer's Identity hash
  const ident_hash_t& ident_hash() const noexcept
  {
    return ident_;
  }

  /// @brief Get a const reference to the peer's speed
  /// @detail Speed is an estimation of bytes/sec
  const speed_t& speed() const noexcept
  {
    return speed_;
  }

  /// @brief Update the PeerProfile's speed
  /// @param bytes Number of bytes transferred through the peer
  /// @param secs Seconds of sampling bytes transfer
  PeerProfile& speed(const bytes_t& bytes, const secs_t& secs = tini2p::under_cast(DefaultTime))
  {
    const exception::Exception ex{"PeerProfile", __func__};

    // ensure no division by zero
    check_time(secs, ex);

    speed_ = (bytes / secs) + (bytes % secs);

    return *this;
  }

  /// @brief Get a const reference to the peer's capacity
  /// @detail Capacity is an approximate calculation of the number of successful
  ///     tunnel builds and test messages.
  ///
  ///     floor((build_success + test_success) / (total_build + total_test))
  const capacity_t& capacity() const noexcept
  {
    return capacity_;
  }

  /// @brief Get a const reference to the peer's build success rate 
  const rate_t& build_success() const noexcept
  {
    return build_success_;
  }

  /// @brief Add a successful build
  PeerProfile& add_build_success()
  {
    ++build_success_;

    const auto& success_weight = tini2p::under_cast(Weight::Success);
    if (weight_ >= tini2p::under_cast(Weight::Min) + success_weight)
      weight_ -= success_weight;

    recalculate_capacity();

    return *this;
  }

  /// @brief Get a const reference to the peer's build fail rate 
  const rate_t& build_fail() const noexcept
  {
    return build_fail_;
  }

  /// @brief Add a failed build
  PeerProfile& add_build_fail() noexcept
  {
    ++build_fail_;
    weight_ += tini2p::under_cast(Weight::Fail);
    recalculate_capacity();

    return *this;
  }

  /// @brief Get a const reference to the peer's build drop rate 
  const rate_t& build_drop() const noexcept
  {
    return build_drop_;
  }

  /// @brief Add a dropped build
  PeerProfile& add_build_drop() noexcept
  {
    ++build_drop_;
    weight_ += tini2p::under_cast(Weight::Drop);
    recalculate_capacity();

    return *this;
  }

  /// @brief Get a const reference to the peer's test success rate 
  const rate_t& test_success() const noexcept
  {
    return test_success_;
  }

  /// @brief Add a successful test
  PeerProfile& add_test_success() noexcept
  {
    ++test_success_;

    const auto& success_weight = tini2p::under_cast(Weight::Success);
    if (weight_ >= tini2p::under_cast(Weight::Min) + success_weight)
      weight_ -= success_weight;

    recalculate_capacity();

    return *this;
  }

  /// @brief Get a const reference to the peer's test fail rate 
  const rate_t& test_fail() const noexcept
  {
    return test_fail_;
  }

  /// @brief Add a failed test
  PeerProfile& add_test_fail() noexcept
  {
    ++test_fail_;
    weight_ += tini2p::under_cast(Weight::Fail);
    recalculate_capacity();

    return *this;
  }

  /// @brief Get a const reference to the peer's test drop rate 
  const rate_t& test_drop() const noexcept
  {
    return test_drop_;
  }

  /// @brief Add a dropped test
  PeerProfile& add_test_drop() noexcept
  {
    ++test_drop_;
    weight_ += tini2p::under_cast(Weight::Drop);
    recalculate_capacity();

    return *this;
  }

 private:
  peer_id_t create_peer_id() const
  {
    return crypto::RandInRange(tini2p::under_cast(MinPeerID), tini2p::under_cast(MaxPeerID));
  }

  void recalculate_capacity()
  {
    // calculate scaled capacity: success / total * scale
    capacity_ = static_cast<capacity_t>(
        static_cast<float>(build_success_ + test_success_)
        / static_cast<float>(
            build_success_ + build_fail_ + build_drop_ + test_success_ + test_fail_ + test_drop_ + weight_)
        * tini2p::under_cast(Scale));
  }

  void check_ident_hash(const ident_hash_t& ident, const exception::Exception& ex) const
  {
    if (ident.is_zero())
      ex.throw_ex<std::logic_error>("null peer Identity hash.");
  }

  void check_time(const secs_t& secs, const exception::Exception& ex) const
  {
    const auto& min_time = tini2p::under_cast(MinTime);
    if (secs < min_time)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid speed sampling time: " + std::to_string(secs) + ", min time: " + std::to_string(min_time));
      }
  }

  peer_id_t peer_id_;
  ident_hash_t ident_;
  speed_t speed_;
  capacity_t capacity_;
  rate_t build_success_, build_fail_, build_drop_;
  rate_t test_success_, test_fail_, test_drop_;
  weight_t weight_;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_PEER_PROFILE_H_
