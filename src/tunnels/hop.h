/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_HOP_H_
#define SRC_TUNNELS_HOP_H_

#include "src/tunnels/processor.h"

namespace tini2p
{
namespace tunnels
{
template <class TLayer>
class Hop : public tunnels::Processor<TLayer, tunnels::HopProcessor>
{
 public:
  using layer_t = TLayer;
  using base_t = tunnels::Processor<layer_t, tunnels::HopProcessor>;
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD encryption trait alias
  using aes_layer_t = crypto::TunnelAES;  //< AES layer encryption trait alias
  using chacha_layer_t = crypto::TunnelChaCha;  //< ChaCha layer encryption trait alias
  using info_t = data::Info;  //< Router info trait alias
  using tunnel_id_t = typename base_t::tunnel_id_t;  //< Tunnel ID trait alias
  using to_hash_t = typename base_t::to_hash_t;  //< To hash trait alias
  using build_request_t = typename base_t::build_request_t;  //< BuildRequestRecord trait alias
  using build_reply_t = typename base_t::build_reply_t;  //< BuildRequestRecord trait alias
  using record_buffer_t = typename base_t::record_buffer_t;  //< Build record buffer trait alias
  using tunnel_message_t = typename base_t::tunnel_message_t;  //< Tunnel message trait alias

  Hop() : base_t() {}

  /// @brief Initializing ctor, creates an AES tunnel Hop
  /// @detail Caller responsible for setting layer encryption keys before en/decrypting tunnel messages
  /// @param id Tunnel ID (non-zero) for this hop to receive messages
  /// @param next_id Tunnel ID (non-zero) for the next hop
  /// @param info Shared pointer to the RouterInfo for this hop
  /// @param peer_key Peer ordering key for calculating XOR metric
  Hop(tunnel_id_t id,
      tunnel_id_t next_id,
      typename info_t::shared_ptr info,
      const typename base_t::peer_key_t& peer_key)
      : base_t(id, next_id, info, peer_key)
  {
  }

  /// @brief Initializing ctor, creates a ChaCha tunnel Hop
  /// @detail Caller responsible for setting layer encryption keys before en/decrypting tunnel messages
  /// @param id Tunnel ID (non-zero) for this hop to receive messages
  /// @param next_id Tunnel ID (non-zero) for the next hop
  /// @param info Shared pointer to the RouterInfo for this hop
  /// @param peer_key Peer ordering key for calculating XOR metric
  Hop(tunnel_id_t id,
      tunnel_id_t next_id,
      typename info_t::shared_ptr info,
      typename chacha_layer_t::aead_t::key_t receive_key,
      const typename base_t::peer_key_t& peer_key)
      : base_t(id, next_id, info, receive_key, peer_key)
  {
  }

  /// @brief Initializing ctor, creates a tunnel Hop for a TransitTunnel
  /// @param id Tunnel ID (non-zero) to receive messages
  /// @param next_id Tunnel ID (non-zero) for the next Hop or endpoint
  /// @param layer Layer encryption created during TransitTunnel construction
  Hop(tunnel_id_t id, tunnel_id_t next_id, std::unique_ptr<layer_t> layer) : base_t(id, next_id, std::move(layer)) {}

  // Deleted functions unused by a tunnel Hop
  decltype(auto) CreateBuildMessage() = delete;
  decltype(auto) PreprocessAESBuildMessage(typename base_t::build_message_t&) = delete;
  decltype(auto) PreprocessChaChaBuildMessage(typename base_t::build_message_t&) = delete;
  decltype(auto) HandleBuildReply(typename base_t::aes_reply_message_t&) = delete;
  decltype(auto) HandleBuildReply(typename base_t::chacha_reply_message_t&) = delete;

  decltype(auto)
  CreateTunnelMessages(const typename base_t::i2np_block_t::buffer_t&, to_hash_t, std::optional<tunnel_id_t>) = delete;

  decltype(auto) ExtractI2NPFragments(tunnel_message_t&) = delete;
  decltype(auto) IterativeDecryption(tunnel_message_t&) = delete;
  decltype(auto) reply_lease() const noexcept = delete;
  decltype(auto) hops() const noexcept = delete;
  decltype(auto) hops() noexcept = delete;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_HOP_H_
