/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_OUTBOUND_TUNNEL_H_
#define SRC_TUNNELS_OUTBOUND_TUNNEL_H_

#include "src/time.h"

#include "src/data/i2np/i2np_message.h"

#include "src/tunnels/inbound_endpoint.h"
#include "src/tunnels/inbound_gateway.h"
#include "src/tunnels/outbound_endpoint.h"
#include "src/tunnels/outbound_gateway.h"

namespace tini2p
{
namespace tunnels
{
/// @class Tunnel
/// @brief Class for creating an inbound or outbound Tunnel
/// @tparam TCreator Tunnel message creator type (InboundEndpoint, OutboundGateway)
template <class TCreator>
class Tunnel
{
 public:
  enum
  {
    ExploreExpireTime = 30,  //< in seconds
    BuiltExpireTime = 600,  //< in seconds
  };

  using creator_t = TCreator;  //< Tunnel creator trait alias
  using info_t = data::Info;  //< RouterInfo trait alias
  using lease_t = data::Lease2;  //< Lease2 trait alias
  using i2np_block_t = data::I2NPBlock;  //< I2NP block trait alias
  using create_time_t = std::uint32_t;  //< Creation time trait alias
  using expire_time_t = std::uint32_t;  //< Expiration time trait alias
  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using bytes_t = std::uint32_t;  //< Bytes transferred trait alias
  using peer_hashes_t = std::vector<info_t::identity_t::hash_t>;  //< Peer hash collection trait alias

  /// @brief Default ctor, only for the ability to use in std collection types
  Tunnel()
      : creator_ptr_(),
        create_time_(tini2p::time::now_s()),
        expire_time_(create_time_ + tini2p::under_cast(ExploreExpireTime)),
        build_msg_id_(),
        peer_hashes_(),
        bytes_()
  {}

  /// @brief Fully-initializing ctor, creates a Tunnel from RouterInfos
  /// @param creator_info Tunnel creator RouterInfo
  /// @param reply_lease Reply tunnel gateway Lease 
  /// @param hop_infos RouterInfos of hops in this Tunnel
  Tunnel(info_t::shared_ptr creator_info, lease_t reply_lease, const std::vector<info_t::shared_ptr>& hop_infos)
      : creator_ptr_(std::make_unique<creator_t>(
          std::forward<info_t::shared_ptr>(creator_info),
          std::forward<lease_t>(reply_lease),
          hop_infos)),
        create_time_(tini2p::time::now_s()),
        expire_time_(create_time_ + tini2p::under_cast(ExploreExpireTime)),
        build_msg_id_(),
        peer_hashes_(),
        bytes_()
  {
    const auto& hops = creator_ptr_->hops();

    for (const auto& hop : hops)
      peer_hashes_.emplace_back(hop.info().identity().hash());
  }

  /// @brief Move-ctor
  Tunnel(Tunnel&& oth)
      : creator_ptr_(std::move(oth.creator_ptr_)),
        create_time_(std::move(oth.create_time_)),
        expire_time_(std::move(oth.expire_time_)),
        build_msg_id_(std::move(oth.build_msg_id_)),
        peer_hashes_(std::move(oth.peer_hashes_)),
        bytes_(std::move(oth.bytes_))
  {
  }

  /// @brief Move-assignment operator
  Tunnel& operator=(Tunnel&& oth)
  {
    creator_ptr_ = std::move(oth.creator_ptr_);
    create_time_ = std::move(oth.create_time_);
    expire_time_ = std::move(oth.expire_time_);
    build_msg_id_ = std::move(oth.build_msg_id_);
    peer_hashes_ = std::move(oth.peer_hashes_);
    bytes_ = std::move(oth.bytes_);

    return *this;
  }

  /// @brief Create an I2NP block containing a VariableTunnelBuild message
  /// @return I2NP block with a VariableTunnelBuild message
  data::I2NPMessage CreateBuildMessage()
  {
    const exception::Exception ex{"Tunnel: Tunnel", __func__};

    check_creator(creator_ptr_, ex);

    auto build_msg = creator_ptr_->CreateBuildMessage();
    creator_ptr_->PreprocessBuildMessage(build_msg);

    build_msg_id_ = build_msg.records().back().next_message_id();

    return data::I2NPMessage(
        data::I2NPMessage::Type::VariableTunnelBuild, build_msg_id_, std::move(build_msg.buffer()));
  }

  /// @brief Handle a VariableTunnelBuildReply message
  /// @param i2np_msg I2NP VariableTunnelBuildReply message
  decltype(auto) HandleBuildReply(data::I2NPMessage&& i2np_msg)
  {
    using reply_message_t = typename creator_t::reply_message_t;

    const exception::Exception ex{"Tunnel: Tunnel", __func__};

    const auto& msg_type = i2np_msg.type();
    const auto& msg_id = i2np_msg.message_id();

    if (msg_type != data::I2NPHeader::Type::VariableTunnelBuildReply)
      {
        ex.throw_ex<std::logic_error>(
            "invalid message type: " + std::to_string(tini2p::under_cast(msg_type))
            + ", expected VariableTunnelBuildReply: "
            + std::to_string(tini2p::under_cast(data::I2NPHeader::Type::VariableTunnelBuildReply)));
      }

    if (msg_id != build_msg_id_)
      {
        ex.throw_ex<std::logic_error>(
            "invalid message ID: " + std::to_string(msg_id) + ", expected: " + std::to_string(build_msg_id_));
      }

    reply_message_t reply_msg(
        std::forward<crypto::SecBytes>(i2np_msg.extract_body()), reply_message_t::deserialize_mode_t::Partial);

    return creator_ptr_->HandleBuildReply(reply_msg);
  }

  /// @brief Get a const reference to the tunnel creator
  const creator_t& creator() const
  {
    const exception::Exception ex{"Tunnel: Tunnel", __func__};

    check_creator(creator_ptr_, ex);

    return *creator_ptr_;
  }

  /// @brief Get a non-const reference to the tunnel creator
  creator_t& creator()
  {
    const exception::Exception ex{"Tunnel: Tunnel", __func__};

    check_creator(creator_ptr_, ex);

    return *creator_ptr_;
  }

  /// @brief Get a const reference to the Tunnel creation time
  const create_time_t& creation_time() const noexcept
  {
    return create_time_;
  }

  /// @brief Update the creation time after receiving an accepted TunnelBuildReply message
  /// @note Updates the expiration time relative to the new creation time
  /// @return A non-const reference to this Tunnel
  Tunnel& creation_time(create_time_t time)
  {
    const exception::Exception ex{"Tunnel: Tunnel", __func__};

    check_create_time(create_time_, time, ex);
    
    create_time_ = std::forward<create_time_t>(time);
    expire_time_ = create_time_ + tini2p::under_cast(BuiltExpireTime);

    return *this;
  }

  /// @brief Get a const reference to the Tunnel expiration time
  const expire_time_t& expiration_time() const noexcept
  {
    return expire_time_;
  }

  /// @brief Get a const reference to the creator's tunnel ID
  const tunnel_id_t& tunnel_id() const
  {
    const exception::Exception ex{"Tunnel: Tunnel", __func__};

    check_creator(creator_ptr_, ex);

    return creator_ptr_->tunnel_id();
  }

  /// @brief Get a const reference to the IBGW tunnel ID
  /// @detail Only valid for inbound tunnels
  const tunnel_id_t& ibgw_tunnel_id() const
  {
    const exception::Exception ex{"Tunnel: Tunnel", __func__};

    check_creator(creator_ptr_, ex);

    return creator_ptr_->hops().front().tunnel_id();
  }

  /// @brief Get a const reference to the OBEP tunnel ID
  /// @detail Only valid for outbound tunnels
  const tunnel_id_t& obep_tunnel_id() const
  {
    const exception::Exception ex{"Tunnel: Tunnel", __func__};

    check_creator(creator_ptr_, ex);

    return creator_ptr_->hops().back().tunnel_id();
  }

  /// @brief Get a const reference to the TunnelBuildMessage/TunnelBuildReply message ID
  const message_id_t& build_message_id() const noexcept
  {
    return build_msg_id_;
  }

  /// @brief Get a const reference to the peer hashes
  const peer_hashes_t& peer_hashes() const noexcept
  {
    return peer_hashes_;
  }

  /// @brief Get a const reference to the bytes transferred through the tunnel
  const bytes_t& bytes_transferred() const noexcept
  {
    return bytes_;
  }

  /// @brief Add bytes transferred by the tunnel
  Tunnel& add_bytes_transferred(const bytes_t& bytes) noexcept
  {
    bytes_ += bytes;

    return *this;
  }

  /// @brief Clear the bytes transferred by the tunnel
  Tunnel& clear_bytes_transferred() noexcept
  {
    bytes_ = 0;

    return *this;
  }

 private:
  void check_creator(const std::unique_ptr<creator_t>& creator_ptr, const exception::Exception& ex) const
  {
    if (creator_ptr == nullptr)
      ex.throw_ex<std::runtime_error>("null tunnel creator pointer.");
  }

  void check_create_time(
      const create_time_t& create_time,
      const create_time_t& new_create_time,
      const exception::Exception& ex) const
  {
    if (new_create_time < create_time)
      {
        ex.throw_ex<std::logic_error>(
            "established creation time: " + std::to_string(new_create_time)
            + " is before the exploratory creation time: " + std::to_string(create_time));
      }

    const auto& expire_time = create_time + tini2p::under_cast(ExploreExpireTime);
    if (new_create_time > expire_time)
      {
        ex.throw_ex<std::logic_error>(
            "established creation time: " + std::to_string(new_create_time)
            + " is past the exploratory expiration: " + std::to_string(expire_time));
      }
  }

  std::unique_ptr<creator_t> creator_ptr_;
  create_time_t create_time_;
  expire_time_t expire_time_;
  message_id_t build_msg_id_;
  peer_hashes_t peer_hashes_;
  bytes_t bytes_;
};

template <class TLayer>
using InboundTunnel = Tunnel<tunnels::InboundEndpoint<TLayer>>;

template <class TLayer>
using OutboundTunnel = Tunnel<tunnels::OutboundGateway<TLayer>>;
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_TUNNEL_H_
