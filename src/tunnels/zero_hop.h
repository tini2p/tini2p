/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_ZERO_HOP_H_
#define SRC_TUNNELS_ZERO_HOP_H_

#include "src/tunnels/processor.h"

namespace tini2p
{
namespace tunnels
{
template <class TLayer>
class ZeroHop : public tunnels::Processor<TLayer, tunnels::ZeroHopProcessor>
{
 public:
  enum
  {
    Expiration = 150,  //< in seconds, zero-hop expiration time
  };

  using layer_t = TLayer;
  using base_t = tunnels::Processor<layer_t, tunnels::ZeroHopProcessor>;
  using aes_layer_t = crypto::TunnelAES;  //< AES layer encryption trait alias
  using chacha_layer_t = crypto::TunnelChaCha;  //< ChaCha layer encryption trait alias
  using info_t = data::Info;  //< RouterInfo trait alias
  using lease_t = data::Lease2;  //< Lease2 trait alias
  using tunnel_id_t = typename base_t::tunnel_id_t;  //< Tunnel ID trait alias
  using to_hash_t = typename base_t::to_hash_t;  //< To hash trait alias
  using build_request_t = typename base_t::build_request_t;  //< BuildRequestRecord trait alias
  using build_reply_t = typename base_t::build_reply_t;  //< BuildRequestRecord trait alias
  using record_buffer_t = typename base_t::record_buffer_t;  //< Build record buffer trait alias
  using tunnel_message_t = typename base_t::tunnel_message_t;  //< Tunnel message trait alias
  using creation_t = std::uint32_t;  //< Creation time trait alias
  using expiration_t = std::uint32_t;  //< Expiration time trait alias

  /// @brief Initializing ctor, creates ZeroHop tunnel processor
  /// @detail Useful for handling TunnelBuildReply messages when no directional tunnels exist
  /// @param info Shared pointer to the RouterInfo for this hop
  /// @param reply_lease Lease for the reply tunnel gateway (IBGW)
  ZeroHop(info_t::shared_ptr info, lease_t reply_lease)
      : base_t(info, std::forward<lease_t>(reply_lease), base_t::BloomFlag::Disable),
        creation_(time::now_s()),
        expiration_(creation_ + tini2p::under_cast(Expiration))
  {
  }

  /// @brief Get a const reference to the creation time
  const creation_t& creation_time() const noexcept
  {
    return creation_;
  }

  /// @brief Get a const reference to the expiration time
  const expiration_t& expiration_time() const noexcept
  {
    return expiration_;
  }

  // Delete unused functions
  decltype(auto) CreateBuildMessage() = delete;
  decltype(auto) PreprocessAESBuildMessage(typename base_t::build_message_t&) = delete;
  decltype(auto) PreprocessChaChaBuildMessage(typename base_t::build_message_t&) = delete;

  decltype(auto)
  CreateTunnelMessages(const typename base_t::i2np_block_t::buffer_t&, to_hash_t, std::optional<tunnel_id_t>) = delete;

  decltype(auto) ExtractI2NPFragments(tunnel_message_t&) = delete;

  template <class T>
  decltype(auto) EncryptRecord(T&) = delete;

  template <class T>
  decltype(auto) DecryptRecord(T&) = delete;

  template <class T>
  decltype(auto) EncryptRequestRecord(T&) = delete;

  template <class T>
  decltype(auto) EncryptReplyRecord(T&) = delete;

  template <class T>
  decltype(auto) EncryptRecord(T&, const std::uint8_t&, const std::uint8_t&) = delete;

  template <class T>
  void DecryptRequestRecord(T& record) = delete;

  template <class T>
  void DecryptReplyRecord(T&) = delete;

  template <class T>
  decltype(auto) DecryptRecord(T&, const std::uint8_t&, const std::uint8_t&) = delete;

  decltype(auto) Encrypt(tunnel_message_t&) = delete;
  decltype(auto) Decrypt(tunnel_message_t&) = delete;
  decltype(auto) EncryptAEAD(tunnel_message_t&) = delete;
  decltype(auto) DecryptAEAD(tunnel_message_t&) = delete;

  decltype(auto) IterativeDecryption(tunnel_message_t&) = delete;
  decltype(auto) reply_lease() const noexcept = delete;
  decltype(auto) hops() const noexcept = delete;
  decltype(auto) hops() noexcept = delete;

 private:
  creation_t creation_;
  expiration_t expiration_;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_ZERO_HOP_H_
