/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_POOL_MANAGER_H_
#define SRC_TUNNELS_POOL_MANAGER_H_

#include <asio.hpp>

#include "src/tunnels/pool.h"
#include "src/tunnels/transit_tunnel.h"
#include "src/tunnels/zero_hop.h"

namespace tini2p
{
namespace tunnels
{
/// @class Pool
/// @brief Class for managing inbound, outbound, and transit tunnels
template <class TLayer>
class PoolManager
{
 public:
  // TODO(tini2p): change adjustable limits to config options
  enum
  {
    CheckTransitTimeout = 10,  //< in seconds, check for and clear expired TransitTunnels
    MinTestTimeout = 5,  //< in seconds, minimum time to send test message
    MaxTestTimeout = 30,  //< in seconds, maximum time to send test message
    TestExpiration = 30,  //< in seconds, test message expiration
  };

  using info_t = data::Info;  //< RouterInfo trait alias
  using lease_t = data::Lease2;  //< Lease2 trait alias
  using i2np_block_t = data::I2NPBlock;  //< I2NP block trait alias
  using i2np_gateway_t = data::TunnelGateway;  //< I2NP TunnelGateway trait alias
  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using message_id_t = std::uint32_t; //< Message ID trait alias
  using layer_t = TLayer;  //< Layer encryption trait alias
  using sym_t = typename layer_t::sym_t;  //< Symmetric encryption trait alias
  using inbound_pool_t = tunnels::Pool<tunnels::InboundTunnel<layer_t>>;  //< Inbound tunnel pool trait alias
  using outbound_pool_t = tunnels::Pool<tunnels::OutboundTunnel<layer_t>>;  //< Outbound tunnel pool trait alias
  using zero_hop_t = tunnels::ZeroHop<layer_t>;  //< Zero-hop tunnel trait alias
  using transit_tunnel_t = tunnels::TransitTunnel<layer_t>;  //< Transit tunnel trait alias
  using transit_tunnels_t = std::vector<transit_tunnel_t>;  //< Transit tunnel collection trait alias
  using build_request_t = data::BuildRequestRecord;  //< BuildRequestRecord trait alias
  using build_reply_t = data::BuildResponseRecord<sym_t>;  //< BuildResponseRecord trait alias
  using build_message_t = data::TunnelBuildMessage;  //< TunnelBuildMessage trait alias
  using reply_message_t = data::TunnelBuildReply<sym_t>;  //< TunnelBuildReply trait alias
  using tunnel_message_t = data::TunnelMessage<layer_t>;  //< TunnelMessage trait alias
  using bloom_filter_t = crypto::BloomFilter;  //< Bloom filter trait alias
  using io_context_t = asio::io_context;  //< IO context trait alias
  using timer_t = asio::system_timer;  //< Timer trait alias
  using error_c = asio::error_code;  //< ASIO error code trait alias

  /// @brief Fully-initializing ctor, creates a PoolManager from a local RouterInfo
  PoolManager(info_t::shared_ptr info)
      : info_(info),
        inbound_(),
        outbound_(),
        transit_tunnels_(),
        transit_ctx_(),
        transit_timer_(),
        transit_timer_thread_()
  {
    const exception::Exception ex{"Tunnel: PoolManager", __func__};

    check_info(info_, ex);

    RunTransitTimer();  // Run timer to clear expired TransitTunnels
  }

  /// @brief Dtor, stop all timers
  ~PoolManager()
  {
    StopTransitTimer();
  }

  /// @brief Handle an incoming request to participate in a tunnel
  /// @detail Checks request creator public key against Bloom filter,
  ///    and rejects the request if the key is a duplicate
  /// @param message TunnelBuildMessage containing request to participate
  /// @return Next hop's tunnel ID and Identity hash
  PoolManager& HandleBuildMessage(typename layer_t::key_info_t&& key_info, build_request_t& record)
  {
    const exception::Exception ex{"Tunnel: PoolManager", __func__};

    check_info(info_, ex);

    // Accept the tunnel, and create a local TransitTunnel
    std::scoped_lock sgd(transit_mutex_);
    transit_tunnels_.emplace_back(std::make_unique<layer_t>(std::move(key_info)), record);

    return *this;
  }

  /// @brief Handle a VariableTunnelBuildReply message for an building tunnel
  /// @param i2np_block I2NP block containing a VariableTunnelBuildReply message
  PoolManager& HandleBuildReply(data::I2NPMessage i2np_msg)
  {
    const exception::Exception ex{"Tunnel: PoolManager", __func__};

    const auto& msg_id = i2np_msg.message_id();

    // Ensure correct message type if received for an inbound tunnel or on a zero-hop tunnel
    if (i2np_msg.type() == data::I2NPHeader::Type::VariableTunnelBuild)
      {
        tini2p::BytesWriter<i2np_block_t::buffer_t> writer(i2np_msg.buffer());

        writer.write_bytes(data::I2NPHeader::Type::VariableTunnelBuildReply);
        i2np_msg.type(data::I2NPHeader::Type::VariableTunnelBuildReply);
      }

    if (inbound_.has_building_tunnel(msg_id) == 1)
      inbound_.HandleBuildReply(std::forward<data::I2NPMessage>(i2np_msg));
    else if (outbound_.has_building_tunnel(msg_id) == 1)
      outbound_.HandleBuildReply(std::forward<data::I2NPMessage>(i2np_msg));
    else
      ex.throw_ex<std::logic_error>("no building tunnels found for build message ID: " + std::to_string(msg_id));

    return *this;
  }

  /// @brief Add an InboundTunnel to the InboundPool
  /// @param hop_infos Hop RouterInfos for tunnel participants
  decltype(auto) AddInboundTunnel(const std::vector<info_t::shared_ptr>& hop_infos)
  {
    return inbound_.AddTunnel(info_, lease_t(), hop_infos);
  }

  /// @brief Add an OutboundTunnel to the OutboundPool
  /// @param reply_lease Lease of the reply InboundGateway
  /// @param hop_infos Hop RouterInfos for tunnel participants
  decltype(auto) AddOutboundTunnel(lease_t reply_lease, const std::vector<info_t::shared_ptr>& hop_infos)
  {
    return outbound_.AddTunnel(info_, std::forward<lease_t>(reply_lease), hop_infos);
  }

  /// @brief Create a test message for a random tunnel in the inbound pool
  /// @return Pair of test information, and an I2NP message to send to the InboundGateway
  decltype(auto) CreateInboundTestMessage()
  {
    auto test = inbound_.CreateTestMessage();
    i2np_gateway_t gw_msg(test.first.to_tunnel_id, std::move(test.second.buffer()));

    // Convert test Data message to TunnelGateway message to deliver to the IBGW
    return std::make_pair<decltype(test.first), data::I2NPMessage>(
        std::move(test.first),
        data::I2NPMessage(data::I2NPHeader::Type::TunnelGateway, {}, std::move(gw_msg.buffer())));
  }

  /// @brief Create a test message for a random tunnel in the outbound pool
  /// @return Pair of test information, and tunnel message(s) to send to the first outbound Hop
  decltype(auto) CreateOutboundTestMessage()
  {
    auto test = outbound_.CreateZeroHopTunnels(info_).CreateTestMessage();

    // Convert to tunnel messages, and encrypt for the outbound tunnel
    auto messages = outbound_.HandleI2NPMessage(
        test.first.creator_tunnel_id, test.second.buffer(), test.first.to_hash, test.first.to_tunnel_id);

    return std::make_pair<decltype(test.first), decltype(messages.second)>(std::move(test.first), std::move(messages.second));
  }

  /// @brief Handle inbound TunnelMessage
  /// @return Collection of completed I2NP block messages
  decltype(auto) HandleTunnelMessage(data::I2NPMessage& message)
  {
    tunnel_message_t msg(message.extract_body(), tunnel_message_t::deserialize_mode_t::Partial);

    const auto& tunnel_id = msg.hop_tunnel_id();

    return inbound_.has_tunnel(tunnel_id) ? inbound_.HandleTunnelMessage(msg)
                                          : find_transit_tunnel(tunnel_id).HandleTunnelMessage(msg);
  }

  /// @brief Handle inbound I2NP message block
  /// @return Collection of encrypted tunnel messages (empty if I2NP test message)
  decltype(auto) HandleI2NPMessage(const data::I2NPMessage& message)
  {
    const exception::Exception ex{"Tunnel: PoolManager", __func__};

    info_t::identity_t::hash_t next_hash;
    std::vector<tunnel_message_t> ret_msgs;

    const auto& msg_type = message.type();

    if (msg_type == data::I2NPHeader::Type::Data || msg_type == data::I2NPHeader::Type::TunnelGateway)
      {
        std::optional<i2np_gateway_t> gw_msg;

        if (msg_type == i2np_block_t::msg_type_t::TunnelGateway)
          gw_msg.emplace(message.message_buffer());

        const auto& data_block = msg_type == data::I2NPHeader::Type::Data
                                     ? message
                                     : data::I2NPMessage(gw_msg->extract_header(), gw_msg->extract_body());

        const auto& msg_id = data_block.message_id();

        if (inbound_.has_test(msg_id))
          inbound_.HandleTestReply(data_block);
        else if (outbound_.has_test(msg_id))
          outbound_.HandleTestReply(data_block);
        else
          {
            if (msg_type != data::I2NPHeader::Type::TunnelGateway)
              ex.throw_ex<std::logic_error>("only TunnelGateway I2NP messages are valid for TransitTunnels.");

            // Find IBGW transit tunnel, and encrypt I2NP message
            auto& tunnel = find_transit_tunnel(gw_msg->tunnel_id());
            next_hash = tunnel.next_ident();
            ret_msgs = tunnel.HandleI2NPMessage(data_block.buffer());
          }
      }
    else
      ex.throw_ex<std::invalid_argument>("invalid I2NP message type, expected Data or TunnelGateway message.");

    return std::make_pair(std::move(next_hash), std::move(ret_msgs));
  }

  /// @brief Get a const reference to the InboundPool
  const inbound_pool_t& inbound_pool() const noexcept
  {
    return inbound_;
  }

  /// @brief Get a non-const reference to the InboundPool
  inbound_pool_t& inbound_pool() noexcept
  {
    return inbound_;
  }

  /// @brief Get a const reference to the OutboundPool
  const outbound_pool_t& outbound_pool() const noexcept
  {
    return outbound_;
  }

  /// @brief Get a non-const reference to the OutboundPool
  outbound_pool_t& outbound_pool() noexcept
  {
    return outbound_;
  }

  /// @brief Get inbound gateway hashes and tunnel IDs
  /// @detail The minimum of the requested number of gateways and the number of established tunnels is returned
  /// @param num Requested number of gateways to retrieve
  std::vector<std::pair<info_t::identity_t::hash_t, tunnel_id_t>> get_inbound_gateways(const std::uint8_t& num = 3)
  {
    return inbound_.get_gateway_hashes_and_ids(num);
  }

  /// @brief Find a TransitTunnel with the given tunnel ID
  /// @throws Logic error if no tunnel found
  /// @return Non-const reference to the found TransitTunnel
  transit_tunnel_t& find_transit_tunnel(const tunnel_id_t& id)
  {
    const exception::Exception ex{"Tunnel: PoolManager", __func__};

    std::shared_lock tfs(transit_mutex_, std::defer_lock);
    std::scoped_lock sgd(tfs);

    const auto transit_end = transit_tunnels_.end();
    auto it = std::find_if(transit_tunnels_.begin(), transit_end, [&id](const auto& t) { return t.tunnel_id() == id; });

    if (it == transit_end)
      ex.throw_ex<std::logic_error>("no TransitTunnel found with ID: " + std::to_string(id));

    return *it;
  }

  /// @brief Get whether the pool manager has a tunnel with the given ID
  std::uint8_t has_tunnel(const tunnel_id_t& id)
  {
    std::shared_lock tfs(transit_mutex_, std::defer_lock);
    std::scoped_lock sgd(tfs);

    const auto transit_end = transit_tunnels_.end();
    auto it = std::find_if(transit_tunnels_.begin(), transit_end, [&id](const auto& t) { return t.tunnel_id() == id; });

    return static_cast<std::uint8_t>(it != transit_end || inbound_.has_tunnel(id) || outbound_.has_tunnel(id));
  }

  /// @brief Get whether the manager has a building tunnel with the given message ID
  std::uint8_t has_building_tunnel(const message_id_t& id)
  {
    return static_cast<std::uint8_t>(inbound_.has_building_tunnel(id) || outbound_.has_building_tunnel(id));
  }

  /// @brief Get whether the manager has a pending test message with the given message ID
  std::uint8_t has_test(const message_id_t& id)
  {
    return static_cast<std::uint8_t>(inbound_.has_test(id) || outbound_.has_test(id));
  }

 private:
  void RunTransitTimer()
  {
    transit_timer_ = std::make_unique<timer_t>(transit_ctx_, std::chrono::minutes(tini2p::under_cast(CheckTransitTimeout)));
    transit_timer_->async_wait([this](const auto& ec) { HandleExpiredTransit(ec); });

    transit_timer_thread_ = std::make_unique<std::thread>([this](){ transit_ctx_.run(); });
  }

  void StopTransitTimer()
  {
    const exception::Exception ex{"Tunnel: TransitTunnel", __func__};

    try
      {
        transit_ctx_.stop();

        if (transit_timer_ != nullptr)
          {
            transit_timer_->cancel();
            transit_timer_.reset();
          }

        if (transit_timer_thread_ != nullptr)
          {
            transit_timer_thread_->join();
            transit_timer_thread_.reset();
          }
      }
    catch (const std::exception& e)
      {
        ex.throw_ex<std::runtime_error>("timer cancellation error: " + std::string(e.what()));
      }
  }

  void HandleExpiredTransit(const error_c& ec)
  {
    const exception::Exception ex{"Tunnel: TransitTunnel", __func__};

    check_timer(transit_timer_, ex);

    if (ec != asio::error::operation_aborted)
      {
        std::scoped_lock sgd(transit_mutex_);

        const auto transit_begin = transit_tunnels_.begin();
        const auto transit_end = transit_tunnels_.end();

        // Remove expired TransitTunnels
        transit_tunnels_.erase(
            std::remove_if(
                transit_begin, transit_end, [](const auto& t) { return t.expiration() <= tini2p::time::now_s(); }),
            transit_end);

        transit_timer_->expires_after(std::chrono::minutes(tini2p::under_cast(CheckTransitTimeout)));
        transit_timer_->async_wait([this](const auto& ec) { HandleExpiredTransit(ec); });
      }
    else
      StopTransitTimer();
  }

  void check_info(const info_t::shared_ptr& info_ptr, const exception::Exception& ex) const
  {
    if (info_ptr == nullptr)
      ex.throw_ex<std::runtime_error>("null RouterInfo.");

    if (!info_ptr->Verify())
      ex.throw_ex<std::logic_error>("RouterInfo failed verification.");
  }

  void check_timer(const std::unique_ptr<timer_t>& timer, const exception::Exception& ex) const
  {
    if (timer == nullptr)
      ex.throw_ex<std::runtime_error>("null timer.");
  }

  info_t::shared_ptr info_;

  inbound_pool_t inbound_;
  outbound_pool_t outbound_;

  transit_tunnels_t transit_tunnels_;
  std::shared_mutex transit_mutex_;

  io_context_t transit_ctx_;
  std::unique_ptr<timer_t> transit_timer_;
  std::unique_ptr<std::thread> transit_timer_thread_;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_POOL_MANAGER_H_
