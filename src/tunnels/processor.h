/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_PROCESSOR_H_
#define SRC_TUNNELS_PROCESSOR_H_

#include <list>

#include "src/crypto/bloom_filter.h"
#include "src/crypto/blake.h"

#include "src/data/blocks/i2np.h"

#include "src/data/i2np/build_request_record.h"
#include "src/data/i2np/build_response_record.h"
#include "src/data/i2np/i2np_message.h"
#include "src/data/i2np/tunnel_build_message.h"
#include "src/data/i2np/tunnel_build_reply.h"
#include "src/data/i2np/tunnel_gateway.h"
#include "src/data/i2np/tunnel_message.h"

#include "src/data/router/info.h"
#include "src/data/router/lease.h"

#include "src/tunnels/fragmented_message.h"

namespace tini2p
{
namespace tunnels
{
/// @class ZeroHopProcessor
/// @brief Zero-hop tunnel processor specialization
class ZeroHopProcessor
{
};

/// @class HopProcessor
/// @brief Hop tunnel processor specialization
class HopProcessor
{
};

/// @class OutboundEndpointProcessor
/// @brief OutboundEndpoint tunnel processor specialization
class OutboundEndpointProcessor
{
};

/// @class OutboundGateway Processor
/// @brief OutboundGateway tunnel processor specialization
class OutboundGatewayProcessor
{
};

/// @class InboundGatewayProcessor
/// @brief InboundGateway tunnel processor specialization
class InboundGatewayProcessor
{
};

/// @class InboundEndpointProcessor
/// @brief InboundEndpoint tunnel processor specialization
class InboundEndpointProcessor
{
};

#ifdef TINI2P_TESTS
/// @brief Flags for creating test TunnelBuildReply messages
enum struct CreateReplyType : std::uint8_t
{
  AllReject,
  AllAccept,
  OneReject,
  OneAccept,
};
#endif

/// @class Processor
/// @brief Base class for tunnel participant message processors
/// @tparam TLayer Layer encryption type
/// @tparam TProcessor Message processor type (Hop, OutboundEndpoint, InboundGateway)
template <
    class TLayer,
    class TProcessor,
    typename = std::enable_if_t<
        std::is_same<TLayer, crypto::TunnelAES>::value || std::is_same<TLayer, crypto::TunnelChaCha>::value>>
class Processor
{
 public:
  enum : std::uint32_t
  {
    MinTunnelID = 1,
    MaxTunnelID = 4294967295,
#if defined(TINI2P_TESTS) and !defined(TINI2P_PERF_TESTS)
    ExpectedMaxI2NP = 600,  //< Test maximum I2NP messages (1 msgs/sec for 10 min)
    FalsePositiveRate = 100,  //< Allow 1-in-100 false-positives
#else
    ExpectedMaxI2NP = 614400,  //< Expected maximum I2NP messages (1024 msgs/sec for 10 min), adjust as necessary
    FalsePositiveRate = 1024,  //< Allow 1-in-1024 false-positives, adjust as necessary
#endif
    MinHops = 1,
    MaxHops = 8,
  };

  /// @brief Bloom filter enable flags
  enum struct BloomFlag : bool
  {
    Disable,
    Enable,
  };

  using layer_t = TLayer;  //< Layer encryption trait alias
  using sym_t = typename layer_t::sym_t;  //< Symmetric encryption trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD encryption trait alias
  using processor_t = TProcessor;  //< Message processor trait alias
  using aes_layer_t = crypto::TunnelAES;  //< AES layer encryption trait alias
  using chacha_layer_t = crypto::TunnelChaCha;  //< ChaCha layer encryption trait alias
  using zero_processor_t = ZeroHopProcessor;  //< Zero-hop processor trait alias
  using hop_processor_t = HopProcessor;  //< Hop processor trait alias
  using obgw_processor_t = OutboundGatewayProcessor;  //< OutboundGateway processor trait alias
  using obep_processor_t = OutboundEndpointProcessor;  //< OutboundEndpoint processor trait alias
  using ibgw_processor_t = InboundGatewayProcessor;  //< InboundGateway processor trait alias
  using ibep_processor_t = InboundEndpointProcessor;  //< InboundEndpoint processor trait alias
  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using info_t = data::Info;  //< Router info trait alias
  using lease_t = data::Lease2;  //< Lease2 trait alias
  using i2np_block_t = data::I2NPBlock;  //< I2NP block trait alias
  using build_request_t = data::BuildRequestRecord;  //< BuildRequestRecord trait alias
  using build_reply_t = data::BuildResponseRecord<sym_t>;  //< BuildRequestRecord trait alias
  using record_buffer_t = typename build_request_t::buffer_t;  //< Build record buffer trait alias
  using build_message_t = data::TunnelBuildMessage;  //< Tunnel build message trait alias
  using reply_message_t = data::TunnelBuildReply<sym_t>;  //< TunnelBuildReply message trait alias
  using aes_reply_message_t = data::TunnelBuildReply<aes_layer_t::sym_t>;  //< AES TunnelBuildReply message trait alias
  using chacha_reply_message_t =
      data::TunnelBuildReply<chacha_layer_t::sym_t>;  //< ChaCha TunnelBuildReply message trait alias
  using to_hash_t = data::FirstDelivery::to_hash_t;  //< To hash trait alias
  using key_info_t = crypto::BuildKeyInfo<sym_t>;  //< Build key information trait alias
  using tunnel_message_t = data::TunnelMessage<layer_t>;  //< Tunnel message trait alias
  using frag_message_t = tunnels::FragmentedMessage;  //< Fragmented tunnel message trait alias
  using messages_t = std::unordered_map<message_id_t, frag_message_t>;  //< I2NP messages collection trait alias
  using extracted_i2np_t =
      std::vector<std::pair<std::optional<to_hash_t>, data::I2NPMessage>>;  //< Extracted I2NP trait alias
  using bloom_filter_t = crypto::BloomFilter;  //< Bloom filter trait alias
  using hop_t = Processor<layer_t, HopProcessor>;  //< Tunnel hop trait alias
  using hops_t = std::list<hop_t>;  //< Tunnel hops container trait alias
  using blake_t = crypto::Blake2b;  //< Blake2b trait alias
  using peer_key_t = blake_t::key_t;  //< Peer key trait alias
  using peer_metric_t = blake_t::key_t;  //< Peer XOR metric trait alias

  /// @brief Default ctor, for use with std containers
  Processor() : id_(), next_id_(), info_(), reply_lease_(), peer_key_(), peer_metric_(), layer_() {}

  /// @brief Tunnel creator ctor, creates an ZeroHop tunnel message processor
  /// @tparam Enable for OutboundGateway and InboundEndpoint processors
  /// @param info RouterInfo for the tunnel creator
  /// @param reply_lease Lease for the reply tunnel gateway
  /// @param bloom Flag for enabling a Bloom filter
  template <typename P = processor_t, typename = std::enable_if_t<std::is_same<P, zero_processor_t>::value>>
  Processor(info_t::shared_ptr info, lease_t reply_lease, const BloomFlag& bloom)
      : id_(create_tunnel_id()),
        next_id_(create_tunnel_id()),
        info_(info),
        reply_lease_(std::forward<lease_t>(reply_lease.check_expiration())),
        peer_key_(),
        peer_metric_(),
        layer_()
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_info(info_, ex);

    if (bloom == BloomFlag::Enable)
      bloom_.emplace(tini2p::under_cast(ExpectedMaxI2NP), tini2p::under_cast(FalsePositiveRate));

    const auto& ident = info_->identity();
    layer_ = std::make_unique<layer_t>(typename layer_t::key_info_t(ident.crypto().pubkey, ident.hash()));
  }

  /// @brief Tunnel creator ctor, creates an OBGW or IBEP tunnel message processor
  /// @tparam Enable for OutboundGateway and InboundEndpoint processors
  /// @param info RouterInfo for the tunnel creator
  /// @param reply_lease RouterInfo for the reply tunnel gateway
  /// @param infos RouterInfos for the hops in this tunnel
  /// @param bloom Flag for enabling a Bloom filter
  template <typename P = processor_t,
      typename = std::enable_if_t<
          std::is_same<P, obgw_processor_t>::value || std::is_same<P, ibep_processor_t>::value>>
  Processor(
      info_t::shared_ptr info,
      lease_t reply_lease,
      const std::vector<info_t::shared_ptr>& infos,
      const BloomFlag& bloom)
      : id_(create_tunnel_id()),
        next_id_(create_tunnel_id()),
        info_(info),
        reply_lease_(std::forward<lease_t>(reply_lease.check_expiration())),
        peer_key_(blake_t::create_key()),
        peer_metric_(),
        layer_()
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_info(info_, ex);
    check_infos(infos, ex);
    check_creator_hops(*info_, infos, ex);

    if (bloom == BloomFlag::Enable)
      bloom_.emplace(tini2p::under_cast(ExpectedMaxI2NP), tini2p::under_cast(FalsePositiveRate));

    const auto& ident = info_->identity();
    layer_ = std::make_unique<layer_t>(typename layer_t::key_info_t(ident.crypto().pubkey, ident.hash()));

    // create tunnel hops for each given RouterInfo
    const auto& infos_len = infos.size();
    for (std::uint8_t i = 0; i < infos_len; ++i)  // create a tunnel hop for the RouterInfo
      hops_.emplace_back(create_tunnel_id(), create_tunnel_id(), infos.at(i), *peer_key_);

    // Sort the Hops by peer key metric, and remove duplicates (if any)
    hops_.sort();
    hops_.unique();

    auto hops_begin = hops_.begin();
    const auto last_hop = --hops_.end();

    // Set hop next ID and receive key after sorting
    for (auto hop_it = hops_begin; hop_it != hops_.end(); ++hop_it)
      {
        if (std::is_same<processor_t, ibep_processor_t>::value)
          {
            if (hop_it == hops_begin)  // Set the reply lease with the InboundGateway's info
              reply_lease_.tunnel_gateway(hop_it->info().identity().hash()).tunnel_id(hop_it->tunnel_id());

            if (hop_it == last_hop)
              hop_it->next_tunnel_id(id_);  // set last hop's next ID to the IBEP ID
            else
              {
                auto next_hop = hop_it;
                hop_it->next_tunnel_id((++next_hop)->tunnel_id());
              }

            if (std::is_same<layer_t, chacha_layer_t>::value)
              {
                if (hop_it == last_hop)  // send IBEP receive key to last hop's send key
                  layer_->key_info().receive_key(hop_it->key_info().send_key());

                if (hop_it != hops_begin)
                  {
                    auto prev_hop = hop_it;
                    hop_it->key_info().receive_key((--prev_hop)->key_info().send_key());
                  }
              }
          }
        else if (std::is_same<processor_t, obgw_processor_t>::value)
          {
            if (hop_it == hops_begin)
              hop_it->tunnel_id(next_id_);

            if (hop_it == last_hop)
              hop_it->next_tunnel_id(reply_lease_.tunnel_id());  // set last hop's next ID to reply IBGW iD
            else
              {
                auto next_hop = hop_it;
                hop_it->next_tunnel_id((++next_hop)->tunnel_id());
              }

            if (std::is_same<layer_t, chacha_layer_t>::value)
              {
                if (hop_it == hops_begin)
                  hop_it->key_info().receive_key(layer_->key_info().send_key());
                else
                  {
                    auto prev_hop = hop_it;
                    hop_it->key_info().receive_key((--prev_hop)->key_info().send_key());
                  }
              }
          }
        else
          ex.throw_ex<std::logic_error>("invalid tunnel processor.");
      }
  }

  /// @brief Gateway ctor, creates an InboundGateway message processor
  /// @param id Tunnel ID (non-zero) for this hop to receive messages
  /// @param next_id Tunnel ID (non-zero) for the next hop
  /// @param info Shared pointer to the RouterInfo for this hop
  /// @param bloom Flag to enable or disable the Bloom filter
  template <typename P = processor_t,
      typename = std::enable_if_t<
          std::is_same<P, ibgw_processor_t>::value || std::is_same<P, obep_processor_t>::value>>
  Processor(tunnel_id_t id, tunnel_id_t next_id, info_t::shared_ptr info, const BloomFlag& bloom)
      : id_(id), next_id_(next_id), info_(info), peer_key_(), peer_metric_(), messages_()
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_info(info_, ex);
    check_ids(id_, next_id_, ex);

    if (bloom == BloomFlag::Enable)
      bloom_.emplace(tini2p::under_cast(ExpectedMaxI2NP), tini2p::under_cast(FalsePositiveRate));

    const auto& ident = info_->identity();
    layer_ = std::make_unique<layer_t>(typename layer_t::key_info_t(ident.crypto().pubkey, ident.hash()));
  }

  /// @brief Gateway ctor, creates a ChaCha OutboundEndpoint message processor
  /// @param id Tunnel ID (non-zero) for this hop to receive messages
  /// @param next_id Tunnel ID (non-zero) for the next hop
  /// @param info Shared pointer to the RouterInfo for this hop
  /// @param bloom Flag to enable or disable the Bloom filter
  template <typename P = processor_t, typename = std::enable_if_t<std::is_same<P, obep_processor_t>::value>>
  Processor(
      tunnel_id_t id,
      tunnel_id_t next_id,
      info_t::shared_ptr info,
      chacha_layer_t::aead_t::key_t receive_key,
      const BloomFlag& bloom)
      : id_(id), next_id_(next_id), info_(info), peer_key_(), peer_metric_(), messages_()
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_info(info_, ex);
    check_ids(id_, next_id_, ex);

    if (bloom == BloomFlag::Enable)
      bloom_.emplace(tini2p::under_cast(ExpectedMaxI2NP), tini2p::under_cast(FalsePositiveRate));

    const auto& ident = info_->identity();
    layer_ = std::make_unique<layer_t>(typename layer_t::key_info_t(
        ident.crypto().pubkey, ident.hash(), std::forward<chacha_layer_t::aead_t::key_t>(receive_key)));
  }

  /// @brief Initializing ctor, creates an AES tunnel Hop
  /// @detail Caller responsible for setting layer encryption keys before en/decrypting tunnel messages
  /// @param id Tunnel ID (non-zero) for this hop to receive messages
  /// @param next_id Tunnel ID (non-zero) for the next hop
  /// @param info Shared pointer to the RouterInfo for this hop
  template <typename P = processor_t, typename = std::enable_if_t<std::is_same<P, hop_processor_t>::value>>
  Processor(tunnel_id_t id, tunnel_id_t next_id, info_t::shared_ptr info, const peer_key_t& peer_key)
      : id_(id), next_id_(next_id), info_(info), peer_key_(), peer_metric_(CalculatePeerMetric(peer_key)), messages_()
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_ids(id_, next_id_, ex);
    check_info(info_, ex);

    const auto& ident = info_->identity();
    layer_ = std::make_unique<layer_t>(typename layer_t::key_info_t(ident.crypto().pubkey, ident.hash()));
  }

  /// @brief Initializing ctor, creates a ChaCha tunnel Hop
  /// @detail Caller responsible for setting layer encryption keys before en/decrypting tunnel messages
  /// @param id Tunnel ID (non-zero) for this hop to receive messages
  /// @param next_id Tunnel ID (non-zero) for the next hop
  /// @param info Shared pointer to the RouterInfo for this hop
  template <typename P = processor_t, typename = std::enable_if_t<std::is_same<P, hop_processor_t>::value>>
  Processor(
      tunnel_id_t id,
      tunnel_id_t next_id,
      info_t::shared_ptr info,
      chacha_layer_t::aead_t::key_t receive_key,
      const peer_key_t& peer_key)
      : id_(id), next_id_(next_id), info_(info), peer_key_(), peer_metric_(CalculatePeerMetric(peer_key)), messages_()
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    if (std::is_same<layer_t, aes_layer_t>::value)
      ex.throw_ex<std::logic_error>("invalid layer encryption, use the AES ctor.");

    check_ids(id_, next_id_, ex);
    check_info(info_, ex);

    const auto& ident = info_->identity();
    layer_ = std::make_unique<layer_t>(typename layer_t::key_info_t(
        ident.crypto().pubkey, ident.hash(), std::forward<chacha_layer_t::aead_t::key_t>(receive_key)));
  }

  /// @brief Initializing ctor, creates a tunnel Hop, InboundGateway, or OutboundEndpoint
  /// @detail Useful for creating a TransitTunnel processor from a BuildRequestRecord
  /// @param id Tunnel ID (non-zero) for this hop to receive messages
  /// @param next_id Tunnel ID (non-zero) for the next hop
  /// @param layer Unique pointer to TunnelLayer encryption
  Processor(tunnel_id_t id, tunnel_id_t next_id, std::unique_ptr<layer_t> layer) : id_(id), next_id_(next_id), messages_()
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_ids(id_, next_id_, ex);

    layer_ = std::move(layer);

    bloom_.emplace(tini2p::under_cast(ExpectedMaxI2NP), tini2p::under_cast(FalsePositiveRate));
  }

  /// @brief Move-ctor
  Processor(Processor&& oth)
      : id_(std::move(oth.id_)),
        next_id_(std::move(oth.next_id_)),
        info_(std::move(oth.info_)),
        peer_key_(std::move(oth.peer_key_)),
        peer_metric_(std::move(oth.peer_metric_)),
        layer_(std::move(oth.layer_)),
        messages_(std::move(oth.messages_)),
        bloom_(std::move(oth.bloom_)),
        hops_(std::move(oth.hops_)),
        shuffle_build_order_(std::move(oth.shuffle_build_order_))
  {
  }

  /// @brief Forwarding assignment operator
  Processor& operator=(Processor&& oth)
  {
    id_ = std::forward<tunnel_id_t>(oth.id_);
    next_id_ = std::forward<tunnel_id_t>(oth.next_id_);
    info_ = std::forward<info_t::shared_ptr>(oth.info_);

    layer_ = std::move(oth.layer_);

    if (oth.bloom_ != std::nullopt)
      bloom_ = std::forward<bloom_filter_t>(*oth.bloom_);

    peer_key_ = std::forward<std::optional<peer_key_t>>(oth.peer_key_);
    peer_metric_ = std::forward<std::optional<peer_metric_t>>(oth.peer_metric_);

    std::scoped_lock sgd(messages_mutex_);
    messages_ = std::forward<messages_t>(oth.messages_);

    std::scoped_lock shd(hops_mutex_);
    hops_ = std::forward<hops_t>(oth.hops_);

    shuffle_build_order_ = std::forward<std::vector<std::uint8_t>>(oth.shuffle_build_order_);

    return *this;
  }

  /// @brief Create BuildRequestRecords for each tunnel hop
  build_message_t CreateBuildMessage()
  {
    using record_params_t = typename build_request_t::params_t;
    using record_flags_t = typename build_request_t::flags_t;

    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_info(info_, ex);

    typename build_message_t::records_t reqs;

    // Create a random message ID for the TunnelBuild and TunnelBuildReply messages
    const auto message_id = i2np_block_t::create_message_id();

    std::scoped_lock sgd(hops_mutex_);
    const auto& creator_info = info();
    const auto& re_lease = reply_lease();
    const auto& hops_len = hops_.size();
    const auto& local_ident_hash = creator_info.identity().hash();

    const auto& is_chacha = std::is_same<layer_t, chacha_layer_t>::value;

    record_flags_t flags(is_chacha ? record_flags_t::ChaChaHop : record_flags_t::Hop);

    auto hops_begin = hops_.begin();
    const auto hops_end = hops_.end();
    const auto last_hop = --hops_.end();

    for (auto hop_it = hops_begin; hop_it != hops_end; ++hop_it)
      {
        // last hop's next hop is the tunnel gateway of the reply tunnel for outbound tunnels,
        // or the IBEP for inbound tunnels (zero-hop, IBGW, or IBEP)
        auto next_hop = hop_it;
        const auto& next_hop_ident = hop_it != last_hop ? (++next_hop)->info().identity().hash()
                                                        : std::is_same<processor_t, obgw_processor_t>::value
                                                              ? re_lease.tunnel_gateway()
                                                              : creator_info.identity().hash();

        const auto& hop_ident = hop_it->info().identity().hash();
        const auto& hop_key_info = hop_it->key_info();

        record_params_t params{
            hop_it->tunnel_id(), local_ident_hash, hop_it->next_tunnel_id(), next_hop_ident, hop_ident, message_id};

        // create and AEAD encrypt each hop's BuildRequestRecord
        if (std::is_same<processor_t, obgw_processor_t>::value)
          {
            if (hop_it != last_hop)
              flags = is_chacha ? record_flags_t::ChaChaHop : record_flags_t::Hop;
            else
              flags = is_chacha ? record_flags_t::ChaChaOBEP : record_flags_t::OBEP;

            if (std::is_same<layer_t, aes_layer_t>::value)  // AES record
              reqs.emplace_back(std::move(params), hop_key_info.creator_pubkey(), flags);
            else if (std::is_same<layer_t, chacha_layer_t>::value)  // ChaCha record
              reqs.emplace_back(std::move(params), hop_key_info.creator_pubkey(), hop_key_info.receive_key(), flags);
            else
              ex.throw_ex<std::logic_error>("invalid layer encryption.");
          }
        else if (std::is_same<processor_t, ibep_processor_t>::value)
          {
            if (hop_it == hops_begin)
              flags = is_chacha ? record_flags_t::ChaChaIBGW : record_flags_t::IBGW;
            else
              flags = is_chacha ? record_flags_t::ChaChaHop : record_flags_t::Hop;

            if (std::is_same<layer_t, aes_layer_t>::value || hop_it == hops_begin)  // AES record or ChaCha IBGW
              reqs.emplace_back(std::move(params), hop_key_info.creator_pubkey(), flags);
            else if (std::is_same<layer_t, chacha_layer_t>::value && hop_it != hops_begin)  // ChaCha record, non-IBGW
              reqs.emplace_back(std::move(params), hop_key_info.creator_pubkey(), hop_key_info.receive_key(), flags);
            else
              ex.throw_ex<std::logic_error>("invalid layer encryption.");
          }
        else
          ex.throw_ex<std::logic_error>("invalid message processor, must be a tunnel creator (OBGW or IBEP).");

        auto& record = reqs.back();

        // AEAD encrypt the hop's BuildRequestRecord
        hop_it->EncryptRequestRecord(record.buffer());
      }

    const auto& max_hops = tini2p::under_cast(MaxHops);
    const auto& records_len = hops_len < max_hops ? crypto::RandInRange(hops_len + 1, max_hops) : hops_len;

    // Add random number of dummy records to obfuscate number of real hops in the tunnel
    for (std::uint8_t i = hops_len; i < records_len; ++i)
      {
        reqs.emplace_back(
            crypto::RandBytes<build_message_t::RecordLen>(), build_request_t::deserialize_mode_t::Partial);
        reqs.back().next_message_id(message_id).flags(is_chacha ? record_flags_t::ChaChaHop : record_flags_t::Hop);
      }

    return build_message_t(reqs);
  }

  Processor& PreprocessBuildMessage(build_message_t& message)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    if (std::is_same<layer_t, aes_layer_t>::value)
      PreprocessAESBuildMessage(message);
    else if (std::is_same<layer_t, chacha_layer_t>::value)
      PreprocessChaChaBuildMessage(message);
    else
      ex.throw_ex<std::invalid_argument>("invalid layer encryption");

    return *this;
  }

  /// @brief Iteratively decrypt AES TunnelBuildMessage records
  /// @detail Decrypts records so that as each hop encrypts the records,
  ///    the next record is revealed for the next hop.
  ///
  ///    Order of decryption is important because of how AES-CBC mixes key
  ///    material with the input.
  ///
  ///    The layers of encryption must be "peeled off" in the reverse order
  ///    they are applied.
  ///
  ///    Record zero (first record) goes out without symmetric decryption,
  ///        so that the first hop can find its record.
  ///
  ///    After encryption, records are shuffled from their order in the tunnel.
  ///
  ///    This allows proper "decryption" by the tunnel hop's, while still obscuring
  ///    the order of hops in the tunnel. Meaning, the first record index does not
  ///    necessarily correspond to the first hop in the tunnel.
  ///
  ///    Example:
  ///        Decrypt(rcrd1, hop0_key)
  ///        Decrypt(rcrd2, hop1_key), Decrypt(rcrd2, hop0_key)
  ///        Decrypt(rcrd3, hop2_key), Decrypt(rcrd3, hop1_key), Decrypt(rcrd3, hop0_key)
  Processor& PreprocessAESBuildMessage(build_message_t& message)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    auto& records = message.records();
    const auto& records_len = records.size();

    std::vector<typename build_request_t::trunc_ident_t> trunc_idents;
    trunc_idents.reserve(records_len);

    // Save the in-order record's truncated Identity hash
    for (const auto& record : records)
      trunc_idents.emplace_back(record.truncated_ident());

    std::scoped_lock sgd(hops_mutex_);
    const auto& hops_len = hops_.size();

    auto hop_rbegin = hops_.rbegin();
    const auto hops_rend = hops_.rend();

    auto rec_it = ++records.begin();
    auto records_begin = records.begin();
    auto records_end = records.end();
    auto rec_hop_end = records.begin();  // end of the actual records (excluding dummies)
    std::advance(rec_hop_end, hops_len);

    // Iteratively decrypt each record, using each preceding hop's reply key
    for (; rec_it != records_end; ++rec_it)
      {
        auto hop_it = hop_rbegin;
        std::advance(hop_it, std::distance(rec_it, rec_hop_end));

        for (; hop_it != hops_rend; ++hop_it)
          hop_it->DecryptRecord(rec_it->buffer());
      }

    // Shuffle the records
    crypto::RandGenerator g;
    std::shuffle(records.begin(), records.end(), g);

    records_begin = records.begin();
    records_end = records.end();

    const auto trunc_begin = trunc_idents.begin();

    shuffle_build_order_.resize(records_len);

    // Save the shuffle order for decrypting TunnelBuildReply
    for (auto trunc_it = trunc_begin; trunc_it != trunc_idents.end(); ++trunc_it)
      {
        const auto rec_it = std::find_if(
            records_begin, records_end, [&trunc_it](const auto& r) { return r.truncated_ident() == *trunc_it; });

        shuffle_build_order_.at(std::distance(trunc_begin, trunc_it)) = std::distance(records_begin, rec_it);
      }

    // Serialize the records to buffer
    message.serialize();

    return *this;
  }

  /// @brief Iteratively decrypt ChaCha TunnelBuildMessage records
  /// @detail Decrypts records so that as each hop encrypts the records,
  ///    the next record is revealed for the next hop.
  ///
  ///    Order of decryption is not important because ChaCha20 simply XORs
  ///    the keystream material with the input.
  ///
  ///    For consistency with AES preprocessing, and to ensure the proper
  ///    hops decrypt the correct records, decryption is applied in reverse order.
  ///
  ///    Record zero (first record) goes out without symmetric decryption,
  ///        so that the first hop can find its record.
  ///
  ///    Before decryption, records are shuffled from their order in the tunnel.
  ///
  ///    This allows proper "decryption" by the tunnel hop's, while still obscuring
  ///    the order of hops in the tunnel. Meaning, the first record index does not
  ///    necessarily correspond to the first hop in the tunnel. Shuffling must
  ///    happen before encryption so that hops use the proper index for nonce
  ///    generation.
  ///
  ///    Example:
  ///        Decrypt(rcrd1, hop0_key)
  ///        Decrypt(rcrd2, hop1_key), Decrypt(rcrd2, hop0_key)
  ///        Decrypt(rcrd3, hop2_key), Decrypt(rcrd3, hop1_key), Decrypt(rcrd3, hop0_key)
  Processor& PreprocessChaChaBuildMessage(build_message_t& message)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    auto& records = message.records();
    const auto& records_len = records.size();

    std::scoped_lock sgd(hops_mutex_);
    const auto& hops_len = hops_.size();
    const auto& first_trunc_ident = hops_.front().key_info().truncated_ident();

    std::vector<typename build_request_t::trunc_ident_t> trunc_idents;
    trunc_idents.reserve(records_len);

    // Save the in-order records truncated Identity hashes
    for (const auto& record : records)
      trunc_idents.emplace_back(record.truncated_ident());

    // Shuffle the records
    crypto::RandGenerator g;
    std::shuffle(records.begin(), records.end(), g);
    shuffle_build_order_.resize(records_len);

    const auto records_begin = records.begin();
    const auto records_end = records.end();

    const auto first_it = std::find_if(records_begin, records_end, [&first_trunc_ident](const auto& r) {
      return r.truncated_ident() == first_trunc_ident;
    });

    // Save the randomized order of the first hop's record
    shuffle_build_order_.at(0) = std::distance(records_begin, first_it);

    // Iteratively decrypt each record, using each preceding hop's reply key
    auto round_end = records.begin();
    std::advance(round_end, hops_len);

    auto hops_rbegin = hops_.rbegin();
    const auto hops_rend = hops_.rend();
    for (std::uint8_t record_idx = 1; record_idx < hops_len; ++record_idx)
      {
        auto hop_it = hops_rbegin;
        std::advance(hop_it, hops_len - record_idx);
        for (; hop_it != hops_rend; ++hop_it)
          {
            const auto& record_ident = trunc_idents.at(record_idx);

            auto rec_it = std::find_if(records_begin, records_end, [&record_ident](const auto& r) {
              return r.truncated_ident() == record_ident;
            });

            if (rec_it == records_end)
              {
                ex.throw_ex<std::runtime_error>(
                    "no record with truncated Identity hash: " + tini2p::bin_to_hex(record_ident, ex));
              }

            const auto rec_pos = std::distance(records_begin, rec_it);
            hop_it->DecryptRecord(rec_it->buffer(), rec_pos, records_len);

            // Save the randomized record order
            shuffle_build_order_.at(record_idx) = rec_pos;
          }
      }

    const auto trunc_begin = trunc_idents.begin();
    auto trunc_it = trunc_idents.begin();
    std::advance(trunc_it, hops_len);

    // Save the randomized order for the remaining dummy records
    for (; trunc_it != trunc_idents.end(); ++trunc_it)
      {
        const auto rec_it = std::find_if(
            records_begin, records_end, [&trunc_it](const auto& r) { return r.truncated_ident() == *trunc_it; });

        shuffle_build_order_.at(std::distance(trunc_begin, trunc_it)) = std::distance(records_begin, rec_it);
      }

    // Serialize the records to buffer
    message.serialize();

    return *this;
  }

#ifdef TINI2P_TESTS
  /// @brief Simulate AES tunnel hops creating and encrypting their BuildResponseRecords
  /// @detail Only used for testing
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  data::I2NPMessage CreateAESReplyMessage(const message_id_t msg_id, const CreateReplyType& reply_type)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    std::scoped_lock shd(hops_mutex_);

    const auto& hops_len = hops_.size();
    const auto& max_hops = tini2p::under_cast(MaxHops);
    const auto& records_len = hops_len < max_hops ? crypto::RandInRange(hops_len + 1, max_hops) : hops_len;

    // if less than max hops, add a random number of records to the message (up to max hops)
    aes_reply_message_t::records_t records;
    // random index for the one accept/reject record, if there is one 
    for (std::uint8_t i = 0; i < records_len; ++i)
      {
        if (reply_type == CreateReplyType::AllReject || reply_type == CreateReplyType::OneAccept)
          records.emplace_back(aes_reply_message_t::reply_record_t::flags_t::Reject);
        else if (reply_type == CreateReplyType::AllAccept || reply_type == CreateReplyType::OneReject)
          records.emplace_back(aes_reply_message_t::reply_record_t::flags_t::Accept);
        else
          ex.throw_ex<std::invalid_argument>("invalid reply creation type.");
      }

    shuffle_build_order_.resize(records_len);
    for (std::uint8_t i = 0; i < records_len; ++i)
      shuffle_build_order_.at(i) = i;

    // Randomize record order to simulate randomization of BuildRequestRecords
    crypto::RandGenerator g;
    std::shuffle(shuffle_build_order_.begin(), shuffle_build_order_.end(), g);

    // Pick a random record to set as the one accept/reject, depending on reply type
    const auto& shuffle_rand_idx = shuffle_build_order_.at(crypto::RandInRange(0, hops_len));
    auto& rand_rec = records.at(shuffle_rand_idx);
    const auto& rand_flag = reply_type == CreateReplyType::AllReject || reply_type == CreateReplyType::OneReject
                                ? build_reply_t::flags_t::Reject
                                : build_reply_t::flags_t::Accept;
    rand_rec.flags(rand_flag);
    rand_rec.serialize();

    // Encrypt the BuildResponseRecords in a randomized hop order
    const auto hops_begin = hops_.begin();
    const auto hops_end = hops_.end();
    for (auto hop_it = hops_.begin(); hop_it != hops_end; ++hop_it)
      {
        for (std::uint8_t rec_idx = 0; rec_idx <= std::distance(hops_begin, hop_it); ++rec_idx)
          hop_it->EncryptRecord(records.at(shuffle_build_order_.at(rec_idx)).buffer());
      }

    return data::I2NPMessage(
        data::I2NPMessage::Type::VariableTunnelBuildReply, msg_id, reply_message_t(std::move(records)).buffer());
  }

  /// @brief Simulate ChaCha tunnel hops creating and encrypting their BuildResponseRecords
  /// @detail Only used for testing
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  data::I2NPMessage CreateChaChaReplyMessage(const message_id_t msg_id, const CreateReplyType& reply_type)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    std::scoped_lock shd(hops_mutex_);

    const auto& hops_len = hops_.size();
    const auto& max_hops = tini2p::under_cast(MaxHops);
    const auto& records_len = hops_len < max_hops ? crypto::RandInRange(hops_len + 1, max_hops) : hops_len;

    // if less than max hops, add a random number of records to the message (up to max hops)
    chacha_reply_message_t::records_t records;
    for (std::uint8_t i = 0; i < records_len; ++i)
      {
        if (reply_type == CreateReplyType::AllReject || reply_type == CreateReplyType::OneAccept)
          records.emplace_back(chacha_reply_message_t::reply_record_t::flags_t::Reject);
        else if (reply_type == CreateReplyType::AllAccept || reply_type == CreateReplyType::OneReject)
          records.emplace_back(chacha_reply_message_t::reply_record_t::flags_t::Accept);
        else
          ex.throw_ex<std::invalid_argument>("invalid reply creation type.");
      }

    shuffle_build_order_.resize(records_len);
    for (std::uint8_t i = 0; i < records_len; ++i)
      shuffle_build_order_.at(i) = i;

    // Randomize record order to simulate randomization of BuildRequestRecords
    crypto::RandGenerator g;
    std::shuffle(shuffle_build_order_.begin(), shuffle_build_order_.end(), g);


    // Pick a random record to set as the one accept/reject, depending on reply type
    const auto& shuffle_rand_idx = shuffle_build_order_.at(crypto::RandInRange(0, hops_len));
    auto& rand_rec = records.at(shuffle_rand_idx);
    const auto& rand_flag = reply_type == CreateReplyType::AllReject || reply_type == CreateReplyType::OneReject
                                ? build_reply_t::flags_t::Reject
                                : build_reply_t::flags_t::Accept;
    rand_rec.flags(rand_flag);
    rand_rec.serialize();

    // Encrypt the BuildResponseRecords in a randomized hop order
    const auto hops_begin = hops_.begin();
    const auto hops_end = hops_.end();
    for (auto hop_it = hops_.begin(); hop_it != hops_end; ++hop_it)
      {
        const auto& hop_pos = std::distance(hops_begin, hop_it);
        const auto& shuffle_hop_idx = shuffle_build_order_.at(hop_pos);

        for (std::uint8_t rec_idx = 0; rec_idx <= hop_pos; ++rec_idx)
          {
            const auto& shuffle_rec_idx = shuffle_build_order_.at(rec_idx);

            if (shuffle_rec_idx == shuffle_hop_idx)  // AEAD encrypt this hop's reply
              hop_it->EncryptReplyRecord(records.at(shuffle_rec_idx).buffer());
            else  // ChaCha20 encrypt other hop replies
              hop_it->EncryptRecord(records.at(shuffle_rec_idx).buffer(), shuffle_rec_idx, records_len);
          }
      }

    return data::I2NPMessage(
        data::I2NPMessage::Type::VariableTunnelBuildReply, msg_id, reply_message_t(std::move(records)).buffer());
  }
#endif

  /// @brief Handle a AES VariableTunnelBuildReply message
  /// @param reply VariableTunnelBuildReply message with each hop's BuildResponseRecord
  /// @return One if the tunnel was accepted by all hops, zero otherwise
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  std::uint8_t HandleBuildReply(aes_reply_message_t& reply)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    auto& records = reply.records();
    const auto& records_len = records.size();

    std::scoped_lock sgd(hops_mutex_);

    const auto& shuffle_len = shuffle_build_order_.size();

    check_shuffle_size(records_len, shuffle_len, ex);

    std::uint8_t accept(1);

    // For each hop, decrypt its BuildResponseRecord, checking if it accepted the tunnel
    // Decrypt all other records in the VariableTunnelBuildReply message
    // Iterate in reverse to process the last hop's record first, removing each hop's
    //   layer of encryption in reverse order.
    auto hops_rbegin = hops_.rbegin();
    const auto hops_rend = hops_.rend();
    const auto last_hop = --hops_.rend();
    for (auto hop_it = hops_rbegin; hop_it != hops_rend; ++hop_it)
      {
        const auto& hop_pos = std::distance(hop_it, last_hop);  // the hop index
        const auto& rec_pos = shuffle_build_order_.at(hop_pos);

        // Using the hop's reply key, decrypt the hop's own BuildResponseRecord and all other records
        for (std::uint8_t round_idx = 0; round_idx < records_len; ++round_idx)
          {
            const auto& shuffle_idx = shuffle_build_order_.at(round_idx);
            auto& reply = records.at(shuffle_idx);
            hop_it->DecryptRecord(reply.buffer());

            if (shuffle_idx == rec_pos)
              {
                reply.deserialize();
                accept *= static_cast<std::uint8_t>(reply.flags() == build_reply_t::flags_t::Accept);
              }
          }
      }

    return accept;
  }

  /// @brief Handle a ChaCha VariableTunnelBuildReply message
  /// @param reply VariableTunnelBuildReply message with each hop's BuildResponseRecord
  /// @return One if the tunnel was accepted by all hops, zero otherwise
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  std::uint8_t HandleBuildReply(chacha_reply_message_t& reply)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    auto& records = reply.records();
    const auto& records_len = records.size();
    const auto& shuffle_len = shuffle_build_order_.size();

    std::scoped_lock sgd(hops_mutex_);

    check_shuffle_size(records_len, shuffle_len, ex);

    std::uint8_t accept(1);

    // For each hop, decrypt its BuildResponseRecord, checking if it accepted the tunnel
    // Decrypt all other records in the VariableTunnelBuildReply message
    // Iterate in reverse to process the last hop's record first, removing each hop's
    //   layer of encryption in reverse order.
    auto hops_rbegin = hops_.rbegin();
    const auto hops_rend = hops_.rend();
    const auto last_hop = --hops_.rend();
    for (auto hop_it = hops_rbegin; hop_it != hops_rend; ++hop_it)
      {
        const auto hop_pos = std::distance(hop_it, last_hop);  // the hop index
        const auto& rec_pos = shuffle_build_order_.at(hop_pos);

        // Using the hop's reply key, decrypt the hop's own BuildResponseRecord and all other records
        for (std::int8_t round_idx = hop_pos; round_idx >= 0; --round_idx)
          {
            const auto& shuffle_idx = shuffle_build_order_.at(round_idx);
            if (shuffle_idx == rec_pos)
              {
                auto& reply = records.at(rec_pos);
                hop_it->DecryptReplyRecord(reply.buffer());

                reply.deserialize();
                accept *= static_cast<std::uint8_t>(reply.flags() == build_reply_t::flags_t::Accept);
              }
            else
              hop_it->DecryptRecord(records.at(shuffle_idx).buffer(), shuffle_idx, records_len);
          }
      }

    return accept;
  }

  /// @brief Fragment an I2NP message buffer into outbound TunnelMessage(s)
  /// @tparam TRandomizer Randomization object (AES IV or ChaCha nonces)
  /// @param i2np_buf I2NP message buffer to fragment
  /// @param to_hash Optional Identity hash of the InboundGateway or Router to deliver the messages
  /// @param tunnel_id Optional tunnel ID of the InboundGateway to deliver the messages (if outbound)
  /// @return Collection of TunnelMessage(s) to send through the outbound tunnel
  std::vector<tunnel_message_t> CreateTunnelMessages(
      const i2np_block_t::buffer_t& i2np_buf,
      std::optional<to_hash_t> to_hash,
      std::optional<tunnel_id_t> tunnel_id)
  {
    using delivery_type_t = typename data::TunnelMessageFragmenter<layer_t>::delivery_type_t;

    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    const auto& delivery_type = to_hash == std::nullopt
                                    ? delivery_type_t::Local
                                    : tunnel_id == std::nullopt ? delivery_type_t::Router : delivery_type_t::Tunnel;

    // Fragment the I2NP buffer into delivery instructions and message fragments
    auto message_body = data::TunnelMessageFragmenter<layer_t>::FragmentI2NP(
        delivery_type,
        i2np_buf,
        std::forward<std::optional<to_hash_t>>(to_hash),
        std::forward<std::optional<tunnel_id_t>>(tunnel_id));

    auto& deliveries = message_body.deliveries;
    auto& fragments = message_body.fragments;
    const auto frags_len = fragments.size();

    tunnel_id_t hop_id(next_id_);

    std::vector<tunnel_message_t> messages;
    std::unique_ptr<tunnel_message_t> message;

    // Add the message fragments and delivery instructions to TunnelMessage(s)
    for (std::uint16_t frag_idx = 0; frag_idx < frags_len; ++frag_idx)
      {
        auto& fragment = fragments.at(frag_idx);

        std::visit(
            [this, &frag_idx, &frags_len, &hop_id, &message, &messages, fragment = std::move(fragment), &ex](auto&& delivery) {
              if (message == nullptr)
                {
                  auto rand = layer_t::create_randomizer();
                  check_bloom_filter(rand, ex);

                  message = std::make_unique<tunnel_message_t>(
                      hop_id, std::move(rand), std::move(delivery), std::move(fragment));
                }
              else if (message->remaining_size() > delivery.size() + fragment.size())
                message->add_fragment(std::move(delivery), std::move(fragment));
              else
                {
                  message->serialize();
                  messages.emplace_back(std::move(*message.release()));

                  auto rand = layer_t::create_randomizer();
                  check_bloom_filter(rand, ex);

                  message = std::make_unique<tunnel_message_t>(hop_id, std::move(rand), std::move(delivery), std::move(fragment));
                }

              if (frag_idx == frags_len - 1)
                {
                  message->serialize();
                  messages.emplace_back(std::move(*message.release()));
                }
            },
            std::move(deliveries.at(frag_idx)));
      }

    return messages;
  }

  /// @brief Extract I2NP message fragments from a tunnel message
  /// @param message Tunnel message containing message fragments
  /// @return Collection of completed I2NP message buffers
  extracted_i2np_t ExtractI2NPFragments(tunnel_message_t& message)
  {
    using first_delivery_t = typename tunnel_message_t::first_delivery_t;
    using follow_delivery_t = typename tunnel_message_t::follow_delivery_t;
    using i2np_gateway_t = data::TunnelGateway;

    const exception::Exception ex{"Tunnel: Processor", __func__};

    auto& frags = message.fragments();
    const auto& deliveries = message.deliveries();
    const auto deliveries_begin = deliveries.begin();
    const auto deliveries_end = deliveries.end();

    extracted_i2np_t i2np_msgs;

    std::scoped_lock sgd(messages_mutex_);
    for (auto dlv_it = deliveries_begin; dlv_it != deliveries_end; ++dlv_it)
      {
        std::optional<tunnel_id_t> tun_id;
        std::optional<message_id_t> msg_id;
        std::optional<to_hash_t> to_hash;
        std::optional<typename first_delivery_t::delivery_t> dlv_type;
        frag_message_t::frag_id_t frag_id(0);
        frag_message_t::is_last_t is_last(frag_message_t::is_last_t::False);

        if (std::holds_alternative<first_delivery_t>(*dlv_it))
          {
            const auto& dlv = std::get<first_delivery_t>(*dlv_it);
            tun_id = dlv.tunnel_id();
            msg_id = dlv.message_id();
            to_hash = dlv.to_hash_opt();
            dlv_type.emplace(dlv.delivery());
          }
        else if (std::holds_alternative<follow_delivery_t>(*dlv_it))
          {
            const auto& delivery = std::get<follow_delivery_t>(*dlv_it);
            msg_id.emplace(delivery.message_id());
            frag_id = delivery.fragment_number();
            is_last = static_cast<frag_message_t::is_last_t>(delivery.is_last());
          }
        else
          ex.throw_ex<std::invalid_argument>("invalid delivery type.");

        if (msg_id == std::nullopt)  // unfragmented first fragment
          {
            auto& frag = frags.at(std::distance(deliveries_begin, dlv_it));
            msg_id.emplace(0);
            tini2p::read_bytes(frag.data() + data::I2NPHeader::TypeLen, *msg_id, tini2p::Endian::Big);

            if (std::is_same<processor_t, obep_processor_t>::value)
              {  // Outbound tunnel delivery
                if (to_hash == std::nullopt)
                  ex.throw_ex<std::runtime_error>("null to hash for outbound tunnel message.");

                if (dlv_type == std::nullopt)
                  ex.throw_ex<std::runtime_error>("no delivery type set for first fragment.");

                if (*dlv_type == first_delivery_t::delivery_t::Router)
                  i2np_msgs.emplace_back(std::move(to_hash), std::move(frag));
                else if (*dlv_type == first_delivery_t::delivery_t::Tunnel)
                  {  // wrap the I2NP message in a TunnelGateway message
                    if (tun_id == std::nullopt)
                      ex.throw_ex<std::invalid_argument>("null tunnel ID for tunnel delivery type.");

                    i2np_msgs.emplace_back(std::make_pair<std::optional<to_hash_t>, data::I2NPMessage>(
                        std::move(to_hash),
                        data::I2NPMessage(
                            data::I2NPHeader::Type::TunnelGateway,
                            *msg_id,
                            i2np_gateway_t(*tun_id, std::move(frag)).buffer(),
                            data::I2NPHeader::Mode::Normal)));
                  }
                else
                  ex.throw_ex<std::logic_error>("invalid delivery type for outbound tunnel.");
              }
            else
              {  // InboundTunnel delivery
                if (dlv_type == std::nullopt)
                  ex.throw_ex<std::runtime_error>("no delivery type set for first fragment.");

                if (*dlv_type != first_delivery_t::delivery_t::Local)
                  ex.throw_ex<std::logic_error>("only local delivery supported for inbound tunnels.");

                i2np_msgs.emplace_back(std::nullopt, std::move(frag));
              }
          }
        else
          {  // fragmented first or follow-on fragment
            auto msg_it = messages_.find(*msg_id);
            const auto frag_pos = dlv_it - deliveries_begin;
            if (msg_it == messages_.end())  // add a new fragmented message
              {
                messages_.try_emplace(*msg_id, std::move(frag_id), std::move(frags.at(frag_pos)), is_last);

                auto& message = messages_[*msg_id];

                if (dlv_type != std::nullopt)
                  message.delivery(std::move(*dlv_type));

                if (tun_id != std::nullopt)
                  message.tunnel_id(std::move(*tun_id));

                if (to_hash != std::nullopt)
                  message.to_hash(std::move(*to_hash));
              }
            else if (msg_it->second.AddFragment(std::move(frag_id), std::move(frags.at(frag_pos)), is_last) == 1)
              {  // message is complete: write it to the I2NP buffer, and discard the fragmented message
                dlv_type.emplace(msg_it->second.delivery());

                if (std::is_same<processor_t, obep_processor_t>::value)
                  {
                    if (*dlv_type == first_delivery_t::delivery_t::Router)
                      i2np_msgs.emplace_back(msg_it->second.to_hash(), msg_it->second.CompleteMessage());
                    else if (*dlv_type == first_delivery_t::delivery_t::Tunnel)
                      {  // wrap the I2NP message in a TunnelGateway message
                        i2np_msgs.emplace_back(std::make_pair<std::optional<to_hash_t>, data::I2NPMessage>(
                            msg_it->second.to_hash(),
                            data::I2NPMessage(
                                data::I2NPMessage::Type::TunnelGateway,
                                *msg_id,
                                i2np_gateway_t(msg_it->second.tunnel_id(), msg_it->second.CompleteMessage())
                                    .buffer(),
                                    data::I2NPHeader::Mode::Normal)));
                      }
                    else
                      ex.throw_ex<std::logic_error>("invalid delivery type for outbound tunnel.");
                  }
                else
                  {
                    if (*dlv_type != first_delivery_t::delivery_t::Local)
                      ex.throw_ex<std::logic_error>("only Local delivery supported for inbound tunnels.");

                    i2np_msgs.emplace_back(std::nullopt, msg_it->second.CompleteMessage());
                  }

                messages_.erase(msg_it);
              }
          }
      }

    return i2np_msgs;
  }

  /// @brief AES encrypt a tunnel build record
  /// @detail Used to encrypt build records with the hop's reply key
  template <class TRecord, typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  void EncryptRecord(TRecord& record)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    layer_->EncryptRecord(record);
  }

  /// @brief AES decrypt a tunnel build record
  /// @detail Used to decrypt build records with the hop's reply key
  template <class TRecord>
  void DecryptRecord(TRecord& record)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    layer_->DecryptRecord(record);
  }

  /// @brief ChaCha20-Poly1305 encrypt this hop's BuildRequestRecord
  /// @param record BuildRequestRecord buffer to decrypt
  template <class TRecord>
  void EncryptRequestRecord(TRecord& record)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    layer_->EncryptRequestRecord(record);
  }

  /// @brief ChaCha20-Poly1305 encrypt this hop's BuildResponseRecord
  /// @param record BuildResponseRecord buffer to decrypt
  template <class TRecord, typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  void EncryptReplyRecord(TRecord& record)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    layer_->EncryptReplyRecord(record);
  }

  /// @brief ChaCha20 encrypt a tunnel BuildRequestRecord or BuildResponseRecord
  /// @detail Used to encrypt build records with the hop's reply key
  template <class TRecord, typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  void EncryptRecord(TRecord& record, const std::uint8_t& pos, const std::uint8_t& num_records)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    layer_->EncryptRecord(record, pos, num_records);
  }

  /// @brief ChaCha20-Poly1305 decrypt this hop's BuildRequestRecord
  /// @param record BuildRequestRecord buffer to decrypt
  template <class TRecord>
  void DecryptRequestRecord(TRecord& record)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    layer_->DecryptRequestRecord(record);
  }

  /// @brief ChaCha20-Poly1305 decrypt this hop's BuildResponseRecord
  /// @param record BuildResponseRecord buffer to decrypt
  template <class TRecord, typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  void DecryptReplyRecord(TRecord& record)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    layer_->DecryptReplyRecord(record);
  }

  /// @brief ChaCha20 decrypt a tunnel BuildRequestRecord or BuildResponseRecord
  /// @detail Used to decrypt build records with the hop's reply key
  template <class TRecord>
  void DecryptRecord(TRecord& record, const std::uint8_t& pos, const std::uint8_t& num_records)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    layer_->DecryptRecord(record, pos, num_records);
  }

  /// @brief AES encrypt a tunnel message
  /// @detail Used by AES tunnel participants to add their layer and IV encryption
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  aes_layer_t::aes_t::iv_t Encrypt(tunnel_message_t& message)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    // Write the next tunnnel ID to the mesage
    auto& msg_buf = message.buffer();
    tini2p::write_bytes(msg_buf.data(), next_id_, tini2p::Endian::Big);

    return layer_->Encrypt(msg_buf);
  }

  /// @brief AES decrypt a tunnel message
  /// @detail Used by AES tunnel:
  ///     - outbound gateway to preemptively decrypt outbound messages
  ///     - inbound endpoint to recover inbound messages packaged by the inbound gateway
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  aes_layer_t::aes_t::iv_t Decrypt(tunnel_message_t& message)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    return layer_->Decrypt(message.buffer());
  }

  /// @brief ChaCha20 encrypt a tunnel message
  /// @detail Used by ChaCha tunnel participants to add their layer and IV encryption
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  chacha_layer_t::nonces_t Encrypt(tunnel_message_t& message)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    // Write the next tunnnel ID to the mesage
    auto& msg_buf = message.buffer();
    tini2p::write_bytes(msg_buf.data(), next_id_, tini2p::Endian::Big);

    return layer_->Encrypt(msg_buf);
  }

  /// @brief ChaCha20-Poly1305-AEAD encrypt a tunnel message
  /// @detail Used after layer and nonce encryption to AEAD encrypt the message for the next hop
  void EncryptAEAD(tunnel_message_t& message)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    layer_->EncryptAEAD(message.buffer());
  }

  /// @brief ChaCha20 decrypt a tunnel message
  /// @detail Used by ChaCha tunnel:
  ///     - outbound gateway to preemptively decrypt outbound messages
  ///     - inbound endpoint to recover inbound messages packaged by the inbound gateway
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  chacha_layer_t::nonces_t Decrypt(tunnel_message_t& message)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    return layer_->Decrypt(message.buffer());
  }

  /// @brief ChaCha20-Poly1305-AEAD decrypt a tunnel message
  /// @detail Used before layer and nonce en/decryption to AEAD decrypt the received message
  void DecryptAEAD(tunnel_message_t& message)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    layer_->DecryptAEAD(message.buffer());
  }

  /// @brief Iteratively decrypt a TunnelMessage
  void IterativeDecryption(tunnel_message_t& message)
  {
    std::scoped_lock sgd(hops_mutex_);
    for (auto hop_it = hops_.rbegin(); hop_it != hops_.rend(); ++hop_it)
      hop_it->Decrypt(message);
  }

#ifdef TINI2P_TESTS
  /// @brief Iteratively encrypt TunnelMessage
  /// @detail Test utility function for simulating hop processing in a tunnel
  void IterativeEncryption(tunnel_message_t& message)
  {
    std::scoped_lock sgd(hops_mutex_);
    for (auto& hop : hops_)
      hop.Encrypt(message);
  }
#endif

  /// @brief Get a const reference to the hop's RouterInfo
  const info_t& info() const
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_info(info_, ex);

    return *info_;
  }

  /// @brief Get a non-const reference to the hop's RouterInfo
  info_t& info()
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_info(info_, ex);

    return *info_;
  }

  /// @brief Get a const reference to the reply tunnel gateway's RouterInfo
  const lease_t& reply_lease() const noexcept
  {
    return reply_lease_;
  }

  /// @brief Get a const reference to the tunnel hops
  const hops_t& hops() const noexcept
  {
    return hops_;
  }

  /// @brief Get a non-const reference to the tunnel hops
  hops_t& hops() noexcept
  {
    return hops_;
  }

  /// @brief Get the first hop in the tunnel
  /// @detail Useful for getting the inbound gateway
  const hop_t& first_hop()
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    std::shared_lock hs(hops_mutex_, std::defer_lock);
    std::scoped_lock sgd(hs);

    if (hops_.empty())
      ex.throw_ex<std::runtime_error>("no hops");

    return hops_.front();
  }

  /// @brief Get a const reference to the hop's tunnel ID
  const tunnel_id_t& tunnel_id() const noexcept
  {
    return id_;
  }

  /// @brief Set the hop's tunnel ID
  Processor& tunnel_id(tunnel_id_t id)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_ids(id, next_id_, ex);

    id_ = std::forward<tunnel_id_t>(id);

    return *this;
  }

  /// @brief Get a const reference to the next hop's tunnel ID
  const tunnel_id_t& next_tunnel_id() const noexcept
  {
    return next_id_;
  }

  /// @brief Set the next hop's tunnel ID
  Processor& next_tunnel_id(tunnel_id_t next_id)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_ids(id_, next_id, ex);

    next_id_ = std::forward<tunnel_id_t>(next_id);

    return *this;
  }

  /// @brief Get a const reference to the hop's key information
  const typename layer_t::key_info_t& key_info() const
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    return layer_->key_info();
  }

  /// @brief Get a non-const reference to the hop's key information
  typename layer_t::key_info_t& key_info()
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_layer(layer_, ex);

    return layer_->key_info();
  }

  /// @brief Get a const reference to the peer key used for peer sorting
  /// @note Only non-null for tunnel creators (IBEP, OBGW)
  /// @throws Runtime error if null peer key
  const peer_key_t& peer_key() const
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_peer_key(peer_key_, ex);

    return *peer_key_;
  }

  /// @brief Get a const reference to the peer key used for peer sorting
  /// @note Only non-null for tunnel participants (IBGW, OBEP, Hop)
  /// @throws Runtime error if null peer metric
  const peer_metric_t& peer_metric() const
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_peer_metric(peer_metric_, ex);

    return *peer_metric_;
  }

  void check_bloom_filter(const aes_layer_t::aes_t::iv_t& iv, const exception::Exception& ex)
  {
    if (bloom_ == std::nullopt)
      ex.throw_ex<std::logic_error>("null Bloom filter.");

    if(bloom_->CheckElement(iv) == 1)
      ex.throw_ex<std::logic_error>("duplicate IV found: " + tini2p::bin_to_hex(iv, ex));
  }

  void check_bloom_filter(const chacha_layer_t::nonces_t& nonces, const exception::Exception& ex)
  {
    if (bloom_ == std::nullopt)
      ex.throw_ex<std::logic_error>("null Bloom filter.");

    if(bloom_->CheckElement(static_cast<chacha_layer_t::nonce_t::buffer_t>(nonces.tunnel_nonce)) == 1)
      ex.throw_ex<std::logic_error>("duplicate tunnel nonce found: " + tini2p::bin_to_hex(nonces.tunnel_nonce, ex));

    if(bloom_->CheckElement(static_cast<chacha_layer_t::nonce_t::buffer_t>(nonces.obfs_nonce)) == 1)
      ex.throw_ex<std::logic_error>("duplicate randomization nonce found: " + tini2p::bin_to_hex(nonces.obfs_nonce, ex));
  }

  /// @brief Equality comparison operator with another Processor
  std::uint8_t operator==(const Processor& oth) const
  {
    const auto& id_eq = static_cast<std::uint8_t>(id_ == oth.id_);
    const auto& next_eq = static_cast<std::uint8_t>(next_id_ == oth.next_id_);
    const auto& info_eq = static_cast<std::uint8_t>(info_ && oth.info_ && (*info_ == *oth.info_));
    const auto& reply_eq = reply_lease_ == oth.reply_lease_;
    const auto& peer_eq = static_cast<std::uint8_t>(peer_key_ == oth.peer_key_);
    const auto& metric_eq = static_cast<std::uint8_t>(peer_metric_ == oth.peer_metric_);
    const auto& lay_key_eq =
        static_cast<std::uint8_t>(layer_ && oth.layer_ && (layer_->key_info() == oth.layer_->key_info()));

    return (id_eq * next_eq * info_eq * reply_eq * peer_eq * metric_eq * lay_key_eq);
  }

  /// @brief Less-than comparison operator with another Processor
  std::uint8_t operator<(const Processor& oth) const
  {
    return peer_metric_ < oth.peer_metric_;
  }

 protected:
  tunnel_id_t create_tunnel_id() const
  {
    return crypto::RandInRange(tini2p::under_cast(MinTunnelID), tini2p::under_cast(MaxTunnelID));
  }

  peer_metric_t CalculatePeerMetric(const peer_key_t& peer_key)
  {
    const exception::Exception ex{"Tunnel: Processor", __func__};

    check_info(info_, ex);

    const auto& ident_hash = info_->identity().hash();

    blake_t::digest_t digest;
    blake_t::Hash(digest, ident_hash, peer_key);

    return digest ^ peer_key;
  }

  void check_id(const tunnel_id_t& id, const exception::Exception& ex) const
  {
    if (id == tini2p::under_cast(tunnel_message_t::NullID))
      ex.throw_ex<std::logic_error>("null tunnel ID.");
  }

  void check_ids(const tunnel_id_t& id, const tunnel_id_t& next_id, const exception::Exception& ex) const
  {
    check_id(id, ex);
    check_id(next_id, ex);

    if (id == next_id)
      {
        ex.throw_ex<std::logic_error>(
            "hop ID: " + std::to_string(id) + " and next ID: " + std::to_string(next_id) + " must be unique.");
      }
  }

  void check_info(const info_t::shared_ptr& info_ptr, const exception::Exception& ex) const
  {
    if (!info_ptr)
      ex.throw_ex<std::invalid_argument>("null RouterInfo.");

    if (!info_ptr->Verify())
      ex.throw_ex<std::invalid_argument>("invalid RouterInfo, failed signature verification.");
  }

  void check_reply_lease(const lease_t& lease, const exception::Exception& ex) const
  {
    try
      {
        lease.check_expiration();
      }
    catch (const std::exception& e)
      {
        ex.throw_ex<std::logic_error>(e.what());
      }
  }

  void check_infos(const std::vector<info_t::shared_ptr>& infos, const exception::Exception& ex) const
  {
    const auto& infos_len = infos.size();
    const auto& min_hops = tini2p::under_cast(MinHops);
    const auto& max_hops = tini2p::under_cast(MaxHops);

    if (infos_len < min_hops || infos_len > max_hops)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid number of hops: " + std::to_string(infos_len) + ", min: " + std::to_string(min_hops)
            + ", max: " + std::to_string(max_hops));
      }

    for (const auto& info : infos)
      check_info(info, ex);
  }

  void check_creator_hops(
      const info_t& creator,
      const std::vector<info_t::shared_ptr>& hops,
      const exception::Exception& ex) const
  {
    for (const auto& hop : hops)
      {
        if (!hop)
          ex.throw_ex<std::invalid_argument>("null hop.");

        if (creator == *hop)
          ex.throw_ex<std::logic_error>("tunnel creator can not be a hop in the tunnel (only OBGW or IBEP).");
      }
  }

  void check_layer(const std::unique_ptr<layer_t>& layer_ptr, const exception::Exception& ex) const
  {
    if (!layer_ptr)
      ex.throw_ex<std::invalid_argument>("null layer encryption, set layer keys.");
  }

  void check_shuffle_size(
      const std::uint8_t& records_len,
      const std::uint8_t& shuffle_len,
      const exception::Exception& ex) const
  {
    if (shuffle_len != records_len)
      {
        ex.throw_ex<std::length_error>(
            "shuffle order length: " + std::to_string(shuffle_len)
            + " does not match records length: " + std::to_string(records_len));
      }
  }

  void check_peer_key(const std::optional<peer_key_t>& peer_key, const exception::Exception& ex) const
  {
    if (peer_key == std::nullopt)
      ex.throw_ex<std::runtime_error>("null peer key.");
  }

  void check_peer_metric(const std::optional<peer_metric_t>& peer_metric, const exception::Exception& ex) const
  {
    if (peer_metric == std::nullopt)
      ex.throw_ex<std::runtime_error>("null peer metric.");
  }

  tunnel_id_t id_;
  tunnel_id_t next_id_;
  info_t::shared_ptr info_;
  lease_t reply_lease_;
  std::optional<peer_key_t> peer_key_;
  std::optional<peer_metric_t> peer_metric_;

  std::unique_ptr<layer_t> layer_;

  messages_t messages_;
  std::shared_mutex messages_mutex_;

  std::optional<bloom_filter_t> bloom_;

  hops_t hops_;
  std::shared_mutex hops_mutex_;

  std::vector<std::uint8_t> shuffle_build_order_;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_PROCESSOR_H_
