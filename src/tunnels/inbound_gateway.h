/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_INBOUND_GATEWAY_H_
#define SRC_TUNNELS_INBOUND_GATEWAY_H_

#include "src/tunnels/processor.h"

namespace tini2p
{
namespace tunnels
{
/// @class InboundGateway
/// @brief For processing inbound tunnel messages for an InboundGateway
template <class TLayer>
class InboundGateway : public tunnels::Processor<TLayer, tunnels::InboundGatewayProcessor>
{
 public:
  using layer_t = TLayer;
  using base_t = tunnels::Processor<layer_t, tunnels::InboundGatewayProcessor>;
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD encryption trait alias
  using aes_layer_t = crypto::TunnelAES;  //< AES layer encryption trait alias
  using chacha_layer_t = crypto::TunnelChaCha;  //< ChaCha layer encryption trait alias
  using info_t = data::Info;  //< Router info trait alias
  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using tunnel_id_t = typename base_t::tunnel_id_t;  //< Tunnel ID trait alias
  using build_request_t = typename base_t::build_request_t;  //< BuildRequestRecord trait alias
  using build_reply_t = typename base_t::build_reply_t;  //< BuildRequestRecord trait alias
  using record_buffer_t = typename base_t::record_buffer_t;  //< Build record buffer trait alias
  using tunnel_message_t = typename base_t::tunnel_message_t;  //< Tunnel message trait alias

  InboundGateway() : base_t() {}

  /// @brief Initializing ctor, creates an tunnel InboundGateway
  /// @detail Caller responsible for setting layer encryption keys before en/decrypting tunnel messages
  /// @param id Tunnel ID (non-zero) for this endpoint to receive messages
  /// @param next_id Tunnel ID (non-zero) for the next Hop
  /// @param info Shared pointer to the RouterInfo for the InboundGateway
  InboundGateway(tunnel_id_t id, tunnel_id_t next_id, info_t::shared_ptr info)
      : base_t(id, next_id, info, base_t::BloomFlag::Enable)
  {
  }

  /// @brief Initializing ctor, creates a tunnel InboundEndpoint for a TransitTunnel
  /// @param id Tunnel ID (non-zero) to receive messages
  /// @param next_id Tunnel ID (non-zero) for the next Hop
  /// @param layer Layer encryption created during TransitTunnel construction
  InboundGateway(tunnel_id_t id, tunnel_id_t next_id, std::unique_ptr<layer_t> layer) : base_t(id, next_id, std::move(layer)) {}

  // Deleted functions unused by an InboundGateway
  decltype(auto) ExtractI2NPFragments(tunnel_message_t&) = delete;
  decltype(auto) CreateBuildMessage() = delete;
  decltype(auto) PreprocessAESBuildMessage(typename base_t::build_message_t&) = delete;
  decltype(auto) PreprocessChaChaBuildMessage(typename base_t::build_message_t&) = delete;
  decltype(auto) HandleBuildReply(typename base_t::aes_reply_message_t&) = delete;
  decltype(auto) HandleBuildReply(typename base_t::chacha_reply_message_t&) = delete;
  decltype(auto) DecryptAEAD(tunnel_message_t&) = delete;
  decltype(auto) IterativeDecryption(tunnel_message_t&) = delete;
  decltype(auto) reply_lease() const noexcept = delete;
  decltype(auto) hops() const noexcept = delete;
  decltype(auto) hops() noexcept = delete;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_INBOUND_GATEWAY_H_
