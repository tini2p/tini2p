/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TUNNELS_FRAGMENTED_MESSAGE_H_
#define SRC_TUNNELS_FRAGMENTED_MESSAGE_H_

#include <map>
#include <mutex>
#include <optional>
#include <shared_mutex>

#include "src/bytes.h"

#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"

#include "src/data/i2np/tunnel_delivery.h"

namespace tini2p
{
namespace tunnels
{
/// @class FragmentedMessage
/// @brief For processing and recombining fragmented I2NP messages
class FragmentedMessage
{
 public:
  enum
  {
    MaxFragID = 63,
    MinTunnelID = 1,
    MaxTunnelID = 4294967295,
  };

  enum struct IsLast : bool
  {
    False,
    True,
  };

  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using to_hash_t = crypto::Sha256::digest_t;  //< ToHash trait alias
  using delivery_t = data::FirstDelivery::delivery_t;  //< Delivery type trait alias
  using frag_id_t = std::uint8_t;  //< Fragment ID trait alias
  using frag_buffer_t = crypto::SecBytes;  //< Fragment buffer trait alias
  using fragments_t = std::map<frag_id_t, frag_buffer_t>;  //< Fragments collection trait alias
  using is_last_t = IsLast;  //< Is last fragment trait alias
  using i2np_buffer_t = crypto::SecBytes;  //< Completed I2NP message buffer trait alias
  using size_type = std::uint16_t;  //< Size type trait alias

  /// @brief Default ctor, creates an empty fragmented message
  FragmentedMessage() : last_fragment_(0), fragments_() {}

  /// @brief Create a fragmented message with an I2NP message fragment
  /// @param frag_id Fragment ID (0-63)
  /// @param frag_buf Message fragment buffer
  /// @param is_last Last fragment flag
  FragmentedMessage(frag_id_t frag_id, frag_buffer_t frag_buf, const is_last_t& is_last = is_last_t::False)
      : last_fragment_(0), fragments_()
  {
    const exception::Exception ex{"FragmentedMessage", __func__};

    check_frag_id(frag_id, is_last, ex);
    check_frag_buffer(frag_buf, ex);

    if (is_last == is_last_t::True)
      last_fragment_ = frag_id;

    fragments_.try_emplace(std::forward<frag_id_t>(frag_id), std::forward<frag_buffer_t>(frag_buf));
  }

  /// @brief Copy-ctor
  FragmentedMessage(const FragmentedMessage& oth) : last_fragment_(oth.last_fragment_), fragments_(oth.fragments_) {}

  /// @brief Move-ctor
  FragmentedMessage(FragmentedMessage&& oth)
      : last_fragment_(std::move(oth.last_fragment_)), fragments_(std::move(oth.fragments_))
  {
  }

  /// @brief Forwarding assignment operator
  FragmentedMessage& operator=(FragmentedMessage&& oth)
  {
    last_fragment_ = std::forward<frag_id_t>(oth.last_fragment_);

    std::scoped_lock sgd(fragments_mutex_);
    fragments_ = std::forward<fragments_t>(oth.fragments_);

    return *this;
  }

  /// @brief Add a message fragment
  /// @detail Caller is responsible for checking if message fragment already exists
  /// @param frag_id Fragment ID (0-63)
  /// @param frag_buf Message fragment buffer
  /// @param is_last Last fragment flag
  /// @throws Logic error if the fragment is already present (replay)
  /// @returns One if the message is completed, zero otherwise
  std::uint8_t AddFragment(frag_id_t frag_id, frag_buffer_t frag_buf, const is_last_t& is_last = is_last_t::False)
  {
    const exception::Exception ex{"FragmentedMessage", __func__};

    std::scoped_lock sgd(fragments_mutex_);

    check_frag_buffer(frag_buf, ex);
    check_additional_fragment(fragments_, frag_id, ex);

    if (is_last == is_last_t::True)
      last_fragment_ = frag_id;

    fragments_.try_emplace(std::forward<frag_id_t>(frag_id), std::forward<frag_buffer_t>(frag_buf));

    return check_message_completed(fragments_);
  }

  /// @brief Check if a fragment is present
  /// @param frag_id Fragment ID (0-63)
  /// @returns One if the fragment is found, zero otherwise
  std::uint8_t FindFragment(const frag_id_t& frag_id)
  {
    std::shared_lock sf(fragments_mutex_, std::defer_lock);
    std::scoped_lock sgd(sf);

    return static_cast<std::uint8_t>(fragments_.find(frag_id) != fragments_.end());
  }

  /// @brief Check if all I2NP message fragments are present
  /// @returns One if the all message fragments are found, zero otherwise
  std::uint8_t CheckMessageCompleted()
  {
    std::shared_lock sf(fragments_mutex_, std::defer_lock);
    std::scoped_lock sgd(sf);

    return check_message_completed(fragments_);
  }

  /// @brief Complete the I2NP message
  /// @detail Caller is responsible for checking that the message is complete
  /// @throws Logic error if message is incomplete (missing fragments)
  i2np_buffer_t CompleteMessage()
  {
    const exception::Exception ex{"FragmentedMessage", __func__};

    std::scoped_lock sgd(fragments_mutex_);

    if (check_message_completed(fragments_) == 0)
      ex.throw_ex<std::logic_error>("missing message fragments.");

    crypto::SecBytes i2np_buf;
    i2np_buf.resize(size(fragments_));

    tini2p::BytesWriter<crypto::SecBytes> writer(i2np_buf);

    // write all message fragments to buffer
    for (const auto& fragment : fragments_)
      writer.write_data(std::get<frag_buffer_t>(fragment));

    // reset to default state
    fragments_.clear();
    last_fragment_ = 0;

    return i2np_buf;
  }

  /// @brief Get a const reference to the delivery type
  const delivery_t& delivery() const noexcept
  {
    return delivery_;
  }

  /// @brief Set the delivery type
  FragmentedMessage& delivery(delivery_t delivery)
  {
    const exception::Exception ex{"Tunnel: FragmentedMessage", __func__};

    check_delivery(delivery, ex);

    delivery_ = std::forward<delivery_t>(delivery);

    return *this;
  }

  /// @brief Get a const reference to the tunnel ID
  const tunnel_id_t& tunnel_id() const noexcept
  {
    return id_;
  }

  /// @brief Set the tunnel ID
  FragmentedMessage& tunnel_id(tunnel_id_t id)
  {
    const exception::Exception ex{"Tunnel: FragmentedMessage", __func__};

    check_tunnel_id(id, ex);

    id_ = std::forward<tunnel_id_t>(id);

    return *this;
  }

  /// @brief Get a const reference to the to hash
  const to_hash_t& to_hash() const
  {
    const exception::Exception ex{"Tunnel: FragmentedMessage", __func__};

    check_to_hash(to_hash_, ex);

    return *to_hash_;
  }

  /// @brief Set the to hash
  FragmentedMessage& to_hash(to_hash_t hash)
  {
    const exception::Exception ex{"Tunnel: FragmentedMessage", __func__};

    if (hash.is_zero())
      ex.throw_ex<std::invalid_argument>("null to hash.");

    to_hash_.emplace(std::forward<to_hash_t>(hash));

    return *this;
  }

 private:
  size_type size(const fragments_t& fragments) const
  {
    size_type len(0);
    for (const auto& fragment : fragments)
      len += fragment.second.size();

    return len;
  }

  void check_frag_id(const frag_id_t& frag_id, const is_last_t& is_last, const exception::Exception& ex) const
  {
    const auto& max_frag_id = tini2p::under_cast(MaxFragID);
    if (frag_id > max_frag_id)
      ex.throw_ex<std::logic_error>("invalid fragment number: " + std::to_string(frag_id)
      + ", max: " + std::to_string(max_frag_id));

    if (frag_id == max_frag_id && last_fragment_ != frag_id)
      ex.throw_ex<std::logic_error>("fragment ID: " + std::to_string(frag_id) + " must be last.");
  }

  void check_frag_buffer(const frag_buffer_t& frag_buf, const exception::Exception& ex) const
  {
    if (frag_buf.empty())
      ex.throw_ex<std::logic_error>("empty message fragment.");
  }

  void check_additional_fragment(const fragments_t& fragments, const frag_id_t& frag_id, const exception::Exception& ex)
      const
  {
    if (fragments.find(frag_id) != fragments.end())
      ex.throw_ex<std::logic_error>("message fragment replay for fragment ID: " + std::to_string(frag_id));
  }

  std::uint8_t check_message_completed(const fragments_t& fragments) const
  {
    std::uint8_t all_fragments(1);
    for (frag_id_t id = 0; id < last_fragment_; ++id)
      all_fragments *= static_cast<std::uint8_t>(fragments_.find(id) != fragments_.end());

    return all_fragments;
  }

  void check_delivery(const delivery_t& delivery, const exception::Exception& ex) const
  {
    if (delivery != delivery_t::Local && delivery != delivery_t::Router && delivery != delivery_t::Tunnel)
      ex.throw_ex<std::invalid_argument>("invalid delivery type: " + std::to_string(tini2p::under_cast(delivery)));
  }

  void check_tunnel_id(const tunnel_id_t& id, const exception::Exception& ex) const
  {
    const auto& min_id = tini2p::under_cast(MinTunnelID);
    const auto& max_id = tini2p::under_cast(MaxTunnelID);

    if (id < min_id || id > max_id)
      {
        ex.throw_ex<std::logic_error>(
            "invalid tunnel ID: " + std::to_string(id) + ", min: " + std::to_string(min_id)
            + ", max: " + std::to_string(max_id));
      }
  }

  void check_to_hash(const std::optional<to_hash_t>& hash, const exception::Exception& ex) const
  {
    if (hash == std::nullopt)
      ex.throw_ex<std::runtime_error>("null to hash.");

    if (hash->is_zero())
      ex.throw_ex<std::logic_error>("zeroed to hash.");
  }

  tunnel_id_t id_;
  std::optional<to_hash_t> to_hash_;
  delivery_t delivery_;
  frag_id_t last_fragment_;
  fragments_t fragments_;
  std::shared_mutex fragments_mutex_;
};
}  // namespace tunnels
}  // namespace tini2p

#endif  // SRC_TUNNELS_FRAGMENTED_MESSAGE_H_
