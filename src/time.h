/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_TIME_H_
#define SRC_TIME_H_

#include <chrono>

#include <date/date.h>

namespace tini2p
{
namespace time
{
enum : std::uint32_t
{
  LagDelta = 120,
  MaxLagDelta = 3 * LagDelta,
};

/// @brief Get current time in hours
inline std::uint32_t ms_to_seconds(const std::uint64_t& ms)
{
  return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::milliseconds(ms)).count();
}

/// @brief Get current time in hours
inline std::uint64_t seconds_to_ms(const std::uint32_t& s)
{
  return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::seconds(s)).count();
}

/// @brief Get current time in hours
inline std::uint32_t now_hr()
{
  return std::chrono::duration_cast<std::chrono::hours>(std::chrono::system_clock::now().time_since_epoch()).count();
}

/// @brief Get current time in minutes
inline std::uint32_t now_min()
{
  return std::chrono::duration_cast<std::chrono::minutes>(std::chrono::system_clock::now().time_since_epoch()).count();
}

/// @brief Get current time in seconds
inline std::uint32_t now_s()
{
  return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

/// @brief Get current time in milliseconds
inline std::uint64_t now_ms()
{
  return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch())
      .count();
}

/// @brief Get current date as ASCII date-string (yyyyMMdd)
inline std::string now_date()
{
  std::ostringstream os;
  date::to_stream(os, "%Y%m%d", date::floor<date::days>(std::chrono::system_clock::now()));
  return os.str();
}

/// @brief Check if timestamp delta (seconds) is within valid range
/// @param time Timestamp to check
inline std::uint8_t check_lag_s(const std::uint32_t time)
{
  return static_cast<std::uint8_t>(now_s() - time <= MaxLagDelta);
}
}  // namespace time
}  // namespace tini2p

#endif  // SRC_TIME_H_
