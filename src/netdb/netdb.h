/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_NETDB_NETDB_H_
#define SRC_NETDB_NETDB_H_

#include <deque>
#include <mutex>
#include <shared_mutex>

#include "src/data/router/info.h"
#include "src/data/router/lease_set.h"

#include "src/data/i2np/database_store.h"
#include "src/data/i2np/database_lookup.h"
#include "src/data/i2np/database_search_reply.h"
#include "src/data/i2np/delivery_status.h"
#include "src/data/i2np/i2np_message.h"

#include "src/netdb/kademlia.h"

namespace tini2p
{
namespace netdb
{
/// @class NetDB
class NetDB
{
 public:
  enum
  {
    DefaultPeersLen = 3,  //< default closest peers length, see spec
    MaxRouterInfos = 1024,  //< arbitrary max, adjust for performance/security
    MaxFloodfills = 1024,  //< arbitrary max, adjust for performance/security
    MaxLeaseSets = 2048,  //< arbitrary max, adjust for performance/security
    MaxUnsupportedRI = 25,
    MaxUnsupportedLS = 25,
  };

  using info_t = data::Info;  //< RouterInfo trait alias
  using lease_set_t = data::LeaseSet2;  //< LeaseSet2 trait alias
  using db_lookup_t = data::DatabaseLookup;  //< DatabaseLookup trait alias
  using db_store_t = data::DatabaseStore;  //< DatabaseStore trait alias
  using db_search_reply_t = data::DatabaseSearchReply;  //< DatabaseSearchReply trait alias
  using lookup_reply_v = std::variant<db_store_t, db_search_reply_t>;  //< DatabaseLookup reply trait alias
  using router_infos_t = std::deque<info_t::shared_ptr>;  //< RouterInfo storage trait alias
  using floodfills_t = std::vector<info_t::shared_ptr>;  //< Floodfill storage trait alias
  using lease_sets_t = std::deque<lease_set_t::shared_ptr>;  //< LeaseSet2 storage trait alias
  using peers_t = std::vector<crypto::Sha256::digest_t>;  //< Peers trait alias

  /// @brief Create a NetDB
  /// @param own_info Shared pointer to our RouterInfo
  NetDB(data::Info::shared_ptr own_info) : own_info_(own_info), infos_(), floodfills_(), lease_sets_()
  {
    const exception::Exception ex{"NetDB", __func__};

    check_info(own_info_, ex);
  }

  /// @brief Handle storing entries from a DatabaseStore message
  /// @detail Stores only supported entries, and relays verifiable unsupported entries
  /// @note Caller is responsible for sending appropriate reply message
  /// @param msg DatabaseStore message to handle
  /// @throw Logic error for type mismatch, and invalid entry types
  NetDB& HandleDatabaseStore(const data::DatabaseStore& msg)
  {
    const exception::Exception ex{"NetDB", __func__};

    // check that the message holds appropriate data entry, and the entry passes signature verification
    if (!msg.Verify())
      ex.throw_ex<std::invalid_argument>("DatabaseStore message failed verification.");

    const auto type = msg.type();
    const auto& entry = msg.entry_data();

    if (msg.locally_reachable())
      {
        if (type == db_store_t::type_t::RouterInfo)
          {
            auto info_ptr = std::get<data::Info::shared_ptr>(entry);
            if (info_ptr->is_floodfill())
              {
                std::scoped_lock fgd(floodfill_mutex_);

                if (floodfills_.size() < MaxFloodfills)
                  floodfills_.push_back(info_ptr);
              }
            else
              {
                std::scoped_lock igd(info_mutex_);

                if (infos_.size() < MaxRouterInfos)
                  infos_.push_back(info_ptr);
              }
          }
        else if (type == db_store_t::type_t::LeaseSet2)
          {
            std::scoped_lock lgd(ls_mutex_);

            if (lease_sets_.size() < MaxLeaseSets)
              lease_sets_.push_back(std::get<lease_set_t::shared_ptr>(entry));
          }
        else
          ex.throw_ex<std::logic_error>("invalid data type.");
      }
    else
      {
        if (type == db_store_t::type_t::RouterInfo)
          {
            std::scoped_lock uigd(uninfo_mutex_);

            if (unsupported_infos_.size() < MaxUnsupportedRI)
              unsupported_infos_.push_back(std::get<data::Info::shared_ptr>(entry));
          }
        else if (type == db_store_t::type_t::LeaseSet2)
          {
            std::scoped_lock ulgd(unls_mutex_);

            if (unsupported_lease_sets_.size() < MaxUnsupportedLS)
              unsupported_lease_sets_.push_back(std::get<lease_set_t::shared_ptr>(entry));
          }
        else
          ex.throw_ex<std::logic_error>("invalid data type.");
      }

    return *this;
  }

  /// @brief Handle received DatabaseLookup message
  /// @detail Searches local RouterInfo & LeaseSet storage for search match.
  ///     Does not consider unsupported entries for matches.
  /// @note Essentialy a const function, but needs non-const to lock mutexes for thread safety
  /// @note Caller is responsible for sending appropriate reply message
  /// @param msg DatabaseLookup message to handle
  /// @return Reply variant that will contain a DatabaseStore message if entry found, or DatabaseSearchReply if not
  data::I2NPMessage HandleDatabaseLookup(const data::DatabaseLookup& msg)
  {
    const auto& key = msg.search_key();
    const auto& lookup = msg.lookup_flags();
    bool unfound = false;

    std::optional<data::I2NPMessage> reply;
    std::uint32_t msg_id(crypto::RandInRange(data::I2NPHeader::MinMsgID, data::I2NPHeader::MaxMsgID));

    {  // ri ff ls lock
      std::shared_lock igd(info_mutex_, std::defer_lock);
      std::shared_lock fgd(floodfill_mutex_, std::defer_lock);
      std::shared_lock lgd(ls_mutex_, std::defer_lock);
      std::scoped_lock sgd(igd, fgd, lgd);

      const auto ri_iter =
          std::find_if(infos_.begin(), infos_.end(), [&key](const auto& e) { return e->hash() == key; });
      const auto ri_found = ri_iter != infos_.end();

      const auto ff_iter =
          std::find_if(floodfills_.begin(), floodfills_.end(), [&key](const auto& e) { return e->hash() == key; });
      const auto ff_found = ff_iter != floodfills_.end();

      const auto ls_iter =
          std::find_if(lease_sets_.begin(), lease_sets_.end(), [&key](const auto& e) { return e->hash() == key; });
      const auto ls_found = ls_iter != lease_sets_.end();

      if (lookup == data::DatabaseLookup::LookupFlags::Normal)
        {
          if (ri_found)
            reply.emplace(data::I2NPHeader::Type::DatabaseStore, msg_id, db_store_t(*ri_iter).buffer());
          else if (ff_found)
            reply.emplace(data::I2NPHeader::Type::DatabaseStore, msg_id, db_store_t(*ff_iter).buffer());
          else if (ls_found)
            reply.emplace(data::I2NPHeader::Type::DatabaseStore, msg_id, db_store_t(*ls_iter).buffer());
          else
            unfound = true;
        }
      else if (lookup == data::DatabaseLookup::LookupFlags::LS)
        {
          if (ls_found)
            reply.emplace(data::I2NPHeader::Type::DatabaseStore, msg_id, db_store_t(*ls_iter).buffer());
          else
            unfound = true;
        }
      else if (lookup == data::DatabaseLookup::LookupFlags::RI)
        {
          if (ri_found)
            reply.emplace(data::I2NPHeader::Type::DatabaseStore, msg_id, db_store_t(*ri_iter).buffer());
          else if (ff_found)
            reply.emplace(data::I2NPHeader::Type::DatabaseStore, msg_id, db_store_t(*ff_iter).buffer());
          else
            unfound = true;
        }
    }  // end-ri-ff-ls-lock

    if (lookup == data::DatabaseLookup::LookupFlags::Explore || unfound)
      {
        reply.emplace(
            data::I2NPHeader::Type::DatabaseSearchReply,
            msg_id,
            db_search_reply_t(key, own_info_->hash(), ClosestPeers(key, lookup, msg.excluded_peers())).buffer());
      }

    return *reply;
  }

  /// @brief Return a DatabaseLookup message for the DatabaseSearchReply search key
  data::I2NPMessage HandleDatabaseSearchReply(const db_search_reply_t& db_reply)
  {
    const exception::Exception ex{"NetDB", __func__};

    check_info(own_info_, ex);

    return data::I2NPMessage(
        data::I2NPHeader::Type::DatabaseLookup,
        crypto::RandInRange(data::I2NPHeader::MinMsgID, data::I2NPHeader::MaxMsgID),
        db_lookup_t(db_reply.search_key(), own_info_->identity().hash()).buffer());
  }

  /// @brief Get a const reference to the non-floodfill RouterInfos
  const router_infos_t& infos() const noexcept
  {
    return infos_;
  }

  /// @brief Get a const reference to the floodfill RouterInfos
  const floodfills_t& floodfills() const noexcept
  {
    return floodfills_;
  }

  /// @brief Manually add a RouterInfo to the NetDB
  /// @detail Used when loading RouterInfos from disk
  NetDB& AddInfo(info_t::shared_ptr info)
  {
    const exception::Exception ex{"NetDB", __func__};

    check_info(info, ex);

    if (info->is_floodfill())
      {
        std::scoped_lock sgd(floodfill_mutex_);

        check_additional_floodfill(floodfills_, ex);
        floodfills_.emplace_back(std::forward<info_t::shared_ptr>(info));
      }
    else
      {
        std::scoped_lock sgd(info_mutex_);

        check_additional_info(infos_, ex);
        infos_.emplace_back(std::forward<info_t::shared_ptr>(info));
      }

    return *this;
  }

  /// @brief Get whether the NetDB contains a RouterInfo with the given Identity hash
  std::uint8_t has_info(const info_t::identity_t::hash_t& ident_hash)
  {
    std::shared_lock is(info_mutex_, std::defer_lock);
    std::shared_lock fs(floodfill_mutex_, std::defer_lock);
    std::scoped_lock sgd(is, fs);

    return has_info(infos_, ident_hash) + has_info(floodfills_, ident_hash);
  }

  /// @brief Find a RouterInfo in the NetDB
  /// @detail May return a floodfill or non-floodfill RouterInfo
  /// @throws Runtime error if no RouterInfo matching the Identity hash exists in the NetDB
  /// @returns Shared pointer to the found RouterInfo
  info_t::shared_ptr find_info(const info_t::identity_t::hash_t& ident_hash)
  {
    const exception::Exception& ex{"NetDB", __func__};

    std::shared_lock is(info_mutex_, std::defer_lock);
    std::shared_lock fs(floodfill_mutex_, std::defer_lock);
    std::scoped_lock sgd(is, fs);

    info_t::shared_ptr ret_info;

    if (has_info(infos_, ident_hash))
      ret_info = find_info(infos_, ident_hash, ex);
    else
      ret_info = find_info(floodfills_, ident_hash, ex);

    return ret_info;
  }

  /// @brief Get a const reference to the LeaseSets
  const lease_sets_t& lease_sets() const noexcept
  {
    return lease_sets_;
  }

  /// @brief Manually add a LeaseSet2 to the NetDB
  /// @detail Used when loading LeaseSet2s from disk
  NetDB& AddLeaseSet(lease_set_t::shared_ptr lease_set)
  {
    const exception::Exception ex{"NetDB", __func__};

    check_lease_set(lease_set, ex);

    std::scoped_lock sgd(ls_mutex_);

    check_additional_lease_set(lease_sets_, ex);
    lease_sets_.emplace_back(std::forward<lease_set_t::shared_ptr>(lease_set));

    return *this;
  }

  /// @brief Get whether the NetDB has a LeaseSet for the given Destination hash
  std::uint8_t has_lease_set(const lease_set_t::destination_t::hash_t& dest_hash)
  {
    std::shared_lock ls(ls_mutex_, std::defer_lock);
    std::scoped_lock sgd(ls);

    return has_lease_set(lease_sets_, dest_hash);
  }

  /// @brief Find a LeaseSet in the NetDB
  /// @throws Runtime error if no LeaseSet matching the Destination hash exists in the NetDB
  /// @returns Shared pointer to the found LeaseSet
  lease_set_t::shared_ptr find_lease_set(const lease_set_t::destination_t::hash_t& dest_hash)
  {
    const exception::Exception ex{"NetDB", __func__};

    std::shared_lock ls(ls_mutex_, std::defer_lock);
    std::scoped_lock sgd(ls);

    return find_lease_set(lease_sets_, dest_hash, ex);
  }

  /// @brief Get the peers closest to the search key
  /// @note Essentially a const function, but needs non-const to lock mutexes for thread safety
  /// @param key Search key (Sha256 of RouterIdentity/Destination, not RoutingKey)
  /// @param flag Lookup flag for the search (Explore lookup only returns non-floodfill peers)
  peers_t ClosestPeers(
      const data::DatabaseLookup::search_key_t& key,
      const data::DatabaseLookup::LookupFlags& flag,
      const std::vector<db_lookup_t::ex_peer_key_t> ex_peers = {})
  {
    using close_peers_t = std::map<Kademlia::metric_t, Kademlia::search_key_t>;

    Kademlia::routing_key_t routing_key;
    Kademlia::CreateRoutingKey(key, routing_key);

    close_peers_t nonff_peers;
    close_peers_t ff_peers;

    const auto not_excluded = [&ex_peers](const auto& k) {
      return std::find(ex_peers.begin(), ex_peers.end(), k) == ex_peers.end();
    };

    {  // ri ff lock
      std::shared_lock igd(info_mutex_, std::defer_lock);
      std::shared_lock fgd(floodfill_mutex_, std::defer_lock);
      std::scoped_lock sgd(igd, fgd);

      /// order the closest non-floodfill peers
      for (const auto& ri : infos_)
        {
          const auto& key = ri->hash();
          Kademlia::metric_t metric;
          Kademlia::XOR(routing_key, key, metric);

          if (not_excluded(key))
            nonff_peers.try_emplace(std::move(metric), key);
        }

      /// order the closest floodfill peers
      for (const auto& ff : floodfills_)
        {
          const auto& key = ff->hash();
          Kademlia::metric_t metric;
          Kademlia::XOR(routing_key, key, metric);

          if (not_excluded(key))
            ff_peers.emplace(std::move(metric), key);
        }
    }  // end-ri-ff-lock

    // if this is an Explore lookup, only consider non-floodfill peers.
    // otherwise, merge all peers into the closest peers list
    if (flag != data::DatabaseLookup::LookupFlags::Explore)
      nonff_peers.merge(ff_peers);  // add floodfill peers to non-floodfill peers

    // get the number of peers to return
    // temporary assignment necessary for memory safety of std::min return type (returns `const T&`)
    const auto& default_peers = static_cast<std::size_t>(DefaultPeersLen);
    const auto& available_peers = nonff_peers.size();
    const auto& num_peers = std::min(default_peers, available_peers);

    peers_t peers;
    peers.reserve(num_peers);
    std::uint8_t n = 0;

    // add the closest `n` peers to return list
    for (auto& peer : nonff_peers)
      {
        if (n == num_peers)
          break;

        peers.emplace_back(std::move(std::get<Kademlia::search_key_t>(peer)));
        ++n;
      }

    return peers;
  }

 private:
  void check_info(const info_t::shared_ptr& info, const exception::Exception& ex) const
  {
    if (info == nullptr)
      ex.throw_ex<std::invalid_argument>("null RouterInfo");

    if (info->Verify() == 0)
      ex.throw_ex<std::logic_error>("RouterInfo failed verification");
  }

  void check_lease_set(const lease_set_t::shared_ptr& lease_set, const exception::Exception& ex) const
  {
    if (lease_set == nullptr)
      ex.throw_ex<std::invalid_argument>("null LeaseSet2");

    if (lease_set->Verify() == 0)
      ex.throw_ex<std::logic_error>("LeaseSet2 failed verification");
  }

  void check_additional_info(const router_infos_t& infos, const exception::Exception& ex) const
  {
    if (infos.size() >= tini2p::under_cast(MaxRouterInfos))
      ex.throw_ex<std::runtime_error>("maximum RouterInfos reached");
  }

  void check_additional_floodfill(const floodfills_t& floodfills, const exception::Exception& ex) const
  {
    if (floodfills.size() >= tini2p::under_cast(MaxFloodfills))
      ex.throw_ex<std::runtime_error>("maximum Floodfills reached");
  }

  void check_additional_lease_set(const lease_sets_t& lease_sets, const exception::Exception& ex) const
  {
    if (lease_sets.size() >= tini2p::under_cast(MaxLeaseSets))
      ex.throw_ex<std::runtime_error>("maximum LeaseSets reached");
  }

  template <class TInfos>
  std::uint8_t has_info(const TInfos& infos, const info_t::identity_t::hash_t& ident_hash)
  {
    const auto infos_end = infos.end();

    return static_cast<std::uint8_t>(
        std::find_if(
            infos.begin(), infos_end, [&ident_hash](const auto& i) { return i && i->identity().hash() == ident_hash; })
        != infos_end);
  }

  template <class TInfos>
  info_t::shared_ptr
  find_info(TInfos& infos, const info_t::identity_t::hash_t& ident_hash, const exception::Exception& ex) const
  {
    const auto infos_end = infos.end();

    auto it = std::find_if(
        infos.begin(), infos_end, [&ident_hash](const auto& i) { return i && i->identity().hash() == ident_hash; });

    if (it == infos_end)
      ex.throw_ex<std::runtime_error>("no RouterInfo found for Identity hash: " + tini2p::bin_to_hex(ident_hash, ex));

    return *it;
  }

  std::uint8_t has_lease_set(const lease_sets_t& lease_sets, const lease_set_t::destination_t::hash_t& dest_hash) const
  {
    const auto ls_end = lease_sets.end();

    return static_cast<std::uint8_t>(
        std::find_if(
            lease_sets.begin(),
            ls_end,
            [&dest_hash](const auto& ls) { return ls && ls->destination().hash() == dest_hash; })
        != ls_end);
  }

  lease_set_t::shared_ptr find_lease_set(
      lease_sets_t& lease_sets,
      const lease_set_t::destination_t::hash_t& dest_hash,
      const exception::Exception& ex) const
  {
    const auto ls_end = lease_sets.end();

    auto it = std::find_if(lease_sets.begin(), ls_end, [&dest_hash](const auto& ls) {
      return ls && ls->destination().hash() == dest_hash;
    });

    if (it == ls_end)
      ex.throw_ex<std::runtime_error>("no LeaseSet found for Destination hash: " + tini2p::bin_to_hex(dest_hash, ex));

    return *it;
  }

  data::Info::shared_ptr own_info_;

  router_infos_t infos_;
  std::shared_mutex info_mutex_;

  floodfills_t floodfills_;
  std::shared_mutex floodfill_mutex_;

  std::vector<data::Info::shared_ptr> unsupported_infos_;
  std::shared_mutex uninfo_mutex_;

  lease_sets_t lease_sets_;
  std::shared_mutex ls_mutex_;

  std::vector<data::LeaseSet2::shared_ptr> unsupported_lease_sets_;
  std::shared_mutex unls_mutex_;
};
}  // namespace netdb
}  // namespace tini2p

#endif  // SRC_NETDB_NETDB_H_
