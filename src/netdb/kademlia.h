/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_NETDB_KADEMLIA_H_
#define SRC_NETDB_KADEMLIA_H_

#include <date/date.h>

#include "src/bytes.h"
#include "src/time.h"
#include "src/exception/exception.h"

#include "src/crypto/keys.h"
#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"

namespace tini2p
{
namespace netdb
{
struct Kademlia
{
  enum
  {
    DateLen = 8,
    RoutingKeyLen = crypto::Sha256::DigestLen,
    MetricLen = crypto::Sha256::DigestLen,
  };

  struct RoutingKey : public crypto::Key<RoutingKeyLen>
  {
    using base_t = crypto::Key<RoutingKeyLen>;

    RoutingKey() : base_t() {}

    RoutingKey(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

    RoutingKey(const crypto::SecBytes& buf) : base_t(buf) {}

    RoutingKey(std::initializer_list<std::uint8_t> list) : base_t(list) {}
  };

  using date_t = crypto::FixedSecBytes<DateLen>;  //< Date trait alias
  using routing_key_t = RoutingKey;  //< Routing key trait alias
  using search_key_t = crypto::Sha256::digest_t;  //< Search key trait alias
  using metric_t = crypto::Key<MetricLen>;  //< Metric trait alias

  static void CreateRoutingKey(const crypto::Sha256::digest_t& search_key, routing_key_t& routing_key)
  {
    crypto::FixedSecBytes<RoutingKeyLen + DateLen> search_key_date;

    // get current date as ASCII date-string: yyyyMMdd
#if defined(TINI2P_TESTS)
    date_t date(reinterpret_cast<const std::uint8_t*>("20030401"), DateLen);
#else
    date_t date(reinterpret_cast<const std::uint8_t*>(tini2p::time::now_date().data()), DateLen);
#endif

    // write input for left routing key: SearchKey || Date
    tini2p::BytesWriter<decltype(search_key_date)> writer(search_key_date);
    writer.write_data(search_key);
    writer.write_data(date);

    // calculate routing key: H(SearchKey || Date)
    crypto::Sha256::Hash(routing_key.buffer(), search_key_date);
  }

  /// @brief Calculate Kademlia XOR distance metric
  /// @param routing_key Routing key of the entry being stored/looked up 
  /// @param entry Search key (Identity/Destination hash) to find closest router in the DHT neighborhood
  static void XOR(const routing_key_t& routing_key, const search_key_t& entry, metric_t& metric)
  {  // calculate routing key ^ entry search key
    metric = routing_key.buffer() ^ entry;
  }
};
}  // namespace netdb
}  //namespace tini2p

#endif  // SRC_NETDB_KADEMLIA_H_
