/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ROUTER_CONTEXT_H_
#define SRC_ROUTER_CONTEXT_H_

#include "src/ecies/session_manager.h"
#include "src/netdb/netdb.h"
#include "src/ntcp2/session/manager.h"
#include "src/tunnels/pool_manager.h"

namespace tini2p
{
namespace router
{
/// @struct SearchEntry
/// @brief Convenience struct for tracking search entry lookup attempts
/// @detail After max attempts, no further lookups are performed for the entry.
///      Entry is cleared from the cache after expiration, allowing more lookup attempts.
///      The goal is to prevent infinite lookup for the same, possibly invalid, entry.
struct SearchEntry
{
  enum : std::uint16_t
  {
    MaxAttempts = 3,
    Expiration = 600,  //< in seconds, reset attempts and clear from search cache
  };

  /// @brief Default ctor
  SearchEntry() : search_key(), attempts(0), expiration(0) {}

  /// @brief Fully-initializing ctor
  SearchEntry(crypto::Sha256::digest_t search, std::uint8_t attempt_num, std::uint32_t expires)
      : search_key(std::forward<crypto::Sha256::digest_t>(search)),
        attempts(std::forward<std::uint8_t>(attempt_num)),
        expiration(std::forward<std::uint32_t>(expires))
  {
  }

  crypto::Sha256::digest_t search_key;
  std::uint8_t attempts;
  std::uint32_t expiration;
};

/// @class Context
/// @brief Class for coordinating various router components
class Context
{
 public:
  enum : std::uint16_t
  {
    CheckListenerTimeout = 45,  //< in milliseconds, check listener sessions for messages
    CheckRequestTimeout = 60,  //< in minutes, clear Bloom filter
    CheckSearchTimeout = 60,  //< in seconds, check for expired search entries
    ExpectedRequests = 3600,  //< Expected tunnel requests in an hour, adjust as necessary
    FalsePositiveRate = 100,  //< Allow 1-in-P false positives, adjust as necessary
    MinTunnelHops = 1,  //< Minimum hops (other than creator) allowed in a tunnel
    MaxTunnelHops = 8,  //< Maximum hops (other than creator) allowed in a tunnel
  };

  using curve_t = crypto::X25519;  //< Curve trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< ChaCha20-Poly1305 trait alias
  using info_t = data::Info;  //< RouterInfo trait alias
  using lease_set_t = data::LeaseSet2;  //< LeaseSet2 trait alias
  using destination_t = data::Destination;  //< Destination trait alias
  using ntcp2_manager_t = ntcp2::SessionManager;  //< NTCP2 SessionManager trait alias
  using ecies_manager_t = ecies::SessionManager;  //< ECIES-X25519 SessionManager trait alias
  using netdb_t = netdb::NetDB;  //< NetDB trait alias
  using aes_pool_manager_t = tunnels::PoolManager<crypto::TunnelAES>;  //< AES PoolManager trait alias
  using chacha_pool_manager_t = tunnels::PoolManager<crypto::TunnelChaCha>;  //< ChaCha PoolManager trait alias
  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using garlic_t = data::Garlic;  //< Garlic trait alias
  using io_context_t = asio::io_context;  //< IO context trait alias
  using bloom_filter_t = crypto::BloomFilter;  //< Bloom filter trait alias
  using timer_t = asio::steady_timer;  //< Timer trait alias
  using tcp_t = asio::ip::tcp;  //< TCP trait alias
  using error_c = asio::error_code;  //< ASIO error code trait alias
  using port_t = std::uint16_t;  //< Port trait alias

  /// @brief Default ctor, creates a RouterContext listening on all interfaces
  Context()
      : info_(std::make_shared<info_t>()),
        ntcp2_manager_(
            info_,
            tcp_t::endpoint(tcp_t::v4(), crypto::RandInRange(ntcp2_manager_t::MinPort, ntcp2_manager_t::MaxPort)),
            tcp_t::endpoint(tcp_t::v6(), crypto::RandInRange(ntcp2_manager_t::MinPort, ntcp2_manager_t::MaxPort))),
        aes_pool_manager_(info_),
        chacha_pool_manager_(info_),
        netdb_(info_),
        queue_(),
        bloom_(ExpectedRequests, FalsePositiveRate),
        search_(),
        bloom_ctx_(),
        ntcp2_ctx_(),
        search_ctx_(),
        bloom_timer_(),
        ntcp2_timer_(),
        bloom_timer_thread_(),
        ntcp2_timer_thread_(),
        search_timer_thread_()
  {
    RunBloomTimer();
    RunNTCP2Timer();
    RunSearchTimer();
  }

  /// @brief Create a RouterContext listening on a specific interface
  /// @param address Interface address (IPv4 or IPv6) to listen on for incoming connections
  Context(const std::string& address, const port_t& port = 0)
      : info_(std::make_shared<info_t>()),
        ntcp2_manager_(
            info_,
            tcp_t::endpoint(
                asio::ip::make_address(address),
                port == 0 ? crypto::RandInRange(ntcp2_manager_t::MinPort, ntcp2_manager_t::MaxPort) : port)),
        aes_pool_manager_(info_),
        chacha_pool_manager_(info_),
        netdb_(info_),
        queue_(),
        bloom_(ExpectedRequests, FalsePositiveRate),
        search_(),
        bloom_ctx_(),
        ntcp2_ctx_(),
        search_ctx_(),
        bloom_timer_(),
        ntcp2_timer_(),
        search_timer_(),
        bloom_timer_thread_(),
        ntcp2_timer_thread_(),
        search_timer_thread_()
  {
    RunBloomTimer();
    RunNTCP2Timer();
    RunSearchTimer();
  }

  /// @brief Create a RouterContext that listens on specific IPv4 and IPv6 interfaces
  /// @param address_v4 IPv4 address to listen for incoming connections
  /// @param address_v6 IPv6 address to listen for incoming connections
  /// @param port Optional port to listen for incoming connections, random port chosen if port == 0
  Context(const std::string& address_v4, const std::string& address_v6, const port_t& port = 0)
      : info_(std::make_shared<info_t>()),
        ntcp2_manager_(
            info_,
            tcp_t::endpoint(
                asio::ip::make_address(address_v4),
                port == 0 ? crypto::RandInRange(ntcp2_manager_t::MinPort, ntcp2_manager_t::MaxPort) : port),
            tcp_t::endpoint(
                asio::ip::make_address(address_v6),
                port == 0 ? crypto::RandInRange(ntcp2_manager_t::MinPort, ntcp2_manager_t::MaxPort) : port)),
        aes_pool_manager_(info_),
        chacha_pool_manager_(info_),
        netdb_(info_),
        queue_(),
        bloom_(ExpectedRequests, FalsePositiveRate),
        search_(),
        bloom_ctx_(),
        ntcp2_ctx_(),
        search_ctx_(),
        bloom_timer_(),
        ntcp2_timer_(),
        search_timer_(),
        bloom_timer_thread_(),
        ntcp2_timer_thread_(),
        search_timer_thread_()
  {
    RunBloomTimer();
    RunNTCP2Timer();
    RunSearchTimer();
  }

  /// @brief Dtor
  /// @detail Stop all timers
  ~Context()
  {
    StopBloomTimer();
    StopNTCP2Timer();
    StopSearchTimer();
  }

  /// @brief Handle incoming I2NP messages received through the NTCP2 transport
  Context& HandleI2NPMessage(data::I2NPMessage& message)
  {
    try
      {
        const exception::Exception ex{"Router: Context", __func__};

        const auto& msg_id = message.message_id();
        const auto& msg_type = message.type();

        if (msg_type == data::I2NPHeader::Type::Garlic)
          HandleGarlicMessage(message);
        else if (is_tunnel_message(message))
          HandleTunnelMessage(message);
        else if (msg_type == data::I2NPHeader::Type::DatabaseStore)
          {
            netdb_t::db_store_t db_store(std::move(message.extract_body()));
            const auto search_key = db_store.search_key();

            netdb_.HandleDatabaseStore(std::move(db_store));

            if (netdb_.has_info(search_key) || netdb_.has_lease_set(search_key))
              {
                std::scoped_lock sed(search_mutex_);

                search_.erase(
                    std::remove_if(
                        search_.begin(),
                        search_.end(),
                        [&search_key](const auto& s) { return s.search_key == search_key; }),
                    search_.end());
              }
          }
        else if (msg_type == data::I2NPHeader::Type::DatabaseLookup)
          {
            netdb_t::db_lookup_t db_lookup(message.extract_body());

            if (aes_pool_manager_.outbound_pool().has_tunnels() || chacha_pool_manager_.outbound_pool().has_tunnels())
              SendTunnelMessage(
                  netdb_.HandleDatabaseLookup(db_lookup), db_lookup.from_key(), db_lookup.reply_tunnel_id());
            else
              SendI2NPMessage(db_lookup.from_key(), netdb_.HandleDatabaseLookup(db_lookup));
          }
        else if (msg_type == data::I2NPHeader::Type::DatabaseSearchReply)
          HandleDatabaseSearchReply(message);
        else if (msg_type == data::I2NPHeader::Type::Data)
          {
            if (aes_pool_manager_.has_test(msg_id))
              aes_pool_manager_.HandleI2NPMessage(message);
            else if (chacha_pool_manager_.has_test(msg_id))
              chacha_pool_manager_.HandleI2NPMessage(message);
            else
              {  // put the data messages in a queue to send to an I2CP client
                std::scoped_lock qgd(queue_mutex_);
                queue_.emplace_back(std::move(message));
              }
          }
        else if (msg_type == data::I2NPHeader::Type::DeliveryStatus)
          {  // Not needed in current implementation, only used for out-of-band session confirmation in ElGamal/AES+SessionTags
            ex.throw_ex<std::runtime_error>("unimplemented");
          }
        else
          ex.throw_ex<std::invalid_argument>("invalid I2NP message type");
      }
    catch (const std::exception& ex)
      {
        std::cerr << ex.what() << std::endl;
      }

    return *this;
  }

  /// @brief Handle a decrypted Garlic message
  Context& HandleGarlicMessage(data::I2NPMessage& message)
  {
    try
      {
        const exception::Exception ex{"Router: Context", __func__};

        auto ecies_msg = ecies_manager_.HandleI2NPMessage(message);

        if (std::holds_alternative<ecies_manager_t::new_message_t>(ecies_msg))
          HandleGarlicPayload(std::get<ecies_manager_t::new_message_t>(ecies_msg).payload_section());
        else if (std::holds_alternative<ecies_manager_t::new_reply_t>(ecies_msg))
          HandleGarlicPayload(std::get<ecies_manager_t::new_reply_t>(ecies_msg).payload_section());
        else if (std::holds_alternative<ecies_manager_t::existing_message_t>(ecies_msg))
          HandleGarlicPayload(std::get<ecies_manager_t::existing_message_t>(ecies_msg));
        else
          ex.throw_ex<std::invalid_argument>("invalid ECIES Garlic message");
      }
    catch (const std::exception& ex)
      {
        std::cerr << ex.what() << std::endl;
      }

    return *this;
  }

  /// @brief Add an outbound tunnel
  /// @tparam TCrypt Tunnel layer encryption type for the outbound tunnel
  /// @param hop_infos RouterInfos for the hops in the tunnel
  template <class TCrypto>
  Context& AddOutboundTunnel(const std::vector<info_t::shared_ptr>& hop_infos)
  {
    try
      {
        const exception::Exception ex{"Router: Context", __func__};

        check_tunnel_infos(hop_infos, ex);

        std::optional<tini2p::data::Lease2> ls_opt;
        auto& aes_in_pool = aes_pool_manager_.inbound_pool();
        auto& chacha_in_pool = chacha_pool_manager_.inbound_pool();

        if (aes_in_pool.has_tunnels())
          {
            auto& tunnel = aes_in_pool.tunnel();
            ls_opt.emplace(tunnel.peer_hashes().front(), tunnel.tunnel_id());
          }
        else if (chacha_in_pool.has_tunnels())
          {
            auto& tunnel = chacha_in_pool.tunnel();
            ls_opt.emplace(tunnel.peer_hashes().front(), tunnel.tunnel_id());
          }
        else
          {
            if (chacha_in_pool.empty_zero_hops())
              {
                check_info(info_, ex);
                chacha_in_pool.CreateZeroHopTunnels(info_);
              }

            auto& zero_hop = chacha_in_pool.zero_hop();
            ls_opt.emplace(zero_hop.info().identity().hash(), zero_hop.tunnel_id());
          }

        std::optional<data::I2NPMessage> build_msg;

        if (std::is_same<TCrypto, crypto::TunnelAES>::value)
          build_msg.emplace(aes_pool_manager_.AddOutboundTunnel(std::move(*ls_opt), hop_infos));
        else if (std::is_same<TCrypto, crypto::TunnelChaCha>::value)
          build_msg.emplace(chacha_pool_manager_.AddOutboundTunnel(std::move(*ls_opt), hop_infos));
        else
          ex.throw_ex<std::invalid_argument>("invalid tunnel layer crypto");

        SendI2NPMessage(hop_infos.front()->identity().hash(), *build_msg);
      }
    catch (const std::exception& ex)
      {
        std::cerr << ex.what() << std::endl;
      }

    return *this;
  }

  /// @brief Add an inbound tunnel
  /// @tparam TCrypt Tunnel layer encryption type for the inbound tunnel
  /// @param hop_infos RouterInfos for the hops in the tunnel
  template <class TCrypto>
  Context& AddInboundTunnel(const std::vector<info_t::shared_ptr>& hop_infos)
  {
    try
      {
        const exception::Exception ex{"Router: Context", __func__};

        check_tunnel_infos(hop_infos, ex);

        std::optional<data::I2NPMessage> build_msg;

        if (std::is_same<TCrypto, crypto::TunnelAES>::value)
          build_msg.emplace(aes_pool_manager_.AddInboundTunnel(hop_infos));
        else if (std::is_same<TCrypto, crypto::TunnelChaCha>::value)
          build_msg.emplace(chacha_pool_manager_.AddInboundTunnel(hop_infos));
        else
          ex.throw_ex<std::invalid_argument>("invalid tunnel layer crypto");

        SendI2NPMessage(hop_infos.front()->identity().hash(), *build_msg);
      }
    catch (const std::exception& ex)
      {
        std::cerr << ex.what() << std::endl;
      }

    return *this;
  }

  /// @brief Create a LeaseSet for the given Destination
  /// @detail Adds the minimum of: default number of leases and number of established inbound tunnels
  ///    Adds the LeaseSet to the NetDB
  /// @throws Runtime error if no established inbound tunnels exist
  lease_set_t::shared_ptr CreateLeaseSet(const destination_t& dest)
  {
    lease_set_t::shared_ptr lease_set;

    try
      {
        auto ibgws = aes_pool_manager_.inbound_pool().has_tunnels() ? aes_pool_manager_.get_inbound_gateways()
                                                                    : chacha_pool_manager_.get_inbound_gateways();

        std::vector<lease_set_t::lease_t> leases;
        for (auto& ibgw : ibgws)
          leases.emplace_back(std::move(ibgw.first), std::move(ibgw.second));

        lease_set = std::make_shared<lease_set_t>(dest, std::move(leases));

        // add the LeaseSet to the NetDB
        netdb_.AddLeaseSet(lease_set);
      }
    catch (const std::exception& ex)
      {
        std::cerr << ex.what() << std::endl;
      }

    return lease_set;
  }

  /// @brief Send a Garlic message to a remote destination
  /// @tparam TMessage Inner message type to wrap in a GarlicClove
  /// @param local_dest Local destination for sending the message
  /// @param remote_dest_hash Remote destination hash to send the message
  /// @param message Data, DatabaseStore, or DeliveryStatus message
  /// @param msg_id Message ID for the Garlic message
  template <class TMessage>
  Context& SendGarlicMessage(
      const destination_t& local_dest,
      const destination_t::hash_t& remote_dest_hash,
      TMessage message,
      message_id_t msg_id = 0)
  {
    try
      {
        const exception::Exception ex{"Router: Context", __func__};

        auto remote_ls = netdb_.find_lease_set(remote_dest_hash);

        // Create the inner Garlic block
        data::GarlicBlock garlic(message_to_type<TMessage>(), message.buffer());

        const auto& remote_key = remote_ls->destination().crypto().pubkey;
        const auto& lease = remote_ls->get_lease();
        const auto ecies_msg_id = garlic.message_id();

        if (ecies_manager_.has_outbound_session(remote_key))
          {  // send the payload as an ExistingSession message
            SendGarlicMessage(
                ecies_msg_id,
                std::move(ecies_manager_.CreateOutboundExistingMessage(remote_key, std::move(garlic)).buffer()),
                lease);
          }
        else if (ecies_manager_.has_pending_inbound_session(remote_key))
          {  // send the payload as an NewSessionReply message
            SendGarlicMessage(
                ecies_msg_id,
                std::move(
                    ecies_manager_.CreateNewSessionReply(remote_key, ecies::ReplyPayloadSection(std::move(garlic)))
                        .buffer()),
                lease);
          }
        else
          {  // send the payload as an NewSession message
            SendGarlicMessage(
                ecies_msg_id,
                std::move(ecies_manager_
                              .CreateOutboundSession(local_dest, remote_key, ecies::PayloadSection(std::move(garlic)))
                              .buffer()),
                lease);
          }
      }
    catch (const std::exception& ex)
      {
        std::cerr << ex.what() << std::endl;
      }

    return *this;
  }

  /// @brief Create tunnel messages from an I2NP message, and send it through a random outbound tunnel
  /// @param i2np_msg Outbound I2NP message
  /// @param to_hash Identity hash of the remote InboundGateway (Tunnel delivery) or router (Router delivery)
  /// @param to_tunnel_id Tunnel ID of the InboundGateway, null if Router delivery
  /// @throws Runtime error if no established outbound tunnels are available
  Context& SendTunnelMessage(
      data::I2NPMessage i2np_msg,
      const info_t::identity_t::hash_t& to_hash,
      const tunnel_id_t& to_tunnel_id = 0)
  {
    try
      {
        const exception::Exception ex{"Router: Context", __func__};

        auto& aes_pool = aes_pool_manager_.outbound_pool();
        auto& chacha_pool = chacha_pool_manager_.outbound_pool();

        // Select an outbound pool with tunnels to send the message
        if (aes_pool.has_tunnels())
          SendTunnelMessage(aes_pool, i2np_msg, to_hash, to_tunnel_id);
        else if (chacha_pool.has_tunnels())
          SendTunnelMessage(chacha_pool, i2np_msg, to_hash, to_tunnel_id);
        else
          ex.throw_ex<std::runtime_error>("no outbound tunnels available");
      }
    catch (const std::exception& ex)
      {
        std::cerr << ex.what() << std::endl;
      }

    return *this;
  }

  /// @brief Handle incoming Tunnel messages
  Context& HandleTunnelMessage(data::I2NPMessage& message)
  {
    try
      {
        const exception::Exception ex{"Router: Context", __func__};

        const auto& msg_id = message.message_id();
        const auto& msg_type = message.type();
        const auto& is_gateway = msg_type == data::I2NPHeader::Type::TunnelGateway;

        tunnel_id_t tun_id(0);
        tini2p::read_bytes(message.message_buffer().data(), tun_id, tini2p::Endian::Big);

        const auto unwrap_reply = [](auto& m) -> data::I2NPMessage {
          data::TunnelGateway gw_msg(std::move(m.extract_body()));
          return data::I2NPMessage(gw_msg.extract_header(), gw_msg.extract_body());
        };

        if (aes_pool_manager_.has_building_tunnel(msg_id))
          aes_pool_manager_.HandleBuildReply(is_gateway ? unwrap_reply(message) : message);
        else if (chacha_pool_manager_.has_building_tunnel(msg_id))
          chacha_pool_manager_.HandleBuildReply(is_gateway ? unwrap_reply(message) : message);
        else if (msg_type == data::I2NPHeader::Type::VariableTunnelBuild)
          {
            info_t::identity_t::hash_t next_hash;
            data::I2NPMessage next_msg;
            std::tie(next_hash, next_msg) = HandleTunnelBuildMessage(message);

            SendI2NPMessage(next_hash, std::move(next_msg));
          }
        else if (aes_pool_manager_.has_tunnel(tun_id))
          HandleTunnelMessage(aes_pool_manager_, message);
        else if (chacha_pool_manager_.has_tunnel(tun_id))
          HandleTunnelMessage(chacha_pool_manager_, message);
        else
          {
            ex.throw_ex<std::runtime_error>(
                "invalid tunnel message, message type: " + std::to_string(tini2p::under_cast(msg_type))
                + " message ID: " + std::to_string(msg_id) + ", tunnel ID: " + std::to_string(tun_id));
          }
      }
    catch (const std::exception& ex)
      {
        std::cerr << ex.what() << std::endl;
      }

    return *this;
  }

  /// @brief Process an incoming tunnel build request
  /// @detail Creates a TransitTunnel on acceptance
  /// @returns Pair of the Identity hash of the next hop or reply tunnel gateway, and the processed build message
  std::pair<info_t::identity_t::hash_t, data::I2NPMessage> HandleTunnelBuildMessage(data::I2NPMessage& message)
  {
    using tunnel_request_t = crypto::TunnelRequest;
    using build_record_t = data::BuildRequestRecord;

    std::pair<info_t::identity_t::hash_t, data::I2NPMessage> ret;

    try
      {
        const exception::Exception ex{"Router: Context", __func__};

        check_info(info_, ex);

        const auto& identity = info_->identity();
        const auto& ident_hash = identity.hash();
        const auto& trunc_ident = static_cast<build_record_t::trunc_ident_t>(ident_hash);

        data::TunnelBuildMessage build_message(
            std::move(message.message_buffer()), data::TunnelBuildMessage::deserialize_mode_t::Partial);

        // Find our record in the build message
        auto& record = build_message.find_record(trunc_ident);
        auto& record_buf = record.buffer();

        curve_t::pubkey_t creator_key;

        // Read the creator's ephemeral public key from the BuildRequestRecord
        tini2p::BytesReader<build_record_t::buffer_t> reader(record_buf);
        reader.skip_bytes(tini2p::under_cast(info_t::identity_t::TruncHashLen));
        reader.read_data(creator_key);

        aead_t::key_t record_key;
        tunnel_request_t::key_buffer_t reply_key, root_key, shared_secret;

        // Derive the record keys
        std::tie(record_key, reply_key, root_key, shared_secret) = tunnel_request_t::DeriveRecordKeys(
            identity.crypto(), creator_key, ident_hash, tunnel_request_t::role_t::Hop);

        // Decrypt the request
        tunnel_request_t::DecryptRecord(record_key, record_buf);
        record.deserialize();

        const auto& is_chacha = record.is_chacha();

        // Create tunnel layer for encrypting the reply and other records
        std::variant<crypto::TunnelAES, crypto::TunnelChaCha> layer;
        if (is_chacha)
          layer = crypto::TunnelChaCha(crypto::TunnelChaCha::key_info_t(reply_key, root_key, shared_secret));
        else
          layer = crypto::TunnelAES(crypto::TunnelAES::key_info_t(reply_key, root_key, shared_secret));

        // Add our layer of encryption to the other request records
        std::visit(
            [&build_message, &trunc_ident](auto& lay) { build_message.EncryptOtherRecords(trunc_ident, lay); }, layer);

        if (bloom_.CheckElement(creator_key.buffer()) == 1)
          {  // reject the tunnel, duplicate key
            auto reply_buf =
                is_chacha
                    ? data::BuildResponseRecord<crypto::ChaCha20>(
                          data::BuildResponseRecord<crypto::ChaCha20>::flags_t::Reject)
                          .buffer()
                    : data::BuildResponseRecord<crypto::AES>(data::BuildResponseRecord<crypto::AES>::flags_t::Reject)
                          .buffer();

            std::visit([&reply_buf](auto& lay) { lay.EncryptReplyRecord(reply_buf); }, layer);

            record_buf = std::move(reply_buf);
          }
        else
          {  // accept the build request, create a transit tunnel
            if (is_chacha)
              chacha_pool_manager_.HandleBuildMessage(
                  std::move(std::get<crypto::TunnelChaCha>(layer).key_info()), record);
            else
              aes_pool_manager_.HandleBuildMessage(std::move(std::get<crypto::TunnelAES>(layer).key_info()), record);
          }

        // serialize the encryped records to buffer
        build_message.serialize();

        const auto is_obep = record.is_obep();
        const auto& msg_id = message.message_id();

        // Create the I2NP message for the next hop
        data::I2NPMessage out_msg(
            is_obep ? data::I2NPHeader::Type::VariableTunnelBuildReply : data::I2NPHeader::Type::VariableTunnelBuild,
            msg_id,
            std::move(build_message.buffer()));

        // Create a message for the reply tunnel gateway, depending if we are an OBGW in the requested tunnel
        std::optional<data::I2NPMessage> gw_msg;
        if (is_obep)
          {
            gw_msg.emplace(
                data::I2NPHeader::Type::TunnelGateway,
                msg_id,
                data::TunnelGateway(
                    record.next_tunnel_id(), std::move(out_msg.extract_header()), std::move(out_msg.extract_body()))
                    .buffer(),
                data::I2NPHeader::Mode::NTCP2);
          }

        ret = std::make_pair(record.next_ident_hash(), is_obep ? std::move(*gw_msg) : std::move(out_msg));
      }
    catch (const std::exception& ex)
      {
        std::cerr << ex.what() << std::endl;
      }

    return ret;
  }

  /// @brief Send an I2NP message to a router with the given Identity hash
  /// @detail Searches for an establish transport (NTCP2) session with the remote router
  ///      If a session is found, the message is sent
  ///      If no session is found, one is created, and the message is sent
  /// @param ident_hash Identity hash of the remote router
  /// @param message I2NP message to send
  Context& SendI2NPMessage(const info_t::identity_t::hash_t& ident_hash, data::I2NPMessage message)
  {
    try
      {
        const exception::Exception ex{"Router: Context", __func__};

        auto remote_info = netdb_.find_info(ident_hash);

        check_info(remote_info, ex);

        const auto& remote_key = remote_info->identity().crypto().pubkey;

        message.mode(data::I2NPHeader::Mode::NTCP2).serialize();

        ntcp2::DataPhaseMessage data_msg;
        data_msg.add_block(data::I2NPBlock(message.extract_header(), message.extract_body()));

        if (ntcp2_manager_.has_outbound_session(remote_key))
          {
            auto& session = ntcp2_manager_.find_outbound_session(remote_key);

            if (session.ready())
              session.Write(data_msg);
            else
              session.Wait().Write(data_msg);
          }
        else
          ntcp2_manager_.session(remote_info).Start().Wait().Write(data_msg);
      }
    catch (const std::exception& ex)
      {
        std::cerr << ex.what() << std::endl;
      }

    return *this;
  }

  /// @brief Get a const reference to the RouterInfo
  const info_t& info() const
  {
    const exception::Exception ex{"Router: Context", __func__};

    check_info(info_, ex);

    return *info_;
  }

  /// @brief Get a non-const reference to the RouterInfo
  info_t& info()
  {
    const exception::Exception ex{"Router: Context", __func__};

    check_info(info_, ex);

    return *info_;
  }

  /// @brief Get a non-const shared pointer to the RouterInfo
  info_t::shared_ptr& info_ptr()
  {
    const exception::Exception ex{"Router: Context", __func__};

    check_info(info_, ex);

    return info_;
  }

  /// @brief Get a const reference to the NTCP2 SessionManager
  const ntcp2_manager_t& ntcp2_manager() const noexcept
  {
    return ntcp2_manager_;
  }

  /// @brief Get a non-const reference to the NTCP2 SessionManager
  ntcp2_manager_t& ntcp2_manager() noexcept
  {
    return ntcp2_manager_;
  }

  /// @brief Get a const reference to the ECIES-X25519 SessionManager
  const ecies_manager_t& ecies_manager() const noexcept
  {
    return ecies_manager_;
  }

  /// @brief Get a non-const reference to the ECIES-X25519 SessionManager
  ecies_manager_t& ecies_manager() noexcept
  {
    return ecies_manager_;
  }

  /// @brief Get a const reference to the AES tunnel PoolManager
  const aes_pool_manager_t& aes_pool_manager() const noexcept
  {
    return aes_pool_manager_;
  }

  /// @brief Get a non-const reference to the AES tunnel PoolManager
  aes_pool_manager_t& aes_pool_manager() noexcept
  {
    return aes_pool_manager_;
  }

  /// @brief Get a const reference to the ChaCha tunnel PoolManager
  const chacha_pool_manager_t& chacha_pool_manager() const noexcept
  {
    return chacha_pool_manager_;
  }

  /// @brief Get a non-const reference to the ChaCha tunnel PoolManager
  chacha_pool_manager_t& chacha_pool_manager() noexcept
  {
    return chacha_pool_manager_;
  }

  /// @brief Get a const reference to the NetDB
  const netdb_t& netdb() const noexcept
  {
    return netdb_;
  }

  /// @brief Get a non-const reference to the NetDB
  netdb_t& netdb() noexcept
  {
    return netdb_;
  }

  /// @brief Drain the message queue
  std::vector<data::I2NPMessage> drain_queue()
  {
    std::scoped_lock sgd(queue_mutex_);
    return std::move(queue_);
  }

 private:
  template <class TPoolManager>
  Context& HandleTunnelMessage(TPoolManager& pool_manager, data::I2NPMessage& message)
  {
    const exception::Exception ex{"Router: Context", __func__};

    const auto& msg_type = message.type();
    message.mode(data::I2NPHeader::Mode::Normal).serialize();

    if (msg_type == data::I2NPHeader::Type::TunnelGateway)
      {
        auto hash_and_msgs = pool_manager.HandleI2NPMessage(message);

        for (auto& msg : hash_and_msgs.second)
          {
            SendI2NPMessage(
                hash_and_msgs.first,
                data::I2NPMessage(
                    data::I2NPHeader::Type::TunnelData, {}, std::move(msg.buffer()), data::I2NPHeader::Mode::NTCP2));
          }
      }
    else if (msg_type == data::I2NPHeader::Type::TunnelData)
      {
        auto i2np_msgs = pool_manager.HandleTunnelMessage(message);

        for (auto& msg : i2np_msgs)
          {
            if (msg.first == std::nullopt)
              HandleI2NPMessage(msg.second);
            else
              SendI2NPMessage(*msg.first, msg.second);
          }
      }
    else
      ex.throw_ex<std::invalid_argument>("invalid tunnel message.");

    return *this;
  }

  template <class TPool>
  Context& SendTunnelMessage(
      TPool& pool,
      data::I2NPMessage i2np_msg,
      const info_t::identity_t::hash_t& to_hash,
      const tunnel_id_t& to_tunnel_id = 0)
  {
    const auto& mode = i2np_msg.mode();
    if (mode == data::I2NPHeader::Mode::NTCP2 || mode == data::I2NPHeader::Mode::SSU)
      i2np_msg.mode(data::I2NPHeader::Mode::Normal);

    i2np_msg.serialize();

    // Create tunnel messages
    auto hash_and_msgs = pool.HandleI2NPMessage(i2np_msg.buffer(), to_hash, to_tunnel_id);

    // Send the tunnel messages to the first hop in the outbound tunnel
    for (auto& msg : hash_and_msgs.second)
      {
        SendI2NPMessage(
            hash_and_msgs.first,
            data::I2NPMessage(data::I2NPHeader::Type::TunnelData, i2np_msg.message_id(), std::move(msg.buffer())));
      }

    return *this;
  }

  Context&
  SendGarlicMessage(const message_id_t& msg_id, crypto::SecBytes&& garlic_buffer, const lease_set_t::lease_t& lease)
  {
    // Create the encrypted Garlic session I2NP block
    data::I2NPMessage ecies_garlic(
        data::I2NPHeader::Type::Garlic, msg_id, std::move(garlic_buffer), data::I2NPHeader::Mode::Garlic);

    SendTunnelMessage(ecies_garlic, lease.tunnel_gateway(), lease.tunnel_id());

    return *this;
  }

  template <class TPayload>
  Context& HandleGarlicPayload(TPayload& payload)
  {
    const exception::Exception ex{"Router: Context", __func__};

    while (payload.template has_block<data::GarlicBlock>())
      {
        auto garlic_block = payload.template extract_block<data::GarlicBlock>();

        if (garlic_block.is_local())
          {
            auto local_msg = data::I2NPMessage(garlic_block.extract_header(), garlic_block.extract_body());
            HandleI2NPMessage(local_msg);
          }
        else if (garlic_block.is_router())
          SendTunnelMessage(
              data::I2NPMessage(garlic_block.extract_header(), garlic_block.extract_body()),
              garlic_block.instructions().to_hash());
        else if (garlic_block.is_tunnel())
          {
            const auto& instructions = garlic_block.instructions();
            SendTunnelMessage(
                data::I2NPMessage(garlic_block.extract_header(), garlic_block.extract_body()),
                instructions.to_hash(),
                instructions.tunnel_id());
          }
        else
          ex.throw_ex<std::logic_error>("invalid GarlicClove delivery instructions");
      }

    return *this;
  }

  void HandleDatabaseSearchReply(data::I2NPMessage& message)
  {
    netdb_t::db_search_reply_t db_reply(std::move(message.extract_body()));
    const auto& search_key = db_reply.search_key();

    if (!netdb_.has_info(search_key) && !netdb_.has_lease_set(search_key))
      {  // did not receive a DatabaseStore for the entry, continue the search
        const auto db_lookup = netdb_.HandleDatabaseSearchReply(db_reply);

        std::scoped_lock sed(search_mutex_);

        const auto search_end = search_.end();

        auto it = std::find_if(
            search_.begin(), search_end, [&search_key](const auto& s) { return s.search_key == search_key; });

        if (it == search_end)
          {
            search_.emplace_back(search_key, 0, tini2p::time::now_s() + tini2p::under_cast(SearchEntry::Expiration));

            it = search_.begin();
            std::advance(it, search_.size() - 1);
          }

        if (it->attempts < SearchEntry::MaxAttempts)
          {
            ++it->attempts;

            for (const auto& peer : db_reply.extract_peer_keys())
              SendTunnelMessage(db_lookup, peer);
          }
      }
  }

  template <class M>
  data::I2NPHeader::Type message_to_type()
  {
    const exception::Exception ex{"Router: Context", __func__};

    data::I2NPHeader::Type type(data::I2NPHeader::Type::Reserved);

    if (std::is_same<M, data::Data>::value)
      type = data::I2NPHeader::Type::Data;
    else if (std::is_same<M, data::DatabaseStore>::value)
      type = data::I2NPHeader::Type::DatabaseStore;
    else
      ex.throw_ex<std::invalid_argument>("invalid I2NP message type");

    return type;
  }

  void check_info(const info_t::shared_ptr info, const exception::Exception& ex) const
  {
    if (info == nullptr)
      ex.throw_ex<std::runtime_error>("null RouterInfo.");
  }

  std::uint8_t is_tunnel_message(const data::I2NPMessage& message) const
  {
    const auto& msg_type = message.type();

    return static_cast<std::uint8_t>(
        msg_type == data::I2NPHeader::Type::TunnelData || msg_type == data::I2NPHeader::Type::TunnelGateway
        || msg_type == data::I2NPHeader::Type::VariableTunnelBuild
        || msg_type == data::I2NPHeader::Type::VariableTunnelBuildReply);
  }

  void RunBloomTimer()
  {
    bloom_timer_ = std::make_unique<timer_t>(bloom_ctx_, std::chrono::minutes(tini2p::under_cast(CheckRequestTimeout)));

    bloom_timer_->async_wait([this](const auto& ec) { ClearBloom(ec); });
    bloom_timer_thread_ = std::make_unique<std::thread>([this](){ bloom_ctx_.run(); });
  }

  void StopBloomTimer()
  {
    try
      {
        if (bloom_timer_ != nullptr)
          bloom_timer_->cancel();

        bloom_ctx_.stop();

        if (bloom_timer_thread_ != nullptr)
          bloom_timer_thread_->join();

      }
    catch(const std::exception& e)
      {
        std::cerr << "timer cancellation error: " << std::string(e.what());
      }
  }

  void ClearBloom(const error_c& ec)
  {
    const exception::Exception ex{"Tunnel: TransitTunnel", __func__};

    check_timer(bloom_timer_, ex);

    if (ec != asio::error::operation_aborted)
      {
        bloom_.clear();

        // Place another callback on the async queue
        bloom_timer_->expires_after(std::chrono::minutes(tini2p::under_cast(CheckRequestTimeout)));
        bloom_timer_->async_wait([this](const auto& ec) { ClearBloom(ec); });
      }
  }

  void RunNTCP2Timer()
  {
    ntcp2_timer_ =
        std::make_unique<timer_t>(ntcp2_ctx_, std::chrono::milliseconds(tini2p::under_cast(CheckListenerTimeout)));

    ntcp2_timer_->async_wait([this](const auto& ec) { CheckNTCP2Messages(ec); });
    ntcp2_timer_thread_ = std::make_unique<std::thread>([this]() { ntcp2_ctx_.run(); });
  }

  void StopNTCP2Timer()
  {
    try
      {
        if (ntcp2_timer_ != nullptr)
          ntcp2_timer_->cancel();

        ntcp2_ctx_.stop();

        if (ntcp2_timer_thread_ != nullptr)
          ntcp2_timer_thread_->join();

      }
    catch (const std::exception& e)
      {
        std::cerr << "timer cancellation error: " << std::string(e.what());
      }
  }

  void CheckNTCP2Messages(const error_c& ec)
  {
    const exception::Exception ex{"Router: Context", __func__};

    check_timer(ntcp2_timer_, ex);

    if (ec != asio::error::operation_aborted)
      {
        ntcp2_manager_.Read();

        std::this_thread::sleep_for(std::chrono::milliseconds(2));

        auto messages = ntcp2_manager_.drain_messages();

        for (auto& message : messages)
          {
            while (message.has_block<data::I2NPBlock>())
              {
                auto i2np_block = message.extract_block<data::I2NPBlock>();
                data::I2NPMessage i2np_msg(i2np_block.extract_header(), i2np_block.extract_body());

                HandleI2NPMessage(i2np_msg);
              }
          }

        // Place another callback on the async queue
        ntcp2_timer_->expires_after(std::chrono::milliseconds(tini2p::under_cast(CheckListenerTimeout)));
        ntcp2_timer_->async_wait([this](const auto& e) { CheckNTCP2Messages(e); });
      }
  }

  void RunSearchTimer()
  {
    search_timer_ =
        std::make_unique<timer_t>(search_ctx_, std::chrono::seconds(tini2p::under_cast(CheckSearchTimeout)));

    search_timer_->async_wait([this](const auto& ec) { CheckExpiredSearches(ec); });
    search_timer_thread_ = std::make_unique<std::thread>([this]() { search_ctx_.run(); });
  }

  void StopSearchTimer()
  {
    try
      {
        if (search_timer_ != nullptr)
          search_timer_->cancel();

        search_ctx_.stop();

        if (search_timer_thread_ != nullptr)
          search_timer_thread_->join();

      }
    catch (const std::exception& e)
      {
        std::cerr << "timer cancellation error: " << std::string(e.what());
      }
  }

  void CheckExpiredSearches(const asio::error_code& ec)
  {
    const exception::Exception ex{"Router: Context", __func__};

    check_timer(search_timer_, ex);

    if (ec != asio::error::operation_aborted)
      {
        const auto& now = tini2p::time::now_s();

        {  // lock-search-entries
          std::scoped_lock sgd(search_mutex_);
          search_.erase(
              std::remove_if(search_.begin(), search_.end(), [&now](const auto& s) { return s.expiration <= now; }),
              search_.end());
        }  // end-search-entry-lock

        // Place another callback on the async queue
        search_timer_->expires_after(std::chrono::milliseconds(tini2p::under_cast(CheckSearchTimeout)));
        search_timer_->async_wait([this](const auto& e) { CheckExpiredSearches(e); });
      }
  }

  void check_timer(const std::unique_ptr<timer_t>& timer, const exception::Exception& ex) const
  {
    if (timer == nullptr)
      ex.throw_ex<std::runtime_error>("null timer.");
  }

  void check_tunnel_infos(const std::vector<info_t::shared_ptr>& hop_infos, const exception::Exception& ex) const
  {
    const auto& hops_size = hop_infos.size();
    const auto& min_hops = tini2p::under_cast(MinTunnelHops);
    const auto& max_hops = tini2p::under_cast(MaxTunnelHops);

    if (hops_size < 1 || hops_size > 8)
      {
        ex.throw_ex<std::logic_error>(
            "invalid number of tunnel hops: " + std::to_string(hops_size) + ", min: " + std::to_string(min_hops)
            + ", max: " + std::to_string(max_hops));
      }

    for (const auto& info : hop_infos)
      check_info(info, ex);
  }

  info_t::shared_ptr info_;
  ntcp2_manager_t ntcp2_manager_;
  ecies_manager_t ecies_manager_;
  aes_pool_manager_t aes_pool_manager_;
  chacha_pool_manager_t chacha_pool_manager_;
  netdb_t netdb_;

  std::vector<data::I2NPMessage> queue_;
  std::shared_mutex queue_mutex_;

  bloom_filter_t bloom_;

  std::vector<SearchEntry> search_;
  std::shared_mutex search_mutex_;

  io_context_t bloom_ctx_, ntcp2_ctx_, search_ctx_;
  std::unique_ptr<timer_t> bloom_timer_, ntcp2_timer_, search_timer_;
  std::unique_ptr<std::thread> bloom_timer_thread_, ntcp2_timer_thread_, search_timer_thread_;
};
}  // namespace router
}  // namespace tini2p

#endif  // SRC_ROUTER_CONTEXT_H_
