/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ECIES_X25519_H_
#define SRC_ECIES_X25519_H_

#include <iostream>

#include "src/exception/exception.h"

#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/keys.h"
#include "src/crypto/sha.h"
#include "src/crypto/signature.h"
#include "src/crypto/x25519.h"

#include "src/ecies/cipher_state.h"
#include "src/ecies/existing_session.h"
#include "src/ecies/keys.h"
#include "src/ecies/new_session.h"
#include "src/ecies/new_session_reply.h"
#include "src/ecies/role.h"

namespace tini2p
{
namespace ecies
{
/// @class EciesX25519
/// @brief Implementation of the ECIES-X25519-AEAD-ChaCha20-Poly1305-Ratchet protocol
/// @detail This scheme is based on [X25519-DH](https://tools.ietf.org/html/rfc7748#section-6.1) and [Signal Double Ratchet](https://signal.org/docs/specifications/doubleratchet/).
///
///   Long-term static X25519 keys are used for input key material for X25519DH.
///
///   A root and chain key are derived from the X25519DH results using the Diffie-Hellman ratchet (DHRatchet).
///
///   The DHRatchet uses X25519DH + HKDF<HMAC-SHA256> to derive new root + chain keys:
///
///     - root_key = X25519DH(static_dh_keys, remote_dh_pubkey)
///
///     - sub_key: Uint8Buffer<64>
///       - root_key = sub_key[:31]
///       - chain_key = sub_key[32:]
///     - root_key: Uint8Buffer<32>
///     - nonce: Nonce{ CounterUint8Buffer<12>, CounterUint16 }
///     - dh_context: CString("KDFDHRatchetStep")
///
///   On each message, a new chain key and one-time message key are derived using the chain key ratchet (ChainRatchet).
///
///   For new sessions between the same parties, each generates a new root key using DHRatchet.
///   The first message after rekey will include the new X25519DH public key to derive the new session's root + chain keys.
//.
///   The ChainRatchet uses HKDF<HmacSha256> to derive new chain + message keys:
///
///     - sub_key: Uint8Buffer<64>
///       - chain_key = sub_key[:31]
///       - message_key = sub_key[32:]
///     - master_key: Uint8Buffer<32>
///     - nonce: Nonce{ CounterUint8Buffer<12>, CounterUint16 }
///     - chain_context: CString("TagAndKeyGenKeys")
///
///   The nonce is strictly increasing, and new sessions must be initiated after MaxNonce<65535> messages.
///
///   IMPORTANT: the same nonce *MUST NOT* be used with the same key to encrypt different messages. Doing so is doom for the cryptosystem.
class EciesX25519
{
 public:
  /// @brief Tag status for incoming message identification
  enum struct TagStatus : std::uint8_t
  {
    NotFound,
    Found,
  };

  /// @brief Trial-decryption status for incoming message identification
  enum struct DecryptionStatus : std::uint8_t
  {
    Failed,
    Successful,
  };

  /// @brief Requirement enum for needed components and/or ratchets
  enum struct Reqs : bool
  {
    NotRequired,
    Required
  };

  enum struct SessionMode : std::uint16_t
  {
    OneTime,
    Unbound,
    Bound = 0x0003,
  };

  using curve_t = crypto::X25519;  //< Curve trait alias
  using elligator_t = crypto::Elligator2;  //< Elligator2 trait alias
  using sha_t = crypto::Sha256;  //< Sha256 trait alias
  using pubkey_t = curve_t::pubkey_t;  //< Public key trait alias
  using keypair_t = curve_t::keypair_t;  //< Keypair trait alias
  using keys_t = ecies::Keys;  //< ECIES keys trait alias
  using dh_state_t = ecies::DHState;  //< ECIES DH ratchet state trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD symmetric crypto impl trait alias
  using session_id_t = std::uint64_t;  //< SessionID trait alias
  using new_message_t = ecies::NewSessionMessage;  //< NewSession message trait alias
  using new_reply_t = ecies::NewSessionReplyMessage;  //< NewSessionReply message trait alias
  using existing_message_t = ecies::ExistingSessionMessage;  //< ExistingSession message trait alias
  using tag_status_t = TagStatus;  //< Tag status trait alias
  using tag_t = ecies::Tag;  //< Session tag trait alias
  using decryption_status_t = DecryptionStatus;  //< Decryption status trait alias
  using session_mode_t = SessionMode;  //< Session mode trait alias
  using initiator_r = ecies::EciesInitiator;  //< ECIES initiator role trait alias
  using responder_r = ecies::EciesResponder;  //< ECIES responder role trait alias
  using in_ciph_state_t = ecies::CipherState<responder_r>;  //< Inbound ECIES state trait alias
  using out_ciph_state_t = ecies::CipherState<initiator_r>;  //< Outbound ECIES state trait alias
  using ecies_state_v = std::variant<in_ciph_state_t, out_ciph_state_t>;  //< CipherState variant trait alias

  enum
  {
    MACLen = aead_t::MACLen,
    NonceLen = aead_t::nonce_t::NonceLen,
    PublicKeyLen = curve_t::PublicKeyLen,
    PrivateKeyLen = curve_t::PrivateKeyLen,
    SharedKeyLen = curve_t::SharedKeyLen,
    NewSessionKeydataLen = 64,
  };

  /// @brief Default ctor, creates new keypair
  EciesX25519() : session_id_(), keys_(), dh_state_(), ciph_state_() {}

  /// @brief Copy-ctor
  EciesX25519(const EciesX25519& oth)
      : session_id_(oth.session_id_), keys_(oth.keys_), dh_state_(oth.dh_state_), ciph_state_(oth.ciph_state_)
  {
  }

  /// @brief Move-ctor
  EciesX25519(EciesX25519&& oth)
      : session_id_(std::move(oth.session_id_)),
        keys_(std::move(oth.keys_)),
        dh_state_(std::move(oth.dh_state_)),
        ciph_state_(std::move(oth.ciph_state_))
  {
  }

  /// @brief Create an EciesX25519 with local identity private key
  /// @param id_key Local long-term identity private key
  explicit EciesX25519(curve_t::pvtkey_t id_key)
      : session_id_(), keys_(std::forward<curve_t::pvtkey_t>(id_key)), dh_state_(), ciph_state_()
  {
  }

  /// @brief Create an EciesX25519 with local identity keypair
  /// @param id_keys Local long-term identity keypair
  explicit EciesX25519(curve_t::keypair_t id_keys)
      : session_id_(), keys_(std::forward<curve_t::keypair_t>(id_keys)), dh_state_(), ciph_state_()
  {
  }

  /// @brief Create an EciesX25519 with local identity keypair
  /// @param id_keys Local long-term identity keypair
  /// @param ep_keys Local ephemeral keypair
  template <
      class TEphemeral,
      class = std::enable_if_t<
          std::is_same<TEphemeral, curve_t::keypair_t>::value
          || std::is_same<TEphemeral, elligator_t::keypair_t>::value>>
  EciesX25519(curve_t::keypair_t id_keys, TEphemeral ep_keys)
      : session_id_(),
        keys_(std::forward<curve_t::keypair_t>(id_keys), std::forward<TEphemeral>(ep_keys)),
        dh_state_(),
        ciph_state_()
  {
  }

  /// @brief Create an EciesX25519 from remote static + ephemeral public keys
  /// @note Creates fresh local static + ephemeral keys
  /// @param remote_id_pk Remote static public key
  /// @param remote_ep_pk Remote ephemeral public key
  EciesX25519(curve_t::pubkey_t remote_id_pk, curve_t::pubkey_t remote_ep_pk)
      : session_id_(),
        keys_(std::forward<curve_t::pubkey_t>(remote_id_pk), std::forward<curve_t::pubkey_t>(remote_ep_pk)),
        dh_state_(),
        ciph_state_()
  {
  }

  /// @brief Create a fully initialized EciesX25519
  /// @param id_keys Local static keypair
  /// @param remote_id_pk Remote static public key
  /// @param remote_ep_pk Remote ephemeral public key
  EciesX25519(curve_t::keypair_t id_keys, curve_t::pubkey_t remote_id_key, curve_t::pubkey_t remote_ep_key)
      : session_id_(),
        keys_(
            std::forward<curve_t::keypair_t>(id_keys),
            std::forward<curve_t::pubkey_t>(remote_id_key),
            std::forward<curve_t::pubkey_t>(remote_ep_key)),
        dh_state_(),
        ciph_state_()
  {
  }

  /// @brief Forwarding-assignment operator
  EciesX25519& operator=(EciesX25519 oth)
  {
    session_id_ = std::forward<session_id_t>(oth.session_id_);
    keys_ = std::forward<keys_t>(oth.keys_);
    dh_state_ = std::forward<dh_state_t>(oth.dh_state_);
    ciph_state_ = std::forward<ecies_state_v>(oth.ciph_state_);

    return *this;
  }

  /// @brief Perform a DH ratchet to derive Session Tag and Symmetric Key chain keys
  /// @detail Used for ratcheting with the chain key and k from the key state
  /// @note One-time "sessions" do not ratchet nor create actual sessions
  template <class TRole>
  EciesX25519& DHRatchet()
  {
    const exception::Exception ex{"EciesX25519", __func__};

    check_cipher_state(ex);

    curve_t::shrkey_t tag_chain_key, sym_chain_key;
    std::tie(tag_chain_key, sym_chain_key) = dh_state_.Ratchet(keys_);

    InitCipherState<TRole>(std::move(tag_chain_key), std::move(sym_chain_key));

    return *this;
  }

  /// @brief Perform a DH ratchet to derive Session Tag and Symmetric Key chain keys
  /// @detail Used for ratcheting with the chain key and k from the key state
  /// @note One-time "sessions" do not ratchet nor create actual sessions
  template <class TRole>
  EciesX25519& DHRatchet(const curve_t::pubkey_t& remote_key)
  {
    const exception::Exception ex{"EciesX25519", __func__};

    curve_t::shrkey_t tag_chain_key, sym_chain_key;
    std::tie(tag_chain_key, sym_chain_key) = dh_state_.Ratchet(keys_.ep_keys(), remote_key);

    InitCipherState<TRole>(std::move(tag_chain_key), std::move(sym_chain_key));

    return *this;
  }

  /// @brief Perform a DH ratchet to derive Session Tag and Symmetric Key chain keys
  /// @detail Used for a received NewSession message w/ Payload or ReplyPayload from remote router
  /// @note One-time "sessions" do not ratchet nor create actual sessions
  /// @tparam TSection Payload section type
  /// @param binding Binding status of the session (Bound or Unbound)
  /// @param section Payload or ReplyPayload from a received NewSession message
  /// @returns Session tag and symmetric key chain keys
  ///     Inbound set only for unbound sessions
  ///     Inbound and outbound sets for bound sessions
  template <class TRole, class TSection>
  EciesX25519& DHRatchet(const TSection& section)
  {
    curve_t::shrkey_t tag_chain_key, sym_chain_key;
    std::tie(tag_chain_key, sym_chain_key) = dh_state_.Ratchet(section);

    InitCipherState<TRole>(std::move(tag_chain_key), std::move(sym_chain_key));

    return *this; 
  }

  /// @brief Initialize the symmetric CipherState with DH ratchet results
  /// @tparam TRole ECIES session role, outbound is initiator, inbound is responder
  template <class TRole>
  EciesX25519& InitCipherState(curve_t::shrkey_t tag_chain_key, curve_t::shrkey_t sym_chain_key)
  {
    if (std::is_same<TRole, initiator_r>::value && !std::holds_alternative<out_ciph_state_t>(ciph_state_))
      {
        ciph_state_.emplace<out_ciph_state_t>(
            std::forward<curve_t::shrkey_t>(tag_chain_key), std::forward<curve_t::shrkey_t>(sym_chain_key));
      }
    else if (std::is_same<TRole, responder_r>::value && !std::holds_alternative<in_ciph_state_t>(ciph_state_))
      {
        ciph_state_.emplace<in_ciph_state_t>(
            std::forward<curve_t::shrkey_t>(tag_chain_key), std::forward<curve_t::shrkey_t>(sym_chain_key));
      }
    else
      {
        std::visit(
            [&tag_chain_key, &sym_chain_key](auto& c) {
              c.UpdateChainKeys(
                  std::forward<curve_t::shrkey_t>(tag_chain_key), std::forward<curve_t::shrkey_t>(sym_chain_key));
            },
            ciph_state_);
      }

    return *this;
  }

  /// @brief Ratchet the session tags to generate a tag window
  /// @detail Used by pending sessions to generate reply tags
  ///     No corresponding symmetric session keys are generated
  template <
      class TRole,
      class = std::enable_if_t<std::is_same<TRole, initiator_r>::value || std::is_same<TRole, responder_r>::value>>
  EciesX25519& ReplyTagRatchet()
  {
    using hkdf_t = crypto::HKDF<crypto::HmacSha256>;

    const exception::Exception ex{"EciesX25519", __func__};

    const auto is_initiator = std::is_same<TRole, initiator_r>::value;

    if (is_initiator && !std::holds_alternative<in_ciph_state_t>(ciph_state_))
      ciph_state_.emplace<in_ciph_state_t>();
    else if (!is_initiator && !std::holds_alternative<out_ciph_state_t>(ciph_state_))
      ciph_state_.emplace<out_ciph_state_t>();
    else
      std::cout << "ECIES: SessionManger: cipherstate initialized" << std::endl;

    if (std::visit([](const auto& s) { return s.tag_initial_round(); }, ciph_state_))
      {
        // tagsetKey = HKDF(chainKey, ZEROLEN, "SessionReplyTags", 32)
        curve_t::shrkey_t tagset_key, sym_key;
        const auto& chain_key = keys_.chain_key();
        hkdf_t::Derive(tagset_key, chain_key, crypto::FixedSecBytes<0>{}, ECIES_REPLY_TAGSET_CTX);

        // part one of TAGSET.CREATE(chain_key, tagsetKey)
        std::tie(tagset_key, sym_key) = dh_state_.Ratchet(chain_key, std::move(tagset_key));

        std::visit(
            [tagset_key = std::move(tagset_key)](auto& s) { s.tag_chain_key(std::move(tagset_key)); }, ciph_state_);
      }

    // part two of TAGSET.CREATE: create the actual tagset
    std::visit([](auto& s) { s.ReplyTagRatchet(); }, ciph_state_);

    return *this;
  }

  /// @brief Encrypt a given NewSession or NewSessionReply message
  /// @detail Encrypts to message buffer, storing the ciphertext and MAC
  /// @param message NewSessionMessage to encrypt in-place
  template <
      class TNew,
      typename = std::enable_if_t<std::is_same<TNew, new_message_t>::value || std::is_same<TNew, new_reply_t>::value>>
  void Encrypt(TNew& message)
  {
    message.serialize(keys_);
  }

  /// @brief Authenticate and decrypt an NewSession or NewSessionReply message
  /// @detail Decrypts message from buffer, removes MAC after successful authentication
  /// @return Status of decryption
  decryption_status_t Decrypt(new_message_t& message)
  {
    auto status = decryption_status_t::Successful;

    try
      {
        message.deserialize(keys_);
      }
    catch (const std::exception& ex)
      {
        std::cerr << "New session decryption failed: " << ex.what() << std::endl;
        status = decryption_status_t::Failed;
      }

    return status;
  }

  /// @brief AEAD Encrypt a NewSessionReply message
  /// @detail Encrypts and serializes message to buffer (including MAC)
  /// @return Cipherstate key for the inbound session cipherstate
  EciesX25519& Encrypt(new_reply_t& message)
  {
    message.serialize(keys_);

    return *this;
  }

  /// @brief Authenticate and decrypt a NewSessionReply message
  /// @detail Decrypts message from buffer, removes MAC after successful authentication
  /// @return Cipherstate key for the outbound session cipherstate
  EciesX25519& Decrypt(new_reply_t& message)
  {
    message.deserialize(keys_);

    return *this;
  }

  /// @brief Encrypt a given ExistingSessionMessage
  /// @detail Encrypts to message buffer, storing the ciphertext and MAC
  /// @param message ExistingSessionMessage to encrypt in-place
  EciesX25519& Encrypt(existing_message_t& message)
  {
    const exception::Exception ex{"EciesX25519", __func__};

    if (!std::holds_alternative<out_ciph_state_t>(ciph_state_))
      ex.throw_ex<std::logic_error>("invalid ECIES cipher state, must be an outbound session.");

    auto& state = std::get<out_ciph_state_t>(ciph_state_);
    state.CheckRatchets();
    message.tag(state.tag());
    message.serialize(state);

    return *this;
  }

  /// @brief Authenticate and decrypt an ExistingSessionMessage
  /// @detail Decrypts message from buffer, removes MAC after successful authentication
  EciesX25519& Decrypt(existing_message_t& message)
  {
    const exception::Exception ex{"EciesX25519", __func__};

    if (!std::holds_alternative<in_ciph_state_t>(ciph_state_))
      ex.throw_ex<std::logic_error>("invalid ECIES cipher state, must be an inbound session.");

    auto& state = std::get<in_ciph_state_t>(ciph_state_);
    state.CheckRatchets();
    message.deserialize(state);

    return *this;
  }

  /// @brief Checks and performs if ratchets are needed
  EciesX25519& CheckRatchets()
  {
    const exception::Exception ex{"EciesX25519", __func__};

    check_cipher_state(ex);

    std::visit([](auto& s) { s.CheckRatchets(); }, ciph_state_);

    return *this;
  }

  /// @brief Get a const reference to the local root key
  const session_id_t& session_id() const noexcept
  {
    return session_id_;
  }

  /// @brief Get a const reference to the local root key
  EciesX25519& session_id(session_id_t id)
  {
    session_id_ = std::forward<session_id_t>(id);

    return *this;
  }

  /// @brief Create a random session ID
  EciesX25519& create_session_id()
  {
    session_id_t id(0);
    crypto::RandBytes(reinterpret_cast<std::uint8_t*>(&id), sizeof(id));
    session_id_ = std::move(id);

    return *this;
  }

  /// @brief Get a const reference to the local root key
  const curve_t::shrkey_t& root_key() const noexcept
  {
    return dh_state_.root_key();
  }

  /// @brief Get a const reference to the DH nonce
  const aead_t::nonce_t& dh_nonce() const noexcept
  {
    return dh_state_.nonce();
  }

  /// @brief Get a const reference to the identity public key
  const curve_t::pubkey_t& pubkey() const noexcept
  {
    return keys_.pubkey();
  }

  /// @brief Get a non-const reference to the identity public key
  curve_t::pubkey_t& pubkey() noexcept
  {
    return keys_.pubkey();
  }

  /// @brief Get local static keys
  const curve_t::keypair_t& id_keys() const noexcept
  {
    return keys_.id_keys();
  }

  /// @brief Set local ephemeral keys
  EciesX25519& id_keys(crypto::X25519::keypair_t keys)
  {
    keys_.id_keys(std::forward<crypto::X25519::keypair_t>(keys));

    return *this;
  }

  /// @brief Get local ephemeral keys
  const curve_t::keypair_t& ep_keys() const noexcept
  {
    return keys_.ep_keys();
  }

  /// @brief Set local ephemeral keys
  template <class TKeys>
  EciesX25519& ep_keys(TKeys keys)
  {
    keys_.ep_keys(std::forward<TKeys>(keys));

    return *this;
  }

  /// @brief Get a const reference to the identity public key
  const curve_t::pubkey_t& remote_id_key() const noexcept
  {
    return keys_.remote_id_key();
  }

  /// @brief Get a const reference to the identity public key
  const curve_t::pubkey_t& remote_ep_key() const noexcept
  {
    return keys_.remote_ep_key();
  }

  /// @brief Set remote static public key
  EciesX25519& remote_id_key(curve_t::pubkey_t key)
  {
    keys_.remote_id_key(std::forward<curve_t::pubkey_t>(key));

    return *this;
  }

  /// @brief Set remote ephemeral public key
  EciesX25519& remote_ep_key(curve_t::pubkey_t key)
  {
    keys_.remote_ep_key(std::forward<curve_t::pubkey_t>(key));

    return *this;
  }

  /// @brief Set remote identity, ephemeral, and one-time ephemeral public keys
  EciesX25519& remote_rekey(curve_t::pubkey_t r_id_key, curve_t::pubkey_t r_ep_key)
  {
    const exception::Exception ex{"EciesX25519", __func__};

    keys_.remote_rekey(std::forward<curve_t::pubkey_t>(r_id_key), std::forward<curve_t::pubkey_t>(r_ep_key));

    require_dh_ratchet(ex);

    return *this;
  }

  /// @brief Reset local static keypair and generate new ephemeral keypair
  /// @detail Requires a DH ratchet, and sending a "Next DH Ratchet Public Key" message
  EciesX25519& rekey(curve_t::keypair_t keys)
  {
    const exception::Exception ex{"EciesX25519", __func__};

    keys_.rekey(std::forward<curve_t::keypair_t>(keys));
    require_dh_ratchet(ex);

    return *this;
  }

  /// @brief Reset local static keypair and generate new ephemeral keypair
  /// @detail Requires a DH ratchet, and sending a "Next DH Ratchet Public Key" message
  EciesX25519& rekey(curve_t::keypair_t id_keys, curve_t::keypair_t ep_keys)
  {
    const exception::Exception ex{"EciesX25519", __func__};

    keys_.rekey(std::forward<curve_t::keypair_t>(id_keys), std::forward<curve_t::keypair_t>(ep_keys));
    require_dh_ratchet(ex);

    return *this;
  }

  /// @brief Reset local static keypair and remote static + ephemeral keys
  /// @param keys Local static keypair
  /// @param id_key Remote static public key
  /// @param ep_key Remote ephemeral public key
  EciesX25519& rekey(curve_t::keypair_t keys, curve_t::pubkey_t id_key, curve_t::pubkey_t ep_key)
  {
    keys_.rekey(
        std::forward<curve_t::keypair_t>(keys),
        std::forward<curve_t::pubkey_t>(id_key),
        std::forward<curve_t::pubkey_t>(ep_key));

    return *this;
  }

  /// @brief Get the tag associated with the current nonce
  /// @note Must be non-const to lock tag state mutexes
  tag_t tag()
  {
    const exception::Exception ex{"EciesX25519", __func__};

    check_cipher_state(ex);

    return std::visit([](auto& s) { return s.tag(); }, ciph_state_);
  }

  /// @brief Find if the tag is in the ECIES tag window
  /// @param tag Session tag to search for
  /// @return Whether the tag was found 
  tag_status_t find_tag(const tag_t& tag) const
  {
    const exception::Exception ex{"EciesX25519", __func__};

    check_cipher_state(ex);

    auto status = tag_status_t::NotFound;
    const auto& tags = std::visit([](const auto& s) { return s.tags(); }, ciph_state_);
    const auto it = std::find_if(tags.begin(), tags.end(), [&tag](const auto& t) { return std::get<tag_t>(t) == tag; });

    if (it != tags.end())
      status = tag_status_t::Found;

    return status;
  }

  /// @brief Remove used tag from the tag state
  /// @detail Used primarily for removing used reply tags from pending sessions
  EciesX25519& remove_used_tag(const tag_t& tag)
  {
    const exception::Exception ex{"EciesX25519", __func__};

    check_cipher_state(ex);
    std::visit([&tag](auto& s) { s.remove_used(tag); }, ciph_state_);

    return *this;
  }

  /// @brief Get whether the session tags are empty
  std::uint8_t empty_tags()
  {
    const exception::Exception ex{"EciesX25519", __func__};

    check_cipher_state(ex);
    return std::visit([](auto& s) { return s.empty_tags(); }, ciph_state_);
  }

  /// @brief Get const reference to the current symmetric message key
  /// @note Must be non-const to lock symmetric keys state mutexes
  curve_t::shrkey_t sym_key()
  {
    const exception::Exception ex{"EciesX25519", __func__};

    check_cipher_state(ex);

    return std::visit([](auto& s) { return s.sym_key(); }, ciph_state_);
  }

  /// @brief Create EciesX25519 keypair
  static curve_t::keypair_t create_keys()
  {
    return curve_t::create_keys();
  }

  /// @brief Get a const reference to the chain key
  const curve_t::shrkey_t& chain_key() const noexcept
  {
    return keys_.chain_key();
  }

  /// @brief Set the chain key
  EciesX25519& chain_key(curve_t::shrkey_t chain_key)
  {
    keys_.chain_key(std::forward<curve_t::shrkey_t>(chain_key));

    return *this;
  }

  /// @brief Get a const reference to h
  const sha_t::digest_t& h() const noexcept
  {
    return keys_.h();
  }

  /// @brief Set h
  EciesX25519& h(sha_t::digest_t h)
  {
    keys_.h(std::forward<sha_t::digest_t>(h));

    return *this;
  }

  /// @brief Get a const reference to the cipherstate k
  const curve_t::shrkey_t& k() const noexcept
  {
    return keys_.k();
  }

  /// @brief Set the cipherstate k
  EciesX25519& k(curve_t::shrkey_t k)
  {
    keys_.k(std::forward<curve_t::shrkey_t>(k));

    return *this;
  }

  /// @brief Get a const reference to the cipherstate k for the opposite direction
  const curve_t::shrkey_t& oth_k() const noexcept
  {
    return keys_.oth_k();
  }

  /// @brief Set the cipherstate k for the opposite direction
  EciesX25519& oth_k(curve_t::shrkey_t k)
  {
    keys_.oth_k(std::forward<curve_t::shrkey_t>(k));

    return *this;
  }

  /// @brief Securely clear the other cipherstate k once it's no longer needed
  EciesX25519& clear_oth_k()
  {
    keys_.clear_oth_k();

    return *this;
  }

  /// @brief Equality comparison with another EciesX25519
  std::uint8_t operator==(EciesX25519& oth)
  {  // attempt constant-time comparison
    const auto& keys_eq = keys_ == oth.keys_;
    const auto& dh_eq = dh_state_ == oth.dh_state_;

    // must use non-const references to lock internal mutexes
    const auto& ciph_eq = std::visit(
        [&oth](auto& c) -> std::uint8_t { return std::visit([&c](auto& oc) { return c == oc; }, oth.ciph_state_); },
        ciph_state_);

    return (keys_eq * dh_eq * ciph_eq);
  }

 private:
  void check_cipher_state(const exception::Exception& ex) const
  {
    if (!std::holds_alternative<in_ciph_state_t>(ciph_state_) && !std::holds_alternative<out_ciph_state_t>(ciph_state_))
      ex.throw_ex<std::logic_error>("checking ratchet state on unitialized cipher state.");
  }

  void require_dh_ratchet(const exception::Exception& ex)
  {
    check_cipher_state(ex);

    std::visit([](auto& s) { s.dh_ratchet_required(ecies::Reqs::Required); }, ciph_state_);
  }

  session_id_t session_id_;
  keys_t keys_;
  dh_state_t dh_state_;
  ecies_state_v ciph_state_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_ECIES_X25519_H_
