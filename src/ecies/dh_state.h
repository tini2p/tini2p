/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ECIES_DH_STATE_H_
#define SRC_ECIES_DH_STATE_H_

#include <optional>
#include <unordered_map>

#include "src/bytes.h"
#include "src/exception/exception.h"

#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/elligator2.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/kdf_context.h"
#include "src/crypto/sha.h"
#include "src/crypto/x25519.h"

#include "src/ecies/keys.h"
#include "src/ecies/message_sections.h"
#include "src/ecies/role.h"

namespace tini2p
{
namespace ecies
{
/// @brief DH ratchet root + chain key context
static const crypto::KDFContext<crypto::HmacSha256> ECIES_DH_RATCHET_CTX{std::string("KDFDHRatchetStep")};

/// @brief DH ratchet inbound session tag + symmetric key chain keys context
static const crypto::KDFContext<crypto::HmacSha256> ECIES_OUT_TAG_AND_KEYS_CTX{std::string("TagAndKeyGenKeys")};

/// @brief DH ratchet outbound session tag + symmetric key chain keys context
static const crypto::KDFContext<crypto::HmacSha256> ECIES_IN_TAG_AND_KEYS_CTX{std::string("InChainTagSymGen")};

/// @brief Initial root key Sha256("144-ECIES-X25519-AEAD-Ratchet")
static const crypto::Sha256::digest_t INITIAL_ROOT_KEY{
    {0xbc, 0x70, 0xc1, 0xd1, 0x9d, 0xe6, 0xb2, 0xfb, 0xd0, 0xc6, 0xa2, 0x95, 0x53, 0x88, 0xaa, 0x69,
     0x30, 0x47, 0x15, 0xbb, 0x33, 0xf9, 0x04, 0xb5, 0x8c, 0x50, 0xbe, 0x19, 0x3e, 0x26, 0xb6, 0x21}};

/// @brief Zero-length secure buffer
static const crypto::FixedSecBytes<0> ZERO_LEN{};

/// @class DHState
/// @brief DH ratchet state for deriving session tag and symmetric key chain keys
class DHState
{
 public:
  enum struct Binding
  {
    Unbound,
    Bound,
  };

  enum
  {
    KeydataLen = 64,
  };

  using curve_t = crypto::X25519;  //< Curve trait alias
  using elligator_t = crypto::Elligator2;  //< Elligator2 trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD symmetric crypto trait alias
  using hmac_t = crypto::HmacSha256;  //< HMAC-Sha256 hashing function trait alias
  using hkdf_t = crypto::HKDF<hmac_t>;  //< HKDF trait alias
  using dh_keydata_t = crypto::FixedSecBytes<KeydataLen>;  //< DH KDF keydata trait alias
  using keys_t = ecies::Keys;  //< ECIES keys trait alias
  using initiator_r = ecies::EciesInitiator;  //< ECIES initiator role trait alias
  using responder_r = ecies::EciesResponder;  //< ECIES responder role trait alias

  /// @brief Default ctor
  DHState() : root_key_(), chain_key_(), nonce_(0) {}

  /// @brief Perform a DH ratchet to derive Session Tag and Symmetric Key chain keys
  /// @detail Used for a received NewSessionReply message from remote router
  /// @note One-time "sessions" do not ratchet nor create actual sessions
  /// @tparam TRole ECIES session role for deriving chain keys
  /// @param local_id_keys Local static keypair to derive the shared secret
  /// @param local_ep_keys Local ephemeral keypair to derive the shared secret
  /// @param remote_ep_key Remote ephemeral public key to derive the shared secret
  /// @returns Session tag and symmetric key chain keys
  std::pair<curve_t::shrkey_t, curve_t::shrkey_t> Ratchet(keys_t& keys)
  {
    const exception::Exception ex{"Ecies: DHState", __func__};

    root_key_ = keys.chain_key();

    return DeriveChainKeys(keys.k());
  }

  /// @brief Perform a DH ratchet to derive Session Tag and Symmetric Key chain keys
  /// @detail Used for a received NewSessionReply message from remote router
  /// @note One-time "sessions" do not ratchet nor create actual sessions
  /// @tparam TRole ECIES session role for deriving chain keys
  /// @param local_id_keys Local static keypair to derive the shared secret
  /// @param local_ep_keys Local ephemeral keypair to derive the shared secret
  /// @param remote_ep_key Remote ephemeral public key to derive the shared secret
  /// @returns Session reply tagset chain key and symmetric key chain key
  std::pair<curve_t::shrkey_t, curve_t::shrkey_t> Ratchet(curve_t::shrkey_t chain_key, curve_t::shrkey_t tagset_key)
  {
    const exception::Exception ex{"Ecies: DHState", __func__};

    root_key_ = std::forward<curve_t::shrkey_t>(chain_key);
    nonce_.reset();

    return DeriveChainKeys(std::forward<curve_t::shrkey_t>(tagset_key));
  }

  /// @brief Perform a DH ratchet to derive Session Tag and Symmetric Key chain keys
  /// @detail Used for a received NextDHKey block from remote router
  /// @note One-time "sessions" do not ratchet nor create actual sessions
  /// @tparam TRole ECIES session role for deriving chain keys
  /// @param binding Binding status of the session (Bound or Unbound)
  /// @param local_ep_keys Local ephemeral keypair to derive the shared secret
  /// @param remote_ep_key Remote ephemeral public key to derive the shared secret
  /// @returns Session tag and symmetric key chain keys
  std::pair<curve_t::shrkey_t, curve_t::shrkey_t> Ratchet(
      const curve_t::keypair_t& local_ep_keys,
      const curve_t::pubkey_t& remote_ep_key)
  {
    const exception::Exception ex{"Ecies: DHState", __func__};

    if (local_ep_keys.pvtkey.is_zero())
      ex.throw_ex<std::invalid_argument>("null local private key.");

    if (remote_ep_key.is_zero())
      ex.throw_ex<std::logic_error>("null remote ephemeral key.");

    curve_t::shrkey_t shared_secret;
    // sharedSecret = DH(rask, rbpk) = DH(rbsk, rapk)
    curve_t::DH(shared_secret, local_ep_keys.pvtkey, remote_ep_key);

    return DeriveChainKeys(shared_secret);
  }

  /// @brief Perform a DH ratchet to derive Session Tag and Symmetric Key chain keys
  /// @detail Used for a received NewSession message w/ Payload or ReplyPayload from remote router
  /// @note One-time "sessions" do not ratchet nor create actual sessions
  /// @tparam TRole ECIES session role for deriving chain keys
  /// @tparam TSection Payload section type
  /// @param binding Binding status of the session (Bound or Unbound)
  /// @param section Payload or ReplyPayload from a received NewSession message
  /// @returns Session tag and symmetric key chain keys
  template <class TSection>
  std::pair<curve_t::shrkey_t, curve_t::shrkey_t> Ratchet(const TSection& section)
  {
    const exception::Exception ex{"Ecies: DHState", __func__};

    const auto& key_info = section.key_info();

    if (key_info.chain_key.is_zero())
      ex.throw_ex<std::logic_error>("null section chain key");

    if (key_info.k.is_zero())
      ex.throw_ex<std::logic_error>("null section k key");

    root_key_ = key_info.chain_key;

    return DeriveChainKeys(key_info.k);
  }

  /// @brief Get a const reference to the local root key
  const curve_t::shrkey_t& root_key() const noexcept
  {
    return root_key_;
  }

  /// @brief Get a const reference to the local chain key
  const curve_t::shrkey_t& chain_key() const noexcept
  {
    return chain_key_;
  }

  /// @brief Get a const reference to the DH nonce
  const aead_t::nonce_t& nonce() const noexcept
  {
    return nonce_;
  }

  /// @brief Equality comparison with another DHState
  std::uint8_t operator==(const DHState& oth) const
  {  // attempt constant-time comparison
    const auto& root_eq = root_key_ == oth.root_key_;
    const auto& chain_eq = chain_key_ == oth.chain_key_;
    const auto& nonce_eq = nonce_ == oth.nonce_;

    return (root_eq * chain_eq * nonce_eq);
  }

 private:
  // TODO(tini2p): refactor derivation to only return the direction indicated by role
  //    The opposite direction will also call DHRatchet with it's chain key and cipherstate key
  std::pair<curve_t::shrkey_t, curve_t::shrkey_t> DeriveChainKeys(const curve_t::shrkey_t& shared_secret)
  {
    // keydata = HKDF(root_key, sharedSecret, "KDFDHRatchetStep", 64)
    dh_keydata_t keydata;
    hkdf_t::Derive(keydata, root_key_, shared_secret, ECIES_DH_RATCHET_CTX);

    tini2p::BytesReader<dh_keydata_t> reader(keydata);

    reader.read_data(root_key_);  // nextRootKey = keydata[0:31]
    reader.read_data(chain_key_);  // ck = keydata[32:63]

    curve_t::shrkey_t tag_chain_key, sym_chain_key;
    // keydata = HKDF(ck, ZEROLEN, "TagAndKeyGenKeys", 64)
    hkdf_t::Derive(keydata, chain_key_, ZERO_LEN, ECIES_OUT_TAG_AND_KEYS_CTX);
    reader.reset();  // reset reader to beginning of keydata

    // read session tag and symmetric key chain keys
    reader.read_data(tag_chain_key);  // sessTag_ck[0] = keydata[0:31]
    reader.read_data(sym_chain_key);  // symmKey_ck[0] = keydata[32:63]

    nonce_++;  // increase the DH nonce

    return std::make_pair(std::move(tag_chain_key), std::move(sym_chain_key));
  }

  curve_t::shrkey_t root_key_, chain_key_;
  aead_t::nonce_t nonce_;
};
}  // namespace ecies
}  // namespace tini2p

#endif  // SRC_ECIES_DH_STATE_H_
