/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ECIES_KEYS_H_
#define SRC_ECIES_KEYS_H_

#include "src/crypto/nonce.h"
#include "src/crypto/sha.h"
#include "src/crypto/x25519.h"

namespace tini2p
{
namespace ecies
{
/// @brief ECIES-X25519 Noise protocol name: "Noise_IKelg2+hs2_25519_ChaChaPoly_SHA256"
static const crypto::FixedSecBytes<40> ECIES_PROTOCOL_NAME{0x4e, 0x6f, 0x69, 0x73, 0x65, 0x5f, 0x49, 0x4b, 0x65, 0x6c,
                                                           0x67, 0x32, 0x2b, 0x68, 0x73, 0x32, 0x5f, 0x32, 0x35, 0x35,
                                                           0x31, 0x39, 0x5f, 0x43, 0x68, 0x61, 0x43, 0x68, 0x61, 0x50,
                                                           0x6f, 0x6c, 0x79, 0x5f, 0x53, 0x48, 0x41, 0x32, 0x35, 0x36};

/// @brief ECIES-X25519 Initial chain key, same for all outbound sessions
static const crypto::FixedSecBytes<32> ECIES_INITIAL_CHAIN_KEY{
    0x4c, 0xaf, 0x11, 0xef, 0x2c, 0x8e, 0x36, 0x56, 0x4c, 0x53, 0xe8, 0x88, 0x85, 0x06, 0x4d, 0xba,
    0xac, 0xbe, 0x00, 0x54, 0xad, 0x17, 0x8f, 0x80, 0x79, 0xa6, 0x46, 0x82, 0x7e, 0x6e, 0xe4, 0x0c};

/// @brief ECIES-X25519 Initial h, same for all outbound sessions
static const crypto::FixedSecBytes<32> ECIES_INITIAL_H{0x9c, 0xcf, 0x85, 0x2c, 0xc9, 0x3b, 0xb9, 0x50, 0x44, 0x41, 0xe9,
                                                       0x50, 0xe0, 0x1d, 0x52, 0x32, 0x2e, 0x0d, 0x47, 0xad, 0xd1, 0xe9,
                                                       0xa5, 0x55, 0xf7, 0x55, 0xb5, 0x69, 0xae, 0x18, 0x3b, 0x5c};

/// @brief Reply tagset context
static const crypto::KDFContext<crypto::HmacSha256> ECIES_REPLY_TAGSET_CTX{std::string("SessionReplyTags")};

/// @class Keys
/// @brief ECIES key information
class Keys
{
 public:
  using curve_t = crypto::X25519;  //< Curve trait alias
  using elligator_t = crypto::Elligator2;  //< Elligator2 trait alias
  using sha_t = crypto::Sha256;  //< Sha256 trait alias

  /// @brief Default ctor
  Keys()
      : id_keys_(curve_t::create_keys()),
        ep_keys_(),
        remote_id_key_(),
        remote_ep_key_(),
        chain_key_(ECIES_INITIAL_CHAIN_KEY),
        k_(),
        oth_k_(),
        h_(ECIES_INITIAL_H)
  {
  }

  /// @brief Copy-ctor
  Keys(const Keys& oth)
      : id_keys_(oth.id_keys_),
        ep_keys_(oth.ep_keys_),
        remote_id_key_(oth.remote_id_key_),
        remote_ep_key_(oth.remote_ep_key_),
        chain_key_(oth.chain_key_),
        k_(oth.k_),
        oth_k_(oth.oth_k_),
        h_(oth.h_)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_keys(id_keys_, ex);

    if (id_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(id_keys_);
  }

  /// @brief Move-ctor
  Keys(Keys&& oth)
      : id_keys_(std::move(oth.id_keys_)),
        ep_keys_(std::move(oth.ep_keys_)),
        remote_id_key_(std::move(oth.remote_id_key_)),
        remote_ep_key_(std::move(oth.remote_ep_key_)),
        chain_key_(std::move(oth.chain_key_)),
        k_(std::move(oth.k_)),
        oth_k_(std::move(oth.oth_k_)),
        h_(std::move(oth.h_))
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_keys(id_keys_, ex);

    if (id_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(id_keys_);

    if (!ep_keys_.pvtkey.is_zero() && ep_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(ep_keys_);
  }

  /// @brief Create ECIES keys from a private static key
  explicit Keys(curve_t::pvtkey_t id_key)
      : ep_keys_(), remote_id_key_(), remote_ep_key_(), chain_key_(), k_(), oth_k_(), h_()
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    if (id_key.is_zero())
      ex.throw_ex<std::invalid_argument>("null local static private key");

    id_keys_.pvtkey = std::forward<curve_t::pvtkey_t>(id_key);
    curve_t::PrivateToPublic(id_keys_);
  }

  /// @brief Create ECIES Keys with local identity keypair
  /// @param id_keys Local long-term identity keypair
  explicit Keys(curve_t::keypair_t id_keys)
      : ep_keys_(), remote_id_key_(), remote_ep_key_(), chain_key_(), k_(), oth_k_(), h_()
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_keys(id_keys, ex);

    id_keys_ = std::forward<curve_t::keypair_t>(id_keys);

    if (id_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(id_keys_);
  }

  /// @brief Create ECIES Keys with local identity keypair
  /// @param id_keys Local long-term identity keypair
  /// @param id_keys Local ephemeral keypair
  Keys(curve_t::keypair_t id_keys, curve_t::keypair_t ep_keys)
      : ep_keys_(), remote_id_key_(), remote_ep_key_(), chain_key_(), k_(), oth_k_(), h_()
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_keys(id_keys, ex);
    check_keys(ep_keys, ex);

    id_keys_ = std::forward<curve_t::keypair_t>(id_keys);
    ep_keys_ = std::forward<curve_t::keypair_t>(ep_keys);

    if (id_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(id_keys_);

    if (ep_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(ep_keys_);
  }

  /// @brief Create ECIES Keys with local identity keypair
  /// @param id_keys Local long-term identity keypair
  /// @param id_keys Local ephemeral keypair
  Keys(curve_t::keypair_t id_keys, elligator_t::keypair_t ep_keys)
      : ep_keys_(), remote_id_key_(), remote_ep_key_(), chain_key_(), k_(), oth_k_(), h_()
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_keys(id_keys, ex);
    check_keys(ep_keys, ex);

    id_keys_ = std::forward<curve_t::keypair_t>(id_keys);
    ep_keys_.pvtkey = std::forward<curve_t::pvtkey_t>(static_cast<curve_t::pvtkey_t>(ep_keys.pvtkey));
    ep_keys_.pubkey = std::forward<curve_t::pubkey_t>(static_cast<curve_t::pubkey_t>(ep_keys.pubkey));

    if (id_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(id_keys_);

    if (ep_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(ep_keys_);
  }

  /// @brief Create an ECIES Keys from remote static + ephemeral public keys
  /// @note Creates fresh local static + ephemeral keys
  /// @param remote_id_pk Remote static public key
  /// @param remote_ep_pk Remote ephemeral public key
  Keys(curve_t::pubkey_t remote_id_pk, curve_t::pubkey_t remote_ep_pk)
      : id_keys_(curve_t::create_keys()), ep_keys_(curve_t::create_keys()), chain_key_(), k_(), oth_k_(), h_()
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    if (remote_id_key_.is_zero())
      ex.throw_ex<std::invalid_argument>("null remote static public key");

    if (remote_ep_key_.is_zero())
      ex.throw_ex<std::invalid_argument>("null remote ephemeral public key");

    remote_id_key_ = std::forward<curve_t::pubkey_t>(remote_id_pk);
    remote_ep_key_ = std::forward<curve_t::pubkey_t>(remote_ep_pk);
  }

  /// @brief Create fully initialized ECIES Keys
  /// @param id_keys Local static keypair
  /// @param remote_id_pk Remote static public key
  /// @param remote_ep_pk Remote ephemeral public key
  Keys(curve_t::keypair_t id_keys, curve_t::pubkey_t remote_id_key, curve_t::pubkey_t remote_ep_key)
      : ep_keys_(), chain_key_(), k_(), oth_k_(), h_()
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_keys(id_keys, ex);

    id_keys_ = std::forward<curve_t::keypair_t>(id_keys);

    if (id_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(id_keys_);

    if (remote_id_key.is_zero())
      ex.throw_ex<std::invalid_argument>("null remote static public key");

    remote_id_key_ = std::forward<curve_t::pubkey_t>(remote_id_key);

    if (remote_ep_key.is_zero())
      ex.throw_ex<std::invalid_argument>("null remote ephemeral public key");

    remote_ep_key_ = std::forward<curve_t::pubkey_t>(remote_ep_key);
  }

  /// @brief Forwarding-assignment operator
  Keys& operator=(Keys oth)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_keys(oth.id_keys_, ex);

    id_keys_ = std::forward<curve_t::keypair_t>(oth.id_keys_);

    if (id_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(id_keys_);

    ep_keys_ = std::forward<curve_t::keypair_t>(oth.ep_keys_);

    if (!ep_keys_.pvtkey.is_zero() && ep_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(ep_keys_);

    remote_id_key_ = std::forward<curve_t::pubkey_t>(oth.remote_id_key_);
    remote_ep_key_ = std::forward<curve_t::pubkey_t>(oth.remote_ep_key_);

    chain_key_ = std::forward<curve_t::shrkey_t>(oth.chain_key_);
    k_ = std::forward<curve_t::shrkey_t>(oth.k_);
    oth_k_ = std::forward<curve_t::shrkey_t>(oth.oth_k_);
    h_ = std::forward<sha_t::digest_t>(oth.h_);

    return *this;
  }

  /// @brief Get a const reference to the identity public key
  const curve_t::pubkey_t& pubkey() const noexcept
  {
    return id_keys_.pubkey;
  }

  /// @brief Get a non-const reference to the identity public key
  curve_t::pubkey_t& pubkey() noexcept
  {
    return id_keys_.pubkey;
  }

  /// @brief Get local static keys
  const curve_t::keypair_t& id_keys() const noexcept
  {
    return id_keys_;
  }

  /// @brief Set the ephemeral keys
  Keys& id_keys(curve_t::keypair_t keys)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_keys(keys, ex);
    id_keys_ = std::forward<curve_t::keypair_t>(keys);

    if (id_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(id_keys_);

    return *this;
  }

  /// @brief Get local ephemeral keys
  const curve_t::keypair_t& ep_keys() const noexcept
  {
    return ep_keys_;
  }

  /// @brief Set the ephemeral keys
  Keys& ep_keys(curve_t::keypair_t keys)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_keys(keys, ex);
    ep_keys_ = std::forward<curve_t::keypair_t>(keys);

    if (ep_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(ep_keys_);

    return *this;
  }

  /// @brief Set the ephemeral keys (convert from Elligator2)
  Keys& ep_keys(elligator_t::keypair_t keys)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_keys(keys, ex);
    ep_keys_ = curve_t::keypair_t{static_cast<curve_t::pubkey_t>(std::forward<elligator_t::pubkey_t>(keys.pubkey)),
                                  static_cast<curve_t::pvtkey_t>(std::forward<elligator_t::pvtkey_t>(keys.pvtkey))};

    if (ep_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(ep_keys_);

    return *this;
  }

  /// @brief Get a const reference to the identity public key
  const curve_t::pubkey_t& remote_id_key() const noexcept
  {
    return remote_id_key_;
  }

  /// @brief Get a const reference to the identity public key
  const curve_t::pubkey_t& remote_ep_key() const noexcept
  {
    return remote_ep_key_;
  }

  /// @brief Set remote static public key
  void remote_id_key(curve_t::pubkey_t key)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    if (key.is_zero())
      ex.throw_ex<std::invalid_argument>("null remote static key.");

    remote_id_key_ = std::forward<curve_t::pubkey_t>(key);
  }

  /// @brief Set remote ephemeral public key
  void remote_ep_key(curve_t::pubkey_t key)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    if (key.is_zero())
      ex.throw_ex<std::invalid_argument>("null remote ephemeral key.");

    remote_ep_key_ = std::forward<curve_t::pubkey_t>(key);
  }

  /// @brief Get a const reference to the chain key
  const curve_t::shrkey_t& chain_key() const noexcept
  {
    return chain_key_;
  }

  /// @brief Get a non-const reference to the chain key
  curve_t::shrkey_t& chain_key() noexcept
  {
    return chain_key_;
  }

  /// @brief Set the chain key
  Keys& chain_key(curve_t::shrkey_t key)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_chain_key(key, ex);

    chain_key_ = std::forward<curve_t::shrkey_t>(key);

    return *this;
  }

  /// @brief Get a const reference to the message key
  const curve_t::shrkey_t& k() const noexcept
  {
    return k_;
  }

  /// @brief Get a non-const reference to the message key
  curve_t::shrkey_t& k() noexcept
  {
    return k_;
  }

  /// @brief Set the message key
  Keys& k(curve_t::shrkey_t k)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_k(k, ex);

    k_ = std::forward<curve_t::shrkey_t>(k);

    return *this;
  }

  /// @brief Get a const reference to the cipherstate k for the opposite direction
  /// @detail This is set after calling Split():
  ///    - for inbound sessions this is Noise temp_k2
  ///    - for outbound sessions this is Noise temp_k1
  const curve_t::shrkey_t& oth_k() const noexcept
  {
    return oth_k_;
  }

  /// @brief Get a const reference to the cipherstate k for the opposite direction
  /// @detail This is set after calling Split():
  ///    - for inbound sessions this is Noise temp_k2
  ///    - for outbound sessions this is Noise temp_k1
  curve_t::shrkey_t& oth_k() noexcept
  {
    return oth_k_;
  }

  /// @brief Securely clear the other cipherstate k once it's no longer needed
  Keys& clear_oth_k()
  {
    crypto::RandBytes(oth_k_);
    oth_k_.zero();

    return *this;
  }

  /// @brief Set the cipherstate k for the opposite direction
  /// @detail This is set after calling Split():
  ///    - for inbound sessions this is Noise temp_k2
  ///    - for outbound sessions this is Noise temp_k1
  Keys& oth_k(curve_t::shrkey_t k)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_k(k, ex);

    oth_k_ = std::forward<curve_t::shrkey_t>(k);

    return *this;
  }

  /// @brief Get a const reference to the chain hash
  const sha_t::digest_t& h() const noexcept
  {
    return h_;
  }

  /// @brief Get a non-const reference to the chain hash
  sha_t::digest_t& h() noexcept
  {
    return h_;
  }

  /// @brief Set the chain hash
  Keys& h(sha_t::digest_t h)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    check_h(h, ex);

    h_ = std::forward<sha_t::digest_t>(h);

    return *this;
  }

  /// @brief Set remote identity and ephemeral public keys
  void remote_rekey(curve_t::pubkey_t r_id_key, curve_t::pubkey_t r_ep_key)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    if (r_id_key.is_zero())
      ex.throw_ex<std::invalid_argument>("null remote static key");

    if (r_ep_key.is_zero())
      ex.throw_ex<std::invalid_argument>("null remote ephemeral key");

    remote_id_key_ = std::forward<curve_t::pubkey_t>(r_id_key);
    remote_ep_key_ = std::forward<curve_t::pubkey_t>(r_ep_key);
  }

  /// @brief Reset local static keypair and generate new ephemeral keypair
  /// @detail Requires a DH ratchet, and sending a "Next DH Ratchet Public Key" message
  void rekey(curve_t::keypair_t keys)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    if (keys.pvtkey.is_zero())
      ex.throw_ex<std::invalid_argument>("null local static private key.");

    id_keys_ = std::forward<curve_t::keypair_t>(keys);

    if (id_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(id_keys_);

    ep_keys_ = curve_t::create_keys();
  }

  /// @brief Reset local static keypair and generate new ephemeral keypair
  /// @detail Requires a DH ratchet
  Keys& rekey(curve_t::keypair_t id_keys, curve_t::keypair_t ep_keys)
  {
    const exception::Exception ex{"ECIES: Keys", __func__};

    if (id_keys.pvtkey.is_zero())
      ex.throw_ex<std::invalid_argument>("null local static private key.");

    if (ep_keys.pvtkey.is_zero())
      ex.throw_ex<std::invalid_argument>("null local ephemeral private key.");

    id_keys_ = std::forward<curve_t::keypair_t>(id_keys);

    if (id_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(id_keys_);

    ep_keys_ = std::forward<curve_t::keypair_t>(ep_keys);

    if (ep_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(ep_keys_);

    return *this;
  }

  /// @brief Reset local static keypair and remote static + ephemeral keys
  /// @param keys Local static keypair
  /// @param id_key Remote static public key
  /// @param ep_key Remote ephemeral public key
  void rekey(curve_t::keypair_t keys, curve_t::pubkey_t id_key, curve_t::pubkey_t ep_key)
  {
    rekey(std::forward<curve_t::keypair_t>(keys));
    remote_id_key(std::forward<curve_t::pubkey_t>(id_key));
    remote_ep_key(std::forward<curve_t::pubkey_t>(ep_key));
  }

  /// @brief Equality comparison with another ECIES Keys
  std::uint8_t operator==(const Keys& oth) const
  {  // attempt constant-time comparison
    const auto& id_eq = static_cast<std::uint8_t>(id_keys_ == oth.id_keys_);
    const auto& ep_eq = static_cast<std::uint8_t>(ep_keys_ == oth.ep_keys_);
    const auto& rem_id_eq = static_cast<std::uint8_t>(remote_id_key_ == oth.remote_id_key_);
    const auto& rem_ep_eq = static_cast<std::uint8_t>(remote_ep_key_ == oth.remote_ep_key_);

    return (id_eq * ep_eq * rem_id_eq * rem_ep_eq);
  }

 private:
  void check_keys(const curve_t::keypair_t& keys, const exception::Exception& ex) const
  {
    if (keys.pvtkey.is_zero())
      ex.throw_ex<std::invalid_argument>("null private key.");

    if (!keys.pubkey.is_zero() && !curve_t::ValidPublicKey(keys))
      ex.throw_ex<std::invalid_argument>("invalid public key.");
  }

  void check_keys(const elligator_t::keypair_t& keys, const exception::Exception& ex) const
  {
    if (keys.pvtkey.is_zero())
      ex.throw_ex<std::invalid_argument>("null private key.");

    if (!keys.pubkey.is_zero() && !elligator_t::ValidPublicKey(keys))
      ex.throw_ex<std::invalid_argument>("invalid public key.");
  }

  void check_chain_key(const curve_t::shrkey_t& chain_key, const exception::Exception& ex) const
  {
    if (chain_key.is_zero())
      ex.throw_ex<std::logic_error>("null chain key");
  }

  void check_k(const curve_t::shrkey_t& k, const exception::Exception& ex) const
  {
    if (k.is_zero())
      ex.throw_ex<std::logic_error>("null k");
  }

  void check_h(const sha_t::digest_t& h, const exception::Exception& ex) const
  {
    if (h.is_zero())
      ex.throw_ex<std::logic_error>("null h");
  }

  curve_t::keypair_t id_keys_;
  curve_t::keypair_t ep_keys_;
  curve_t::pubkey_t remote_id_key_;
  curve_t::pubkey_t remote_ep_key_;
  curve_t::shrkey_t chain_key_;
  curve_t::shrkey_t k_, oth_k_;
  sha_t::digest_t h_;
};
}  // namespace ecies
}  // namespace tini2p

#endif  // SRC_ECIES_KEYS_H_
