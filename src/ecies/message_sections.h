/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ECIES_MESSAGE_SECTIONS_H_
#define SRC_ECIES_MESSAGE_SECTIONS_H_

#include <mutex>
#include <variant>
#include <shared_mutex>

#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/elligator2.h"

#include "src/data/blocks/date_time.h"
#include "src/data/blocks/ecies_options.h"
#include "src/data/blocks/garlic.h"
#include "src/data/blocks/i2np.h"
#include "src/data/blocks/message_number.h"
#include "src/data/blocks/new_dh_key.h"
#include "src/data/blocks/new_session_ack.h"
#include "src/data/blocks/padding.h"

#include "src/ecies/keys.h"

namespace tini2p
{
namespace ecies
{
/// @struct SectionKeyInfo
/// @brief Section key information for new session message
struct SectionKeyInfo
{
  using sha_t = crypto::Sha256;  //< Sha256 trait alias
  using x25519_t = crypto::X25519;  //< X25519 trait alias
  using chain_key_t = x25519_t::shrkey_t;  //< Chain key trait alias
  using k_t = x25519_t::shrkey_t;  //< Section key trait alias
  using n_t = crypto::ChaChaPoly1305::nonce_t;  //< Nonce trait alias
  using ad_t = sha_t::digest_t;  //< Additional data trait alias
  using h_t = sha_t::digest_t;  //< H trait alias

  /// @brief Equality comparison with another SectionKeyInfo
  std::uint8_t operator==(const SectionKeyInfo& oth) const
  {  // attempt constant-time comparison
    const auto& chain_eq = static_cast<std::uint8_t>(chain_key == oth.chain_key);
    const auto& k_eq = static_cast<std::uint8_t>(k == oth.k);
    const auto& n_eq = static_cast<std::uint8_t>(n == oth.n);
    const auto& ad_eq = static_cast<std::uint8_t>(ad == oth.ad);
    const auto& h_eq = static_cast<std::uint8_t>(h == oth.h);

    return (chain_eq * k_eq * n_eq * ad_eq * h_eq);
  }

#if defined(TINI2P_TESTS)
  /// @brief Create the initial chain key and h for ECIES sessions
  /// @detail Only used for testing, non-test builds use static constants of the resulting chain key + h
  static std::pair<chain_key_t, h_t> InitializeChainKey()
  {
    chain_key_t chain_key;
    h_t h;
    // chainKey = SHA256("Noise_IKelg2_25519_ChaChaPoly_SHA256"), see spec
    sha_t::Hash(chain_key, ECIES_PROTOCOL_NAME);
    // h = SHA256(chainKey), see spec
    sha_t::Hash(h, chain_key);

    return std::make_pair(chain_key, h);
  }
#endif

  chain_key_t chain_key;
  k_t k;
  n_t n;
  ad_t ad;
  h_t h;
};

/// @class EphemeralKeySection
/// @brief Ephemeral key section (unencrypted) in NewSession/NewSessionReply message
class EphemeralKeySection
{
 public:
  using elligator_t = crypto::Elligator2;  //< Elligator2 trait alias
  using key_t = elligator_t::pubkey_t;  //< Key trait alias
  using encoded_key_t = elligator_t::encoded_key_t;  //< Elligator2 encoded key trait alias
  using key_info_t = SectionKeyInfo;  //< Section key info trait alias
  using buffer_t = crypto::FixedSecBytes<elligator_t::PublicKeyLen>;  //< Buffer trait alias

  /// @brief Default ctor, creates uninitialized EphemeralKeySection
  /// @detail Must set static key before serializing
  EphemeralKeySection() : key_(), buf_() {}

  /// @brief Deserializing ctor, creates an EphemeralKeySection from a secure buffer
  explicit EphemeralKeySection(buffer_t buf) : key_(), buf_()
  {
    const exception::Exception ex{"EphemeralKeySection", __func__};

    check_buffer(buf, ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Serialize EphemeralKeySection to buffer
  EphemeralKeySection& serialize()
  {
    const exception::Exception ex{"EphemeralKeySection", __func__};

    check_key(key_, ex);

    tini2p::BytesWriter<buffer_t> writer(buf_);
    writer.write_data(elligator_t::Encode(key_));

    return *this;
  }

  /// @brief Deserialize EphemeralKeySection from buffer
  EphemeralKeySection& deserialize()
  {
    const exception::Exception ex{"EphemeralKeySection", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    encoded_key_t key;
    reader.read_data(key);
    key_ = elligator_t::Decode(key);

    return *this;
  }

  /// @brief Get a const reference to the Elligator2 ephemeral key
  const key_t& key() const noexcept
  {
    return key_;
  }

  /// @brief Set the Elligator2 ephemeral key
  EphemeralKeySection& key(key_t key)
  {
    const exception::Exception ex{"EphemeralKeySection", __func__};

    check_key(key, ex);

    key_ = std::forward<key_t>(key);

    return *this;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Get the size of the unencrypted EphemeralKeySection
  decltype(auto) size() const
  {
    return tini2p::under_cast(elligator_t::PublicKeyLen);
  }

  /// @brief Get the size of the encrypted EphemeralKeySection
  decltype(auto) encrypted_len() const
  {
    return tini2p::under_cast(elligator_t::PublicKeyLen);
  }

  /// @brief Equality comparison with another EphemeralKeySection
  std::uint8_t operator==(const EphemeralKeySection& oth) const
  {  // attempt constant-time comparison
    const auto& key_eq = static_cast<std::uint8_t>(key_ == oth.key_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    return (key_eq * buf_eq);
  }

 private:
  void check_key(const elligator_t::pubkey_t& key, const exception::Exception& ex)
  {
    if (key.is_zero())
      ex.throw_ex<std::logic_error>("null public key");

    if (elligator_t::ValidElligator(key) == 0)
      ex.throw_ex<std::logic_error>("invalid Elligator2 public key");
  }

  void check_buffer(const buffer_t& buf, const exception::Exception& ex)
  {
    if (buf.is_zero())
      ex.throw_ex<std::logic_error>("null buffer");
  }

  key_t key_;
  buffer_t buf_;
};

/// @class StaticKeySection
/// @brief Static key section (unencrypted) in NewSession/NewSessionReply message
class StaticKeySection
{
 public:
  enum
  {
    KeyLen = crypto::X25519::PublicKeyLen,
    SectionLen = 32,
    MinLen = SectionLen,
    MaxLen = 48,  // SectionLen(32) + Poly1305::MACLen(16)
  };

  using key_t = crypto::X25519::pubkey_t;  //< Key trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief One-time ctor, creates an StaticKeySection in OneTimeKey mode
  /// @detail OneTimeKey mode includes a zeroed ephemeral key, and no following static key section
  /// @param pay_len Encrypted ReplyPayloadSection length including the MAC
  StaticKeySection() : key_(), buf_()
  {
    serialize();
  }

  /// @brief Copy-ctor
  StaticKeySection(const StaticKeySection& oth) : key_(oth.key_), buf_(oth.buf_) {}

  /// @brief Move-ctor
  StaticKeySection(StaticKeySection&& oth) : key_(std::move(oth.key_)), buf_(std::move(oth.buf_)) {}

  /// @brief Fully-initializing ctor, creates an StaticKeySection with the given flags and key
  /// @detail Flags default to a bound session (StaticKeySection follows)
  /// @param key_in Ephemeral public key for DH-encryption of StaticKeySection and ReplyPayloadSection
  explicit StaticKeySection(key_t key) : key_(std::forward<key_t>(key)), buf_()
  {
    serialize();
  }

  /// @brief Deserializing ctor, creates an StaticKeySection from a secure buffer
  explicit StaticKeySection(buffer_t buf)
  {
    const exception::Exception ex{"StaticKeySection", __func__};

    check_buffer(buf, ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Copy-assignment operator
  void operator=(const StaticKeySection& oth)
  {
    key_ = oth.key_;
    buf_ = oth.buf_;
  }

  /// @brief Serialize the StaticKeySection to buffer
  StaticKeySection& serialize()
  {
    const exception::Exception ex{"StaticKeySection", __func__};

    buf_.resize(tini2p::under_cast(SectionLen));

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_data(key_);

    return *this;
  }

  /// @brief Deserialize the StaticKeySection from buffer
  StaticKeySection& deserialize()
  {
    const exception::Exception ex{"StaticKeySection", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    key_t key;
    reader.read_data(key);
    key_ = std::move(key);

    // reclaim unused space (possibly from unused MAC after decryption)
    buf_.resize(reader.count());
    buf_.shrink_to_fit();

    return *this;
  }

  /// @brief Get a const reference to the key
  const key_t& key() const noexcept
  {
    return key_;
  }

  /// @brief Set the key and flags
  /// @param key Ephemeral key for the section
  /// @param flags Flags for the section (defaults to Bound session, static key section follows)
  StaticKeySection& key(key_t key)
  {
    key_ = std::forward<key_t>(key);

    return serialize();
  }

  /// @brief Get whether a static key is included
  std::uint8_t has_static_key() const
  {
    return !key_.is_zero();
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Get the size of the unencrypted StaticKeySection
  decltype(auto) size() const noexcept
  {
    return tini2p::under_cast(MinLen);
  }

  /// @brief Get the size of the encrypted StaticKeySection
  decltype(auto) encrypted_len() const noexcept
  {
    return tini2p::under_cast(MaxLen);
  }

  /// @brief Equality comparison with another StaticKeySection
  std::uint8_t operator==(const StaticKeySection& oth) const
  {  // attempt constant-time comparison
    const auto& key_eq = static_cast<std::uint8_t>(key_ == oth.key_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    return (key_eq * buf_eq);
  }

 private:
  void check_buffer(const buffer_t buf, const exception::Exception& ex) const
  {
    const auto& buf_len = buf.size();
    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  key_t key_;
  buffer_t buf_;
};

/// @class PayloadSection
/// @brief Payload section (unencrypted) in NewSession message
class PayloadSection
{
 public:
  enum
  {
    ElgNewSessionLen = 642,
    TunnelMsgLen = 1024,
    MinPayloadLen = 7,  //< DateTime (7)
    MaxPayloadLen = 65431,  //< MaxECIESLen (65519) - (OneTimeKey (32) + EphemeralSection (40) + MAC (16))
    MinBlocksLen = 1,  //< see spec for required blocks
    MinPadLen = 617,  //< ElG(642) - MinPayload(25)
    MaxPadLen = 999,  //< Tunnel(1024) - MinPayload(25)
  };

  /// @brief Block order indices
  enum struct Index
  {
    DateTime,
    Additional,
  };

  /// @brief Payload block variant trait alias
  using payload_v = std::variant<
      data::DateTimeBlock,
      data::ECIESOptionsBlock,
      data::GarlicBlock,
      data::PaddingBlock>;
  using payload_blocks_t = std::vector<payload_v>;  //< Payload blocks container trait alias
  using key_info_t = SectionKeyInfo;  //< Section key info trait alias
  using size_type = std::uint16_t;  //< Size type trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor, creates a fully-initialized default PayloadSection
  PayloadSection()
      : blocks_{{data::DateTimeBlock()}, {data::PaddingBlock(get_padding_len())}},
        buf_(tini2p::under_cast(MinPayloadLen))
  {
    serialize();
  }

  /// @brief Fully-initializing ctor, creates a PayloadSection from an I2NP block & Ephemeral Key Section flags
  PayloadSection(data::GarlicBlock garlic_block)
      : blocks_{{data::DateTimeBlock()},
                {data::ECIESOptionsBlock()},
                {std::forward<data::GarlicBlock>(garlic_block)}},
        buf_()
  {
    const exception::Exception ex{"PayloadSection", __func__};

    const auto& i2np_len = std::visit([](const auto& b){ return b.size(); }, blocks_.back());
    size_type pad_len(0);
    const auto rand_pad_len = get_padding_len();

    if (i2np_len < rand_pad_len)
      pad_len = rand_pad_len - i2np_len;
    else if (rand_pad_len < i2np_len)
      pad_len = i2np_len - rand_pad_len;
    else
      pad_len = rand_pad_len;

    blocks_.emplace_back(data::PaddingBlock(pad_len));
    
    serialize();
  }

  /// @brief Copy-ctor
  PayloadSection(const PayloadSection& oth) : blocks_(oth.blocks_), buf_(oth.buf_) {}

  /// @brief Move-ctor
  PayloadSection(PayloadSection&& oth) : blocks_(std::move(oth.blocks_)), buf_(std::move(oth.buf_)) {}

  /// @brief Deserializing ctor, creates a PayloadSection from a secure buffer
  explicit PayloadSection(buffer_t buf)
  {
    const exception::Exception ex{"PayloadSection", __func__};

    check_buffer(buf, ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Forwarding-assignment operator
  PayloadSection& operator=(PayloadSection oth)
  {
    buf_ = std::forward<buffer_t>(oth.buf_);

    std::scoped_lock bgd(blocks_mutex_);
    blocks_ = std::forward<payload_blocks_t>(oth.blocks_);

    return *this;
  }

  /// @brief Serialize the PayloadSection to buffer
  PayloadSection& serialize()
  {
    const exception::Exception ex{"PayloadSection", __func__};

    // lock the member blocks, and check their order + total size
    std::scoped_lock bgd(blocks_mutex_);

    check_blocks_order(blocks_, ex);

    // serialize the member blocks
    for (auto& block : blocks_)
      std::visit([](auto& b) { b.serialize(); }, block);

    check_blocks_size(blocks_, ex);

    buf_.resize(blocks_size(blocks_));

    tini2p::BytesWriter<buffer_t> writer(buf_);

    for (const auto& block : blocks_)
      std::visit([&writer](const auto& b) { writer.write_data(b.buffer()); }, block);

    return *this;
  }

  /// @brief Deserialize the PayloadSection from buffer
  /// @detail Caller must resize PayloadSection buffer based on new session message length
  PayloadSection& deserialize()
  {
    const exception::Exception ex{"PayloadSection", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    std::size_t block_ind(0);
    std::uint8_t padding_seen(0);
    payload_blocks_t blocks;
    while (reader.gcount())
      {
        if (padding_seen != 0)
          ex.throw_ex<std::logic_error>("padding block must be the final block.");

        // read and check the block type
        data::Block::Type type;
        reader.read_bytes(type);
        padding_seen += check_block_type(type, static_cast<Index>(block_ind), ex);

        // read the block size, and skip back to beginning of the block
        data::Block::size_type size;
        const auto& header_len = tini2p::under_cast(data::Block::HeaderLen);
        reader.read_bytes(size, tini2p::Endian::Big);
        reader.skip_back(header_len);

        data::Block::buffer_t buf(header_len + size);
        reader.read_data(buf);
        add_to_blocks(blocks, type, buf, ex);

        // increase the block index
        ++block_ind;
      }

    // lock member blocks, and swap with the temp container
    std::scoped_lock bgd(blocks_mutex_);
    blocks_.swap(blocks);

    return *this;
  }

  /// @brief Get the size of the unencrypted PayloadSection
  size_type size()
  {
    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return blocks_size(blocks_);
  }

  /// @brief Get the size of the encrypted PayloadSection
  size_type encrypted_len()
  {
    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return blocks_size(blocks_) + tini2p::under_cast(crypto::ChaChaPoly1305::MACLen);
  }

  /// @brief Get whether the message contains a block of the given type
  /// @tparam TBlock Block type to search for
  template <class TBlock>
  std::uint8_t has_block()
  {
    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return has_block<TBlock>(blocks_);
  }

  /// @brief Get a const reference to a block of the given type
  /// @tparam TBlock Block type to search for
  /// @throws Runtime error if the message does not contain a block of the given type
  template <class TBlock>
  const TBlock& get_block()
  {
    const exception::Exception ex{"PayloadSection", __func__};

    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return get_block<TBlock>(blocks_, ex);
  }

  /// @brief Extract a block of the given type from the message
  /// @tparam TBlock Block type to extract
  /// @throws Runtime error if the message does not contain a block of the given type
  template <class TBlock>
  TBlock extract_block()
  {
    const exception::Exception ex{"PayloadSection", __func__};

    std::scoped_lock sgd(blocks_mutex_);

    return extract_block<TBlock>(blocks_, ex);
  }

  /// @brief Set the payload blocks
  PayloadSection& blocks(payload_blocks_t blocks)
  {
    const exception::Exception ex{"PayloadSection", __func__};

    check_blocks_order(blocks, ex);

    {  // lock blocks
      std::scoped_lock sgd(blocks_mutex_);

      blocks_ = std::forward<payload_blocks_t>(blocks);
    }  // end blocks lock

    return serialize();
  }

  /// @brief Add a block to the payload
  /// @detail Block can either be an GarlicBlock or PaddingBlock.
  /// @note Does not convert from a One-time payload to repliable or bound payload.
  ///    If the caller wishes to change the payload type, a new PayloadSection must be created.
  template <
      class TBlock,
      typename = std::enable_if_t<
          std::is_same<TBlock, data::ECIESOptionsBlock>::value || std::is_same<TBlock, data::GarlicBlock>::value
          || std::is_same<TBlock, data::PaddingBlock>::value>>
  PayloadSection& add_block(TBlock block)
  {
    const exception::Exception ex{"PayloadSection", __func__};

    std::scoped_lock bgd(blocks_mutex_);

    const auto& block_len = block.size();
    auto blocks_len = blocks_size(blocks_);
    data::PaddingBlock::size_type max_len(tini2p::under_cast(MaxPayloadLen));

    if (block_len + blocks_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "adding block exceeds max length: " + std::to_string(max_len)
            + ", current len: " + std::to_string(blocks_len) + ", block len: " + std::to_string(block_len));
      }
    else
      {
        if (std::holds_alternative<data::PaddingBlock>(blocks_.back()))
          {
            if (std::is_same<TBlock, data::PaddingBlock>::value)
              ex.throw_ex<std::logic_error>("multiple padding blocks not allowed.");
            else  // add block to the second-to-last position
              blocks_.insert(blocks_.end() - 1, std::forward<TBlock>(block));
          }
        else  // no padding block present, add block to the back
          blocks_.emplace_back(std::forward<TBlock>(block));
      }

    return *this;
  }

  /// @brief Removes padding block, if present
  PayloadSection& remove_padding()
  {
    std::scoped_lock bgd(blocks_mutex_);

    if (has_block<data::PaddingBlock>(blocks_))
      blocks_.pop_back();

    return *this;
  }

  std::uint8_t is_unbound()
  {
    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return get_block<data::ECIESOptionsBlock>().is_unbound();
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another PayloadSection
  std::uint8_t operator==(const PayloadSection& oth)
  {  // attempt constant-time comparison
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);
    const auto& blocks_eq = static_cast<std::uint8_t>(blocks_ == oth.blocks_);

    return (blocks_eq * buf_eq);
  }

 private:
  std::uint8_t check_block_type(const data::Block::Type& type, const Index& index, const exception::Exception& ex)
  {
    if (index == Index::DateTime && type != data::Block::Type::DateTime)
      ex.throw_ex<std::logic_error>("invalid block type at DateTime index.");

    if (tini2p::under_cast(index) >= tini2p::under_cast(Index::Additional) && type != data::Block::Type::ECIESOptions
        && type != data::Block::Type::Garlic && type != data::Block::Type::Padding)
      {
        const auto& type_str = std::to_string(tini2p::under_cast(type));
        const auto& garlic_str = std::to_string(tini2p::under_cast(data::Block::Type::Garlic));
        const auto& opt_str = std::to_string(tini2p::under_cast(data::Block::Type::ECIESOptions));
        const auto& pad_str = std::to_string(tini2p::under_cast(data::Block::Type::Padding));

        ex.throw_ex<std::logic_error>(
            "invalid additional block type: " + type_str + ", must be Garlic: " + garlic_str + " or Options: " + opt_str
            + " or Padding: " + pad_str);
      }

    // return whether we have seen a padding block
    return static_cast<std::uint8_t>(type == data::Block::Type::Padding);
  }

  void check_blocks_order(const payload_blocks_t& blocks, const exception::Exception& ex)
  {
    // check required number of blocks is present
    const auto& blocks_len = blocks.size();
    const auto& min_len = tini2p::under_cast(MinBlocksLen);
    if (blocks_len < min_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid number of blocks: " + std::to_string(blocks_len) + ", min: " + std::to_string(min_len));
      }

    const auto block_type = [](const auto& b) { return b.type(); };

    // check order of blocks
    // required blocks (in order): DateTime, ECIESOptions, MessageNumber, NewDHKey (if not One-time) 
    // allowed additional blocks: I2NP & Padding
    // only one padding block is allowed, and it must be the last block.
    // no termination block is allowed, since this is a new session message,
    // and including a termination block makes no sense.
    std::size_t block_ind(0);
    std::uint8_t padding_seen = 0;
    for (const auto& block : blocks)
      {
        if (padding_seen != 0)
          ex.throw_ex<std::logic_error>("padding block must be the final block.");

        padding_seen += check_block_type(std::visit(block_type, block), static_cast<Index>(block_ind), ex);
        ++block_ind;
      }
  }

  void check_blocks_size(const payload_blocks_t& blocks, const exception::Exception& ex)
  {
    const auto& size = blocks_size(blocks);
    const auto& max_len = tini2p::under_cast(data::Block::MaxTotalLen);

    if (size > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid total payload length: " + std::to_string(size) + ", max: " + std::to_string(max_len));
      }
  }

  size_type blocks_size(const payload_blocks_t& blocks) const
  {
    size_type size(0);
    for (const auto& block : blocks)
      size += std::visit([](const auto& b){ return b.size(); }, block);

    return size;
  }

  // deserialize and add block to the supplied blocks container
  void add_to_blocks(
      payload_blocks_t& blocks,
      const data::Block::Type& type,
      const data::Block::buffer_t& buf,
      const exception::Exception& ex)
  {
    if (type == data::Block::Type::DateTime)
      blocks.emplace_back(data::DateTimeBlock(buf));
    else if (type == data::Block::Type::ECIESOptions)
      blocks.emplace_back(data::ECIESOptionsBlock(buf));
    else if (type == data::Block::Type::Garlic)
      blocks.emplace_back(data::GarlicBlock(buf));
    else if (type == data::Block::Type::Padding)
      blocks.emplace_back(data::PaddingBlock(buf));
    else
      ex.throw_ex<std::logic_error>("adding invalid block type.");
  }

  template <class TBlock>
  std::uint8_t has_block(const payload_blocks_t& blocks) const
  {
    const auto blocks_end = blocks.end();

    return static_cast<std::uint8_t>(
        std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); })
        != blocks_end);
  }

  template <class TBlock>
  const TBlock& get_block(const payload_blocks_t& blocks, const exception::Exception& ex) const
  {
    const auto blocks_end = blocks.end();

    auto it = std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); });

    if (it == blocks_end)
      ex.throw_ex<std::runtime_error>("no block found");

    return std::get<TBlock>(*it);
  }

  template <class TBlock>
  TBlock extract_block(payload_blocks_t& blocks, const exception::Exception& ex)
  {
    const auto blocks_end = blocks.end();

    auto it = std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); });

    if (it == blocks_end)
      ex.throw_ex<std::runtime_error>("no block found");

    auto block = std::move(*it);
    blocks_.erase(it);

    return std::get<TBlock>(block);
  }

  size_type get_padding_len()
  {
    return crypto::RandInRange(MinPadLen, MaxPadLen);
  }

  void check_buffer(const buffer_t& buf, const exception::Exception& ex) const
  {
    const auto& buf_len = buf.size();
    const auto& min_len = tini2p::under_cast(MinPayloadLen);
    const auto& max_len = tini2p::under_cast(MaxPayloadLen);

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  payload_blocks_t blocks_;
  std::shared_mutex blocks_mutex_;

  buffer_t buf_;
};

/// @class ReplyPayloadSection
/// @brief Payload section (unencrypted) in NewSessionReplyMessage
class ReplyPayloadSection
{
 public:
  enum
  {
    ElgNewSessionLen = 642,
    // TunnelDataPayload (1015) - (I2NPHeader(4) + GarlicDelivery(41) + GarlicCloveHeader(12) + KeyCertLen(4) + Poly1305MAC(16))
    MaxTunnelPayloadLen = 938,
    // EphemeralKey (32) + MinPay (6) + MAC (16)
    NewSessionReplyLen = 54,
    MinPayloadLen = 16,  //< DateTime (7) + ECIESOptions (9)
    MaxPayloadLen = 65431,  //< MaxTunnelPayloadLen (65519) - (OneTimeKey (32) + EphemeralSection (40) + MAC (16))
    MinBlocksLen = 1,  //< see spec for required blocks
    MinReplyPadLen = ElgNewSessionLen - NewSessionReplyLen,  //< match old ElGamal new session message length
    MaxReplyPadLen = MaxTunnelPayloadLen - NewSessionReplyLen,  //< match tunnel message length
  };

  /// @brief Block order indices
  enum struct Index
  {
    DateTime,
    Additional,
  };

  /// @brief ReplyPayload block variant trait alias
  using payload_v = std::variant<data::DateTimeBlock, data::ECIESOptionsBlock, data::GarlicBlock, data::PaddingBlock>;
  using payload_blocks_t = std::vector<payload_v>;  //< Payload blocks container trait alias
  using key_info_t = SectionKeyInfo;  //< Section key info trait alias
  using size_type = std::uint16_t;  //< Size type trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor
  ReplyPayloadSection() : blocks_{{data::DateTimeBlock()}, {data::ECIESOptionsBlock()}}, buf_()
  {
    serialize();
  }

  /// @brief Conversion ctor, creates a ReplyPayloadSection from an I2NPBlock
  /// @note Adds a PaddingBlock with random padding in valid range
  explicit ReplyPayloadSection(data::GarlicBlock garlic_block)
      : blocks_{{data::DateTimeBlock()}, {data::ECIESOptionsBlock()}, {std::forward<data::GarlicBlock>(garlic_block)}},
        buf_()
  {
    const exception::Exception ex{"ReplyPayloadSection", __func__};
    
    const auto& min_pad = tini2p::under_cast(MinReplyPadLen);
    const auto& max_pad =
        tini2p::under_cast(MaxReplyPadLen) - std::visit([](const auto& b) { return b.size(); }, blocks_.back());

    blocks_.emplace_back(data::PaddingBlock(crypto::RandInRange(min_pad, max_pad)));

    serialize();
  }

  /// @brief Copy-ctor
  ReplyPayloadSection(const ReplyPayloadSection& oth) : blocks_(oth.blocks_), buf_(oth.buf_)
  {
  }

  /// @brief Move-ctor
  ReplyPayloadSection(ReplyPayloadSection&& oth) : blocks_(std::move(oth.blocks_)), buf_(std::move(oth.buf_))
  {
  }

  /// @brief Deserializing ctor, creates a ReplyPayloadSection from a secure buffer
  explicit ReplyPayloadSection(buffer_t buf)
  {
    const exception::Exception ex{"ReplyPayloadSection", __func__};

    const auto& buf_len = buf.size();
    const auto& min_len = tini2p::under_cast(MinPayloadLen);
    const auto& max_len = tini2p::under_cast(MaxPayloadLen);
    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Forwarding-assignment operator
  ReplyPayloadSection& operator=(ReplyPayloadSection oth)
  {
    buf_ = std::forward<buffer_t>(oth.buf_);

    std::scoped_lock bgd(blocks_mutex_);
    blocks_ = std::forward<payload_blocks_t>(oth.blocks_);

    return *this;
  }

  /// @brief Serialize the ReplyPayloadSection to buffer
  ReplyPayloadSection& serialize()
  {
    const exception::Exception ex{"ReplyPayloadSection", __func__};

    // lock the member blocks, and check their order + total size
    std::scoped_lock bgd(blocks_mutex_);

    check_blocks_order(blocks_, ex);
    check_blocks_size(blocks_, ex);

    buf_.resize(blocks_size(blocks_));

    tini2p::BytesWriter<buffer_t> writer(buf_);

    const auto serialize_block = [&writer](auto& b) {
      b.serialize();
      writer.write_data(b.buffer());
    };

    // serialize the member blocks
    for (auto& block : blocks_)
      std::visit(serialize_block, block);

    return *this;
  }

  /// @brief Deserialize the ReplyPayloadSection from buffer
  /// @detail Caller must resize ReplyPayloadSection buffer based on new session message length
  ReplyPayloadSection& deserialize()
  {
    const exception::Exception ex{"ReplyPayloadSection", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    std::size_t block_ind(0);
    std::uint8_t padding_seen(0);
    payload_blocks_t blocks;

    while (reader.gcount())
    {
      if (padding_seen != 0)
        ex.throw_ex<std::logic_error>("padding block must be the final block.");

      // read and check the block type
      data::Block::Type type;
      reader.read_bytes(type);
      padding_seen += check_block_type(type, static_cast<Index>(block_ind), ex);

      // read the block size, and skip back to beginning of the block
      data::Block::size_type size;
      const auto& header_len = tini2p::under_cast(data::Block::HeaderLen);
      reader.read_bytes(size, tini2p::Endian::Big);
      reader.skip_back(header_len);

      data::Block::buffer_t buf(header_len + size);
      reader.read_data(buf);
      add_to_blocks(blocks, type, buf, ex);

      // increase the block index
      ++block_ind;
    }

    // lock member blocks, and swap with the temp container
    std::scoped_lock bgd(blocks_mutex_);
    blocks_.swap(blocks);

    return *this;
  }

  /// @brief Get the size of the unencrypted ReplyPayloadSection
  size_type size()
  {
    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return blocks_size(blocks_);
  }

  /// @brief Get the size of the encrypted ReplyPayloadSection
  size_type encrypted_len()
  {
    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return blocks_size(blocks_) + tini2p::under_cast(crypto::ChaChaPoly1305::MACLen);
  }

  /// @brief Get whether the message contains a block of the given type
  /// @tparam TBlock Block type to search for
  template <class TBlock>
  std::uint8_t has_block()
  {
    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return has_block<TBlock>(blocks_);
  }

  /// @brief Get a const reference to a block of the given type
  /// @tparam TBlock Block type to search for
  /// @throws Runtime error if the message does not contain a block of the given type
  template <class TBlock>
  const TBlock& get_block()
  {
    const exception::Exception ex{"PayloadSection", __func__};

    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return get_block<TBlock>(blocks_, ex);
  }

  /// @brief Extract a block of the given type from the message
  /// @tparam TBlock Block type to extract
  /// @throws Runtime error if the message does not contain a block of the given type
  template <class TBlock>
  TBlock extract_block()
  {
    const exception::Exception ex{"PayloadSection", __func__};

    std::scoped_lock sgd(blocks_mutex_);

    return extract_block<TBlock>(blocks_, ex);
  }

  /// @brief Set the payload blocks
  ReplyPayloadSection& blocks(payload_blocks_t blocks)
  {
    const exception::Exception ex{"ReplyPayloadSection", __func__};

    check_blocks_order(blocks, ex);

    {  // lock blocks
      std::scoped_lock sgd(blocks_mutex_);

      blocks_ = std::forward<payload_blocks_t>(blocks);
    }  // end-blocks-lock

    return serialize();
  }

  /// @brief Add a block to the payload
  /// @detail Block can either be an I2NPBlock or PaddingBlock.
  template <
      class TBlock,
      typename = std::enable_if_t<
          std::is_same<TBlock, data::ECIESOptionsBlock>::value || std::is_same<TBlock, data::GarlicBlock>::value
          || std::is_same<TBlock, data::PaddingBlock>::value>>
  ReplyPayloadSection& add_block(TBlock block)
  {
    const exception::Exception ex{"ReplyPayloadSection", __func__};

    std::scoped_lock bgd(blocks_mutex_);

    const auto& block_len = block.size();
    auto blocks_len = blocks_size(blocks_);
    data::PaddingBlock::size_type max_len(tini2p::under_cast(MaxPayloadLen));

    if (block_len + blocks_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "adding I2NP block exceeds max length: " + std::to_string(max_len)
            + ", current len: " + std::to_string(blocks_len) + ", block len: " + std::to_string(block_len));
      }
    else
      {
        if (std::holds_alternative<data::PaddingBlock>(blocks_.back()))
          {
            if (std::is_same<TBlock, data::PaddingBlock>::value)
              ex.throw_ex<std::logic_error>("multiple padding blocks not allowed.");
            else  // add block to the second-to-last position
              blocks_.insert(blocks_.end() - 1, std::forward<TBlock>(block));
          }
        else  // no padding block present, add block to the back
          blocks_.emplace_back(std::forward<TBlock>(block));
      }

    return *this;
  }

  /// @brief Removes padding block, if present
  ReplyPayloadSection& remove_padding()
  {
    std::scoped_lock bgd(blocks_mutex_);

    if (has_block<data::PaddingBlock>(blocks_))
      blocks_.pop_back();

    return *this;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another ReplyPayloadSection
  std::uint8_t operator==(const ReplyPayloadSection& oth)
  {  // attempt constant-time comparison
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    const auto& blocks_eq = static_cast<std::uint8_t>(blocks_ == oth.blocks_);

    return (blocks_eq * buf_eq);
  }

 private:
  std::uint8_t check_block_type(const data::Block::Type& type, const Index& index, const exception::Exception& ex)
  {
    if (index == Index::DateTime && type != data::Block::Type::DateTime)
      ex.throw_ex<std::logic_error>("invalid block type at DateTime index.");

    if (tini2p::under_cast(index) >= tini2p::under_cast(Index::Additional) && type != data::Block::Type::ECIESOptions
        && type != data::Block::Type::Garlic && type != data::Block::Type::Padding)
      {
        const auto& type_str = std::to_string(tini2p::under_cast(type));
        const auto& opt_str = std::to_string(tini2p::under_cast(data::Block::Type::ECIESOptions));
        const auto& garlic_str = std::to_string(tini2p::under_cast(data::Block::Type::Garlic));
        const auto& pad_str = std::to_string(tini2p::under_cast(data::Block::Type::Padding));

        ex.throw_ex<std::logic_error>(
            "invalid additional block type: " + type_str + ", must be Garlic: " + garlic_str
            + " or ECIES Options: " + opt_str + " or Padding: " + pad_str);
      }

    // return whether we have seen a padding block
    return static_cast<std::uint8_t>(type == data::Block::Type::Padding);
  }

  void check_blocks_order(const payload_blocks_t& blocks, const exception::Exception& ex)
  {
    // check required number of blocks is present
    const auto& blocks_len = blocks.size();
    const auto& min_len = tini2p::under_cast(MinBlocksLen);
    if (blocks_len < min_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid number of blocks: " + std::to_string(blocks_len) + ", min: " + std::to_string(min_len));
      }

    const auto block_type = [](const auto& b) { return b.type(); };

    // check order of blocks
    // required blocks (in order): DateTime, ECIESOptions, MessageNumber, NewDHKey (if not One-time) 
    // allowed additional blocks: I2NP & Padding
    // only one padding block is allowed, and it must be the last block.
    // no termination block is allowed, since this is a new session message,
    // and including a termination block makes no sense.
    std::size_t block_ind(0);
    std::uint8_t padding_seen = 0;
    for (const auto& block : blocks)
      {
        if (padding_seen != 0)
          ex.throw_ex<std::logic_error>("padding block must be the final block.");

        padding_seen += check_block_type(std::visit(block_type, block), static_cast<Index>(block_ind), ex);
        ++block_ind;
      }
  }

  void check_blocks_size(const payload_blocks_t& blocks, const exception::Exception& ex)
  {
    const auto& size = blocks_size(blocks);
    const auto& max_len = tini2p::under_cast(data::Block::MaxTotalLen);
    if (size > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid total payload length: " + std::to_string(size) + ", max: " + std::to_string(max_len));
      }
  }

  size_type blocks_size(const payload_blocks_t& blocks) const
  {
    size_type size(0);
    for (const auto& block : blocks)
      size += std::visit([](const auto& b){ return b.size(); }, block);

    return size;
  }

  // deserialize and add block to the supplied blocks container
  void add_to_blocks(
      payload_blocks_t& blocks,
      const data::Block::Type& type,
      const data::Block::buffer_t& buf,
      const exception::Exception& ex)
  {
    if (type == data::Block::Type::DateTime)
      blocks.emplace_back(data::DateTimeBlock(buf));
    else if (type == data::Block::Type::ECIESOptions)
      blocks.emplace_back(data::ECIESOptionsBlock(buf));
    else if (type == data::Block::Type::Garlic)
      blocks.emplace_back(data::GarlicBlock(buf));
    else if (type == data::Block::Type::Padding)
      blocks.emplace_back(data::PaddingBlock(buf));
    else
      ex.throw_ex<std::logic_error>("adding invalid block type.");
  }

  template <class TBlock>
  std::uint8_t has_block(const payload_blocks_t& blocks) const
  {
    const auto blocks_end = blocks.end();

    return static_cast<std::uint8_t>(
        std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); })
        != blocks_end);
  }

  template <class TBlock>
  const TBlock& get_block(const payload_blocks_t& blocks, const exception::Exception& ex) const
  {
    const auto blocks_end = blocks.end();

    auto it = std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); });

    if (it == blocks_end)
      ex.throw_ex<std::runtime_error>("no block found");

    return std::get<TBlock>(*it);
  }

  template <class TBlock>
  TBlock extract_block(payload_blocks_t& blocks, const exception::Exception& ex)
  {
    const auto blocks_end = blocks.end();

    auto it = std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); });

    if (it == blocks_end)
      ex.throw_ex<std::runtime_error>("no block found");

    auto block = std::move(*it);
    blocks_.erase(it);

    return std::get<TBlock>(block);
  }

  payload_blocks_t blocks_;
  std::shared_mutex blocks_mutex_;

  buffer_t buf_;
};
}  // namespace ecies
}  // namespace tini2p

#endif  // SRC_ECIES_MESSAGE_SECTIONS_H_
