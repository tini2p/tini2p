/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ECIES_SYMMETRIC_STATE_H_
#define SRC_ECIES_SYMMETRIC_STATE_H_

#include <mutex>
#include <shared_mutex>

#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/elligator2.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/x25519.h"

#include "src/ecies/dh_state.h"
#include "src/ecies/role.h"

namespace tini2p
{
namespace ecies
{
/// @brief Symmetric ratchet chain key + message key context
static const crypto::KDFContext<crypto::HmacSha256> ECIES_SYM_CTX{std::string("SymmetricRatchet")};

/// @class SymmetricState
/// @brief ECIES session symmetric key ratchet state
/// @tparam TRole ECIES session role, initiator or responder
template <class TRole>
class SymmetricState
{
 public:
  enum
  {
    KeyWindow = 32,
    KeydataLen = 64,
  };

  /// @brief Ratchet round
  enum struct Round : std::uint8_t
  {
    Initial,
    Main,
  };

  using role_t = TRole;  //< ECIES session role trait alias
  using curve_t = crypto::X25519;  //< EC curve trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD trait alias
  using hmac_t = crypto::HmacSha256;  //< HMAC-Sha256 trait alias
  using hkdf_t = crypto::HKDF<hmac_t>;  //< HKDF-HMAC-Sha256 trait alias
  using chain_keys_t = std::unordered_map<std::uint16_t, curve_t::shrkey_t>;  //< Message chain keys trait alias
  using keys_t = std::unordered_map<std::uint16_t, curve_t::shrkey_t>;  //< Message keys trait alias
  using keydata_t = crypto::FixedSecBytes<KeydataLen>;  //< Symmetric KDF keydata trait alias
  using initiator_r = ecies::EciesInitiator;  //< ECIES initiator role trait alias
  using responder_r = ecies::EciesResponder;  //< ECIES initiator role trait alias

  /// @brief Default ctor
  SymmetricState() : nonce_(0), beg_nonce_(0), end_nonce_(0), keys_(), chain_keys_(), round_(Round::Initial) {}

  /// @brief Create a SymmetricState from a chain key
  explicit SymmetricState(curve_t::shrkey_t chain_key)
      : nonce_(0),
        beg_nonce_(0),
        end_nonce_(0),
        keys_(),
        chain_keys_{{0, std::forward<curve_t::shrkey_t>(chain_key)}},
        round_(Round::Initial)
  {
    const exception::Exception ex{"ECIES: SymmetricState", __func__};

    if (chain_keys_[0].is_zero())
      ex.throw_ex<std::invalid_argument>("null chain key.");

    if (std::is_same<role_t, initiator_r>::value)
      Ratchet();
    else
      WindowedRatchet();
  }

  /// @brief Copy ctor
  SymmetricState(const SymmetricState& oth)
      : nonce_(oth.nonce_),
        beg_nonce_(oth.beg_nonce_),
        end_nonce_(oth.end_nonce_),
        keys_(oth.keys_),
        chain_keys_(oth.chain_keys_),
        round_(oth.round_)
  {
  }

  /// @brief Copy-assignment operator
  SymmetricState& operator=(const SymmetricState& oth)
  {
    nonce_ = oth.nonce_;
    beg_nonce_ = oth.beg_nonce_;
    end_nonce_ = oth.end_nonce_;

    round_ = oth.round_;

    std::scoped_lock sgd(keys_mutex_, ck_mutex_);
    keys_ = oth.keys_;
    chain_keys_ = oth.chain_keys_;

    return *this;
  }

  /// @brief Run the symmetric ratchet to generate the full window of keys
  SymmetricState& WindowedRatchet()
  {
    const exception::Exception ex{"ECIES: SymmetricState", __func__};

    if (std::is_same<role_t, initiator_r>::value)
      ex.throw_ex<std::logic_error>("outbound sessions do not use a key window.");

    if (!keys_.empty())
      ex.throw_ex<std::logic_error>("invalid call to windowed ratchet, unused keys remain in the window.");

    const auto& end_nonce = static_cast<aead_t::nonce_t::uint_t>(end_nonce_);
    const auto& win_len = tini2p::under_cast(KeyWindow);

    if (round_ == Round::Initial && !end_nonce)
      end_nonce_(win_len);  // initial state, create the initial symkey window

    if (round_ == Round::Main)
      {  // advance the key window
        nonce_(end_nonce);  // advance current nonce to previous window end
        beg_nonce_(end_nonce + 1);  // set new window begin to one past previous end
        end_nonce_(end_nonce + 1 + win_len);  // set new window end to next window length
      }

    // ratchet the symmetric keys for the window length
    std::scoped_lock sgd(keys_mutex_, ck_mutex_);
    for (std::uint16_t i = 0; i < win_len; ++i)
      DoRatchet();

    // set the current nonce to the beginning of the window
    nonce_ = beg_nonce_;

    return *this;
  }

  /// @brief Ratchet the symmetric keys
  void Ratchet()
  {
    std::scoped_lock sgd(keys_mutex_, ck_mutex_);
    DoRatchet();
  }

  /// @brief Get a const reference to the current symmetric key
  /// @throws Logic error if called before first symmetric ratchet
  const curve_t::shrkey_t& chain_key()
  {
    const exception::Exception ex{"ECIES: SymmetricState", __func__};

    const auto& nonce = static_cast<aead_t::nonce_t::uint_t>(nonce_);

    std::shared_lock cks(ck_mutex_, std::defer_lock);
    std::scoped_lock ckgd(cks);

    if (chain_keys_.find(nonce) == chain_keys_.end())
      ex.throw_ex<std::logic_error>("initial symmetric ratchet required.");

    return chain_keys_.at(nonce);
  }

  /// @brief Set the initial chain key
  /// @detail Clears any existing chain keys and symmetric keys 
  SymmetricState& chain_key(curve_t::shrkey_t key)
  {
    const exception::Exception ex{"ECIES: SymmetricState", __func__};

    check_chain_key(key, ex);

    std::scoped_lock sgd(ck_mutex_, keys_mutex_);

    chain_keys_.clear();
    chain_keys_.try_emplace(0, std::forward<curve_t::shrkey_t>(key));

    keys_.clear();

    nonce_.reset();

    round_ = Round::Initial;

    return *this;
  }

  /// @brief Get a const reference to the symmetric message keys
  const keys_t& keys() const noexcept
  {
    return keys_;
  }

  /// @brief Get a const reference to the current session symmetric message key
  /// @throws Logic error if called before first symmetric ratchet
  const curve_t::shrkey_t& sym_key()
  {
    const exception::Exception ex{"ECIES: SymmetricState", __func__};

    const auto& nonce = static_cast<aead_t::nonce_t::uint_t>(nonce_);

    // acquire read-lock on symmetric keys
    std::shared_lock ks(keys_mutex_, std::defer_lock);
    std::scoped_lock sgd(ks);

    if (keys_.find(nonce) == keys_.end())
      ex.throw_ex<std::logic_error>("initial symmetric ratchet required.");

    return keys_.at(nonce);
  }

  /// @brief Get the current chain key
  /// @throws Logic error if called before first symmetric ratchet
  const curve_t::shrkey_t& chain_key() const noexcept
  {
    const exception::Exception ex{"ECIES: SymmetricState", __func__};

    const auto& nonce = static_cast<aead_t::nonce_t::uint_t>(nonce_);

    // acquire read-lock on symmetric keys
    std::shared_lock ks(ck_mutex_, std::defer_lock);
    std::scoped_lock sgd(ks);

    if (chain_keys_.find(nonce) == chain_keys_.end())
      ex.throw_ex<std::logic_error>("initial symmetric ratchet required.");

    return chain_keys_.at(nonce);
  }

  /// @brief Remove used symmetric key
  /// @param nonce Nonce for the used symmetric key
  /// @throws Runtime error if no key found for given nonce
  SymmetricState& remove_used(const aead_t::nonce_t& nonce)
  {
    const exception::Exception ex{"ECIES: SymmetricState", __func__};

    std::scoped_lock sgd(keys_mutex_);

    const auto& nonce_uint = static_cast<aead_t::nonce_t::uint_t>(nonce);

    if (keys_.find(nonce_uint) == keys_.end())
      ex.throw_ex<std::runtime_error>("no symkey for nonce: " + std::to_string(nonce_uint));

    keys_.erase(nonce_uint);

    return *this;
  }

  /// @brief Swap with another SymmetricState
  SymmetricState& swap(SymmetricState& oth)
  {
    nonce_.swap(oth.nonce_);
    beg_nonce_.swap(oth.beg_nonce_);
    end_nonce_.swap(oth.end_nonce_);

    auto tmp_round = std::move(round_);
    round_ = std::move(oth.round_);
    oth.round_ = std::move(tmp_round);

    std::scoped_lock sgd(keys_mutex_, ck_mutex_);
    keys_.swap(oth.keys_);
    chain_keys_.swap(oth.chain_keys_);

    return *this;
  }

  /// @brief Get whether ratchet is in the initial round
  std::uint8_t initial_round() const
  {
    return static_cast<std::uint8_t>(round_ == Round::Initial);
  }

  /// @brief Equality comparison with another Symmetric ratchet state
  /// @detail Must be non-const to lock internal mutexes for thread safety
  std::uint8_t operator==(SymmetricState& oth)
  {
    const auto& nonce_eq = static_cast<std::uint8_t>(nonce_ == oth.nonce_);
    const auto& beg_nonce_eq = static_cast<std::uint8_t>(beg_nonce_ == oth.beg_nonce_);
    const auto& end_nonce_eq = static_cast<std::uint8_t>(end_nonce_ == oth.end_nonce_);
    const auto& round_eq = static_cast<std::uint8_t>(round_ == oth.round_);

    // read-lock keys and chain keys mutexes
    // can only lock local mutexes, other instance may be owned by a separate thread
    std::shared_lock cks(ck_mutex_, std::defer_lock), ks(keys_mutex_, std::defer_lock);
    std::scoped_lock tgd(cks, ks);

    const auto& chain_eq = static_cast<std::uint8_t>(chain_keys_ == oth.chain_keys_);
    const auto& keys_eq = static_cast<std::uint8_t>(keys_ == oth.keys_);

    return (nonce_eq * beg_nonce_eq * end_nonce_eq * round_eq * chain_eq * keys_eq);
  }

 private:
  void DoRatchet()
  {
    keydata_t keydata;
    auto nonce = static_cast<aead_t::nonce_t::uint_t>(nonce_);  // get nonce from last round or DH ratchet

    // keydata_0 = HKDF(symmKey_ck_0, SYMMKEY_CONSTANT(ZEROLEN), "SymmetricRatchet", 64)
    // OR
    // keydata_n = HKDF(symmKey_ck_(n-1), SYMMKEY_CONSTANT(ZEROLEN), "SymmetricRatchet", 64)
    hkdf_t::Derive(keydata, chain_keys_[nonce], ZERO_LEN, ECIES_SYM_CTX);

    if (std::is_same<role_t, initiator_r>::value)
      {  // only keep current chain and message keys if we are the transmitter
        // TODO(tini2p): breaks constant-time, since lookup (O(logn)) will differ for receiver vs. transmitter,
        //     and the `clear` calls will take extra time.
        //
        //     Is it worth the space savings to break constant-time?
        chain_keys_.clear();
        keys_.clear();
      }

    aead_t::nonce_t temporary_nonce(0);
    if (round_ == Round::Initial)  // initial round
      {
        nonce = temporary_nonce++;  // `nonce` + sym_nonce still zero, post-increment temp nonce for constant-time
        round_ = Round::Main;
      }
    else  // subsequent rounds
      nonce = ++nonce_;  // pre-increment sym_nonce to get new current value, updates `nonce`

    tini2p::BytesReader<keydata_t> reader(keydata);
    reader.read_data(chain_keys_[nonce]);  // symmKey_ck_n = keydata_n[0:31]
    reader.read_data(keys_[nonce]);  // k_n = keydata_n[32:63]
  }

  void check_chain_key(const curve_t::shrkey_t& chain_key, const exception::Exception& ex) const
  {
    if (chain_key.is_zero())
      ex.throw_ex<std::logic_error>("null chain key");
  }

  aead_t::nonce_t nonce_;
  aead_t::nonce_t beg_nonce_, end_nonce_;

  keys_t keys_;
  std::shared_mutex keys_mutex_;

  chain_keys_t chain_keys_;
  std::shared_mutex ck_mutex_;

  Round round_;
};
}  // namespace ecies
}  // namespace tini2p


#endif  // SRC_ECIES_SYMMETRIC_STATE_H_
