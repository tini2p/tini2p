/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ECIES_STATE_H_
#define SRC_ECIES_STATE_H_

#include <mutex>
#include <shared_mutex>

#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/elligator2.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/x25519.h"

#include "src/ecies/dh_state.h"
#include "src/ecies/tag_state.h"
#include "src/ecies/symmetric_state.h"

namespace tini2p
{
namespace ecies
{
/// @brief Requirement enum for needed components and/or ratchets
enum struct Reqs : bool
{
  NotRequired,
  Required
};

/// @class CipherState
/// @brief Holds EciesX25519 keys and ratchet state. Performs DH, Symmetric Key, and Session Tag ratchets.
/// @tparam TRole ECIES session role, initiator for outbound sessions, responder for inbound sessions
template <
    class TRole,
    typename = std::enable_if_t<
        std::is_same<TRole, ecies::EciesInitiator>::value || std::is_same<TRole, ecies::EciesResponder>::value>>
class CipherState
{
 public:
  using role_t = TRole;  //< ECIES session role
  using curve_t = crypto::X25519;  //< Curve trait alias
  using elligator_t = crypto::Elligator2;  //< Elligator2 trait alias 
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD symmetric crypto trait alias
  using hmac_t = crypto::HmacSha256;  //< HMAC-Sha256 hashing function trait alias
  using hkdf_t = crypto::HKDF<hmac_t>;  //< HKDF trait alias
  using dh_state_t = ecies::DHState;  //< ECIES DH ratchet state trait alias
  using tag_state_t = ecies::TagState<role_t>;  //< ECIES tag ratchet state trait alias
  using tag_t = typename tag_state_t::tag_t;  //< Session tag trait alias
  using tags_t = typename tag_state_t::tags_t;  //< Session tag collection trait alias
  using tag_chain_keys_t = typename tag_state_t::chain_keys_t;  //< Session tag chain keys collection trait alias
  using sym_state_t = ecies::SymmetricState<role_t>;  //< ECIES symmetric ratchet state trait alias
  using sym_keys_t = typename sym_state_t::keys_t;  //< Symmetric keys trait alias
  using sym_chain_keys_t = typename sym_state_t::chain_keys_t;  //< Symmetric chain keys trait alias
  using initiator_r = ecies::EciesInitiator;  //< ECIES initiator role trait alias
  using responder_r = ecies::EciesResponder;  //< ECIES initiator role trait alias

  /// @brief Default ctor
  /// @detail Creates new static and ephemeral keypairs,
  ///    and requires remote keys before performing a DH ratchet
  CipherState()
      : pn_(0),
        tag_state_(),
        sym_state_(),
        dh_ratchet_req_(Reqs::Required),
        sym_ratchet_req_(Reqs::Required),
        tag_ratchet_req_(Reqs::Required)
  {
  }

  /// @brief Create an ECIES CipherState from DH ratchet results
  CipherState(curve_t::shrkey_t tag_chain_key, curve_t::shrkey_t sym_chain_key)
      : pn_(0),
        tag_state_(std::forward<curve_t::shrkey_t>(tag_chain_key)),
        sym_state_(std::forward<curve_t::shrkey_t>(sym_chain_key)),
        dh_ratchet_req_(Reqs::NotRequired),
        sym_ratchet_req_(Reqs::NotRequired),
        tag_ratchet_req_(Reqs::NotRequired)
  {
  }

  /// @brief Copy-ctor
  CipherState(const CipherState& oth)
      : pn_(oth.pn_),
        tag_state_(oth.tag_state_),
        sym_state_(oth.sym_state_),
        dh_ratchet_req_(oth.dh_ratchet_req_),
        sym_ratchet_req_(oth.sym_ratchet_req_),
        tag_ratchet_req_(oth.tag_ratchet_req_)
  {
  }

  /// @brief Copy-assignment operator
  CipherState& operator=(const CipherState& oth)
  {
    pn_ = oth.pn_;
    tag_state_ = oth.tag_state_;
    sym_state_ = oth.sym_state_;
    dh_ratchet_req_ = oth.dh_ratchet_req_;
    sym_ratchet_req_ = oth.sym_ratchet_req_;
    tag_ratchet_req_ = oth.tag_ratchet_req_;

    return *this;
  }

  /// @brief Update the session tag and symmetric key chain keys
  CipherState& UpdateChainKeys(curve_t::shrkey_t tag_chain_key, curve_t::shrkey_t sym_chain_key)
  {
    const exception::Exception ex{"CipherState", __func__};

    check_chain_key(tag_chain_key, ex);
    check_chain_key(sym_chain_key, ex);

    // set the requirement vars
    dh_ratchet_req_ = Reqs::NotRequired;
    tag_ratchet_req_ = Reqs::Required;
    sym_ratchet_req_ = Reqs::Required;

    tag_state_.chain_key(std::forward<curve_t::shrkey_t>(tag_chain_key));
    sym_state_.chain_key(std::forward<curve_t::shrkey_t>(sym_chain_key));

    CheckRatchets();

    return *this;
  }

  /// @brief Session tag ratchet to derive tag chain key + session tag
  CipherState& TagRatchet()
  {
    const exception::Exception ex{"CipherState", __func__};

    if (dh_ratchet_req_ == Reqs::Required)
      ex.throw_ex<std::logic_error>("DH ratchet required.");

    tag_state_.Ratchet();

    if (std::is_same<role_t, initiator_r>::value)
      tag_ratchet_req_ = Reqs::NotRequired;

    return *this;
  }

  /// @brief Generate a window of session tags
  CipherState& WindowedTagRatchet()
  {
    const exception::Exception ex{"CipherState", __func__};

    if (dh_ratchet_req_ == Reqs::Required)
      ex.throw_ex<std::logic_error>("DH ratchet required.");

    tag_state_.WindowedRatchet();

    return *this;
  }

  /// @brief Generate a window of session reply tags
  CipherState& ReplyTagRatchet()
  {
    tag_state_.WindowedRatchet();

    return *this;
  }

  /// @brief Get whether the session tag ratchet is in the initial round
  std::uint8_t tag_initial_round() const
  {
    return tag_state_.initial_round();
  }

  /// @brief Symmetric key ratchet to derive symmetric chain + message key
  void SymmetricRatchet()
  {
    const exception::Exception ex{"CipherState", __func__};

    if (dh_ratchet_req_ == Reqs::Required)
      ex.throw_ex<std::logic_error>("DH ratchet required.");

    sym_state_.Ratchet();

    if (std::is_same<role_t, initiator_r>::value)
      sym_ratchet_req_ = Reqs::NotRequired;
  }

  /// @brief Checks and performs if tag and/or symmetric ratchet(s) are needed
  void CheckRatchets()
  {
    const exception::Exception ex{"CipherState", __func__};

    if (dh_ratchet_req_ == Reqs::Required)
      ex.throw_ex<std::logic_error>("DH ratchet required.");

    if (std::is_same<role_t, initiator_r>::value)
      {
        if (sym_ratchet_req_ == Reqs::Required)
          {
            sym_state_.Ratchet();
            sym_ratchet_req_ = Reqs::NotRequired;
          }

        if (tag_ratchet_req_ == Reqs::Required)
          {
            tag_state_.Ratchet();
            tag_ratchet_req_ = Reqs::NotRequired;
          }
      }
    else
      {
        if (tag_state_.initial_round())
          tag_ratchet_req_  = Reqs::Required;

        if (tag_state_.tags().empty())
          tag_ratchet_req_  = Reqs::Required;

        if (tag_ratchet_req_ == Reqs::Required)
          {
            tag_state_.WindowedRatchet();
            tag_ratchet_req_ = Reqs::NotRequired;
          }

        if (sym_state_.initial_round())
          sym_ratchet_req_ = Reqs::Required;

        if (sym_state_.keys().empty())
          sym_ratchet_req_ = Reqs::Required;

        if (sym_ratchet_req_ == Reqs::Required)
          {
            sym_state_.WindowedRatchet();
            sym_ratchet_req_ = Reqs::NotRequired;
          }
      }
  }

  /// @brief Get a const reference to the local session nonce
  /// @detail Tied to the symmetric key nonce value, which may lag behind the tag nonce.
  ///    Caller must check tag and symmetric key ratchets are synchronized before calling.
  const aead_t::nonce_t& nonce() const noexcept
  {
    return sym_state_.nonce();
  }

  /// @brief Get the nonce associated with the given tag
  /// @throws Runtime error if the tag is not found in the current tag window
  /// @return Nonce associated with the given tag
  aead_t::nonce_t nonce(const tag_t& tag) const
  {
    const exception::Exception ex{"CipherState", __func__};

    const auto& tags = tag_state_.tags();

    const auto it = std::find_if(tags.begin(), tags.end(), [&tag](const auto& i) { return std::get<tag_t>(i) == tag; });

    if (it == tags.end())
      ex.throw_ex<std::runtime_error>("tag not found in the tag window.");

    return aead_t::nonce_t(it->first); 
  }

  /// @brief Get a const reference to the local session previous message(s) number
  const aead_t::nonce_t::uint_t& pn() const noexcept
  {
    return pn_;
  }

  /// @brief Get a const reference to the local session symmetric message keys
  /// @detail Only most recent ratcheted message key stored for transmitter
  const sym_keys_t& sym_keys() const noexcept
  {
    return sym_state_.keys();
  }

  /// @brief Get a const reference to the current session symmetric message key
  /// @throws Logic error if called before first symmetric ratchet
  const curve_t::shrkey_t& sym_key()
  {
    return sym_state_.sym_key();
  }

  /// @brief Get the symmetric key associated with the given session tag
  const curve_t::shrkey_t& sym_key(const tag_t& tag) const
  {
    const exception::Exception ex{"CipherState", __func__};

    const auto& tags = tag_state_.tags();

    const auto it = std::find_if(tags.begin(), tags.end(), [&tag](const auto& i) { return std::get<tag_t>(i) == tag; });

    if (it == tags.end())
      ex.throw_ex<std::runtime_error>("tag not found in tag window.");

    const auto& sym_keys = sym_state_.keys();

    if (sym_keys.find(it->first) == sym_keys.end())
      ex.throw_ex<std::logic_error>("additional symmetric ratchet(s) required.");

    return sym_keys.at(it->first);
  }

  /// @brief Get a const reference to the local session tag chain keys
  /// @detail Only most recent ratcheted tag key stored for transmitter
  const tag_chain_keys_t& tag_chain_keys() const noexcept
  {
    return tag_state_.chain_keys();
  }

  /// @brief Get a const reference to the current session tag key
  /// @throws Logic error if called before first symmetric ratchet
  const curve_t::shrkey_t& tag_chain_key()
  {
    return tag_state_.chain_key();
  }

  /// @brief Get a const reference to the current session tag key
  /// @throws Logic error if called before first symmetric ratchet
  CipherState& tag_chain_key(curve_t::shrkey_t chain_key)
  {
    tag_state_.chain_key(std::forward<curve_t::shrkey_t>(chain_key));

    return *this;
  }

  /// @brief Return a const reference to the session tags
  const tags_t& tags() const noexcept
  {
    return tag_state_.tags();
  }

  /// @brief Get a const reference to the current session tag
  const tag_t& tag()
  {
    return tag_state_.tag();
  }

  /// @brief Get whether the session tags are empty
  std::uint8_t empty_tags()
  {
    return tag_state_.empty();
  }

  /// @brief Remove used symmetric key and session tag
  /// @param nonce Nonce for the used symmetric key and session tag
  void remove_used(const aead_t::nonce_t& nonce)
  {
    tag_state_.remove_used(nonce);
    sym_state_.remove_used(nonce);

    if (std::is_same<TRole, initiator_r>::value)
      {
        tag_ratchet_req_ = Reqs::Required;
        sym_ratchet_req_ = Reqs::Required;
      }

    CheckRatchets();
  }

  /// @brief Remove used session tag
  /// @detail Used primarily by pending sessions to remove used reply tags
  /// @param tag Session tag to remove
  CipherState& remove_used(const tag_t& tag)
  {
    tag_state_.remove_used(tag);

    if (tag_state_.empty())
      tag_state_.WindowedRatchet();

    return *this;
  }

  /// @brief Set the DH ratchet requirement
  void dh_ratchet_required(const Reqs& req)
  {
    const exception::Exception ex{"CipherState", __func__};

    if (req != Reqs::NotRequired && req != Reqs::Required)
      ex.throw_ex<std::invalid_argument>("invalid requirement state.");

    dh_ratchet_req_ = req;
  }

  /// @brief Equality comparison with another CipherState
  std::uint8_t operator==(CipherState& oth)
  {
    const auto& tag_eq = static_cast<std::uint8_t>(tag_state_ == oth.tag_state_);
    const auto& sym_eq = static_cast<std::uint8_t>(sym_state_ == oth.sym_state_);
    const auto& pn_eq = static_cast<std::uint8_t>(pn_ == oth.pn_);
    const auto& dh_req_eq =  static_cast<std::uint8_t>(dh_ratchet_req_ == oth.dh_ratchet_req_);
    const auto& sym_req_eq =  static_cast<std::uint8_t>(sym_ratchet_req_ == oth.sym_ratchet_req_);
    const auto& tag_req_eq =  static_cast<std::uint8_t>(tag_ratchet_req_ == oth.tag_ratchet_req_);

    return (tag_eq * sym_eq * pn_eq * dh_req_eq * sym_req_eq * tag_req_eq);
  }

  /// @brief Equality comparison with another CipherState
  /// @detail Enables comparison between different template instantiations (always false)
  template <typename R = role_t, typename = std::enable_if_t<std::is_same<R, responder_r>::value>>
  std::uint8_t operator==(CipherState<initiator_r>& oth)
  {
    return 0;
  }

  /// @brief Equality comparison with another CipherState
  /// @detail Enables comparison between different template instantiations (always false)
  template <typename R = role_t, typename = std::enable_if_t<std::is_same<R, initiator_r>::value>>
  std::uint8_t operator==(CipherState<responder_r>& oth)
  {
    return 0;
  }

 private:
  void check_chain_key(const curve_t::shrkey_t& key, const exception::Exception& ex)
  {
    if (key.is_zero())
      ex.throw_ex<std::runtime_error>("null chain key");
  }

  aead_t::nonce_t::uint_t pn_;

  tag_state_t tag_state_;
  sym_state_t sym_state_;

  Reqs dh_ratchet_req_, sym_ratchet_req_, tag_ratchet_req_;
};
}  // namespace ecies
}  // namespace tini2p

#endif  // SRC_ECIES_STATE_H_
