/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ECIES_TAG_STATE_H_
#define SRC_ECIES_TAG_STATE_H_

#include <iostream>

#include <mutex>
#include <shared_mutex>

#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/elligator2.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/x25519.h"

#include "src/ecies/dh_state.h"
#include "src/ecies/role.h"

namespace tini2p
{
namespace ecies
{
/// @brief Session tag ratchet initialization context
static const crypto::KDFContext<crypto::HmacSha256> ECIES_TAG_INIT_CTX{std::string("STInitialization")};

/// @brief Session tag ratchet chain key + tag context
static const crypto::KDFContext<crypto::HmacSha256> ECIES_TAG_CTX{std::string("SessionTagKeyGen")};

enum : std::uint8_t
{
  TagLen = 8,
};

/// @struct Tag
/// @brief ECIES session tag
struct Tag : public crypto::Key<TagLen>
{
  using base_t = crypto::Key<TagLen>;  //< Tag base trait alias

  Tag() : base_t() {}

  Tag(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

  Tag(const crypto::SecBytes& buf) : base_t(buf) {}

  Tag(std::initializer_list<std::uint8_t> list) : base_t(list) {}
};

/// @class TagState
/// @brief ECIES session tag ratchet state
/// @tparam TRole ECIES session role, initiator or responder
template <class TRole>
class TagState
{
 public:
  enum
  {
    TagLen = 8,  //< see prop. 144, is 12/16 doable?
    TagWindow = 32,
    KeydataLen = 64,
  };

  /// @brief Ratchet round
  enum struct Round : std::uint8_t
  {
    Initial,  //< initial ratchet
    Main,  //< main round, already initialized
  };

  using role_t = TRole;  //< ECIES session role trait alias
  using curve_t = crypto::X25519;  //< EC curve trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD trait alias
  using hmac_t = crypto::HmacSha256;  //< HMAC-Sha256 trait alias
  using hkdf_t = crypto::HKDF<hmac_t>;  //< HKDF-HMAC-Sha256 trait alias
  using tag_t = Tag;  //< ECIES session tag trait alias
  using tags_t = std::unordered_map<std::uint16_t, tag_t>;  //< Session tags collection trait alias
  using chain_keys_t = std::unordered_map<std::uint16_t, curve_t::shrkey_t>;  //< Session tag chain keys trait alias
  using keydata_t = crypto::FixedSecBytes<KeydataLen>;  //< Session tag KDF keydata trait alias
  using initiator_r = ecies::EciesInitiator;  //< ECIES initiator role trait alias
  using responder_r = ecies::EciesResponder;  //< ECIES responder role trait alias

  /// @brief Default-ctor, create an uninitialized session tag ratchet state
  TagState() : nonce_(0), beg_nonce_(0), end_nonce_(0), constant_(), chain_keys_(), tags_(), round_(Round::Initial) {}

  /// @brief Fully-initializing ctor, creates an initialized session tag ratchet state from an initial chain key
  TagState(curve_t::shrkey_t chain_key)
      : nonce_(0),
        beg_nonce_(0),
        end_nonce_(0),
        constant_(),
        chain_keys_{{0, std::forward<curve_t::shrkey_t>(chain_key)}},
        tags_(),
        round_(Round::Initial)
  {
    const exception::Exception ex{"TagState", __func__};

    if (chain_keys_[0].is_zero())
      ex.throw_ex<std::invalid_argument>("null chain key.");

    if (std::is_same<role_t, initiator_r>::value)
      Ratchet();
    else
      WindowedRatchet();
  }

  /// @brief Copy ctor
  TagState(const TagState& oth)
      : nonce_(oth.nonce_),
        beg_nonce_(oth.beg_nonce_),
        end_nonce_(oth.end_nonce_),
        constant_(oth.constant_),
        chain_keys_(oth.chain_keys_),
        tags_(oth.tags_),
        round_(oth.round_)
  {
  }

  /// @brief Copy-assignment operator
  TagState& operator=(TagState oth)
  {
    nonce_ = std::forward<aead_t::nonce_t>(oth.nonce_);
    beg_nonce_ = std::forward<aead_t::nonce_t>(oth.beg_nonce_);
    end_nonce_ = std::forward<aead_t::nonce_t>(oth.end_nonce_);
    constant_ = std::forward<hmac_t::salt_t>(oth.constant_);
    round_ = std::forward<Round>(oth.round_);

    std::scoped_lock tgd(ck_mutex_, tags_mutex_);
    chain_keys_ = std::forward<chain_keys_t>(oth.chain_keys_);
    tags_ = std::forward<tags_t>(oth.tags_);

    return *this;
  }

  /// @brief Create a session tag window for inbound sessions
  /// @throws Logic error if tag window is not empty
  void WindowedRatchet()
  {
    const exception::Exception ex{"ECIES: TagState", __func__};

    if (!tags_.empty())
      ex.throw_ex<std::logic_error>("unused tags remain in the window");

    const auto& end_nonce = static_cast<aead_t::nonce_t::uint_t>(end_nonce_);
    const auto& win_len = tini2p::under_cast(TagWindow);

    if (round_ == Round::Initial)
      {
        beg_nonce_(0);
        end_nonce_(win_len);  // initial state, create the initial window
      }

    if (round_ == Round::Main && tags_.empty())
      {  // advance the symkey + tag window
        nonce_(end_nonce);  // advance current nonce to previous window end
        beg_nonce_(end_nonce + 1);  // set new window begin to one past previous end
        end_nonce_(end_nonce + 1 + win_len);  // set new window end to next window length
      }

    {  // lock chain keys and tags
      std::scoped_lock(ck_mutex_, tags_mutex_);
      for (std::uint16_t i = 0; i < win_len; ++i)
        DoRatchet();
    }  // end-chain-keys-and-tags-lock

    nonce_ = beg_nonce_;
  }

  /// @brief Ratchet the session tags and tag chain keys
  void Ratchet()
  {
    std::scoped_lock tgd(ck_mutex_, tags_mutex_);
    DoRatchet();
  }

  /// @brief Get a const reference to the current session tag key
  /// @throws Logic error if called before first symmetric ratchet
  const curve_t::shrkey_t& chain_key()
  {
    const exception::Exception ex{"ECIES: TagState", __func__};

    const auto& nonce = static_cast<aead_t::nonce_t::uint_t>(nonce_);

    std::shared_lock cks(ck_mutex_, std::defer_lock);
    std::scoped_lock ckgd(cks);

    if (chain_keys_.find(nonce) == chain_keys_.end())
      ex.throw_ex<std::logic_error>("initial tag ratchet required.");

    return chain_keys_.at(nonce);
  }

  /// @brief Set the initial chain key
  /// @detail Clears any existing chain keys and tags
  TagState& chain_key(curve_t::shrkey_t key)
  {
    const exception::Exception ex{"ECIES: TagState", __func__};

    check_chain_key(key, ex);

    std::scoped_lock sgd(ck_mutex_, tags_mutex_);

    chain_keys_.clear();
    chain_keys_.try_emplace(0, std::forward<curve_t::shrkey_t>(key));

    tags_.clear();

    nonce_.reset();

    round_ = Round::Initial;

    return *this;
  }

  /// Get the current session tag
  const tag_t& tag()
  {
    const exception::Exception ex{"ECIES: TagState", __func__};

    const auto& nonce = static_cast<aead_t::nonce_t::uint_t>(nonce_);

    std::shared_lock ts(tags_mutex_, std::defer_lock);
    std::scoped_lock tgd(ts);

    if (tags_.find(nonce) == tags_.end())
      ex.throw_ex<std::logic_error>("initial tag ratchet required.");

    return tags_.at(nonce);
  }

  /// @brief Get a const reference to the chain keys
  const chain_keys_t& chain_keys() const noexcept
  {
    return chain_keys_;
  }

  /// @brief Get a const reference to the tags
  const tags_t& tags() const noexcept
  {
    return tags_;
  }

  /// @brief Get whether the tags are empty
  std::uint8_t empty()
  {
    std::shared_lock ts(tags_mutex_, std::defer_lock);
    std::scoped_lock sgd(ts);

    return static_cast<std::uint8_t>(tags_.empty());
  }

  /// @brief Remove used tags from the ratchet state
  void remove_used(const aead_t::nonce_t& nonce)
  {
    const exception::Exception ex{"ECIES: TagState", __func__};

    const auto& nonce_uint = static_cast<aead_t::nonce_t::uint_t>(nonce);

    std::scoped_lock gd(tags_mutex_);

    if (tags_.find(nonce_uint) == tags_.end())
      ex.throw_ex<std::runtime_error>("no tag for nonce: " + std::to_string(nonce_uint));

    tags_.erase(nonce_uint);
  }

  /// @brief Remove used tag from the ratchet state
  /// @throws Runtime error if tag not found
  TagState& remove_used(const tag_t& tag)
  {
    const exception::Exception ex{"ECIES: TagState", __func__};

    std::scoped_lock gd(tags_mutex_);

    const auto tags_end = tags_.end();

    const auto it = std::find_if(tags_.begin(), tags_end, [&tag](const auto& t) { return t.second == tag; });

    if (it != tags_end)
      {
        tags_.erase(it);

        if (nonce_ < end_nonce_)
          ++nonce_;
      }
    else
      ex.throw_ex<std::runtime_error>("no tag found matching: " + tini2p::bin_to_hex(tag, ex));

    return *this;
  }

  /// @brief Clear the tags
  TagState& clear()
  {
    std::scoped_lock sgd(tags_mutex_);

    tags_.clear();

    return *this;
  }

  /// @brief Swap with another TagState
  TagState& swap(TagState& oth)
  {
    nonce_.swap(oth.nonce_);
    beg_nonce_.swap(oth.beg_nonce_);
    end_nonce_.swap(oth.end_nonce_);
    constant_.swap(oth.constant_);

    auto tmp_round = std::move(round_);
    round_ = std::move(oth.round_);
    oth.round_ = std::move(tmp_round);

    std::scoped_lock(ck_mutex_, tags_mutex_, oth.ck_mutex_, oth.tags_mutex_);
    chain_keys_.swap(oth.chain_keys_);
    tags_.swap(oth.tags_);

    return *this;
  }

  /// @brief Get whether session tag ratchet is in the initial round
  std::uint8_t initial_round() const
  {
    return static_cast<std::uint8_t>(round_ == Round::Initial);
  }

  /// @brief Equality comparison with another TagState
  /// @detail Must be non-const to lock internal mutexes for thread safety
  std::uint8_t operator==(TagState& oth)
  {
    const auto& round_eq = static_cast<std::uint8_t>(round_ == oth.round_);
    const auto& const_eq = static_cast<std::uint8_t>(constant_ == oth.constant_);
    const auto& nonce_eq = static_cast<std::uint8_t>(nonce_ == oth.nonce_);
    const auto& beg_nonce_eq = static_cast<std::uint8_t>(beg_nonce_ == oth.beg_nonce_);
    const auto& end_nonce_eq = static_cast<std::uint8_t>(end_nonce_ == oth.end_nonce_);

    // read-lock tags and chain keys mutexes
    // can only lock local mutexes, other instance may be owned by a separate thread
    std::shared_lock cks(ck_mutex_, std::defer_lock), ts(tags_mutex_, std::defer_lock);
    std::scoped_lock tgd(cks, ts);

    const auto& chain_eq = static_cast<std::uint8_t>(chain_keys_ == oth.chain_keys_);
    const auto& tags_eq = static_cast<std::uint8_t>(tags_ == oth.tags_);

    return (round_eq * const_eq * nonce_eq * beg_nonce_eq * end_nonce_eq * chain_eq * tags_eq);
  }

 private:
  // Separate ratchet impl to aquire lock once in WindowedRatchet
  // Acquiring and releasing locks per-loop iteration is wasteful
  void DoRatchet()
  {
    keydata_t keydata;
    auto nonce = static_cast<aead_t::nonce_t::uint_t>(nonce_);  // get the nonce from previous round or DH ratchet
    auto& tag_key = chain_keys_.at(nonce);

    // If first tag ratchet, perform session tag constant initialization.
    // Helps ensure variation from other independent sessions, see spec for rationale.
    // Breaks constant-time with the extra DH calculation in init round.
    // Is it worth calculating a dummy DH each subsequent round to approach constant-time?
    if (round_ == Round::Initial)
      {
        // keydata = HKDF(sessTag_ck, ZEROLEN, "STInitialization", 64)
        hkdf_t::Derive(keydata, tag_key, ZERO_LEN, ECIES_TAG_INIT_CTX);

        tini2p::BytesReader<keydata_t> reader(keydata);
        reader.read_data(tag_key);  // sessTag_ck = keydata[0:31]
        reader.read_data(constant_);  // SESSTAG_CONSTANT = keydata[32:63]
      }

    // keydata_0 = HKDF(sessTag_ck_0, SESSTAG_CONSTANT, "SessionTagKeyGen", 64)
    // OR
    // keydata_n = HKDF(sessTag_ck_(n-1), SESSTAG_CONSTANT, "SessionTagKeyGen", 64)
    hkdf_t::Derive(keydata, tag_key, constant_, ECIES_TAG_CTX);

    aead_t::nonce_t temporary_nonce(0);
    if (round_ == Round::Initial)  // initial round
      {
        nonce = temporary_nonce++;  // `nonce` + tag_nonce still zero, post-increment temp nonce for constant-time
        round_ = Round::Main;
      }
    else  // subsequent rounds
      nonce = ++nonce_;  // pre-increment nonce to get new current value, updates `nonce`

    tini2p::BytesReader<keydata_t> reader(keydata);
    reader.read_data(chain_keys_[nonce]);  // sessTag_ck_n = keydata_n[0:31]
    reader.read_data(tags_[nonce]);  // tag_n = keydata_n[32:39] (or more if tag > 8 bytes)
  }

  void check_chain_key(const curve_t::shrkey_t& chain_key, const exception::Exception& ex) const
  {
    if (chain_key.is_zero())
      ex.throw_ex<std::logic_error>("null chain key");
  }

  aead_t::nonce_t nonce_;
  aead_t::nonce_t beg_nonce_, end_nonce_;  // for tracking windowed tag gen

  hmac_t::salt_t constant_;
  chain_keys_t chain_keys_;
  std::shared_mutex ck_mutex_;

  tags_t tags_;
  std::shared_mutex tags_mutex_;

  Round round_;
};
}  // namespace ecies
}  // namespace tini2p

#endif  // SRC_ECIES_TAG_STATE_H_
