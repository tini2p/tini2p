/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ECIES_NEW_SESSION_H_
#define SRC_ECIES_NEW_SESSION_H_

#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/elligator2.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/kdf_context.h"
#include "src/crypto/sha.h"

#include "src/ecies/dh_state.h"
#include "src/ecies/keys.h"
#include "src/ecies/message_sections.h"
#include "src/ecies/role.h"

#include "src/noise/noise.h"

namespace tini2p
{
namespace ecies
{
/// @class NewSessionMessage
/// @brief Message for a new ECIES session
/// @detail Used to securely establish a new end-to-end ECIES session between Alice and Bob.
///
///    As an initial message, Alice sends a set of keys to Bob. The set always
///    contains a one-time ephemeral public key, used to encrypt an EphemeralKeySection.
///
///    If the message is for one-time use, the ephemeral public key is the only key transferred.
///    An StaticKeySection still follows, containing flags indicating one-time use.
///
///    For bound sessions, the EphemeralKeySection is followed by a StaticKeySection,
///    containing Alice's static public key.
///
///    One-time and unbound sessions use a one-way handshake between Alice and Bob, satisfying the N Noise pattern:
///
///    s <-
///    ...
///    e, es ->
///
///    For bound sessions, our handshake closely matches the IK Noise pattern:
///
///    s <-
///    ...
///    e, es, s, ss ->
///    <- e, ee, se
///
///    Bob's half of the handshake will establish his new outbound session, and Alice's inbound session.
///
///    Bob includes a reply tag, prepended to the EphemeralKeySection, derived from the first received NewSessionMessage
///    from Alice.
///
///    Alice uses the reply tag to associate the inbound session with the original outbound session
///    (instead of a new session from Bob).
///
///    The full Noise handshake must complete before starting the ratchets, and sending ExistingSessionMessages.
///    Each peer will have one outbound and one inbound set of ratchets.
class NewSessionMessage
{
  enum struct MsgState : std::uint8_t
  {
    Decrypted,
    Encrypted,
  };

 public:
  enum : std::uint16_t
  {
    KeydataLen = 64,
    OneTimeKeyLen = crypto::Elligator2::PublicKeyLen,
    MinLen = 103,   //< Elligator2::Pubkey(32) + StaticKeySection (40) + Decrypted Payload::MinLen (41)
    MaxLen = 62708,  //< I2NP max, see spec
    BoundHInputLen = 72,  //< h(32) + StaticKeySection(40)
  };

  using elligator_t = crypto::Elligator2;  //< Elligator2 trait alias
  using curve_t = crypto::X25519;  //< Curve implmentation trait alias
  using hmac_t = crypto::HmacSha256;  //< HMAC-Sha256 trait alias
  using hkdf_t = crypto::HKDF<hmac_t>;  //< HKDF trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD crypto trait alias
  using sha_t = crypto::Sha256;  //< Sha256 trait alias
  using ecies_keys_t = ecies::Keys;  //< ECIES keys trait alias
  using ephemeral_section_t = ecies::EphemeralKeySection;  //< Ephemeral key section trait alias
  using static_section_t = ecies::StaticKeySection;  //< Static key section trait alias
  using payload_t = ecies::PayloadSection;  // Payload section trait alias
  using key_info_t = ecies::SectionKeyInfo;  //< ECIES KeyInfo trait alias
  using initiator_r = ecies::EciesInitiator;  //< ECIES initator role trait alias
  using responder_r = ecies::EciesResponder;  //< ECIES responder role trait alias
  using size_type = std::uint16_t;  //< Size type trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor, creates an one-time NewSessionMessage
  /// @detail Caller responsible for setting any additional payload blocks
  NewSessionMessage()
      : ep_keys_(elligator_t::create_keys()),
        ephemeral_section_(),
        static_section_(),
        payload_(),
        static_state_(MsgState::Decrypted),
        pay_state_(MsgState::Decrypted),
        buf_()
  {
    ephemeral_section_.key(ep_keys_.pubkey);
  }

  /// @brief Partially deserializing ctor, creates an encrypted NewSession message from a secure buffer
  explicit NewSessionMessage(buffer_t buf)
      : ep_keys_(),
        ephemeral_section_(),
        static_section_(),
        payload_(),
        static_state_(MsgState::Encrypted),
        pay_state_(MsgState::Encrypted)
  {
    const exception::Exception ex{"ECIES: NewSessionMessage", __func__};

    check_buffer(buf, ex);

    buf_ = std::forward<buffer_t>(buf);

    tini2p::BytesReader<buffer_t> reader(buf_);

    deserialize_header(reader, ex);
  }

  /// @brief Create an unbound NewSessionMessage
  /// @param payload Payload section for the message
  explicit NewSessionMessage(payload_t payload)
      : ep_keys_(elligator_t::create_keys()),
        ephemeral_section_(),
        static_section_(),
        payload_(std::forward<payload_t>(payload)),
        static_state_(MsgState::Decrypted),
        pay_state_(MsgState::Decrypted),
        buf_()
  {
    ephemeral_section_.key(ep_keys_.pubkey);
  }

  /// @brief Fully-initializing ctor, creates a bound NewSessionMessage with all sections
  /// @param st_key Static key for binding ECIES session + encrypting payload
  /// @param payload Payload section containing additional information for session establishment
  NewSessionMessage(curve_t::pubkey_t st_key, payload_t payload)
      : ep_keys_(elligator_t::create_keys()),
        ephemeral_section_(),
        static_section_(std::forward<curve_t::pubkey_t>(st_key)),
        payload_(std::forward<payload_t>(payload)),
        static_state_(MsgState::Decrypted),
        pay_state_(MsgState::Decrypted),
        buf_()
  {
    ephemeral_section_.key(ep_keys_.pubkey);
  }

  /// @brief Serialize NewSessionMessage to the buffer
  /// @param ecies_keys ECIES keys for processing message sections
  NewSessionMessage& serialize(ecies_keys_t& ecies_keys)
  {
    const exception::Exception ex{"NewSessionMessage", __func__};

    ephemeral_section_.serialize();

    // set the local ephemeral keys if this is a locally created NewSession message
    if (!ep_keys_.pvtkey.is_zero())
      ecies_keys.ep_keys(ep_keys_);

    ProcessStaticKeySection<initiator_r>(static_section_, ecies_keys);
    static_state_ = MsgState::Encrypted;

    ProcessPayloadSection<initiator_r>(payload_, static_section_, ecies_keys);
    pay_state_ = MsgState::Encrypted;

    buf_.resize(size());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_data(ephemeral_section_.buffer());  // write Elligator2 encoded public key
    writer.write_data(static_section_.buffer());  // write encrypted StaticKeySection
    writer.write_data(payload_.buffer());  // write encrypted PayloadSection

    return *this;
  }

  /// @brief Deserialize NewSessionMessage from the buffer
  /// @detail Caller responsible for setting total encrypted message length before deserializing
  /// @param ecies_keys ECIES keys for processing message sections
  NewSessionMessage& deserialize(ecies_keys_t& ecies_keys)
  {
    const exception::Exception ex{"NewSessionMessage", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    // read and decode Elligator2 ephemeral key
    deserialize_header(reader, ex);

    const auto& ep_key = ephemeral_section_.key();
    ecies_keys.remote_ep_key(static_cast<curve_t::pubkey_t>(ep_key));

    if (!(ep_key == ep_keys_.pubkey))
      {  // deserializing a remote NewSession message, zero the local keypair
        ep_keys_.pubkey.zero();
        ep_keys_.pvtkey.zero();
      }

    static_section_t fk_section;
    auto& fk_buf = fk_section.buffer();
    fk_buf.resize(static_section_t::MaxLen);
    reader.read_data(fk_buf);

    // attempt to decrypt the temporary StaticKeySection
    ProcessStaticKeySection<responder_r>(fk_section, ecies_keys);

    static_section_ = std::move(fk_section);
    static_state_ = MsgState::Decrypted;

    // resize and read encrypted Payload Section
    // PayloadSection + MAC should be the remaining buffer length, if caller resized message buffer correctly
    payload_t payload;

    auto& pay_buf = payload.buffer();
    pay_buf.resize(reader.gcount());
    reader.read_data(pay_buf);

    // decrypt & deserialize the Payload Section
    ProcessPayloadSection<responder_r>(payload, static_section_, ecies_keys);
    payload_ = std::move(payload);
    pay_state_ = MsgState::Decrypted;

    return *this;
  }

  /// @brief Get the total size of the NewSessionMessage
  /// @detail Only accurate when created locally, or after being succefully decrypted + deserialized
  size_type size()
  {  // calculate MAC lengths based on section encrypted state
    const auto& mac_len = tini2p::under_cast(aead_t::MACLen);
    const auto& fk_mac_len = static_cast<std::uint8_t>(static_state_ == MsgState::Encrypted) * mac_len;
    const auto& pay_mac_len = static_cast<std::uint8_t>(pay_state_ == MsgState::Encrypted) * mac_len;

    return elligator_t::PublicKeyLen + static_section_.size() + fk_mac_len + payload_.size() + pay_mac_len;
  }

  /// @brief Get the encrypted size of the NewSessionMessage
  /// @detail Only accurate when created locally, or after being succefully decrypted + deserialized
  size_type encrypted_len()
  {
    return ephemeral_section_.size() + static_section_.encrypted_len() + payload_.encrypted_len();
  }

  /// @brief Get a const reference to the ephemeral key section
  const ephemeral_section_t& ephemeral_section() const noexcept
  {
    return ephemeral_section_;
  }

  /// @brief Get a non-const reference to the ephemeral key section
  ephemeral_section_t& ephemeral_section() noexcept
  {
    return ephemeral_section_;
  }

  /// @brief Get a const reference to the static key section
  const static_section_t& static_section() const noexcept
  {
    return static_section_;
  }

  /// @brief Get a non-const reference to the static key section
  static_section_t& static_section() noexcept
  {
    return static_section_;
  }

  /// @brief Get a const reference to the payload section
  const payload_t& payload_section() const noexcept
  {
    return payload_;
  }

  /// @brief Get a non-const reference to the payload section
  payload_t& payload_section() noexcept
  {
    return payload_;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Get whether the key section contains a static key
  std::uint8_t is_bound() const
  {
    return static_section_.has_static_key();
  }

  /// @brief Get whether the NewSession message is for an unbound session
  std::uint8_t is_unbound()
  {
    return payload_.is_unbound();
  }

  /// @brief Process a StaticKeySection based on ECIES role
  /// @detail Caller must have set decrypted remote ephemeral public key in the ECIES state
  /// @note Sets ECIES state remote static key for EciesResponder
  /// @tparam TState ECIES state for processing the static section
  /// @param section Static section to process
  /// @param ecies_keys ECIES key set including local and remote keys
  template <
      class TRole,
      typename = std::enable_if_t<std::is_same<TRole, initiator_r>::value || std::is_same<TRole, responder_r>::value>>
  NewSessionMessage& ProcessStaticKeySection(static_section_t& section, ecies_keys_t& ecies_keys)
  {
    const exception::Exception ex{"EciesX25519", __func__};

    const auto& is_initiator = std::is_same<TRole, initiator_r>::value;

    const auto& bob_static_pub = is_initiator ? ecies_keys.remote_id_key() : ecies_keys.id_keys().pubkey;
    const auto& alice_ephemeral_pub = is_initiator ? ecies_keys.ep_keys().pubkey : ecies_keys.remote_ep_key();

    auto& h = ecies_keys.h();
    h = ecies::ECIES_INITIAL_H;

    // premessage s MixHash(s)
    noise::MixHash(h, bob_static_pub);

    // e MixHash(eapk)
    // h = SHA256(h || Alice's Elligator2 ephemeral public key)
    noise::MixHash(h, alice_ephemeral_pub);

    // sharedSecret = DH(rask, bpk) or DH(bsk, rapk)
    curve_t::shrkey_t secret;
    curve_t::DH(
        secret,
        is_initiator ? ecies_keys.ep_keys().pvtkey : ecies_keys.id_keys().pvtkey,
        is_initiator ? bob_static_pub : alice_ephemeral_pub);

    // ee MixKey(DH(e, re))
    auto& chain_key = ecies_keys.chain_key();
    auto& k = ecies_keys.k();

    chain_key = ecies::ECIES_INITIAL_CHAIN_KEY;
    noise::MixKey(chain_key, k, secret);

    // n = 0
    crypto::Nonce n(0);

    // En/decrypt the key section based on role (Alice encrypts, Bob decrypts)
    auto& section_buf = section.buffer();
    if (std::is_same<TRole, initiator_r>::value)
      {
        // s EncryptAndHash(flags || s): Encrypt(flags || s, ad = h) + MixHash(ciphertext)
        section.serialize();
        aead_t::AEADEncrypt(static_cast<aead_t::key_t>(k), n, section_buf, h, section_buf);

        noise::MixHash(h, section_buf);
      }
    else
      {
        // s DecryptAndHash(flags || s): MixHash(ciphertext) + Decrypt(ciphertext, ad = h before MixHash)
        const auto ad = h;
        noise::MixHash(h, section_buf);

        aead_t::AEADDecrypt(static_cast<aead_t::key_t>(k), n, section_buf, ad, section_buf);
        section.deserialize();

        const auto& key = section.key();

        if (!key.is_zero())
          ecies_keys.remote_id_key(key);
      }

    return *this;
  }

  /// @brief Process a PayloadSection based on ECIES role
  /// @detail
  ///     Decrypted ephemeral key used for KDF if ephemeral_section_t has ephemeral key flag set,
  ///         and no static key flag set.
  ///     Decrypted static key used for KDF if ephemeral_section_t has static key flag set.
  ///     Remote one-time ephemeral key used for KDF if ephemeral_section_t has one-time flag set (no reply, unbound).
  ///     Caller is responsible for setting necessary keys in ECIES state
  /// @tparam TState ECIES state for processing the payload section
  /// @param section Payload section to process
  /// @param ephemeral_section Decrypted ephemeral section (for chain key + AD)
  /// @param sta_section Decrypted static section (for chain key + AD, unused if no static key flag set)
  template <
      class TRole,
      typename = std::enable_if_t<std::is_same<TRole, initiator_r>::value || std::is_same<TRole, responder_r>::value>>
  NewSessionMessage&
  ProcessPayloadSection(payload_t& section, const static_section_t& fk_section, ecies_keys_t& ecies_keys)
  {
    const exception::Exception ex{"EciesX25519", __func__};

    auto& chain_key = ecies_keys.chain_key();
    check_chain_key(chain_key, ex);

    auto& k = ecies_keys.k();

    // n = 0
    crypto::Nonce n(0);

    if (fk_section.has_static_key())
      {  // ss MixKey(DH(s, rs))
        curve_t::shrkey_t secret;
        curve_t::DH(secret, ecies_keys.id_keys().pvtkey, ecies_keys.remote_id_key());
        noise::MixKey(chain_key, k, secret);
      }
    else
        ++n;  // n = 1

    auto& h = ecies_keys.h();

    // En/decrypt the key section based on role (Alice encrypts, Bob decrypts)
    auto& section_buf = section.buffer();
    if (std::is_same<TRole, initiator_r>::value)
      {  // EncryptAndHash(payload): Encrypt(payload, ad = h) + MixHash(ciphertext)
        section.serialize();
        aead_t::AEADEncrypt(static_cast<aead_t::key_t>(k), n, section_buf, h, section_buf);

        noise::MixHash(h, section_buf);
      }
    else
      {  // DecryptAndHash(payload): MixHash(ciphertext) + Decrypt(payload, ad = h before MixHash)
        const auto ad = h;
        noise::MixHash(h, section_buf);

        aead_t::AEADDecrypt(static_cast<aead_t::key_t>(k), n, section_buf, ad, section_buf);
        section.deserialize();
      }

    return *this;
  }

  /// @brief Equality comparison with another NewSessionMessage
  std::uint8_t operator==(const NewSessionMessage& oth)
  {  // attempt constant-time comparison
    const auto& ep_eq = static_cast<std::uint8_t>(ephemeral_section_ == oth.ephemeral_section_);
    const auto& st_eq = static_cast<std::uint8_t>(static_section_ == oth.static_section_);
    const auto& pay_eq = static_cast<std::uint8_t>(payload_ == oth.payload_);
    const auto& static_state_eq = static_cast<std::uint8_t>(static_state_ == oth.static_state_);
    const auto& pay_state_eq = static_cast<std::uint8_t>(pay_state_ == oth.pay_state_);

    return (ep_eq * st_eq * pay_eq * static_state_eq * pay_state_eq);
  }

 private:
  void deserialize_header(tini2p::BytesReader<buffer_t>& reader, const exception::Exception& ex)
  {
    ephemeral_section_t ephemeral_section;
    reader.read_data(ephemeral_section.buffer());
    ephemeral_section.deserialize();

    ephemeral_section_ = std::move(ephemeral_section);
  }

  void check_chain_key(const curve_t::shrkey_t& chain_key, const exception::Exception& ex) const
  {
    if (chain_key.is_zero())
      ex.throw_ex<std::logic_error>("null chain key");
  }

  void check_h(const sha_t::digest_t& h, const exception::Exception& ex) const
  {
    if (h.is_zero())
      ex.throw_ex<std::invalid_argument>("null h");
  }

  void check_buffer(const buffer_t& buf, const exception::Exception& ex) const
  {
    if (buf.is_zero())
      ex.throw_ex<std::invalid_argument>("null message buffer");

    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);
    const auto& buf_len = buf.size();

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  elligator_t::keypair_t ep_keys_;
  ephemeral_section_t ephemeral_section_;
  static_section_t static_section_;
  payload_t payload_;
  MsgState static_state_, pay_state_;
  buffer_t buf_;
};
}  // namespace ecies
}  // namespace tini2p

#endif  // SRC_ECIES_NEW_SESSION_H_
