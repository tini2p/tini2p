/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ECIES_EXISTING_SESSION_H_
#define SRC_ECIES_EXISTING_SESSION_H_

#include <mutex>
#include <variant>

#include "src/data/blocks/ack.h"
#include "src/data/blocks/ack_request.h"
#include "src/data/blocks/date_time.h"
#include "src/data/blocks/ecies_options.h"
#include "src/data/blocks/garlic.h"
#include "src/data/blocks/message_number.h"
#include "src/data/blocks/new_dh_key.h"
#include "src/data/blocks/padding.h"
#include "src/data/blocks/termination.h"

#include "src/ecies/cipher_state.h"

namespace tini2p
{
namespace ecies
{
/// @class ExistingSessionMessage
/// @brief Message for an existing ECIES session
class ExistingSessionMessage
{
 public:
  enum : std::uint16_t
  {
    SessionTagLen = data::ECIESOptionsBlock::DefaultTagLen,  //< Session tag length
    MinLen = SessionTagLen,  //< Session tag required, no payload or MAC, see spec
    MaxLen = 62708,  //< GarlicClove::MaxMsgLen, see I2NP section in ECIES spec
    MaxPayloadLen = 62700,  //< MaxLen - SessionTagLen(8)
  };

  using aead_t = crypto::ChaChaPoly1305;  //< AEAD crypto implementation trait alias
  /// @brief Payload block variant trait alias
  using payload_v = std::variant<
      data::AckBlock,
      data::AckRequestBlock,
      data::DateTimeBlock,
      data::ECIESOptionsBlock,
      data::GarlicBlock,
      data::MessageNumberBlock,
      data::NewDHKeyBlock,
      data::PaddingBlock,
      data::TerminationBlock>;
  using payload_blocks_t = std::vector<payload_v>;  //< Payload blocks container trait alias
  using initiator_r = ecies::EciesInitiator;  //< ECIES initiator role trait alias
  using responder_r = ecies::EciesResponder;  //< ECIES responder role trait alias
  using in_ecies_state_t = ecies::CipherState<responder_r>;  //< Inbound ECIES cipher state trait alias
  using out_ecies_state_t = ecies::CipherState<initiator_r>;  //< Outbound ECIES cipher state trait alias
  using session_tag_t = ecies::Tag;  //< Session tag trait alias
  using size_type = std::uint16_t;  //< Size type trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor, creates an ExistingSessionMessage
  /// @detail Caller must set tag before serializing
  ExistingSessionMessage() : tag_(), blocks_(), pay_buf_(), buf_() {}

  /// @brief Create an ExistingSessionMessage from payload block
  /// @detail Caller must set tag before serializing
  template <class TBlock>
  explicit ExistingSessionMessage(TBlock block)
      : tag_(), blocks_(), pay_buf_(), buf_()
  {
    const exception::Exception ex{"ExistingSessionMessage", __func__};

    if (block_to_type<TBlock>(ex) == data::Block::type_t::Garlic)
      check_garlic_block(block, ex);

    blocks_.emplace_back(std::forward<TBlock>(block));
  }

  /// @brief Create an ExistingSessionMessage from payload blocks
  /// @detail Caller must set tag before serializing
  explicit ExistingSessionMessage(payload_blocks_t blocks)
      : tag_(), blocks_(), pay_buf_(), buf_()
  {
    const exception::Exception ex{"ExistingSessionMessage", __func__};

    check_blocks_order(blocks, ex);

    blocks_ = std::forward<payload_blocks_t>(blocks);
  }

  /// @brief Fully-initializing ctor, creates an ExistingSessionMessage from session tag & blocks container
  ExistingSessionMessage(session_tag_t tag, payload_blocks_t pay_blocks)
      : tag_(), blocks_(), pay_buf_(), buf_()
  {
    const exception::Exception ex{"ExistingSessionMessage", __func__};

    check_tag(tag, ex);

    check_blocks_order(pay_blocks, ex);

    tag_ = std::forward<session_tag_t>(tag);
    blocks_ = std::forward<payload_blocks_t>(pay_blocks);
  }

  /// @brief Partially deserializing ctor, creates an ExistingSessionMessage from a secure buffer
  /// @detail Reads session tag from the ciphertext buffer
  /// @param buf Secure buffer containing an encrypted ExistingSessionMessage
  explicit ExistingSessionMessage(buffer_t buf) : tag_(), blocks_(), pay_buf_(), buf_()
  {
    const exception::Exception ex{"ExistingSessionMessage", __func__};

    check_buffer(buf, ex);

    tini2p::BytesReader<buffer_t> reader(buf);

    session_tag_t tag;
    reader.read_data(tag);

    check_tag(tag, ex);

    buf_ = std::forward<buffer_t>(buf);
    tag_ = std::move(tag);
  }

  /// @brief Copy-ctor
  ExistingSessionMessage(const ExistingSessionMessage& oth)
  {
    tag_ = oth.tag_;
    blocks_ = oth.blocks_;
    pay_buf_ = oth.pay_buf_;
    buf_ = oth.buf_;
  }

  /// @brief Copy assignment operator
  ExistingSessionMessage& operator=(const ExistingSessionMessage& oth)
  {
    std::scoped_lock bgd(blocks_mutex_);

    tag_ = oth.tag_;
    blocks_ = oth.blocks_;
    pay_buf_ = oth.pay_buf_;
    buf_ = oth.buf_;

    return *this;
  }

  /// @brief Serialize and encrypt the ExistingSessionMessage to the buffer
  /// @detail Caller responsible for performing necessary ratchets on ECIES state
  ExistingSessionMessage& serialize(out_ecies_state_t& ecies_state)
  {
    const exception::Exception ex{"ExistingSessionMessage", __func__};

    check_tag(tag_, ex);

    buf_.resize(encrypted_len());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_data(tag_);

    {  // lock payload blocks
      std::scoped_lock bgd(blocks_mutex_);
      if (blocks_.size())
        {
          // serialize + encrypt payload blocks
          EncryptPayload(blocks_, ecies_state);

          // write encrypted payload buffer
          writer.write_data(pay_buf_);
        }
    }  // end-payload-blocks-lock

    // reclaim unused space
    buf_.resize(writer.count());
    buf_.shrink_to_fit();

    return *this;
  }

  /// @brief Decrypt and deserialize the ExistingSessionMessage from the buffer
  /// @detail Caller responsible for performing necessary ratchets on ECIES state,
  ///    and resizing message buffer to full encrypted ExistingSessionMessage length
  ExistingSessionMessage& deserialize(in_ecies_state_t& ecies_state)
  {
    const exception::Exception ex{"ExistingSessionMessage", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    session_tag_t tag;
    reader.read_data(tag);

    check_tag(tag, ex);

    tag_ = std::move(tag);

    const auto encrypted_pay_len = reader.gcount();
    if (encrypted_pay_len)
      {
        // resize payload buffer, read encrypted payload
        // payload + MAC should be the remaining buffer length, if caller resized message buffer correctly
        pay_buf_.resize(encrypted_pay_len);
        reader.read_data(pay_buf_);

        // decrypt and deserialize encrypted payload
        payload_blocks_t blocks;
        DecryptPayload(blocks, ecies_state);

        // lock payload blocks and swap with temporary container
        std::scoped_lock bgd(blocks_mutex_);
        blocks_.swap(blocks);
      }

    // reclaim unused space
    buf_.resize(reader.count());
    buf_.shrink_to_fit();

    return *this;
  }

  /// @brief Get the unencrypted size of the ExistingSessionMessage
  /// @detail Only valid when created locally, and/or after being decrypted + deserialized
  size_type size() const noexcept
  {
    return tini2p::under_cast(SessionTagLen) + blocks_size(blocks_);
  }

  /// @brief Get the encrypted size of the ExistingSessionMessage
  /// @detail Only valid when created locally, and/or after being decrypted + deserialized
  size_type encrypted_len() const noexcept
  {
    return size() + tini2p::under_cast(aead_t::MACLen);
  }

  /// @brief Get the encrypted size of the ExistingSessionMessage payload
  /// @detail Only valid when created locally, and/or after being decrypted + deserialized
  size_type encrypted_payload_len() const noexcept
  {
    return blocks_size(blocks_) + tini2p::under_cast(aead_t::MACLen);
  }

  /// @brief Get a const reference to the session tag
  const session_tag_t& tag() const noexcept
  {
    return tag_;
  }

  /// @brief Set the session tag
  /// @throws Logic error if the tag is null
  void tag(session_tag_t tag)
  {
    const exception::Exception ex{"ExistingSessionMessage", __func__};

    check_tag(tag, ex);

    tag_ = std::forward<session_tag_t>(tag);
  }

  /// @brief Set the payload blocks
  ExistingSessionMessage& blocks(payload_blocks_t pay_blocks)
  {
    const exception::Exception ex{"ExistingSessionMessage", __func__};

    check_blocks_order(pay_blocks, ex);

    std::scoped_lock bgd(blocks_mutex_);
    blocks_ = std::forward<payload_blocks_t>(pay_blocks);

    return *this;
  }

  /// @brief Add a block to the message payload
  template <class TBlock>
  ExistingSessionMessage& add_block(TBlock block)
  {
    const exception::Exception ex{"ECIES: ExistingSessionMessage", __func__};

    std::scoped_lock sgd(blocks_mutex_);

    check_additional_block(blocks_, block, ex);

    if (std::is_same<TBlock, data::PaddingBlock>::value || std::is_same<TBlock, data::TerminationBlock>::value)
      {
        if (has_block<data::TerminationBlock>())
          {
            auto next_to_last = blocks_.begin();
            std::advance(next_to_last, blocks_.size() - 2);
            blocks_.insert(next_to_last, std::forward<TBlock>(block));
          }
        else
          blocks_.emplace_back(std::forward<TBlock>(block));
      }
    else
      blocks_.insert(blocks_.begin(), std::forward<TBlock>(block));

    return *this;
  }

  /// @brief Get whether the message contains a payload block of a given type
  /// @return One if the block is found, zero if not
  template <class TBlock>
  std::uint8_t has_block()
  {
    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return has_block<TBlock>(blocks_);
  }

  /// @brief Get a block of a given block type
  /// @tparam TBlock Block type to search for
  /// @throws Runtime error if no block of the given type is found
  template <class TBlock>
  const TBlock& get_block()
  {
    const exception::Exception ex{"ECIES: ExistingSession", __func__};

    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return get_block<TBlock>(blocks_, ex);
  }

  /// @brief Extract a block of a given block type
  /// @tparam TBlock Block type to extract
  /// @throws Runtime error if no block of the given type is found
  template <class TBlock>
  TBlock extract_block()
  {
    const exception::Exception ex{"ECIES: ExistingSession", __func__};

    std::scoped_lock sgd(blocks_mutex_);

    return extract_block<TBlock>(blocks_, ex);
  }

  /// @brief Serialize and encrypt the payload blocks to the payload buffer
  /// @detail Removes used symmetric key and session tag from ECIES state
  void EncryptPayload(payload_blocks_t& blocks, out_ecies_state_t& ecies_state)
  {
    pay_buf_.resize(blocks_size(blocks));

    tini2p::BytesWriter<buffer_t> writer(pay_buf_);

    for (auto& block : blocks)
      {
        std::visit(
            [&writer](auto& b) {
              b.serialize();
              writer.write_data(b.buffer());
            },
            block);
      }

    // encrypted_payload = ChaCha20-Poly1305::Encrypt(symmetric_key, nonce, data = payload, AD = session_tag)
    const auto& key = ecies_state.sym_key(tag_);
    const auto& nonce = ecies_state.nonce(tag_);
    aead_t::AEADEncrypt(static_cast<aead_t::key_t>(key), nonce, pay_buf_, tag_, pay_buf_);

    // remove used symmetric key + session tag, advances symmetric and tag ratchets
    ecies_state.remove_used(nonce);
  }

  /// @brief Decrypt and deserialize the payload blocks from the payload buffer
  /// @detail Removes used symmetric key and session tag from ECIES state
  void DecryptPayload(payload_blocks_t& blocks, in_ecies_state_t& ecies_state)
  {
    const exception::Exception ex{"ExistingSessionMessage", __func__};

    // decrypt the payload buffer
    const auto& key = ecies_state.sym_key(tag_);
    const auto& nonce = ecies_state.nonce(tag_);
    aead_t::AEADDecrypt(static_cast<aead_t::key_t>(key), nonce, pay_buf_, tag_, pay_buf_);

    // remove used symmetric key + session tag
    // advances symmetric and tag ratchets if windows are empty
    ecies_state.remove_used(nonce);

    tini2p::BytesReader<buffer_t> reader(pay_buf_);

    std::uint8_t padding_seen(0), termination_seen(0);

    // read blocks from buffer, and add to temporary container
    while (reader.gcount())
      {
        data::Block::type_t block_type;
        reader.read_bytes(block_type);

        // check if blocks are the correct type and order
        check_block_type(block_type, padding_seen, termination_seen, ex);

        data::Block::size_type block_size;
        reader.read_bytes(block_size, tini2p::Endian::Big);

        const auto& header_len = tini2p::under_cast(data::Block::HeaderLen);
        reader.skip_back(header_len);

        // read claimed block buffer
        data::Block::buffer_t block_buffer(header_len + block_size);
        reader.read_data(block_buffer);

        // deserialize and add block to temporary container
        add_to_blocks(blocks, block_type, block_buffer, ex);
      }
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another ExistingSessionMessage
  std::uint8_t operator==(const ExistingSessionMessage& oth)
  {
    const auto& tag_eq = static_cast<std::uint8_t>(tag_ == oth.tag_);

    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);
    const auto& blocks_eq = static_cast<std::uint8_t>(blocks_ == oth.blocks_);

    return (tag_eq * blocks_eq);
  }

 private:
  size_type blocks_size(const payload_blocks_t& blocks) const
  {
    size_type size(0);
    for (const auto& block : blocks)
      size += std::visit([](const auto& b) { return b.size(); }, block);

    return size;
  }

  void check_buffer(const buffer_t& buf, const exception::Exception& ex) const
  {
    if (buf.is_zero())
      ex.throw_ex<std::logic_error>("null buffer");

    const auto& buf_len = buf.size();
    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  template <class TBlock>
  data::Block::type_t block_to_type(const exception::Exception& ex) const
  {
    data::Block::type_t type(data::Block::type_t::Reserved);

    if (std::is_same<TBlock, data::AckBlock>::value)
      type = data::Block::type_t::Ack;
    else if (std::is_same<TBlock, data::AckRequestBlock>::value)
      type = data::Block::type_t::AckRequest;
    else if (std::is_same<TBlock, data::DateTimeBlock>::value)
      type = data::Block::type_t::DateTime;
    else if (std::is_same<TBlock, data::ECIESOptionsBlock>::value)
      type = data::Block::type_t::ECIESOptions;
    else if (std::is_same<TBlock, data::GarlicBlock>::value)
      type = data::Block::type_t::Garlic;
    else if (std::is_same<TBlock, data::MessageNumberBlock>::value)
      type = data::Block::type_t::MessageNumber;
    else if (std::is_same<TBlock, data::NewDHKeyBlock>::value)
      type = data::Block::type_t::NewDHKey;
    else if (std::is_same<TBlock, data::PaddingBlock>::value)
      type = data::Block::type_t::Padding;
    else if (std::is_same<TBlock, data::TerminationBlock>::value)
      type = data::Block::type_t::Termination;
    else
      ex.throw_ex<std::invalid_argument>("invalid payload block type");

    return type;
  }

  template <class T>
  void check_garlic_block(const T& garlic_block, const exception::Exception& ex) const
  {
    const auto& msg_type = garlic_block.i2np_type();

    if (msg_type != data::I2NPHeader::Type::Data && msg_type != data::I2NPHeader::Type::DatabaseStore)
      {
        ex.throw_ex<std::logic_error>(
            "invalid Garlic block I2NP type: " + std::to_string(tini2p::under_cast(msg_type)) + ", expected Data("
            + std::to_string(tini2p::under_cast(data::I2NPHeader::Type::Data)) + ") or DatabaseStore("
            + std::to_string(tini2p::under_cast(data::I2NPHeader::Type::DatabaseStore)) + ")");
      }
  }
  
  void check_tag(const session_tag_t& tag, const exception::Exception& ex)
  {
    if (tag.is_zero())
      ex.throw_ex<std::logic_error>("null session tag.");
  }

  void check_block_type(const data::Block::type_t& type, std::uint8_t& padding_seen, std::uint8_t& termination_seen, const exception::Exception& ex)
  {
    using block_type_t = data::Block::type_t;

    if (type == block_type_t::Ack || type == block_type_t::AckRequest || type == block_type_t::DateTime
        || type == block_type_t::ECIESOptions || type == block_type_t::Garlic || type == block_type_t::MessageNumber
        || type == block_type_t::NewDHKey)
      {
        if (padding_seen || termination_seen)
          ex.throw_ex<std::logic_error>("padding or termination block must be the final block.");
      }
    else if (type == block_type_t::Padding)
      {
        if (termination_seen)
          ex.throw_ex<std::logic_error>("termination block must be the final block.");
        else if (padding_seen)
          ex.throw_ex<std::logic_error>("only one padding block allowed.");
        else
          padding_seen += 1;
      }
    else if (type == block_type_t::Termination)
      {
        if (termination_seen)
          ex.throw_ex<std::logic_error>("only one termination block allowed.");
        else
          termination_seen += 1;
      }
    else
      ex.throw_ex<std::logic_error>("invalid block type: " + std::to_string(tini2p::under_cast(type)));
  }

  void check_blocks_order(const payload_blocks_t blocks, const exception::Exception& ex)
  {
    std::uint8_t padding_seen(0), termination_seen(0);
    for (const auto& block : blocks)
      {
        const auto block_type = std::visit([](const auto& b) { return b.type(); }, block);
        check_block_type(block_type, padding_seen, termination_seen, ex);
      }
  }

  void check_encrypted_len(const size_type encrypted_len, const exception::Exception& ex)
  {
    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);
    if (encrypted_len < min_len || encrypted_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid encrypted length: " + std::to_string(encrypted_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  // add block to blocks container
  // caller responsible for checking block order
  void add_to_blocks(
      payload_blocks_t& blocks,
      const data::Block::type_t& type,
      data::Block::buffer_t block_buffer,
      const exception::Exception& ex)
  {
    if (type == data::Block::type_t::Ack)
      blocks.emplace_back(data::AckBlock(std::forward<data::Block::buffer_t>(block_buffer)));
    else if (type == data::Block::type_t::AckRequest)
      blocks.emplace_back(data::AckRequestBlock(std::forward<data::Block::buffer_t>(block_buffer)));
    else if (type == data::Block::type_t::DateTime)
      blocks.emplace_back(data::DateTimeBlock(std::forward<data::Block::buffer_t>(block_buffer)));
    else if (type == data::Block::type_t::ECIESOptions)
      blocks.emplace_back(data::ECIESOptionsBlock(std::forward<data::Block::buffer_t>(block_buffer)));
    else if (type == data::Block::type_t::Garlic)
      blocks.emplace_back(data::GarlicBlock(std::forward<data::Block::buffer_t>(block_buffer)));
    else if (type == data::Block::type_t::MessageNumber)
      blocks.emplace_back(data::MessageNumberBlock(std::forward<data::Block::buffer_t>(block_buffer)));
    else if (type == data::Block::type_t::NewDHKey)
      blocks.emplace_back(data::NewDHKeyBlock(std::forward<data::Block::buffer_t>(block_buffer)));
    else if (type == data::Block::type_t::Padding)
      blocks.emplace_back(data::PaddingBlock(std::forward<data::Block::buffer_t>(block_buffer)));
    else if (type == data::Block::type_t::Termination)
      blocks.emplace_back(data::TerminationBlock(std::forward<data::Block::buffer_t>(block_buffer)));
    else  // should never happen, but just in case
      ex.throw_ex<std::logic_error>("invalid block type: " + std::to_string(tini2p::under_cast(type)));
  }

  template <class TBlock>
  data::Block::type_t check_block(const TBlock& block, const exception::Exception& ex) const
  {
    const auto& block_type = block_to_type<TBlock>(ex);
    if (block_type == data::Block::type_t::Garlic)
      check_garlic_block(block, ex);

    return block_type;
  }

  template <class TBlock>
  data::Block::type_t
  check_additional_block(const payload_blocks_t& blocks, const TBlock& block, const exception::Exception& ex) const
  {
    const auto& block_type = check_block(block, ex);

    const auto& blocks_len = blocks_size(blocks);
    const auto& block_len = block.size();

    const auto& max_len = tini2p::under_cast(MaxPayloadLen);

    if (blocks_len + block_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "additional block too large: " + std::to_string(block_len)
            + ", current payload size: " + std::to_string(blocks_len) + ", max payload: " + std::to_string(max_len));
      }

    if ((block_type == data::Block::type_t::Padding || block_type == data::Block::type_t::Termination)
        && has_block<TBlock>())
      ex.throw_ex<std::logic_error>("invalid additional block, multiple padding or termination blocks");

    return block_type;
  }

  template <class TBlock>
  std::uint8_t has_block(const payload_blocks_t& blocks) const
  {
    const auto blocks_end = blocks.end();

    return static_cast<std::uint8_t>(
        std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); })
        != blocks_end);
  }

  template <class TBlock>
  const TBlock& get_block(const payload_blocks_t& blocks, const exception::Exception& ex) const
  {
    const auto blocks_end = blocks.end();

    auto it = std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); });

    if (it == blocks_end)
      ex.throw_ex<std::runtime_error>("no block found");

    return std::get<TBlock>(*it);
  }

  template <class TBlock>
  TBlock extract_block(payload_blocks_t& blocks, const exception::Exception& ex)
  {
    const auto blocks_end = blocks.end();

    auto it = std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); });

    if (it == blocks_end)
      ex.throw_ex<std::runtime_error>("no block found");

    auto block = std::move(*it);
    blocks_.erase(it);

    return std::get<TBlock>(block);
  }

  session_tag_t tag_;

  payload_blocks_t blocks_;
  std::shared_mutex blocks_mutex_;

  buffer_t pay_buf_, buf_;
};
}  // namespace ecies
}  // namespace tini2p

#endif  // SRC_ECIES_EXISTING_SESSION_H_
