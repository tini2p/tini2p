/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ECIES_NEW_SESSION_REPLY_H_
#define SRC_ECIES_NEW_SESSION_REPLY_H_

#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/elligator2.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/kdf_context.h"
#include "src/crypto/sha.h"

#include "src/ecies/dh_state.h"
#include "src/ecies/keys.h"
#include "src/ecies/message_sections.h"
#include "src/ecies/role.h"
#include "src/ecies/tag_state.h"

#include "src/noise/noise.h"

namespace tini2p
{
namespace ecies
{
/// @brief NewSessionReply payload context
static const crypto::KDFContext<crypto::HmacSha256> ECIES_ATTACH_PAYLOAD_CTX{std::string("AttachPayloadKDF")};

/// @class NewSessionReplyMessage
/// @brief Message for a reply to a new ECIES session
/// @detail Used to securely establish a new end-to-end ECIES session between Alice and Bob.
///
///    As an initial message, Alice sends a set of keys to Bob. The set always
///    contains a one-time ephemeral public key, used to encrypt an EphemeralKeySection.
///
///    If the message is for one-time use, the ephemeral public key is the only key transferred.
///    An StaticKeySection still follows, containing flags indicating one-time use.
///
///    For bound sessions, the EphemeralKeySection is followed by a StaticKeySection,
///    containing Alice's static public key.
///
///    One-time and unbound sessions use a one-way handshake between Alice and Bob, satisfying the N Noise pattern:
///
///    s <-
///    ...
///    e, es ->
///
///    For bound sessions, our handshake closely matches the IK Noise pattern:
///
///    s <-
///    ...
///    e, es, s, ss ->
///    <- e, ee, se
///
///    Bob's half of the handshake will establish his new outbound session, and Alice's inbound session.
///
///    Bob includes a reply tag, prepended to the EphemeralKeySection, derived from the first received NewSessionReplyMessage
///    from Alice.
///
///    Alice uses the reply tag to associate the inbound session with the original outbound session
///    (instead of a new session from Bob).
///
///    The full Noise handshake must complete before starting the ratchets, and sending ExistingSessionMessages.
///    Each peer will have one outbound and one inbound set of ratchets.
class NewSessionReplyMessage
{
  enum struct MsgState : std::uint8_t
  {
    Decrypted,
    Encrypted,
  };

 public:
  enum : std::uint16_t
  {
    KeydataLen = 64,
    MinLen = 72,  //< ReplyTag(8) + Ephemeral key (32) + ReplyPayload (no additional blocks) (16) + Poly1305::MAC(16)
    MaxLen = 62708,  //< I2NP max, see spec
  };

  using elligator_t = crypto::Elligator2;  //< Elligator2 trait alias
  using curve_t = crypto::X25519;  //< Curve implmentation trait alias
  using hmac_t = crypto::HmacSha256;  //< HMAC-Sha256 trait alias
  using hkdf_t = crypto::HKDF<hmac_t>;  //< HKDF trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD crypto trait alias
  using ecies_keys_t = ecies::Keys;  //< ECIES keys trait alias
  using key_info_t = ecies::SectionKeyInfo;  //< KeyInfo trait alias
  using ephemeral_section_t = ecies::EphemeralKeySection;  //< Ephemeral key section trait alias
  using flags_key_section_t = ecies::StaticKeySection;  //< Static key section trait alias
  using payload_t = ecies::ReplyPayloadSection;  // Payload section trait alias
  using tag_t = ecies::Tag;  //< ECIES tag state trait alias
  using initiator_r = ecies::EciesInitiator;  //< ECIES initator role trait alias
  using responder_r = ecies::EciesResponder;  //< ECIES responder role trait alias
  using size_type = std::uint16_t;  //< Size type trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor
  NewSessionReplyMessage()
      : reply_tag_(),
        ep_keys_(elligator_t::create_keys()),
        ep_section_(),
        payload_(),
        pay_state_(MsgState::Decrypted)
  {
    ep_section_.key(ep_keys_.pubkey);
  }

  /// @brief Fully-initializing ctor, creates a NewSessionReplyMessage with reply payload section
  /// @param reply_tag Reply tag derived from Alice's NewSessionReplyMessage
  /// @param chain_key Chain key from Alice's NewSessionReplyMessage PayloadSection
  /// @param ep_key Bob's ephemeral key for DH-encrypting FlagsKey section
  /// @param payload Reply payload section containing an Ack to the initiating NewSessionReplyMessage
  NewSessionReplyMessage(tag_t reply_tag, payload_t payload)
      : reply_tag_(std::forward<tag_t>(reply_tag)),
        ep_keys_(elligator_t::create_keys()),
        ep_section_(),
        payload_(std::forward<payload_t>(payload)),
        pay_state_(MsgState::Decrypted)
  {
    const exception::Exception ex{"NewSessionReplyMessage", __func__};

    check_reply_tag(reply_tag_, ex);

    ep_section_.key(ep_keys_.pubkey);
  }

  /// @brief Partially deserializing ctor
  /// @detail Reads the reply tag from the buffer
  ///     Caller responsible for calling deserialize to decrypt the FlagsKey and Payload sections
  NewSessionReplyMessage(buffer_t buf)
      : reply_tag_(),
        ep_keys_(),
        ep_section_(),
        payload_(),
        pay_state_(MsgState::Encrypted),
        buf_()
  {
    const exception::Exception ex{"NewSessionReplyMessage", __func__};

    check_buffer(buf, ex);
    buf_ = std::forward<buffer_t>(buf);

    tini2p::BytesReader<buffer_t> reader(buf_);
    deserialize_header(reader, ex);
  }

  /// @brief Serialize NewSessionReplyMessage to the buffer
  /// @param ecies_keys ECIES keys for processing message sections
  /// @return Cipherstate k for the opposite direction
  NewSessionReplyMessage& serialize(ecies_keys_t& ecies_keys)
  {
    const exception::Exception ex{"NewSessionReplyMessage", __func__};

    check_reply_tag(reply_tag_, ex);

    ep_section_.serialize();

    // set the local ephemeral keys if this is a locally created NewSession message
    if (!ep_keys_.pvtkey.is_zero())
      ecies_keys.ep_keys(ep_keys_);

    buf_.resize(encrypted_len());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_data(reply_tag_);
    writer.write_data(ep_section_.buffer());  // write Elligator2 encoded public key

    // Complete the handshake, and call Split()
    aead_t::mac_t mac{};
    ProcessSplit<initiator_r>(mac, ecies_keys);
    writer.write_data(mac);

    ProcessPayloadSection<initiator_r>(payload_, ecies_keys);
    pay_state_ = MsgState::Encrypted;
    writer.write_data(payload_.buffer());  // write encrypted PayloadSection

    return *this;
  }

  /// @brief Deserialize NewSessionReplyMessage from the buffer
  /// @param ecies_keys ECIES keys for processing message sections
  /// @return Cipherstate key for the opposite direction
  NewSessionReplyMessage& deserialize(ecies_keys_t& ecies_keys)
  {
    const exception::Exception ex{"NewSessionReplyMessage", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    deserialize_header(reader, ex);

    const auto& ep_key = ep_section_.key();
    ecies_keys.remote_ep_key(static_cast<curve_t::pubkey_t>(ep_key));

    if (!(ep_key == ep_keys_.pubkey))
      {  // deserializing a remote NewSession message, zero local ephemeral keypair
        ep_keys_.pubkey.zero();
        ep_keys_.pvtkey.zero();
      }

    // Complete the handshake, and call Split()
    aead_t::mac_t mac;
    reader.read_data(mac);
    ProcessSplit<responder_r>(mac, ecies_keys);

    // resize and read encrypted Payload Section
    // PayloadSection + MAC should be the remaining buffer length, if caller resized message buffer correctly
    payload_t payload;

    auto& pay_buf = payload.buffer();
    pay_buf.resize(reader.gcount());
    reader.read_data(pay_buf);

    // decrypt & deserialize the Payload Section
    ProcessPayloadSection<responder_r>(payload, ecies_keys);
    payload_ = std::move(payload);
    pay_state_ = MsgState::Decrypted;

    return *this;
  }

  /// @brief Complete the handshake, and call Noise Split() to get directional cipherstate keys
  template <
      class TRole,
      typename = std::enable_if_t<std::is_same<TRole, initiator_r>::value || std::is_same<TRole, responder_r>::value>>
  NewSessionReplyMessage& ProcessSplit(aead_t::mac_t& mac, ecies_keys_t& ecies_keys)
  {
    const exception::Exception ex{"EciesX25519", __func__};

    const auto& is_initiator = std::is_same<TRole, initiator_r>::value;
    const auto& local_ep_keys = ecies_keys.ep_keys();
    const auto& remote_ep_key = ecies_keys.remote_ep_key();

    if (local_ep_keys.pvtkey.is_zero())
      ex.throw_ex<std::invalid_argument>("null local ephemeral private key.");

    if (remote_ep_key.is_zero())
      ex.throw_ex<std::logic_error>("null remote ephemeral key.");

    auto& h = ecies_keys.h();

    // MixHash(tag)
    noise::MixHash(h, reply_tag_);

    // MixHash(e) or MixHash(re) (Bob's ephemeral public key)
    noise::MixHash(h, is_initiator ? local_ep_keys.pubkey : remote_ep_key); 

    // Bob's and Alice's ephemeral keys
    // sharedSecret = DH(e, re)
    curve_t::shrkey_t shared_secret;
    curve_t::DH(shared_secret, local_ep_keys.pvtkey, remote_ep_key);

    auto& chain_key = ecies_keys.chain_key();
    auto& k = ecies_keys.k();

    // MixKey(DH(e, re))
    noise::MixKey(chain_key, k, shared_secret);

    // Bob's ephemeral and Alice's static keys
    // sharedSecret = DH(e, rs) or DH(s, re)
    curve_t::DH(
        shared_secret,
        is_initiator ? local_ep_keys.pvtkey : ecies_keys.id_keys().pvtkey,
        is_initiator ? ecies_keys.remote_id_key() : remote_ep_key);

    // MixKey(DH(e, rs)) or MixKey(DH(s, re))
    noise::MixKey(chain_key, k, shared_secret);

    crypto::Nonce n(0);

    // Split()
    // temp_k1 (left) is the cipherstate key for:
    //   - initiator (Alice) outbound sessions
    //   - responder (Bob) inbound sessions
    // temp_k2 (right) is the cipherstate key for:
    //   - initiator (Alice) inbound sessions
    //   - responder (Bob) outbound sessions
    // Use chain key and cipherstate key to intialize the DHRatchet for both directions
    auto& oth_k = ecies_keys.oth_k();
    if (is_initiator)
      {
        aead_t::AEADEncrypt(static_cast<aead_t::key_t>(k), n, mac.data(), 0, h, mac.data(), aead_t::MACLen);

        noise::MixHash(h, mac);

        std::tie(oth_k, k) = noise::Split(chain_key);
      }
    else
      {
        auto ad = h;

        noise::MixHash(h, mac);

        aead_t::AEADDecrypt(static_cast<aead_t::key_t>(k), n, mac.data(), 0, ad, mac.data(), aead_t::MACLen);

        std::tie(k, oth_k) = noise::Split(chain_key);
      }

    return *this;
  }

  /// @brief Process a PayloadSection based on ECIES role
  /// @detail
  ///     Decrypted ephemeral key used for KDF if ephemeral_section_t has ephemeral key flag set,
  ///         and no static key flag set.
  ///     Decrypted static key used for KDF if ephemeral_section_t has static key flag set.
  ///     Remote one-time ephemeral key used for KDF if ephemeral_section_t has one-time flag set (no reply, unbound).
  ///     Caller is responsible for setting necessary keys in ECIES state
  /// @tparam TState ECIES state for processing the payload section
  /// @param section Payload section to process
  /// @param ecies_keys ECIES key state for en/decrypting the payload
  template <
      class TRole,
      typename = std::enable_if_t<std::is_same<TRole, initiator_r>::value || std::is_same<TRole, responder_r>::value>>
  NewSessionReplyMessage& ProcessPayloadSection(payload_t& section, ecies_keys_t& ecies_keys)
  {
    const exception::Exception ex{"EciesX25519", __func__};

    const auto& is_initiator = std::is_same<TRole, initiator_r>::value;

    auto& k = ecies_keys.k();
    auto& oth_k = ecies_keys.oth_k();
    auto& h = ecies_keys.h();
    crypto::Nonce n(0);

    // En/decrypt the key section based on role (Alice encrypts, Bob decrypts)
    auto& section_buf = section.buffer();
    aead_t::key_t pk;
    if (is_initiator)
      {
        // k = HKDF(temp_k2, ZEROLEN, "AttachPayloadKDF", 32)
        hkdf_t::Derive(pk, k, crypto::FixedSecBytes<0>{}, ECIES_ATTACH_PAYLOAD_CTX);

        section.serialize();
        aead_t::AEADEncrypt(pk, n, section_buf, h, section_buf);
      }
    else
      {
        // k = HKDF(temp_k2, ZEROLEN, "AttachPayloadKDF", 32)
        hkdf_t::Derive(pk, oth_k, crypto::FixedSecBytes<0>{}, ECIES_ATTACH_PAYLOAD_CTX);

        aead_t::AEADDecrypt(pk, n, section_buf, h, section_buf);
        section.deserialize();
      }

    return *this;
  }

  /// @brief Get the total size of the NewSessionMessage
  /// @detail Only accurate when created locally, or after being succefully decrypted + deserialized
  size_type size()
  {
    // calculate payload MAC lengths based on encrypted state
    return ecies::TagLen + elligator_t::PublicKeyLen + aead_t::MACLen + payload_.size()
           + (static_cast<std::uint8_t>(pay_state_ == MsgState::Encrypted) * tini2p::under_cast(aead_t::MACLen));
  }

  /// @brief Get the encrypted size of the NewSessionMessage
  /// @detail Only accurate when created locally, or after being succefully decrypted + deserialized
  size_type encrypted_len()
  {
    return ecies::TagLen + ep_section_.size() + aead_t::MACLen + payload_.encrypted_len();
  }

  /// @brief Get a const reference to the reply tag
  const tag_t& reply_tag() const noexcept
  {
    return reply_tag_;
  }

  /// @brief Set the reply tag
  NewSessionReplyMessage& reply_tag(tag_t tag)
  {
    const exception::Exception ex{"ECIES: NewSessionReplyMessage", __func__};

    check_reply_tag(tag, ex);

    reply_tag_ = std::forward<tag_t>(tag);

    return *this;
  }

  /// @brief Get a const reference to the ephemeral key section
  const ephemeral_section_t& ephemeral_section() const noexcept
  {
    return ep_section_;
  }

  /// @brief Get a non-const reference to the ephemeral key section
  ephemeral_section_t& ephemeral_section() noexcept
  {
    return ep_section_;
  }

  /// @brief Get a const reference to the payload section
  const payload_t& payload_section() const noexcept
  {
    return payload_;
  }

  /// @brief Get a non-const reference to the payload section
  payload_t& payload_section() noexcept
  {
    return payload_;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another NewSessionReplyMessage
  std::uint8_t operator==(const NewSessionReplyMessage& oth)
  {  // attempt constant-time comparison
    const auto& tag_eq = static_cast<std::uint8_t>(reply_tag_ == oth.reply_tag_);
    const auto& ep_eq = static_cast<std::uint8_t>(ep_section_ == oth.ep_section_);
    const auto& pay_eq = static_cast<std::uint8_t>(payload_ == oth.payload_);
    const auto& pay_state_eq = static_cast<std::uint8_t>(pay_state_ == oth.pay_state_);

    return (tag_eq * ep_eq * pay_eq * pay_state_eq);
  }

 private:
  void deserialize_header(tini2p::BytesReader<buffer_t>& reader, const exception::Exception& ex)
  {
    tag_t tag;
    reader.read_data(tag);

    check_reply_tag(tag, ex);

    ephemeral_section_t ep_section;
    reader.read_data(ep_section.buffer());
    ep_section.deserialize();

    reply_tag_ = std::move(tag);
    ep_section_ = std::move(ep_section);
  }

  void check_buffer(const buffer_t& buf, const exception::Exception& ex) const
  {
    if (buf.is_zero())
      ex.throw_ex<std::invalid_argument>("null message buffer");

    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);
    const auto& buf_len = buf.size();

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  void check_reply_tag(const tag_t& tag, const exception::Exception& ex) const
  {
    if (tag.buffer().is_zero())
      ex.throw_ex<std::logic_error>("null reply tag");
  }

  void check_chain_key(const key_info_t::chain_key_t& chain_key, const exception::Exception& ex) const
  {
    if (chain_key.is_zero())
      ex.throw_ex<std::logic_error>("null chain key");
  }

  void check_h(const key_info_t::h_t& h, const exception::Exception& ex) const
  {
    if (h.is_zero())
      ex.throw_ex<std::invalid_argument>("null h");
  }

  tag_t reply_tag_;
  elligator_t::keypair_t ep_keys_;
  ephemeral_section_t ep_section_;
  payload_t payload_;
  MsgState pay_state_;
  buffer_t buf_;
};
}  // namespace ecies
}  // namespace tini2p

#endif  // SRC_ECIES_NEW_SESSION_REPLY_H_
