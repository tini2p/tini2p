/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_ECIES_SESSION_MANAGER_H_
#define SRC_ECIES_SESSION_MANAGER_H_

#include <deque>
#include <mutex>
#include <optional>
#include <shared_mutex>

#include "src/crypto/bloom_filter.h"

#include "src/data/i2np/database_store.h"
#include "src/data/i2np/garlic.h"
#include "src/data/i2np/i2np_message.h"

#include "src/data/router/destination.h"

#include "src/ecies/x25519.h"

namespace tini2p
{
namespace ecies
{
/// @class SessionManager
/// @brief Manages inbound and outbound ECIES sessions
/// @detail Local state is kept in its initial form for creating new
///     outbound and inbound session states that ratchet independently.
///     Allows for simpler processing of NewSessionMessages (outbound + inbound).
class SessionManager
{
 public:
  enum
  {
    OutboundSessionTimeout = 10,  //< in minutes, paired to LeaseSet2 expiration
    InboundIdleTimeout = 2,  //< in minutes, somewhat arbitrary, kill session after expiry
    AggressiveIdleTimeout = 30,  //< in seconds, somewhat arbitrary, kill session after expiry
    BloomTimeout = 120,  //< in minutes, clear the Bloom filter afterwards, and generate new static keypair
    ExpectedEphemeralKeys = 15360,  //< estimated number of sessions in the clear interval
    EphemeralFalsePositive = 1024,  //< one-in-P number of allowed false-positive Bloom filter checks
  };

  using destination_t = data::Destination;  //< Destination trait alias
  using curve_t = crypto::X25519;  //< Curve trait alias
  using elligator_t = crypto::Elligator2;  //< Elligator2 trait alias
  using ecies_x25519_t = ecies::EciesX25519;  //< ECIES-X25519-AEAD-Ratchet trait alias
  using expiring_inbound_t = std::deque<ecies_x25519_t>;  //< Expiring inbound session trait alias
  using sessions_t = std::deque<ecies_x25519_t>;  //< ECIES sessions collection trait alias
  using session_id_t = std::uint64_t;  //< SessionID trait alias
  using new_message_t = ecies::NewSessionMessage;  //< New session message trait alias
  using new_reply_t = ecies::NewSessionReplyMessage;  //< New session reply message trait alias
  using existing_message_t = ecies::ExistingSessionMessage;  //< Existing session message trait alias
  using message_v = std::variant<existing_message_t, new_message_t, new_reply_t>;  //< Message variant trait alias
  using tag_t = ecies::Tag;  //< ECIES session tag trait alias
  using key_info_t = ecies::SectionKeyInfo;  //< ECIES KeyInfo trait alias
  using i2np_block_t = data::I2NPBlock;  //< I2NP block trait alias
  using garlic_t = data::Garlic;  //< Garlic trait alias
  using database_store_t = data::DatabaseStore;  //< DatabaseStore message trait alias
  using bloom_filter_t = crypto::BloomFilter;  //< Bloom filter trait alias

  /// @brief Default ctor, creates fresh sets of local keys, and empty sessions collections
  SessionManager()
      : ecies_(),
        inbound_(),
        pending_inbound_(),
        outbound_(),
        pending_outbound_(),
        bloom_(ExpectedEphemeralKeys, EphemeralFalsePositive)
  {
  }

  /// @brief Handle an incoming I2NP message
  /// @detail Depending on the type of Garlic message, a different ECIES session message variant will be returned
  ///     For incoming ExistingSession messages:
  ///       - the decrypted ExistingSession message
  ///     For incoming NewSession messages:
  ///       - the decrypted NewSession message
  ///     For incoming NewSessionReply messages:
  ///       - the decrypted NewSessionReply message
  /// @param message I2NP block containing an inbound Garlic message
  /// @return Collection of message variants: ExistingSession, NewSession, NewSessionReply
  message_v HandleI2NPMessage(data::I2NPMessage& message)
  {
    const exception::Exception ex{"ECIES: SessionManager", __func__};

    if (message.type() != data::I2NPHeader::Type::Garlic)
      ex.throw_ex<std::invalid_argument>("I2NP message is not a Garlic message.");

    message.mode(data::I2NPHeader::Mode::Garlic).deserialize();

    auto garlic_buf = message.message_buffer();
    tini2p::BytesReader<garlic_t::buffer_t> reader(garlic_buf);

    tag_t tag;
    reader.read_data(tag);

    std::uint8_t has_inbound(0), has_pending_inbound(0), has_pending_outbound(0);
    {  // read-lock inbound, pending inbound, and pending outbound sessions
      std::shared_lock igd(inbound_mutex_, std::defer_lock);
      std::shared_lock pid(pending_inbound_mutex_, std::defer_lock), pod(pending_outbound_mutex_, std::defer_lock);

      std::scoped_lock sgd(igd, pid, pod);

      has_inbound = has_tag(inbound_, tag);
      has_pending_inbound = has_tag(pending_inbound_, tag);
      has_pending_outbound = has_tag(pending_outbound_, tag);
    }  // end-sessions-read-lock

    message_v ret_msg;
    if (has_inbound || has_pending_inbound)
      {
        existing_message_t message(std::move(garlic_buf));
        HandleInboundExistingMessage(message);

        ret_msg.emplace<existing_message_t>(std::move(message));
      }
    else if (has_pending_outbound)
      {
        new_reply_t message(std::move(garlic_buf));
        HandleNewSessionReply(message);

        ret_msg.emplace<new_reply_t>(std::move(message));
      }
    else
      {
        ecies_x25519_t::decryption_status_t status;
        ecies_x25519_t::session_mode_t mode;

        new_message_t message(std::move(garlic_buf));

        std::tie(status, mode) = TrialDecrypt(message);

        if (status == ecies_x25519_t::decryption_status_t::Failed)
          ex.throw_ex<std::runtime_error>("error decrypting NewSession message");
        
        ret_msg.emplace<new_message_t>(std::move(message));
      }

    return ret_msg;
  }

  /// @brief Search established inbound sessions for a given tag
  /// @detail If tag is not found in established sessions, caller responsible for checking pending inbound sessions
  /// @param tag Session tag to search for
  /// @return Found status of the session tag
  decltype(auto) FindInboundTag(const tag_t& tag)
  {
    std::shared_lock is(inbound_mutex_, std::defer_lock);
    std::scoped_lock sgd(is);

    return static_cast<ecies_x25519_t::tag_status_t>(has_tag(inbound_, tag));
  }

  /// @brief Search pending inbound sessions for a given tag
  /// @detail If tag is not found, caller responsible for trial decrypting the message as a NewSessionMessage
  /// @param tag Session tag to search for
  /// @return Found status of the session tag
  decltype(auto) FindPendingInboundTag(const tag_t& tag)
  {
    std::shared_lock is(pending_inbound_mutex_, std::defer_lock);
    std::scoped_lock sgd(is);

    return static_cast<ecies_x25519_t::tag_status_t>(has_tag(pending_inbound_, tag));
  }

  /// @brief Search pending outbound sessions for a given tag
  /// @detail If tag is not found, caller responsible for trial decrypting the message as a NewSessionMessage
  /// @param tag Session tag to search for
  /// @return Found status of the session tag
  decltype(auto) FindPendingOutboundTag(const tag_t& tag)
  {
    std::shared_lock os(pending_outbound_mutex_, std::defer_lock);
    std::scoped_lock sgd(os);

    return static_cast<ecies_x25519_t::tag_status_t>(has_tag(pending_outbound_, tag));
  }

  /// @brief Trial-decrypt an incoming NewSessionMessage
  /// @detail Caller is responsible for:
  ///       - Checking whether the return decryption status flag was successful
  ///       - Checking whether the returned optional pointer to a NewSessionReply message is null.
  ///       - Creating additional NewSessionReply messages if needed, and decryption was successful
  /// @param new_message NewSessionMessage received on an incoming tunnel
  /// @param ecies ECIES instance to contain local keys, and store decrypted keys from the NewSessionMessage
  /// @return Optional pointer to an ECIES instance, std::nullopt for:
  ///     - decryption failure
  ///     - pending inbound session exists for the remote Destination
  std::pair<ecies_x25519_t::decryption_status_t, ecies_x25519_t::session_mode_t> TrialDecrypt(new_message_t& new_message)
  {
    const exception::Exception ex{"ECIES: SessionManager", __func__};

    check_bloom(new_message.ephemeral_section().key(), ex);

    std::optional<ecies_x25519_t> ecies(ecies_);

    const auto decrypt_status = ecies->Decrypt(new_message);

    if (decrypt_status == ecies_x25519_t::decryption_status_t::Failed)
      ecies.reset();

    ecies_x25519_t::session_mode_t session_mode(ecies_x25519_t::session_mode_t::OneTime);

    std::scoped_lock sgd(pending_inbound_mutex_);
    if (ecies != std::nullopt && !has_key(pending_inbound_, ecies->remote_id_key()))
      {
        if (new_message.is_bound())
          {
            ecies->ReplyTagRatchet<ecies_x25519_t::responder_r>();  // Create reply tag window

            pending_inbound_.emplace_back(std::move(*ecies));

            session_mode = ecies_x25519_t::SessionMode::Bound;
          }
        else if (new_message.is_unbound())
          {
            std::scoped_lock igd(inbound_mutex_);
            inbound_.emplace_back(std::move(*ecies));
            session_mode = ecies_x25519_t::SessionMode::Unbound;
          }
        else
          session_mode = ecies_x25519_t::SessionMode::OneTime;
      }

    return std::make_pair(std::move(decrypt_status), std::move(session_mode));
  }

  /// @brief Create NewSessionReply message to send to the remote Destination
  /// @detail Used to send multiple payloads when the response to a NewSession message
  ///     is too large for one NewSessionReply message
  new_reply_t CreateNewSessionReply(const curve_t::pubkey_t& id_key, new_reply_t::payload_t payload)
  {
    const exception::Exception ex{"ECIES: SessionManager", __func__};

    std::scoped_lock sgd(pending_inbound_mutex_, pending_outbound_mutex_);

    return CreateNewSessionReply(id_key, std::forward<new_reply_t::payload_t>(payload), ex);
  }

  /// @brief Handle encrypted NewSessionReply message for a pending outbound session
  /// @detail NewSessionReply messages will only be received for pending outbound sessions
  ///     Pending inbound sessions are acknowledged by an ExistingSession message
  /// @param new_reply Encrypted NewSessionReply message
  SessionManager& HandleNewSessionReply(new_reply_t& new_reply)
  {
    const exception::Exception ex{"ECIES: SessionManager", __func__};

    check_bloom(new_reply.ephemeral_section().key(), ex);

    const auto& tag = new_reply.reply_tag();

    std::scoped_lock sgd(pending_outbound_mutex_);
    
    auto it = find_session(pending_outbound_, tag, ex);

    it->Decrypt(new_reply);

    const auto& id_key = it->remote_id_key();

    std::scoped_lock igd(inbound_mutex_, outbound_mutex_, pending_inbound_mutex_);

    // add a session to established inbound and outbound sessions
    // only add a new session for the first NewSessionReply message received
    if (!has_key(outbound_, id_key))
      {
        if (has_key(inbound_, id_key))
          inbound_.erase(find_session(inbound_, id_key, ex));

        // create an inbound session
        auto in_ecies = *it;
        // set inbound message key to temp_k2
        in_ecies.k(in_ecies.oth_k());

        // clear the other cipherstate k, no longer needed
        in_ecies.clear_oth_k();
        it->clear_oth_k();

        // DH ratchet, and create inbound session tags + symmetric keys
        in_ecies.DHRatchet<ecies_x25519_t::responder_r>();
        in_ecies.CheckRatchets();

        // DH ratchet, and create outbound session tags + symmetric keys
        it->DHRatchet<ecies_x25519_t::initiator_r>();
        it->CheckRatchets();

        inbound_.emplace_back(std::move(in_ecies));
        outbound_.emplace_back(std::move(*it));

        pending_outbound_.erase(it);
      }

    // remove any other pending inbound session(s) for the remote Destination
    remove_pending_sessions(pending_inbound_, id_key);

    return *this;
  }

  /// @brief Decrypt an inbound existing session message from the given Destination
  /// @param dest_hash Destination hash of the remote router
  /// @param existing_message Existing session message from the remote Destination
  SessionManager& HandleInboundExistingMessage(existing_message_t& existing_message)
  {
    const exception::Exception ex{"ECIES: SessionManager", __func__};

    std::scoped_lock igd(inbound_mutex_, pending_inbound_mutex_);

    const auto& tag = existing_message.tag();
    const auto is_pending = has_tag(pending_inbound_, tag);

    sessions_t::iterator it;
    if (is_pending)
      it = find_session(pending_inbound_, tag, ex);
    else
      it = find_session(inbound_, tag, ex);

    it->Decrypt(existing_message);

    if (is_pending)
      {
        const auto id_key = it->remote_id_key();
        if (has_key(inbound_, id_key))
          inbound_.erase(find_session(inbound_, id_key, ex));

        std::scoped_lock pgd(outbound_mutex_, pending_outbound_mutex_);
        if (has_key(outbound_, id_key))
          outbound_.erase(find_session(outbound_, id_key, ex));

        auto out_ecies = find_session(pending_outbound_, it->session_id(), ex);

        inbound_.emplace_back(std::move(*it));
        outbound_.emplace_back(std::move(*out_ecies));

        remove_pending_sessions(pending_inbound_, id_key);
        remove_pending_sessions(pending_outbound_, id_key);
      }

    // check for a "Next DH Key" block
    if (existing_message.has_block<data::NewDHKeyBlock>())
      {  // rekey the with the new remote ephemeral key, and perform a DH ratchet
        const auto& remote_key = existing_message.get_block<data::NewDHKeyBlock>().ratchet_key();

        it->DHRatchet<ecies_x25519_t::responder_r>(remote_key);

        const auto& id_key = it->remote_id_key();

        std::scoped_lock ogd(outbound_mutex_);
        if (has_key(outbound_, id_key))
          {
            auto out_it = find_session(outbound_, id_key, ex);
            out_it->DHRatchet<ecies_x25519_t::initiator_r>(remote_key);
          }
      }

    return *this;
  }

  /// @brief Create a new outbound session to the given Destination
  /// @detail Caller should search for an existing outbound session to the Destination before attempting
  ///     to create a new one.
  ///     If there is an existing outbound session to the Destination, it is replaced by the new session.
  ///     Caller is responsible for sending the encrypted NewSession message through on outbound tunnel.
  /// @param remote_id_key Remote static public key of the Destination, used to encrypt the NewSession message
  /// @param dest_hash Destination hash of the remote router
  /// @param payload Payload section for creating a NewSession message
  /// @return Encrypted NewSession message
  new_message_t CreateOutboundSession(curve_t::pubkey_t remote_id_key, new_message_t::payload_t payload)
  {
    const exception::Exception ex{"ECIES: SessionManager", __func__};

    if (remote_id_key.is_zero())
      ex.throw_ex<std::invalid_argument>("null remote static key.");

    ecies_x25519_t new_ecies(ecies_.id_keys());
    new_ecies.remote_id_key(std::forward<ecies_x25519_t::curve_t::pubkey_t>(remote_id_key));

    // create a bound NewSession message
    // TODO(tini2p): create function for creating one-time/unbound sessions
    ecies_x25519_t::new_message_t new_message(
        new_ecies.id_keys().pubkey, std::forward<ecies_x25519_t::new_message_t::payload_t>(payload));

    new_ecies.Encrypt(new_message);

    new_ecies.ReplyTagRatchet<ecies_x25519_t::initiator_r>();

    std::scoped_lock ogd(pending_outbound_mutex_);
    pending_outbound_.emplace_back(std::move(new_ecies));

    return new_message;
  }

  /// @brief Create a new outbound session to the given Destination
  /// @detail Caller should search for an existing outbound session to the Destination before attempting
  ///     to create a new one.
  ///     If there is an existing outbound session to the Destination, it is replaced by the new session.
  ///     Caller is responsible for sending the encrypted NewSession message through on outbound tunnel.
  /// @param remote_id_key Remote static public key of the Destination, used to encrypt the NewSession message
  /// @param dest_hash Destination hash of the remote router
  /// @param payload Payload section for creating a NewSession message
  /// @return Encrypted NewSession message
  new_message_t CreateOutboundSession(
      const data::Destination& local_dest,
      curve_t::pubkey_t remote_id_key,
      new_message_t::payload_t payload)
  {
    const exception::Exception ex{"ECIES: SessionManager", __func__};

    if (remote_id_key.is_zero())
      ex.throw_ex<std::invalid_argument>("null remote static key.");

    ecies_x25519_t new_ecies(local_dest.crypto());
    new_ecies.remote_id_key(std::forward<ecies_x25519_t::curve_t::pubkey_t>(remote_id_key));

    // create a bound NewSession message
    // TODO(tini2p): create function for creating one-time/unbound sessions
    ecies_x25519_t::new_message_t new_message(
        new_ecies.id_keys().pubkey, std::forward<ecies_x25519_t::new_message_t::payload_t>(payload));

    new_ecies.Encrypt(new_message);

    new_ecies.ReplyTagRatchet<ecies_x25519_t::initiator_r>();

    std::scoped_lock ogd(pending_outbound_mutex_);
    pending_outbound_.emplace_back(std::move(new_ecies));

    return new_message;
  }

  /// @brief Encrypt an outbound existing session message to the given Destination
  /// @param dest_hash Destination hash of the remote router
  /// @param existing_message Existing session message to the remote Destination
  SessionManager& HandleOutboundExistingMessage(
      const curve_t::pubkey_t& id_key,
      ecies_x25519_t::existing_message_t& existing_message)
  {
    const exception::Exception ex{"ECIES: SessionManager", __func__};

    std::scoped_lock ogd(outbound_mutex_);

    auto it = find_session(outbound_, id_key, ex);

    it->Encrypt(existing_message);

    // check for a "Next DH Key" block
    if (existing_message.has_block<data::NewDHKeyBlock>())
      {
        it->DHRatchet<ecies_x25519_t::initiator_r>(it->remote_ep_key());

        std::scoped_lock igd(inbound_mutex_);
        if (has_key(inbound_, id_key))
          {  // DH ratchet the corresponding inbound session CipherState
            auto in_it = find_session(inbound_, id_key, ex);
            in_it->ep_keys(it->ep_keys());
            in_it->DHRatchet<ecies_x25519_t::responder_r>(in_it->remote_ep_key());
          }
      }

    return *this;
  }

  /// @brief Encrypt an outbound existing session message to the given Destination
  /// @param dest_hash Destination hash of the remote router
  /// @param existing_message Existing session message to the remote Destination
  template <class TPayloadBlock>
  existing_message_t CreateOutboundExistingMessage(const curve_t::pubkey_t& id_key, TPayloadBlock payload)
  {
    const exception::Exception ex{"ECIES: SessionManager", __func__};

    std::scoped_lock ogd(outbound_mutex_);

    auto it = find_session(outbound_, id_key, ex);

    existing_message_t existing_message(std::forward<TPayloadBlock>(payload));

    it->Encrypt(existing_message);

    // check for a "Next DH Key" block
    if (existing_message.has_block<data::NewDHKeyBlock>())
      {
        it->DHRatchet<ecies_x25519_t::initiator_r>(it->remote_ep_key());

        std::scoped_lock igd(inbound_mutex_);
        if (has_key(inbound_, id_key))
          {  // DH ratchet the corresponding inbound session CipherState
            auto in_it = find_session(inbound_, id_key, ex);
            in_it->ep_keys(it->ep_keys());
            in_it->DHRatchet<ecies_x25519_t::responder_r>(in_it->remote_ep_key());
          }
      }

    return existing_message;
  }

  /// @brief Get whether there is a pending inbound session the given key
  /// @param key Static public key for the remote Destination
  std::uint8_t has_pending_inbound_session(const curve_t::pubkey_t& key)
  {
    std::shared_lock pi(pending_inbound_mutex_, std::defer_lock);
    std::scoped_lock sgd(pi);

    return has_key(pending_inbound_, key);
  }

  /// @brief Get whether there is an established inbound session the given key
  /// @param key Static public key for the remote Destination
  std::uint8_t has_inbound_session(const curve_t::pubkey_t& key)
  {
    std::shared_lock ib(inbound_mutex_, std::defer_lock);
    std::scoped_lock sgd(ib);

    return has_key(inbound_, key);
  }

  /// @brief Get whether there is an established inbound session the given key
  /// @param key Static public key for the remote Destination
  std::uint8_t has_pending_outbound_session(const curve_t::pubkey_t& key)
  {
    std::shared_lock po(pending_outbound_mutex_, std::defer_lock);
    std::scoped_lock sgd(po);

    return has_key(pending_outbound_, key);
  }

  /// @brief Get whether there is an established outbound session the given key
  /// @param key Static public key for the remote Destination
  std::uint8_t has_outbound_session(const curve_t::pubkey_t& key)
  {
    std::shared_lock ob(outbound_mutex_, std::defer_lock);
    std::scoped_lock sgd(ob);

    return has_key(outbound_, key);
  }

  /// @brief Get a const reference to the base ECIES instance
  const ecies_x25519_t& ecies() const noexcept
  {
    return ecies_;
  }

  /// @brief Get a non-const reference to the base ECIES instance
  ecies_x25519_t& ecies() noexcept
  {
    return ecies_;
  }

  const SessionManager& display_keys() const
  {
    const auto& loc_id = ecies_.id_keys();
    const auto& loc_ep = ecies_.ep_keys();
    const auto& remote_id = ecies_.remote_id_key();
    const auto& remote_ep = ecies_.remote_ep_key();

    std::cout << "Symmetric keys: " << std::endl;
    std::cout << "\tchain key: " << crypto::Base64::Encode(ecies_.chain_key()) << std::endl;
    std::cout << "\thandshake hash: " << crypto::Base64::Encode(ecies_.h()) << std::endl;
    std::cout << "*----*----*----*" << std::endl;
    std::cout << "Cipherstate key: " << std::endl;
    std::cout << "\tpoly key: " << crypto::Base64::Encode(ecies_.k()) << std::endl;
    std::cout << "*----*----*----*" << std::endl;
    std::cout << "Local static keys: " << std::endl;
    std::cout << "\tlocal static private key: " << crypto::Base64::Encode(loc_id.pvtkey) << std::endl;
    std::cout << "\tlocal static public key: " << crypto::Base64::Encode(loc_id.pubkey) << std::endl;
    std::cout << "*----*----*----*" << std::endl;
    std::cout << "Local ephemeral keys: " << std::endl;
    std::cout << "\tlocal ephemeral private key: " << crypto::Base64::Encode(loc_ep.pvtkey) << std::endl;
    std::cout << "\tlocal ephemeral public key: " << crypto::Base64::Encode(loc_ep.pubkey) << std::endl;
    std::cout << "*----*----*----*" << std::endl;
    std::cout << "Remote public keys: " << std::endl;
    std::cout << "\tremote static public key: " << crypto::Base64::Encode(remote_id) << std::endl;
    std::cout << "\tremote ephemeral public key: " << crypto::Base64::Encode(remote_ep) << std::endl;
    std::cout << "*----*----*----*" << std::endl;

    return *this;
  }

 private:
  new_reply_t
  CreateNewSessionReply(const curve_t::pubkey_t& id_key, new_reply_t::payload_t payload, const exception::Exception& ex)
  {
    auto it = find_session(pending_inbound_, id_key, ex);
    auto reply_tag = it->tag();
    it->remove_used_tag(reply_tag);

    if (it->empty_tags())
      it->ReplyTagRatchet<ecies_x25519_t::responder_r>();

    new_reply_t new_reply(std::move(reply_tag), std::forward<new_reply_t::payload_t>(payload));
    ecies_x25519_t out_ecies = *it;
    out_ecies.Encrypt(new_reply);
    out_ecies.create_session_id();

    out_ecies.DHRatchet<ecies_x25519_t::initiator_r>();

    ecies_x25519_t in_ecies = out_ecies;

    in_ecies.session_id(out_ecies.session_id());

    in_ecies.k(out_ecies.oth_k());
    out_ecies.clear_oth_k();
    in_ecies.clear_oth_k();

    in_ecies.DHRatchet<ecies_x25519_t::responder_r>();

    pending_inbound_.emplace_back(std::move(in_ecies));
    pending_outbound_.emplace_back(std::move(out_ecies));

    return new_reply;
  }

  sessions_t::iterator find_session(sessions_t& sessions, const curve_t::pubkey_t& id_key, const exception::Exception& ex)
  {
    const auto sess_end = sessions.end();

    auto it =
        std::find_if(sessions.begin(), sess_end, [&id_key](const auto& s) { return s.remote_id_key() == id_key; });

    if (it == sess_end)
      ex.throw_ex<std::logic_error>("no session found for static key: " + tini2p::bin_to_hex(id_key, ex));

    return it;
  }

  sessions_t::iterator find_session(sessions_t& sessions, const tag_t& tag, const exception::Exception& ex)
  {
    const auto sess_end = sessions.end();

    auto it = std::find_if(sessions.begin(), sess_end, [&tag](const auto& s) {
      return s.find_tag(tag) == ecies_x25519_t::tag_status_t::Found;
    });

    if (it == sess_end)
      ex.throw_ex<std::logic_error>("no session found with the tag: " + tini2p::bin_to_hex(tag, ex));

    return it;
  }

  sessions_t::iterator find_session(sessions_t& sessions, const session_id_t& id, const exception::Exception& ex)
  {
    const auto sess_end = sessions.end();

    auto it = std::find_if(sessions.begin(), sess_end, [&id](const auto& s) { return s.session_id() == id; });

    if (it == sess_end)
      ex.throw_ex<std::logic_error>("no session found with session ID: " + std::to_string(id));

    return it;
  }

  std::uint8_t has_tag(const sessions_t& sessions, const tag_t& tag) const
  {
    const auto sess_end = sessions.end();

    return static_cast<std::uint8_t>(
        std::find_if(
            sessions.begin(),
            sess_end,
            [&tag](const auto& s) { return s.find_tag(tag) == ecies_x25519_t::tag_status_t::Found; })
        != sess_end);
  }

  std::uint8_t has_key(const sessions_t& sessions, const curve_t::pubkey_t& id_key) const
  {
    const auto sess_end = sessions.end();

    return static_cast<std::uint8_t>(
        std::find_if(sessions.begin(), sess_end, [&id_key](const auto& s) { return s.remote_id_key() == id_key; })
        != sess_end);
  }

  void remove_pending_sessions(sessions_t& sessions, const curve_t::pubkey_t& id_key)
  {
    for (auto it = sessions.begin(); it != sessions.end();)
      {
        if (it->remote_id_key() == id_key)
          it = sessions.erase(it);
        else
          ++it;
      }
  }

  void check_bloom(const elligator_t::pubkey_t& key, const exception::Exception& ex)
  {
    if (bloom_.CheckElement(key.buffer()) == 1)
      ex.throw_ex<std::logic_error>("ephemeral keys must be unique, prevents derived key re-use and replay attacks.");
  }

  ecies_x25519_t ecies_;

  sessions_t inbound_;
  std::shared_mutex inbound_mutex_;

  sessions_t pending_inbound_;
  std::shared_mutex pending_inbound_mutex_;

  sessions_t outbound_;
  std::shared_mutex outbound_mutex_;

  sessions_t pending_outbound_;
  std::shared_mutex pending_outbound_mutex_;

  bloom_filter_t bloom_;
};
}  // namespace ecies
}  // namespace tini2p

#endif  // SRC_ECIES_SESSION_MANAGER_H_
