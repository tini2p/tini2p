/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_GZIP_H_
#define SRC_GZIP_H_

#include <zlib.h>

#include "src/bytes.h"

namespace tini2p
{
/// @class Gzip
/// @brief Wrappers for zlib compression functions
class Gzip
{
  enum struct Mode
  {
    Compress,
    Decompress,
  };

  static void init(z_stream& z_s, const Mode mode, const exception::Exception& ex)
  {
    z_s.zalloc = Z_NULL;
    z_s.zfree = Z_NULL;
    z_s.opaque = Z_NULL;
    z_s.avail_in = 0;
    z_s.next_in = Z_NULL;

    if (mode == Mode::Compress)
      {
        if (deflateInit(&z_s, Z_FINISH) != Z_OK)
          ex.throw_ex<std::runtime_error>(z_s.msg);
      }
    else
      {
        if (inflateInit(&z_s) != Z_OK)
          ex.throw_ex<std::runtime_error>(z_s.msg);
      }
  }

 public:
  enum
  {
    MinLen = 1,
    MaxLen = 655535,
  };

  /// @brief Gzip compress a given buffer
  /// @tparam TOut Output buffer type
  /// @param data Pointer to the input buffer
  /// @param size Size of the input buffer
  /// @param out Output buffer
  template <class TOut>
  static void Compress(const std::uint8_t* data, const std::size_t size, TOut& out)
  {
    const exception::Exception ex{"Gzip", __func__};

    tini2p::check_cbuf(data, size, MinLen, MaxLen, ex);

    // initialize for compression
    z_stream z_s;
    init(z_s, Mode::Compress, ex); 

    z_s.next_in = const_cast<std::uint8_t*>(data);  // unchanged, needed for zlib
    z_s.avail_in = size;

    // get upper compression bound to compress in one-pass
    const auto deflate_len = deflateBound(&z_s, size);
    out.resize(deflate_len);

    z_s.next_out = out.data();
    z_s.avail_out = deflate_len;

    if (deflate(&z_s, Z_FINISH) != Z_STREAM_END)
      ex.throw_ex<std::runtime_error>(
          "unable to compress stream" + (z_s.msg ? std::string(": ") + z_s.msg : std::string(".")));

    // shrink to fit actual compressed size
    out.resize(z_s.total_out);
    out.shrink_to_fit();

    deflateEnd(&z_s);  // clean up
  }

  /// @brief Gzip decompress a given buffer
  /// @tparam TOut Output buffer type
  /// @param data Pointer to the input buffer
  /// @param size Size of the input buffer
  /// @param out Output buffer
  template <class TOut>
  static void Decompress(const std::uint8_t* in, const std::size_t size, TOut& out)
  {
    const exception::Exception ex{"Gzip", __func__};

    tini2p::check_cbuf(in, size, MinLen, MaxLen, ex);

    // initialize for decompression
    z_stream z_s;
    init(z_s, Mode::Decompress, ex); 

    z_s.next_in = const_cast<std::uint8_t*>(in);  // unchanged, needed for zlib
    z_s.avail_in = size;
    
    // set to max len to avoid overrun
    out.resize(MaxLen);

    z_s.next_out = reinterpret_cast<std::uint8_t*>(out.data());
    z_s.avail_out = out.size();

    // attempt to decompress in one shot
    if (inflate(&z_s, Z_FINISH) != Z_STREAM_END)
      ex.throw_ex<std::runtime_error>(
          "unable to decompress stream" + (z_s.msg ? std::string(": ") + z_s.msg : std::string(".")));

    out.resize(z_s.total_out);
    out.shrink_to_fit();

    inflateEnd(&z_s);
  }
};
}  // namespace tini2p

#endif  // SRC_GZIP_H_
