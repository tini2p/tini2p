/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_ROUTER_DESTINATION_H_
#define SRC_DATA_ROUTER_DESTINATION_H_

#include <variant>

#include "src/crypto/aes.h"
#include "src/crypto/keys.h"

#include "src/data/router/meta.h"
#include "src/data/router/certificate.h"

namespace tini2p
{
namespace data
{
/// @class Destination
/// @brief Class for I2P Destination
class Destination
{
 public:
  using cert_t = Certificate;  //< Certificate trait alias
  using hash_t = crypto::Sha256::digest_t;  //< Hash trait alias
  using padding_t = crypto::SecBytes;  //< Padding trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  using crypto_t = crypto::X25519;  //< Crypto keys trait alias

  using eddsa_t = crypto::EdDSASha512;  //< EdDSA-SHA512 trait alias
  using reddsa_t = crypto::RedDSASha512;  //< RedDSA-SHA512 trait alias
  using xeddsa_t = crypto::XEdDSASha512;  //< XEdDSA-SHA512 trait alias
  using signing_v = std::variant<eddsa_t, reddsa_t, xeddsa_t>;  //< Signing variant trait alias
  using blind_signing_v = std::variant<reddsa_t, xeddsa_t>;  //< Blind signing variant trait alias

  /// @alias signature_v
  /// @brief Signature variant trait alias
  using signature_v = std::variant<eddsa_t::signature_t, reddsa_t::signature_t, xeddsa_t::signature_t>;

  /// @alias sign_pubkey_v
  /// @brief Signing pubkey variant trait alias
  using sign_pubkey_v = std::variant<eddsa_t::pubkey_t, reddsa_t::pubkey_t, xeddsa_t::pubkey_t>;

  /// @alias blind_signature_v
  /// @brief Blind signature variant trait alias
  using blind_signature_v = std::variant<reddsa_t::signature_t, xeddsa_t::signature_t>;

  /// @alias blind_pubkey_v
  /// @brief Blinded signing pubkey trait alias
  using blind_pubkey_v = std::variant<reddsa_t::pubkey_t, xeddsa_t::pubkey_t>;

  enum Sizes : std::uint16_t
  {
    KeysPaddingLen = 384,  //< Size of keys + padding, see spec
    MinLen = KeysPaddingLen + cert_t::HeaderLen,
    DefaultLen = MinLen + cert_t::KeyCertLen,  // Ed25519 key cert
    HashLen = 32,
    MaxLen = DefaultLen,
  };

  enum Offsets : std::uint16_t
  {
    CertOffset = KeysPaddingLen,
    CertSizeOffset = CertOffset + cert_t::CertTypeLen,
  };

  /// @brief Default ctor, creates an Destination with EciesX25519 e2e crypto, and EdDSA signing
  Destination()
      : cert_(cert_t::cert_type_t::KeyCert),
        crypto_keys_(),
        signing_(),
        blind_signing_(),
        hash_(),
        padding_(),
        buf_(DefaultLen)
  {
    rekey<eddsa_t>(crypto_t::create_keys(), eddsa_t::create_keys());

    serialize();
  }

  /// @brief Create a Destination from an end-to-end keypair
  /// @detail Creates a random EdDSASha512 signing keypair
  explicit Destination(crypto_t::keypair_t crypto_keys)
      : cert_(typeid(eddsa_t), typeid(crypto_t)),
        crypto_keys_(),
        signing_(),
        blind_signing_(),
        hash_(),
        padding_(),
        buf_(DefaultLen)
  {
    rekey<eddsa_t>(std::forward<crypto_t::keypair_t>(crypto_keys), eddsa_t::create_keys());

    serialize();
  }

  /// @brief Create an Destination with given crypto + signing implementations
  /// @tparam TSigning Signing implementation type
  /// @param t_crypto Crytpo implementation
  /// @param signing Signing implementation
  template <
      class TSigning,
      typename = std::enable_if_t<
          std::is_same<TSigning, eddsa_t>::value || std::is_same<TSigning, reddsa_t>::value
          || std::is_same<TSigning, xeddsa_t>::value>>
  Destination(crypto_t::keypair_t crypto_keys, TSigning signing)
      : cert_(typeid(TSigning), typeid(crypto_t)),
        crypto_keys_(std::forward<crypto_t::keypair_t>(crypto_keys)),
        signing_(std::forward<TSigning>(signing)),
        blind_signing_(),
        hash_(),
        padding_(),
        buf_(DefaultLen)
  {
    const exception::Exception ex{"Router: Destination", __func__};

    if (crypto_keys_.pvtkey.is_zero())
      ex.throw_ex<std::invalid_argument>("null crypto private key.");

    if (crypto_keys_.pubkey.is_zero())
      crypto_t::PrivateToPublic(crypto_keys_);

    update_padding();

    serialize();
  }

  /// @brief Converting ctor for deserializing from a buffer
  /// @param buffer Buffer containing raw router identity
  explicit Destination(buffer_t buf)
      : cert_(cert_t::cert_type_t::KeyCert),
        crypto_keys_(),
        signing_(),
        blind_signing_(),
        hash_(),
        padding_(),
        buf_(std::forward<buffer_t>(buf))
  {
    const exception::Exception ex{"RouterDestination", __func__};

    if (buf_.size() < MinLen || buf_.size() > MaxLen)
      ex.throw_ex<std::length_error>("invalid identity size.");

    deserialize();
  }

  /// @brief Converting ctor for deserializing from a buffer
  /// @param buffer Buffer containing raw router identity
  Destination(buffer_t::const_iterator begin, buffer_t::const_iterator end)
      : cert_(cert_t::cert_type_t::KeyCert),
        crypto_keys_(),
        signing_(),
        blind_signing_(),
        hash_(),
        padding_(),
        buf_(begin, end)
  {
    const exception::Exception ex{"RouterDestination", __func__};

    const auto size = end - begin;
    if (size < MinLen || size > MaxLen)
      ex.throw_ex<std::length_error>("invalid identity size.");

    deserialize();
  }

  /// @brief Create an Destination from a buffer
  /// @param data Pointer to the buffer
  /// @param len Size of the buffer
  Destination(const std::uint8_t* data, const std::size_t len)
      : cert_(cert_t::cert_type_t::KeyCert), crypto_keys_(), signing_(), blind_signing_(), hash_(), padding_(), buf_()
  {
    const exception::Exception ex{"RouterDestination", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  /// @brief Sign a message
  /// @param msg_ptr Const pointer to the beginning of the message
  /// @param msg_len Length of the message
  /// @return Signature variant containing the message signature
  decltype(auto) Sign(const std::uint8_t* msg_ptr, const std::size_t msg_len) const
  {
    return std::visit(
        [msg_ptr, msg_len](const auto& val) {
          typename std::decay_t<decltype(val)>::signature_t sig;
          val.Sign(msg_ptr, msg_len, sig);
          return signature_v(std::move(sig));
        },
        signing_);
  }

  /// @brief Verify a signed a message
  /// @detail Signature variant must match the Destination's signing variant type
  /// @param msg_ptr Const pointer to the beginning of the message
  /// @param msg_len Length of the message
  /// @param sig Signature variant containing the message signature
  /// @return True if the signature is valid
  /// @throw Logic error on signature type mismatch
  std::uint8_t Verify(const std::uint8_t* msg_ptr, const std::size_t msg_len, const signature_v& sig) const
  {
    const exception::Exception ex{"Destination", __func__};

    return std::visit(
        [msg_ptr, msg_len, sig, ex](const auto& s) {
          using sig_t = typename std::decay_t<decltype(s)>::signature_t;

          if (!std::holds_alternative<sig_t>(sig))
            ex.throw_ex<std::logic_error>("invalid signature type.");

          return s.Verify(msg_ptr, msg_len, std::get<sig_t>(sig));
        },
        signing_);
  }

  /// @brief Blind-sign a message
  /// @param msg_ptr Const pointer to the beginning of the message
  /// @param msg_len Length of the message
  /// @return Signature variant containing the message signature
  decltype(auto) BlindSign(const std::uint8_t* msg_ptr, const std::size_t msg_len) const
  {
    return std::visit(
        [msg_ptr, msg_len](const auto& val) {
          typename std::decay_t<decltype(val)>::signature_t sig;
          val.Sign(msg_ptr, msg_len, sig);
          return blind_signature_v(std::move(sig));
        },
        blind_signing_);
  }

  /// @brief Verify a blind-signed a message
  /// @detail Signature variant must match the Destination's signing variant type
  /// @param msg_ptr Const pointer to the beginning of the message
  /// @param msg_len Length of the message
  /// @param sig Signature variant containing the message signature
  /// @return True if the signature is valid
  /// @throw Logic error on signature type mismatch
  bool BlindVerify(const std::uint8_t* msg_ptr, const std::size_t msg_len, const blind_signature_v& sig) const
  {
    const exception::Exception ex{"Destination", __func__};

    return std::visit(
        [msg_ptr, msg_len, sig, ex](const auto& s) {
          using sig_t = typename std::decay_t<decltype(s)>::signature_t;

          if (!std::holds_alternative<sig_t>(sig))
            ex.throw_ex<std::logic_error>("invalid signature type.");

          return s.Verify(msg_ptr, msg_len, std::get<sig_t>(sig));
        },
        blind_signing_);
  }

  /// @brief Serialize the router identity to buffer
  Destination& serialize()
  {
    buf_.resize(size());
    BytesWriter<buffer_t> writer(buf_);

    writer.write_data(crypto_keys_.pubkey);
    writer.write_data(padding_);
    std::visit([&writer](const auto& k) { writer.write_data(k.pubkey()); }, signing_);

    cert_.serialize();
    writer.write_data(cert_.buffer);

    update_hash();

    return *this;
  }

  /// @brief Deserialize the router identity from buffer
  Destination& deserialize()
  {
    const exception::Exception ex{"Router: Destination", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    reader.skip_bytes(CertOffset);
    reader.read_data(cert_.buffer);
    cert_.deserialize();

    if (cert_.locally_unreachable())
      {
        std::cerr << "Router: Destination: unreachable because of unsupported crypto.";
        return *this;
      }
    reader.skip_back(CertOffset + cert_.buffer.size());

    type_to_variant();
    resize_padding();

    crypto_t::pubkey_t pubkey;
    reader.read_data(pubkey);

    if (pubkey.is_zero())
      ex.throw_ex<std::runtime_error>("null crypto public key.");

    crypto_keys_.pubkey = std::move(pubkey);
    if (!crypto_keys_.pvtkey.is_zero() && !crypto_t::ValidPublicKey(crypto_keys_))
      crypto_keys_.pvtkey.zero();  // nullify the private key

    reader.read_data(padding_);

    std::visit(
        [&reader, &ex](auto& s) {
          typename std::decay_t<decltype(s)>::pubkey_t key;
          reader.read_data(key);

          if (key.is_zero())
            ex.throw_ex<std::runtime_error>("null signing public key.");

          s.rekey(std::move(key));
        },
        signing_);

    buf_.resize(cert_.buffer.size() + reader.count());
    update_hash();

    return *this;
  }

  /// @brief Rekey local crypto and signing keypairs
  /// @param crypto_keys Local crypto identity keypair
  /// @param sign_keys Local signing keypair
  template <
      class TSigning,
      typename = std::enable_if_t<
          std::is_same<TSigning, eddsa_t>::value || std::is_same<TSigning, reddsa_t>::value
          || std::is_same<TSigning, xeddsa_t>::value>>
  Destination& rekey(crypto_t::keypair_t crypto_keys, typename TSigning::keypair_t sign_keys)
  {
    const exception::Exception ex{"Router: Destination", __func__};

    using sign_keys_t = typename TSigning::keypair_t;

    if (crypto_keys.pvtkey.is_zero())
      ex.throw_ex<std::invalid_argument>("null crypto private key.");

    crypto_keys_.pvtkey = std::forward<crypto_t::pvtkey_t>(crypto_keys.pvtkey);

    if (crypto_keys.pubkey.is_zero())
      crypto_t::PrivateToPublic(crypto_keys_);
    else
      crypto_keys_.pubkey = std::forward<crypto_t::pubkey_t>(crypto_keys.pubkey);

    if (!std::holds_alternative<TSigning>(signing_))
      signing_.emplace<TSigning>(std::forward<sign_keys_t>(sign_keys));
    else
      std::get<TSigning>(signing_).rekey(std::forward<sign_keys_t>(sign_keys));

    update_padding();

    serialize();

    return *this;
  }

  template <
      class TSigning,
      typename = std::enable_if_t<
          std::is_same<TSigning, eddsa_t>::value
          || std::is_same<TSigning, reddsa_t>::value
          || std::is_same<TSigning, xeddsa_t>::value>>
  Destination& rekey(typename TSigning::pubkey_t sk)
  {
    using pubkey_t = typename TSigning::pubkey_t;

    if (!std::holds_alternative<TSigning>(signing_))
      signing_.emplace<TSigning>(std::forward<pubkey_t>(sk));
    else
      std::get<TSigning>(signing_).rekey(std::forward<pubkey_t>(sk));

    update_padding();

    return *this;
  }

  /// @brief Initialize the unblinded signature
  void init_signature(signature_v& sig) const
  {
    const exception::Exception ex{"Destination", __func__};

    if (std::holds_alternative<eddsa_t>(signing_))
      sig.emplace<eddsa_t::signature_t>();
    else if (std::holds_alternative<reddsa_t>(signing_))
      sig.emplace<reddsa_t::signature_t>();
    else if (std::holds_alternative<xeddsa_t>(signing_))
      sig.emplace<xeddsa_t::signature_t>();
    else
      ex.throw_ex<std::logic_error>("unsupported signing type");
  }

  /// @brief Initialize the blinded signature
  void init_signature(blind_signature_v& sig) const
  {
    const exception::Exception ex{"Destination", __func__};

    if (std::holds_alternative<reddsa_t>(blind_signing_))
      sig.emplace<reddsa_t::signature_t>();
    else if (std::holds_alternative<xeddsa_t>(blind_signing_))
      sig.emplace<xeddsa_t::signature_t>();
    else
      ex.throw_ex<std::logic_error>("unsupported blind signing type");
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Get a const reference to the crypto class
  const crypto_t::keypair_t& crypto() const noexcept
  {
    return crypto_keys_;
  }

  /// @brief Get a non-const reference to the crypto class
  crypto_t::keypair_t& crypto() noexcept
  {
    return crypto_keys_;
  }

  /// @brief Get the crypto public key length
  decltype(auto) crypto_pubkey_len() const
  {
    return tini2p::under_cast(crypto_t::PublicKeyLen);
  }

  /// @brief Get a const reference to the signing class
  const signing_v& signing() const noexcept
  {
    return signing_;
  }

  /// @brief Get a non-const reference to the signing class
  signing_v& signing() noexcept
  {
    return signing_;
  }

  /// @brief Get the Certificate signing type of the Destination
  cert_t::sign_type_t sign_type() const
  {
    cert_t::sign_type_t type;
    if (std::holds_alternative<eddsa_t>(signing_))
      type = cert_t::sign_type_t::EdDSA;
    else if (std::holds_alternative<reddsa_t>(signing_))
      type = cert_t::sign_type_t::RedDSA;
    else if (std::holds_alternative<xeddsa_t>(signing_))
      type = cert_t::sign_type_t::XEdDSA;
    else
      type = cert_t::sign_type_t::SigningUnsupported;

    return type;
  }

  /// @brief Get the signing public key length
  decltype(auto) signing_pubkey_len() const
  {
    return std::visit([](const auto& s) -> std::uint16_t { return std::decay_t<decltype(s)>::PublicKeyLen; }, signing_);
  }

  /// @brief Get the signature length
  decltype(auto) sig_len() const
  {
    return std::visit([](const auto& s) -> std::uint16_t { return std::decay_t<decltype(s)>::SignatureLen; }, signing_);
  }

  /// @brief Get the blind signing public key length
  decltype(auto) blind_signing_pubkey_len() const
  {
    return std::visit(
        [](const auto& s) -> std::uint16_t { return std::decay_t<decltype(s)>::PublicKeyLen; }, blind_signing_);
  }

  /// @brief Get the blind signature length
  decltype(auto) blind_sig_len() const
  {
    return std::visit(
        [](const auto& s) -> std::uint16_t { return std::decay_t<decltype(s)>::SignatureLen; }, blind_signing_);
  }

  const padding_t& padding() const noexcept
  {
    return padding_;
  }

  /// @brief Get a const reference to the certificate
  const cert_t& cert() const noexcept
  {
    return cert_;
  }

  /// @brief Get the total size of the router identity
  std::size_t size() const noexcept
  {
    return crypto_pubkey_len() + padding_.size() + signing_pubkey_len() + cert_.size();
  }

  /// @brief Get the padding size
  std::uint16_t padding_len() const noexcept
  {
    return padding_.size();
  }

  /// @brief Get a const reference to the Destination hash
  const hash_t& hash() const noexcept
  {
    return hash_;
  }

  /// @brief Calculate a new Destination hash from the current buffer
  void update_hash()
  {
    crypto::Sha256::Hash(hash_, buf_);
  }

 private:
  void resize_padding()
  {
    padding_.resize(KeysPaddingLen - (crypto_pubkey_len() + signing_pubkey_len()));
  }

  void update_padding()
  {
    resize_padding();
    crypto::RandBytes(padding_);
  }

  void type_to_variant()
  {
    if (cert_.sign_type == cert_t::sign_type_t::EdDSA && !std::holds_alternative<eddsa_t>(signing_))
      signing_.emplace<eddsa_t>();
    else if (cert_.sign_type == cert_t::sign_type_t::RedDSA && !std::holds_alternative<reddsa_t>(signing_))
      signing_.emplace<reddsa_t>();
    else if (cert_.sign_type == cert_t::sign_type_t::XEdDSA && !std::holds_alternative<xeddsa_t>(signing_))
      signing_.emplace<xeddsa_t>();
  }

  cert_t cert_;
  crypto_t::keypair_t crypto_keys_;
  signing_v signing_;
  blind_signing_v blind_signing_;
  hash_t hash_;
  padding_t padding_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_ROUTER_DESTINATION_H_
