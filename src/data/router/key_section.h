/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_ROUTER_KEY_SECTION_H_
#define SRC_DATA_ROUTER_KEY_SECTION_H_

#include "src/crypto/sec_bytes.h"
#include "src/crypto/x25519.h"

#include "src/data/router/destination.h"

namespace tini2p
{
namespace data
{
/// @struct KeySection
// TODO(tini2p): convert to class, make all data members private, impl getters/setters
struct KeySection
{
  /// @brief Key section public key type
  enum struct Type : std::uint16_t
  {
    ElGamal = 0x00,
    P256 = 0x01,
    P384 = 0x02,
    P521 = 0x03,
    X25519 = 0x04,
    Unsupported = 0xFFFE,
    Reserved = 0xFFFF,
  };

  enum : std::uint16_t
  {
    TypeLen = 2,  //< uint16_be, see spec
    SizeLen = 2,  //< uint16_be, see spec
    MinKeyLen = 32,  //< all EdDSA/ECDSA variants
    MaxKeyLen = 256,  //< ElGamal
    HeaderLen = TypeLen + SizeLen,
    MinLen = HeaderLen + MinKeyLen,
    MaxLen = HeaderLen + MaxKeyLen,
  };

  using type_t = Type;  //< Type trait alias
  using key_len_t = std::uint16_t;  //< Size trait alias
  using key_t = crypto::X25519::pubkey_t;  //< Key trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  type_t type;
  key_len_t key_len;
  key_t key;
  buffer_t buffer;

  /// @brief Default ctor, creates a KeySection for an X25519 key
  KeySection() : type(Type::X25519), key_len(Destination::crypto_t::PublicKeyLen), buffer(MinLen) {}

  /// @brief Create a KeySection for a given key buffer
  /// @param key_in Public key for this KeySection
  /// @detail Only X25519 keys currently supported
  explicit KeySection(key_t key_in)
      : type(Type::X25519),
        key_len(key_in.size()),
        key(std::forward<key_t>(key_in)),
        buffer(),
        locally_unreachable_(false)
  {
    serialize();
  }

  /// @brief Create a KeySection from a buffer
  KeySection(const std::uint8_t* data, const std::size_t len)
      : type(Type::X25519), key_len(), key(), buffer(), locally_unreachable_(false)
  {
    const exception::Exception ex{"KeySection", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buffer.resize(len);
    std::copy_n(data, len, buffer.data());

    deserialize();
  }

  std::uint16_t size() const noexcept
  {
    return TypeLen + SizeLen + key_len;
  }

  /// @brief Serialize KeySection to buffer
  void serialize()
  {
    const exception::Exception ex{"KeySection", __func__};

    if (key.is_zero())
      ex.throw_ex<std::logic_error>("null key.");

    key_len = key.size();
    buffer.resize(size());
    check_params(ex);

    tini2p::BytesWriter<buffer_t> writer(buffer);
    writer.write_bytes(type, tini2p::Endian::Big);
    writer.write_bytes(key_len, tini2p::Endian::Big);
    writer.write_data(key);
  }

  /// @brief Deserialize KeySection from buffer
  void deserialize()
  {
    const exception::Exception ex{"KeySection", __func__};

    tini2p::BytesReader<buffer_t> reader(buffer);
    reader.read_bytes(type, tini2p::Endian::Big);
    reader.read_bytes(key_len, tini2p::Endian::Big);

    check_params(ex);

    if (!locally_unreachable_)
      {
        key_t t_key;
        reader.read_data(t_key);

        if (t_key.is_zero())
          ex.throw_ex<std::logic_error>("null key.");

        key = std::move(t_key);
      }
    else
      reader.skip_bytes(key_len);

    // reclaim unused buffer space
    buffer.resize(reader.count());
    buffer.shrink_to_fit();
  }

  bool locally_unreachable() const noexcept
  {
    return locally_unreachable_;
  }

  /// @brief Compare equality with other KeySection
  std::uint8_t operator==(const KeySection& oth) const
  {  // attempt constant-time comparison
     const auto type_eq = static_cast<std::uint8_t>(type == oth.type);
     const auto len_eq = static_cast<std::uint8_t>(key_len == oth.key_len);
     const auto key_eq = static_cast<std::uint8_t>(key == oth.key);

     return (type_eq * len_eq * key_eq);
  }

 private:
  void check_params(const exception::Exception& ex)
  {
    const auto& buf_len = buffer.size();
    if (buf_len < MinLen || buf_len > MaxLen)
      ex.throw_ex<std::length_error>("invalid buffer length.");

    if (type != Type::X25519)
      locally_unreachable_ = true;

    if (key_len < MinKeyLen || key_len > MaxKeyLen)
      ex.throw_ex<std::length_error>("invalid key length: " + std::to_string(key_len));
  }

  bool locally_unreachable_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_ROUTER_KEY_SECTION_H_
