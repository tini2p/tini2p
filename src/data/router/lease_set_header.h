/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_ROUTER_LEASE_SET_HEADER_H_
#define SRC_DATA_ROUTER_LEASE_SET_HEADER_H_

#include <mutex>

#include "src/crypto/sec_bytes.h"

#include "src/data/router/destination.h"
#include "src/data/router/mapping.h"

#include "src/data/router/key_section.h"
#include "src/data/router/lease.h"

namespace tini2p
{
namespace data
{
/// @struct LeaseSet2Header
/// @detail LeaseSet2+ header data structure
class LeaseSet2Header
{
 public:
  enum struct Flag : std::uint16_t
  {
    OfflineKeys = 0x0000,  //< 0000000000000000, see spec
    OnlineKeys = 0x0001,   //< 0000000000000001, see spec
    Published = 0x0000,    //< 0000000000000000, see spec
    Unpublished = 0x0002,  //< 0000000000000010, see spec
    ReservedMask = 0xFFFC, //< 1111111111111100, see spec
  };

  enum : std::uint16_t
  {
    MinDestLen = Destination::MinLen,
    MaxDestLen = Destination::MaxLen,
    TimestampLen = 4,
    ExpiresLen = 2,
    FlagLen = 2,
    BlindExpiresLen = 4,
    BlindSigTypeLen = 2,
    MaxBlindPubKeyLen = 32,
    MaxSignatureLen = 64,
    MetaLen = TimestampLen + ExpiresLen + FlagLen,
    MinLen = MinDestLen + MetaLen,
    DefaultLen = MaxDestLen + MetaLen,
    MaxLen = MaxDestLen + MetaLen + BlindExpiresLen + BlindSigTypeLen + MaxBlindPubKeyLen + MaxSignatureLen,
    Timeout = 600,  //< 10 min in seconds, see spec
  };

  using flag_t = Flag; //< Flag trait alias
  using destination_t = data::Destination;  //< Destination trait alias
  using timestamp_t = std::uint32_t;  //< Timestamp trait alias
  using expires_t = std::uint16_t;  //< Expires trait alias
  using blind_expires_t = std::uint32_t;  //< Blind expires trait alias
  using blind_sigtype_t = destination_t::cert_t::sign_type_t;  //< Blind signing type trait alias
  using signature_v = destination_t::signature_v;  //< Signature trait alias
  using size_type = std::uint16_t;
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor, creates new Destination
  LeaseSet2Header() : dest_(), ts_(time::now_s()), expires_(Timeout), flags_(flag_t::OnlineKeys)
  {
    serialize();
  }

  /// @brief Create a LeaseSet2 for a given destination
  /// @param dest Local Destination for this LeaseSet2
  LeaseSet2Header(destination_t dest)
      : dest_(std::forward<destination_t>(dest)), ts_(time::now_s()), expires_(Timeout), flags_(flag_t::OnlineKeys)
  {
    serialize();
  }

  /// @brief Create a LeaseSet2Header from a given buffer
  /// @param buf Buffer containing a serialized LeaseSet2
  explicit LeaseSet2Header(buffer_t buf)
  {
    const exception::Exception ex{"LeaseSet2Header", __func__};

    check_buffer_len(buf, ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  LeaseSet2Header(const LeaseSet2Header& oth)
      : dest_(oth.dest_),
        ts_(oth.ts_),
        expires_(oth.expires_),
        flags_(oth.flags_),
        blind_expires_(oth.blind_expires_),
        blind_type_(oth.blind_type_),
        blind_key_(oth.blind_key_),
        signature_(oth.signature_),
        buf_(oth.buf_)
  {
  }

  LeaseSet2Header(LeaseSet2Header&& oth)
      : dest_(std::forward<destination_t>(oth.dest_)),
        ts_(std::forward<timestamp_t>(oth.ts_)),
        expires_(std::forward<expires_t>(oth.expires_)),
        flags_(std::forward<flag_t>(oth.flags_)),
        blind_expires_(std::forward<blind_expires_t>(oth.blind_expires_)),
        blind_type_(std::forward<blind_sigtype_t>(oth.blind_type_)),
        blind_key_(std::forward<destination_t::blind_pubkey_v>(oth.blind_key_)),
        signature_(std::forward<destination_t::signature_v>(oth.signature_)),
        buf_(std::forward<buffer_t>(oth.buf_))
  {
  }

  /// @brief Forwarding-assignment operator
  LeaseSet2Header& operator=(LeaseSet2Header oth)
  {
    dest_ = std::forward<destination_t>(oth.dest_);
    ts_ = std::forward<timestamp_t>(oth.ts_);
    expires_ = std::forward<expires_t>(oth.expires_);
    flags_ = std::forward<flag_t>(oth.flags_);
    blind_expires_ = std::forward<blind_expires_t>(oth.blind_expires_);
    blind_type_ = std::forward<blind_sigtype_t>(oth.blind_type_);
    blind_key_ = std::forward<destination_t::blind_pubkey_v>(oth.blind_key_);
    signature_ = std::forward<destination_t::signature_v>(oth.signature_);
    buf_ = std::forward<buffer_t>(oth.buf_);

    return *this;
  }

  /// @brief Serialize the LeaseSet2Header to buffer
  void serialize()
  {
    buf_.resize(size());

    check_params({"LeaseSet2Header", __func__});

    tini2p::BytesWriter<buffer_t> writer(buf_);

    // type is set in NetDB messages

    dest_.serialize();
    writer.write_data(dest_.buffer());

    writer.write_bytes(ts_, tini2p::Endian::Big);
    writer.write_bytes(expires_, tini2p::Endian::Big);
    writer.write_bytes(flags_, tini2p::Endian::Big);

    if (has_offline_keys())
      {
        // write blinding data
        const auto expires_offset = writer.count();
        writer.write_bytes(blind_expires_, tini2p::Endian::Big);
        writer.write_bytes(blind_type_, tini2p::Endian::Big);
        std::visit([&writer](const auto& k) { writer.write_data(k); }, blind_key_);

        // sign + write signature
        signature_ = dest_.Sign(buf_.data() + expires_offset, writer.count() - expires_offset);
        std::visit([&writer](const auto& s) { writer.write_data(s); }, signature_);
      }
  }

  /// @brief Deserialize the LeaseSet2Header from buffer
  /// @detail Caller must set expected header type (e.g. LeaseSet2, EncLeaseSet2, etc.) before calling
  void deserialize()
  {
    const exception::Exception ex{"LeaseSet2Header", __func__};

    if (buf_.size() > MaxLen)
      ex.throw_ex<std::length_error>("invalid LeaseSet2 size.");

    tini2p::BytesReader<buffer_t> reader(buf_);

    // type is determined from NetDB messages

    if (reader.gcount() < destination_t::MinLen)
        ex.throw_ex<std::length_error>("too small for a valid LeaseSet2.");

    // read destination
    auto& dest_buf = dest_.buffer();
    dest_buf.resize(MaxDestLen);
    std::copy_n(buf_.begin() + reader.count(), MaxDestLen, dest_.buffer().begin());
    dest_.deserialize();

    reader.skip_bytes(dest_.size());
    reader.read_bytes(ts_, tini2p::Endian::Big);  // read timestamp
    reader.read_bytes(expires_, tini2p::Endian::Big);  // read expires (past timestamp)

    // read flags
    reader.read_bytes(flags_, tini2p::Endian::Big);

    // if verifiable, read the signature
    if (!dest_.cert().locally_unreachable() && has_offline_keys())
      {
        reader.read_bytes(blind_expires_, tini2p::Endian::Big);
        reader.read_bytes(blind_type_, tini2p::Endian::Big);
        std::visit([&reader](auto& k) { reader.read_data(k); }, blind_key_);
        dest_.init_signature(signature_);
        std::visit([&reader](auto& s) { reader.read_data(s); }, signature_);
      }
  }

  /// @brief Verify the LeaseSet2 signature
  /// @return True if signature passes verification
  std::uint16_t Verify() const
  {
    const auto expires_offset = buf_.size() - offline_size();
    std::uint16_t ret = has_online_keys();

    if (has_offline_keys() && !dest_.cert().locally_unreachable())
      ret = dest_.Verify(buf_.data() + expires_offset, expires_offset, signature_);

    return ret;
  }

  /// @brief Get a const reference to the destination
  const destination_t& destination() const noexcept
  {
    return dest_;
  }

  /// @brief Get a non-const reference to the destination
  destination_t& destination() noexcept
  {
    return dest_;
  }

  /// @brief Get a non-const reference to the destination hash
  const destination_t::hash_t& hash() const noexcept
  {
    return dest_.hash();
  }

  /// @brief Get the total size of the LeaseSet2
  std::uint16_t size() const
  {
    return dest_.size() + TimestampLen + ExpiresLen + FlagLen + offline_size();
  }

  /// @brief Get the size of offline signing data
  std::uint16_t offline_size() const
  {
    return has_offline_keys()
           * (BlindExpiresLen + BlindSigTypeLen + std::visit([](const auto& k) { return k.size(); }, blind_key_));
  }

  /// @brief Get a const reference to the creation time
  const timestamp_t& ts() const noexcept
  {
    return ts_;
  }

  /// @brief Get a const reference to the expiration time
  const expires_t& expires() const noexcept
  {
    return expires_;
  }

  /// @brief Get a const reference to the flag
  const flag_t& flags() const noexcept
  {
    return flags_;
  }

  /// @brief Check if the online keys flag is set
  std::uint16_t has_online_keys() const noexcept
  {  // mask with the online keys flag, unsets all irrelevant bits
    return tini2p::under_cast(flags_) & tini2p::under_cast(flag_t::OnlineKeys);
  }

  /// @brief Check if the online keys flag is unset
  std::uint16_t has_offline_keys() const noexcept
  {
    return static_cast<std::uint16_t>(has_online_keys() == 0);
  }

  /// @brief Check if the unpublished flag is unset
  bool is_published() const
  {
    return !is_unpublished();
  }

  /// @brief Check if the unpublished flag is set
  bool is_unpublished() const
  {  // mask with the unpublished flag, unsets all irrelevant bits
    const auto unpublished_f = tini2p::under_cast(flag_t::Unpublished);
    return (tini2p::under_cast(flags_) & unpublished_f) == unpublished_f;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Compare equality with other LeaseSet2Header
  std::uint8_t operator==(const LeaseSet2Header& oth) const
  {  // attempt constant-time comparison
    const auto& dst_eq = static_cast<std::uint8_t>(dest_.hash() == oth.dest_.hash());
    const auto& tsp_eq = static_cast<std::uint8_t>(ts_ == oth.ts_);
    const auto& exp_eq = static_cast<std::uint8_t>(expires_ == oth.expires_);
    const auto& flg_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);

    return (dst_eq * tsp_eq * exp_eq * flg_eq);
  }

 private:
  void check_params(const exception::Exception& ex)
  {
    check_buffer_len(buf_, ex);

    if (tini2p::under_cast(flags_) & tini2p::under_cast(flag_t::ReservedMask))
      ex.throw_ex<std::logic_error>("invalid LeaseSet2 flag.");

    // check if LeaseSet2 creation timestamp + expiration timeout has already passed
    const auto& now = time::now_s();
    if (ts_ + expires_ <= now)
      {
        ex.throw_ex<std::logic_error>(
            "expired LeaseSet2, ts: " + std::to_string(ts_) + ", expires: " + std::to_string(expires_)
            + ", now: " + std::to_string(now) + ".");
      }
  }

  void check_buffer_len(const buffer_t& buf, const exception::Exception& ex)
  {
    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);
    const auto& buf_len = buf.size();

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  destination_t dest_;
  timestamp_t ts_;
  expires_t expires_;
  flag_t flags_;
  blind_expires_t blind_expires_;
  blind_sigtype_t blind_type_;
  destination_t::blind_pubkey_v blind_key_;
  destination_t::signature_v signature_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_ROUTER_LEASE_SET_HEADER_H_
