/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_ROUTER_LEASE_SET_H_
#define SRC_DATA_ROUTER_LEASE_SET_H_

#include <mutex>

#include "src/crypto/sec_bytes.h"

#include "src/data/router/mapping.h"

#include "src/data/router/key_section.h"
#include "src/data/router/lease.h"
#include "src/data/router/lease_set_header.h"

namespace tini2p
{
namespace data
{
class Info;  // forward-declaration for comparison

/// @class LeaseSet2
/// @brief LeaseSet2+ implementation
// TODO(tini2p): continue cleanup, restrict KeySection + Leases setter, review thread-safety
class LeaseSet2
{
 public:
  using properties_t = Mapping;  //< Properties trait alias
  using key_section_t = KeySection;  //< Key section trait alias
  using lease_t = Lease2;  //< Lease trait alias
  using cert_t = data::Certificate;  //< Certificate trait alias
  using destination_t = data::Destination;  //< Destination trait alias
  using header_t = LeaseSet2Header;  //< Header trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  using pointer = LeaseSet2*;  //< Non-owning pointer trait alias
  using const_pointer = const LeaseSet2*;  //< Const non-owning pointer trait alias
  using unique_ptr = std::unique_ptr<LeaseSet2>;  //< Unique pointer trait alias
  using const_unique_ptr = std::unique_ptr<const LeaseSet2>;  //< Const unique pointer trait alias
  using shared_ptr = std::shared_ptr<LeaseSet2>;  //< Shared pointer trait alias
  using const_shared_ptr = std::shared_ptr<const LeaseSet2>;  //< Const shared pointer trait alias

  enum : std::uint16_t
  {
    MinKeySections = 1,  //< because what's the point without one key?
    MaxKeySections = 5,  //< arbitrary max, one for each key type
    MinLeases = 0,  //< see spec, limit total number of zero-lease LS in NetDB to prevent DDoS
    MaxLeases = 255,  //< bound by uint8_t_MAX, limit more?
    MinPropertiesLen = Mapping::MinLen,
    MaxPropertiesLen = Mapping::MaxLen,
    MinSigLen = 40,  // DSA signature
    MaxSigLen = 64,  // EdDSA signature(s)
    KeySectionNumLen = 1,
    LeaseNumLen = 1,
    MetaLen = KeySectionNumLen + LeaseNumLen,
    MinLen = LeaseSet2Header::MinLen + MetaLen + MinPropertiesLen + (MinKeySections * KeySection::MinLen) + MinSigLen,
    MaxLen = LeaseSet2Header::MaxLen + MetaLen + MaxPropertiesLen + (MaxKeySections * KeySection::MaxLen) + (MaxLeases * Lease2::Len) + MaxSigLen,
  };

  /// @brief LeaseSet2 default-ctor
  LeaseSet2() : header_(), properties_(), leases_(), buf_(tini2p::under_cast(MinLen))
  {
    key_sections_.emplace_back(header_.destination().crypto().pubkey);

    serialize();
  }

  /// @brief Create a LeaseSet2 for a given destination
  /// @param dest Local Destination for this LeaseSet2
  /// @param leases Container of Leases for this LeaseSet2
  LeaseSet2(header_t::destination_t dest, std::vector<lease_t> leases = {})
      : leases_(std::forward<std::vector<lease_t>>(leases)), buf_()
  {
    const exception::Exception ex{"LeaseSet2", __func__};

    key_sections_.emplace_back(dest.crypto().pubkey);
    header_ = std::forward<header_t::destination_t>(dest);

    serialize();
  }

  LeaseSet2(buffer_t buf)
  {
    const exception::Exception ex{"LeaseSet2", __func__};

    check_len(buf.size(), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Create a LeaseSet2 from a buffer
  /// @param data Pointer to the buffer
  /// @param len Size of the buffer
  /// @throw Invalid argument for null and/or out-of-range buffer/length
  LeaseSet2(const std::uint8_t* data, const std::size_t len)
  {
    const exception::Exception ex{"LeaseSet2", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  /// @brief Serialize the LeaseSet2 to buffer
  void serialize()
  {
    const exception::Exception ex{"LeaseSet2", __func__};

    check_key_sections(ex);

    const auto full_len = size();
    check_len(full_len, ex);

    buf_.resize(full_len);

    tini2p::BytesWriter<buffer_t> writer(buf_);

    header_.serialize();
    writer.write_data(header_.buffer());

    properties_.serialize();
    writer.write_data(properties_.buffer());

    write_key_sections(writer);
    write_leases(writer);

    if (header_.has_online_keys())
      {
        signature_ = header_.destination().Sign(buf_.data(), writer.count());
        std::visit([&writer](const auto& s) { writer.write_data(s); }, signature_);
      }
    else
      {
        blind_signature_ = header_.destination().BlindSign(buf_.data(), writer.count());
        std::visit([&writer](const auto& s) { writer.write_data(s); }, blind_signature_);
      }
  }

  /// @brief Derialize the LeaseSet2 from buffer
  void deserialize()
  {
    const exception::Exception ex{"LeaseSet2", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    reader.skip_bytes(header_t::MinDestLen - cert_t::HeaderLen);
    cert_t::cert_type_t cert_type(cert_t::cert_type_t::NullCert);
    reader.read_bytes(cert_type);

    header_t::size_type header_len(0);

    cert_t::sign_type_t dest_sig_type(cert_t::sign_type_t::EdDSA);
    cert_t::length_t cert_len;
    if (cert_type == cert_t::cert_type_t::KeyCert)
      {
        header_len = header_t::DefaultLen;
        reader.read_bytes(cert_len);
        reader.read_bytes(dest_sig_type, tini2p::Endian::Big);
        reader.skip_bytes(cert_t::CryptoTypeLen);
      }
    else
      ex.throw_ex<std::runtime_error>("unsupported certificate type.");

    // TODO(tini2p): change when other signature types are supported.
    //     Using versioned crypto, only one signature type should be supported for any
    //     given release version.
    if (dest_sig_type != cert_t::sign_type_t::EdDSA)
      ex.throw_ex<std::runtime_error>("unsupported signature type.");

    reader.skip_bytes(header_t::MetaLen - header_t::FlagLen);
    header_t::flag_t head_flag;
    reader.read_bytes(head_flag, tini2p::Endian::Big);

    if (!(tini2p::under_cast(head_flag) & tini2p::under_cast(header_t::flag_t::OnlineKeys)))
      {  // header has offline keys, blinded data included
        reader.skip_bytes(header_t::BlindExpiresLen);
        header_t::blind_sigtype_t blind_sigtype;
        reader.read_bytes(blind_sigtype, tini2p::Endian::Big);
        header_len += tini2p::under_cast(header_t::BlindExpiresLen) + tini2p::under_cast(header_t::BlindSigTypeLen)
                      + cert_t::sign_pubkey_len(blind_sigtype) + cert_t::signature_len(dest_sig_type);
      }

    reader.reset();

    auto& head_buf = header_.buffer();
    head_buf.resize(header_len);
    reader.read_data(head_buf);

    header_.deserialize();

    properties_t::size_type prop_len;
    reader.read_bytes(prop_len);
    reader.skip_back(properties_t::SizeLen);
    auto& prop_buf = properties_.buffer();
    prop_buf.resize(tini2p::under_cast(properties_t::SizeLen) + prop_len);
    reader.read_data(prop_buf);
    properties_.deserialize();

    read_key_sections(reader);  // read n key sections
    read_leases(reader);  // read n leases

    const auto& dest = header_.destination();
    if (header_.has_online_keys())
      {
        dest.init_signature(signature_);
        std::visit([&reader](auto& s) { reader.read_data(s); }, signature_);
      }
    else
      {
        dest.init_signature(blind_signature_);
        std::visit([&reader](auto& s) { reader.read_data(s); }, blind_signature_);
      }

    check_key_sections(ex);
  }

  /// @brief Verify the LeaseSet2 signature
  std::uint8_t Verify() const
  {
    const auto& dest = header_.destination();
    std::uint8_t ret(0);

    if (header_.has_online_keys())
      ret = dest.Verify(buf_.data(), buf_.size() - dest.sig_len(), signature_);
    else
      ret = dest.BlindVerify(buf_.data(), buf_.size() - dest.blind_sig_len(), blind_signature_);

    return ret;
  }

  /// @brief Get a const reference to the destination hash
  const header_t::destination_t::hash_t& hash() const noexcept
  {
    return header_.hash();
  }

  /// @brief Get a const reference to the destination
  const header_t::destination_t& destination() const noexcept
  {
    return header_.destination();
  }

  /// @brief Get a non-const reference to the destination
  header_t::destination_t& destination() noexcept
  {
    return header_.destination();
  }

  /// @brief Get a const reference to the header
  const header_t& header() const noexcept
  {
    return header_;
  }

  /// @brief Get a non-const reference to the header
  header_t& header() noexcept
  {
    return header_;
  }

  /// @brief Get a const reference to the properties
  const properties_t& properties() const noexcept
  {
    return properties_;
  }

  /// @brief Get a non-const reference to the properties
  properties_t& properties() noexcept
  {
    return properties_;
  }

  /// @brief Get a const reference to the KeySections
  const std::vector<key_section_t>& key_sections() const noexcept
  {
    return key_sections_;
  }

  /// @brief Add a KeySection to the LeaseSet2
  /// @param ks KeySection to add
  /// @throw Logic error on reaching max key sections limit
  void add_key_section(key_section_t ks)
  {
    const exception::Exception ex{"LeaseSet2", __func__};

    if (key_sections_.size() == MaxKeySections)
      ex.throw_ex<std::logic_error>("max key sections reached: " + std::to_string(MaxKeySections));

    key_sections_.emplace_back(std::forward<key_section_t>(ks));
  }

  /// @brief Get a const reference to the Leases
  const std::vector<lease_t>& leases() const noexcept
  {
    return leases_;
  }

  /// @brief Add a Lease to the LeaseSet2
  /// @param le Lease to add
  /// @throw Logic error on reaching max leases limit
  LeaseSet2& add_lease(lease_t le)
  {
    const exception::Exception ex{"LeaseSet2", __func__};

    if (leases_.size() == MaxLeases)
      ex.throw_ex<std::logic_error>("max leases reached: " + std::to_string(MaxLeases));

    leases_.emplace_back(std::forward<lease_t>(le));

    return *this;
  }

  /// @brief Get a random lease
  const lease_t& get_lease()
  {
    const exception::Exception& ex{"Router: LeaseSet2", __func__};

    std::shared_lock ll(ls_mutex_, std::defer_lock);
    std::scoped_lock sgd(ll);

    if (leases_.empty())
      ex.throw_ex<std::runtime_error>("empty leases");

    return leases_.at(crypto::RandInRange(0, leases_.size() - 1));
  }

  std::uint32_t size() const noexcept
  {
    return header_.size() + properties_.size() + KeySectionNumLen + key_sections_len() + LeaseNumLen + leases_len()
           + (header_.has_online_keys() ? header_.destination().sig_len() : header_.destination().blind_sig_len());
  }

  std::uint16_t key_sections_len() const
  {
    std::uint16_t len(0);
    for (const auto& k : key_sections_)
      len += k.size();

    return len;
  }

  std::uint16_t leases_len() const
  {
    return leases_.size() * lease_t::Len;
  }

  std::uint32_t expiration() const noexcept
  {
    return header_.ts() + header_.expires();
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Compare equality with other LeaseSet2
  /// @detail Not thread-safe because mutex locking requires non-const
  std::uint8_t operator==(const LeaseSet2& oth) const
  {  // attempt constant-time comparison
    const auto& hdr_eq = static_cast<std::uint8_t>(header_ == oth.header_);
    const auto& prp_eq = static_cast<std::uint8_t>(properties_ == oth.properties_);
    const auto& key_eq = static_cast<std::uint8_t>(key_sections_ == oth.key_sections_);
    const auto& lss_eq = static_cast<std::uint8_t>(leases_ == oth.leases_);

    return (hdr_eq * prp_eq * key_eq * lss_eq);
  }

  std::uint8_t operator==(const Info&) const
  {
    return 0;
  }

 private:
  void check_len(const std::size_t buf_len, const exception::Exception& ex)
  {
    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  void check_key_sections(const exception::Exception& ex)
  {
    if (key_sections_.size() > MaxKeySections)
      ex.throw_ex<std::logic_error>("too many key sections: " + std::to_string(key_sections_.size()));
  }

  void write_key_sections(tini2p::BytesWriter<buffer_t>& writer)
  {
    std::scoped_lock kgd(ks_mutex_);
    writer.write_bytes(static_cast<std::uint8_t>(key_sections_.size()));
    for (auto& section : key_sections_)
      {
        section.serialize();
        writer.write_data(section.buffer);
      }
  }

  void write_leases(tini2p::BytesWriter<buffer_t>& writer)
  {
    std::scoped_lock lgd(ls_mutex_);
    writer.write_bytes(static_cast<std::uint8_t>(leases_.size()));
    for (auto& lease : leases_)
      {
        lease.serialize();
        writer.write_data(lease.buffer());
      }
  }

  void read_key_sections(tini2p::BytesReader<buffer_t>& reader)
  {
    const exception::Exception ex{"LeaseSet2", __func__};

    std::uint8_t ks_n;
    reader.read_bytes(ks_n);  // read number of key sections

    if (ks_n < MinKeySections || ks_n > MaxKeySections)
      ex.throw_ex<std::length_error>("invalid number of key sections: " + std::to_string(ks_n));

    // create temp key section container to swap after processing
    key_section_t::key_len_t k_len;
    std::vector<key_section_t> key_sections;
    key_sections.reserve(ks_n);
    
    for (std::uint16_t i = 0; i < ks_n; ++i)
      {
        reader.skip_bytes(key_section_t::TypeLen);
        reader.read_bytes(k_len, tini2p::Endian::Big);  // read key length
        reader.skip_back(key_section_t::HeaderLen);  // rewind reader to beginning of key section

        const auto& section_len = static_cast<std::size_t>(key_section_t::HeaderLen) + k_len;
        if (k_len < key_section_t::MinKeyLen || k_len > key_section_t::MaxKeyLen)
          {
            ex.throw_ex<std::length_error>(
                "invalid key length: " + std::to_string(k_len) + ", min: " + std::to_string(key_section_t::MinKeyLen)
                + "max: " + std::to_string(key_section_t::MaxKeyLen));
          }

        if (section_len > reader.gcount())
          {
            ex.throw_ex<std::length_error>(
                "invalid section length: " + std::to_string(section_len)
                + ", remaining: " + std::to_string(reader.gcount()));
          }

        key_sections.emplace_back(key_section_t(buf_.data() + reader.count(), section_len));  // deserialize in-place
        reader.skip_bytes(section_len);  // advance the reader
      }
    std::scoped_lock kgd(ks_mutex_);
    key_sections_.swap(key_sections);
  }

  void read_leases(tini2p::BytesReader<buffer_t>& reader)
  {
    const exception::Exception ex{"LeaseSet2", __func__};

    std::uint8_t ls_n;
    reader.read_bytes(ls_n);  // read number of leases

    if (ls_n < MinLeases || ls_n > MaxLeases)
      ex.throw_ex<std::length_error>("invalid number of leases.");

    // create temp lease container to swap after processing
    std::vector<lease_t> leases;
    leases.reserve(ls_n);

    for (std::uint16_t i = 0; i < ls_n; ++i)
      {
        lease_t::buffer_t lease_buf;
        reader.read_data(lease_buf);
        leases.emplace_back(std::move(lease_buf));
      }
    std::scoped_lock lgd(ls_mutex_);
    leases_.swap(leases);
  }

  header_t header_;
  properties_t properties_;

  std::vector<key_section_t> key_sections_;
  std::shared_mutex ks_mutex_;

  std::vector<lease_t> leases_;
  std::shared_mutex ls_mutex_;

  header_t::destination_t::signature_v signature_;
  header_t::destination_t::blind_signature_v blind_signature_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_ROUTER_LEASE_SET_H_
