/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_ROUTER_CERTIFICATE_H_
#define SRC_DATA_ROUTER_CERTIFICATE_H_

#include <iostream>
#include <type_traits>

#include "src/exception/exception.h"

#include "src/bytes.h"

#include "src/data/router/meta.h"

namespace tini2p
{
namespace data
{
/// @brief Container and processor of router certificates
// TODO(tini2p): make all data members private, create getters/setters
class Certificate
{
 public:
  enum Sizes : std::uint8_t
  {
    CertTypeLen = 1,
    NullCertLen = 0,
    KeyCertLen = 4,
    LengthLen = 2,
    SignTypeLen = 2,
    CryptoTypeLen = 2,
    HeaderLen = CertTypeLen + LengthLen,
  };

  enum Offsets : std::uint8_t
  {
    CertTypeOffset = 0,
    LengthOffset,
    SignTypeOffset = 3,
    CryptoTypeOffset = 5,
  };

  enum Cert_t : std::uint8_t
  {
    NullCert = 0,
    HashCashCert,
    HiddenCert,
    SignedCert,
    MultipleCert,
    KeyCert
  };

  enum struct SignatureLen
  {
    EdDSA = 64,
    RedDSA = 64,
    XEdDSA = 64,
  };

  enum struct PubkeyLen
  {
    EdDSA = 32,
    RedDSA = 32,
    XEdDSA = 32,
  };

  enum Signing_t : std::uint16_t
  {
    DSASha1 = 0,
    ECDSASha256P256,
    ECDSASha384P384,
    ECDSASha512P521,
    RSASha256_2048,
    RSASha384_3072,
    RSASha512_4096,
    EdDSASha512Ed25519,
    EdDSASha512Ed25519ph,
    Gost256,
    Gost512,
    RedDSA,
    XEdDSA,
    SigningUnsupported = 65534,
    SigningReserved = 65535,
    // convenience definition
    EdDSA = EdDSASha512Ed25519,
  };

  enum Crypto_t : std::uint16_t
  {
    ElGamal = 0,
    EciesP256,
    EciesP384,
    EciesP521,
    EciesX25519,
    EciesX25519Blake,
    CryptoUnsupported = 65534,
    CryptoReserved = 65535
  };

  using cert_type_t = Cert_t;
  using length_t = std::uint16_t;
  using sign_type_t = Signing_t;
  using crypto_type_t = Crypto_t;
  using size_type = std::uint16_t;
  using buffer_t = crypto::SecBytes;

  cert_type_t cert_type;
  length_t length;
  sign_type_t sign_type;
  crypto_type_t crypto_type;
  buffer_t buffer;

  /// @brief Default ctor
  Certificate()
      : cert_type(cert_type_t::NullCert),
        length(NullCertLen),
        sign_type(sign_type_t::EdDSA),
        crypto_type(crypto_type_t::EciesX25519),
        buffer(HeaderLen),
        locally_unreachable_(false)
  {
    serialize();
  }

  /// @brief Create a Certificate by certificate type
  /// @detail Valid types are NullCert and KeyCert.
  ///     KeyCert defaults to EdDSA signing type, and ECIES-X25519-AEAD-Ratchet crypto type
  /// @throw Invalid argument for unsupported certificate types
  explicit Certificate(cert_type_t type) : cert_type(std::forward<cert_type_t>(type)), buffer()
  {
    const exception::Exception ex{"Router: Certificate", __func__};

    check_cert_type(ex);

    if (cert_type == cert_type_t::KeyCert)
      {
        length = KeyCertLen;
        sign_type = sign_type_t::EdDSA;
        crypto_type = crypto_type_t::EciesX25519;
      }
    else
      {
        length = NullCertLen;
        sign_type = sign_type_t::SigningReserved;
        crypto_type = crypto_type_t::CryptoReserved;
      }

    locally_unreachable_ = false;
    
    serialize();
  }

  /// @brief Create a certificate with given sign + crypto types
  Certificate(const sign_type_t sign_type_in, const crypto_type_t crypto_type_in)
      : cert_type(cert_type_t::KeyCert),
        length(KeyCertLen),
        sign_type(sign_type_in),
        crypto_type(crypto_type_in),
        buffer(KeyCertLen),
        locally_unreachable_(false)
  {
    serialize();
  }

  /// @brief Create a certificate with given sign + crypto type info
  Certificate(const std::type_info& sign_type_info, const std::type_info& crypto_type_info)
      : cert_type(cert_type_t::KeyCert), length(KeyCertLen), buffer(KeyCertLen), locally_unreachable_(false)
  {
    type_info_to_type(sign_type_info, crypto_type_info);

    serialize();
  }

  /// @brief Convert signing + crypto type_info to sign + crypto types
  void type_info_to_type(const std::type_info& sign_type_info, const std::type_info& crypto_type_info)
  {
    if (sign_type_info == typeid(crypto::EdDSASha512))
      sign_type = sign_type_t::EdDSA;
    else if (sign_type_info == typeid(crypto::RedDSASha512))
      sign_type = sign_type_t::RedDSA;
    else if (sign_type_info == typeid(crypto::XEdDSASha512))
      sign_type = sign_type_t::XEdDSA;
    else
      sign_type = sign_type_t::SigningUnsupported;

    if (crypto_type_info == typeid(crypto::X25519))
      crypto_type = crypto_type_t::EciesX25519;
    else
      crypto_type = crypto_type_t::CryptoUnsupported;
  }

  /// @brief Serialize the certificate to buffer
  void serialize()
  {
    const exception::Exception ex{"Router: Certificate", __func__};

    buffer.resize(size());

    check_params(ex);

    tini2p::BytesWriter<buffer_t> writer(buffer);

    writer.write_bytes(cert_type);
    writer.write_bytes(length, tini2p::Endian::Big);

    if (cert_type == cert_type_t::KeyCert)
      {
        writer.write_bytes(sign_type, tini2p::Endian::Big);
        writer.write_bytes(crypto_type, tini2p::Endian::Big);
      }
  }

  /// @brief Deserialize the certificate from buffer
  void deserialize()
  {
    const exception::Exception ex{"Router: Certificate", __func__};

    tini2p::BytesReader<buffer_t> reader(buffer);

    reader.read_bytes(cert_type);
    reader.read_bytes(length, tini2p::Endian::Big);

    check_length(ex);

    if (cert_type == cert_type_t::KeyCert)
      {
        reader.read_bytes(sign_type, tini2p::Endian::Big);
        reader.read_bytes(crypto_type, tini2p::Endian::Big);
      }
    else if (cert_type == cert_type_t::NullCert)
      {
        sign_type = sign_type_t::SigningReserved;
        crypto_type = crypto_type_t::CryptoReserved;
      }

    check_params(ex);

    // reclaim unused space
    buffer.resize(reader.count());
    buffer.shrink_to_fit();
  }

  std::uint16_t size() const
  {
    return HeaderLen + length;
  }

  /// @brief Get the signature length for the signing type
  static size_type signature_len(const sign_type_t& sig_type)
  {
    const exception::Exception ex{"Router: Certificate", __func__};
    
    size_type sig_len(0);

    if (sig_type == sign_type_t::EdDSA)
      sig_len = tini2p::under_cast(SignatureLen::EdDSA);
    else if (sig_type == sign_type_t::RedDSA)
      sig_len = tini2p::under_cast(SignatureLen::RedDSA);
    else if (sig_type == sign_type_t::XEdDSA)
      sig_len = tini2p::under_cast(SignatureLen::XEdDSA);
    else
      ex.throw_ex<std::invalid_argument>("invalid blinded signature type.");

    return sig_len;
  }

  /// @brief Get the signing public key length for the signing type
  static size_type sign_pubkey_len(const sign_type_t& sig_type)
  {
    const exception::Exception ex{"Router: Certificate", __func__};
    
    size_type pubkey_len(0);

    if (sig_type == sign_type_t::EdDSA)
      pubkey_len = tini2p::under_cast(PubkeyLen::EdDSA);
    else if (sig_type == sign_type_t::RedDSA)
      pubkey_len = tini2p::under_cast(PubkeyLen::RedDSA);
    else if (sig_type == sign_type_t::XEdDSA)
      pubkey_len = tini2p::under_cast(PubkeyLen::XEdDSA);
    else
      ex.throw_ex<std::invalid_argument>("invalid public key type.");

    return pubkey_len;
  }

  /// @brief Get unreachable status
  /// @detail Identity/Destination is unreachable if cert has unsupported crypto
  constexpr bool locally_unreachable() const noexcept
  {
    return locally_unreachable_;
  }

  /// @brief Compare equality with another Certificate
  std::uint8_t operator==(const Certificate& oth) const
  {  // attempt constant-time comparison
    const auto& type_eq = static_cast<std::uint8_t>(cert_type == oth.cert_type);
    const auto& len_eq = static_cast<std::uint8_t>(length == oth.length);
    const auto& sign_eq = static_cast<std::uint8_t>(sign_type == oth.sign_type);
    const auto& crypto_eq = static_cast<std::uint8_t>(crypto_type == oth.crypto_type);

    return (type_eq * len_eq * sign_eq * crypto_eq);
  }

 private:
  void check_params(const tini2p::exception::Exception& ex)
  {
    check_cert_type(ex);
    check_length(ex);

    if (cert_type == cert_type_t::KeyCert)
    {
      switch (crypto_type)
        {
          case crypto_type_t::EciesX25519:
          case crypto_type_t::EciesX25519Blake:
            break;
          default:
            locally_unreachable_ = true;
            break;
        };

      switch (sign_type)
        {
          case sign_type_t::EdDSA:
          case sign_type_t::RedDSA:
          case sign_type_t::XEdDSA:
            break;
          default:
            locally_unreachable_ = true;
            break;
        };
    }
  }

  void check_length(const exception::Exception& ex) const
  {
    const auto& expected_len =
        cert_type == cert_type_t::KeyCert ? tini2p::under_cast(KeyCertLen) : tini2p::under_cast(NullCertLen);
    if (length != expected_len)
      {
        ex.throw_ex<std::runtime_error>(
            "invalid certificate length: " + std::to_string(length) + " expected: " + std::to_string(expected_len));
      }
  }

  void check_cert_type(const exception::Exception& ex) const
  {
    if (cert_type != cert_type_t::NullCert && cert_type != cert_type_t::KeyCert)
      ex.throw_ex<std::runtime_error>("invalid certificate type.");
  }

  bool locally_unreachable_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_ROUTER_CERTIFICATE_H_
