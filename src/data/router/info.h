/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_ROUTER_INFO_H_
#define SRC_DATA_ROUTER_INFO_H_

#include "src/time.h"

#include "src/crypto/aes.h"
#include "src/crypto/codecs.h"
#include "src/crypto/keys.h"
#include "src/crypto/signature.h"
#include "src/crypto/x25519.h"

#include "src/data/router/address.h"
#include "src/data/router/identity.h"
#include "src/data/router/meta.h"
#include "src/data/router/mapping.h"

namespace tini2p
{
namespace data
{
class LeaseSet2;  // forward-declaration for comparison;

#if defined(TINI2P_MAINNET)
static constexpr const std::uint8_t NET_ID(2);
#else
static constexpr const std::uint8_t NET_ID(42);
#endif

static const std::string ROUTER_VERSION{"0.9.42"};

static constexpr const std::uint8_t V(2);

/// @class Info
/// @brief Class for parsing and storing an I2P RouterInfo
class Info
{
 public:
  using identity_t = Identity;  //< Identity variant trait alias
  using date_t = std::uint64_t;  //< Date trait alias
  using address_t = Address;  //< Address trait alias
  using addresses_t = std::vector<address_t>;  //< Addresses trait alias
  using options_t = Mapping;  //< Options trait alias
  using transport_t = crypto::SecBytes;  //< Transport string trait alias
  using iv_t = crypto::AES::iv_t;  //< IV trait alias
  using signature_v = identity_t::signature_v;  //< Signature variant trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  using pointer = Info*;  //< Non-owning pointer trait alias
  using const_pointer = const Info*;  //< Const non-owning pointer trait alias
  using unique_ptr = std::unique_ptr<Info>;  //< Unique pointer trait alias
  using const_unique_ptr = std::unique_ptr<const Info>;  //< Const unique pointer trait alias
  using shared_ptr = std::shared_ptr<Info>;  //< Shared pointer trait alias
  using const_shared_ptr = std::shared_ptr<const Info>;  //< Const shared pointer trait alias

  enum : std::uint16_t
  {
    DateLen = 8,
    PeerLen = 0,  // always zero, see spec
    MaxTransportLen = 256,
    NTCP2TransportLen = 6,
    // size of field lengths
    PeerSizeLen = 1,
    AddressSizeLen = 1,
    MinLen = 440,
    MaxLen = 65515,  // max block size - flag (1)
    DefaultLen = 440,
  };

  enum : std::uint32_t
  {
    MinExpiration = 3600,  //< 1 hour, min (floodfill) expiration time, see NetDB spec
    MaxExpiration = 259200,  //< 72 hours, max expiration time, see NetDB spec
  };

  /// @brief Default RouterInfo ctor
  /// @detail Creates all new keys (identity + noise)
  Info() : identity_(), addresses_(), options_(), transport_(ntcp2_transport)
  {
    crypto::RandBytes(iv_);
    update_id_pubkey();
    update_iv();
    set_default_options();

    serialize();
  }

  /// @brief Create RouterInfo from identity, addresses, and options
  /// @param ident RouterIdentity (signs/verifies RouterInfo signature, encrypts/decrypts garlic)
  /// @param addrs RouterAddress(es) for contacting this RouterInfo
  /// @param opts Router mapping of RouterInfo options
  Info(identity_t ident, addresses_t addrs, options_t opts = options_t{})
      : identity_(std::forward<identity_t>(ident)),
        date_(tini2p::time::now_ms()),
        addresses_(std::forward<addresses_t>(addrs)),
        options_(std::forward<options_t>(opts)),
        transport_(ntcp2_transport)
  {
    const exception::Exception ex{"RouterInfo", __func__};

    const auto total_size = size();

    if (total_size < MinLen || total_size > MaxLen)
      ex.throw_ex<std::length_error>("invalid size.");

    // update Noise options entries
    crypto::RandBytes(iv_);
    update_id_pubkey();
    update_iv();
    set_default_options();

    serialize();
  }

  /// @brief Create RouterInfo from buffer 
  /// @param buf Buffer containing RouterIdentity bytes
  template <class Buffer>
  explicit Info(const Buffer& buf) : buf_(buf.begin(), buf.end())
  {
    deserialize();
  }

  /// @brief Serialize RouterInfo data members to buffer
  void serialize()
  {
    buf_.resize(size());

    BytesWriter<buffer_t> writer(buf_);

    identity_.serialize();
    writer.write_data(identity_.buffer());

    date_ = tini2p::time::now_ms();
    writer.write_bytes(date_, tini2p::Endian::Big);
    writer.write_bytes<std::uint8_t>(addresses_.size());

    for (auto& address : addresses_)
      {
        address.serialize();
        writer.write_data(address.buffer);
      }

    // write zero peer-size, see spec
    writer.write_bytes(std::uint8_t(0));

    options_.serialize();
    writer.write_data(options_.buffer());
    
    signature_ = identity_.Sign(writer.data(), writer.count());
    std::visit([&writer](const auto& s) { writer.write_data(s); }, signature_);
  }

  /// @brief Deserialize RouterInfo data members from buffer
  void deserialize()
  {
    const exception::Exception ex{"RouterInfo", __func__};

    BytesReader<buffer_t> reader(buf_);

    ProcessIdentity(reader);
    reader.read_bytes(date_, tini2p::Endian::Big);

    ProcessAddresses(reader, ex);
    reader.skip_bytes(PeerSizeLen);

    ProcessOptions(reader, ex);

    signature_ = std::visit(
        [&reader](const auto& val) {
          typename std::decay_t<decltype(val)>::signature_t sig;
          reader.read_data(sig);
          return signature_v(std::move(sig));
        },
        identity_.signing());

    transport_ = ntcp2_transport;
  }

  bool Verify() const
  {
    return identity_.Verify(
        buf_.data(), buf_.size() - identity_.sig_len(), signature_);
  }

  bool is_floodfill() const
  {
     const auto& caps = options_.entry(std::string("caps"));
     return std::find_if(caps.begin(), caps.end(), [](const auto& c) { return c == 'f'; }) != caps.end();
  }

  /// @brief Get a const reference to the RouterIdentity
  const identity_t& identity() const noexcept
  {
    return identity_;
  }

  /// @brief Get a non-const reference to the RouterIdentity
  identity_t& identity() noexcept
  {
    return identity_;
  }

  /// @brief Get a const reference to the identity hash
  const identity_t::hash_t& hash() const noexcept
  {
    return identity_.hash();
  }

  /// @brief Get a const reference to the creation date
  const date_t& date() const noexcept
  {
    return date_;
  }

  /// @brief Get a const reference to the RouterAddresses
  const addresses_t& addresses() const noexcept
  {
    return addresses_;
  }

  /// @brief Get a non-const reference to the RouterAddresses
  addresses_t& addresses() noexcept
  {
    return addresses_;
  }

  /// @brief Add an address to the RouterInfo
  Info& add_address(address_t address)
  {
    const exception::Exception ex{"Router: Info", __func__};

    check_address(address, ex);

    std::scoped_lock sgd(addresses_mutex_);

    addresses_.emplace_back(std::forward<address_t>(address));

    update_addresses();

    return *this;
  }

  /// @brief Get a host IP and port from router addresses
  /// @param prefer_v6 Flag to prefer IPv6 addresses
  /// @return Endpoint object containing IP address and port
  /// @throw On empty addresses
  /// @throw On invalid IP address (non-IPv4/6)
  /// @throw On invalid port
  decltype(auto) host(const bool prefer_v6 = true)
  {
    const exception::Exception ex{"Router: Info", __func__};

    std::unique_lock<std::mutex> l(addresses_mutex_);

    if (addresses_.empty())
      ex.throw_ex<std::logic_error>("empty router addresses.");

    for (const auto& address : addresses_)
      {
        auto ret = address.ToEndpoint();

        if ((ret.address().is_v6() && prefer_v6) || (ret.address().is_v4() && !prefer_v6))
          return ret;
      }

    return addresses_.front().ToEndpoint();
  }

  /// @brief Get a const reference to the options mapping
  const options_t& options() const noexcept
  {
    return options_;
  }

  /// @brief Get a non-const reference to the options mapping
  options_t& options() noexcept
  {
    return options_;
  }

  /// @brief Get a const reference to the transport style
  const transport_t& transport() const noexcept
  {
    return transport_;
  }

  /// @brief Get a non-const reference to the transport style
  transport_t& transport() noexcept
  {
    return transport_;
  }

  /// @brief Get a const reference to the RouterInfo signature
  const signature_v& signature() const noexcept
  {
    return signature_;
  }

  /// @brief Get a const reference to the Noise IV
  const iv_t& iv() const noexcept
  {
    return iv_;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Get the total size of the RouterInfo
  std::uint32_t size() const
  {
    std::uint32_t address_size = 0;
    for (const auto& address : addresses_)
      address_size += address.size();

    return identity_.size() + sizeof(date_) + AddressSizeLen + address_size
           + PeerSizeLen + options_.size() + identity_.sig_len();
  }

  /// @brief Compare equality with other RouterInfo
  std::uint8_t operator==(const Info& oth) const
  {  // attempt constant-time comparison
    const auto hash_eq = static_cast<std::uint8_t>(identity_.hash() == oth.identity_.hash());
    const auto addr_eq = static_cast<std::uint8_t>(addresses_ == oth.addresses_);
    const auto opt_eq = static_cast<std::uint8_t>(options_ == oth.options_);
    const auto trp_eq = static_cast<std::uint8_t>(transport_ == oth.transport_);

    return (hash_eq * addr_eq * opt_eq * trp_eq);
  }

  std::uint8_t operator==(const data::LeaseSet2& ls) const
  {
    return 0;
  }

 private:
  void ProcessIdentity(BytesReader<buffer_t>& reader)
  {
    auto& ident_buf = identity_.buffer();
    if (ident_buf.size() < Identity::DefaultLen)
      ident_buf.resize(Identity::DefaultLen);

    reader.read_data(ident_buf);
    identity_.deserialize();
    update_id_pubkey();
  }

  void ProcessAddresses(BytesReader<buffer_t>& reader, const exception::Exception& ex)
  {
    std::uint8_t num_addresses;
    reader.read_bytes(num_addresses);
    addresses_.clear();

    const std::string host_str("host"), i_str("i"), s_str("s"), v_str("v");

    const auto& s_bytes = static_cast<crypto::SecBytes>(identity_.crypto().pubkey.buffer());
    auto iv_bytes = static_cast<crypto::SecBytes>(iv_);

    for (std::uint8_t addr = 0; addr < num_addresses; ++addr)
      {
        Address address;

        // copy remaining buffer, we don't know address size yet
        const auto addr_begin = buf_.begin() + reader.count();

        if (addr_begin == buf_.end())
          ex.throw_ex<std::logic_error>("addresses overflows the router info.");

        address.buffer.insert(address.buffer.begin(), addr_begin, buf_.end());

        address.deserialize();

        auto& addr_opts = address.options;
        const auto& addr_s = crypto::Base64::Decode(addr_opts.entry(s_str));

        if (address.transport != ntcp2_transport)
          {
            ex.throw_ex<std::logic_error>(
                "invalid transport: " + std::string(address.transport.begin(), address.transport.end())
                + ", only support transport: " + std::string(ntcp2_transport.begin(), ntcp2_transport.end()));
          }

        if (addr_s != s_bytes)
          {
            ex.throw_ex<std::logic_error>(
                "invalid s public key: " + tini2p::bin_to_hex(addr_s, ex)
                + ", expected: " + tini2p::bin_to_hex(s_bytes, ex));
          }

        const auto& addr_v = addr_opts.entry(v_str);

        if (addr_v.size() != 1 || addr_v.front() != V)
          {
            ex.throw_ex<std::logic_error>(
                "invalid address version: " + tini2p::bin_to_hex(addr_v, ex) + ", expected: " + std::to_string(V));
          }

        if (addr_opts.has_entry(host_str))
          {
            const auto& addr_i = crypto::Base64::Decode(addr_opts.entry(i_str));

            if (addr_i != iv_bytes)
              {
                if (addr == 0)
                  {
                    iv_ = addr_i;
                    iv_bytes = static_cast<crypto::SecBytes>(iv_);
                  }
                else
                  {
                    ex.throw_ex<std::logic_error>(
                        "invalid address IV: " + tini2p::bin_to_hex(addr_i, ex)
                        + ", expected: " + tini2p::bin_to_hex(iv_, ex));
                  }
              }
          }

        reader.skip_bytes(address.size());
        addresses_.emplace_back(std::move(address));
      }
  }

  void ProcessOptions(BytesReader<buffer_t>& reader, const exception::Exception& ex)
  {
    if (!reader.gcount())
      ex.throw_ex<std::logic_error>("missing router options size, options, and signature.");

    options_t::size_type opt_size;
    reader.read_bytes(opt_size, tini2p::Endian::Big);

    if (opt_size)
      {
        reader.skip_back(sizeof(opt_size));
        options_.buffer().resize(sizeof(opt_size) + opt_size);
        reader.read_data(options_.buffer());

        options_.deserialize();

        if (options_.entry(std::string("s")).empty())
          ex.throw_ex<std::logic_error>("null Noise static key.");

        const auto version = options_.entry(std::string("v"));
        if (version.empty() || version.front() != V)
          {
            ex.throw_ex<std::logic_error>(
                "invalid NTCP2 version option: " + std::to_string(version.front())
                + ", expected: " + std::to_string(V));
          }
      }
  }

  void set_default_options()
  {
    options_.add(std::string("caps"), std::string("fOR"));
    options_.add(std::string("netID"), crypto::FixedSecBytes<1>{NET_ID});  // expected netID
    options_.add(std::string("router.version"), ROUTER_VERSION);  // match I2P
    options_.add(std::string("v"), crypto::FixedSecBytes<1>{V});  // NTCP2 version

    update_addresses();
  }

  void update_addresses()
  {
    std::string i_str("i"), s_str("s"), v_str("v");
    const auto& s_b64 = crypto::Base64::Encode(identity_.crypto().pubkey);
    const auto& i_b64 = crypto::Base64::Encode(iv_);
    const crypto::FixedSecBytes<1> v_byte{V};

    for (auto& address : addresses_)
      {
        auto& addr_opts = address.options;
        addr_opts.add(s_str, s_b64);
        addr_opts.add(v_str, v_byte);

        // if the address is "published", add the IV
        if (addr_opts.has_entry(std::string("host")))
          addr_opts.add(i_str, i_b64);
      }
  }

  void update_id_pubkey()
  {
    const std::string s_str("s");
    const auto& s = crypto::Base64::Encode(identity_.crypto().pubkey);

    options_.add(s_str, s);

    for (auto& addr : addresses_)
      addr.options.add(s_str, s);
  }

  void update_iv()
  {
    crypto::RandBytes(iv_);

    const std::string i_str("i");
    const auto& i = crypto::Base64::Encode(iv_);

    options_.add(i_str, i);

    for (auto& addr : addresses_)
      addr.options.add(i_str, i);
  }

  void check_address(const address_t& address, const exception::Exception& ex) const
  {
    const auto& host = address.options.entry(std::string("host"));

    if (host.empty())
      ex.throw_ex<std::logic_error>("null host");

    const auto& port = address.options.entry(std::string("port"));

    if (port.empty() || port.is_zero())
      ex.throw_ex<std::logic_error>("null port");

    std::uint16_t port_int;
    tini2p::read_bytes(port.data(), port_int);

    asio::error_code ec;
    asio::ip::make_address(std::string(host.begin(), host.end()), ec);

    if (ec)
      ex.throw_ex<std::runtime_error>("invalid host: " + ec.message());
  }

  identity_t identity_;
  date_t date_;
  addresses_t addresses_;
  std::mutex addresses_mutex_;
  options_t options_;
  transport_t transport_;
  signature_v signature_;

  // Noise specific
  iv_t iv_;

  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_ROUTER_INFO_H_
