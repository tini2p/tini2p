/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_ROUTER_ADDRESS_H_
#define SRC_DATA_ROUTER_ADDRESS_H_

#include <asio.hpp>

#include "src/crypto/sec_bytes.h"

#include "src/data/router/meta.h"
#include "src/data/router/mapping.h"

namespace tini2p
{
namespace data
{
struct Address
{
  enum Sizes : std::uint8_t
  {
    CostLen = 1,
    ExpirationLen = 8,
    TransportSizeLen = 1,
  };

  enum Costs : std::uint8_t
  {
    DefaultCost = 8,  // mid-way SSU(6) & NTCP(10)
  };

  enum Offsets : std::uint8_t
  {
    CostOffset,
    ExpirationOffset,
    TransportOffset = 9,
  };

  using cost_t = std::uint8_t;  //< Cost trait alias
  using expiration_t = std::uint64_t;  //< Expiration trait alias
  using transport_t = crypto::SecBytes;  //< Transport trait alias
  using options_t = Mapping;  //< Options trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  cost_t cost;
  expiration_t expiration;
  transport_t transport;
  options_t options;
  buffer_t buffer;

  Address() : cost(DefaultCost), expiration(0), transport(ntcp2_transport)
  {
    serialize();
  }

  /// @brief Create a router address from a host and port
  /// @param host Host for the router address
  /// @param port Port for the router address
  template <class Host>
  Address(const Host& host, const std::uint16_t port) : cost(DefaultCost), expiration(0), transport(ntcp2_transport)
  {  // set host and port options
    options.add(std::string("host"), host);
    options.add(std::string("port"), std::to_string(port));

    serialize();
  }

  decltype(auto) ToEndpoint() const
  {
    const exception::Exception ex{"Router: Info", __func__};

    const auto& host = options.entry(std::string("host"));
    if (host.empty())
      ex.throw_ex<std::length_error>("null host.");

    const auto& port_buf = options.entry(std::string("port"));
    if (port_buf.empty())
      ex.throw_ex<std::length_error>("null port.");

    return asio::ip::tcp::endpoint(
        asio::ip::make_address(std::string(host.begin(), host.end())),
        std::stoul(std::string(port_buf.begin(), port_buf.end())));
  }

  /// @brief Return the size of the mapping buffer
  std::uint32_t size() const noexcept
  {
    return CostLen + ExpirationLen + TransportSizeLen + transport.size() + options.size();
  }

  /// @brief Serialize the mapping to buffer
  void serialize()
  {
    buffer.resize(size()); 

    tini2p::BytesWriter<buffer_t> writer(buffer);

    check_params(tini2p::exception::Exception{"Router: Address", __func__});

    writer.write_bytes(cost);
    writer.write_bytes(expiration, tini2p::Endian::Big);
    writer.write_bytes<std::uint8_t>(transport.size());
    writer.write_data(transport);

    options.serialize();
    writer.write_data(options.buffer());
  }

  /// @brief Deserialize the mapping from buffer
  void deserialize()
  {
    const tini2p::exception::Exception ex{"Router: Address", __func__};

    tini2p::BytesReader<buffer_t> reader(buffer);

    reader.read_bytes(cost);
    reader.read_bytes(expiration, tini2p::Endian::Big);

    std::uint8_t transport_size;
    reader.read_bytes(transport_size);

    transport.resize(transport_size);
    reader.read_data(transport);

    check_params(ex);

    // read options size
    options_t::size_type opt_size;
    reader.read_bytes(opt_size, tini2p::Endian::Big);
    reader.skip_back(options_t::SizeLen);

    // read and deserialize options mapping
    auto& opt_buf = options.buffer();
    opt_buf.resize(options_t::SizeLen + opt_size);
    reader.read_data(opt_buf);
    options.deserialize();
  }

  /// @brief Equality comparison with another Address
  std::uint8_t operator==(const Address& oth) const
  {  // attempt constant-time comparison
    const auto& cst_eq = static_cast<std::uint8_t>(cost == oth.cost);
    const auto& exp_eq = static_cast<std::uint8_t>(expiration == oth.expiration);
    const auto& trp_eq = static_cast<std::uint8_t>(transport == oth.transport);
    const auto& opt_eq = static_cast<std::uint8_t>(options == oth.options);

    return (cst_eq * exp_eq * trp_eq * opt_eq);
  }

 private:
  void check_params(const tini2p::exception::Exception& ex) const
  {
    if (expiration)
      ex.throw_ex<std::runtime_error>("invalid expiration date.");

    if (transport != transport_t(ntcp_transport) && transport != transport_t(ntcp2_transport))
      ex.throw_ex<std::length_error>("invalid transport.");
  }
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_ROUTER_ADDRESS_H_
