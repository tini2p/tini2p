/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_ROUTER_LEASE_H_
#define SRC_DATA_ROUTER_LEASE_H_

#include "src/time.h"

#include "src/data/router/destination.h"

namespace tini2p
{
namespace data
{
/// @struct Lease
/// @detail Supports Lease2+ format
struct Lease2
{
  enum : std::uint32_t
  {
    Len = 40,  //< gw(32) + id(4) + exp(4), see spec
    SizeLen = 1,
    MinTunnelID = 1,
    MaxTunnelID = 0xFFFFFFFF,
    Timeout = 600,  //< in secs, 10 min. see spec
  };

  using destination_t = data::Destination;  //< Destination trait alias
  using tunnel_gw_t = destination_t::hash_t;  //< Tunnel gateway trait alias
  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using expiration_t = std::uint32_t;  //< Tunnel expiration trait alias
  using buffer_t = crypto::FixedSecBytes<Len>;  //< Buffer trait alias

  /// @brief Create a default Lease with null tunnel gateway + ID
  Lease2()
      : tunnel_gw_(),
        tunnel_id_(crypto::RandInRange(MinTunnelID, MaxTunnelID)),
        expiration_(time::now_s() + Timeout),
        buf_()
  {
  }

  /// @brief Create a Lease for a given tunnel gateway + random ID
  /// @param gateway IdentHash of the tunnel gateway for this Lease
  explicit Lease2(tunnel_gw_t&& gateway)
      : tunnel_gw_(std::forward<tunnel_gw_t>(gateway)),
        tunnel_id_(crypto::RandInRange(MinTunnelID, MaxTunnelID)),
        expiration_(time::now_s() + Timeout)
  {
    serialize();
  }

  /// @brief Create a Lease for a given tunnel gateway + ID
  /// @param gateway IdentHash of the tunnel gateway for this Lease
  /// @param id Tunnel ID for this Lease
  Lease2(tunnel_gw_t gateway, tunnel_id_t id)
      : tunnel_gw_(std::forward<tunnel_gw_t>(gateway)),
        tunnel_id_(std::forward<tunnel_id_t>(id)),
        expiration_(time::now_s() + Timeout)
  {
    serialize();
  }

  /// @brief Deserializing ctor, create Lease2 from a C-like buffer
  Lease2(const std::uint8_t* data, const std::size_t len)
  {
    tini2p::check_cbuf(data, len, Len, Len, {"Lease", __func__});

    std::copy_n(data, len, buf_.data());
    deserialize();
  }

  /// @brief Deserializing ctor, create a Lease2 from a secure buffer
  Lease2(buffer_t buf) : buf_(std::forward<buffer_t>(buf))
  {
    deserialize();
  }

  /// @brief Serialize Lease to buffer
  void serialize()
  {
    const exception::Exception ex{"Lease2", __func__};

    check_tunnel_gw(tunnel_gw_, ex);
    check_tunnel_id(tunnel_id_, ex);
    check_expiration(expiration_, ex);

    tini2p::BytesWriter<buffer_t> writer(buf_);
    writer.write_data(tunnel_gw_);
    writer.write_bytes(tunnel_id_, tini2p::Endian::Big);
    writer.write_bytes(expiration_, tini2p::Endian::Big);
  }

  /// @brief Deserialize Lease from buffer
  void deserialize()
  {
    const exception::Exception ex{"Lease2", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    tunnel_gw_t tunnel_gw;
    reader.read_data(tunnel_gw);
    check_tunnel_gw(tunnel_gw, ex);

    tunnel_id_t tunnel_id;
    reader.read_bytes(tunnel_id, tini2p::Endian::Big);
    check_tunnel_id(tunnel_id, ex);

    expiration_t expiration;
    reader.read_bytes(expiration, tini2p::Endian::Big);
    check_expiration(expiration, ex);

    tunnel_gw_.swap(tunnel_gw);
    tunnel_id_ = std::move(tunnel_id);
    expiration_ = std::move(expiration);
  }

  /// @brief Get a const reference to the tunnel gateway hash
  const tunnel_gw_t& tunnel_gateway() const noexcept
  {
    return tunnel_gw_;
  }

  /// @brief Set the tunnel gateway hash
  Lease2& tunnel_gateway(tunnel_gw_t tunnel_gw)
  {
    const exception::Exception ex{"Lease2", __func__};

    check_tunnel_gw(tunnel_gw, ex);

    tunnel_gw_ = std::forward<tunnel_gw_t>(tunnel_gw);

    return *this;
  }

  /// @brief Get a const reference to the tunnel ID
  const tunnel_id_t& tunnel_id() const noexcept
  {
    return tunnel_id_;
  }

  /// @brief Set the tunnel ID
  Lease2& tunnel_id(tunnel_id_t tunnel_id)
  {
    const exception::Exception ex{"Lease2", __func__};

    check_tunnel_id(tunnel_id, ex);

    tunnel_id_ = std::forward<tunnel_id_t>(tunnel_id);

    return *this;
  }

  /// @brief Get a const reference to the Lease expiration
  const expiration_t& expiration() const noexcept
  {
    return expiration_;
  }

  /// @brief Set the Lease expiration
  Lease2& expiration(expiration_t expiration)
  {
    const exception::Exception ex{"Lease2", __func__};

    check_expiration(expiration, ex);

    expiration_ = std::forward<expiration_t>(expiration);

    return *this;
  }

  /// @brief Check if the Lease expired
  Lease2& check_expiration()
  {
    const exception::Exception ex{"Lease2", __func__};

    check_expiration(expiration_, ex);

    return *this;
  }

  /// @brief Check if the Lease expired
  const Lease2& check_expiration() const
  {
    const exception::Exception ex{"Lease2", __func__};

    check_expiration(expiration_, ex);

    return *this;
  }

  /// @brief Get a const reference to the Lease buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the Lease buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Compare equality with other Lease
  std::uint8_t operator==(const Lease2& oth) const
  {  // attempt constant-time comparison
    const auto& tgw_eq = static_cast<std::uint8_t>(tunnel_gw_ == oth.tunnel_gw_);
    const auto& tid_eq = static_cast<std::uint8_t>(tunnel_id_ == oth.tunnel_id_);
    const auto& exp_eq = static_cast<std::uint8_t>(expiration_ == oth.expiration_);

    return (tgw_eq * tid_eq * exp_eq);
  }

 private:
  void check_tunnel_gw(const tunnel_gw_t& tunnel_gw, const exception::Exception& ex) const
  {
    if (tunnel_gw.is_zero())
      ex.throw_ex<std::logic_error>("null tunnel gateway hash.");
  }

  void check_tunnel_id(const tunnel_id_t& tunnel_id, const exception::Exception& ex) const
  {
    const auto& min_tunnel_id = tini2p::under_cast(MinTunnelID);
    const auto& max_tunnel_id = tini2p::under_cast(MaxTunnelID);

    if (tunnel_id < min_tunnel_id || tunnel_id > max_tunnel_id)
      {
        ex.throw_ex<std::logic_error>(
            "invalid tunnel ID: " + std::to_string(tunnel_id) + ", min: " + std::to_string(min_tunnel_id)
            + ", max: " + std::to_string(max_tunnel_id));
      }
  }

  void check_expiration(const expiration_t& expiration, const exception::Exception& ex) const
  {
    const auto& now = tini2p::time::now_s();
    if (expiration <= now)
      {
        ex.throw_ex<std::logic_error>(
            "lease expired, expiration: " + std::to_string(expiration_) + " now: " + std::to_string(now));
      }
  }

  tunnel_gw_t tunnel_gw_;
  tunnel_id_t tunnel_id_;
  expiration_t expiration_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_ROUTER_LEASE_H_
