/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_I2NP_GARLIC_DELIVERY_H_
#define SRC_I2NP_GARLIC_DELIVERY_H_

#include "src/bytes.h"

#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"

namespace tini2p
{
namespace data
{
class GarlicDeliveryInstructions
{
 public:
  enum struct DeliveryFlags : std::uint8_t
  {
    Local = 0x00,        //< 00000000, see spec
    Destination = 0x20,  //< 00100000, bit 5 set (0x01), see spec
    Router = 0x40,       //< 01000000, bit 6 set (0x02), see spec
    Tunnel = 0x60,       //< 01100000, bits 5 & 6 set (0x03), see spec
  };

  enum struct UnusedFlags : std::uint8_t
  {
    Encrypted = 0x80,    //< 10000000, unimplemented, never set, see spec
    Unencrypted = 0x00,  //< 00000000, see spec
    Delay = 0x10,        //< 00010000, bit 4 set (not set in practice), see spec
  };

  enum : std::uint8_t
  {
    FlagsLen = 1,
    HashLen = crypto::Sha256::DigestLen,
    LocalLen = FlagsLen,
    DestinationLen = FlagsLen + HashLen,
    RouterLen = DestinationLen,
    TunnelIDLen = 4,
    TunnelLen = DestinationLen + TunnelIDLen,
    DelayLen = 4,
    MinLen = FlagsLen,
    MaxLen = FlagsLen + HashLen + TunnelIDLen + DelayLen,
    ReservedMask = 0x0F,  //< 00001111, reserved bits, bitwise-AND to check, see spec
    DeliveryMask = 0x60,  //< 01100000, delivery bits, bitwise-AND to check, see spec
  };

  using flags_t = std::uint8_t;  //< Flags trait alias
  using delivery_t = DeliveryFlags;  //< Delivery type trait alias
  using hash_t = crypto::Sha256::digest_t; //< ToHash trait alias
  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using delay_t = std::uint32_t;  //< Delay trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor, creates local delivery GarlicDeliveryInstructions
  GarlicDeliveryInstructions() : flags_(), hash_(), tunnel_id_(), delay_()
  {
    serialize();
  }

  /// @brief Fully initializing ctor
  /// @details Provide delivery flags, "to hash", and tunnel ID (if tunnel delivery)
  /// @param flags Delivery flags for determining style of delivery (local, destination, router, tunnel)
  /// @param hash To hash of the endpoint to deliver the garlic message
  /// @param tunnel_id Optional reply tunnel ID (must be set for tunnel delivery)
  /// @throw Logic error if local delivery flag set (use default ctor instead)
  /// @throw Logic error if tunnel delivery flag set, and tunnel ID is zero
  GarlicDeliveryInstructions(const delivery_t& delivery, hash_t hash, const tunnel_id_t tunnel_id = 0)
      : flags_(tini2p::under_cast(delivery)), hash_(std::forward<hash_t>(hash)), tunnel_id_(tunnel_id), delay_()
  {
    const exception::Exception ex{"I2NP: GarlicDelivery", __func__};

    check_delivery(delivery, hash_, tunnel_id, ex);

    serialize();
  }

  /// @brief Create GarlicDeliveryInstructions from a secure buffer
  explicit GarlicDeliveryInstructions(buffer_t buf) : flags_(), hash_(), tunnel_id_(), delay_()
  {
    from_buffer(std::forward<buffer_t>(buf));
  }

  /// @brief Create GarlicDeliveryInstructions from a C-like buffer
  GarlicDeliveryInstructions(const std::uint8_t* data, const std::size_t len)
      : flags_(), hash_(), tunnel_id_(), delay_()
  {
    const exception::Exception ex{"I2NP: GarlicDelivery", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  };

  /// @brief Serialize GarlicDeliveryInstructions to buffer
  void serialize()
  {
    buf_.resize(size());

    tini2p::BytesWriter<buffer_t> writer(buf_);
    writer.write_bytes(flags_);

    const auto flags = delivery_flags();
    if (flags != DeliveryFlags::Local)
      writer.write_data(hash_);

    if (flags == DeliveryFlags::Tunnel)
      writer.write_bytes(tunnel_id_);

    // should never be used, but just in case
    if (flags_ & tini2p::under_cast(UnusedFlags::Delay))
      writer.write_bytes(delay_);
  }

  /// @brief Deserialize GarlicDeliveryInstructions from buffer
  void deserialize()
  {
    const exception::Exception ex{"I2NP: GarlicDelivery", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    reader.read_bytes(flags_);

    check_flags(flags_, ex);

    const auto flags = delivery_flags();
    if (flags != DeliveryFlags::Local)
      {
        reader.read_data(hash_);
        check_to_hash(flags, hash_, ex);
      }

    if (flags == DeliveryFlags::Tunnel)
      reader.read_bytes(tunnel_id_);

    if (flags_ & tini2p::under_cast(UnusedFlags::Delay))
      reader.read_bytes(delay_);

    buf_.resize(reader.count());
  }

  /// @brief Create GarlicDeliveryInstructions from buffer
  void from_buffer(buffer_t buf)
  {
    const exception::Exception ex{"I2NP: GarlicDelivery", __func__};

    const auto& buf_len = buf.size();
    if (buf_len < MinLen || buf_len > MaxLen)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid length: " + std::to_string(buf_len) + " min: " + std::to_string(tini2p::under_cast(MinLen))
            + " max: " + std::to_string(tini2p::under_cast(MaxLen)));
      }

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Resize the buffer based on the flags present
  void resize_from_flags(const flags_t& flags)
  {
    const exception::Exception ex{"I2NP: GarlicDelivery", __func__};

    check_flags(flags, ex);

    flags_ = flags;
    buf_.resize(size());
    buf_.shrink_to_fit();
  }

  /// @brief Get the total size of the GarlicDeliveryInstructions
  std::uint8_t size() const
  {  // attempt close to constant-time size calculation
     return size(flags_);
  }

  /// @brief Get the instructions size based on flags
  /// @note Useful for deserializing from buffer
  static std::uint8_t size(const flags_t& flags)
  {
    const exception::Exception ex{"I2NP: GarlicDelivery", __func__};

    check_flags(flags, ex);
    
    const auto delivery_flag = flags & DeliveryMask;

    const auto hash_size =
        static_cast<std::uint8_t>(delivery_flag != tini2p::under_cast(DeliveryFlags::Local)) * HashLen;
    const auto tun_id_size =
        static_cast<std::uint8_t>(delivery_flag == tini2p::under_cast(DeliveryFlags::Tunnel)) * TunnelIDLen;
    const auto delay_size =
        static_cast<std::uint8_t>((flags & tini2p::under_cast(UnusedFlags::Delay)) != 0) * DelayLen;

    return FlagsLen + hash_size + tun_id_size + delay_size;
  }

  /// @brief Get a const reference to the flags
  const flags_t& flags() const noexcept
  {
    return flags_;
  }

  /// @brief Get the garlic clove delivery flags
  DeliveryFlags delivery_flags() const
  {
    return static_cast<DeliveryFlags>(flags_ & tini2p::under_cast(DeliveryMask));
  }

  /// @brief Get a const reference to the "to hash"
  const hash_t& to_hash() const noexcept
  {
    return hash_;
  }

  /// @brief Set delivery instructions
  /// @param flag Delivery flag for the garlic clove
  /// @param to_hash Hash of the router, destination or tunnel endpoint/gateway
  /// @param tunnel_id Tunnel ID to deliver the garlic clove (only set for tunnel delivery)
  void set_delivery(const DeliveryFlags& flag, hash_t to_hash, const tunnel_id_t tunnel_id = 0)
  {
    const exception::Exception ex{"I2NP: GarlicDelivery", __func__};

    check_delivery(flag, to_hash, tunnel_id, ex);

    flags_ = tini2p::under_cast(flag);
    hash_ = std::forward<hash_t>(to_hash);

    if (flag == DeliveryFlags::Tunnel && tunnel_id)
      tunnel_id_ = tunnel_id;

    buf_.resize(size());
    buf_.shrink_to_fit();
  }

  /// @brief Get a const reference to the reply tunnel ID
  const tunnel_id_t& tunnel_id() const noexcept
  {
    return tunnel_id_;
  }

  /// @brief Get a const reference to the delay
  const delay_t& delay() const noexcept
  {
    return delay_;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another GarlicDeliveryInstructions
  std::uint8_t operator==(const GarlicDeliveryInstructions& oth) const
  {  // attempt constant time
    const auto flags_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);
    const auto hash_eq = static_cast<std::uint8_t>(hash_ == oth.hash_);
    const auto tunnel_eq = static_cast<std::uint8_t>(tunnel_id_ == oth.tunnel_id_);
    const auto delay_eq = static_cast<std::uint8_t>(delay_ == oth.delay_);

    return (flags_eq * hash_eq * tunnel_eq * delay_eq);
  }

 private:
  static void check_flags(const flags_t& flags, const exception::Exception& ex)
  {
    if (flags & tini2p::under_cast(ReservedMask))
      ex.throw_ex<std::logic_error>("invalid use of reserved flags");

    if (flags & tini2p::under_cast(UnusedFlags::Encrypted))
      ex.throw_ex<std::logic_error>("invalid use of unsupported encrypted flag");
  }

  void check_delivery(
      const DeliveryFlags& flag,
      const hash_t& hash,
      const tunnel_id_t tunnel_id = 0,
      const exception::Exception& ex = {"I2NP: GarlicDelivery"})
  {
    if (flag == DeliveryFlags::Tunnel && tunnel_id == 0)
      ex.throw_ex<std::logic_error>("must set tunnel ID for tunnel delivery.");

    check_to_hash(flag, hash, ex);
  }

  void check_to_hash(const DeliveryFlags& flag, const hash_t& hash, const exception::Exception& ex)
  {
    if (flag == DeliveryFlags::Local && !hash.is_zero())
      ex.throw_ex<std::invalid_argument>("non-null `to hash` set with Local delivery.");

    if (flag != DeliveryFlags::Local && hash.is_zero())
      ex.throw_ex<std::invalid_argument>("null `to hash` with non-Local delivery.");
  }

  flags_t flags_;
  hash_t hash_;
  tunnel_id_t tunnel_id_;
  delay_t delay_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_GARLIC_DELIVERY_H_
