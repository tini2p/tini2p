/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_I2NP_TUNNEL_MESSAGE_H_
#define SRC_DATA_I2NP_TUNNEL_MESSAGE_H_

#include <mutex>
#include <shared_mutex>

#include "src/crypto/sec_bytes.h"

#include "src/crypto/tunnel/aes.h"
#include "src/crypto/tunnel/chacha.h"

#include "src/data/i2np/tunnel_delivery.h"

namespace tini2p
{
namespace data
{
/// @brief Zero-byte for non-zero padding termination
const static crypto::FixedSecBytes<1> ZERO{0x00};

/// @class TunnelMessageBody
/// @brief Container for delivery instructions and message fragments
/// @detail Not thread-safe, memory-management left to owning class.
///     This is a simple container, mainly used for passing data from
///     a message fragmenter into TunnelMessages for packing.
/// @tparam TLayer Layer encryption type
struct TunnelMessageBody
{
  using first_delivery_t = data::FirstDelivery;  //< First fragment delivery instructions trait alias
  using follow_delivery_t = data::FollowDelivery;  //< Follow-on fragment delivery instructions trait alias
  using delivery_v = std::variant<first_delivery_t, follow_delivery_t>;  //< Delivery instructions variant trait alias
  using deliveries_t = std::vector<delivery_v>;  //< Delivery variant collection trait alias
  using fragment_t = crypto::SecBytes;  //< I2NP message fragment trait alias
  using fragments_t = std::vector<fragment_t>;  //< Fragment collection trait alias

  deliveries_t deliveries;
  fragments_t fragments;
};

/// @class TunnelMessage
/// @brief Class for containing and processing tunnel messages
/// @tparam TLayer Layer encryption type
template <
    class TLayer,
    typename = std::enable_if_t<
        std::is_same<TLayer, crypto::TunnelAES>::value || std::is_same<TLayer, crypto::TunnelChaCha>::value>>
class TunnelMessage
{
 public:
  enum
  {
    MessageLen = 1028,  //< Total tunnel message length, see spec
    MessageTrailingLen = 1044,  //< Message length with trailing IV/nonces, see spec
    ChaChaMessageLen = 1012,  //< ChaCha tunnel message length without inter-hop MAC
    TunnelIDLen = 4,
    IVLen = 16,  //< Tunnel IV length (if AES layer encryption), see spec
    ClearHeaderLen = 20,  //< Unencrypted header length
    TunnelNonceLen = 8,  //< Tunnel nonce length (if ChaCha layer encryption), see spec
    ObfsNonceLen = 8,  //< Obfs nonce length (if ChaCha layer encryption), see spec
    ChecksumLen = 4,  //< Checksum length, see spec
    PadDelimLen = 1,  //< Padding delimeter length, see spec
    MinHeaderLen = 25,  //< Minimum header length, see spec
    ChecksumPadDelimLen = 5,  //< Size of checksum and padding delimiter, see spec
    NullID = 0,
  };

  enum : std::uint8_t
  {
    PadDelim = 0,
  };

  enum struct DeserializeMode : std::uint8_t
  {
    Partial,
    Full,
  };

  using layer_t = TLayer;  //< Layer encryption trait alias
  using delivery_layer_t = data::Layer;  //< Delivery instruction layer encryption trait alias
  using aes_layer_t = crypto::TunnelAES;  //< AES layer encryption trait alias
  using chacha_layer_t = crypto::TunnelChaCha;  //< ChaCha layer encryption trait alias
  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using iv_t = crypto::FixedSecBytes<IVLen>;  //< IV trait alias
  using nonce_t = crypto::TunnelNonce;  //< Tunnel nonce trait alias
  using nonces_t = chacha_layer_t::Nonces;  //< TunnelNonces trait alias
  using checksum_t = std::uint32_t;  //< Checksum trait alias
  using padding_t = crypto::SecBytes;  //< Padding trait alias
  using body_t = TunnelMessageBody;  //< Tunnel message body trait alias
  using first_delivery_t = body_t::first_delivery_t;
  using follow_delivery_t = body_t::follow_delivery_t;
  using deliveries_t = body_t::deliveries_t;
  using fragment_t = body_t::fragment_t;
  using fragments_t = body_t::fragments_t;
  using size_type = std::uint16_t;  //< Size trait alias
  using body_buffer_t = crypto::SecBytes;  //< Body buffer trait alias
  using deserialize_mode_t = DeserializeMode;  //< Deserialize mode trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief First fragment ctor, creates a tunnel message from a first fragment
  /// @detail Caller is responsible for:
  ///     - Creating delivery instructions
  ///     - Splitting the I2NP message buffer into fragments
  /// @note Use TunnelMessageBuilder for packaging I2NP messages
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  TunnelMessage(tunnel_id_t tun_id, iv_t iv, first_delivery_t delivery, fragment_t fragment)
      : buf_(tini2p::under_cast(MessageLen))
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    check_tunnel_id(tun_id, ex);
    check_iv(iv, ex);
    check_fragment(delivery, fragment, ex);

    tunnel_id_ = std::forward<tunnel_id_t>(tun_id);
    iv_ = std::forward<iv_t>(iv);

    deliveries_.emplace_back(std::forward<first_delivery_t>(delivery));
    fragments_.emplace_back(std::forward<fragment_t>(fragment));

    serialize();
  }

  /// @brief First fragment ctor, creates a tunnel message from a first fragment
  /// @detail Caller is responsible for:
  ///     - Creating delivery instructions
  ///     - Splitting the I2NP message buffer into fragments
  /// @note Use TunnelMessageBuilder for packaging I2NP messages
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  TunnelMessage(
      tunnel_id_t tun_id,
      nonces_t nonces,
      first_delivery_t delivery,
      fragment_t fragment)
      : buf_(tini2p::under_cast(MessageLen))
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    check_tunnel_id(tun_id, ex);
    check_nonces(nonces, ex);
    check_fragment(delivery, fragment, ex);

    tunnel_id_ = std::forward<tunnel_id_t>(tun_id);
    nonces_ = std::forward<nonces_t>(nonces);

    deliveries_.emplace_back(std::forward<first_delivery_t>(delivery));
    fragments_.emplace_back(std::forward<fragment_t>(fragment));

    serialize();
  }

  /// @brief Follow-on fragment ctor, creates a tunnel message from a follow-on fragment
  /// @detail Caller is responsible for:
  ///     - Creating delivery instructions
  ///     - Splitting the I2NP message buffer into fragments
  /// @note Use TunnelMessageBuilder for packaging I2NP messages
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  TunnelMessage(tunnel_id_t tun_id, iv_t iv, follow_delivery_t delivery, fragment_t fragment)
      : buf_(tini2p::under_cast(MessageLen))
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    check_tunnel_id(tun_id, ex);
    check_iv(iv, ex);
    check_fragment(delivery, fragment, ex);

    tunnel_id_ = std::forward<tunnel_id_t>(tun_id);
    iv_ = std::forward<iv_t>(iv);

    deliveries_.emplace_back(std::forward<follow_delivery_t>(delivery));
    fragments_.emplace_back(std::forward<fragment_t>(fragment));

    serialize();
  }

  /// @brief Follow-on fragment ctor, creates a tunnel message from a follow-on fragment
  /// @detail Caller is responsible for:
  ///     - Creating delivery instructions
  ///     - Splitting the I2NP message buffer into fragments
  /// @note Use TunnelMessageBuilder for packaging I2NP messages
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  TunnelMessage(
      tunnel_id_t tun_id,
      nonces_t nonces,
      follow_delivery_t delivery,
      fragment_t fragment)
      : buf_(tini2p::under_cast(MessageLen))
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    check_tunnel_id(tun_id, ex);
    check_nonces(nonces, ex);
    check_fragment(delivery, fragment, ex);

    tunnel_id_ = std::forward<tunnel_id_t>(tun_id);
    nonces_ = std::forward<nonces_t>(nonces);

    deliveries_.emplace_back(std::forward<follow_delivery_t>(delivery));
    fragments_.emplace_back(std::forward<fragment_t>(fragment));

    serialize();
  }

  /// @brief Deserializing ctor, creates a TunnelMessage from a secure buffer
  /// @throws Length error for invalid buffer length
  /// @throws Various errors for invalid message parameters
  TunnelMessage(buffer_t buf, const deserialize_mode_t& mode = deserialize_mode_t::Full)
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    check_buffer(buf, ex);

    buf_ = std::forward<buffer_t>(buf);

    if (mode == deserialize_mode_t::Partial)
      {
        tini2p::BytesReader<buffer_t> reader(buf_);
        deserialize_header(reader, ex);
      }
    else if (mode == deserialize_mode_t::Full)
      deserialize();
    else
      ex.throw_ex<std::invalid_argument>("invalid deserialization mode.");
  }

  /// @brief Copy-ctor
  TunnelMessage(const TunnelMessage& oth)
      : tunnel_id_(oth.tunnel_id_),
        iv_(oth.iv_),
        nonces_(oth.nonces_),
        checksum_(oth.checksum_),
        padding_(oth.padding_),
        deliveries_(oth.deliveries_),
        fragments_(oth.fragments_),
        buf_(oth.buf_)
  {
  }

  /// @brief Move-ctor
  TunnelMessage(TunnelMessage&& oth)
      : tunnel_id_(std::move(oth.tunnel_id_)),
        iv_(std::move(oth.iv_)),
        nonces_(std::move(oth.nonces_)),
        checksum_(std::move(oth.checksum_)),
        padding_(std::move(oth.padding_)),
        deliveries_(std::move(oth.deliveries_)),
        fragments_(std::move(oth.fragments_)),
        buf_(std::move(oth.buf_))
  {
  }

  /// @brief Copy-assignment operator
  TunnelMessage& operator=(TunnelMessage oth)
  {
    tunnel_id_ = std::forward<tunnel_id_t>(oth.tunnel_id_);
    iv_ = std::forward<iv_t>(oth.iv_);
    nonces_ = std::forward<nonces_t>(oth.nonces_);
    checksum_ = std::forward<checksum_t>(oth.checksum_);
    padding_ = std::forward<padding_t>(oth.padding_);
    buf_ = std::forward<buffer_t>(oth.buf_);

    std::scoped_lock sgd(deliveries_mutex_, fragments_mutex_);

    deliveries_ = std::forward<deliveries_t>(oth.deliveries_);
    fragments_ = std::forward<fragments_t>(oth.fragments_);

    return *this;
  }

  /// @brief Serialize the TunnelMessage to the buffer
  void serialize()
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    check_tunnel_id(tunnel_id_, ex);

    if (std::is_same<layer_t, aes_layer_t>::value)
      check_iv(iv_, ex);
    else if (std::is_same<layer_t, chacha_layer_t>::value)
      check_nonces(nonces_, ex);
    else
      ex.throw_ex<std::logic_error>("invalid layer encryption.");

    std::shared_lock fs(fragments_mutex_, std::defer_lock);
    std::scoped_lock lgd(deliveries_mutex_, fs);

    const auto body_len = body_size(deliveries_, fragments_, ex);
    const auto& max_body_len = max_body_size();

    if (body_len < max_body_len)
      random_padding(padding_, max_body_len - body_len);

    const auto& message_len = tini2p::under_cast(MessageLen);
    const auto& iv_len = tini2p::under_cast(IVLen);

    // resize buffer for trailing IV for checksum calculation
    // if ChaCha layer encryption is used, overwrite the space reserved for inter-hop MACs
    if (std::is_same<layer_t, aes_layer_t>::value)
      buf_.resize(message_len + iv_len);

    tini2p::BytesWriter<buffer_t> writer(buf_);

    // write the tunnel ID to buffer
    writer.write_bytes(tunnel_id_, tini2p::Endian::Big);

    // write the IV/nonces to buffer
    if (std::is_same<layer_t, aes_layer_t>::value)
      writer.write_data(iv_);
    else if (std::is_same<layer_t, chacha_layer_t>::value)
      {
        writer.write_data(static_cast<nonce_t::buffer_t>(nonces_.tunnel_nonce));
        writer.write_data(static_cast<nonce_t::buffer_t>(nonces_.obfs_nonce));
      }
    else
      ex.throw_ex<std::logic_error>("invalid layer encryption.");

     // skip checksum for now, write after serializing remaining message
     const auto& checksum_len = tini2p::under_cast(ChecksumLen);
     writer.skip_bytes(checksum_len);
     writer.write_data(padding_);
     writer.write_bytes(tini2p::under_cast(PadDelim));

    for (size_type i = 0; i < deliveries_.size(); ++i)
      {  // write delivery instructions and fragments to body buffer for checksum
        // and to the message buffer
        std::visit(
            [&writer](auto& d) {
              d.serialize();
              writer.write_data(d.buffer());
            },
            deliveries_[i]);
        writer.write_data(fragments_[i]);
      }

    // write IV or nonces to buffer for checksum
    if (std::is_same<layer_t, aes_layer_t>::value)
      writer.write_data(iv_);
    else if (std::is_same<layer_t, chacha_layer_t>::value)
      {
        writer.write_data(static_cast<nonce_t::buffer_t>(nonces_.tunnel_nonce));
        writer.write_data(static_cast<nonce_t::buffer_t>(nonces_.obfs_nonce));
      }
    else
      ex.throw_ex<std::logic_error>("invalid layer encryption.");

    // get pointer to the beginning of the message body
    const auto body_ptr = buf_.data() + tini2p::under_cast(MinHeaderLen) + padding_.size();

    // calculate message checksum: checksum = Sha256(instructions + fragments || IV/nonces)
    crypto::Sha256::digest_t checksum;
    crypto::Sha256::Hash(checksum, body_ptr, body_len + iv_len);
    tini2p::read_bytes(checksum.data(), checksum_);

    // write four bytes of the checksum to the buffer
    writer.reset();
    writer.skip_bytes(tini2p::under_cast(ClearHeaderLen));
    writer.write_bytes(checksum_);

    // if AES layer encryption used, resize buffer to tunnel message length
    if (std::is_same<layer_t, aes_layer_t>::value)
      {
        buf_.resize(message_len);
        buf_.shrink_to_fit();
      }
  }

  /// @brief Deserialize the TunnelMessage from the buffer
  void deserialize()
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    check_buffer(buf_, ex);

    tini2p::BytesReader<buffer_t> reader(buf_);

    deserialize_header(reader, ex);

    // read the checksum, check later against calculated checksum
    reader.read_bytes(checksum_);

    // search for the padding delimiter
    const auto& pad_delim = tini2p::under_cast(PadDelim);
    const auto pad_start = buf_.begin() + reader.count();
    const auto buf_end = buf_.end();
    const auto it = std::find_if(pad_start, buf_end, [&pad_delim](const auto& b) { return b == pad_delim; });

    if (it == buf_end)
      ex.throw_ex<std::logic_error>("no padding delimiter found.");

    // read non-zero, random padding
    padding_.resize(it - pad_start);
    reader.read_data(padding_);

    // skip over the padding delimiter
    reader.skip_bytes(tini2p::under_cast(PadDelimLen));

    const auto& message_len = tini2p::under_cast(MessageLen);

    // save beginning of message body for checksum calculation
    const auto body_start = reader.count();
    // end at the full message length for AES layer encryption,
    //   or at the message length without the Poly1305 MAC for ChaCha layer encryption
    const auto body_end = std::is_same<layer_t, aes_layer_t>::value ? message_len 
                                                                    : tini2p::under_cast(ChaChaMessageLen);

    // resize buffer for trailing IV/nonces
    size_type message_with_trail(0);
    if (std::is_same<layer_t, aes_layer_t>::value)
      {
        message_with_trail = tini2p::under_cast(MessageTrailingLen);
        buf_.resize(message_with_trail);
      }
    else
      message_with_trail = message_len;

    tini2p::BytesWriter<buffer_t> writer(buf_);

    // skip to end of the message body
    writer.skip_bytes(body_end);
    
    // write trailing IV or nonces, based on layer encryption
    if (std::is_same<layer_t, aes_layer_t>::value)
      writer.write_data(iv_);
    else if (std::is_same<layer_t, chacha_layer_t>::value)
      {
        writer.write_data(static_cast<nonce_t::buffer_t>(nonces_.tunnel_nonce));
        writer.write_data(static_cast<nonce_t::buffer_t>(nonces_.obfs_nonce));
      }
    else
      ex.throw_ex<std::logic_error>("invalid layer encryption.");

    // calculate checksum: Sha256(delviery instructions + fragments || IV/nonces)
    crypto::Sha256::digest_t full_checksum;
    crypto::Sha256::Hash(full_checksum, buf_.data() + body_start, message_with_trail - body_start);

    if (std::is_same<layer_t, aes_layer_t>::value)
      {  // resize to TunnelMessage length (1028), and reclaim unused space
        buf_.resize(message_len);
        buf_.shrink_to_fit();
      }

    // read first four bytes of the checksum
    checksum_t calc_checksum;
    tini2p::read_bytes(full_checksum.data(), calc_checksum);

    // compare calculated checksum with claimed checksum
    if (checksum_ != calc_checksum)
      {
        ex.throw_ex<std::logic_error>(
            "invalid checksum: " + std::to_string(checksum_)
            + " does not match calculated checksum: " + std::to_string(calc_checksum));
      }

    deliveries_t deliveries;
    fragments_t fragments;

    // read delivery instructions and fragments until the end of the message
    while (reader.count() < body_end)
      {
        std::uint8_t flags;
        reader.read_bytes(flags);
        reader.skip_back(tini2p::under_cast(first_delivery_t::FlagsLen));

        if (flags & tini2p::under_cast(first_delivery_t::flags_t::FollowMask))
          {  // read follow-on instructions and fragment
            follow_delivery_t::buffer_t follow_buf;
            reader.read_data(follow_buf);
            follow_delivery_t follow(layer_encryption(), follow_buf);

            fragment_t fragment;
            fragment.resize(follow.fragment_size());

            reader.read_data(fragment);

            check_additional_fragment(deliveries, fragments, follow, fragment, ex);
            deliveries.emplace_back(std::move(follow));
            fragments.emplace_back(std::move(fragment));
          }
        else
          {  // read first instructions and fragment
            first_delivery_t::buffer_t first_buf;
            first_buf.resize(first_delivery_t::size(static_cast<typename first_delivery_t::flags_t>(flags)));
            reader.read_data(first_buf);
            first_delivery_t first(layer_encryption(), first_buf);

            fragment_t fragment;
            fragment.resize(first.fragment_size());
            reader.read_data(fragment);

            check_additional_fragment(deliveries, fragments, first, fragment, ex);
            deliveries.emplace_back(std::move(first));
            fragments.emplace_back(std::move(fragment));
          }
      }

    std::scoped_lock sgd(deliveries_mutex_, fragments_mutex_);
    deliveries_.swap(deliveries);
    fragments_.swap(fragments);
  }

  /// @brief Get a const reference to the hop's tunnel ID
  const tunnel_id_t& hop_tunnel_id() const noexcept
  {
    return tunnel_id_;
  }

  /// @brief Set the hop's tunnel ID
  void hop_tunnel_id(tunnel_id_t tunnel_id)
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    check_tunnel_id(tunnel_id, ex);

    tunnel_id_ = std::forward<tunnel_id_t>(tunnel_id);
  }

  /// @brief Get a const reference to the tunnel IV
  template<typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  const iv_t& iv() const noexcept
  {
    return iv_;
  }

  /// @brief Get a const reference to the tunnel nonce
  template<typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  const nonce_t& tunnel_nonce() const noexcept
  {
    return nonces_.tunnel_nonce;
  }

  /// @brief Get a const reference to the obfs nonce
  template<typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  const nonce_t& obfs_nonce() const noexcept
  {
    return nonces_.obfs_nonce;
  }

  /// @brief Get a const reference to the message nonces
  template<typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  const nonces_t& nonces() const noexcept
  {
    return nonces_;
  }

  /// @brief Get a const reference to the message IV
  /// @note Alias for templated use
  template<typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  const iv_t& randomizer() const noexcept
  {
    return iv_;
  }

  /// @brief Get a const reference to the message nonces
  /// @note Alias for templated use
  template<typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  const nonces_t& randomizer() const noexcept
  {
    return nonces_;
  }

  /// @brief Get a const reference to the collection of delivery instructions
  const deliveries_t& deliveries() const noexcept
  {
    return deliveries_;
  }

  /// @brief Get a const reference to the collection of message fragments
  const fragments_t& fragments() const noexcept
  {
    return fragments_;
  }

  /// @brief Add delivery instructions and an I2NP fragment to the tunnel message
  /// @tparam TDelivery Delivery instructions type
  /// @param delivery Tunnel delivery instructions for the I2NP fragment
  /// @param fragment I2NP fragment to add to the tunnel message
  /// @throws Length error if additional instructions and fragment exceed the max body length
  /// @throws Length error if the instructions fragment size doesn't match the fragment size
  template <
      class TDelivery,
      typename = std::enable_if_t<
          std::is_same<TDelivery, first_delivery_t>::value
          || std::is_same<TDelivery, follow_delivery_t>::value>>
  void add_fragment(TDelivery delivery, fragment_t fragment)
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    std::scoped_lock(deliveries_mutex_, fragments_mutex_);

    check_additional_fragment(deliveries_, fragments_, delivery, fragment, ex);

    deliveries_.emplace_back(std::forward<TDelivery>(delivery));
    fragments_.emplace_back(std::forward<fragment_t>(fragment));
  }

  /// @brief Get a const reference to the checksum
  const checksum_t& checksum() const noexcept
  {
    return checksum_;
  }

  /// @brief Get a const reference to the padding
  const padding_t& padding() const noexcept
  {
    return padding_;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Get the tunnel message length
  size_type size() const noexcept
  {
    return tini2p::under_cast(MessageLen);
  }

  /// @brief Get the size of the delivery instructions and fragments
  size_type body_size()
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    std::shared_lock dm(deliveries_mutex_, std::defer_lock), fm(fragments_mutex_, std::defer_lock);
    std::scoped_lock(dm, fm);

    return body_size(deliveries_, fragments_, ex);
  }

  /// @brief Get the maximum size of delivery instructions and message fragments
  size_type max_body_size() const noexcept
  {
    return tini2p::under_cast(layer_t::MessageBodyLen) - tini2p::under_cast(ChecksumPadDelimLen);
  }

  /// @brief Get the remaining size left for additional delivery instructions and message fragments
  size_type remaining_size()
  {
    return max_body_size() - body_size();
  }

  /// @brief Equality comparison with another TunnelMessage
  std::uint8_t operator==(const TunnelMessage& oth)
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    const auto& tun_eq = static_cast<std::uint8_t>(tunnel_id_ == oth.tunnel_id_);
    std::uint8_t layer_eq(0);
    if (std::is_same<layer_t, aes_layer_t>::value)
      layer_eq = static_cast<std::uint8_t>(iv_ == oth.iv_);
    else if (std::is_same<layer_t, chacha_layer_t>::value)
      layer_eq = static_cast<std::uint8_t>(nonces_ == oth.nonces_);
    else
      ex.throw_ex<std::logic_error>("invalid layer encryption.");

    const auto& check_eq = static_cast<std::uint8_t>(checksum_ == oth.checksum_);
    const auto& pad_eq = static_cast<std::uint8_t>(padding_ == oth.padding_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    std::shared_lock ds(deliveries_mutex_, std::defer_lock), fs(fragments_mutex_, std::defer_lock);
    std::scoped_lock sgd(ds, fs);

    const auto& dlv_eq = static_cast<std::uint8_t>(deliveries_ == oth.deliveries_);
    const auto& frag_eq = static_cast<std::uint8_t>(fragments_ == oth.fragments_);

    return (tun_eq * layer_eq * check_eq * pad_eq * buf_eq * dlv_eq * frag_eq);
  }

#ifdef TINI2P_TESTS
  /// @brief Test utility function for calculating the TunnelMessage checksum
  /// @param deliveries Collection of delivery instructions for the TunnelMessage
  /// @param fragments Collection of I2NP message fragments for the TunnelMessage
  /// @param iv Tunnel IV for message encryption
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, aes_layer_t>::value>>
  checksum_t CalculateChecksum(const deliveries_t& deliveries, const fragments_t& fragments, const iv_t& iv) const
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    buffer_t buf;
    buf.resize(body_size(deliveries, fragments, ex) + iv.size());

    tini2p::BytesWriter<buffer_t> writer(buf);
    const auto& dlv_len = deliveries.size();
    for (size_type i = 0; i < dlv_len; ++i)
      {
        std::visit([&writer](const auto& d) { writer.write_data(d.buffer()); }, deliveries[i]);
        writer.write_data(fragments[i]);
      }
    writer.write_data(iv);

    crypto::Sha256::digest_t check_digest;
    crypto::Sha256::Hash(check_digest, buf);

    checksum_t checksum;
    tini2p::read_bytes(check_digest.data(), checksum);

    return checksum;
  }

  /// @brief Test utility function for calculating the TunnelMessage checksum
  /// @param deliveries Collection of delivery instructions for the TunnelMessage
  /// @param fragments Collection of I2NP message fragments for the TunnelMessage
  /// @param tunnel_nonce Tunnel nonce for message encryption
  /// @param obfs_nonce Obfs nonce for tunnel nonce encryption
  template <typename L = layer_t, typename = std::enable_if_t<std::is_same<L, chacha_layer_t>::value>>
  checksum_t CalculateChecksum(const deliveries_t& deliveries, const fragments_t& fragments, const nonces_t& nonces)
      const
  {
    const exception::Exception ex{"TunnelMessage", __func__};

    buffer_t buf;
    buf.resize(body_size(deliveries, fragments, ex) + nonces.tunnel_nonce.size() + nonces.obfs_nonce.size());

    tini2p::BytesWriter<buffer_t> writer(buf);
    const auto& dlv_len = deliveries.size();

    for (size_type i = 0; i < dlv_len; ++i)
      {
        std::visit([&writer](const auto& d) { writer.write_data(d.buffer()); }, deliveries[i]);
        writer.write_data(fragments[i]);
      }

    writer.write_data(static_cast<nonce_t::buffer_t>(nonces.tunnel_nonce));
    writer.write_data(static_cast<nonce_t::buffer_t>(nonces.obfs_nonce));

    crypto::Sha256::digest_t check_digest;
    crypto::Sha256::Hash(check_digest, buf);

    checksum_t checksum;
    tini2p::read_bytes(check_digest.data(), checksum);

    return checksum;
  }
#endif

 private:
  void deserialize_header(tini2p::BytesReader<buffer_t>& reader, const exception::Exception& ex)
  {
    // read and check the tunnel ID
    tunnel_id_t tunnel_id;
    reader.read_bytes(tunnel_id, tini2p::Endian::Big);
    check_tunnel_id(tunnel_id, ex);
    tunnel_id_ = std::move(tunnel_id);

    if (std::is_same<layer_t, aes_layer_t>::value)
      {  // read and check the tunnel IV
        iv_t iv;
        reader.read_data(iv);
        check_iv(iv, ex);
        iv_ = std::move(iv);
      }
    else if (std::is_same<layer_t, chacha_layer_t>::value)
      {  // read and check the tunnel nonces
        nonce_t::buffer_t tun_nonce_buf;
        reader.read_data(tun_nonce_buf);

        nonce_t::buffer_t obfs_nonce_buf;
        reader.read_data(obfs_nonce_buf);
        nonces_t nonces{nonce_t(std::move(tun_nonce_buf)), nonce_t(std::move(obfs_nonce_buf))};

        check_nonces(nonces, ex);
        nonces_ = std::move(nonces);
      }
    else
      ex.throw_ex<std::logic_error>("invalid layer encryption.");
  }

  void check_tunnel_id(const tunnel_id_t& tun_id, const exception::Exception& ex) const
  {
    if (tun_id == tini2p::under_cast(NullID))
      ex.throw_ex<std::invalid_argument>("null tunnel ID.");
  }

  void check_iv(const iv_t& iv, const exception::Exception& ex) const
  {
    if (iv.is_zero())
      ex.throw_ex<std::invalid_argument>("null tunnel IV.");
  }

  void check_nonces(const nonces_t& nonces, const exception::Exception& ex) const
  {
    if (nonces.tunnel_nonce.is_zero())
      ex.throw_ex<std::invalid_argument>("null tunnel nonce.");

    if (nonces.obfs_nonce.is_zero())
      ex.throw_ex<std::invalid_argument>("null obfs nonce.");

    if (nonces.tunnel_nonce == nonces.obfs_nonce)
      ex.throw_ex<std::logic_error>("tunnel nonce and obfs nonce are equal, the must be unique.");
  }

  template <
      class TDelivery,
      typename = std::enable_if_t<
          std::is_same<TDelivery, first_delivery_t>::value || std::is_same<TDelivery, follow_delivery_t>::value>>
  void check_fragment(const TDelivery& delivery, const fragment_t& fragment, const exception::Exception& ex) const
  {
    check_layer(delivery.layer(), ex);

    const auto& dlv_frag_len = delivery.fragment_size();
    const auto& frag_len = fragment.size();

    if (dlv_frag_len != frag_len)
      {
        ex.throw_ex<std::length_error>(
            "delivery instructions fragment size: " + std::to_string(dlv_frag_len)
            + " does not match fragment size: " + std::to_string(frag_len));
      }
  }

  template <
      class TDelivery,
      typename = std::enable_if_t<
          std::is_same<TDelivery, first_delivery_t>::value || std::is_same<TDelivery, follow_delivery_t>::value>>
  void check_additional_fragment(
      const deliveries_t& deliveries,
      const fragments_t& fragments,
      const TDelivery& delivery,
      const fragment_t& fragment,
      const exception::Exception& ex)
  {
    check_fragment(delivery, fragment, ex);

    const auto& body_len = body_size(deliveries, fragments, ex);
    const auto& new_frag_len = delivery.size() + fragment.size();
    const auto& max_body_len = max_body_size();

    if (body_len + new_frag_len > max_body_len)
      {
        ex.throw_ex<std::length_error>(
            "adding new instructions + fragment length: " + std::to_string(new_frag_len) + " to current body length: "
            + std::to_string(body_len) + " exceeds max body length: " + std::to_string(max_body_len));
      }

    const auto end = deliveries.end();
    const typename body_t::delivery_v dlv_var(delivery);
    const auto it = std::find_if(deliveries.begin(), end, [&dlv_var](const auto& d) { return d == dlv_var; });

    if (it != end)
      ex.throw_ex<std::logic_error>("adding duplicate delivery instructions.");
  }

  void check_buffer(const buffer_t& buf, const exception::Exception& ex) const
  {
    const auto& buf_len = buf.size();
    const auto& msg_len = tini2p::under_cast(MessageLen);

    if (buf_len != msg_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + " expected length: " + std::to_string(msg_len));
      }
  }

  size_type body_size(const deliveries_t& deliveries, const fragments_t& fragments, const exception::Exception& ex)
      const
  {
    const auto& dlv_len = deliveries.size();
    const auto& frag_len = fragments.size();

    if (dlv_len != frag_len)
      {
        ex.throw_ex<std::length_error>(
            "number of delivery instructions: " + std::to_string(dlv_len)
            + " does not match the number of fragments: " + std::to_string(frag_len));
      }

    size_type body_len(0);
    for (size_type i = 0; i < dlv_len; ++i)
      {
        const auto& frag = fragments[i];
        body_len += std::visit([&frag](const auto& d) { return d.size() + frag.size(); }, deliveries[i]);
      }
    return body_len;
  }

  void random_padding(padding_t& padding, const size_type& pad_len)
  {
    padding = crypto::RandBuffer(pad_len);

    const std::uint8_t null_byte(0);
    const std::uint8_t min_pad_byte(1);
    const std::uint8_t max_pad_byte(255);

    for (auto& pad : padding)
      {  // overwrite null bytes with non-zero value
        if (pad == null_byte)
          pad = crypto::RandInRange(min_pad_byte, max_pad_byte);
      }
  }

  void check_layer(const delivery_layer_t& layer, const exception::Exception& ex) const
  {
    if (layer != layer_encryption())
      ex.throw_ex<std::invalid_argument>("invalid layer encryption.");
  }

  delivery_layer_t layer_encryption() const noexcept
  {
    return std::is_same<layer_t, aes_layer_t>::value ? delivery_layer_t::AES : delivery_layer_t::ChaCha;
  }

  tunnel_id_t tunnel_id_;
  iv_t iv_;
  nonces_t nonces_;
  checksum_t checksum_;
  padding_t padding_;

  deliveries_t deliveries_;
  std::shared_mutex deliveries_mutex_;

  fragments_t fragments_;
  std::shared_mutex fragments_mutex_;

  buffer_t buf_;
};

/// @class TunnelMessageFragmenter 
/// @brief Class for processing I2NP message buffers into TunnelMessage delivery instructions and fragments
/// @detail Useful for outbound and inbound gateway classes to fragment I2NP messages
template <
    class TLayer,
    typename = std::enable_if_t<
        std::is_same<TLayer, crypto::TunnelAES>::value || std::is_same<TLayer, crypto::TunnelChaCha>::value>>
class TunnelMessageFragmenter
{
 public:
  using layer_t = TLayer;  //< Layer encryption type trait alias
  using delivery_layer_t = data::Layer;  //< Delivery instructions layer encryption trait alias
  using aes_layer_t = crypto::TunnelAES;  //< AES layer encryption trait alias
  using chacha_layer_t = crypto::TunnelChaCha;  //< ChaCha layer encryption trait alias
  using first_delivery_t = FirstDelivery;  //< First delivery trait alias
  using follow_delivery_t = FollowDelivery;  //< Follow-on delivery trait alias
  using first_flags_t = first_delivery_t::flags_t;  //< FirstDelivery flags trait alias
  using delivery_type_t = first_delivery_t::Delivery;  //< Delivery type trait alias
  using tunnel_id_t = first_delivery_t::tunnel_id_t;  //< Tunnel ID trait alias
  using message_id_t = first_delivery_t::message_id_t;  //< Message ID trait alias
  using to_hash_t = first_delivery_t::to_hash_t;  //< Deliver to hash trait alias
  using frag_num_t = follow_delivery_t::frag_num_t;  //< Follow-on fragment number trait alias
  using is_last_t = follow_delivery_t::is_last_t;  //< Follow-on is last fragment trait alias
  using message_body_t = TunnelMessageBody;  //< Tunnnel message trait alias
  using fragment_t = crypto::SecBytes;  //< Fragment trait alias
  using i2np_buffer_t = crypto::SecBytes;  //< I2NP message buffer

  /// @brief Creates delivery instructions and message fragments from an I2NP message buffer
  /// @param delivery_type Type of delivery for the message (Local, Router, Tunnel)
  /// @param tunnel_id Tunnel ID for the end-to-end message recipient
  static message_body_t FragmentI2NP(
      delivery_type_t delivery_type,
      const i2np_buffer_t& i2np_buf,
      std::optional<to_hash_t> to_hash = std::nullopt,
      std::optional<tunnel_id_t> tunnel_id = std::nullopt)
  {
    const exception::Exception& ex{"TunnelMessageFragmenter", __func__};

    check_fragments(delivery_type, i2np_buf, ex);

    if (delivery_type == delivery_type_t::Local && to_hash != std::nullopt)
      ex.throw_ex<std::invalid_argument>("local delivery instructions do not use a to hash.");

    if (delivery_type == delivery_type_t::Local && tunnel_id != std::nullopt)
      ex.throw_ex<std::invalid_argument>("local delivery instructions do not use a tunnel ID.");

    if ((delivery_type == delivery_type_t::Router || delivery_type == delivery_type_t::Tunnel) && to_hash == std::nullopt)
      ex.throw_ex<std::invalid_argument>("router and tunnel delivery instructions require a to hash.");

    if (delivery_type == delivery_type_t::Tunnel && (to_hash == std::nullopt || tunnel_id == std::nullopt))
      ex.throw_ex<std::invalid_argument>("tunnel delivery instructions require a to hash and tunnel ID.");

    message_body_t body;

    const auto& layer = layer_encryption();
    const auto& i2np_len = i2np_buf.size();
    const auto& max_first_frag_len =
        first_delivery_t::max_fragment_size(static_cast<first_flags_t>(delivery_type), layer);

    first_flags_t first_flags;
    fragment_t first_fragment;
    first_delivery_t::mode_t mode(first_delivery_t::mode_t::Unfragmented);
    std::optional<message_id_t> msg_id;

    tini2p::BytesReader<i2np_buffer_t> reader(i2np_buf);

    // read first fragment and create random message ID if fragmented
    if (i2np_len > max_first_frag_len)
      {
        first_flags = static_cast<first_flags_t>(
            tini2p::under_cast(delivery_type) | tini2p::under_cast(first_flags_t::Fragmented));

        first_fragment.resize(first_delivery_t::max_fragment_size(first_flags, layer));
        reader.read_data(first_fragment);
        msg_id.emplace(crypto::RandInRange());
        mode = first_delivery_t::mode_t::Fragmented;
      }
    else
      {
        first_fragment.resize(i2np_len);
        reader.read_data(first_fragment);
      }

    // create delivery instructions based on delivery type
    if (delivery_type == delivery_type_t::Local)
      body.deliveries.emplace_back(first_delivery_t(layer, mode, first_fragment.size(), msg_id));
    else if (delivery_type == delivery_type_t::Router)
      body.deliveries.emplace_back(first_delivery_t(layer, mode, first_fragment.size(), *to_hash, msg_id));
    else if (delivery_type == delivery_type_t::Tunnel)
      body.deliveries.emplace_back(first_delivery_t(layer, mode, first_fragment.size(), *tunnel_id, *to_hash, msg_id));
    else
      ex.throw_ex<std::invalid_argument>("invalid delivery type.");

    // add first fragment to message body
    body.fragments.emplace_back(std::move(first_fragment));

    // for any remaining message bytes, create follow-on delivery instructions and fragments
    // increment the fragment number for each additional fragment
    frag_num_t frag_num(1);
    while (reader.gcount())
      {
        const auto& remaining = reader.gcount();
        const auto& max_follow_len = follow_delivery_t::max_fragment_size(layer);
        fragment_t follow_fragment;
        is_last_t is_last(is_last_t::False);

        if (remaining > max_follow_len)
          follow_fragment.resize(max_follow_len);
        else
          {
            follow_fragment.resize(remaining);
            is_last = is_last_t::True;
          }

        reader.read_data(follow_fragment);
        body.deliveries.emplace_back(follow_delivery_t(layer, frag_num, *msg_id, follow_fragment.size(), is_last));
        body.fragments.emplace_back(std::move(follow_fragment));

        ++frag_num;
      }

    return body;
  }

 private:
  static void check_fragments(const delivery_type_t& delivery_type, const i2np_buffer_t& i2np_buf, const exception::Exception& ex)
  {
    const auto& msg_len = i2np_buf.size();

    const auto& layer = layer_encryption();
    const auto& flags =
        static_cast<first_flags_t>(tini2p::under_cast(first_flags_t::Fragmented) | tini2p::under_cast(delivery_type));

    const auto& max_first_len = first_delivery_t::max_fragment_size(flags, layer);
    const auto& max_follow_len =
        tini2p::under_cast(follow_delivery_t::MaxFragmentNum) * follow_delivery_t::max_fragment_size(layer);

    if (msg_len > max_first_len + max_follow_len)
      {
        ex.throw_ex<std::length_error>(
            "message length: " + std::to_string(msg_len) + " exceeds max first fragment length: "
            + std::to_string(max_first_len) + " + total max follow-on length: " + std::to_string(max_follow_len));
      }
  }

  static delivery_layer_t layer_encryption()
  {
    const exception::Exception& ex{"TunnelMessageFragmenter", __func__};

    delivery_layer_t layer(delivery_layer_t::AES);

    if (std::is_same<layer_t, aes_layer_t>::value)
      layer = delivery_layer_t::AES;
    else if (std::is_same<layer_t, chacha_layer_t>::value)
      layer = delivery_layer_t::ChaCha;
    else
      ex.throw_ex<std::logic_error>("invalid layer encryption.");

    return layer;
  }
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_I2NP_TUNNEL_MESSAGE_H_
