/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_I2NP_GARLIC_H_
#define SRC_I2NP_GARLIC_H_

#include "src/data/i2np/garlic_clove.h"

namespace tini2p
{
namespace data
{
/// @class Garlic
/// @brief Class for creating and processing Garlic I2NP messages
class Garlic
{
 public:
  enum : std::uint32_t
  {
    NumLen = 1,
    MessageIDLen = 4,
    ExpirationLen = 8,
    DefaultExpiration = 30000,  //< 5 min. in milliseconds, same as cloves
    MetadataLen = 16,  //< Num(1) + NullCert(3) + MsgID(4) + Expiration(8)
    MinCloves = 1,
    MaxCloves = 255,  //< uint8_max
    MinClovesLen = 32,  //< GarlicClove::Min(32)
    MaxClovesLen = 62675,  //< MaxLen(62696) - Metadata(21)
    MinMessageID = 1,
    MaxMessageID = 0xFFFFFFFF,
    MinLen = 12,  //< min ECIES ExistingSession + GarlicHeader, see spec
    MaxLen = 62696,  //< MaxI2NP - I2NPHeader, see spec
  };

  using num_t = std::uint8_t;  //< Number of cloves trait alias
  using clove_t = data::GarlicClove;  //< Garlic clove trait alias
  using cloves_t = std::vector<clove_t>;  //< Garlic clove collection trait alias
  using message_id_t = std::uint32_t;  //< MessageID trait alias
  using certificate_t = data::Certificate;  //< Router certificate trait alias
  using expiration_t = std::uint64_t;  //< Expiration trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor
  Garlic()
      : cloves_(),
        cert_(),
        msg_id_(crypto::RandInRange(MinMessageID, MaxMessageID)),
        expiration_(time::now_ms() + DefaultExpiration),
        buf_()
  {
  }

  /// @brief Fully-initializing ctor, create a Garlic message from a collection of GarlicCloves
  Garlic(cloves_t cloves, message_id_t msg_id = 0)
      : cert_(),
        msg_id_(msg_id == 0 ? crypto::RandInRange(MinMessageID, MaxMessageID) : msg_id),
        expiration_(time::now_ms() + DefaultExpiration),
        buf_()
  {
    const exception::Exception ex{"I2NP: Garlic", __func__};

    check_cloves(cloves, ex);

    cloves_ = std::forward<cloves_t>(cloves);

    serialize();
  }

  /// @brief Deserializing ctor, create a Garlic message from a secure buffer
  explicit Garlic(buffer_t buf) : cloves_(), cert_(), msg_id_(), expiration_(), buf_()
  {
    const exception::Exception ex{"I2NP: Garlic", __func__};

    check_buffer(buf, ex);
    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Copy-ctor
  Garlic(const Garlic& oth)
  {
    const exception::Exception ex{"I2NP: Garlic", __func__};

    check_cloves(oth.cloves_, ex);
    check_cert_header(oth.cert_.cert_type, oth.cert_.length, MetadataLen - NumLen, ex); 
    check_message_id(oth.msg_id_, ex);
    check_expiration(oth.expiration_, ex);
    check_buffer(oth.buf_, ex);

    cloves_ = oth.cloves_;
    cert_ = oth.cert_;
    msg_id_ = oth.msg_id_;
    expiration_ = oth.expiration_;
    buf_ = oth.buf_;
  }

  /// @brief Move-ctor
  Garlic(Garlic&& oth)
  {
    const exception::Exception ex{"I2NP: Garlic", __func__};

    check_cloves(oth.cloves_, ex);
    check_cert_header(oth.cert_.cert_type, oth.cert_.length, MetadataLen - NumLen, ex); 
    check_message_id(oth.msg_id_, ex);
    check_expiration(oth.expiration_, ex);
    check_buffer(oth.buf_, ex);

    cloves_ = std::move(oth.cloves_);
    cert_ = std::move(oth.cert_);
    msg_id_ = std::move(oth.msg_id_);
    expiration_ = std::move(oth.expiration_);
    buf_ = std::move(oth.buf_);
  }

  /// @brief Forwarding-assignment operator
  Garlic& operator=(Garlic oth)
  {
    const exception::Exception ex{"I2NP: Garlic", __func__};

    check_cloves(oth.cloves_, ex);
    check_cert_header(oth.cert_.cert_type, oth.cert_.length, MetadataLen - NumLen, ex); 
    check_message_id(oth.msg_id_, ex);
    check_expiration(oth.expiration_, ex);
    check_buffer(oth.buf_, ex);

    cert_ = std::forward<certificate_t>(oth.cert_);
    msg_id_ = std::forward<message_id_t>(oth.msg_id_);
    expiration_ = std::forward<expiration_t>(oth.expiration_);
    buf_ = std::forward<buffer_t>(oth.buf_);

    std::scoped_lock sgd(cloves_mutex_);
    cloves_ = std::forward<cloves_t>(oth.cloves_);

    return *this;
  }

  /// @brief Serialize the Garlic message to buffer
  Garlic& serialize()
  {
    const exception::Exception ex{"I2NP: Garlic", __func__};

    std::scoped_lock sgd(cloves_mutex_);

    check_cloves(cloves_, ex);

    num_t num = cloves_.size();

    buf_.resize(size(cloves_, cert_));

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_bytes(num);

    for (auto& clove : cloves_)
      {
        clove.serialize();
        writer.write_data(clove.buffer());
      }

    cert_.serialize();
    writer.write_data(cert_.buffer);

    writer.write_bytes(msg_id_, tini2p::Endian::Big);
    writer.write_bytes(expiration_, tini2p::Endian::Big);

    return *this;
  }

  /// @brief Deserialize the Garlic message from buffer
  Garlic& deserialize()
  {
    const exception::Exception ex{"I2NP: Garlic", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    num_t num(0);
    reader.read_bytes(num);

    if (num == 0)
      ex.throw_ex<std::logic_error>("must have at least one clove");

    cloves_t cloves;
    cloves.reserve(num);

    // Deserialize specified number of cloves
    for (num_t i = 0; i < num; ++i)
      DeserializeClove(reader, cloves, ex);

    certificate_t cert;
    DeserializeCertificate(reader, cert, ex);

    message_id_t msg_id(0);
    reader.read_bytes(msg_id, tini2p::Endian::Big);

    check_message_id(msg_id, ex);

    expiration_t expiration(0);
    reader.read_bytes(expiration, tini2p::Endian::Big);

    check_expiration(expiration, ex);

    // Move temporary variables into data members
    cert_ = std::move(cert);
    msg_id_ = std::move(msg_id);
    expiration_ = std::move(expiration);

    {  // lock cloves
      std::scoped_lock sgd(cloves_mutex_);
      cloves_.swap(cloves);
    }  // end-cloves-lock

    // reclaim unused space
    buf_.resize(reader.count());
    buf_.shrink_to_fit();

    return *this;
  }

  /// @brief Get the size of the Garlic message
  std::uint16_t size()
  {
    std::shared_lock cs(cloves_mutex_, std::defer_lock);
    std::scoped_lock sgd(cs);

    return size(cloves_, cert_);
  }

  /// @brief Get whether the cloves are empty
  std::uint8_t empty_cloves()
  {
    std::shared_lock cs(cloves_mutex_, std::defer_lock);
    std::scoped_lock sgd(cs);

    return static_cast<std::uint8_t>(cloves_.empty());
  }

  /// @brief Get whether the message has the max number of cloves
  std::uint8_t has_max_cloves()
  {
    std::shared_lock cs(cloves_mutex_, std::defer_lock);
    std::scoped_lock sgd(cs);

    return static_cast<std::uint8_t>(cloves_.size() == tini2p::under_cast(MaxCloves));
  }

  /// @brief Get whether the message has the max cloves length
  std::uint8_t has_max_cloves_len()
  {
    std::shared_lock cs(cloves_mutex_, std::defer_lock);
    std::scoped_lock sgd(cs);

    return static_cast<std::uint8_t>(cloves_size(cloves_) == tini2p::under_cast(MaxClovesLen));
  }

  /// @brief Add a clove to the message
  /// @param clove GarlicClove to add
  /// @throws Logic error if message has max number of cloves
  /// @throws Length error if clove exceeds the max cloves length
  Garlic& add_clove(clove_t clove)
  {
    const exception::Exception ex{"I2NP: Garlic", __func__};

    std::scoped_lock sgd(cloves_mutex_);

    const auto& num_cloves = cloves_.size();
    const auto& max_cloves = tini2p::under_cast(MaxCloves);

    if (num_cloves == max_cloves)
      ex.throw_ex<std::logic_error>("message has max number of cloves: " + std::to_string(max_cloves));

    const auto& clove_len = clove.size();
    const auto& cloves_len = cloves_size(cloves_);
    const auto& max_len = tini2p::under_cast(MaxClovesLen);

    if (clove_len + cloves_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "max clove length exceeded, clove length: " + std::to_string(clove_len) + ", current cloves length: "
            + std::to_string(cloves_len) + ", max cloves length: " + std::to_string(max_len));
      }

    cloves_.emplace_back(std::forward<clove_t>(clove));

    return *this;
  }

  /// @brief Set the message cloves
  Garlic& cloves(cloves_t cloves)
  {
    const exception::Exception ex{"I2NP: Garlic", __func__};

    check_cloves(cloves, ex);

    cloves_ = std::forward<cloves_t>(cloves);

    return *this;
  }

  /// @brief Extract the GarlicCloves from the message
  cloves_t extract_cloves()
  {
    std::scoped_lock sgd(cloves_mutex_);

    auto cloves = std::move(cloves_);

    cloves_.reserve(MaxCloves);

    return cloves;
  }

  /// @brief Get a const reference to the message certificate
  const certificate_t& certificate() const noexcept
  {
    return cert_;
  }

  /// @brief Get a const reference to the message ID
  const message_id_t& message_id() const noexcept
  {
    return msg_id_;
  }

  /// @brief Set the message ID
  Garlic& message_id(message_id_t msg_id)
  {
    const exception::Exception ex{"I2NP: Garlic", __func__};

    check_message_id(msg_id, ex);

    msg_id_ = std::forward<message_id_t>(msg_id);

    return *this;
  }

  /// @brief Get a const reference to the message expiration
  const expiration_t& expiration() const noexcept
  {
    return expiration_;
  }

  /// @brief Set the message expiration
  Garlic& expiration(expiration_t expiration)
  {
    const exception::Exception ex{"I2NP: Garlic", __func__};

    check_expiration(expiration, ex);

    expiration_ = std::forward<expiration_t>(expiration);

    return *this;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison operator
  std::uint8_t operator==(const Garlic& oth)
  {  // attempt constant-time comparison
    const auto& cert_eq = static_cast<std::uint8_t>(cert_ == oth.cert_);
    const auto& msg_eq = static_cast<std::uint8_t>(msg_id_ == oth.msg_id_);
    const auto& exp_eq = static_cast<std::uint8_t>(expiration_ == oth.expiration_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    std::shared_lock cs(cloves_mutex_, std::defer_lock);
    std::scoped_lock sgd(cs);
    const auto& cloves_eq = static_cast<std::uint8_t>(cloves_ == oth.cloves_);

    return (cert_eq * msg_eq * exp_eq * buf_eq * cloves_eq);
  }

 private:
  std::uint16_t size(const cloves_t& cloves, const certificate_t& cert)
  {
    return NumLen + cloves_size(cloves) + cert.size() + MessageIDLen + ExpirationLen;
  }

  void DeserializeClove(tini2p::BytesReader<buffer_t>& reader, cloves_t& cloves, const exception::Exception& ex) const
  {
    const auto clove_start = reader.count();

    // read instructions flags to get the length
    clove_t::instructions_t::flags_t flags;
    reader.read_bytes(flags);

    const auto delivery_len = clove_t::instructions_t::size(flags);
    reader.skip_bytes(delivery_len - clove_t::instructions_t::FlagsLen);

    // read I2NP header to do a basic validity check, and get the length
    crypto::SecBytes head_buf;
    head_buf.resize(data::I2NPHeader::HeaderLen);
    reader.read_data(head_buf);

    data::I2NPHeader header(std::move(head_buf));
    const auto& msg_type = header.type();
    const auto& msg_len = header.message_size();

    check_i2np_header(msg_type, msg_len, reader.gcount(), ex);

    reader.skip_bytes(msg_len + clove_t::CloveIDLen + clove_t::ExpirationLen);

    // read the certificate header to do a basic validity check, and get the length
    const auto full_cert_len = DeserializeCertHeader(reader, ex);

    crypto::SecBytes clove_buf;
    clove_buf.resize(delivery_len + data::I2NPHeader::HeaderLen + msg_len + MessageIDLen + ExpirationLen + full_cert_len);

    reader.skip_back(reader.count() - clove_start);
    reader.read_data(clove_buf);

    cloves.emplace_back(std::move(clove_buf));
  }

  std::uint16_t DeserializeCertHeader(tini2p::BytesReader<buffer_t>& reader, const exception::Exception& ex) const
  {
    certificate_t::cert_type_t cert_type;
    certificate_t::length_t cert_len;

    reader.read_bytes(cert_type);
    reader.read_bytes(cert_len, tini2p::Endian::Big);

    return check_cert_header(cert_type, cert_len, reader.gcount(), ex);
  }

  void DeserializeCertificate(
      tini2p::BytesReader<buffer_t>& reader,
      certificate_t& cert,
      const exception::Exception& ex) const
  {
    const auto full_cert_len = DeserializeCertHeader(reader, ex);

    reader.skip_back(certificate_t::HeaderLen);

    auto& cert_buf = cert.buffer;
    cert_buf.resize(full_cert_len);

    reader.read_data(cert_buf);
    cert.deserialize();
  }

  std::uint16_t cloves_size(const cloves_t& cloves) const
  {
    std::uint16_t cloves_len(0);

    for (const auto& clove : cloves)
      cloves_len += clove.size();

    return cloves_len;
  }

  void check_cloves(const cloves_t& cloves, const exception::Exception& ex) const
  {
    const auto& num_cloves = cloves.size();
    const auto& min_cloves = tini2p::under_cast(MinCloves);
    const auto& max_cloves = tini2p::under_cast(MaxCloves);

    if (num_cloves < min_cloves || num_cloves > max_cloves)
      {
        ex.throw_ex<std::logic_error>(
            "invalid number of cloves: " + std::to_string(num_cloves) + ", min: " + std::to_string(min_cloves)
            + ", max: " + std::to_string(max_cloves));
      }

    check_cloves_length(cloves, ex);
  }

  void check_cloves_length(const cloves_t& cloves, const exception::Exception& ex) const
  {
    const auto& cloves_len = cloves_size(cloves);
    const auto& min_cloves_len = tini2p::under_cast(MinClovesLen);
    const auto& max_cloves_len = tini2p::under_cast(MaxClovesLen);

    if (cloves_len < min_cloves_len || cloves_len > max_cloves_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid total cloves length: " + std::to_string(cloves_len) + ", min: " + std::to_string(min_cloves_len)
            + ", max: " + std::to_string(max_cloves_len));
      }
  }

  void check_i2np_header(
      const data::I2NPHeader::Type& msg_type,
      const std::uint16_t& size,
      const std::size_t& remaining,
      const exception::Exception& ex) const
  {
    const auto& min_len = tini2p::under_cast(data::I2NPHeader::MinMessageLen);
    const auto& max_len = remaining - tini2p::under_cast(MetadataLen) + tini2p::under_cast(NumLen);

    if (size < min_len || size > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid I2NP message length: " + std::to_string(size) + ", min: " + std::to_string(min_len)
            + ", max remaining: " + std::to_string(max_len));
      }

    if (msg_type != data::I2NPHeader::Type::Data
        && msg_type != data::I2NPHeader::Type::DeliveryStatus
        && msg_type != data::I2NPHeader::Type::DatabaseStore)
      ex.throw_ex<std::logic_error>("unexpected I2NP message type: " + std::to_string(tini2p::under_cast(msg_type)));
  }

  std::uint16_t check_cert_header(
      const certificate_t::cert_type_t& cert_type,
      const certificate_t::length_t& cert_len,
      const std::size_t remaining,
      const exception::Exception& ex) const
  {
    // TODO(tini2p): expand accepted certificate types as more are used
    if (cert_type != certificate_t::cert_type_t::NullCert)
      ex.throw_ex<std::logic_error>("expected null certificate");

    // TODO(tini2p): expand accepted certificate lengths as more are used
    const auto& exp_len = tini2p::under_cast(certificate_t::NullCertLen);
    const auto& max_len = remaining - (tini2p::under_cast(MessageIDLen) + tini2p::under_cast(ExpirationLen));

    if (cert_len != exp_len || cert_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid certificate length: " + std::to_string(cert_len) + ", expected: " + std::to_string(exp_len)
            + ", max remaining: " + std::to_string(max_len));
      }

    return tini2p::under_cast(certificate_t::HeaderLen) + cert_len;
  }

  void check_message_id(const message_id_t& msg_id, const exception::Exception& ex) const
  {
    const auto& min_id = tini2p::under_cast(MinMessageID);
    const auto& max_id = tini2p::under_cast(MaxMessageID);

    if (msg_id < min_id || msg_id > max_id)
      {
        ex.throw_ex<std::logic_error>(
            "invalid message ID: " + std::to_string(msg_id) + ", min: " + std::to_string(min_id)
            + ", max: " + std::to_string(max_id));
      }
  }

  void check_expiration(const expiration_t& expiration, const exception::Exception& ex) const
  {
    const auto& now = tini2p::time::now_ms();

    if (expiration <= now)
      {
        ex.throw_ex<std::logic_error>(
            "message expired, expiration: " + std::to_string(expiration) + ", now: " + std::to_string(now));
      }
  }

  void check_buffer(const buffer_t& buf, const exception::Exception& ex) const
  {
    if (buf.is_zero())
      ex.throw_ex<std::logic_error>("null message buffer");

    check_buffer_length(buf.size(), ex);
  }

  void check_buffer_length(const std::size_t& len, const exception::Exception& ex) const
  {
    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);

    if (len < min_len || len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  cloves_t cloves_;
  std::shared_mutex cloves_mutex_;

  certificate_t cert_;
  message_id_t msg_id_;
  expiration_t expiration_;

  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_GARLIC_H_
