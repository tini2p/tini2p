/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_I2NP_I2NP_MESSAGE_H_
#define SRC_DATA_I2NP_I2NP_MESSAGE_H_

#include "src/data/i2np/data.h"
#include "src/data/i2np/database_lookup.h"
#include "src/data/i2np/database_search_reply.h"
#include "src/data/i2np/database_store.h"
#include "src/data/i2np/delivery_status.h"
#include "src/data/i2np/i2np_header.h"
#include "src/data/i2np/tunnel_build_message.h"
#include "src/data/i2np/tunnel_build_reply.h"
#include "src/data/i2np/tunnel_gateway.h"
#include "src/data/i2np/tunnel_message.h"

namespace tini2p
{
namespace data
{
// Forward declaration of Garlic class (recursive definition)
class Garlic;

/// @class I2NPMessage
/// @brief Message for creating and processing I2NP messages
/// @detail Message format differs depending on context:
///    Normal (16-byte header):
///      [type][msg_id][expiration_64bit][msg_len_16bit][chksum][msg_body]
///
///    Garlic:
///      [type][msg_id][expiration_64bit][msg_len_16bit][chksum][enc_len_32bit][enc_msg_body]
///
///    NTCP2:
///      [type][msg_id][expiration_32bit][msg_body]
///
///    SSU:
///      [type][expiration_32bit][msg_body]
class I2NPMessage
{
 public:
  enum : std::uint32_t
  {
    HeaderLen = 16,
    NTCP2HeaderLen = 9,
    SSUHeaderLen = 5,
    TypeLen = 1,
    MessageIDLen = 4,
    MinMsgID = 1,
    MaxMsgID = 0xFFFFFFFF,  // uint32_max
    ExpirationSLen = 8,
    ExpirationMSLen = 8,
    SizeLen = 2,
    ChecksumLen = 1,
    GarlicSizeLen = 4,
    MinMessageLen = 0,
    MaxMessageLen = 62703,
    MinLen = SSUHeaderLen,
    MaxLen = 62708,  //< max I2NP length, see I2NP spec
    ExpirationS = 120,  //< in seconds, for NTCP2 + SSU header, see I2NP spec 
    ExpirationMS = 120000,  //< in milliseconds, for 16-byte header, see I2NP spec
  };

  /// @brief I2NP message type
  using Type = data::I2NPHeader::Type;
  using Mode = data::I2NPHeader::Mode;

  /// @brief Default ctor
  I2NPMessage() : header_(), msg_buf_(), buf_()
  {
    serialize();
  }

  /// @brief Converting ctor
  /// @detail Wrap an I2NP message body in the appropriate header
  /// @tparam TMsg Inner I2NP message class: DatabaseStore, DatabaseLookup, ...
  /// @param msg I2NP message body
  /// @param mode I2NP mode for the message: Normal, Garlic, NTCP2, SSU
  template <class TMsg>
  I2NPMessage(TMsg msg, Mode mode = Mode::Normal) : header_()
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    header_.mode(std::forward<Mode>(mode));
    header_.type(message_to_type<TMsg>());
    header_.message_size(
        msg.size() + (static_cast<std::uint8_t>(header_.mode() == Mode::Garlic) * data::I2NPHeader::GarlicSizeLen));

    auto msg_buf = msg.buffer();

    header_.checksum(CalculateChecksum(msg_buf.data(), msg_buf.size()));

    check_message(msg_buf, ex);
    msg_buf_ = msg_buf;

    serialize();
  }

  /// @brief Converting ctor
  /// @detail Wrap an I2NP message body in the appropriate header
  /// @param msg I2NP message body
  /// @param mode I2NP mode for the message: Normal, Garlic, NTCP2, SSU
  I2NPMessage(Type type, std::uint32_t msg_id, crypto::SecBytes msg_buf, Mode mode = Mode::Normal)
      : header_(std::forward<Type>(type), std::forward<std::uint32_t>(msg_id), std::forward<Mode>(mode))
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    check_message(msg_buf, ex);

    header_.message_size(
        msg_buf.size() + (static_cast<std::uint8_t>(header_.mode() == Mode::Garlic) * data::I2NPHeader::GarlicSizeLen));

    msg_buf_ = std::forward<crypto::SecBytes>(msg_buf);

    serialize();
  }

  /// @brief Converting ctor
  /// @detail Wrap an I2NP message body in the appropriate header
  /// @param header I2NP header
  /// @param msg_buf I2NP message body
  /// @param mode I2NP mode for the message: Normal, Garlic, NTCP2, SSU
  I2NPMessage(data::I2NPHeader header, crypto::SecBytes msg_buf)
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    check_message(msg_buf, ex);

    header_ = std::forward<data::I2NPHeader>(header);

    msg_buf_ = std::forward<crypto::SecBytes>(msg_buf);

    serialize();
  }

  /// @brief Deserializing ctor, creates an I2NPMessage from secure buffer
  /// @param buf Buffer containing a serialized I2NPMessage
  /// @param mode I2NP message mode: Normal, Garlic, NTCP2, SSU
  I2NPMessage(crypto::SecBytes buf, Mode mode = Mode::Normal) : header_(), msg_buf_(), buf_()
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    header_.mode(std::forward<Mode>(mode));

    check_buffer(buf, ex);

    header_.message_size(buf.size() - header_.size());

    buf_ = std::forward<crypto::SecBytes>(buf);

    deserialize();
  }

  /// @brief Serialize the I2NP message to the buffer
  I2NPMessage& serialize()
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    check_message(msg_buf_, ex);

    const auto& mode = header_.mode();
    const auto& is_garlic = static_cast<std::uint8_t>(mode == data::I2NPHeader::Mode::Garlic);
    const auto& garlic_len = is_garlic * data::I2NPHeader::GarlicSizeLen;

    const std::uint16_t& msg_len = size();
    const std::uint32_t& body_len = msg_buf_.size() + garlic_len;

    header_.message_size(body_len);

    header_.serialize();

    buf_.resize(msg_len);

    tini2p::BytesWriter<crypto::SecBytes> writer(buf_);

    writer.write_data(header_.buffer());

    // write the message
    if (body_len != 0)
      {
        if (is_garlic)
          {
            writer.write_bytes(
                static_cast<std::uint32_t>(body_len - data::I2NPHeader::GarlicSizeLen), tini2p::Endian::Big);
          }

        writer.write_data(msg_buf_);
      }

    if (mode == data::I2NPHeader::Mode::Normal || is_garlic)
      {
        writer.skip_back(msg_buf_.size() + garlic_len);
        const auto& header_len = header_.size();
        auto checksum = CalculateChecksum(buf_.data() + writer.count(), writer.gcount());

        header_.checksum(std::move(checksum));
        header_.serialize();

        writer.skip_back(header_len);
        writer.write_data(header_.buffer());
      }

    return *this;
  }

  /// @brief Deserialize an I2NPMessage from the buffer
  /// @detail NTCP2 and SSU callers are responsible for setting the appropriate buffer length before calling
  /// @note SSU callers may benefit from helper functions for resizing based on message type
  I2NPMessage& deserialize()
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    tini2p::BytesReader<crypto::SecBytes> reader(buf_);

    crypto::SecBytes head_buf;
    head_buf.resize(header_.size());

    const auto& mode = header_.mode();

    reader.read_data(head_buf);
    data::I2NPHeader header(head_buf, mode);

    // read the message
    // inner message handling deferred to the caller
    auto msg_len = header_.message_size();
    const auto& rem_len = reader.gcount();

    if (msg_len > rem_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid message length: " + std::to_string(msg_len) + ", remaining bytes: " + std::to_string(rem_len));
      }

    if (msg_len == 0)
      header_.message_size(rem_len);

    crypto::SecBytes msg_buf;

    // calculate message checksum, write first byte to buffer
    if (mode == Mode::Normal || mode == Mode::Garlic)
      {
        const std::uint8_t& checksum = header.checksum();
        std::uint8_t calc_checksum = CalculateChecksum(buf_.data() + reader.count(), reader.gcount());

        check_checksum(checksum, calc_checksum, ex);
        header.checksum(std::move(checksum));
      }

    msg_len = header_.message_size();
    if (msg_len != 0)
      {
        if (mode == data::I2NPHeader::Mode::Garlic)
          {
            std::uint32_t garlic_len(0);
            reader.read_bytes(garlic_len, tini2p::Endian::Big);
            check_message_len(garlic_len, ex);
            msg_len = garlic_len;
          }

        msg_buf.resize(msg_len);
        reader.read_data(msg_buf);
      }

    header_ = std::move(header);
    msg_buf_ = std::move(msg_buf);

    return *this;
  }

  /// @brief Get the size of the I2NP message
  std::uint16_t size() const
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    return header_.size() +
           (static_cast<std::uint8_t>(header_.mode() == Mode::Garlic) * GarlicSizeLen) +
           msg_buf_.size();
  }

  /// @brief Convert the inner message buffer to a given message type
  template <class TMsg>
  TMsg to_message()
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    check_type<TMsg>(ex);

    return TMsg(msg_buf_);
  }

  /// @brief Get a const reference to the I2NP message type
  const Type& type() const noexcept
  {
    return header_.type();
  }

  /// @brief Set the I2NP message type
  I2NPMessage& type(Type type)
  {
    header_.type(std::forward<Type>(type));

    return *this;
  }

  /// @brief Get a const reference to the I2NP message ID
  const std::uint32_t& message_id() const noexcept
  {
    return header_.message_id();
  }

  /// @brief Set the I2NP message ID
  I2NPMessage& message_id(std::uint32_t msg_id)
  {
    header_.message_id(std::forward<std::uint32_t>(msg_id));

    return *this;
  }

  /// @brief Get a const reference to the expiration
  const std::uint64_t& expiration() const noexcept
  {
    return header_.expiration();
  }

  /// @brief Get the default expiration from now for a given I2NP mode
  std::uint64_t expiration(const Mode& mode) const
  {
    return header_.expiration(mode);
  }

  /// @brief Set the expiration
  I2NPMessage& expiration(std::uint64_t expiration)
  {
    header_.expiration(std::forward<std::uint64_t>(expiration));

    return *this;
  }

  /// @brief Get a const reference to the one-byte checksum
  const std::uint8_t& checksum() const noexcept
  {
    return header_.checksum();
  }

  const data::I2NPHeader& header() const noexcept
  {
    return header_;
  }

  data::I2NPHeader extract_header()
  {
    return std::move(header_);
  }

  crypto::SecBytes extract_body()
  {
    return std::move(msg_buf_);
  }
  
  /// @brief Get a const reference to the message body buffer
  const crypto::SecBytes& message_buffer() const noexcept
  {
    return msg_buf_;
  }

  /// @brief Set the message body buffer
  I2NPMessage& message_buffer(crypto::SecBytes buf)
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    check_message(buf, ex);

    msg_buf_ = std::forward<crypto::SecBytes>(buf);

    return *this;
  }

  /// @brief Get a const reference to the message body size
  const std::uint16_t message_size() const
  {
    return msg_buf_.size();
  }

  /// @brief Get a const reference to the I2NP message mode
  const Mode& mode() const noexcept
  {
    return header_.mode();
  }

  /// @brief Set the I2NP message mode
  I2NPMessage& mode(Mode mode)
  {
    const auto need_checksum = mode == Mode::Normal || mode == Mode::Garlic;
    const auto msg_offset = header_.size();

    header_.mode(std::forward<Mode>(mode));

    if (need_checksum)
      header_.checksum(CalculateChecksum(buf_.data() + msg_offset, buf_.size() - msg_offset));

    return *this;
  }
  
  /// @brief Get a const reference to the full message buffer
  const crypto::SecBytes& buffer() const noexcept
  {
    return buf_;
  }
  
  /// @brief Get a non-const reference to the full message buffer
  crypto::SecBytes& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Calculate the one-byte checksum of the I2NP message body
  std::uint8_t CalculateChecksum(const crypto::SecBytes& msg_buf) const
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    check_message(msg_buf, ex);

    crypto::Sha256::digest_t checksum;
    crypto::Sha256::Hash(checksum, msg_buf);

    return checksum.front();
  }

  /// @brief Calculate the one-byte checksum of the I2NP message body
  /// @param msg_buf Pointer to the beginning of the message buffer
  /// @param len Length of the message buffer
  /// @throws Invalid argument for null buffer with non-null length
  /// @returns First byte of the Sha256 digest for the message buffer
  std::uint8_t CalculateChecksum(const std::uint8_t* msg_buf, const std::uint16_t& len) const
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    check_message_len(len, ex);
    tini2p::check_cbuf(msg_buf, len, 0, MaxLen - header_.size(), ex);

    crypto::Sha256::digest_t checksum;
    crypto::Sha256::Hash(checksum, msg_buf, len);

    return checksum.front();
  }

  /// @brief Equality comparison with another I2NPMessage
  std::uint8_t operator==(const I2NPMessage& oth) const
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    const auto& head_eq = static_cast<std::uint8_t>(header_ == oth.header_);
    const auto& msg_eq = static_cast<std::uint8_t>(msg_buf_ == oth.msg_buf_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    return (head_eq * msg_eq * buf_eq);
  }

 private:
  template <class TMsg>
  void check_type(const Type& type, const exception::Exception& ex) const
  {
    header_.check_type(type, ex);

    const auto& calc_type = message_to_type<TMsg>();

    if (calc_type != type)
      {
        ex.throw_ex<std::logic_error>(
            "invalid I2NP type: " + std::to_string(tini2p::under_cast(calc_type))
            + ", expected: " + std::to_string(tini2p::under_cast(type)));
      }
  }

  template <class TMsg>
  void check_type(const exception::Exception& ex) const
  {
    const auto& type = header_.type();
    header_.check_type(header_.type(), ex);

    const auto& calc_type = message_to_type<TMsg>();

    if (type != calc_type)
      ex.throw_ex<std::invalid_argument>("invalid message type");
  }

  template <class TMsg>
  Type message_to_type() const
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    Type type(Type::Reserved);

    if (std::is_same<TMsg, data::DatabaseStore>::value)
      type = Type::DatabaseStore;
    else if (std::is_same<TMsg, data::DatabaseLookup>::value)
      type = Type::DatabaseLookup;
    else if (std::is_same<TMsg, data::DatabaseSearchReply>::value)
      type = Type::DatabaseSearchReply;
    else if (std::is_same<TMsg, data::DeliveryStatus>::value)
      type = Type::DeliveryStatus;
    else if (std::is_same<TMsg, data::Garlic>::value)
      type = Type::Garlic;
    else if (
        std::is_same<TMsg, data::TunnelMessage<crypto::TunnelAES>>::value
        || std::is_same<TMsg, data::TunnelMessage<crypto::TunnelChaCha>>::value)
      type = Type::TunnelData;
    else if (std::is_same<TMsg, data::TunnelGateway>::value)
      type = Type::TunnelGateway;
    else if (std::is_same<TMsg, data::Data>::value)
      type = Type::Data;
    else if (std::is_same<TMsg, data::TunnelBuildMessage>::value)
      type = Type::VariableTunnelBuild;
    else if (std::is_same<TMsg, data::TunnelBuildReply<crypto::AES>>::value ||
             std::is_same<TMsg, data::TunnelBuildReply<crypto::ChaCha20>>::value)
      type = Type::VariableTunnelBuildReply;
    else
      ex.throw_ex<std::invalid_argument>("invalid message type");

    return type;
  }

  void check_checksum(const std::uint8_t& chk, const std::uint8_t& exp_chk, const exception::Exception& ex) const
  {
    if (chk != exp_chk)
      {
        ex.throw_ex<std::logic_error>(
            "invalid checksum: " + std::to_string(chk) + ", expected: " + std::to_string(exp_chk));
      }
  }

  void check_message(const crypto::SecBytes& msg_buf, const exception::Exception& ex) const
  {
    check_message_len(msg_buf.size(), ex);
  }

  template <std::size_t N>
  void check_message(const crypto::FixedSecBytes<N>&, const exception::Exception& ex) const
  {
    check_message_len(N, ex);
  }

  void check_message_len(const std::uint16_t& msg_len, const exception::Exception& ex) const
  {
    const std::uint16_t& max_len = tini2p::under_cast(MaxLen) - header_.size();

    if (msg_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid body length: " + std::to_string(msg_len) + ", max: " + std::to_string(max_len));
      }
  }

  void check_mode(const Mode& mode, const exception::Exception& ex) const
  {
    if (mode != Mode::Normal && mode != Mode::Garlic && mode != Mode::NTCP2 && mode != Mode::SSU)
      ex.throw_ex<std::invalid_argument>("invalid I2NP message mode");
  }

  void check_buffer(const crypto::SecBytes& buf, const exception::Exception& ex) const
  {
    const auto& buf_len = buf.size();
    const auto& min_len = header_.size();
    const auto& max_len = tini2p::under_cast(MaxLen);

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }

    if (buf.is_zero())
      ex.throw_ex<std::logic_error>("null buffer");
  }

  data::I2NPHeader header_;
  crypto::SecBytes msg_buf_;
  crypto::SecBytes buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_I2NP_I2NP_MESSAGE_H_
