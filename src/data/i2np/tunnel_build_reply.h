/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_I2NP_TUNNEL_BUILD_REPLY_H_
#define SRC_DATA_I2NP_TUNNEL_BUILD_REPLY_H_

#include <shared_mutex>

#include "src/data/i2np/build_response_record.h"

namespace tini2p
{
namespace data
{
/// @class TunnelBuildReply
/// @brief VariableTunnelBuildReply implementation
/// @tparam TSym Symmetric crypto type for reply, layer, and random keys
template <class TSym>
class TunnelBuildReply
{
 public:
  enum
  {
    SizeLen = 1,
    MinRecords = 1,
    MaxRecords = 8,
    MinLen = 529,  //< 1 + Encrypted BuildRequestRecord length
    MaxLen = 4225,  //< 1 + (8 * Encrypted BuildRequestRecord length)
    RecordLen = 528,  //< Encrypted BuildRequestRecord length
  };

  using sym_t = TSym;  //< Symmetric crypto trait alias
  using aes_t = crypto::AES;  //< AES trait alias
  using chacha_t = crypto::ChaCha20;  //< ChaCha trait alias
  using num_records_t = std::uint8_t;  //< Number of records trait alias
  using reply_record_t = data::BuildResponseRecord<sym_t>;  //< Build request record trait alias
  using records_t = std::vector<reply_record_t>;  //< Build request records collection trait alias
  using deserialize_mode_t = typename reply_record_t::deserialize_mode_t;  //< Deserialize mode trait alias
  using size_type = std::uint16_t;  //< Size type trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor, creates an empty TunnelBuildReply
  /// @detail Caller must add at least one BuildRequestRecord before serializing
  TunnelBuildReply() : records_() {}

  /// @brief Fully initializign ctor, creates a TunnelBuildReply from BuildRequestRecords
  /// @detail Caller must ensure records are encrypted and serialized to buffer before adding
  /// @param records Collection of BuildRequestRecords to add
  explicit TunnelBuildReply(records_t records)
  {
    const exception::Exception ex{"TunnelBuildReply", __func__};

    check_records(records, ex);

    records_ = std::forward<records_t>(records);

    serialize();
  }

  /// @brief Copy-ctor
  /// @detail Caller must ensure records are encrypted and serialized to buffer
  TunnelBuildReply(const TunnelBuildReply& oth)
  {
    const exception::Exception ex{"TunnelBuildReply", __func__};

    check_records(oth.records_, ex);
    records_ = oth.records_;

    serialize();
  }

  /// @brief Deserializing ctor, creates a TunnelBuildReply from a secure buffer
  /// @param buf Secure buffer containing the TunnelBuildReply message
  /// @param mode Deserialize mode for processing encrypted or decrypted messages
  TunnelBuildReply(buffer_t buf, const deserialize_mode_t& mode = deserialize_mode_t::Full)
  {
    const exception::Exception ex{"TunnelBuildReply", __func__};

    check_buffer(buf, ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize(mode);
  }

  /// @brief Move-ctor
  /// @detail Caller must ensure records are encrypted and serialized to buffer
  TunnelBuildReply(TunnelBuildReply&& oth)
  {
    const exception::Exception ex{"TunnelBuildReply", __func__};

    check_records(oth.records_, ex);
    records_ = std::move(oth.records_);

    serialize();
  }

  /// @brief Forwarding-assignment operator
  /// @detail Caller must ensure records are encrypted and serialized to buffer
  TunnelBuildReply& operator=(TunnelBuildReply oth)
  {
    const exception::Exception ex{"TunnelBuildReply", __func__};

    check_records(oth.records_, ex);

    {  // lock records
      std::scoped_lock sgd(records_mutex_);
      records_ = std::forward<records_t>(oth.records_);
    } // end-records-lock

    serialize();

    return *this;
  }

  /// @brief Serialize the TunnelBuildReply to the buffer
  void serialize()
  {
    const exception::Exception ex{"TunnelBuildReply", __func__};

    std::shared_lock sr(records_mutex_, std::defer_lock);
    std::scoped_lock sgd(sr);

    check_record_num(records_.size(), ex);

    buf_.resize(size(records_, ex));

    tini2p::BytesWriter<buffer_t> writer(buf_);

    // write the number of BuildRequestRecords in the message
    writer.write_bytes(static_cast<num_records_t>(records_.size()));

    for (const auto& record : records_)
      writer.write_data(record.buffer());  // write each record
  }

  /// @brief Deserialize the TunnelBuildReply from the buffer
  void deserialize(const deserialize_mode_t& mode = deserialize_mode_t::Full)
  {
    const exception::Exception ex{"TunnelBuildReply", __func__};

    check_buffer(buf_, ex);

    tini2p::BytesReader<buffer_t> reader(buf_);

    num_records_t num_records;
    reader.read_bytes(num_records);
    check_record_num(num_records, ex);

    records_t records;
    for (num_records_t i = 0; i < num_records; ++i)
      {  // partially deserialize the encrypted records
        typename reply_record_t::buffer_t buf;

        reader.read_data(buf);

        // check for duplicate records
        reply_record_t record(std::move(buf), mode);
        check_additional_record(records, ex);

        records.emplace_back(std::move(record));
      }

    std::scoped_lock sgd(records_mutex_);
    records_.swap(records);
  }

  /// @brief Get the size of the TunnelBuildReply
  size_type size()
  {
    const exception::Exception ex{"TunnelBuildReply", __func__};

    std::shared_lock sr(records_mutex_, std::defer_lock);
    std::scoped_lock sgd(sr);

    return size(records_, ex);
  }

  /// @brief Add a single BuildRequestRecord to the TunnelBuildReply
  /// @detail Caller must ensure record is encrypted and serialized to buffer
  void add_record(reply_record_t record)
  {
    const exception::Exception ex{"TunnelBuildReply", __func__};

    std::scoped_lock sgd(records_mutex_);

    check_additional_record(records_, ex);

    records_.emplace_back(std::forward<reply_record_t>(record));
  }

  /// @brief Find a BuildRequestRecord by its truncated Identity hash
  /// @throws Logic error if no record found
  /// @returns A non-const reference to the found record
  reply_record_t& find_record(const typename reply_record_t::key_info_t::trunc_ident_t& trunc_ident)
  {
    const exception::Exception ex{"TunnelBuildReply", __func__};

    std::shared_lock sr(records_mutex_, std::defer_lock);
    std::scoped_lock sgd(sr);

    const auto record_end = records_.end();
    auto it = std::find_if(
        records_.begin(), record_end, [&trunc_ident](const auto& r) { return r.truncated_ident() == trunc_ident; });

    if (it == record_end)
      ex.throw_ex<std::logic_error>("record not found for truncated Identity: " + tini2p::bin_to_hex(trunc_ident, ex));

    return *it;
  }

  /// @brief Get a const reference to the BuildRequestRecords
  const records_t& records() const noexcept
  {
    return records_;
  }

  /// @brief Get a non-const reference to the BuildRequestRecords
  records_t& records() noexcept
  {
    return records_;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another TunnelBuildReply
  std::uint8_t operator==(const TunnelBuildReply& oth)
  {
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    std::shared_lock sr(records_mutex_, std::defer_lock);
    std::scoped_lock sgd(sr);

    const auto& rec_eq = static_cast<std::uint8_t>(records_ == oth.records_);

    return (buf_eq * rec_eq);
  }

 private:
  void check_record_num(const std::size_t& num_records, const exception::Exception& ex) const
  {
    const auto& min_records = tini2p::under_cast(MinRecords);
    const auto& max_records = tini2p::under_cast(MaxRecords);

    if (num_records < min_records || num_records > max_records)
      {
        ex.throw_ex<std::length_error>(
            "invalid number of records: " + std::to_string(num_records) + ", min: " + std::to_string(min_records)
            + ", max: " + std::to_string(max_records));
      }
  }

  void check_records(const records_t& records, const exception::Exception& ex) const
  {
    check_record_num(records.size(), ex);

    const auto& exp_record_len = tini2p::under_cast(RecordLen);

    for (const auto& record : records)
      {
        // check for valid record size
        const auto& record_len = record.buffer().size();
        if (record_len != exp_record_len)
          {
            ex.throw_ex<std::length_error>(
                "invalid encrypted BuildRequestRecordLength: " + std::to_string(record_len)
                + ", expected: " + std::to_string(exp_record_len));
          }
      }
  }

  void check_additional_record(const records_t& records, const exception::Exception& ex) const
  {
    const auto& max_records = tini2p::under_cast(MaxRecords);

    if (records.size() == max_records)
      ex.throw_ex<std::length_error>("unable to add a record, max records: " + std::to_string(max_records) + " reached.");
  }

  void check_buffer(const buffer_t& buf, const exception::Exception& ex) const
  {
    const auto& buf_len = buf.size();
    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  size_type size(const records_t& records, const exception::Exception& ex) const
  {
    return (tini2p::under_cast(SizeLen) + (records.size() * tini2p::under_cast(RecordLen)));
  }

  records_t records_;
  std::shared_mutex records_mutex_;

  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_I2NP_TUNNEL_BUILD_REPLY_H_
