/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_I2NP_TUNNEL_GATEWAY_H_
#define SRC_DATA_I2NP_TUNNEL_GATEWAY_H_

#include "src/bytes.h"

#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"

#include "src/data/i2np/i2np_header.h"

namespace tini2p
{
namespace data
{
/// @class TunnelGateway
/// @brief TunnelGateway message for wrapping I2NP messages to InboundGateways
class TunnelGateway
{
 public:
  enum
  {
    TunnelIDLen = 4,
    LengthLen = 2,
    HeaderLen = 6,
    MinTunnelID = 1,
    MaxTunnelID = 4294967295,
    MinDataLen = 5,  //< I2NP message header (5)
    MaxDataLen = 62690,  //< max I2NP length (62708) - (Header (6) + MinDataLen (12)), see I2NP spec
    MinBufferLen = 11,  //< Header (6) + MinDataLen (5)
    MaxBufferLen = 63696,  //< Header (6) + MaxDataLen (62690)
  };

  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using length_t = std::uint16_t;  //< Length trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor, creates an empty TunnelGateway message
  /// @note For use with std containers
  TunnelGateway() : id_(crypto::RandInRange(0x01, 0xFFFFFFFF)), length_(0), i2np_header_(), i2np_body_() {}

  /// @brief Fully initializing ctor, creates a TunnelGateway message with wrapped I2NP data
  /// @param id Tunnel ID of the InboundGateway to send the message to
  /// @param i2np_header I2NP header for the inner message
  /// @param i2np_body I2NP message body
  TunnelGateway(tunnel_id_t id, crypto::SecBytes i2np_buf, data::I2NPHeader::Mode mode = data::I2NPHeader::Mode::Normal)
      : i2np_header_(), i2np_body_()
  {
    const exception::Exception ex{"I2NP: TunnelGateway", __func__};

    check_tunnel_id(id, ex);
    check_buffer(i2np_buf, ex);

    id_ = std::forward<std::uint32_t>(id);

    auto& head_buf = i2np_header_.buffer();
    tini2p::BytesReader<crypto::SecBytes> reader(i2np_buf);
    reader.read_data(head_buf);
    i2np_header_.deserialize();

    i2np_body_.resize(reader.gcount());
    reader.read_data(i2np_body_);

    length_ = i2np_header_.size() + i2np_body_.size();

    serialize();
  }

  /// @brief Fully initializing ctor, creates a TunnelGateway message with wrapped I2NP data
  /// @param id Tunnel ID of the InboundGateway to send the message to
  /// @param i2np_header I2NP header for the inner message
  /// @param i2np_body I2NP message body
  TunnelGateway(tunnel_id_t id, data::I2NPHeader i2np_header, crypto::SecBytes i2np_body)
  {
    const exception::Exception ex{"I2NP: TunnelGateway", __func__};

    check_tunnel_id(id, ex);
    check_i2np_data(i2np_body, ex);

    id_ = std::forward<tunnel_id_t>(id);
    
    i2np_header_ = std::forward<data::I2NPHeader>(i2np_header);
    i2np_body_ = std::forward<crypto::SecBytes>(i2np_body);

    length_ = i2np_header_.size() + i2np_body_.size();

    serialize();
  }

  /// @brief Deserializing ctor, creates a TunnelGateway message from a secure buffer
  TunnelGateway(buffer_t buf, data::I2NPHeader::Mode mode = data::I2NPHeader::Mode::Normal)
      : i2np_header_(), i2np_body_()
  {
    const exception::Exception ex{"I2NP: TunnelGateway", __func__};

    check_buffer(buf, ex);

    i2np_header_.mode(std::forward<data::I2NPHeader::Mode>(mode));

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Serialize the TunnelGateway message to buffer
  TunnelGateway& serialize()
  {
    const exception::Exception ex{"I2NP: TunnelGateway", __func__};

    check_tunnel_id(id_, ex);
    check_i2np_data(i2np_body_, ex);

    length_ = i2np_header_.size() + i2np_body_.size();
    buf_.resize(size());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_bytes(id_, tini2p::Endian::Big);
    writer.write_bytes(length_, tini2p::Endian::Big);

    const auto& mode = i2np_header_.mode();
    if (mode == data::I2NPHeader::Mode::Normal || mode == data::I2NPHeader::Mode::Garlic)
      {
        writer.skip_bytes(i2np_header_.size());
        
        if (mode == data::I2NPHeader::Mode::Garlic)
          writer.write_bytes(static_cast<std::uint32_t>(i2np_body_.size()), tini2p::Endian::Big);

        writer.write_data(i2np_body_);

        const auto garlic_len =
            static_cast<std::uint8_t>(mode == data::I2NPHeader::Mode::Garlic) * data::I2NPHeader::GarlicSizeLen;

        const auto body_len = i2np_body_.size();

        writer.skip_back(body_len + garlic_len);

        i2np_header_.message_size(body_len + garlic_len);
        i2np_header_.checksum(CalculateChecksum(buf_.data() + writer.count(), writer.gcount()));
        i2np_header_.serialize();

        writer.skip_back(i2np_header_.size());
        writer.write_data(i2np_header_.buffer());
      }
    else
      {
        i2np_header_.serialize();
        writer.write_data(i2np_header_.buffer());
        writer.write_data(i2np_body_);
      }

    return *this;
  }

  /// @brief Deserialize the TunnelGateway message from buffer
  TunnelGateway& deserialize()
  {
    const exception::Exception ex{"I2NP: TunnelGateway", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    // read and check the tunnel ID
    tunnel_id_t id;
    reader.read_bytes(id, tini2p::Endian::Big);
    check_tunnel_id(id, ex);

    // read and check the message length
    length_t len;
    reader.read_bytes(len, tini2p::Endian::Big);
    check_length(len, ex);

    crypto::SecBytes head_buf;
    head_buf.resize(i2np_header_.size());

    reader.read_data(head_buf);

    const auto& mode = i2np_header_.mode();
    data::I2NPHeader i2np_header(std::move(head_buf), mode);

    if (mode == data::I2NPHeader::Mode::Normal || mode == data::I2NPHeader::Mode::Garlic)
      {
        auto calc_checksum = CalculateChecksum(buf_.data() + reader.count(), reader.gcount());
        const auto& exp_checksum = i2np_header.checksum();
        if (calc_checksum != exp_checksum)
          {
            ex.throw_ex<std::logic_error>(
                "invalid checksum: " + std::to_string(calc_checksum) + ", expected: " + std::to_string(exp_checksum));
          }
      }

    crypto::SecBytes msg_buf;
    msg_buf.resize(reader.gcount());

    reader.read_data(msg_buf);

    // assign the valid parameters
    id_ = std::move(id);
    length_ = std::move(len);
    i2np_header_ = std::move(i2np_header);
    i2np_body_ = std::move(msg_buf);
    
    // reclaim unused space
    buf_.resize(reader.count());
    buf_.shrink_to_fit();

    return *this;
  }

  /// @brief Get a const reference to the tunnel ID
  const tunnel_id_t& tunnel_id() const noexcept
  {
    return id_;
  }

  /// @brief Set the tunnel ID
  TunnelGateway& tunnel_id(tunnel_id_t id)
  {
    const exception::Exception ex{"I2NP: TunnelGateway", __func__};

    check_tunnel_id(id, ex);

    id_ = std::forward<tunnel_id_t>(id);

    return *this;
  }

  /// @brief Get a const reference to the wrapped I2NP message
  const data::I2NPHeader& i2np_header() const noexcept
  {
    return i2np_header_;
  }

  /// @brief Set the wrapped I2NP message block
  TunnelGateway& i2np_header(data::I2NPHeader i2np_header)
  {
    i2np_header_ = std::forward<data::I2NPHeader>(i2np_header);

    return *this;
  }

  /// @brief Extract the I2NP header
  data::I2NPHeader extract_header()
  {
    return std::move(i2np_header_);
  }

  /// @brief Get a const reference to the I2NP message body
  const crypto::SecBytes& i2np_body() const noexcept
  {
    return i2np_body_;
  }

  /// @brief Get a const reference to the I2NP message body
  TunnelGateway& i2np_body(crypto::SecBytes i2np_body)
  {
    const exception::Exception ex{"I2NP: TunnelGateway", __func__};

    check_i2np_data(i2np_body, ex);
    
    i2np_body_ = std::forward<crypto::SecBytes>(i2np_body);

    return *this;
  }

  /// @brief Extract the I2NP message body
  crypto::SecBytes extract_body()
  {
    return std::move(i2np_body_);
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Get the size of the TunnelGateway message
  length_t size() const
  {
    return tini2p::under_cast(HeaderLen) + i2np_header_.size() + i2np_body_.size();
  }

  /// @brief Equality comparison with another TunnelGateway message
  std::uint8_t operator==(const TunnelGateway& oth) const
  {  // attempt constant-time comparison
    const auto& id_eq = static_cast<std::uint8_t>(id_ == oth.id_);
    const auto& len_eq = static_cast<std::uint8_t>(length_ == oth.length_);
    const auto& head_eq = static_cast<std::uint8_t>(i2np_header_ == oth.i2np_header_);
    const auto& body_eq = static_cast<std::uint8_t>(i2np_body_ == oth.i2np_body_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    return (id_eq * len_eq * head_eq * body_eq * buf_eq);
  }

 private:
  void check_tunnel_id(const tunnel_id_t& id, const exception::Exception& ex) const
  {
    const auto& min_id = tini2p::under_cast(MinTunnelID);
    const auto& max_id = tini2p::under_cast(MaxTunnelID);

    if (id < min_id || id > max_id)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid tunnel ID: " + std::to_string(id) + ", min: " + std::to_string(min_id)
            + ", max: " + std::to_string(max_id));
      }
  }

  void check_length(const std::size_t& len, const exception::Exception& ex) const
  {
    const auto& min_len = tini2p::under_cast(MinDataLen);
    const auto& max_len = tini2p::under_cast(MaxDataLen);

    if (len < min_len || len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid I2NP data length: " + std::to_string(len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  void check_i2np_data(const crypto::SecBytes& data, const exception::Exception& ex) const
  {
    if (data.is_zero())
      ex.throw_ex<std::invalid_argument>("null I2NP data.");

    check_length(data.size(), ex);
  }

  void check_buffer(const buffer_t& buf, const exception::Exception& ex) const
  {
    if (buf.is_zero())
      ex.throw_ex<std::invalid_argument>("null buffer.");

    const auto& min_len = tini2p::under_cast(MinBufferLen);
    const auto& max_len = tini2p::under_cast(MaxBufferLen);
    const auto& buf_len = buf.size();

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  std::uint8_t CalculateChecksum(const crypto::SecBytes& msg_buf) const
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    crypto::Sha256::digest_t checksum;
    crypto::Sha256::Hash(checksum, msg_buf);

    return checksum.front();
  }

  std::uint8_t CalculateChecksum(const std::uint8_t* msg_buf, const std::uint16_t& len) const
  {
    const exception::Exception ex{"I2NP: Message", __func__};

    tini2p::check_cbuf(msg_buf, len, 0, data::I2NPHeader::MaxMessageLen, ex);

    crypto::Sha256::digest_t checksum;
    crypto::Sha256::Hash(checksum, msg_buf, len);

    return checksum.front();
  }

  tunnel_id_t id_;
  length_t length_;
  data::I2NPHeader i2np_header_;
  crypto::SecBytes i2np_body_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_I2NP_TUNNEL_GATEWAY_H_
