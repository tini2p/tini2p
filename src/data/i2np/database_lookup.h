/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_I2NP_DATABASE_LOOKUP_H_
#define SRC_I2NP_DATABASE_LOOKUP_H_

#include <mutex>
#include <shared_mutex>

#include "src/bytes.h"

#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/rand.h"
#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"

namespace tini2p
{
namespace data
{
/// @class DatabaseLookup
/// @brief Class for storing and processing DatabaseLookup messages
class DatabaseLookup
{
 public:
  enum : std::uint16_t
  {
    SearchKeyLen = crypto::Sha256::DigestLen,
    FromKeyLen = crypto::Sha256::DigestLen,
    FlagsLen = 1,
    ExPeersSizeLen = 2,
    ExPeerLen = crypto::Sha256::DigestLen,
    MaxExcludedPeers = 512,  // see spec
    TagsSizeLen = 1,
    ReplyTagLen = 8,  // ECIES session tag length (final value TBD, see prop. 144)
    MinReplyTags = 1,  // if encrypted flag set, see spec
    MaxReplyTags = 32,  // if encrypted flag set, see spec
    MinLen = SearchKeyLen + FromKeyLen + FlagsLen + ExPeersSizeLen,  // min message length w/ all required fields, see spec 
    MaxLen = MinLen + (MaxExcludedPeers * ExPeerLen) + (MaxReplyTags * ReplyTagLen),
  };

  enum struct DeliveryFlags : std::uint8_t
  {
    Direct = 0x00,  // 0000 0000, see spec
    Tunnel = 0x01,  // 0000 0001, see spec
  };

  enum struct EncryptionFlags : std::uint8_t
  {
    Unencrypted = 0x00,  // 0000 0000, see spec
    Encrypted = 0x02,    // 0000 0010, see spec
  };

  enum struct LookupFlags : std::uint8_t
  {
    Normal = 0x00,   // 0000 0000, see spec
    LS = 0x04,       // 0000 0100, see spec
    RI = 0x08,       // 0000 1000, see spec
    Explore = 0x0c,  // 0000 1100, see spec
  };

  enum : std::uint8_t
  {
    LookupFlagMask = 0x0C,      // 0000 1100, see spec. Bitwise-AND w/ flags to get lookup type
    EncryptionFlagMask = 0x02,  // 0000 0010, see spec. Bitwise-AND w/ flags to get encryption type
    ReservedFlagMask = 0xF0,    // 1111 0000, see spec. Bitwise-AND w/ flags for validity check
  };

  using search_key_t = crypto::Sha256::digest_t;  //< Search key trait alias
  using from_key_t = crypto::Sha256::digest_t;  //< From key trait alias
  using flags_t = std::uint8_t;  //< Flags trait alias
  using tunnel_id_t = std::uint32_t;  //< Reply tunnel ID trait alias
  using ex_peers_size_t = std::uint16_t;  //< Excluded peer size trait alias
  using ex_peer_key_t = crypto::Sha256::digest_t;  //< Excluded peer key trait alias
  using reply_key_t = crypto::ChaChaPoly1305::key_t;  //< Reply key trait alias
  using tags_size_t = std::uint8_t;  //< Tags size trait alias
  using reply_tag_t = crypto::FixedSecBytes<ReplyTagLen>;  //< Reply tag trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  using pointer = DatabaseLookup*;  //< Non-owning pointer trait alias
  using const_pointer = const DatabaseLookup*;  //< Const non-owning pointer trait alias
  using unique_ptr = std::unique_ptr<DatabaseLookup>;  //< Unique pointer trait alias
  using const_unique_ptr = std::unique_ptr<const DatabaseLookup>;  //< Const unique pointer trait alias
  using shared_ptr = std::shared_ptr<DatabaseLookup>;  //< Shared pointer trait alias
  using const_shared_ptr = std::shared_ptr<const DatabaseLookup>;  //< Const shared pointer trait alias

  /// @brief Create an unencrypted DatabaseLookup message w/ direct delivery
  /// @param search_key Search key of the entry to lookup
  DatabaseLookup(
      search_key_t search_key,
      from_key_t from_key,
      tunnel_id_t reply_id = 0,
      LookupFlags lookup_flag = LookupFlags::Normal)
      : search_key_(std::forward<search_key_t>(search_key)),
        from_key_(std::forward<from_key_t>(from_key)),
        reply_tunnel_id_(std::forward<tunnel_id_t>(reply_id)),
        buf_(MinLen)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    check_flags({}, {}, lookup_flag, ex);

    const auto& lookup_int = tini2p::under_cast(lookup_flag);
    const auto& delivery_int =
        reply_tunnel_id_ == 0 ? tini2p::under_cast(DeliveryFlags::Direct) : tini2p::under_cast(DeliveryFlags::Tunnel);

    flags_ = lookup_int | delivery_int;

    serialize();
  }

  /// @brief Create a DatabaseLookup message from a secure buffer
  DatabaseLookup(buffer_t buf) : reply_tunnel_id_(0)
  {
    from_buffer(std::forward<buffer_t>(buf));
  }

  /// @brief Create a DatabaseLookup message from a C-like buffer
  /// @param data Pointer to the message buffer
  /// @param len Length of the message buffer
  DatabaseLookup(const std::uint8_t* data, const std::size_t len) : reply_tunnel_id_(0)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  /// @brief Forwarding-assignment operator
  DatabaseLookup& operator=(DatabaseLookup oth)
  {
    search_key_ = std::forward<search_key_t>(oth.search_key_);
    from_key_ = std::forward<from_key_t>(oth.from_key_);
    flags_ = std::forward<flags_t>(oth.flags_);
    reply_tunnel_id_ = std::forward<tunnel_id_t>(oth.reply_tunnel_id_);
    reply_key_ = std::forward<reply_key_t>(oth.reply_key_);
    buf_ = std::forward<buffer_t>(oth.buf_);

    {  // lock peers and tags
      std::scoped_lock sgd(peer_mutex_, tag_mutex_);
      ex_peers_ = std::forward<std::vector<ex_peer_key_t>>(oth.ex_peers_);
      reply_tags_ = std::forward<std::vector<reply_tag_t>>(oth.reply_tags_);
    }  // end-peer-tag-lock

    return *this;
  }

  /// @brief Serialize DatabaseLookup message to buffer
  DatabaseLookup& serialize()
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    check_params(ex);

    buf_.resize(size());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_data(search_key_);
    writer.write_data(from_key_);
    writer.write_bytes(flags_);

    if (flags_ & tini2p::under_cast(DeliveryFlags::Tunnel))
      writer.write_bytes(reply_tunnel_id_, tini2p::Endian::Big);

    writer.write_bytes(static_cast<ex_peers_size_t>(ex_peers_.size()), tini2p::Endian::Big);
    for (const auto& peer : ex_peers_)
      writer.write_data(peer);

    if (flags_ & tini2p::under_cast(EncryptionFlags::Encrypted))
      {
        writer.write_data(reply_key_);
        writer.write_bytes(static_cast<tags_size_t>(reply_tags_.size()));
        for (const auto& tag : reply_tags_)
          writer.write_data(tag);
      }

    return *this;
  }

  /// @brief Deserialize DatabaseLookup message from buffer
  DatabaseLookup& deserialize()
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    reader.read_data(search_key_);
    reader.read_data(from_key_);

    reader.read_bytes(flags_);
    if (flags_ & ReservedFlagMask)
      ex.throw_ex<std::logic_error>("reserved flags.");

    if (flags_ & tini2p::under_cast(DeliveryFlags::Tunnel))
      reader.read_bytes(reply_tunnel_id_, tini2p::Endian::Big);

    ex_peers_size_t ex_size;
    reader.read_bytes(ex_size, tini2p::Endian::Big);

    if (ex_size > MaxExcludedPeers)
      ex.throw_ex<std::logic_error>("invalid excluded peers size: " + std::to_string(ex_size));

    std::vector<ex_peer_key_t> ex_peer_keys{};
    for (ex_peers_size_t i = 0; i < ex_size; ++i)
      {
        ex_peer_key_t peer;
        reader.read_data(peer);
        ex_peer_keys.emplace_back(std::move(peer));
      }

    {  // lock excluded peers
      std::scoped_lock pgd(peer_mutex_);
      ex_peers_.swap(ex_peer_keys);
    }  // end-excluded-peers-lock

    if (flags_ & tini2p::under_cast(EncryptionFlags::Encrypted))
    {
      reader.read_data(reply_key_);
      tags_size_t tag_size;
      reader.read_bytes(tag_size);

      if (tag_size > MaxReplyTags)
        ex.throw_ex<std::logic_error>("invalid reply tags size: " + std::to_string(tag_size));

      std::vector<reply_tag_t> rep_tags;
      for (tags_size_t i = 0; i < tag_size; ++i)
        {
          reply_tag_t t_tag;
          reader.read_data(t_tag);
          rep_tags.emplace_back(std::move(t_tag));
        }

      {  // lock reply tags
        std::lock_guard<std::mutex> tgd(tag_mutex_);
        reply_tags_.swap(rep_tags);
      }  // end-reply-tags-lock
    }

    return *this;
  }

  /// @brief Create a DatabaseLookup message from a secure buffer
  void from_buffer(buffer_t buf)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    const auto buf_len = buf.size();
    if (buf_len < MinLen || buf_len > MaxLen)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid buffer length: " + std::to_string(buf_len) + " min: " + std::to_string(tini2p::under_cast(MinLen))
            + " max: " + std::to_string(tini2p::under_cast(MaxLen)));
      }

    buf_ = std::forward<buffer_t>(buf);
    deserialize();
  }

  /// @brief Get the total size of the DatabaseLookup message
  std::uint32_t size() const noexcept
  {
    const auto search_from_keys_len = crypto::Sha256::DigestLen * 2;
    const auto tun_len =
        static_cast<std::uint8_t>(flags_ & tini2p::under_cast(DeliveryFlags::Tunnel)) * sizeof(tunnel_id_t);
    const auto ex_peer_len = ExPeersSizeLen + (ex_peers_.size() * crypto::Sha256::DigestLen);
    const auto enc_len = static_cast<std::uint8_t>(flags_ & tini2p::under_cast(EncryptionFlags::Encrypted))
                         * (crypto::ChaChaPoly1305::KeyLen + TagsSizeLen + (reply_tags_.size() * ReplyTagLen));

    return search_from_keys_len + FlagsLen + tun_len + ex_peer_len + enc_len;
  }

  /// @brief Get a const reference to the search key
  const search_key_t& search_key() const noexcept
  {
    return search_key_;
  }

  /// @brief Set the search key
  DatabaseLookup& search_key(search_key_t key)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    check_key(key, "search", ex);

    search_key_ = std::forward<search_key_t>(key);

    return *this;
  }

  /// @brief Get a const reference to the from key
  const from_key_t& from_key() const noexcept
  {
    return from_key_;
  }

  /// @brief Set the from key
  DatabaseLookup& from_key(from_key_t key)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    check_key(key, "from", ex);

    from_key_ = std::forward<from_key_t>(key);

    return *this;
  }

  /// @brief Get a const reference to the flags
  const flags_t& flags() const noexcept
  {
    return flags_;
  }

  /// @brief Get the lookup flags for this message
  /// @detail Used for determining the type of DatabaseLookup (Normal, RI, LS, Explore)
  LookupFlags lookup_flags() const noexcept
  {
    return static_cast<LookupFlags>(flags_ & tini2p::under_cast(LookupFlagMask));
  }

  /// @brief Get a const reference to the reply tunnel ID
  /// @detail Used to identify the reply tunnel for DatabaseLookup responses. Zero indicates a direct response.
  const tunnel_id_t& reply_tunnel_id() const noexcept
  {
    return reply_tunnel_id_;
  }

  /// @brief Set the from key
  DatabaseLookup& reply_tunnel_id(tunnel_id_t id)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    reply_tunnel_id_ = std::forward<tunnel_id_t>(id);

    const auto& deliver_int =
        reply_tunnel_id_ == 0 ? tini2p::under_cast(DeliveryFlags::Direct) : tini2p::under_cast(DeliveryFlags::Tunnel);

    const auto& enc_int = flags_ & tini2p::under_cast(EncryptionFlagMask);

    flags_ = (flags_ & tini2p::under_cast(LookupFlagMask)) | enc_int | deliver_int;

    return *this;
  }

  /// @brief Get a const reference to the list of excluded peers
  /// @detail Used for excluding peers from list of closest floodfills in a DatabaseSearchReply
  const std::vector<ex_peer_key_t>& excluded_peers() const noexcept
  {
    return ex_peers_;
  }

  /// @brief Set the list of excluded peers
  DatabaseLookup& excluded_peers(std::vector<ex_peer_key_t> peers)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    check_peers(peers, ex);

    for (const auto& peer : peers)
      {
        if (peer.is_zero())
          flags_ |= tini2p::under_cast(LookupFlags::Explore);
      }

    std::scoped_lock sgd(peer_mutex_);
    ex_peers_ = std::forward<std::vector<ex_peer_key_t>>(peers);

    return *this;
  }

  /// @brief Add an excluded peer
  /// @detail Sets the explore lookup flag if peer key is null, see spec
  /// @param peer Excluded peer key (Identity or Destination hash)
  /// @throw Logic error if maximum excluded peer limit exceeded
  DatabaseLookup& add_excluded_peer(ex_peer_key_t peer)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    std::scoped_lock sgd(peer_mutex_);

    if (ex_peers_.size() == MaxExcludedPeers)
      ex.throw_ex<std::logic_error>("exceeds max excluded peers.");

    if (peer.is_zero())
      flags_ |= tini2p::under_cast(LookupFlags::Explore);

    ex_peers_.emplace_back(std::forward<ex_peer_key_t>(peer));

    return *this;
  }

  /// @brief Get a const reference to the reply key
  /// @detail Used for encrypting DatabaseLookup responses
  const reply_key_t& reply_key() const noexcept
  {
    return reply_key_;
  }

  /// @brief Set encryption reply key and session tag(s)
  /// @detail Sets the encryption flag
  /// @param key Reply session key
  /// @param tags Reply session tags
  /// @throw Invalid argument for null reply key, null reply tag(s), and/or invalid amount of reply tags
  DatabaseLookup& set_encrypted_data(reply_key_t key, std::vector<reply_tag_t> tags)
  {
    const exception::Exception ex{"DatabaseLookup", __func__};

    check_key(key, "reply", ex);

    const auto tags_len = tags.size();
    if (tags_len < MinReplyTags || tags_len > MaxReplyTags)
      ex.throw_ex<std::invalid_argument>("invalid tags size: " + std::to_string(tags_len));

    for (const auto& tag : tags)
      {
        if (tag.is_zero())
          ex.throw_ex<std::invalid_argument>("null reply tag.");
      }

    flags_ |= tini2p::under_cast(EncryptionFlags::Encrypted);
    reply_key_ = std::forward<reply_key_t>(key);
    reply_tags_ = std::forward<std::vector<reply_tag_t>>(tags);

    return *this;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another DatabaseLookup message
  std::uint8_t operator==(const DatabaseLookup& oth) const
  {  // attempt constant-time comparison
    const auto search_eq = static_cast<std::uint8_t>(search_key_ == oth.search_key_);
    const auto from_eq = static_cast<std::uint8_t>(from_key_ == oth.from_key_);
    const auto flags_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);
    const auto rep_tun_eq = static_cast<std::uint8_t>(reply_tunnel_id_ == oth.reply_tunnel_id_);
    const auto ex_peers_eq = static_cast<std::uint8_t>(ex_peers_ == oth.ex_peers_);
    const auto rep_key_eq = static_cast<std::uint8_t>(reply_key_ == oth.reply_key_);
    const auto rep_tags_eq = static_cast<std::uint8_t>(reply_tags_ == oth.reply_tags_);

    return (search_eq * from_eq * flags_eq * rep_tun_eq * ex_peers_eq * rep_key_eq * rep_tags_eq);
  }

 private:
  void check_params(const exception::Exception& ex)
  {
    check_key(search_key_, "search", ex);
    check_key(from_key_, "from", ex);

    if (flags_ & ReservedFlagMask)
      ex.throw_ex<std::logic_error>("reserved flags: " + std::to_string(flags_));

    if (ex_peers_.size() > MaxExcludedPeers)
      ex.throw_ex<std::logic_error>("invalid excluded peers size: " + std::to_string(ex_peers_.size()));

    if (reply_tags_.size() > MaxReplyTags)
      ex.throw_ex<std::logic_error>("invalid reply tags size: " + std::to_string(reply_tags_.size()));
  }

  void check_peers(const std::vector<ex_peer_key_t>& peers, const exception::Exception& ex) const
  {
    if (peers.size() > MaxExcludedPeers)
      {
        ex.throw_ex<std::logic_error>(
            "exceeds max excluded peers: " + std::to_string(tini2p::under_cast(MaxExcludedPeers)));
      }
  }

  template <class TKey>
  void check_key(const TKey& key, const std::string& name, const exception::Exception& ex) const
  {
    if (key.is_zero())
      ex.throw_ex<std::logic_error>("null " + name + " key");
  }

  void check_flags(
      const DeliveryFlags& dlv_flag = DeliveryFlags::Direct,
      const EncryptionFlags& enc_flag = EncryptionFlags::Unencrypted,
      const LookupFlags& lookup_flag = LookupFlags::Normal,
      const exception::Exception& ex = {"DatabaseLookup", "check_flags"})
  {
    switch(dlv_flag)
    {
      case DeliveryFlags::Direct:
      case DeliveryFlags::Tunnel:
        break;
      default:
        ex.throw_ex<std::invalid_argument>("invalid delivery flag: " + std::to_string(tini2p::under_cast(dlv_flag)));
    }

    switch(enc_flag)
    {
      case EncryptionFlags::Unencrypted:
      case EncryptionFlags::Encrypted:
        break;
      default:
        ex.throw_ex<std::invalid_argument>("invalid encryption flag: " + std::to_string(tini2p::under_cast(enc_flag)));
    }

    switch(lookup_flag)
    {
      case LookupFlags::Normal:
      case LookupFlags::LS:
      case LookupFlags::RI:
      case LookupFlags::Explore:
        break;
      default:
        ex.throw_ex<std::invalid_argument>("invalid lookup flag: " + std::to_string(tini2p::under_cast(lookup_flag)));
    }
  }

  search_key_t search_key_;
  from_key_t from_key_;
  flags_t flags_;
  tunnel_id_t reply_tunnel_id_;

  std::vector<ex_peer_key_t> ex_peers_;
  std::shared_mutex peer_mutex_;

  reply_key_t reply_key_;
  std::vector<reply_tag_t> reply_tags_;
  std::mutex tag_mutex_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_DATABASE_LOOKUP_H_
