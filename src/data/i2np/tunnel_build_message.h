/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_I2NP_TUNNEL_BUILD_MESSAGE_H_
#define SRC_DATA_I2NP_TUNNEL_BUILD_MESSAGE_H_

#include <shared_mutex>

#include "src/crypto/tunnel/aes.h"
#include "src/crypto/tunnel/chacha.h"

#include "src/data/i2np/build_request_record.h"

namespace tini2p
{
namespace data
{
/// @class TunnelBuildMessage
/// @brief Variable tunnel build message implementation
/// @tparam TSym Symmetric crypto type for reply, layer, and random keys
class TunnelBuildMessage
{
 public:
  enum
  {
    SizeLen = 1,
    MinRecords = 1,
    MaxRecords = 8,
    MinLen = 529,  //< 1 + Encrypted BuildRequestRecord length
    MaxLen = 4225,  //< 1 + (8 * Encrypted BuildRequestRecord length)
    RecordLen = 528,  //< Encrypted BuildRequestRecord length
  };

  using aes_t = crypto::AES;  //< AES trait alias
  using chacha_t = crypto::ChaCha20;  //< ChaCha trait alias
  using aes_layer_t = crypto::TunnelAES;  //< AES layer encryption trait alias
  using chacha_layer_t = crypto::TunnelChaCha;  //< ChaCha layer encryption trait alias
  using num_records_t = std::uint8_t;  //< Number of records trait alias
  using build_record_t = data::BuildRequestRecord;  //< Build request record trait alias
  using deserialize_mode_t = build_record_t::deserialize_mode_t;  //< Deserialize mode trait alias
  using trunc_ident_t = build_record_t::trunc_ident_t;  //< Truncated Identity hash trait alias
  using records_t = std::vector<build_record_t>;  //< Build request records collection trait alias
  using size_type = std::uint16_t;  //< Size type trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor, creates an empty TunnelBuildMessage
  /// @detail Caller must add at least one BuildRequestRecord before serializing
  TunnelBuildMessage() : records_() {}

  /// @brief Fully initializign ctor, creates a TunnelBuildMessage from BuildRequestRecords
  /// @detail Caller must ensure records are encrypted and serialized to buffer before adding
  /// @param records Collection of BuildRequestRecords to add
  explicit TunnelBuildMessage(records_t records)
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    check_records(records, ex);

    records_ = std::forward<records_t>(records);

    serialize();
  }

  TunnelBuildMessage(buffer_t buf, const deserialize_mode_t& mode = deserialize_mode_t::Full)
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    check_buffer(buf, ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize(mode);
  }

  /// @brief Copy-ctor
  /// @detail Caller must ensure records are encrypted and serialized to buffer
  TunnelBuildMessage(const TunnelBuildMessage& oth)
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    check_records(oth.records_, ex);
    records_ = oth.records_;

    serialize();
  }

  /// @brief Move-ctor
  /// @detail Caller must ensure records are encrypted and serialized to buffer
  TunnelBuildMessage(TunnelBuildMessage&& oth)
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    check_records(oth.records_, ex);
    records_ = std::move(oth.records_);

    serialize();
  }

  /// @brief Forwarding-assignment operator
  /// @detail Caller must ensure records are encrypted and serialized to buffer
  TunnelBuildMessage& operator=(TunnelBuildMessage oth)
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    check_records(oth.records_, ex);

    {  // lock records
      std::scoped_lock sgd(records_mutex_);
      records_ = std::forward<records_t>(oth.records_);
    } // end-records-lock

    serialize();

    return *this;
  }

  /// @brief Serialize the TunnelBuildMessage to the buffer
  void serialize()
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    std::shared_lock sr(records_mutex_, std::defer_lock);
    std::scoped_lock sgd(sr);

    check_record_num(records_.size(), ex);

    buf_.resize(size(records_, ex));

    tini2p::BytesWriter<buffer_t> writer(buf_);

    // write the number of BuildRequestRecords in the message
    writer.write_bytes(static_cast<num_records_t>(records_.size()));

    for (const auto& record : records_)
      writer.write_data(record.buffer());  // write each record
  }

  /// @brief Deserialize the TunnelBuildMessage from the buffer
  void deserialize(const deserialize_mode_t& mode = deserialize_mode_t::Full)
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    check_buffer(buf_, ex);

    tini2p::BytesReader<buffer_t> reader(buf_);

    num_records_t num_records;
    reader.read_bytes(num_records);
    check_record_num(num_records, ex);

    records_t records;
    for (num_records_t i = 0; i < num_records; ++i)
      {  // partially deserialize the encrypted records
        typename build_record_t::buffer_t buf;

        reader.read_data(buf);

        // check for duplicate records
        build_record_t record(std::move(buf), mode);
        check_additional_record(records, record.truncated_ident(), ex);

        records.emplace_back(std::move(record));
      }

    std::scoped_lock sgd(records_mutex_);
    records_.swap(records);
  }

  /// @brief Get the size of the TunnelBuildMessage
  size_type size()
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    std::shared_lock sr(records_mutex_, std::defer_lock);
    std::scoped_lock sgd(sr);

    return size(records_, ex);
  }

  /// @brief Add a single BuildRequestRecord to the TunnelBuildMessage
  /// @detail Caller must ensure record is encrypted and serialized to buffer
  void add_record(build_record_t record)
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    std::scoped_lock sgd(records_mutex_);

    check_additional_record(records_, record.truncated_ident(), ex);

    records_.emplace_back(std::forward<build_record_t>(record));
  }

  /// @brief Find a BuildRequestRecord by its truncated Identity hash
  /// @throws Logic error if no record found
  /// @returns A non-const reference to the found record
  template <class TID>
  build_record_t& find_record(const TID& id)
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    std::shared_lock sr(records_mutex_, std::defer_lock);
    std::scoped_lock sgd(sr);

    return *find_record(id, ex);
  }

  /// @brief Encrypt other records not matching the truncated Identity hash
  /// @param trunc_ident The owning hop's truncated Identity hash
  /// @param layer The owning hop's layer encryption
  TunnelBuildMessage& EncryptOtherRecords(const trunc_ident_t& trunc_ident, aes_layer_t& layer)
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    std::scoped_lock sgd(records_mutex_);

    auto record_it = find_record(trunc_ident, ex);

    for (auto it = records_.begin(); it != records_.end(); ++it)
      {
        if (it != record_it)
          layer.EncryptRecord(it->buffer());
      }

    return *this;
  }

  /// @brief Encrypt other records not matching the truncated Identity hash
  /// @param trunc_ident The owning hop's truncated Identity hash
  /// @param layer The owning hop's layer encryption
  TunnelBuildMessage& EncryptOtherRecords(const trunc_ident_t& trunc_ident, chacha_layer_t& layer)
  {
    const exception::Exception ex{"TunnelBuildMessage", __func__};

    std::scoped_lock sgd(records_mutex_);

    auto record_it = find_record(trunc_ident, ex);
    const auto record_begin = records_.begin();
    const auto record_end = records_.end();
    const auto& records_len = records_.size();

    for (auto it = record_begin; it != record_end; ++it)
      {
        if (it != record_it)
          layer.EncryptRecord(it->buffer(), it - record_begin, records_len);
      }

    return *this;
  }

  /// @brief Get a const reference to the BuildRequestRecords
  const records_t& records() const noexcept
  {
    return records_;
  }

  /// @brief Get a non-const reference to the BuildRequestRecords
  records_t& records() noexcept
  {
    return records_;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another TunnelBuildMessage
  std::uint8_t operator==(const TunnelBuildMessage& oth)
  {
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    std::shared_lock sr(records_mutex_, std::defer_lock);
    std::scoped_lock sgd(sr);

    const auto& rec_eq = static_cast<std::uint8_t>(records_ == oth.records_);

    return (buf_eq * rec_eq);
  }

 private:
  void check_record_num(const std::size_t& num_records, const exception::Exception& ex) const
  {
    const auto& min_records = tini2p::under_cast(MinRecords);
    const auto& max_records = tini2p::under_cast(MaxRecords);

    if (num_records < min_records || num_records > max_records)
      {
        ex.throw_ex<std::length_error>(
            "invalid number of records: " + std::to_string(num_records) + ", min: " + std::to_string(min_records)
            + ", max: " + std::to_string(max_records));
      }
  }

  void check_records(const records_t& records, const exception::Exception& ex) const
  {
    check_record_num(records.size(), ex);

    const auto& exp_record_len = tini2p::under_cast(RecordLen);

    const auto& is_chacha = records.front().is_chacha();
    std::vector<build_record_t::trunc_ident_t> idents;
    for (const auto& record : records)
      {
        // check for valid record size
        const auto& record_len = record.buffer().size();
        if (record_len != exp_record_len)
          {
            ex.throw_ex<std::length_error>(
                "invalid encrypted BuildRequestRecordLength: " + std::to_string(record_len)
                + ", expected: " + std::to_string(exp_record_len));
          }

        // only hops using the same layer encryption can be in the same tunnel
        // either all AES or all ChaCha
        if (record.is_chacha() != is_chacha)
          ex.throw_ex<std::logic_error>("invalid use of mixed AES and ChaCha layer encryption");

        // check for duplicate records
        const auto idents_end = idents.end();
        auto trunc_ident = record.truncated_ident();
        const auto it =
            std::find_if(idents.begin(), idents_end, [&trunc_ident](const auto& t) { return t == trunc_ident; });

        if (it != idents_end)
          {
            ex.throw_ex<std::logic_error>(
                "adding duplicate record for truncated Identity: " + tini2p::bin_to_hex(trunc_ident, ex));
          }

        idents.emplace_back(std::move(trunc_ident));
      }
  }

  void check_additional_record(
      const records_t& records,
      const typename build_record_t::trunc_ident_t& trunc_ident,
      const exception::Exception& ex) const
  {
    const auto& max_records = tini2p::under_cast(MaxRecords);

    if (records.size() == max_records)
      ex.throw_ex<std::length_error>("unable to add a record, max records: " + std::to_string(max_records) + " reached.");

    const auto record_end = records.end();
    const auto it = std::find_if(
        records.begin(), record_end, [&trunc_ident](const auto& r) { return r.truncated_ident() == trunc_ident; });

    if (it != record_end)
      {
        ex.throw_ex<std::logic_error>(
            "adding duplicate record for truncated Identity: " + tini2p::bin_to_hex(trunc_ident, ex));
      }
  }

  void check_buffer(const buffer_t& buf, const exception::Exception& ex) const
  {
    const auto& buf_len = buf.size();
    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  records_t::iterator find_record(const trunc_ident_t& trunc_ident, const exception::Exception& ex)
  {
    const auto record_begin = records_.begin();
    const auto record_end = records_.end();

    auto it = std::find_if(
        record_begin, record_end, [&trunc_ident](const auto& r) { return r.truncated_ident() == trunc_ident; });

    if (it == record_end)
      ex.throw_ex<std::logic_error>("record not found for truncated Identity: " + tini2p::bin_to_hex(trunc_ident, ex));

    return it;
  }

  records_t::iterator find_record(const std::uint8_t& idx, const exception::Exception& ex)
  {
    if (idx >= records_.size())
      ex.throw_ex<std::logic_error>("invalid record index: " + std::to_string(idx));

    auto it = records_.begin();
    std::advance(it, idx);

    return it;
  }

  size_type size(const records_t& records, const exception::Exception& ex) const
  {
    return (tini2p::under_cast(SizeLen) + (records.size() * tini2p::under_cast(RecordLen)));
  }

  records_t records_;
  std::shared_mutex records_mutex_;

  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_I2NP_TUNNEL_BUILD_MESSAGE_H_
