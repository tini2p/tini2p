/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_I2NP_BUILD_REQUEST_RECORD_H_
#define SRC_DATA_I2NP_BUILD_REQUEST_RECORD_H_

#include "src/crypto/aes.h"
#include "src/crypto/chacha20.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/x25519.h"

#include "src/data/blocks/build_options.h"
#include "src/data/router/identity.h"

namespace tini2p
{
namespace data
{
/// @struct RequestRecordParams
/// @brief Convenience struct for BuildRequestRecord parameters
struct RequestRecordParams
{
  using tunnel_id_t = std::uint32_t;
  using message_id_t = std::uint32_t;
  using ident_hash_t = data::Identity::hash_t;

  tunnel_id_t tunnel_id;
  ident_hash_t local_ident;
  tunnel_id_t next_tunnel_id;
  ident_hash_t next_ident;
  ident_hash_t hop_ident;
  message_id_t next_message_id;
};

/// @class BuildRequestRecord
/// @brief Class for creating and processing ECIES-X25519 BuildRequestRecords
/// @tparam TSym Symmetric crypto type for reply, layer, and random keys
class BuildRequestRecord
{
 public:
  enum : std::uint32_t
  {
    NullID = 0,
    FlagsLen = 1,
    RecordLen = 528,  //< to match ElGamal BRR, may change when all ECIES, TBD
    EncryptedHeaderLen = 48,  //< Truncated Identity Hash (16) + Creator ephemeral public key (32)
    UnencryptedLen = 464,  //< Unencrypted BRR, not including encrypted header + MAC
    EncryptedLen = 480,  //< Encrypted BRR, not including the encrypted header
    OptionsPaddingLen = 379,  //< 464 - MainRecordInfo(85)
    Expiration = 10,  //< in minutes, time past creation when the request expires
    ElGRequestSkew = 90,  //< in seconds, accounts for network skew, and creation time close to the hour
    RequestSkew = 90,  //< in seconds, accounts for network skew
    ExpireSkew = 90,  //< in seconds, accounts for network skew
    MinNextMsgID = 1,
    MaxNextMsgID = 0xFFFFFFFF,
  };

  /// @brief Build Request Flags
  enum struct Flags : std::uint8_t
  {
    Hop = 0x00,                           //< Tunnel hop, 0000 0000, see spec
    IBGW = 0x80,  //< Inbound Gateway, allow from anyone, 1000 0000, see spec
    OBEP = 0x40,  //< Outbound Endpoint, allow to anyone, 0100 0000, see spec
    ChaChaLayer = 0x20,      //< ChaCha layer encryption, 0010 0000, see spec
    ChaChaHop = 0x20,
    ChaChaIBGW = 0xA0,
    ChaChaOBEP = 0x60,
    ReservedMask = 0x1F,        //< Reserved/unused bits, 0001 1111, see spec
  };

  enum struct DeserializeMode : std::uint8_t
  {
    Partial,
    Full,
  };

  using curve_t = crypto::X25519;  // EC curve trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD trait alias
  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using ident_t = data::Identity;  //< Identity trait alias
  using ident_hash_t = ident_t::hash_t;  //< Identity hash trait alias
  using trunc_ident_t = ident_t::trunc_hash_t;  //< Truncated Identity hash trait alias
  using params_t = RequestRecordParams;  //< Parameters trait alias
  using flags_t = Flags;  //< Flags trait alias
  using deserialize_mode_t = DeserializeMode;  //< Deserialization flags trait alias
  using elg_request_time_t = std::uint32_t;  //< ElGamal request time (hours) trait alias
  using request_time_t = std::uint32_t;  //< Request time (minutes) trait alias
  using expiration_t = std::uint32_t;  //< Expiration time (minutes) trait alias
  using message_id_t = std::uint32_t;  //< Next message ID trait alias
  using options_t = data::BuildOptionsBlock;  //< Build options trait alias
  using padding_t = data::PaddingBlock;  //< Random padding block trait alias
  using size_type = std::uint16_t;  //< Size type trait alias
  using buffer_t = crypto::FixedSecBytes<RecordLen>;  //< Buffer trait alias

  /// @brief Fully-initializing ctor, creates a BuildRequestRecord from tunnel IDs and ident hashes
  /// @detail Sets creation time and default expiration (10 min. from creation)
  /// @param tun_id Tunnel ID (non-zero) for this hop to receive messages
  /// @param loc_ident Local identity hash
  /// @param next_tun_id Tunnel ID (non-zero) of the next hop or reply tunnel gateway
  /// @param next_ident Idnentity hash of the next hop in the tunnel
  /// @param hop_ident Hop's Identity hash
  /// @param creator_key Tunnel creator's public ephemeral key for this BuildRequestRecord
  /// @param flags Flags for indicating the participant type (Hop, OutboundEndpoint, InboundGateway)
  // TODO(tini2p): restrict to ECIES-only when generalizing for ElG
  BuildRequestRecord(params_t params, curve_t::pubkey_t creator_key, flags_t flags = flags_t::Hop)
      : trunc_ident_(),
        creator_key_(),
        tun_id_(params.tunnel_id),
        local_ident_(),
        next_tun_id_(params.next_tunnel_id),
        next_ident_(),
        flags_(),
        time_(tini2p::time::now_min()),
        expiration_(time_ + tini2p::under_cast(Expiration)),
        elg_time_(tini2p::time::now_hr()),
        next_msg_id_(params.next_message_id),
        options_(),
        padding_(get_padding_len()),
        buf_()
  {
    const exception::Exception ex{"BuildRequestRecord", __func__};

    check_tunnel_ids(tun_id_, next_tun_id_, ex);
    check_ident_hash(params.local_ident, ex);
    check_ident_hash(params.next_ident, ex);
    check_all_idents(params.local_ident, params.next_ident, params.hop_ident, ex);
    check_creator_key(creator_key, ex);
    check_flags(flags, ex);

    local_ident_ = std::forward<ident_hash_t>(params.local_ident);
    next_ident_ = std::forward<ident_hash_t>(params.next_ident);
    trunc_ident_ = static_cast<trunc_ident_t>(params.hop_ident);
    creator_key_ = std::forward<curve_t::pubkey_t>(creator_key);
    flags_ = std::forward<flags_t>(flags);

    serialize();
  }

  /// @brief Fully-initializing ctor, creates a ChaCha BuildRequestRecord from tunnel IDs and ident hashes
  /// @detail Sets creation time and default expiration (10 min. from creation)
  /// @param tun_id Tunnel ID (non-zero) for this hop to receive messages
  /// @param loc_ident Local identity hash
  /// @param next_tun_id Tunnel ID (non-zero) of the next hop or reply tunnel gateway
  /// @param next_ident Idnentity hash of the next hop in the tunnel
  /// @param hop_ident Hop's Identity hash
  /// @param creator_key Tunnel creator's public ephemeral key for this BuildRequestRecord
  /// @param receive_key AEAD receive key for this BuildRequestRecord
  /// @param flags Flags for indicating the participant type (Hop, OutboundEndpoint, InboundGateway)
  // TODO(tini2p): restrict to ECIES-only when generalizing for ElG
  BuildRequestRecord(
      params_t params,
      curve_t::pubkey_t creator_key,
      aead_t::key_t receive_key,
      flags_t flags = flags_t::Hop)
      : trunc_ident_(),
        tun_id_(params.tunnel_id),
        local_ident_(),
        next_tun_id_(params.next_tunnel_id),
        next_ident_(),
        flags_(),
        time_(tini2p::time::now_min()),
        expiration_(time_ + tini2p::under_cast(Expiration)),
        elg_time_(tini2p::time::now_hr()),
        next_msg_id_(params.next_message_id),
        options_(std::forward<aead_t::key_t>(receive_key)),
        padding_(get_padding_len()),
        buf_()
  {
    const exception::Exception ex{"BuildRequestRecord", __func__};

    check_tunnel_ids(tun_id_, next_tun_id_, ex);
    check_ident_hash(params.local_ident, ex);
    check_ident_hash(params.next_ident, ex);
    check_all_idents(params.local_ident, params.next_ident, params.hop_ident, ex);
    check_creator_key(creator_key, ex);
    check_flags(flags, ex);

    local_ident_ = std::forward<ident_hash_t>(params.local_ident);
    next_ident_ = std::forward<ident_hash_t>(params.next_ident);
    trunc_ident_ = static_cast<trunc_ident_t>(params.hop_ident);
    creator_key_ = std::forward<curve_t::pubkey_t>(creator_key);
    flags_ = std::forward<flags_t>(flags);

    serialize();
  }

  /// @brief Deserializing ctor, used by tunnel hop to decrypt a received BuildRequestRecord
  /// @detail Optionally, partially deserializes encrypted BuildRequestRecord from buffer.
  ///     Used by the TunnelBuildMessage for the caller to search for their record.
  ///     The caller then decrypts the record containing their truncated Identity hash.
  /// @param buf BuildRequestRecord buffer
  /// @param deserialize_flag Flag for partially or fully deserializing the record
  BuildRequestRecord(buffer_t buf, const deserialize_mode_t& deserialize_mode = deserialize_mode_t::Full)
  {
    const exception::Exception ex{"BuildRequestRecord", __func__};

    buf_ = std::forward<buffer_t>(buf);

    if (deserialize_mode == deserialize_mode_t::Partial)
      {
        tini2p::BytesReader<buffer_t> reader(buf_);
        deserialize_header(reader, ex);
      }
    else if (deserialize_mode == deserialize_mode_t::Full)
      deserialize();
    else
      ex.throw_ex<std::invalid_argument>("invalid deserialization flag.");
  }

  /// @brief Serialize and encrypt the BuildRequestRecord to buffer
  void serialize()
  {
    const exception::Exception ex{"BuildRequestRecord", __func__};

    check_tunnel_ids(tun_id_, next_tun_id_, ex);
    check_ident_hash(local_ident_, ex);
    check_ident_hash(next_ident_, ex);
    // TODO(tini2p): only check padding when generalizing for ElG
    check_options_and_padding(options_, padding_, ex);

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_data(trunc_ident_);
    writer.write_data(creator_key_);
    writer.write_bytes(tun_id_, tini2p::Endian::Big);
    writer.write_data(local_ident_);
    writer.write_bytes(next_tun_id_, tini2p::Endian::Big);
    writer.write_data(next_ident_);
    writer.write_bytes(flags_);
    // TODO(tini2p): when generalizing for ElG records, only write ElG request time (hour resolution)
    writer.write_bytes(time_, tini2p::Endian::Big);
    writer.write_bytes(expiration_, tini2p::Endian::Big);
    writer.write_bytes(next_msg_id_, tini2p::Endian::Big);

    // TODO(tini2p): only write padding when generalizing for ElG
    options_.serialize();
    writer.write_data(options_.buffer());

    padding_.serialize();
    writer.write_data(padding_.buffer());
  }

  /// @brief Decrypt and deserialize the BuildRequestRecor from buffer
  void deserialize()
  {
    const exception::Exception ex{"BuildRequestRecord", __func__};

    check_buffer(buf_, tini2p::under_cast(RecordLen), ex);

    tini2p::BytesReader<buffer_t> reader(buf_);

    // read and check the record header
    deserialize_header(reader, ex);

    // read and check the tunnel ID
    tunnel_id_t tun_id;
    reader.read_bytes(tun_id, tini2p::Endian::Big);
    check_tunnel_id(tun_id, ex);
    tun_id_ = std::move(tun_id);

    // read and check the creator's Identity hash
    ident_hash_t ident_hash;
    reader.read_data(ident_hash);
    check_ident_hash(ident_hash, ex);
    local_ident_.swap(ident_hash);

    // read and check the next tunnel ID
    tunnel_id_t next_tun_id;
    reader.read_bytes(next_tun_id, tini2p::Endian::Big);
    check_tunnel_id(next_tun_id, ex);
    next_tun_id_ = std::move(next_tun_id);

    check_tunnel_ids(tun_id_, next_tun_id_, ex);

    // read and check the next Identity hash
    reader.read_data(ident_hash);
    check_ident_hash(ident_hash, ex);
    next_ident_.swap(ident_hash);

    // read and check the flags
    flags_t flags;
    reader.read_bytes(flags);
    check_flags(flags, ex);
    flags_ = flags;

    // TODO(tini2p): when generalizing for ElG, only read ElG request time (hour resolution)
    // read and check the request time
    request_time_t time;
    reader.read_bytes(time, tini2p::Endian::Big);
    check_time(time, ex);
    time_ = std::move(time);

    // read and check the request expiration time
    expiration_t expiration;
    reader.read_bytes(expiration, tini2p::Endian::Big);
    check_expiration(expiration, ex);
    expiration_ = std::move(expiration);

    // read and check the next message ID
    message_id_t msg_id;
    reader.read_bytes(msg_id, tini2p::Endian::Big);
    // do we need a check for the next message ID?
    next_msg_id_ = msg_id;

    // TODO(tini2p): only read padding when generalizing for ElG
    // read and check the options block
    options_t::type_t opt_type;
    reader.read_bytes(opt_type);

    if (opt_type != options_t::type_t::BuildOptions)
      ex.throw_ex<std::logic_error>("invalid tunnel build options type.");
    
    options_t::size_type opt_size;
    reader.read_bytes(opt_size, tini2p::Endian::Big);

    const auto& block_header_len = static_cast<std::size_t>(options_t::HeaderLen);
    const auto& full_opt_len = block_header_len + opt_size;
    std::int32_t remaining = reader.gcount() - tini2p::under_cast(aead_t::MACLen);
    if (static_cast<std::int32_t>(full_opt_len) > remaining)
      {
        ex.throw_ex<std::length_error>(
            "invalid options length: " + std::to_string(full_opt_len)
            + ", remaining record: " + std::to_string(remaining));
      }

    // skip back to beginning of options block
    reader.skip_back(block_header_len);

    options_t::buffer_t opt_buf(full_opt_len);
    reader.read_data(opt_buf);
    options_t opt(opt_buf);
    options_ = std::move(opt);

    // check for a padding block
    remaining = reader.gcount() - tini2p::under_cast(aead_t::MACLen);
    if (remaining > 0)
      {  // read remaining data as padding
        padding_t::type_t type;
        reader.read_bytes(type);
        if (type != padding_t::type_t::Padding)
          ex.throw_ex<std::logic_error>("invalid padding block type.");

        padding_t::size_type pad_len;
        reader.read_bytes(pad_len, tini2p::Endian::Big);
        const auto& full_pad_len = block_header_len + pad_len;
        if (static_cast<std::int32_t>(full_pad_len) > remaining)
          {
            ex.throw_ex<std::length_error>(
                "invalid padding length: " + std::to_string(full_pad_len)
                + ", remaining record: " + std::to_string(remaining));
          }

        // skip back to beginning of padding block
        reader.skip_back(block_header_len);

        padding_t::buffer_t pad_buf(full_pad_len);
        reader.read_data(pad_buf);

        padding_ = padding_t(std::move(pad_buf));
      }
  }

  /// @brief Get a const reference to this hop's truncated Identity hash
  const trunc_ident_t& truncated_ident() const noexcept
  {
    return trunc_ident_;
  }

  /// @brief Get a const reference to the tunnel creator's public ephemeral key
  const curve_t::pubkey_t& creator_key() const noexcept
  {
    return creator_key_;
  }

  /// @brief Get a const reference to the tunnel hop's AEAD receive key
  const aead_t::key_t& receive_key() const
  {
    return options_.receive_key();
  }

  /// @brief Get a const reference to this hop's tunnel ID
  const tunnel_id_t& tunnel_id() const noexcept
  {
    return tun_id_;
  }

  /// @brief Get a const reference to the local Identity hash
  const ident_hash_t& local_ident_hash() const noexcept
  {
    return local_ident_;
  }

  /// @brief Get a const reference to the next hop's tunnel ID
  const tunnel_id_t& next_tunnel_id() const noexcept
  {
    return next_tun_id_;
  }

  /// @brief Get a const reference to the next hop's Identity hash
  const ident_hash_t& next_ident_hash() const noexcept
  {
    return next_ident_;
  }

  /// @brief Get a const reference to the record flags
  const flags_t& flags() const noexcept
  {
    return flags_;
  }

  /// @brief Set the request flags
  BuildRequestRecord& flags(flags_t flags)
  {
    const exception::Exception ex{"BuildRequestRecord", __func__};

    check_flags(flags, ex);

    flags_ = std::forward<flags_t>(flags);

    return *this;
  }

  /// @brief Get whether the request is for an outbound gateway
  std::uint8_t is_obep() const
  {
    return static_cast<std::uint8_t>(
        static_cast<flags_t>(tini2p::under_cast(flags_) & tini2p::under_cast(flags_t::OBEP)) == flags_t::OBEP);
  }

  /// @brief Get whether the request is for ChaCha layer encryption
  std::uint8_t is_chacha() const
  {
    return static_cast<std::uint8_t>(
        static_cast<flags_t>(tini2p::under_cast(flags_) & tini2p::under_cast(flags_t::ChaChaLayer))
        == flags_t::ChaChaLayer);
  }

  // TODO(tini2p): write a getter for ElG request time (in hours)

  /// @brief Get a const reference to the creation time (in minutes)
  /// TODO(tini2p): restrict to ChaCha-only when generalizing for ElG
  const request_time_t& request_time() const noexcept
  {
    return time_;
  }

  /// @brief Get a const reference to the expiration time (in minutes)
  /// TODO(tini2p): restrict to ChaCha-only when generalizing for ElG
  const expiration_t& expiration() const noexcept
  {
    return expiration_;
  }

  /// @brief Get a const reference to the next message ID received in the established tunnel
  const message_id_t& next_message_id() const noexcept
  {
    return next_msg_id_;
  }

  /// @brief Get a const reference to the next message ID received in the established tunnel
  BuildRequestRecord& next_message_id(message_id_t next_id)
  {
    const exception::Exception ex{"BuildRequestRecord", __func__};

    check_next_message_id(next_id, ex);

    next_msg_id_ = std::forward<message_id_t>(next_id);

    return *this;
  }

  /// @brief Get a const reference to the tunnel build options
  const options_t& options() const noexcept
  {
    return options_;
  }

  /// @brief Get a const reference to the random padding
  const padding_t& padding() const noexcept
  {
    return padding_;
  }

  /// @brief Get size of the encrypted BuildRequestRecord
  size_type size() const noexcept
  {
    return tini2p::under_cast(RecordLen);
  }

  /// @brief Get size of the decrypted BuildRequestRecord
  size_type clear_size() const noexcept
  {
    return tini2p::under_cast(UnencryptedLen);
  }

  /// @brief Get a const reference to the record buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the record buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another BuildRequestRecord
  std::uint8_t operator==(const BuildRequestRecord& oth) const
  {  // attempt constant-time comparison
     const auto& trunc_eq =  static_cast<std::uint8_t>(trunc_ident_ == oth.trunc_ident_);
     const auto& tun_eq = static_cast<std::uint8_t>(tun_id_ == oth.tun_id_);
     const auto& local_ident_eq = static_cast<std::uint8_t>(local_ident_ == oth.local_ident_);
     const auto& next_tun_eq = static_cast<std::uint8_t>(next_tun_id_ == oth.next_tun_id_);
     const auto& next_ident_eq = static_cast<std::uint8_t>(next_ident_ == oth.next_ident_);
     const auto& flags_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);
     const auto& time_eq = static_cast<std::uint8_t>(time_ == oth.time_);
     const auto& next_msg_id_eq = static_cast<std::uint8_t>(next_msg_id_ == oth.next_msg_id_);
     const auto& opt_eq = static_cast<std::uint8_t>(options_ == oth.options_);
     const auto& pad_eq = static_cast<std::uint8_t>(padding_ == oth.padding_);

     return (
         trunc_eq * tun_eq * local_ident_eq * next_tun_eq * next_ident_eq * flags_eq * time_eq * next_msg_id_eq * opt_eq
         * pad_eq);
  }

 private:
  void deserialize_header(tini2p::BytesReader<buffer_t>& reader, const exception::Exception& ex)
  {
    // read and check the truncated Identity hash
    trunc_ident_t trunc_ident;
    reader.read_data(trunc_ident);

    if (trunc_ident.is_zero())
      ex.throw_ex<std::logic_error>("null truncated Identity hash.");

    trunc_ident_.swap(trunc_ident);

    // read and check the tunnel creator's ephemeral public key
    curve_t::pubkey_t creator_key;
    reader.read_data(creator_key);

    if (creator_key.is_zero())
      ex.throw_ex<std::logic_error>("null tunnel creator ephemeral public key.");

    creator_key_.swap(creator_key);
  }

  padding_t::size_type get_padding_len() const noexcept
  {
    return tini2p::under_cast(OptionsPaddingLen) - tini2p::under_cast(padding_t::HeaderLen) - options_.size();
  }

  void check_tunnel_id(const tunnel_id_t& tun_id, const exception::Exception& ex) const
  {
    if (tun_id == tini2p::under_cast(NullID))
      ex.throw_ex<std::invalid_argument>("null tunnel ID.");
  }

  void check_tunnel_ids(const tunnel_id_t& tun_id, const tunnel_id_t& next_tun_id, const exception::Exception& ex) const
  {
    check_tunnel_id(tun_id, ex);
    check_tunnel_id(next_tun_id, ex);

    if (tun_id == next_tun_id)
      ex.throw_ex<std::logic_error>("next tunnel ID must be unique from tunnel ID.");
  }

  void check_ident_hash(const ident_hash_t& ident, const exception::Exception& ex) const
  {
    if (ident.is_zero())
      ex.throw_ex<std::invalid_argument>("null Identity hash.");
  }

  void check_truncated_ident(const trunc_ident_t& trunc_ident, const exception::Exception& ex) const
  {
    if (trunc_ident.is_zero())
      ex.throw_ex<std::invalid_argument>("null truncated Identity hash.");
  }

  void check_all_idents(
      const ident_hash_t& local_ident,
      const ident_hash_t& next_ident,
      const ident_hash_t& hop_ident,
      const exception::Exception& ex) const
  {
    if (local_ident == hop_ident)
      ex.throw_ex<std::logic_error>("local Identity hash matches hop Identity hash.");

    if (next_ident == hop_ident)
      ex.throw_ex<std::logic_error>("next Identity hash matches hop Identity hash.");

    check_truncated_ident(static_cast<trunc_ident_t>(hop_ident), ex);
  }

  void check_creator_key(const curve_t::pubkey_t& creator_key, const exception::Exception& ex) const
  {
    if (creator_key.is_zero())
      ex.throw_ex<std::logic_error>("null creator public key.");
  }

  void check_flags(const flags_t& flags, const exception::Exception& ex) const
  {
    if (tini2p::under_cast(flags) & tini2p::under_cast(flags_t::ReservedMask))
      ex.throw_ex<std::logic_error>("use of reserved flags.");
  }

  void check_time(const request_time_t& time, const exception::Exception& ex) const
  {
    const auto time_sec = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::minutes(time)).count();

    const auto now_sec = tini2p::time::now_s() - tini2p::under_cast(RequestSkew);

    if (time_sec < now_sec)
      {
        ex.throw_ex<std::logic_error>(
            "record creation time: " + std::to_string(time_sec)
            + " is too far past the creation skew: " + std::to_string(now_sec));
      }
  }

  void check_expiration(const expiration_t& time, const exception::Exception& ex) const
  {
    const auto expire_sec = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::minutes(time)).count();
    const auto now_sec = tini2p::time::now_s() - tini2p::under_cast(ExpireSkew);

    if (expire_sec < now_sec)
      {
        ex.throw_ex<std::logic_error>(
            "record expiration time: " + std::to_string(expire_sec)
            + " is too far past the expiration skew: " + std::to_string(now_sec));
      }
  }

  // TODO(tini2p): write check for ElG request time

  void check_options_and_padding(const options_t& options, const padding_t& padding, const exception::Exception& ex)
      const
  {
    const auto& opt_len = options.size();
    const auto& pad_len = padding.size();
    const auto& exp_len = tini2p::under_cast(OptionsPaddingLen);

    if (opt_len + pad_len != exp_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid trailing options and padding length: " + std::to_string(opt_len + pad_len)
            + ", options: " + std::to_string(opt_len) + ", padding: " + std::to_string(pad_len)
            + ", total expected: " + std::to_string(exp_len));
      }
  }

  void check_buffer(const buffer_t& buf, const std::size_t expected_len, const exception::Exception& ex) const
  {
    const auto& buf_len = buf.size();
    if (buf_len != expected_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", expected: " + std::to_string(expected_len));
      }

    if (buf.is_zero())
      ex.throw_ex<std::invalid_argument>("null buffer.");
  }

  void check_next_message_id(const message_id_t& next_id, const exception::Exception& ex) const
  {
    const auto& min_id = tini2p::under_cast(MinNextMsgID);
    const auto& max_id = tini2p::under_cast(MaxNextMsgID);

    if (next_id < min_id || next_id > max_id)
      {
        ex.throw_ex<std::logic_error>(
            "invalid next message ID: " + std::to_string(next_id) + ", min: " + std::to_string(min_id)
            + ", max: " + std::to_string(max_id));
      }
  }

  trunc_ident_t trunc_ident_;
  curve_t::pubkey_t creator_key_;
  tunnel_id_t tun_id_;
  ident_hash_t local_ident_;
  tunnel_id_t next_tun_id_;
  ident_hash_t next_ident_;
  flags_t flags_;
  request_time_t time_;
  expiration_t expiration_;
  request_time_t elg_time_;
  message_id_t next_msg_id_;
  options_t options_;
  padding_t padding_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_I2NP_BUILD_REQUEST_RECORD_H_
