/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_I2NP_DATABASE_DELIVERY_STATUS_H_
#define SRC_I2NP_DATABASE_DELIVERY_STATUS_H_

#include "src/bytes.h"
#include "src/time.h"

#include "src/crypto/sec_bytes.h"

namespace tini2p
{
namespace data
{
/// @class DeliveryStatus
class DeliveryStatus
{
 public:
  enum
  {
    MessageIDLen = 4,
    TimestampLen = 8,
    MessageLen = MessageIDLen + TimestampLen,
  };

  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using timestamp_t = std::uint64_t;  //< Timestamp trait alias
  using buffer_t = crypto::FixedSecBytes<MessageLen>;  //< Buffer trait alias

  using pointer = DeliveryStatus*;  //< Non-owning pointer trait alias
  using const_pointer = const DeliveryStatus*;  //< Const non-owning pointer trait alias
  using unique_ptr = std::unique_ptr<DeliveryStatus>;  //< Unique pointer trait alias
  using const_unique_ptr = std::unique_ptr<const DeliveryStatus>;  //< Const unique pointer trait alias
  using shared_ptr = std::shared_ptr<DeliveryStatus>;  //< Shared pointer trait alias
  using const_shared_ptr = std::shared_ptr<const DeliveryStatus>;  //< Const shared pointer trait alias

  DeliveryStatus(message_id_t message_id, timestamp_t time = tini2p::time::now_s())
      : message_id_(message_id), time_(time), buf_()
  {
    serialize();
  }

  explicit DeliveryStatus(buffer_t buf) : buf_(std::forward<buffer_t>(buf))
  {
    deserialize();
  }

  explicit DeliveryStatus(const crypto::SecBytes& buf)
  {
    from_buffer(buf);
  }

  DeliveryStatus(const std::uint8_t* data, const std::size_t len)
  {
    const exception::Exception ex{"DeliveryStatus", __func__};

    tini2p::check_cbuf(data, len, MessageLen, MessageLen, ex);

    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  void serialize()
  {
    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_bytes(message_id_, tini2p::Endian::Big);
    writer.write_bytes(time_, tini2p::Endian::Big);
  }

  void deserialize()
  {
    tini2p::BytesReader<buffer_t> reader(buf_);

    reader.read_bytes(message_id_, tini2p::Endian::Big);
    reader.read_bytes(time_, tini2p::Endian::Big);
  }

  /// @brief Create a DeliveryStatus message from a secure buffer
  void from_buffer(const crypto::SecBytes& buf)
  {
    const exception::Exception ex{"DeliveryStatus", __func__};

    const auto& buf_len = buf.size();
    if (buf_len != MessageLen)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid message size: " + std::to_string(buf_len)
            + " expected: " + std::to_string(tini2p::under_cast(MessageLen)));
      }

    std::copy_n(buf.data(), buf_len, buf_.data());
    deserialize();
  }

  std::uint8_t size() const noexcept
  {
    return MessageLen;
  }

  const message_id_t& message_id() const noexcept
  {
    return message_id_;
  }

  const timestamp_t& timestamp() const noexcept
  {
    return time_;
  }

  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another DeliveryStatus message
  std::uint8_t operator==(const DeliveryStatus& oth) const
  {  // attempt constant-time
    const auto msg_id_eq = static_cast<std::uint8_t>(message_id_ == oth.message_id_);
    const auto time_eq = static_cast<std::uint8_t>(time_ == oth.time_);

    return (msg_id_eq * time_eq);
  }

 private:
  message_id_t message_id_;
  timestamp_t time_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_DATABASE_DELIVERY_STATUS_H_
