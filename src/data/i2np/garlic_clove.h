/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_I2NP_GARLIC_CLOVE_H_
#define SRC_I2NP_GARLIC_CLOVE_H_

#include <variant>

#include "src/bytes.h"
#include "src/time.h"

#include "src/crypto/sec_bytes.h"

#include "src/data/i2np/i2np_header.h"

#include "src/data/i2np/data.h"
#include "src/data/i2np/database_store.h"
#include "src/data/i2np/delivery_status.h"
#include "src/data/i2np/garlic_delivery.h"

namespace tini2p
{
namespace data
{
/// @brief Forward declaration of I2NPMessage (recursive definition)
class I2NPMessage;

/// @class GarlicClove
/// @brief Class for creating and processing GarlicClove I2NP messages
class GarlicClove
{
  enum struct I2NPPart : std::uint8_t
  {
    Instructions,  //< the garlic kind
    Message,  //< any supported I2NP message
  };

 public:
  enum : std::uint32_t
  {
    NullID = 0,
    CloveIDLen = 4,
    Expiration = 30000,  //< in milliseconds
    ExpirationLen = 8,
    MinLen = 36,  //< GarlicDelivery::Min(1) + I2NPHeader(16) + Data::Min(4) + CloveID(4) + Expiration(8) + NullCert(3),
    MaxLen = data::I2NPHeader::MaxMessageLen,
    MinCloveID = 1,
    MaxCloveID = 0xFFFFFFFF,
  };

  using instructions_t = GarlicDeliveryInstructions;  //< Delivery instructions trait alias
  using clove_id_t = std::uint32_t;  //< Clove ID trait alias
  using expiration_t = std::uint64_t;  //< Expiration trait alias
  using cert_t = data::Certificate;  //< Certificate trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor
  GarlicClove()
      : instructions_(),
        clove_id_(crypto::RandInRange(MinCloveID, MaxCloveID)),
        i2np_header_(data::I2NPHeader::Type::Data, clove_id_, data::I2NPHeader::Mode::Normal),
        i2np_body_(data::Data().buffer()),
        expiration_(tini2p::time::now_ms() + Expiration),
        cert_(),
        buf_()
  {
    i2np_header_.message_size(i2np_body_.size());

    serialize();
  }

  /// @brief Create a fully initialized GarlicClove
  /// @tparam TMessage I2NP message type
  /// @param instructions Garlic delivery instructions for end-to-end routing
  /// @param message I2NP message to deliver
  template <class TMessage>
  GarlicClove(instructions_t instructions, TMessage msg, clove_id_t clove_id = 0)
      : instructions_(std::forward<instructions_t>(instructions)),
        clove_id_(clove_id == 0 ? crypto::RandInRange(MinCloveID, MaxCloveID) : std::forward<clove_id_t>(clove_id)),
        i2np_header_(message_to_type<TMessage>(), clove_id_, data::I2NPHeader::Mode::Normal),
        i2np_body_(msg.buffer()),
        expiration_(tini2p::time::now_ms() + Expiration),
        cert_(cert_t::cert_type_t::NullCert),
        buf_()
  {
    i2np_header_.message_size(i2np_body_.size());

    serialize();
  }

  /// @brief Create a GarlicCove from a secure buffer
  explicit GarlicClove(buffer_t buf)
  {
    const exception::Exception ex{"GarlicClove", __func__};

    check_len(buf.size(), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Create a GarlicClove from a C-like buffer
  GarlicClove(const std::uint8_t* data, const std::size_t len)
  {
    const exception::Exception ex{"GarlicClove", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  /// @brief Serialize GarlicClove to buffer
  GarlicClove& serialize()
  {
    const exception::Exception ex{"GarlicClove", __func__};

    buf_.resize(size());

    check_params(ex);

    tini2p::BytesWriter<buffer_t> writer(buf_);

    // write GarlicDeliveryInstructions
    instructions_.serialize();
    writer.write_data(instructions_.buffer());

    // write the I2NP message
    i2np_header_.serialize();
    writer.write_data(i2np_header_.buffer());
    writer.write_data(i2np_body_);

    writer.write_bytes(clove_id_, tini2p::Endian::Big);
    writer.write_bytes(expiration_, tini2p::Endian::Big);

    cert_.serialize();
    writer.write_data(cert_.buffer);

    return *this;
  }

  /// @brief Deserialize GarlicClove from buffer
  GarlicClove& deserialize()
  {
    const exception::Exception ex{"GarlicClove", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    // read garlic delivery instructions from buffer.
    read_i2np_part(reader, I2NPPart::Instructions, ex);

    // read I2NP message from buffer
    read_i2np_part(reader, I2NPPart::Message, ex);

    check_instructions(instructions_, i2np_header_, ex);

    reader.read_bytes(clove_id_, tini2p::Endian::Big);

    expiration_t expiration(0);
    reader.read_bytes(expiration, tini2p::Endian::Big);
    check_expiration(expiration, ex);

    expiration_ = std::move(expiration);

    // read cert len to resize the buffer properly
    cert_t::length_t cert_len;
    reader.skip_bytes(cert_t::CertTypeLen);
    reader.read_bytes(cert_len, tini2p::Endian::Big);
    reader.skip_back(cert_t::HeaderLen);
    cert_.buffer.resize(cert_t::HeaderLen + cert_len);

    reader.read_data(cert_.buffer);
    cert_.deserialize();

    // reclaim unused buffer space
    buf_.resize(reader.count());
    buf_.shrink_to_fit();

    return *this;
  }

  /// @brief Get the total size of the GarlicClove
  std::uint32_t size() const
  {
    return instructions_.size() + i2np_header_.size() + i2np_body_.size() + CloveIDLen + ExpirationLen + cert_.size();
  }

  /// @brief Get the I2NP message size
  std::uint16_t message_size() const
  {
    return i2np_header_.size() + i2np_body_.size();
  }

  /// @brief Get a const reference to the delivery instructions
  const instructions_t& instructions() const noexcept
  {
    return instructions_;
  }

  /// @brief Get whether the clove is for local delivery
  std::uint8_t is_local() const
  {
    return static_cast<std::uint8_t>(instructions_.delivery_flags() == instructions_t::DeliveryFlags::Local);
  }

  /// @brief Get whether the clove is for router delivery
  std::uint8_t is_router() const
  {
    return static_cast<std::uint8_t>(instructions_.delivery_flags() == instructions_t::DeliveryFlags::Router);
  }

  /// @brief Get whether the clove is for tunnel delivery
  std::uint8_t is_tunnel() const
  {
    return static_cast<std::uint8_t>(instructions_.delivery_flags() == instructions_t::DeliveryFlags::Tunnel);
  }

  /// @brief Get a const reference to the I2NP message type
  const data::I2NPHeader::Type& message_type() const noexcept
  {
    return i2np_header_.type();
  }

  /// @brief Get a const reference to the I2NP message
  const data::I2NPHeader& message_header() const noexcept
  {
    return i2np_header_;
  }

  /// @brief Get a const reference to the I2NP message
  const crypto::SecBytes& message_body() const noexcept
  {
    return i2np_body_;
  }

  /// @brief Extract the inner I2NP message
  template <class TMsg>
  TMsg extract_message()
  {
    const exception::Exception ex{"I2NP: GarlicClove", __func__};

    const auto& calc_type = message_to_type<TMsg>();
    const auto& head_type = i2np_header_.type();

    if (calc_type != head_type)
      {
        ex.throw_ex<std::logic_error>(
            "invalid type: " + std::to_string(tini2p::under_cast(calc_type))
            + ", expected: " + std::to_string(tini2p::under_cast(head_type)));
      }

    return TMsg(std::move(i2np_body_));
  }

  /// @brief Get a const reference to the clove ID
  const clove_id_t& id() const noexcept
  {
    return clove_id_;
  }

  /// @brief Get a const reference to the clove expiration
  const expiration_t& expiration() const noexcept
  {
    return expiration_;
  }

  /// @brief Get a const reference to the clove certificate
  const cert_t& cert() const noexcept
  {
    return cert_;
  }

  /// @brief Get a const refernce to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const refernce to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Compare equality with another GarlicClove
  std::uint8_t operator==(const GarlicClove& oth) const
  {  // attempt constant-time
    const auto& instr_eq = static_cast<std::uint8_t>(instructions_ == oth.instructions_);
    const auto& head_eq = static_cast<std::uint8_t>(i2np_header_ == oth.i2np_header_);
    const auto& body_eq = static_cast<std::uint8_t>(i2np_body_ == oth.i2np_body_);
    const auto& id_eq = static_cast<std::uint8_t>(clove_id_ == oth.clove_id_);
    const auto& exp_eq = static_cast<std::uint8_t>(expiration_ == oth.expiration_);
    const auto& cert_eq = static_cast<std::uint8_t>(cert_ == oth.cert_);
 
    return (instr_eq * head_eq * body_eq * id_eq * exp_eq * cert_eq);
  }

 private:
  void read_i2np_part(tini2p::BytesReader<buffer_t>& reader, const I2NPPart& part, const exception::Exception& ex)
  {
    if (part == I2NPPart::Instructions)
      {  // read flags to resize, then read instructions and deserialize
        instructions_t::flags_t flags;
        reader.read_bytes(flags);
        instructions_.resize_from_flags(flags);
        reader.skip_back(instructions_t::FlagsLen);
        reader.read_data(instructions_.buffer());
        instructions_.deserialize();
      }
    else if (part == I2NPPart::Message)
      {  // read and deserialize I2NP message
        crypto::SecBytes head_buf;
        head_buf.resize(data::I2NPHeader::HeaderLen);

        reader.read_data(head_buf);
        data::I2NPHeader i2np_header(head_buf, data::I2NPHeader::Mode::Normal);
        check_i2np_type(i2np_header.type(), ex);

        crypto::SecBytes msg_buf;
        msg_buf.resize(i2np_header.message_size());

        reader.read_data(msg_buf);

        i2np_header_ = std::move(i2np_header);
        i2np_body_ = std::move(msg_buf);
      }
    else
      ex.throw_ex<std::invalid_argument>("invalid I2NP part.");
  }

  void check_i2np_type(const data::I2NPHeader::Type& type, const exception::Exception& ex) const
  {
    if (type != data::I2NPHeader::Type::Data && type != data::I2NPHeader::Type::DatabaseStore
        && type != data::I2NPHeader::Type::DeliveryStatus)
      {
        ex.throw_ex<std::logic_error>(
            "invalid I2NP message type: " + std::to_string(tini2p::under_cast(type)) + ", expected: Data("
            + std::to_string(tini2p::under_cast(data::I2NPHeader::Type::Data)) + "), DatabaseStore("
            + std::to_string(tini2p::under_cast(data::I2NPHeader::Type::DatabaseStore)) + "), or DeliveryStatus("
            + std::to_string(tini2p::under_cast(data::I2NPHeader::Type::DeliveryStatus)) + ")");
      }
  }

  template <class T>
  data::I2NPHeader::Type message_to_type() const
  {
    const exception::Exception ex{"I2NP: GarlicClove", __func__};

    data::I2NPHeader::Type type;

    if (std::is_same<T, Data>::value)
      type = data::I2NPHeader::Type::Data;
    else if (std::is_same<T, DatabaseStore>::value)
      type = data::I2NPHeader::Type::DatabaseStore;
    else if (std::is_same<T, DeliveryStatus>::value)
      type = data::I2NPHeader::Type::DeliveryStatus;
    else
      ex.throw_ex<std::logic_error>("invalid message type.");

    return type;
  }

  void check_params(const exception::Exception& ex)
  {
    check_len(buf_.size(), ex);

    if (clove_id_ == NullID)
      ex.throw_ex<std::logic_error>("null clove ID.");

    check_instructions(instructions_, i2np_header_, ex);

    check_i2np_type(i2np_header_.type(), ex);
  }

  void check_len(const std::size_t len, const exception::Exception& ex)
  {
    if (len < MinLen || len > MaxLen)
      {
        ex.throw_ex<std::length_error>(
            "invalid message size: " + std::to_string(len) + " min: " + std::to_string(tini2p::under_cast(MinLen))
            + " max: " + std::to_string(tini2p::under_cast(MaxLen)));
      }
  }

  void check_instructions(
      const instructions_t& instructions,
      const data::I2NPHeader& header,
      const exception::Exception& ex) const
  {
    const auto& delivery = instructions.delivery_flags();
    const auto& msg_type = header.type();

    if (delivery != instructions_t::delivery_t::Local && msg_type != data::I2NPHeader::Type::DeliveryStatus)
      ex.throw_ex<std::logic_error>("invalid Garlic routing: non-Local delivery of a non-DeliveryStatus I2NP message.");

    if (delivery == instructions_t::delivery_t::Local && msg_type == data::I2NPHeader::Type::DeliveryStatus)
      ex.throw_ex<std::logic_error>("invalid Garlic routing: Local delivery of a DeliveryStatus I2NP message.");
  }

  void check_expiration(const expiration_t& expiration, const exception::Exception& ex) const
  {
    const auto& now = time::now_ms();

    if (expiration <= now)
      {
        ex.throw_ex<std::logic_error>(
            "clove expired, expiration: " + std::to_string(expiration) + ", now: " + std::to_string(now));
      }
  }

  instructions_t instructions_;
  clove_id_t clove_id_;
  data::I2NPHeader i2np_header_;
  crypto::SecBytes i2np_body_;
  expiration_t expiration_;
  cert_t cert_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_GARLIC_CLOVE_H_
