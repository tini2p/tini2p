/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_I2NP_I2NP_HEADER_H_
#define SRC_DATA_I2NP_I2NP_HEADER_H_

#include "src/time.h"

#include "src/crypto/rand.h"
#include "src/crypto/sec_bytes.h"

namespace tini2p
{
namespace data
{
/// @class I2NPHeader
/// @brief Message for creating and processing I2NP messages
/// @detail Message format differs depending on context:
///    Normal (16-byte header):
///      [type][msg_id][expiration_64bit][msg_len_16bit][chksum][msg_body]
///
///    Garlic:
///      [type][msg_id][expiration_64bit][msg_len_16bit][chksum][enc_len_32bit][enc_msg_body]
///
///    NTCP2:
///      [type][msg_id][expiration_32bit][msg_body]
///
///    SSU:
///      [type][expiration_32bit][msg_body]
class I2NPHeader
{
 public:
  enum : std::uint32_t
  {
    HeaderLen = 16,
    NTCP2HeaderLen = 9,
    SSUHeaderLen = 5,
    TypeLen = 1,
    MessageIDLen = 4,
    MinMsgID = 1,
    MaxMsgID = 0xFFFFFFFF,  // uint32_max
    ExpirationSLen = 8,
    ExpirationMSLen = 8,
    SizeLen = 2,
    ChecksumLen = 1,
    GarlicSizeLen = 4,
    MinMessageLen = 0,
    MaxMessageLen = 62703,
    MinLen = SSUHeaderLen,
    MaxLen = HeaderLen,  //< max I2NP length, see I2NP spec
    ExpirationS = 120,  //< in seconds, for NTCP2 + SSU header, see I2NP spec 
    ExpirationMS = 120000,  //< in milliseconds, for 16-byte header, see I2NP spec
  };

  /// @brief I2NP message type
  enum struct Type : std::uint8_t
  {
    Reserved = 0,
    DatabaseStore,
    DatabaseLookup,
    DatabaseSearchReply,
    DeliveryStatus = 10,
    Garlic,
    TunnelData = 18,
    TunnelGateway,
    Data,
    TunnelBuild,
    TunnelBuildReply,
    VariableTunnelBuild,
    VariableTunnelBuildReply,
    FutureReserved = 255,
  };

  /// @brief I2NP message transport mode
  /// @detail Used for formatting, especially the header.
  ///     - Normal/Garlic Header: 16 bytes
  ///     - NTCP2 Header:          9 bytes
  ///     - SSU Header:            5 bytes
  ///     See the I2NP spec for full details.
  enum struct Mode : std::uint8_t
  {
    Normal,
    Garlic,
    NTCP2,
    SSU,
  };

  /// @brief Default ctor
  I2NPHeader()
      : type_(Type::Data),
        msg_id_(crypto::RandInRange(0x01, 0xFFFFFFFF)),
        expiration_(time::now_ms() + ExpirationMS),
        checksum_(),
        mode_(Mode::Normal),
        buf_()
  {
    serialize();
  }

  /// @brief Converting ctor
  /// @detail Wrap an I2NP message body in the appropriate header
  /// @tparam TMsg Inner I2NP message class: DatabaseStore, DatabaseLookup, ...
  /// @param msg I2NP message body
  /// @param mode I2NP mode for the message: Normal, Garlic, NTCP2, SSU
  I2NPHeader(Type type, std::uint32_t msg_id = 0, Mode mode = Mode::Normal)
      : msg_id_(msg_id == 0 ? crypto::RandInRange(MinMsgID, MaxMsgID) : std::forward<std::uint32_t>(msg_id)),
        expiration_(),
        checksum_(),
        mode_(std::forward<Mode>(mode))
  {
    const exception::Exception ex{"I2NP: Header", __func__};

    check_type(type, ex);
    check_mode(mode_, ex);

    type_ = std::forward<Type>(type);
    expiration_ = expiration(mode_);
    buf_.resize(size(mode_, ex));

    serialize();
  }

  /// @brief Deserializing ctor, creates an I2NPHeader from secure buffer
  /// @param buf Buffer containing a serialized I2NPHeader
  /// @param mode I2NP message mode: Normal, Garlic, NTCP2, SSU
  I2NPHeader(crypto::SecBytes buf, Mode mode = Mode::Normal)
      : type_(Type::Reserved),
        msg_id_(),
        expiration_(),
        checksum_(),
        mode_(std::forward<Mode>(mode)),
        buf_()
  {
    const exception::Exception ex{"I2NP: Header", __func__};

    check_buffer(buf, ex);
    check_mode(mode_, ex);

    buf_ = std::forward<crypto::SecBytes>(buf);

    deserialize();
  }

  /// @brief Serialize the I2NP message to the buffer
  I2NPHeader& serialize()
  {
    const exception::Exception ex{"I2NP: Header", __func__};

    // Check data members
    check_type(type_, ex);
    check_message_id(msg_id_, ex);
    check_expiration(expiration_, ex);

    const std::uint16_t& msg_len = size(mode_, ex);

    buf_.resize(msg_len);

    tini2p::BytesWriter<crypto::SecBytes> writer(buf_);

    writer.write_bytes(type_);

    if (mode_ != Mode::SSU)
      writer.write_bytes(msg_id_, tini2p::Endian::Big);

    // write the expiration
    if (mode_ == Mode::Normal || mode_ == Mode::Garlic)
      writer.write_bytes(expiration_, tini2p::Endian::Big);
    else
      writer.write_bytes(static_cast<std::uint32_t>(expiration_), tini2p::Endian::Big);

    if (mode_ == Mode::Normal || mode_ == Mode::Garlic)
      writer.write_bytes(length_, tini2p::Endian::Big);

    // write one-byte checksum to buffer
    if (mode_ == Mode::Normal || mode_ == Mode::Garlic)
      writer.write_bytes(checksum_, tini2p::Endian::Big);

    return *this;
  }

  /// @brief Deserialize an I2NPHeader from the buffer
  /// @detail NTCP2 and SSU callers are responsible for setting the appropriate buffer length before calling
  /// @note SSU callers may benefit from helper functions for resizing based on message type
  I2NPHeader& deserialize()
  {
    const exception::Exception ex{"I2NP: Header", __func__};

    tini2p::BytesReader<crypto::SecBytes> reader(buf_);

    Type type(Type::Reserved);
    reader.read_bytes(type);
    check_type(type, ex);

    std::uint32_t msg_id(0);

    if (mode_ != Mode::SSU)
      reader.read_bytes(msg_id, tini2p::Endian::Big);

    std::uint64_t exp_64(0);
    std::uint32_t exp_32(0);

    if (mode_ == Mode::Normal || mode_ == Mode::Garlic)
      reader.read_bytes(exp_64, tini2p::Endian::Big);
    else
      reader.read_bytes(exp_32, tini2p::Endian::Big);

    std::uint64_t exp = exp_64 + exp_32;
    check_expiration(exp, ex);

    std::uint16_t body_len(0);

    if (mode_ == Mode::Normal || mode_ == Mode::Garlic)
      reader.read_bytes(body_len, tini2p::Endian::Big);

    check_message_len(body_len, ex);

    std::uint8_t checksum(0);

    crypto::SecBytes msg_buf;

    // calculate message checksum, write first byte to buffer
    if (mode_ == Mode::Normal || mode_ == Mode::Garlic)
      reader.read_bytes(checksum);

    type_ = std::move(type);
    msg_id_ = std::move(msg_id);
    expiration_ = std::move(exp);
    length_ = std::move(body_len);
    checksum_ = std::move(checksum);

    return *this;
  }

  /// @brief Get the size of the I2NP message
  std::uint16_t size() const
  {
    const exception::Exception ex{"I2NP: Header", __func__};

    return size(mode_, ex);
  }

  /// @brief Get a const reference to the I2NP message type
  const Type& type() const noexcept
  {
    return type_;
  }

  /// @brief Set the I2NP message type
  I2NPHeader& type(Type type)
  {
    const exception::Exception ex{"I2NP: Header", __func__};

    check_type(type, ex);

    type_ = std::forward<Type>(type);

    return *this;
  }

  /// @brief Get a const reference to the I2NP message ID
  const std::uint32_t& message_id() const noexcept
  {
    return msg_id_;
  }

  /// @brief Set the I2NP message ID
  I2NPHeader& message_id(std::uint32_t msg_id)
  {
    const exception::Exception ex{"I2NP: Header", __func__};

    check_message_id(msg_id, ex);

    msg_id_ = std::forward<std::uint32_t>(msg_id);

    return *this;
  }

  /// @brief Get a const reference to the expiration
  const std::uint64_t& expiration() const noexcept
  {
    return expiration_;
  }

  /// @brief Get the default expiration from now for a given I2NP mode
  std::uint64_t expiration(const Mode& mode) const
  {
    return mode == Mode::Normal || mode == Mode::Garlic ? time::now_ms() + ExpirationMS : time::now_s() + ExpirationS;
  }

  /// @brief Set the expiration
  I2NPHeader& expiration(std::uint64_t expiration)
  {
    const exception::Exception ex{"I2NP: Header", __func__};

    check_expiration(expiration, ex);

    expiration_ = std::forward<std::uint64_t>(expiration);

    return *this;
  }

  /// @brief Get a const reference to the one-byte checksum
  const std::uint8_t& checksum() const noexcept
  {
    return checksum_;
  }

  /// @brief Set the one-byte checksum
  I2NPHeader& checksum(std::uint8_t chk)
  {
    checksum_ = std::forward<std::uint8_t>(chk);

    return *this;
  }
  
  /// @brief Get a const reference to the message body size
  const std::uint16_t& message_size() const
  {
    return length_;
  }
  
  /// @brief Set the message body size
  I2NPHeader& message_size(std::uint16_t len)
  {
    const exception::Exception ex{"I2NP: Header", __func__};

    check_message_len(len, ex);

    length_ = std::forward<std::uint16_t>(len);

    return *this;
  }

  /// @brief Get a const reference to the I2NP message mode
  const Mode& mode() const noexcept
  {
    return mode_;
  }

  /// @brief Set the I2NP message mode
  I2NPHeader& mode(Mode mode)
  {
    const exception::Exception ex{"I2NP: Header", __func__};

    check_mode(mode, ex);

    // if converting from a 64-bit timestamp mode to 32-bit,
    //   covert expiration from milliseconds to seconds
    if ((mode_ == Mode::Normal || mode_ == Mode::Garlic) && (mode == Mode::NTCP2 || mode == Mode::SSU))
      {
        expiration_ = time::ms_to_seconds(expiration_);
        checksum_ = 0x00;
      }

    if ((mode_ == Mode::NTCP2 || mode_ == Mode::SSU) && (mode == Mode::Normal || mode == Mode::Garlic))
      {
        expiration_ = time::seconds_to_ms(expiration_);
        checksum_ = 0x00;
      }

    buf_.resize(size());

    mode_ = std::forward<Mode>(mode);

    return *this;
  }
  
  /// @brief Get a const reference to the full message buffer
  const crypto::SecBytes& buffer() const noexcept
  {
    return buf_;
  }
  
  /// @brief Get a non-const reference to the full message buffer
  crypto::SecBytes& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another I2NPHeader
  std::uint8_t operator==(const I2NPHeader& oth) const
  {
    const exception::Exception ex{"I2NP: Header", __func__};

    const auto& type_eq = static_cast<std::uint8_t>(type_ == oth.type_);
    const auto& id_eq = static_cast<std::uint8_t>(msg_id_ == oth.msg_id_);
    const auto& exp_eq = static_cast<std::uint8_t>(expiration_ == oth.expiration_);
    const auto& chk_eq = static_cast<std::uint8_t>(checksum_ == oth.checksum_);
    const auto& mode_eq = static_cast<std::uint8_t>(mode_ == oth.mode_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    return (type_eq * id_eq * exp_eq * chk_eq * mode_eq * buf_eq);
  }

  const I2NPHeader& check_type(const Type& type, const exception::Exception& ex) const
  {
    if (type != Type::DatabaseStore &&
        type != Type::DatabaseLookup &&
        type != Type::DatabaseSearchReply &&
        type != Type::DeliveryStatus &&
        type != Type::Garlic &&
        type != Type::TunnelData &&
        type != Type::TunnelGateway &&
        type != Type::Data &&
        type != Type::VariableTunnelBuild &&
        type != Type::VariableTunnelBuildReply)
      ex.throw_ex<std::logic_error>("invalid message type: " + std::to_string(tini2p::under_cast(type)));

    return *this;
  }

 private:
  void check_message_id(const std::uint32_t& msg_id, const exception::Exception& ex) const
  {
    if (msg_id == 0)
      ex.throw_ex<std::logic_error>("null message ID");
  }

  void check_expiration(const std::uint64_t& expiration, const exception::Exception& ex) const
  {
    const std::uint64_t& now = mode_ == Mode::NTCP2 || mode_ == Mode::SSU ? time::now_s() : time::now_ms();

    if (expiration <= now)
      {
        ex.throw_ex<std::logic_error>(
            "expired message, expiration: " + std::to_string(expiration) + ", now: " + std::to_string(now));
      }
  }

  void check_checksum(const std::uint8_t& chk, const std::uint8_t& exp_chk, const exception::Exception& ex) const
  {
    if (chk != exp_chk)
      {
        ex.throw_ex<std::logic_error>(
            "invalid checksum: " + std::to_string(chk) + ", expected: " + std::to_string(exp_chk));
      }
  }

  std::uint8_t size(const Mode& mode, const exception::Exception) const
  {
    return mode == Mode::Normal || mode == Mode::Garlic ? HeaderLen
                                                        : mode == Mode::NTCP2 ? NTCP2HeaderLen : SSUHeaderLen;
  }

  void check_mode(const Mode& mode, const exception::Exception& ex) const
  {
    if (mode != Mode::Normal && mode != Mode::Garlic && mode != Mode::NTCP2 && mode != Mode::SSU)
      ex.throw_ex<std::invalid_argument>("invalid I2NP message mode");
  }

  void check_message_len(const std::uint16_t& msg_len, const exception::Exception& ex) const
  {
    const auto& max_len = tini2p::under_cast(MaxMessageLen);

    if (msg_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid body length: " + std::to_string(msg_len) + ", max: " + std::to_string(max_len));
      }
  }

  void check_buffer(const crypto::SecBytes& buf, const exception::Exception& ex) const
  {
    const auto& buf_len = buf.size();
    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid header length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }

    if (buf.is_zero())
      ex.throw_ex<std::logic_error>("null buffer");
  }

  Type type_;
  std::uint32_t msg_id_;
  std::uint64_t expiration_;
  std::uint16_t length_;
  std::uint8_t checksum_;
  Mode mode_;
  crypto::SecBytes buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_I2NP_I2NP_HEADER_H_
