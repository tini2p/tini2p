/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_I2NP_DATA_H_
#define SRC_I2NP_DATA_H_

#include "src/bytes.h"

#include "src/crypto/sec_bytes.h"

#include "src/data/blocks/i2np.h"

namespace tini2p
{
namespace data
{
/// @class Data
/// @brief For creating and processing I2NP Data messages
class Data
{
 public:
  enum
  {
    LengthLen = 4,
    MinLen = LengthLen,
    MinDataLen = 0,
    MaxLen = data::I2NPBlock::MaxMsgLen,
    MaxDataLen = MaxLen - LengthLen,
  };

  using length_t = std::uint32_t;  //< Length trait alias
  using data_t = crypto::SecBytes;  //< Data trait alias
  using buffer_t = crypto::SecBytes; //< Buffer trait alias

  using pointer = Data*;  //< Non-owning pointer trait alias
  using const_pointer = const Data*;  //< Const non-owning pointer trait alias
  using unique_ptr = std::unique_ptr<Data>;  //< Unique pointer trait alias
  using const_unique_ptr = std::unique_ptr<const Data>;  //< Const unique pointer trait alias
  using shared_ptr = std::shared_ptr<Data>;  //< Shared pointer trait alias
  using const_shared_ptr = std::shared_ptr<const Data>;  //< Const shared pointer trait alias

  /// @brief Default ctor
  Data() : length_(0), data_(), buf_(LengthLen) {}

  /// @brief Random data ctor, create a Data block of random data
  /// @param len Length of the random data
  explicit Data(length_t len)
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    check_length(len, ex);

    length_ = std::forward<length_t>(len);

    data_.resize(length_);

    crypto::RandBytes(data_);

    serialize();
  }

  /// @brief Create an I2NP Data message from a secure buffer
  explicit Data(buffer_t buf)
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    check_buffer(buf, ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Create an I2NP Data message from a C-like buffer
  Data(const std::uint8_t* data, const std::size_t len)
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    tini2p::check_cbuf(data, len, MinLen, MaxLen, ex);

    buf_.resize(len);
    std::copy_n(data, len, buf_.data());

    deserialize();
  }

  /// @brief Serialize the data message to the buffer
  void serialize()
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    check_length(length_, ex);
    check_data(data_, ex);

    buf_.resize(tini2p::under_cast(LengthLen) + data_.size());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_bytes(length_, tini2p::Endian::Big);
    writer.write_data(data_);
  }

  /// @brief Deserialize the data message from the buffer
  void deserialize()
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    check_buffer(buf_, ex);

    tini2p::BytesReader<buffer_t> reader(buf_);

    length_t len;
    reader.read_bytes(len, tini2p::Endian::Big);
    check_length(len, ex);
    length_ = std::move(len);

    data_.resize(length_);
    reader.read_data(data_);

    // reclaim unused space
    buf_.resize(reader.count());
    buf_.shrink_to_fit();
  }

  /// @brief Deserialize an I2NP Data message from a secure buffer
  Data& from_buffer(buffer_t buf)
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    check_buffer(buf, ex);

    buf_ = std::forward<buffer_t>(buf);
    deserialize();

    return *this;
  }

  /// @brief Get the total size of the data message
  length_t size() const noexcept
  {
    return LengthLen + length_;
  }

  /// @brief Get the length of the data payload
  const length_t& data_length() const noexcept
  {
    return length_;
  }

  /// @brief Get a const reference to the data payload
  const data_t& data() const noexcept
  {
    return data_;
  }

  /// @brief Set the data payload
  void data(data_t data)
  {
    const exception::Exception ex{"I2NP: Data", __func__};

    check_data(data, ex);

    length_ = data.size();
    data_ = std::forward<data_t>(data);
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Compare equality with another Data message
  std::uint8_t operator==(const Data& oth) const
  {
    const auto& len_eq = static_cast<std::uint8_t>(length_ == oth.length_);
    const auto& data_eq = static_cast<std::uint8_t>(data_ == oth.data_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    return (len_eq * data_eq * buf_eq);
  }

 private:
  void check_buffer(const buffer_t& buf, const exception::Exception& ex)
  {
    const auto& buf_len = buf.size();
    const auto& min_len = tini2p::under_cast(MinLen);
    const auto& max_len = tini2p::under_cast(MaxLen);

    if(buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + " min: " + std::to_string(min_len)
            + " max: " + std::to_string(max_len));
      }
  }

  void check_data(const data_t& data, const exception::Exception& ex)
  {
    const auto& data_len = data.size();
    const auto& min_data_len = tini2p::under_cast(MinDataLen);
    const auto& max_data_len = tini2p::under_cast(MaxDataLen);

    if(data_len < min_data_len || data_len > max_data_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid data length: " + std::to_string(data_len) + " min: " + std::to_string(min_data_len)
            + " max: " + std::to_string(max_data_len));
      }
  }

  void check_length(const length_t& len, const exception::Exception& ex)
  {
    const auto& min_data_len = tini2p::under_cast(MinDataLen);
    const auto& max_data_len = tini2p::under_cast(MaxDataLen);

    if(len < min_data_len || len > max_data_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid data length: " + std::to_string(len) + " min: " + std::to_string(min_data_len)
            + " max: " + std::to_string(max_data_len));
      }
  }

  length_t length_;
  data_t data_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_I2NP_DATA_H_
