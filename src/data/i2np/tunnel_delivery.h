/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_I2NP_TUNNEL_DELIVERY_H_
#define SRC_DATA_I2NP_TUNNEL_DELIVERY_H_

#include <optional>

#include "src/crypto/sec_bytes.h"
#include "src/crypto/tunnel/aes.h"
#include "src/crypto/tunnel/chacha.h"

#include "src/data/router/identity.h"

namespace tini2p
{
namespace data
{
/// @brief Enum for layer encryption being used
enum struct Layer : std::uint8_t
{
  AES,
  ChaCha,
};

/// @class FirstDelivery
/// @brief Tunnel delivery instructions for the first I2NP fragment
class FirstDelivery
{
 public:
  enum
  {
    FlagsLen = 1,
    TunnelIDLen = 4,
    ToHashLen = 32,
    MessageIDLen = 4,
    SizeLen = 2,
    UnfragLocalLen = 3,
    FragLocalLen = 7,
    UnfragRouterLen = 35,
    FragRouterLen = 39,
    UnfragTunnelLen = 39,
    FragTunnelLen = 43,
    MinFragmentLen = 12,
    TunnelHeaderLen = 25,
    TunnelMessageLen = 1028,
    AESCipherLen = 1008,
    ChaChaCipherLen = 992,
    ChecksumPadDelimLen = 5,
  };

  /// @brief Delivery flags (for function arguments, ease of use)
  /// @detail The delivery types are used as follows:
  ///    Local: inbound tunnel delivery from the IBGW to the IBEP, i.e. local to the IBEP
  ///    Tunnel: outbound tunnel delivery to an inbound tunnel's IBGW
  ///    Router: outbound tunnel delivery directly to a router, e.g. for DatabaseStore, DatabaseLookup, etc.
  enum struct Delivery : std::uint8_t
  {
    Local,
    Tunnel = 0x20,
    Router = 0x40,
  };

  /// @brief Fragmentation mode (for function arguments, ease of use)
  enum struct Mode : std::uint8_t
  {
    Unfragmented,
    Fragmented = 0x08,
  };

  /// @brief Flags for delivery type and included components
  enum struct Flags : std::uint8_t
  {
    First = 0x00,  //< First fragment, or unfragmented message, 0000 0000
    Local = 0x00,                           //< Local delivery, 0000 0000
    FragLocal = 0x08,            //< Fragmented Local delivery, 0000 1000
    Tunnel = 0x20,                         //< Tunnel delivery, 0010 0000   
    FragTunnel = 0x28,          //< Fragmented Tunnel delivery, 0010 1000   
    Router = 0x40,                         //< Router delivery, 0100 0000
    FragRouter = 0x48,          //< Fragmented Router delivery, 0100 1000
    Invalid = 0x60,                       //< Invalid delivery, 0110 0000
    Delay = 0x10,                      //< Delay byte included, 0001 0000
    Fragmented = 0x08,                  //< Fragmented message, 0000 1000
    Extended = 0x04,                      //< Extended options, 0000 0100
    ReservedMask = 0x03,                     //< Reserved bits, 0000 0011
    DeliveryMask = 0x60,                //< Mask delivery bits, 0110 0000
    FollowMask = 0x80,   //< Mask first/follow-on fragment bit, 1000 0000
  };

  using layer_t = data::Layer;  //< Layer encryption trait alias
  using delivery_t = Delivery;  //< Delivery trait alias
  using mode_t = Mode;  //< Fragmentation mode trait alias
  using flags_t = Flags;  //< Flags trait alias
  using tunnel_id_t = std::uint32_t;  //< Tunnel ID trait alias
  using to_hash_t = data::Identity::hash_t;  //< Deliver to Identity hash trait alias
  using delay_t = std::uint8_t;  //< Delay byte trait alias (never used)
  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using ext_options_t = crypto::SecBytes;  //< Extended options trait alias (never used)
  using size_type = std::uint16_t;  //< Size type trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor, creates unfragmented FirstDelivery with local delivery instructions and minimum fragment size
  FirstDelivery()
      : layer_(layer_t::AES),
        flags_(flags_t::Local),
        tun_id_(std::nullopt),
        to_hash_(std::nullopt),
        msg_id_(std::nullopt),
        size_(tini2p::under_cast(MinFragmentLen)),
        buf_(tini2p::under_cast(UnfragLocalLen))
  {
  }

  /// @brief Create an unfragmented FirstDelivery with local delivery instructions and minimum fragment size
  /// @detail Useful for testing purposes
  /// @param layer Layer encryption type
  explicit FirstDelivery(layer_t layer)
      : layer_(std::forward<layer_t>(layer)),
        flags_(flags_t::Local),
        tun_id_(std::nullopt),
        to_hash_(std::nullopt),
        msg_id_(std::nullopt),
        size_(tini2p::under_cast(MinFragmentLen)),
        buf_(tini2p::under_cast(UnfragLocalLen))
  {
  }

  /// @brief Local ctor, creates a FirstDelivery with local delivery instructions
  /// @detail Message ID must be set for fragmented messages, and must be null for unfragmented messages
  /// @param layer Layer encryption flag
  /// @param mode Fragmented or unfragmented instructions
  /// @param size Fragment size
  /// @param msg_id (Optional) message ID for a fragmented local message
  FirstDelivery(const layer_t& layer, const mode_t& mode, size_type size, std::optional<message_id_t> msg_id = std::nullopt)
      : layer_(layer),
        flags_(flags_t::Local),
        tun_id_(std::nullopt),
        to_hash_(std::nullopt),
        msg_id_(std::forward<std::optional<message_id_t>>(msg_id)),
        size_(std::forward<size_type>(size)),
        buf_()
  {
    const exception::Exception ex{"Tunnel Delivery: FirstDelivery", __func__};

    check_layer(layer, ex);
    check_mode(mode, ex);

    flags_ = static_cast<flags_t>(tini2p::under_cast(flags_) | tini2p::under_cast(mode));

    serialize();
  }

  /// @brief Router ctor, creates a FirstDelivery with router delivery instructions
  /// @detail Message ID must be set for fragmented messages, and must be null for unfragmented messages
  /// @param mode Fragmented or unfragmented instructions
  /// @param size Fragment size
  /// @param to_hash Deliver to router with this Identity hash
  /// @param msg_id (Optional) message ID for a fragmented router message
  FirstDelivery(
      const layer_t& layer,
      const mode_t& mode,
      size_type size,
      to_hash_t to_hash,
      std::optional<message_id_t> msg_id = std::nullopt)
      : layer_(layer),
        flags_(flags_t::Router),
        tun_id_(std::nullopt),
        to_hash_(std::nullopt),
        msg_id_(std::forward<std::optional<message_id_t>>(msg_id)),
        size_(std::forward<size_type>(size)),
        buf_()
  {
    const exception::Exception ex{"Tunnel Delivery: FirstDelivery", __func__};

    check_layer(layer, ex);
    check_mode(mode, ex);

    if (to_hash.is_zero())
      ex.throw_ex<std::invalid_argument>("null deliver to hash.");

    to_hash_.emplace(std::forward<to_hash_t>(to_hash));
    flags_ = static_cast<flags_t>(tini2p::under_cast(flags_) | tini2p::under_cast(mode));

    serialize();
  }

  /// @brief Tunnel ctor, creates a FirstDelivery with tunnel delivery instructions
  /// @detail Message ID must be set for fragmented messages, and must be null for unfragmented messages
  /// @param mode Fragmented or unfragmented instructions
  /// @param size Fragment size
  /// @param tun_id Tunnel ID the message will be delivered through
  /// @param to_hash Deliver to tunnel gateway with this Identity hash
  /// @param msg_id (Optional) message ID for a fragmented tunnel message
  FirstDelivery(
      const layer_t& layer,
      const mode_t& mode,
      size_type size,
      tunnel_id_t tun_id,
      to_hash_t to_hash,
      std::optional<message_id_t> msg_id = std::nullopt)
      : layer_(layer),
        flags_(flags_t::Tunnel),
        tun_id_(std::nullopt),
        to_hash_(std::nullopt),
        msg_id_(std::forward<std::optional<message_id_t>>(msg_id)),
        size_(std::forward<size_type>(size)),
        buf_()
  {
    const exception::Exception ex{"Tunnel Delivery: FirstDelivery", __func__};

    check_layer(layer, ex);
    check_mode(mode, ex);

    if (tun_id == 0)
      ex.throw_ex<std::invalid_argument>("null tunnel ID.");

    if (to_hash.is_zero())
      ex.throw_ex<std::invalid_argument>("null deliver to hash.");

    tun_id_.emplace(std::forward<tunnel_id_t>(tun_id));
    to_hash_.emplace(std::forward<to_hash_t>(to_hash));
    flags_ = static_cast<flags_t>(tini2p::under_cast(flags_) | tini2p::under_cast(mode));

    serialize();
  }

  /// @brief Deserializing ctor, creates a FirstDelivery instructions from a secure buffer
  FirstDelivery(const layer_t& layer, buffer_t buf) : layer_(layer)
  {
    const exception::Exception ex{"Tunnel Delivery: FirstDelivery", __func__};

    check_layer(layer, ex);
    check_buffer(buf, ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Serialize the FirstDelivery instructions to the buffer
  void serialize()
  {
    const exception::Exception ex{"Tunnel Delivery: FirstDelivery", __func__};

    // check instruction flags are valid
    check_flags(flags_, ex);

    // check all parameters are valid based on flags
    check_size(flags_, size_, ex);
    check_tunnel_id(flags_, tun_id_, ex);
    check_to_hash(flags_, to_hash_, ex);
    check_message_id(flags_, msg_id_, ex);

    const auto& flag_int = tini2p::under_cast(flags_);
    const auto& delivery = static_cast<flags_t>(flag_int & tini2p::under_cast(flags_t::DeliveryMask));
    const auto& fragmented = flag_int & tini2p::under_cast(flags_t::Fragmented);

    buf_.resize(size(flags_));

    tini2p::BytesWriter<buffer_t> writer(buf_);

    // write instruction flags
    writer.write_bytes(flags_);

    // write tunnel ID if tunnel delivery
    if (delivery == flags_t::Tunnel)
      writer.write_bytes(*tun_id_, tini2p::Endian::Big);

    // write deliver to hash if router or tunnel delivery
    if (delivery == flags_t::Router || delivery == flags_t::Tunnel)
      writer.write_data(*to_hash_);

    // write message ID if fragmented
    if (fragmented != 0)
      writer.write_bytes(*msg_id_, tini2p::Endian::Big);

    // write fragment size
    writer.write_bytes(size_, tini2p::Endian::Big);
  }

  /// @brief Deserialize the FirstDelivery instructions from the buffer
  void deserialize()
  {
    const exception::Exception ex{"TunnelDelivery: FirstDelivery", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    // read and check instruction flags
    flags_t flags;
    reader.read_bytes(flags);
    check_flags(flags, ex);
    flags_ = flags;

    const auto& flag_int = tini2p::under_cast(flags_);
    const auto& delivery = static_cast<flags_t>(flag_int & tini2p::under_cast(flags_t::DeliveryMask));
    const auto& fragmented = flag_int & tini2p::under_cast(flags_t::Fragmented);

    if (delivery == flags_t::Tunnel)
      {  // tunnel delivery, read and check tunnel ID
        tunnel_id_t tun_id;
        reader.read_bytes(tun_id, tini2p::Endian::Big);

        if (tun_id == 0)
          ex.throw_ex<std::logic_error>("null tunnel ID.");

        tun_id_.emplace(std::move(tun_id));
      }

    if (delivery == flags_t::Router || delivery == flags_t::Tunnel)
      {  // router or tunnel delivery, read and check deliver to hash
        to_hash_t to_hash;
        reader.read_data(to_hash);

        if (to_hash.is_zero())
          ex.throw_ex<std::logic_error>("null deliver to hash.");

        to_hash_.emplace(std::move(to_hash));
      }

    if (fragmented != 0)
      {  // fragmented message
        message_id_t msg_id;
        reader.read_bytes(msg_id, tini2p::Endian::Big);
        msg_id_.emplace(std::move(msg_id));
      }
    else
      msg_id_.reset();

    size_type frag_size;
    reader.read_bytes(frag_size, tini2p::Endian::Big);
    check_size(flags_, frag_size, ex);
    size_ = std::move(frag_size);

    const auto& read_size = reader.count();
    const auto& exp_size = size(flags_);
    if (read_size != exp_size)
      {
        ex.throw_ex<std::logic_error>(
            "invalid instructions size: " + std::to_string(read_size) + " for the flags: "
            + std::to_string(tini2p::under_cast(flags_)) + ", expected size: " + std::to_string(exp_size));
      }

    // reclaim unused space
    buf_.resize(read_size);
    buf_.shrink_to_fit();
  }

  /// @brief Get a const reference to the instruction flags
  const flags_t& flags() const noexcept
  {
    return flags_;
  }

  /// @brief Get the instruction delivery type
  delivery_t delivery() const noexcept
  {
    return static_cast<delivery_t>(tini2p::under_cast(flags_) & tini2p::under_cast(flags_t::DeliveryMask));
  }

  /// @brief Get a const reference to the tunnel ID
  /// @note returns std::nullopt if non-tunnel delivery
  const std::optional<tunnel_id_t>& tunnel_id() const noexcept
  {
    return tun_id_;
  }

  /// @brief Get a const reference to the deliver to hash
  const to_hash_t& to_hash() const
  {
    const exception::Exception ex{"Tunnel: FirstDelivery", __func__};

    check_to_hash(to_hash_, ex);

    return *to_hash_;
  }

  /// @brief Get a const reference to the deliver to hash
  /// @note returns std::nullopt if local delivery
  const std::optional<to_hash_t>& to_hash_opt() const noexcept
  {
    return to_hash_;
  }

  /// @brief Get a const reference to the message ID
  /// @note returns std::nullopt if unfragmented delivery
  const std::optional<message_id_t>& message_id() const noexcept
  {
    return msg_id_;
  }

  /// @brief Get a const reference to the fragment size
  const size_type& fragment_size() const noexcept
  {
    return size_;
  }

  /// @brief Set the fragment size
  void fragment_size(size_type size)
  {
    const exception::Exception ex{"TunnelDelivery: FirstDelivery", __func__};
    
    check_size(flags_, size, ex);

    size_ = std::forward<size_type>(size);
  }

  /// @brief Get the size of this FirstDelivery instructions
  size_type size() const
  {
    return size(flags_);
  }

  /// @brief Get the size of FirstDelivery instructions based on flags
  static size_type size(const flags_t& flags)
  {
    const exception::Exception ex{"TunnelDelivery: FirstDelivery", __func__};

    check_flags(flags, ex);

    const auto& flag_int = tini2p::under_cast(flags);
    const auto& delivery_flag = static_cast<flags_t>(flag_int & tini2p::under_cast(flags_t::DeliveryMask));

    const auto& is_local = static_cast<std::uint8_t>(delivery_flag == flags_t::Local);
    const auto& is_router = static_cast<std::uint8_t>(delivery_flag == flags_t::Router);
    const auto& is_tunnel = static_cast<std::uint8_t>(delivery_flag == flags_t::Tunnel);

    const auto& unfragmented = static_cast<std::uint8_t>((flag_int & tini2p::under_cast(flags_t::Fragmented)) == 0); 
    const auto& fragmented = static_cast<std::uint8_t>(!unfragmented);

    // Get the sizes for the various flag combinations, only one will be non-zero
    // Avoids branching, should get close to constant-time
    const auto& unlocal_len = is_local * unfragmented * tini2p::under_cast(UnfragLocalLen);
    const auto& local_len = is_local * fragmented * tini2p::under_cast(FragLocalLen);

    const auto& untunnel_len = is_tunnel * unfragmented * tini2p::under_cast(UnfragTunnelLen);
    const auto& tunnel_len = is_tunnel * fragmented * tini2p::under_cast(FragTunnelLen);

    const auto& unrouter_len = is_router * unfragmented * tini2p::under_cast(UnfragRouterLen);
    const auto& router_len = is_router * fragmented * tini2p::under_cast(FragRouterLen);

    // return the sum of the one non-zero length
    return (unlocal_len + local_len + unrouter_len + router_len + untunnel_len + tunnel_len);
  }

  /// @brief Get the max fragment length for this FirstDelivery instructions
  size_type max_fragment_size() const
  {
    return max_fragment_size(flags_, layer_);
  }

  /// @brief Get the max first fragment length for the given FirstDelivery instruction flags
  static size_type max_fragment_size(const flags_t& flags, const layer_t& layer)
  {
    const exception::Exception ex{"TunnelDelivery: FirstDelivery", __func__};

    size_type max_len(0);

    if (layer == layer_t::AES)
      max_len = tini2p::under_cast(AESCipherLen) - tini2p::under_cast(ChecksumPadDelimLen) - size(flags);
    else if (layer == layer_t::ChaCha)
      max_len = tini2p::under_cast(ChaChaCipherLen) - tini2p::under_cast(ChecksumPadDelimLen) - size(flags);
    else
      ex.throw_ex<std::invalid_argument>("invalid layer encryption.");

    return max_len;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Get a const reference to the layer encryption type
  const layer_t& layer() const noexcept
  {
    return layer_;
  }

  /// @brief Equality comparison with another FirstDelivery
  std::uint8_t operator==(const FirstDelivery& oth) const
  {
    const auto& layer_eq = static_cast<std::uint8_t>(layer_ == oth.layer_);
    const auto& flag_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);
    const auto& tun_eq = static_cast<std::uint8_t>(tun_id_ == oth.tun_id_);
    const auto& hash_eq = static_cast<std::uint8_t>(to_hash_ == oth.to_hash_);
    const auto& msg_eq = static_cast<std::uint8_t>(msg_id_ == oth.msg_id_);
    const auto& size_eq = static_cast<std::uint8_t>(size_ == oth.size_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    return (layer_eq * flag_eq * tun_eq * hash_eq * msg_eq * size_eq * buf_eq);
  }

  /// @brief Equality comparison with another FirstDelivery
  std::uint8_t operator==(const FirstDelivery& oth)
  {
    const auto& layer_eq = static_cast<std::uint8_t>(layer_ == oth.layer_);
    const auto& flag_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);
    const auto& tun_eq = static_cast<std::uint8_t>(tun_id_ == oth.tun_id_);
    const auto& hash_eq = static_cast<std::uint8_t>(to_hash_ == oth.to_hash_);
    const auto& msg_eq = static_cast<std::uint8_t>(msg_id_ == oth.msg_id_);
    const auto& size_eq = static_cast<std::uint8_t>(size_ == oth.size_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    return (layer_eq * flag_eq * tun_eq * hash_eq * msg_eq * size_eq * buf_eq);
  }

 private:
  void check_layer(const layer_t& layer, const exception::Exception& ex) const
  {
    if (layer != layer_t::AES && layer != layer_t::ChaCha)
      ex.throw_ex<std::invalid_argument>("invalid layer encryption.");
  }

  static void check_flags(const flags_t& flags, const exception::Exception& ex)
  {  // check for valid instruction flags
    const auto& flag_int = tini2p::under_cast(flags);
    if (flag_int & tini2p::under_cast(flags_t::FollowMask))
      ex.throw_ex<std::logic_error>("follow-on fragment bit set.");

    if (static_cast<flags_t>(flag_int & tini2p::under_cast(flags_t::DeliveryMask)) == flags_t::Invalid)
      ex.throw_ex<std::logic_error>("invalid delivery type.");

    if (flag_int & tini2p::under_cast(flags_t::Delay))
      ex.throw_ex<std::logic_error>("delay bit set, unimplemented.");

    if (flag_int & tini2p::under_cast(flags_t::Extended))
      ex.throw_ex<std::logic_error>("extended options bit set, unimplemented.");

    if (flag_int & tini2p::under_cast(flags_t::ReservedMask))
      ex.throw_ex<std::logic_error>("reserved bits set.");
  }

  void check_tunnel_id(const flags_t& flags, const std::optional<tunnel_id_t>& tun_id, const exception::Exception& ex)
      const
  {
    const auto& delivery_flag =
        static_cast<flags_t>(tini2p::under_cast(flags) & tini2p::under_cast(flags_t::DeliveryMask));

    if (delivery_flag == flags_t::Tunnel)
      {
        if (tun_id == std::nullopt)
          ex.throw_ex<std::logic_error>("tunnel delivery set, and tunnel ID is null.");

        if (tun_id != std::nullopt && *tun_id == 0)
          ex.throw_ex<std::logic_error>("tunnel ID must be non-zero.");
      }

    if (delivery_flag != flags_t::Tunnel && tun_id != std::nullopt)
      ex.throw_ex<std::logic_error>("non-tunnel delivery, tunnel ID is set.");
  }

  void check_to_hash(const flags_t& flags, const std::optional<to_hash_t>& to_hash, const exception::Exception& ex)
      const
  {
    const auto& delivery_flag =
        static_cast<flags_t>(tini2p::under_cast(flags) & tini2p::under_cast(flags_t::DeliveryMask));

    if (delivery_flag == flags_t::Local && to_hash != std::nullopt)
      ex.throw_ex<std::logic_error>("local delivery, and to-hash is set.");

    if ((delivery_flag == flags_t::Router || delivery_flag == flags_t::Tunnel) && to_hash == std::nullopt)
      ex.throw_ex<std::logic_error>("router or tunnel delivery, and to-hash is null.");

    if (to_hash != std::nullopt && to_hash->is_zero())
      ex.throw_ex<std::logic_error>("to-hash is zeroed.");
  }

  void check_to_hash(const std::optional<to_hash_t>& to_hash, const exception::Exception& ex) const
  {
    if (to_hash == std::nullopt)
      ex.throw_ex<std::runtime_error>("null to hash");
  }

  void check_message_id(const flags_t& flags, const std::optional<message_id_t>& msg_id, const exception::Exception& ex)
      const
  {
    const auto& is_fragmented = tini2p::under_cast(flags) & tini2p::under_cast(flags_t::Fragmented);
    if (is_fragmented && msg_id == std::nullopt)
      ex.throw_ex<std::logic_error>("fragmented message, and message ID is unset.");

    if (!is_fragmented && msg_id != std::nullopt)
      ex.throw_ex<std::logic_error>("unfragmented message, and message ID is set.");
  }

  void check_size(const flags_t& flags, const size_type& size, const exception::Exception& ex) const
  {
    const auto& min_len = tini2p::under_cast(MinFragmentLen);
    const auto& max_len = max_fragment_size(flags, layer_);

    if (size < min_len || size > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid fragment size: " + std::to_string(size) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  void check_buffer(const buffer_t& buf, const exception::Exception& ex) const
  {
    const auto& buf_len = buf.size();
    const auto& min_len = tini2p::under_cast(UnfragLocalLen);
    const auto& max_len = tini2p::under_cast(FragTunnelLen);

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid instructions size: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }
  
  void check_mode(const mode_t& mode, const exception::Exception& ex) const
  {
    if (mode != mode_t::Unfragmented && mode != mode_t::Fragmented)
      ex.throw_ex<std::logic_error>("invalid fragmentation mode.");
  }

  void check_delivery(const delivery_t& delivery, const exception::Exception& ex) const
  {
    if (delivery != delivery_t::Local && delivery != delivery_t::Router && delivery != delivery_t::Tunnel)
      ex.throw_ex<std::logic_error>("invalid delivery type.");
  }

  layer_t layer_;
  flags_t flags_;
  std::optional<tunnel_id_t> tun_id_;
  std::optional<to_hash_t> to_hash_;
  std::optional<message_id_t> msg_id_;
  size_type size_;
  buffer_t buf_;
};

/// @class FollowDelivery
/// @brief Tunnel delivery instructions for follow-on I2NP fragments
class FollowDelivery
{
 public:
  enum
  {
    FlagLen = 1,
    MessageIDLen = 4,
    SizeLen = 2,
    HeaderLen = 7,
    MinFragmentNum = 1,
    MaxFragmentNum = 63,
    TunnelMessageLen = 1028,
    TunnelHeaderLen = 25,
    ChecksumPadDelimLen = 5,
    MinFragmentLen = 1,
    AESCipherLen = 1008,
    ChaChaCipherLen = 992,
  };

  enum struct IsLast
  {
    False = 0x00,
    True = 0x1,
  };

  enum struct Flags : std::uint8_t
  {
    Follow = 0x80,                        //< Follow-on fragment, 1000 0000
    FragNumMask = 0x7e,                 //< Fragment number mask, 0111 1110
    LastFragment = 0x01,                       //< Last fragment, 0000 0001
    ClearNumMask = 0x81,      //< Clear the fragment number mask, 1000 0001
    ClearLastMask = 0xed,  //< Clear the last fragment flag mask, 1111 1110
  };

  using layer_t = Layer;  //< Layer encryption trait alias
  using flags_t = Flags;  //< Flags trait alias
  using frag_num_t = std::uint8_t;  //< Fragment number trait alias
  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using size_type = std::uint16_t;  //< Size trait alias
  using is_last_t = IsLast;  //< Fragment is I2NP message fragment trait alias
  using buffer_t = crypto::FixedSecBytes<HeaderLen>;  //< Buffer trait alias

  /// @brief Default ctor, creates a FollowDelivery w/ null message ID and minimum fragment size
  /// @detail Caller responsible for setting:
  ///     - fragment number
  ///     - last fragment flag (if needed)
  ///     - fragment size
  /// @note Useful for deserializing from buffer
  FollowDelivery() : layer_(layer_t::AES), flags_(flags_t::Follow), msg_id_(0), size_(tini2p::under_cast(MinFragmentLen)), buf_()
  {
    const exception::Exception ex{"TunnelDelivery: FollowDelivery", __func__};

    flags_ = FragNumToFlags(flags_, tini2p::under_cast(MinFragmentNum), ex);

    serialize();
  }

  /// @brief Fully-initializing ctor, creates a FollowDelivery from fragment number, length, and is last parity flag
  /// @param frag_num Fragment number
  /// @param frag_len Fragment size
  /// @param is_last Is this the last fragment of the I2NP message
  FollowDelivery(
      const layer_t& layer,
      frag_num_t frag_num,
      message_id_t msg_id,
      size_type frag_len,
      const is_last_t& is_last = is_last_t::False)
      : layer_(layer), flags_(flags_t::Follow), msg_id_(std::forward<message_id_t>(msg_id)), buf_()
  {
    const exception::Exception ex{"TunnelDelivery: FollowDelivery", __func__};

    check_layer(layer, ex);
    check_size(frag_len, ex);
    check_is_last(frag_num, is_last, ex);

    flags_ = IsLastToFlags(flags_, is_last);
    flags_ = FragNumToFlags(flags_, frag_num, ex);
    
    size_ = std::forward<size_type>(frag_len);
  }

  /// @brief Deserializing ctor, creates a FollowDelivery from a secure buffer
  FollowDelivery(const layer_t& layer, buffer_t buf) : layer_(layer), buf_(std::forward<buffer_t>(buf))
  {
    const exception::Exception ex{"TunnelDelivery: FollowDelivery", __func__};

    check_layer(layer, ex);

    deserialize();
  }

  /// @brief Serialize FollowDelivery to the buffer
  void serialize()
  {
    const exception::Exception ex{"TunnelDelivery: FollowDelivery", __func__};

    check_flags(flags_, ex);
    check_size(size_, ex);

    tini2p::BytesWriter<buffer_t> writer(buf_);

    writer.write_bytes(flags_);
    writer.write_bytes(msg_id_, tini2p::Endian::Big);
    writer.write_bytes(size_, tini2p::Endian::Big);
  }

  /// @brief Deserialize FollowDelivery from the buffer
  void deserialize()
  {
    const exception::Exception ex{"TunnelDelivery: FollowDelivery", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    flags_t flags;
    reader.read_bytes(flags);
    check_flags(flags, ex);
    flags_ = std::move(flags);

    reader.read_bytes(msg_id_, tini2p::Endian::Big);

    size_type frag_len;
    reader.read_bytes(frag_len, tini2p::Endian::Big);
    check_size(frag_len, ex);
    size_ = std::move(frag_len);
  }

  /// @brief Get a const reference to the flags
  const flags_t& flags() const noexcept
  {
    return flags_;
  }

  /// @brief Get the fragment number
  frag_num_t fragment_number() const noexcept
  {
    return fragment_number(flags_);
  }

  /// @brief Set the fragment number in the instructions flags
  /// @param num Fragment number to set
  /// @param is_last Is this the last fragment
  void fragment_number(const frag_num_t& num, const is_last_t& is_last = is_last_t::False)
  {
    const exception::Exception ex{"TunnelDelivery: FollowDelivery", __func__};

    check_is_last(num, is_last, ex);

    flags_ = IsLastToFlags(flags_, is_last);
    flags_ = FragNumToFlags(flags_, num, ex);
  }

  /// @brief Get the message ID
  message_id_t message_id() const noexcept
  {
    return msg_id_;
  }

  /// @brief Get a const reference to the fragment size
  const size_type& fragment_size() const noexcept
  {
    return size_;
  }

  /// @brief Set the fragment size
  void fragment_size(size_type size)
  {
    const exception::Exception ex{"TunnelDelivery: FollowDelivery", __func__};
    
    check_size(size, ex);

    size_ = std::forward<size_type>(size);
  }

  /// @brief Get the maximum size of the fragment
  static size_type max_fragment_size(const layer_t& layer)
  {
    const exception::Exception ex{"TunnelDelivery: FollowDelivery", __func__};

    check_layer(layer, ex);

    size_type max_len(0);

    if (layer == layer_t::AES)
      max_len = tini2p::under_cast(AESCipherLen) - tini2p::under_cast(ChecksumPadDelimLen) - tini2p::under_cast(HeaderLen);
    else
      max_len = tini2p::under_cast(ChaChaCipherLen) - tini2p::under_cast(ChecksumPadDelimLen) - tini2p::under_cast(HeaderLen);

    return max_len;
  }

  /// @brief Get whether this is the last fragment
  std::uint8_t is_last() const noexcept
  {
    return tini2p::under_cast(flags_) & tini2p::under_cast(flags_t::LastFragment);
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Get the instructions size
  size_type size() const noexcept
  {
    return tini2p::under_cast(HeaderLen);
  }

  /// @brief Get a const reference to the layer encryption type
  const layer_t& layer() const noexcept
  {
    return layer_;
  }

  /// @brief Add fragment number to instructions flags
  /// @note Convenience function for check valid numbers, and shifting number into place
  /// @param flags Instructions flags to add fragment number to
  /// @param num Fragment number to add
  /// @param ex Exception handler
  /// @returns Flags with fragment number added
  flags_t FragNumToFlags(const flags_t& flags, const frag_num_t& num, const exception::Exception& ex) const
  {
    check_fragment_number(num, ex);

    const auto& is_last =
        static_cast<is_last_t>(tini2p::under_cast(flags_) & tini2p::under_cast(flags_t::LastFragment));

    check_is_last(num, is_last, ex);

    auto flag_int = tini2p::under_cast(flags);
    flag_int &= tini2p::under_cast(flags_t::ClearNumMask);
    flag_int |= num << 1;

    return static_cast<flags_t>(flag_int);
  }

  /// @brief Equality comparison with another FollowDelivery
  std::uint8_t operator==(const FollowDelivery& oth) const
  {  // attempt constant-time comparison
    const auto& layer_eq = static_cast<std::uint8_t>(layer_ == oth.layer_);
    const auto& flag_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);
    const auto& msg_eq = static_cast<std::uint8_t>(msg_id_ == oth.msg_id_);
    const auto& size_eq = static_cast<std::uint8_t>(size_ == oth.size_);

    return (layer_eq * flag_eq * msg_eq * size_eq);
  }

  /// @brief Equality comparison with another FollowDelivery
  std::uint8_t operator==(const FollowDelivery& oth)
  {  // attempt constant-time comparison
    const auto& layer_eq = static_cast<std::uint8_t>(layer_ == oth.layer_);
    const auto& flag_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);
    const auto& msg_eq = static_cast<std::uint8_t>(msg_id_ == oth.msg_id_);
    const auto& size_eq = static_cast<std::uint8_t>(size_ == oth.size_);

    return (layer_eq * flag_eq * msg_eq * size_eq);
  }

 private:
  static void check_layer(const layer_t& layer, const exception::Exception& ex)
  {
    if (layer != layer_t::AES && layer != layer_t::ChaCha)
      ex.throw_ex<std::invalid_argument>("invalid layer encryption.");
  }

  flags_t IsLastToFlags(const flags_t& flags, const is_last_t& is_last) const noexcept
  {
    auto ret_flags = static_cast<flags_t>(tini2p::under_cast(flags_) & tini2p::under_cast(flags_t::ClearLastMask));
    return static_cast<flags_t>(tini2p::under_cast(ret_flags) | tini2p::under_cast(is_last));
  }

  void check_flags(const flags_t& flags, const exception::Exception& ex) const
  {
    const auto& flag_int = tini2p::under_cast(flags);

    if ((flag_int & tini2p::under_cast(flags_t::Follow)) == 0)
      ex.throw_ex<std::logic_error>("follow-on flag unset.");
    
    const auto& frag_num = fragment_number(flags);
    check_fragment_number(frag_num, ex);
    check_is_last(frag_num, static_cast<is_last_t>(flag_int & tini2p::under_cast(flags_t::LastFragment)), ex);
  }

  void check_fragment_number(const frag_num_t& num, const exception::Exception& ex) const
  {
    const auto& min_num = tini2p::under_cast(MinFragmentNum);
    const auto& max_num = tini2p::under_cast(MaxFragmentNum);

    if (num < min_num || num > max_num)
      {
        ex.throw_ex<std::logic_error>(
            "invalid fragment number: " + std::to_string(num) + ", min: " + std::to_string(min_num)
            + ", max: " + std::to_string(max_num));
      }
  }

  void check_size(const size_type& size, const exception::Exception& ex) const
  {
    const auto& min_len = tini2p::under_cast(MinFragmentLen);
    const auto& max_len = max_fragment_size(layer_);

    if (size < min_len || size > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid fragment size: " + std::to_string(size) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  frag_num_t fragment_number(const flags_t& flags) const noexcept
  {
    return (tini2p::under_cast(flags) & tini2p::under_cast(flags_t::FragNumMask)) >> 1;
  }

  void check_is_last(const frag_num_t& frag_num, const is_last_t& is_last, const exception::Exception& ex) const
  {
    if (is_last != is_last_t::False && is_last != is_last_t::True)
      ex.throw_ex<std::invalid_argument>("invalid is last flag.");

    if (frag_num == tini2p::under_cast(MaxFragmentNum) && is_last != is_last_t::True)
      ex.throw_ex<std::invalid_argument>("fragment number: " + std::to_string(frag_num) + " must be last.");
  }

  layer_t layer_;
  flags_t flags_;
  message_id_t msg_id_;
  size_type size_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_I2NP_TUNNEL_DELIVERY_H_
