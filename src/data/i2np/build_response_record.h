/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_I2NP_BUILD_RESPONSE_RECORD_H_
#define SRC_DATA_I2NP_BUILD_RESPONSE_RECORD_H_

#include "src/crypto/aes.h"
#include "src/crypto/chacha20.h"

#include "src/crypto/tunnel/build_key_info.h"

#include "src/data/blocks/build_options.h"
#include "src/data/router/identity.h"

namespace tini2p
{
namespace data
{
/// @class BuildResponseRecord
/// @brief Class for creating and processing BuildResponseRecords
/// @tparam TSym Symmetric crypto type for reply, layer, and random keys
template <
    class TSym,
    typename = std::enable_if_t<std::is_same<TSym, crypto::AES>::value || std::is_same<TSym, crypto::ChaCha20>::value>>
class BuildResponseRecord
{
 public:
  enum struct Flags : std::uint8_t
  {
    Accept = 0x00,  //< ACCEPT(0), see spec
    Reject = 0x1e,  //< REJECT_BANDWIDTH(30), see spec
  };

  /// @brief Deserialization mode for processing encrypted or decrypted records
  enum struct DeserializeMode : std::uint8_t
  {
    Partial,
    Full,
  };

  enum : std::uint16_t
  {
    RecordLen = 528,
    FlagsLen = 1,
    AESBodyLen = 496,
    ChaChaBodyLen = 512,
    AESOptPadLen = 495,  //< 528 - Checksum(32) - ReplyByte(1)
    ChaChaOptPadLen = 511,  //< 528 - MAC(16) - ReplyByte(1)
  };

  using sym_t = TSym;  //< Symmetric crypto trait alias
  using aes_t = crypto::AES;  //< AES trait alias
  using chacha_t = crypto::ChaCha20;  //< ChaCha20 trait alias
  using sha_t = crypto::Sha256;  //< Sha256 trait alias
  using options_t = data::BuildOptionsBlock;   //< Tunnel build options trait alias
  using padding_t = data::PaddingBlock;  //< Padding trait alias
  using flags_t = Flags;  //< Reply flags trait alias
  using key_info_t = crypto::BuildKeyInfo<sym_t>;  //< Build key information trait alias
  using deserialize_mode_t = DeserializeMode;  //< Deserialize mode trait alias
  using buffer_t = crypto::FixedSecBytes<RecordLen>;  //< Buffer trait alias

  /// @brief Default ctor, creates a BuildResponseRecord to accept the requested tunnel participation
  BuildResponseRecord() : checksum_(), options_(), padding_(get_padding_len()), flags_(flags_t::Accept), buf_()
  {
    const exception::Exception ex{"BuildResponseRecord", __func__};

    serialize();
  }

  /// @brief Fully initializing ctor, creates BuildResponseRecord with the given flags
  /// @detail Caller is responsible for encrypting the reply record
  /// @param flags Reply flag (ACCEPT or REJECT) 
  /// @param key_info Key information for encrypting the record
  /// @throws Logic errors if key information is in an invalid state
  BuildResponseRecord(flags_t flags) : checksum_(), options_(), padding_(get_padding_len()), flags_(), buf_()
  {
    const exception::Exception ex{"BuildResponseRecord", __func__};

    check_flags(flags, ex);

    flags_ = std::forward<flags_t>(flags);

    serialize();
  }

  /// @brief Deserializing ctor, deserialize a BuildResponseRecord from a secure buffer
  /// @detail Caller is responsible for decrypting first
  /// @param buf Secure buffer with a serialized and encrypted BuildResponseRecord
  /// @param mode Deserialize mode for processing an encrypted or decrypted record
  /// @throws Length error if buffer is invalid length
  /// @throws Logic errors if key information is in an invalid state
  BuildResponseRecord(buffer_t buf, const deserialize_mode_t& mode = deserialize_mode_t::Full)
  {
    const exception::Exception ex{"BuildResponseRecord", __func__};

    buf_ = std::forward<buffer_t>(buf);

    if (mode == deserialize_mode_t::Full)
      deserialize();
  }

  /// @brief Serialize and encrypt the BuildResponseRecord to the buffer
  /// @throws Logic error for invalid flags
  /// @throws Logic errors for invalid options and padding
  /// @throws Logic errors if key information is in an invalid state
  void serialize()
  {
    const exception::Exception ex{"BuildResponseRecord", __func__};

    check_flags(flags_, ex);

    options_.serialize();
    padding_.serialize();

    check_options_and_padding(options_, padding_, ex);

    tini2p::BytesWriter<buffer_t> writer(buf_);
    
    // skip checksum to write later if this is an AES BuildResponseRecord
    const auto& digest_len = tini2p::under_cast(sha_t::DigestLen);
    if (std::is_same<sym_t, aes_t>::value)
      writer.skip_bytes(digest_len);

    writer.write_data(options_.buffer());

    writer.write_data(padding_.buffer());

    writer.write_bytes(flags_);

    // calculate and write checksum if this is an AES BuildResponseRecord
    if (std::is_same<sym_t, aes_t>::value)
    { 
      writer.reset();
      sha_t::Hash(checksum_, buf_.data() + digest_len, tini2p::under_cast(AESBodyLen));
      writer.write_data(checksum_);
    }
  }

  /// @brief Decrypt and deserialize the BuildResponseRecord from the buffer
  /// @throws Logic error for invalid checksum (if AES)
  /// @throws Logic errors for invalid options and padding
  /// @throws Logic errors if key information is in an invalid state
  /// @throws Logic error for invalid flags
  void deserialize()
  {
    const exception::Exception ex{"BuildResponseRecord", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    // calculate checksum if this is an AES BuildResponseRecord
    if (std::is_same<sym_t, aes_t>::value)
      {
        sha_t::digest_t checksum, calc_checksum;
        reader.read_data(checksum);

        sha_t::Hash(calc_checksum, buf_.data() + reader.count(), reader.gcount());

        if (checksum != calc_checksum)
          {
            ex.throw_ex<std::logic_error>(
                "invalid checksum: " + tini2p::bin_to_hex(checksum, ex)
                + ", expected: " + tini2p::bin_to_hex(calc_checksum, ex));
          }

       checksum_.swap(checksum);
      }

    // read and check the options block
    options_t::type_t opt_type;
    reader.read_bytes(opt_type);

    if (opt_type != options_t::type_t::BuildOptions)
      ex.throw_ex<std::logic_error>("invalid tunnel build options type");
    
    options_t::size_type opt_size;
    reader.read_bytes(opt_size, tini2p::Endian::Big);

    const auto& block_header_len = tini2p::under_cast(options_t::HeaderLen);
    const auto& full_opt_len = block_header_len + opt_size;
    std::int32_t remaining = std::is_same<sym_t, aes_t>::value
                                 ? reader.gcount() - FlagsLen
                                 : reader.gcount() - FlagsLen - key_info_t::aead_t::MACLen;

    if (static_cast<std::int32_t>(full_opt_len) > remaining)
      {
        ex.throw_ex<std::length_error>(
            "invalid options length: " + std::to_string(full_opt_len)
            + ", remaining record: " + std::to_string(remaining));
      }

    // skip back to beginning of options block
    reader.skip_back(block_header_len);

    options_t::buffer_t opt_buf(full_opt_len);
    reader.read_data(opt_buf);
    options_t opt(opt_buf);
    options_ = std::move(opt);

    // check for a padding block
    remaining = std::is_same<sym_t, aes_t>::value ? reader.gcount() - FlagsLen
                                                  : reader.gcount() - FlagsLen - key_info_t::aead_t::MACLen;

    if (remaining > 0)
      {  // read remaining data as padding
        padding_t::type_t type;
        reader.read_bytes(type);

        if (type != padding_t::type_t::Padding)
          ex.throw_ex<std::logic_error>("invalid padding block type.");

        padding_t::size_type pad_len;
        reader.read_bytes(pad_len, tini2p::Endian::Big);
        const auto& full_pad_len = static_cast<std::int32_t>(block_header_len + pad_len);

        if (full_pad_len > remaining)
          ex.throw_ex<std::length_error>(
              "invalid padding length: " + std::to_string(full_pad_len)
              + ", remaining record: " + std::to_string(remaining));

        // skip back to beginning of padding block
        reader.skip_back(block_header_len);

        padding_t::buffer_t pad_buf(full_pad_len);
        reader.read_data(pad_buf);

        padding_ = std::move(padding_t(pad_buf));
      }

    // read the reply flags
    flags_t flags;
    reader.read_bytes(flags);
    check_flags(flags, ex);
    flags_ = std::move(flags);
  }

  /// @brief Get the record checksum (if AES)
  template <typename S = sym_t, typename = std::enable_if_t<std::is_same<S, aes_t>::value>>
  const sha_t::digest_t& checksum() const noexcept
  {
    return checksum_;
  }

  /// @brief Get a const reference to the reply flags
  const flags_t& flags() const noexcept
  {
    return flags_;
  }

  /// @brief Get a const reference to the reply flags
  BuildResponseRecord& flags(flags_t flags)
  {
    const exception::Exception ex{"BuildResponseRecord", __func__};

    check_flags(flags, ex);
    
    flags_ = std::forward<flags_t>(flags);

    return *this;
  }

  /// @brief Get a const reference to the reply build options 
  const options_t& options() const noexcept
  {
    return options_;
  }

  /// @brief Get a const reference to the reply padding
  const padding_t& padding() const noexcept
  {
    return padding_;
  }

  /// @brief Get a const reference to the reply buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a const reference to the reply buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Equality comparison with another BuildResponseRecord
  std::uint8_t operator==(const BuildResponseRecord& oth) const
  {  // attempt constant-time comparison
    const auto& flags_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);
    const auto& opt_eq = static_cast<std::uint8_t>(options_ == oth.options_);
    const auto& pad_eq = static_cast<std::uint8_t>(padding_ == oth.padding_);

    return (flags_eq * opt_eq * pad_eq);
  }

#ifdef TINI2P_TESTS
  /// @brief Convenience function to calculate AES BuildResponseRecord Sha256 checksum
  /// @note Only useful for testing
  template <typename S = sym_t, typename = std::enable_if_t<std::is_same<S, aes_t>::value>>
  static sha_t::digest_t CalculateChecksum(const flags_t& flags, const options_t& options, const padding_t& padding)
  {
    const auto& body_len = tini2p::under_cast(AESBodyLen);

    buffer_t buf;

    tini2p::BytesWriter<buffer_t> writer(buf);

    writer.write_data(options.buffer());
    writer.write_data(padding.buffer());
    writer.write_bytes(flags);

    sha_t::digest_t checksum;
    sha_t::Hash(checksum, buf.data(), body_len);

    return checksum;
  }
#endif

 private:
  padding_t::size_type get_padding_len() const
  {
    const auto& opt_pad_len = std::is_same<sym_t, aes_t>::value ? tini2p::under_cast(AESOptPadLen) :
    tini2p::under_cast(ChaChaOptPadLen);

    return opt_pad_len - tini2p::under_cast(padding_t::HeaderLen) - options_.size();
  }

  void check_flags(const flags_t& flags, const exception::Exception& ex) const
  {
    if (flags != flags_t::Accept && flags != flags_t::Reject)
      {
        const auto& accept_str = std::to_string(tini2p::under_cast(flags_t::Accept));
        const auto& reject_str = std::to_string(tini2p::under_cast(flags_t::Reject));

        ex.throw_ex<std::logic_error>(
            "invalid reply flags expected ACCEPT: " + accept_str + " or REJECT: " + reject_str);
      }
  }

  void check_options_and_padding(const options_t& options, const padding_t& padding, const exception::Exception& ex)
      const
  {
    const auto& opt_len = options.size();
    const auto& pad_len = padding.size();
    options_t::size_type exp_len(0);

    if (std::is_same<sym_t, aes_t>::value)
      exp_len = tini2p::under_cast(AESOptPadLen);
    else if (std::is_same<sym_t, chacha_t>::value)
      exp_len = tini2p::under_cast(ChaChaOptPadLen);
    else
      ex.throw_ex<std::logic_error>("invalid symmetric encryption.");

    if (opt_len + pad_len != exp_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid trailing options and padding length: " + std::to_string(opt_len + pad_len)
            + ", options: " + std::to_string(opt_len) + ", padding: " + std::to_string(pad_len)
            + ", total expected: " + std::to_string(exp_len));
      }
  }

  sha_t::digest_t checksum_;
  options_t options_;
  padding_t padding_;
  flags_t flags_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif // SRC_DATA_I2NP_BUILD_RESPONSE_RECORD_H_
