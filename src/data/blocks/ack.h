/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_ACK_H_
#define SRC_DATA_BLOCKS_ACK_H_

#include <mutex>
#include <unordered_map>

#include "src/data/blocks/block.h"

namespace tini2p
{
namespace data
{
class AckBlock : public Block
{
 public:
  enum
  {
    KeyIDLen = 2,
    NLen = 2,
    AckLen = KeyIDLen + NLen,  //< Ack length (KeyID-N pair), see spec
    MinAcksLen = AckLen,  //< a single Ack is required, see spec 
    MaxAcksLen = MaxLen,  //< fit as many Acks in block as possible, should we limit more?
    MinAcksNum = 1,  //< must have at least one Ack, see spec
    MaxAcksNum = 16379,  // MaxLen / MinAcksLen, max number of Acks to fit in a valid block
  };

  enum
  {
    KeyIndex = 0,
    NIndex,
  };

  using key_id_t = std::uint16_t;  //< KeyID trait alias
  using n_t = std::uint16_t;  //< N trait alias
  using acks_t = std::unordered_map<key_id_t, n_t>;  //< Acks container trait alias

  /// @brief Default ctor, creates an partly-initialized AckBlock
  /// @detail Must set at least one Ack (KeyID-N pair) before serializing
  /// @note Needed for inclusion in std::variant containers
  AckBlock() : Block(type_t::Ack), acks_() {}

  /// @brief Fully initializing ctor, creates an AckBlock with one entry
  AckBlock(key_id_t key, n_t n) : Block(type_t::Ack, tini2p::under_cast(MinAcksLen)), acks_{{key, n}}
  {
    serialize();
  }

  /// @brief Fully initializing ctor, creates an AckBlock with one-to-MaxAcksNum Acks
  /// @throws Length error if Acks container length is outside valid range
  explicit AckBlock(acks_t acks) : Block(type_t::Ack), acks_()
  {
    const exception::Exception ex{"AckBlock", __func__};

    const auto& acks_len = acks.size() * tini2p::under_cast(AckLen);
    check_len(
        tini2p::under_cast(HeaderLen) + acks_len, tini2p::under_cast(MinAcksLen), tini2p::under_cast(MaxAcksLen), ex);

    size_ = acks_len;
    acks_ = std::forward<acks_t>(acks);

    serialize();
  }

  /// @brief Deserializing ctor, creates an AckBlock from a secure buffer
  explicit AckBlock(buffer_t buf) : Block(type_t::Ack), acks_()
  {
    const exception::Exception ex{"AckBlock", __func__};

    check_len(buf.size(), tini2p::under_cast(MinAcksLen), tini2p::under_cast(MaxAcksLen), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Copy-ctor
  AckBlock(const AckBlock& oth) : Block(type_t::Ack)
  {
    acks_ = oth.acks_;
    size_ = oth.size_;
    buf_ = oth.buf_;
  }

  /// @brief Copy-assignment operator
  void operator=(const AckBlock& oth)
  {
    acks_ = oth.acks_;
    size_ = oth.size_;
    buf_ = oth.buf_;
  }

  /// @brief Serialize AckBlock to buffer
  /// @throws Length error if Acks container length is outside valid range
  void serialize()
  {
    const exception::Exception ex{"AckBlock", __func__};

    const auto& acks_len = acks_.size() * tini2p::under_cast(AckLen);
    check_len(
        tini2p::under_cast(HeaderLen) + acks_len, tini2p::under_cast(MinAcksLen), tini2p::under_cast(MaxAcksLen), ex);

    // update block and buffer size
    size_ = acks_len;
    buf_.resize(size());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    write_header(writer);
    for (const auto& ack : acks_)
    {
      writer.write_bytes(std::get<KeyIndex>(ack), tini2p::Endian::Big);
      writer.write_bytes(std::get<NIndex>(ack), tini2p::Endian::Big);
    }
  }

  /// @brief Deserialize AckBlock from buffer
  /// @note Caller must resize buffer appropriately for claimed size
  /// @throws Logic error if duplicate Acks deserialized
  void deserialize()
  {
    const exception::Exception ex{"AckBlock", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);
    read_header(reader, type_t::Ack, tini2p::under_cast(MinAcksLen), tini2p::under_cast(MaxAcksLen), ex);

    acks_t acks;
    const auto& exp_len = static_cast<std::size_t>(HeaderLen) + size_;
    while(reader.count() < exp_len)
    {
      key_id_t key_id;
      n_t n;

      reader.read_bytes(key_id, tini2p::Endian::Big);
      check_duplicate_acks(acks, key_id, ex);

      reader.read_bytes(n, tini2p::Endian::Big);

      acks.try_emplace(std::move(key_id), std::move(n));
    }

    {  // lock acks
      // swap with the temporary Acks container
      std::lock_guard<std::mutex> agd(acks_mutex_);
      acks_.swap(acks);
    }  // end-acks-lock

    // reclaim unused space (should be no-ops if caller did resizing prior)
    buf_.resize(reader.count());
    buf_.shrink_to_fit();
  }

  /// @brief Get a const reference to the Acks container
  const acks_t& acks() const noexcept
  {
    return acks_;
  }

  /// @brief Set a new acks container
  void acks(acks_t acks)
  {
    const exception::Exception ex{"AckBlock", __func__};

    const auto& acks_len = acks.size() * tini2p::under_cast(AckLen);
    check_len(acks_len, tini2p::under_cast(MinAcksLen), tini2p::under_cast(MaxAcksLen), ex);

    // update block size
    size_ = acks_len;

    std::lock_guard<std::mutex> agd(acks_mutex_);
    acks_ = std::forward<acks_t>(acks);
  }

  /// @brief Add an Ack to the container
  /// @detail Updates an Ack entry if an Ack with `key_id` is already present
  void add_ack(key_id_t key_id, n_t n)
  {
    const exception::Exception ex{"AckBlock", __func__};

    // check if we are adding a new entry
    const auto& new_key_id = static_cast<std::uint8_t>(acks_.find(key_id) == acks_.end());
    const auto& ack_len = tini2p::under_cast(AckLen);
    const auto& acks_len = (acks_.size() * ack_len) + (new_key_id * ack_len);
    // check updated Acks length is in valid range
    check_len(acks_len, ack_len, tini2p::under_cast(MaxAcksLen), ex);

    // update block size
    size_ = acks_len;

    std::lock_guard<std::mutex> agd(acks_mutex_);
    acks_[key_id] = n;
  }

  /// @brief Equality comparison with another AckBlock
  std::uint8_t operator==(const AckBlock& oth) const
  {
    return static_cast<std::uint8_t>(acks_ == oth.acks_);
  }

  /// @brief For templated use with I2NP blocks
  std::uint8_t msg_type() const
  {
    const exception::Exception ex{"AckBlock", __func__};

    ex.throw_ex<std::logic_error>("invalid call to non-I2NP block");

    return 0;
  }

 private:
  // only called during deserialization to avoid DoS from duplicate Acks
  void check_duplicate_acks(const acks_t& acks, const key_id_t key_id, const exception::Exception& ex)
  {
    if (acks.find(key_id) != acks.end())
      ex.throw_ex<std::logic_error>("duplicate Acks found for key ID: " + std::to_string(key_id));
  }

  acks_t acks_;
  std::mutex acks_mutex_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_ACK_H_
