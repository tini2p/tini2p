/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_BLOCK_H_
#define SRC_DATA_BLOCKS_BLOCK_H_

#include "src/bytes.h"
#include "src/crypto/sec_bytes.h"
#include "src/exception/exception.h"

namespace tini2p 
{
namespace data
{
/// @brief Container for NTCP2 + ECIES blocks
class Block
{
 public:
  enum : std::uint16_t
  {
    TypeLen = 1,
    SizeLen = 2,
    HeaderLen = 3,
    MaxLen = 65516,
    MaxTotalLen = HeaderLen + MaxLen,
  };

  enum : std::uint8_t
  {
    TypeOffset = 0,
    SizeOffset = 1,
    DataOffset = 3
  };

  enum struct Type : std::uint8_t
  {
    DateTime = 0,
    NTCP2Options,
    Info,
    I2NP,
    Termination,
    ECIESOptions,
    MessageNumber,
    NewDHKey,
    Ack,
    AckRequest,
    NewSessionAck,
    Garlic,
    BuildOptions,
    // 13-223 unknown, see spec
    // 224-253 + 255 reserved for future use, see spec
    Padding = 254,
    Reserved = 255,
  };

  using type_t = Type;  //< Type trait alias
  using size_type = std::uint16_t;  //< Size type trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  using pointer = Block*;  //< Non-owning pointer trait alias
  using unique_ptr = std::unique_ptr<Block>;  //< Unique pointer trait alias
  using shared_ptr = std::shared_ptr<Block>;  //< Shared pointer trait alias

  virtual ~Block() = default;

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  /// @brief Get the block type
  type_t type() const noexcept
  {
    return type_;
  }

  /// @brief Get the block's total size
  std::uint16_t size() const noexcept
  {
    return tini2p::under_cast(HeaderLen) + size_;
  }

  /// @brief get the payload data size
  std::uint16_t data_size() const noexcept
  {
    return size_;
  }

  /// @brief Serialize block to buffer
  /// @detail Must be implemented by inheriting classes
  virtual void serialize() = 0;

  /// @brief Deserialize block from buffer
  /// @detail Must be implemented by inheriting classes
  virtual void deserialize() = 0;

 protected:
  /// @brief Create an empty Block of a given type
  /// @detail Intended to be called by inheriting classes
  Block(const type_t type) : type_(type), size_(0), buf_(HeaderLen) {}

  /// @brief Create a Block of a given type and size
  /// @param type Type of block to create
  /// @param size Size of the block payload
  /// @detail Intended to be called by inheriting classes
  Block(const type_t type, const std::uint16_t size) : type_(type), size_(size), buf_(HeaderLen + size) {}

  // write common block header to buffer
  void write_header(tini2p::BytesWriter<buffer_t>& writer)
  {
    writer.write_bytes(type_);
    writer.write_bytes(size_, tini2p::Endian::Big);
  }

  // read the block type and data size, throw if read values don't match expected values
  void read_header(
      tini2p::BytesReader<buffer_t>& reader,
      const type_t& exp_type,
      const size_type exp_len,
      const exception::Exception& ex)
  {
    type_t type;
    reader.read_bytes(type);
    check_type(type, exp_type, ex);
    type_ = type;

    size_type size;
    reader.read_bytes(size, tini2p::Endian::Big);
    check_len(tini2p::under_cast(HeaderLen) + size, exp_len, ex);
    size_ = size;
  }

  // read variable length block type and data size, throw if read values don't match expected values
  void read_header(
      tini2p::BytesReader<buffer_t>& reader,
      const type_t& exp_type,
      const size_type min_len,
      const size_type max_len,
      const exception::Exception& ex)
  {
    type_t type;
    reader.read_bytes(type);
    check_type(type, exp_type, ex);
    type_ = type;

    size_type size;
    reader.read_bytes(size, tini2p::Endian::Big);
    check_len(tini2p::under_cast(HeaderLen) + size, min_len, max_len, ex);
    size_ = size;
  }

  // check type matches expected type
  void check_type(const type_t& type, const type_t& exp_type, const exception::Exception& ex)
  {
    if (type != exp_type)
      ex.throw_ex<std::invalid_argument>(
          "invalid message number block type: " + std::to_string(tini2p::under_cast(type))
          + ", expected: " + std::to_string(tini2p::under_cast(exp_type)));
  }

  // check length of a fixed-length block
  void check_len(const std::size_t len, const size_type exp_len, const exception::Exception& ex) const
  {
    const auto& full_len = static_cast<std::size_t>(HeaderLen) + exp_len;

    if (len != full_len)
      ex.throw_ex<std::invalid_argument>(
          "invalid buffer length: " + std::to_string(len) + ", expected: " + std::to_string(full_len));
  }

  // check length of a variable-length block
  void check_len(
      const std::size_t len,
      const size_type min_len,
      const size_type max_len,
      const exception::Exception& ex) const
  {
    if (len < min_len || len > max_len)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid buffer length: " + std::to_string(len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  type_t type_;
  size_type size_;
  buffer_t buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_BLOCK_H_
