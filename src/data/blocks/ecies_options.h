/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_ECIES_OPTIONS_H_
#define SRC_DATA_BLOCKS_ECIES_OPTIONS_H_

#include "src/data/blocks/block.h"

namespace tini2p
{
namespace data
{
class ECIESOptionsBlock : public Block
{
 public:
  enum
  {
    DefaultTagLen = 8,
    MinTagLen = 8,
    MaxTagLen = 16,
    DefaultTagWindow = 32,
    MinTagWindow = 16,  //< Min allowed tag window, adjust if tag len > 8
    MaxTagWindow = 64,  //< Max allowed tag window, adjust if tag len > 8
    DefaultTimeout = 10,  //< Default timeout (in minutes)
    MinTimeout = 10,  //< Min timeout (in minutes), adjust if tag len > 8
    MaxTimeout = 15,  //< Max timeout (in minutes), adjust if tag len > 8
    // TagSizeLen + OutWindow + Timeout + InWindow + Flag + MiscOptions
    ECIESOptionsLen = 6,  //< may adjust depending on misc options format, see spec
  };

  enum struct Flags : std::uint8_t
  {
    NullFlag = 0x00,
    RequestRatchet = 0x01,  //< 0000 0001, see spec
    Unbound = 0x02,         //< 0000 0010, see spec
  };

  enum : std::uint8_t
  {
    ReservedFlagMask = 0xFE,    //< 1111 1110, see spec
  };

  using tag_length_t = std::uint8_t;  //< Session tag length trait alias
  using tag_window_t = std::uint8_t;  //< Outbound tag window trait alias
  using timeout_t = std::uint8_t;  //< Session timeout trait alias
  using flag_t = Flags;  //< Flag trait alias
  using misc_options_t = std::uint8_t;  //< Miscellaneous options trait alias

  /// @brief Default ctor, creates fully initialized ECIESOptionsBlock with default values
  ECIESOptionsBlock()
      : Block(type_t::ECIESOptions, tini2p::under_cast(ECIESOptionsLen)),
        tag_len_(tini2p::under_cast(DefaultTagLen)),
        out_win_(tini2p::under_cast(DefaultTagWindow)),
        timeout_(tini2p::under_cast(DefaultTimeout)),
        in_win_(tini2p::under_cast(DefaultTagWindow)),
        flag_(),
        misc_()
  {
    serialize();
  }

  /// @brief Fully initializing ctor, creates ECIESOptionsBlock with supplied values
  /// @detail Setting tag length is restricted, may be versioned in final spec TBD
  /// @param out_win Outbound tag window lookahead
  /// @param timeout Session idle timeout
  /// @param in_win Inbound tag window lookahead
  /// @param flag Options flag (currently only used to request new ratchet key)
  /// @throws Length error or logic error for any invalid values
  ECIESOptionsBlock(tag_window_t out_win, timeout_t timeout, tag_window_t in_win, flag_t flag)
      : Block(type_t::ECIESOptions, tini2p::under_cast(ECIESOptionsLen)),
        tag_len_(tini2p::under_cast(DefaultTagLen)),
        out_win_(out_win),
        timeout_(timeout),
        in_win_(in_win),
        flag_(flag),
        misc_()
  {
    const exception::Exception ex{"ECIESOptions", __func__};

    check_tag_window(out_win_, ex);
    check_timeout(timeout_, ex);
    check_tag_window(in_win_, ex);
    check_flag(flag_, ex);

    serialize();
  }

  /// @brief Deserializing ctor, creates an ECIESOptionsBlock from secure buffer
  explicit ECIESOptionsBlock(buffer_t buf) : Block(type_t::ECIESOptions)
  {
    const exception::Exception ex{"ECIESOptions", __func__};

    const auto& buf_len = buf.size();

    check_len(buf_len, tini2p::under_cast(ECIESOptionsLen), ex);

    size_ = buf_len;

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Serialize ECIESOptionsBlock to buffer
  void serialize()
  {
    tini2p::BytesWriter<buffer_t> writer(buf_);

    write_header(writer);

    writer.write_bytes(tag_len_);
    writer.write_bytes(out_win_);
    writer.write_bytes(timeout_);
    writer.write_bytes(in_win_);
    writer.write_bytes(flag_);
    writer.write_bytes(misc_);
  }

  /// @brief Deserialize ECIESOptionsBlock from buffer
  void deserialize()
  {
    const exception::Exception ex{"ECIESOptions", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::ECIESOptions, tini2p::under_cast(ECIESOptionsLen), ex);

    tag_length_t tag_len;
    reader.read_bytes(tag_len);
    check_tag_length(tag_len, ex);
    tag_len_ = tag_len;

    tag_window_t out_win;
    reader.read_bytes(out_win);
    check_tag_window(out_win, ex);
    out_win_ = out_win;

    timeout_t time;
    reader.read_bytes(time);
    check_timeout(time, ex);
    timeout_ = time;

    tag_window_t in_win;
    reader.read_bytes(in_win);
    check_tag_window(in_win, ex);
    in_win_ = in_win;

    flag_t flag;
    reader.read_bytes(flag);
    check_flag(flag, ex);
    flag_ = flag;

    misc_options_t misc;
    reader.read_bytes(misc);
    // TODO(tini2p): write misc check when format finalized
    misc_ = misc;
  }

  /// @brief Get a const reference to the tag length
  const tag_length_t& tag_len() const noexcept
  {
    return tag_len_;
  }

  /// @brief Set the tag length
  // TODO(tini2p): remove if tag length will be versioned
  void tag_len(tag_length_t tag_len)
  {
    check_tag_length(tag_len, {"ECIESOptions", __func__});

    tag_len_ = tag_len;
  }

  /// @brief Get a const reference to the outbound tag window
  const tag_window_t& out_tag_window() const noexcept
  {
    return out_win_;
  }

  /// @brief Set the outbound tag window
  void out_tag_window(tag_window_t tag_win)
  {
    check_tag_window(tag_win, {"ECIESOptions", __func__});

    out_win_ = tag_win;
  }

  /// @brief Get a const reference to the session idle timeout
  const timeout_t& timeout() const noexcept
  {
    return timeout_;
  }

  /// @brief Set the session idle timeout
  void timeout(timeout_t time)
  {
    check_timeout(time, {"ECIESOptions", __func__});

    timeout_ = time;
  }

  /// @brief Get a const reference to the inbound tag window
  const tag_window_t& in_tag_window() const noexcept
  {
    return in_win_;
  }

  /// @brief Set the inbound tag window
  void in_tag_window(tag_window_t tag_win)
  {
    check_tag_window(tag_win, {"ECIESOptions", __func__});

    in_win_ = tag_win;
  }

  /// @brief Get a const reference to the options flag
  const flag_t& flag() const noexcept
  {
    return flag_;
  }

  /// @brief Set the options flag
  void flag(flag_t flag)
  {
    check_flag(flag, {"ECIESOptions", __func__});

    flag_ = flag;
  }

  /// @brief Get whether the options are for an unbound session
  std::uint8_t is_unbound() const
  {
    return tini2p::under_cast(flag_) & tini2p::under_cast(flag_t::Unbound);
  }

  /// @brief Get a const reference to the miscellaneous options
  const misc_options_t& misc() const noexcept
  {
    return misc_;
  }

  /// @brief Set the miscellaneous options
  void misc(misc_options_t misc)
  {
    // TODO(tini2p): check misc options when format finalized

    misc_ = misc;
  }

  /// @brief Equality comparison with another ECIESOptionsBlock
  std::uint8_t operator==(const ECIESOptionsBlock& oth) const
  {  // attempt constant-time comparison
    const auto& tag_eq = static_cast<std::uint8_t>(tag_len_ == oth.tag_len_);
    const auto& owin_eq = static_cast<std::uint8_t>(out_win_ == oth.out_win_);
    const auto& time_eq = static_cast<std::uint8_t>(timeout_ == oth.timeout_);
    const auto& iwin_eq = static_cast<std::uint8_t>(in_win_ == oth.in_win_);
    const auto& flag_eq = static_cast<std::uint8_t>(flag_ == oth.flag_);
    const auto& misc_eq = static_cast<std::uint8_t>(misc_ == oth.misc_);

    return (tag_eq * owin_eq * time_eq * iwin_eq * flag_eq * misc_eq);
  }

  /// @brief For templated use with I2NP blocks
  std::uint8_t msg_type() const
  {
    const exception::Exception ex{"ECIESOptionsBlock", __func__};

    ex.throw_ex<std::logic_error>("invalid call to non-I2NP block");

    return 0;
  }

 private:
  void check_tag_length(const tag_length_t len, const exception::Exception& ex)
  {
    const auto& min_len = tini2p::under_cast(MinTagLen);
    const auto& max_len = tini2p::under_cast(MaxTagLen);

    // TODO(tini2p): adjust to fixed-length check, if tag length becomes versioned
    if (len < min_len || len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid tag length: " + std::to_string(len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  void check_tag_window(const tag_window_t win, const exception::Exception& ex)
  {
    const auto& min_win = tini2p::under_cast(MinTagWindow);
    const auto& max_win = tini2p::under_cast(MaxTagWindow);

    if (win < min_win || win > max_win)
      {
        ex.throw_ex<std::logic_error>(
            "invalid tag window: " + std::to_string(win) + ", min: " + std::to_string(min_win)
            + ", max: " + std::to_string(max_win));
      }
  }

  void check_timeout(const timeout_t time, const exception::Exception& ex)
  {
    const auto& min_time = tini2p::under_cast(MinTimeout);
    const auto& max_time = tini2p::under_cast(MaxTimeout);

    if (time < min_time || time > max_time)
      {
        ex.throw_ex<std::logic_error>(
            "invalid timeout: " + std::to_string(time) + ", min: " + std::to_string(min_time)
            + ", max: " + std::to_string(max_time));
      }
  }

  void check_flag(const flag_t flag, const exception::Exception& ex)
  {
    const auto& f = tini2p::under_cast(flag);
    const auto& mask = tini2p::under_cast(ReservedFlagMask);

    if (f & mask)
      ex.throw_ex<std::logic_error>("invalid flag: " + std::to_string(f));
  }

  tag_length_t tag_len_;
  tag_window_t out_win_;
  timeout_t timeout_;
  tag_window_t in_win_;
  flag_t flag_;
  misc_options_t misc_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_ECIES_OPTIONS_H_
