/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_NTCP2_OPTIONS_H_
#define SRC_DATA_BLOCKS_NTCP2_OPTIONS_H_

#include "src/exception/exception.h"
#include "src/time.h"

#include "src/data/blocks/block.h"

namespace tini2p
{
namespace data
{
/// @brief NTCP2 Options block
class NTCP2OptionsBlock : public Block
{
 public:
  enum : std::uint8_t
  {
    NTCP2OptionsBlockLen = 12,
    TMinOffset = 3,
    TMaxOffset,
    RMinOffset,
    RMaxOffset,
    TDummyOffset,
    RDummyOffset = 9,
    TDelayOffset = 11,
    RDelayOffset = 13
  };

  using tmin_t = float;  //< TMin trait alias
  using tmax_t = float;  //< TMax trait alias
  using rmin_t = float;  //< RMin trait alias
  using rmax_t = float;  //< RMax trait alias
  using tdummy_t = std::uint16_t;  //< TDummy trait alias
  using tdelay_t = std::uint16_t;  //< TDelay trait alias
  using rdummy_t = std::uint16_t;  //< RDummy trait alias
  using rdelay_t = std::uint16_t;  //< RDelay trait alias

  const float CastRatio{16.0}, MinPaddingRatio{0}, MaxPaddingRatio{15.9375};

  NTCP2OptionsBlock()
      : Block(type_t::Options, NTCP2OptionsBlockLen),
        tmin_(0),
        tmax_(0),
        rmin_(0),
        rmax_(0),
        tdummy_(0),
        tdelay_(0),
        rdummy_(0),
        rdelay_(0)
  {
    serialize();
  }

  /// @brief Create an OptionsBlock from an iterator range
  NTCP2OptionsBlock(buffer_t buf) : Block(type_t::Options)
  {
    const exception::Exception ex{"NTCP2OptionsBlock", __func__};

    check_len(buf.size(), tini2p::under_cast(NTCP2OptionsBlockLen), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  NTCP2OptionsBlock(const OptionsBlock& oth)
      : Block(type_t::Options, oth.size_),
        tmin_(oth.tmin_),
        tmax_(oth.tmax_),
        rmin_(oth.rmin_),
        rmax_(oth.rmax_),
        tdummy_(oth.tdummy_),
        tdelay_(oth.tdelay_),
        rdummy_(oth.rdummy_),
        rdelay_(oth.rdelay_)
  {
    serialize();
  }

  void operator=(const NTCP2OptionsBlock& oth)
  {
    buf_ = oth.buf_;
    size_ = oth.size_;
    tmin_ = oth.tmin_;
    tmax_ = oth.tmax_;
    rmin_ = oth.rmin_;
    rmax_ = oth.rmax_;
    tdummy_ = oth.tdummy_;
    tdelay_ = oth.tdelay_;
    rdummy_ = oth.rdummy_;
    rdelay_ = oth.rdelay_;
  }

  /// @brief Serialize options block to buffer
  void serialize()
  {
    const exception::Exception ex{"NTCP2OptionsBlock", __func__};

    check_ratios(ex);

    tini2p::BytesWriter<buffer_t> writer(buf_);

    write_header(writer);

    // cast from float, see spec
    writer.write_bytes<std::uint8_t>(tmin_ * CastRatio);
    writer.write_bytes<std::uint8_t>(tmax_ * CastRatio);
    writer.write_bytes<std::uint8_t>(rmin_ * CastRatio);
    writer.write_bytes<std::uint8_t>(rmax_ * CastRatio);

    writer.write_bytes(tdummy_, tini2p::Endian::Big);
    writer.write_bytes(rdummy_, tini2p::Endian::Big);
    writer.write_bytes(tdelay_, tini2p::Endian::Big);
    writer.write_bytes(rdelay_, tini2p::Endian::Big);
  }

  /// @brief Deserialize options block to buffer
  void deserialize()
  {
    const exception::Exception ex{"OptionsBlock", __func__};

    if (buf_.size() != HeaderLen + NTCP2OptionsLen)
      ex.throw_ex<std::length_error>("invalid buffer length.");

    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::NTCP2Options, tini2p::under_cast(NTCPOptionsBlockLen), ex);

    // cast to float (see spec)
    std::uint8_t tmp_tmin;
    reader.read_bytes(tmp_tmin);
    tmin_ = tmp_tmin / CastRatio;

    std::uint8_t tmp_tmax;
    reader.read_bytes(tmp_tmax);
    tmax_ = tmp_tmax / CastRatio;

    std::uint8_t tmp_rmin;
    reader.read_bytes(tmp_rmin);
    rmin_ = tmp_rmin / CastRatio;

    std::uint8_t tmp_rmax;
    reader.read_bytes(tmp_rmax);
    rmax_ = tmp_rmax / CastRatio;

    reader.read_bytes(tdummy, tini2p::Endian::Big);
    reader.read_bytes(rdummy, tini2p::Endian::Big);
    reader.read_bytes(tdelay, tini2p::Endian::Big);
    reader.read_bytes(rdelay, tini2p::Endian::Big);

    check_ratios(ex);
  }

  /// @brief Get a const reference to the min transmit padding ratio
  const tmin_t& tmin() const noexcept
  {
    return tmin_;
  }

  /// @brief Set min transmit padding ratio
  void tmin(const tmin_t tmin)
  {
    const exception::Exception ex{"NTCP2OptionsBlock", __func__};

    if (tmin > tmax_)
      ex.throw_ex<std::logic_error>("min tx padding larger than max tx padding.");

    check_ratio(tmin, ex);

    tmin_ = tmin;
  }

  /// @brief Get a const reference to the max transmit padding ratio
  const tmax_t& tmax() const noexcept
  {
    return tmax_;
  }

  /// @brief Set max transmit padding ratio
  void tmax(const tmax_t tmax)
  {
    const exception::Exception ex{"NTCP2OptionsBlock", __func__};

    if (tmax < tmin_)
      ex.throw_ex<std::logic_error>("max tx padding smaller than min tx padding.");

    check_ratio(tmax, ex);

    tmax_ = tmax;
  }

  /// @brief Get a const reference to the min receive padding ratio
  const rmin_t& rmin() const noexcept
  {
    return rmin_;
  }

  /// @brief Set min receive padding ratio
  void rmin(const rmin_t rmin)
  {
    const exception::Exception ex{"NTCP2OptionsBlock", __func__};

    if (rmin > rmax_)
      ex.throw_ex<std::logic_error>("min rx padding larger than max rx padding.");

    check_ratio(rmin, ex);

    rmin_ = rmin;
  }

  /// @brief Get a const reference to the max receive padding ratio
  const rmax_t& rmax() const noexcept
  {
    return rmax_;
  }

  /// @brief Set max receive padding ratio
  void rmax(const rmax_t rmax)
  {
    const exception::Exception ex{"NTCP2OptionsBlock", __func__};

    if (rmax < rmin_)
      ex.throw_ex<std::logic_error>("max rx padding smaller than min rx padding.");

    check_ratio(rmax, ex);

    rmax_ = rmax;
  }

  /// @brief Get a const reference to the max count of transmit dummy data
  const tdummy_t& tdummy() const noexcept
  {
    return tdummy_;
  }

  /// @brief Set the max count of transmit dummy data
  void tdummy(const tdummy_t tdummy) noexcept
  {
    tdummy_ = tdummy;
  }

  /// @brief Get a const reference to the max count of intra-message transmit delay
  const tdelay_t& tdelay() const noexcept
  {
    return tdelay_;
  }

  /// @brief Set the max count of intra-message transmit delay
  void tdelay(const tdelay_t tdelay) noexcept
  {
    tdelay_ = tdelay;
  }

  /// @brief Get a const reference to the max count of receive dummy data
  const rdummy_t& rdummy() const noexcept
  {
    return rdummy_;
  }

  /// @brief Set the max count of receive dummy data
  void rdummy(const rdummy_t rdummy) noexcept
  {
    rdummy_ = rdummy;
  }

  /// @brief Get a const reference to the max count of intra-message receive delay
  const rdelay_t& rdelay() const noexcept
  {
    return rdelay_;
  }

  /// @brief Set the max count of intra-message receive delay
  void rdelay(const rdelay_t rdelay) noexcept
  {
    rdelay_ = rdelay;
  }

  /// @brief Get the cast ratio for converting over-the-wire ratio to float
  constexpr float cast_ratio() const noexcept
  {
    return CastRatio;
  }

  /// @brief Get the minimum padding ratio
  constexpr float min_padding_ratio() const noexcept
  {
    return MinPaddingRatio;
  }

  /// @brief Get the maximum padding ratio
  constexpr float max_padding_ratio() const noexcept
  {
    return MaxPaddingRatio;
  }

 private:
  void check_ratios(const exception::Exception& ex)
  {
    if (tmin_ > tmax_)
      ex.throw_ex<std::logic_error>("min tx padding larger than max tx padding.");

    if (rmin_ > rmax_)
      ex.throw_ex<std::logic_error>("min rx padding larger than max rx padding.");

    check_ratio(tmin_, ex);
    check_ratio(tmax_, ex);
    check_ratio(rmin_, ex);
    check_ratio(rmax_, ex);
  }

  void check_ratio(const float ratio, const exception::Exception& ex)
  {
    if (ratio < MinPaddingRatio || ratio > MaxPaddingRatio)
      ex.throw_ex<std::length_error>("invalid padding ratio.");
  }

  tmin_t tmin_;
  tmax_t tmax_;
  rmin_t rmin_;
  rmax_t rmax_;
  tdummy_t tdummy_;
  tdelay_t tdelay_;
  rdummy_t rdummy_;
  rdelay_t rdelay_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_NTCP2_OPTIONS_H_
