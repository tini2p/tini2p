/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_BLOCKS_H_
#define SRC_DATA_BLOCKS_BLOCKS_H_

#include <variant>

#include "src/bytes.h"

#include "src/data/blocks/date_time.h"
#include "src/data/blocks/i2np.h"
#include "src/data/blocks/info.h"
#include "src/data/blocks/ntcp2_options.h"
#include "src/data/blocks/padding.h"
#include "src/data/blocks/termination.h"

namespace tini2p
{
namespace data
{
class Blocks
{
 public:
  /// @alias confirm_variant_v
  /// @brief SessionConfirmed block variant trait alias
  using confirm_variant_v = std::variant<InfoBlock, NTCP2OptionsBlock, PaddingBlock>;

  /// @alias data_variant_v
  /// @brief DataPhase block variant trait alias
  using data_variant_v =
      std::variant<DateTimeBlock, I2NPBlock, InfoBlock, NTCP2OptionsBlock, PaddingBlock, TerminationBlock>;

  using confirm_blocks_t = std::vector<confirm_variant_v>;  //< SessionConfirmed blocks container trait alias
  using data_blocks_t = std::vector<data_variant_v>;  //< DataPhase blocks container trait alias

  /// @brief Read and deserialize a block from a buffer reader
  /// @tparam TBlockVar Block variant type
  /// @param reader Buffer reader
  /// @param block Block variant for deserialization result
  /// @param ex Exception handler
  template <
      class TBlockVar,
      typename = std::enable_if_t<
          std::is_same<TBlockVar, confirm_variant_v>::value || std::is_same<TBlockVar, data_variant_v>::value>>
  static void ReadToBlock(BytesReader<Block::buffer_t>& reader, TBlockVar& block, const exception::Exception& ex)
  {
    Block::Type type;
    Block::size_type size;

    reader.read_bytes(type);
    reader.read_bytes(size, tini2p::Endian::Big);
    reader.skip_back(Block::HeaderLen);

    TypeToBlock(type, block, ex);

    const auto full_len = Block::HeaderLen + size;
    std::visit(
        [&reader, full_len, size](auto& b) {
          auto& buf = b.buffer();
          buf.resize(full_len);
          reader.read_data(buf);

          if (size)
            b.deserialize();
        },
        block);
  }

  static void TypeToBlock(const Block::Type type, confirm_variant_v& block, const exception::Exception& ex)
  {
    if (type == Block::Type::Info)
      block.emplace<data::InfoBlock>();
    else if (type == Block::Type::NTCP2Options)
      block.emplace<data::NTCP2OptionsBlock>();
    else if (type == Block::Type::Padding)
      block.emplace<data::PaddingBlock>();
    else
      ex.throw_ex<std::runtime_error>("invalid block type.");
  }

  static void TypeToBlock(const Block::Type type, data_variant_v& block, const exception::Exception& ex)
  {
    if (type == Block::Type::DateTime)
      block.emplace<data::DateTimeBlock>();
    else if (type == Block::Type::I2NP)
      block.emplace<data::I2NPBlock>();
    else if (type == Block::Type::Info)
      block.emplace<data::InfoBlock>();
    else if (type == Block::Type::NTCP2Options)
      block.emplace<data::NTCP2OptionsBlock>();
    else if (type == Block::Type::Padding)
      block.emplace<data::PaddingBlock>();
    else if (type == Block::Type::Termination)
      block.emplace<data::TerminationBlock>();
    else
      ex.throw_ex<std::runtime_error>("invalid block type.");
  }

  /// @struct TotalSize
  /// @brief Get total size of block variant container
  template <
      class TBlocks,
      typename = std::enable_if_t<
          std::is_same<TBlocks, confirm_blocks_t>::value || std::is_same<TBlocks, data_blocks_t>::value>>
  static constexpr std::size_t TotalSize(const TBlocks& blocks)
  {
    std::size_t size(0);
    for (const auto& block : blocks)
      size += std::visit([](const auto& b){ return b.size(); }, block);

    return size;
  }

  /// @brief Check for correct block ordering
  ///
  /// @detail
  ///
  ///   SessionConfirmed ordering must be:
  ///
  ///     - InfoBlock (required)
  ///     - OptionsBlock (optional)
  ///     - PaddingBlock (optional, must be final block if present)
  ///
  ///   Multiple padding blocks are disallowed.
  ///
  ///   See I2P [spec](https://geti2p.net/spec/ntcp2#block-ordering-rules) for details.
  ///
  /// @param blocks Block variant container
  /// @param ex Exception handler
  static void CheckBlockOrder(const confirm_blocks_t& blocks, const exception::Exception& ex)
  {
    std::uint8_t block_pos(0);
    constexpr const std::uint8_t first = 0, second = 1, third = 2, max = 4;

    for (const auto& block : blocks)
      {
        const auto& type = std::visit([](const auto& b){ return b.type(); }, block);

        if (block_pos == first && type != Block::Type::Info)
          ex.throw_ex<std::logic_error>("RouterInfo must be the first block.");

        if (block_pos == second && type != Block::Type::NTCP2Options && type != Block::Type::Padding)
          ex.throw_ex<std::logic_error>("second block must be Options or Padding block.");

        if (block_pos == third && type != Block::Type::Padding)
          ex.throw_ex<std::logic_error>("last block must be Padding block.");

        if (block_pos == max)
          ex.throw_ex<std::logic_error>("max block count exceeded.");

        if (type == Block::Type::Info || type == Block::Type::NTCP2Options)
          ++block_pos;
        else if (type == Block::Type::Padding)
          block_pos = max;
      }
  }

  /// @brief Check for correct block ordering
  ///
  /// @detail
  ///
  ///   DataPhase final block(s) must be:
  ///  
  ///     - PaddingBlock
  ///     - TerminationBlock -> PaddingBlock
  ///
  ///   Termination block must either be last, or followed only by a single padding block.
  ///
  ///   Multiple padding blocks are disallowed.
  ///
  ///   See I2P [spec](https://geti2p.net/spec/ntcp2#block-ordering-rules) for details.
  ///
  /// @param blocks Block variant container
  /// @param ex Exception handler
  static void CheckBlockOrder(const data_blocks_t& blocks, const exception::Exception& ex)
  {
    bool last_block(false), term_block(false);
    for (const auto& block : blocks)
      {
        const auto& type = std::visit([](const auto& b){ return b.type(); }, block);
        if (last_block)
          ex.throw_ex<std::logic_error>("padding must be the last block.");

        if (term_block && type != Block::Type::Padding)
          ex.throw_ex<std::logic_error>("only padding block can follow termination block.");

        if (type == Block::Type::Termination)
          term_block = true;
        else if (type == Block::Type::Padding)
          last_block = true;
      }
  }

  /// @brief Add a block variant to a container
  /// @tparam TBlocks Block variant container type
  /// @param blocks Block variant container
  /// @param block Block to add
  /// @param ex Exception handler
  template <
      class TBlocks,
      typename = std::enable_if_t<
          std::is_same<TBlocks, confirm_blocks_t>::value || std::is_same<TBlocks, data_blocks_t>::value>>
  static void AddBlock(TBlocks& blocks, typename TBlocks::value_type block, const exception::Exception& ex)
  {
    blocks.emplace_back(std::forward<decltype(block)>(block));
    CheckBlockOrder(blocks, ex);
  }
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_BLOCKS_H_
