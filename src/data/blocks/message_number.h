/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_MESSAGE_NUMBER_H_
#define SRC_DATA_BLOCKS_MESSAGE_NUMBER_H_

#include "src/data/blocks/block.h"

namespace tini2p
{
namespace data
{
class MessageNumberBlock : public Block
{
 public:
  enum
  {
    MsgKeyIDLen = 2,
    MsgPNLen = 2,
    MsgNLen = 2,
    MsgNumBlockLen = MsgKeyIDLen + MsgPNLen + MsgNLen,
  };

  enum : std::uint16_t
  {
    NewSessionKeyID = 65535,  //< 0xffff, see spec
  };

  using key_id_t = std::uint16_t;  //< Key ID trait alias
  using pn_t = std::uint16_t;  //< Previous message key number trait alias
  using n_t = std::uint16_t;  //< Message number trait alias

  /// @brief Default ctor, creates an uninitialized MessageNumberBlock
  /// @detail Key ID, previous message key number, and current message number set to zero
  MessageNumberBlock() : Block(type_t::MessageNumber, MsgNumBlockLen), key_id_(0), pn_(0), n_(0)
  {
    serialize();
  }

  /// @brief Fully-initializing ctor, creates a MessageNumberBlock with all needed parts
  /// @param key_id Key ID of the current ratchet key being used
  /// @param pn Number of keys (one key per-message) sent in the previous chain
  /// @param n Number of messages in the current chain
  MessageNumberBlock(key_id_t key_id, pn_t pn, n_t n)
      : Block(type_t::MessageNumber, MsgNumBlockLen),
        key_id_(std::forward<key_id_t>(key_id)),
        pn_(std::forward<pn_t>(pn)),
        n_(std::forward<n_t>(n))
  {
    serialize();
  }

  /// @brief Deserializing buffer, creates a MessageNumberBlock from a secure buffer
  explicit MessageNumberBlock(buffer_t buf) : Block(type_t::MessageNumber)
  {
    const exception::Exception ex{"MessageNumberBlock", __func__};

    check_len(buf.size(), tini2p::under_cast(MsgNumBlockLen), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Serialize MessageNumberBlock to buffer
  void serialize()
  {
    tini2p::BytesWriter<buffer_t> writer(buf_);

    write_header(writer);

    writer.write_bytes(key_id_, tini2p::Endian::Big);
    writer.write_bytes(pn_, tini2p::Endian::Big);
    writer.write_bytes(n_, tini2p::Endian::Big);
  }

  /// @brief Serialize MessageNumberBlock from buffer
  void deserialize()
  {
    const exception::Exception ex{"MessageNumberBlock", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::MessageNumber, tini2p::under_cast(MsgNumBlockLen), ex);

    reader.read_bytes(key_id_, tini2p::Endian::Big);
    reader.read_bytes(pn_, tini2p::Endian::Big);
    reader.read_bytes(n_, tini2p::Endian::Big);
  }

  /// @brief Get a const reference to the key ID
  const key_id_t& key_id() const noexcept
  {
    return key_id_;
  }

  /// @brief Set the key ID
  void key_id(key_id_t key_id) noexcept
  {
    key_id_ = std::forward<key_id_t>(key_id);
  }

  /// @brief Get a const reference to the previous message key number
  const pn_t& pn() const noexcept
  {
    return pn_;
  }

  /// @brief Set the previous message key number
  void pn(pn_t pn) noexcept
  {
    pn_ = std::forward<pn_t>(pn);
  }

  /// @brief Get a const reference to the current message number
  const n_t& n() const noexcept
  {
    return n_;
  }

  /// @brief Set the current message number
  void n(n_t n) noexcept
  {
    n_ = std::forward<n_t>(n);
  }

  /// @brief Equality comparison with another MessageNumberBlock
  std::uint8_t operator==(const MessageNumberBlock& oth) const
  {
    const auto& key_eq = static_cast<std::uint8_t>(key_id_ == oth.key_id_);
    const auto& pn_eq = static_cast<std::uint8_t>(pn_ == oth.pn_);
    const auto& n_eq = static_cast<std::uint8_t>(n_ == oth.n_);

    return (key_eq * pn_eq * n_eq);
  }

  /// @brief For templated use with I2NP blocks
  std::uint8_t msg_type() const
  {
    const exception::Exception ex{"MessageNumberBlock", __func__};

    ex.throw_ex<std::logic_error>("invalid call to non-I2NP block");

    return 0;
  }

 private:
  key_id_t key_id_;
  pn_t pn_;
  n_t n_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_MESSAGE_NUMBER_H_
