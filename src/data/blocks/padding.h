/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_PADDING_H_
#define SRC_DATA_BLOCKS_PADDING_H_

#include "src/exception/exception.h"

#include "src/crypto/rand.h"

#include "src/data/blocks/block.h"

namespace tini2p
{
namespace data
{
/// @brief Options NTCP2 block
class PaddingBlock : public Block
{
 public:
  using padding_t = crypto::SecBytes;  //< Padding trait alias

  enum
  {
    MinPaddingLen = 0,
    MaxPaddingLen = MaxLen - HeaderLen,
  };

  PaddingBlock() : Block(type_t::Padding), padding_{}
  {
    serialize();
  }

  /// @brief Create a PaddingBlock from a length
  /// @param size Length of padding in the block
  explicit PaddingBlock(const size_type size) : Block(type_t::Padding, size)
  {
    padding_.resize(size);
    crypto::RandBytes(padding_);

    serialize();
  }

  /// @brief Create an PaddingBlock from a secure buffer
  explicit PaddingBlock(buffer_t buf) : Block(type_t::Padding)
  {
    const exception::Exception ex{"PaddingBlock", __func__};

    check_len(buf.size(), tini2p::under_cast(MinPaddingLen), tini2p::under_cast(MaxPaddingLen), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Serialize options block to buffer
  void serialize()
  {
    const exception::Exception ex{"PaddingBlock", __func__};

    size_ = padding_.size();

    check_len(
        tini2p::under_cast(HeaderLen) + size_,
        tini2p::under_cast(MinPaddingLen),
        tini2p::under_cast(MaxPaddingLen),
        ex);

    buf_.resize(size());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    write_header(writer);

    if (size_)
      writer.write_data(padding_);
  }

  /// @brief Deserialize options block to buffer
  void deserialize()
  {
    const exception::Exception ex{"PaddingBlock", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::Padding, tini2p::under_cast(MinPaddingLen), tini2p::under_cast(MaxPaddingLen), ex);

    if (size_)
      {
        padding_.resize(size_);
        padding_.shrink_to_fit();
        reader.read_data(padding_);
      }

    // reclaim unused space
    buf_.resize(reader.count());
    buf_.shrink_to_fit();
  }

  /// @brief Get a const refernce to the padding
  const padding_t& padding() const noexcept
  {
    return padding_;
  }

  /// @brief Get a non-const refernce to the padding
  padding_t& padding() noexcept
  {
    return padding_;
  }

  /// @brief Set random padding for a given pad length
  void padding(const size_type pad_len)
  {
    const exception::Exception ex{"PaddingBlock", __func__};

    check_len(
        tini2p::under_cast(HeaderLen) + pad_len,
        tini2p::under_cast(MinPaddingLen),
        tini2p::under_cast(MaxPaddingLen),
        ex);

    padding_ = crypto::RandBuffer(pad_len);
  }

  /// @brief Equality comparison with another PaddingBlock
  std::uint8_t operator==(const PaddingBlock& oth) const
  {
    const auto& type_eq = static_cast<std::uint8_t>(type_ == oth.type_);
    const auto& size_eq = static_cast<std::uint8_t>(size_ == oth.size_);
    const auto& pad_eq = static_cast<std::uint8_t>(padding_ == oth.padding_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    return (type_eq * size_eq * pad_eq * buf_eq);
  }

  /// @brief For templated use with I2NP blocks
  std::uint8_t msg_type() const
  {
    const exception::Exception ex{"PaddingBlock", __func__};

    ex.throw_ex<std::logic_error>("invalid call to non-I2NP block");

    return 0;
  }

 private:
  padding_t padding_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_PADDING_H_
