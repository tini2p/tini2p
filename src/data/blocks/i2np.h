/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_I2NP_H_
#define SRC_DATA_BLOCKS_I2NP_H_

#include "src/exception/exception.h"

#include "src/crypto/rand.h"

#include "src/ntcp2/meta.h"
#include "src/time.h"

#include "src/data/blocks/block.h"

#include "src/data/i2np/i2np_header.h"

namespace tini2p
{
namespace data
{
class I2NPMessage;

class I2NPBlock : public Block
{
 public:
  enum : std::uint16_t
  {
    MsgHeaderLen = 9,
    MsgTypeLen = 1,
    MinMsgLen = MsgHeaderLen,
    MaxMsgLen = 62708,  //< max I2NP length, see I2NP spec
    I2NPTypeOffset = 3,
    MessageIDOffset = 4,
    ExpirationOffset = 8,
    MessageOffset = 12,
    DefaultI2NPExp = 120,  //< in seconds
    MinI2NPBlockLen = HeaderLen + MinMsgLen,
    MaxI2NPBlockLen = HeaderLen + MaxMsgLen,
  };

  enum : std::uint32_t
  {
    MinMsgID = 1,
    MaxMsgID = 0xFFFFFFFF,  // uint32_max
  };

  using msg_type_t = data::I2NPHeader::Type;  //< Message type trait alias
  using message_id_t = std::uint32_t;  //< Message ID trait alias
  using expiration_t = std::uint32_t;  //< Expiration trait alias
  using garlic_length_t = std::uint32_t;  //< Garlic session message length trait alias
  using msg_buffer_t = crypto::SecBytes;  //< Message buffer trait alias

  /// @brief Default ctor, creates a fully initialized I2NPBlock
  /// @detail Sets I2NP message type to Data, a random message ID, and default expiration
  I2NPBlock()
      : Block(type_t::I2NP, MinMsgLen),
        i2np_header_(data::I2NPHeader::Type::Data, {}, data::I2NPHeader::Mode::NTCP2),
        msg_buf_()
  {
    serialize();
  }

  /// @brief Create an I2NPBlock with a given I2NP message type
  /// @detail Sets a random message ID, and default expiration
  I2NPBlock(msg_type_t type)
      : Block(type_t::I2NP, MinMsgLen),
        i2np_header_(std::forward<data::I2NPHeader::Type>(type), {}, data::I2NPHeader::Mode::NTCP2),
        msg_buf_()
  {
    serialize();
  }

  /// @brief Fully-initializing ctor, create an I2NPBlock from message parameters
  /// @param msg_type I2NP message type
  /// @param msg_id I2NP message ID
  /// @param msg_buf I2NP message buffer
  template <class TBuffer>
  I2NPBlock(msg_type_t msg_type, message_id_t msg_id, TBuffer msg_buf)
      : Block(type_t::I2NP),
        i2np_header_(
            std::forward<msg_type_t>(msg_type),
            std::forward<message_id_t>(msg_id),
            data::I2NPHeader::Mode::NTCP2)
  {
    const exception::Exception ex{"I2NPBlock", __func__};

    const auto& msg_len = tini2p::under_cast(MsgHeaderLen) + msg_buf.size();
    check_len(msg_len, tini2p::under_cast(MinMsgLen), tini2p::under_cast(MaxMsgLen), ex);

    msg_buf_ = msg_buf;

    size_ = msg_len;

    serialize();
  }

  /// @brief Fully-initializing ctor, create an I2NPBlock from message parameters
  /// @param msg_type I2NP message type
  /// @param msg_id I2NP message ID
  /// @param msg_buf I2NP message buffer
  template <class TBuffer>
  I2NPBlock(data::I2NPHeader header, TBuffer msg_buf) : Block(type_t::I2NP)
  {
    const exception::Exception ex{"I2NPBlock", __func__};

    check_header(header, ex);
    i2np_header_ = std::forward<data::I2NPHeader>(header);

    i2np_header_.mode(data::I2NPHeader::Mode::NTCP2);

    const auto& msg_len = tini2p::under_cast(MsgHeaderLen) + msg_buf.size();
    check_len(msg_len, tini2p::under_cast(MinMsgLen), tini2p::under_cast(MaxMsgLen), ex);

    msg_buf_ = msg_buf;

    serialize();
  }

  /// @brief Deserializing ctor, creates an I2NPBlock from a secure buffer
  I2NPBlock(buffer_t buf) : Block(type_t::I2NP, buf.size())
  {
    const exception::Exception ex{"I2NPBlock", __func__};

    const auto& buf_len = buf.size();

    check_len(buf_len, tini2p::under_cast(MinI2NPBlockLen), tini2p::under_cast(MaxI2NPBlockLen), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Serialize I2NP block to buffer
  void serialize()
  {
    const exception::Exception ex{"I2NPBlock", __func__};

    size_ = tini2p::under_cast(data::I2NPHeader::NTCP2HeaderLen) + msg_buf_.size();

    buf_.resize(size());

    tini2p::BytesWriter<buffer_t> writer(buf_);

    write_header(writer);

    i2np_header_.serialize();
    writer.write_data(i2np_header_.buffer());

    const std::uint32_t msg_len(msg_buf_.size());
    if (msg_len)
      {
        check_message_buffer(msg_buf_, ex);
        writer.write_data(msg_buf_);
      }

    // reclaim unused space
    buf_.resize(writer.count());
    buf_.shrink_to_fit();
  }

  /// @brief Deserialize I2NP block from buffer
  void deserialize()
  {
    const exception::Exception ex{"I2NPBlock", __func__};
  
    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::I2NP, tini2p::under_cast(MinI2NPBlockLen), tini2p::under_cast(MaxI2NPBlockLen), ex);

    crypto::SecBytes head_buf;
    head_buf.resize(data::I2NPHeader::NTCP2HeaderLen);

    reader.read_data(head_buf);
    data::I2NPHeader i2np_header(std::move(head_buf), data::I2NPHeader::Mode::NTCP2);

    const auto msg_len = reader.gcount();

    crypto::SecBytes msg_buf;
    if (msg_len)
      {
        msg_buf.resize(msg_len);
        reader.read_data(msg_buf);
      }

    i2np_header_ = std::move(i2np_header);
    msg_buf_ = std::move(msg_buf);
  }

  /// @brief Create an I2NP block from a secure buffer 
  I2NPBlock& from_buffer(buffer_t buf)
  {
    const exception::Exception ex{"I2NPBlock", __func__};

    check_len(buf.size(), tini2p::under_cast(MinI2NPBlockLen), tini2p::under_cast(MaxI2NPBlockLen), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();

    return *this;
  }

  /// @brief Get a const reference to the message type
  const msg_type_t& msg_type() const noexcept
  {
    return i2np_header_.type();
  }

  /// @brief Set the I2NP message type
  /// @param type I2NP message type
  /// @throw Exception on invalid I2NP message type
  I2NPBlock& msg_type(msg_type_t type)
  {
    i2np_header_.type(std::forward<data::I2NPHeader::Type>(type));

    return *this;
  }

  /// @brief Get const reference to the I2NP message ID
  const message_id_t& message_id() const noexcept
  {
    return i2np_header_.message_id();
  }

  /// @brief Get const reference to the I2NP expiration
  std::uint32_t expiration() const
  {
    return i2np_header_.expiration();
  }

  /// @brief Get a const reference to I2NP message data
  const msg_buffer_t& msg_data() const noexcept
  {
    return msg_buf_;
  }

  /// @brief Get a non-const reference to I2NP message data
  msg_buffer_t& msg_data() noexcept
  {
    return msg_buf_;
  }

  /// @brief Set the message data from a secure buffer
  I2NPBlock& msg_data(crypto::SecBytes msg_buf)
  {
    const exception::Exception ex{"I2NPBlock", __func__};

    check_message_buffer(msg_buf, ex);

    msg_buf_ = std::forward<crypto::SecBytes>(msg_buf);
    size_ = tini2p::under_cast(MsgHeaderLen) + msg_buf_.size();

    return *this;
  }

  /// @brief Set the message data from a fixed secure buffer
  template <std::size_t N>
  I2NPBlock& msg_data(const crypto::FixedSecBytes<N>& msg_buf)
  {
    const exception::Exception ex{"I2NPBlock", __func__};

    check_len(N, tini2p::under_cast(MinMsgLen), tini2p::under_cast(MaxMsgLen), ex);

    size_ = data::I2NPHeader::NTCP2HeaderLen + N;

    msg_buf_ = msg_buf;

    return *this;
  }

  void resize_message(const std::size_t size)
  {
    const exception::Exception ex{"I2NPBlock", __func__};

    if (size > MaxMsgLen)
      ex.throw_ex<std::invalid_argument>("invalid message size");

    msg_buf_.resize(size);
    size_ = MsgHeaderLen + size;
  }

  /// @brief Create a random next message ID
  static message_id_t create_message_id()
  {
    return crypto::RandInRange(tini2p::under_cast(MinMsgID), tini2p::under_cast(MaxMsgID));
  }

  /// @brief Compare equality with another I2NPBlock
  std::uint8_t operator==(const I2NPBlock& oth) const
  {  // attempt constant-time comparisons
    const auto& head_eq = static_cast<std::uint8_t>(i2np_header_ == oth.i2np_header_);
    const auto& buf_eq = static_cast<std::uint8_t>(msg_buf_ == oth.msg_buf_);

    return (head_eq * buf_eq);
  }

  /// @brief Extract the inner I2NP header
  data::I2NPHeader extract_header()
  {
    return std::move(i2np_header_);
  }

  /// @brief Extract the inner I2NP message body
  crypto::SecBytes extract_body()
  {
    return std::move(msg_buf_);
  }

 private:
  void check_header(const data::I2NPHeader& header, const exception::Exception& ex) const
  {
    if (header.mode() != data::I2NPHeader::Mode::NTCP2)
      ex.throw_ex<std::logic_error>("invalid I2NP mode, expected NTCP2");
  }

  void check_message_buffer(const crypto::SecBytes& msg_buf, const exception::Exception& ex) const
  {
    check_message_len(msg_buf.size(), ex);
  }

  void check_message_len(const std::uint16_t& msg_len, const exception::Exception& ex) const
  {
    const std::uint16_t& max_len = tini2p::under_cast(MaxLen) - i2np_header_.size();

    if (msg_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid body length: " + std::to_string(msg_len) + ", max: " + std::to_string(max_len));
      }
  }
  data::I2NPHeader i2np_header_;
  crypto::SecBytes msg_buf_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_I2NP_H_
