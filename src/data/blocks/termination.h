/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_TERMINATION_H_
#define SRC_DATA_BLOCKS_TERMINATION_H_


#include "src/crypto/rand.h"
#include "src/time.h"

#include "src/data/blocks/block.h"

namespace tini2p
{
namespace data
{
class TerminationBlock : public Block
{
 public:
  enum
  {
    TermHeaderLen = 9,
    MinTermLen = TermHeaderLen,
    MaxTermLen = MaxLen,
    MaxTermAddDataLen = MaxTermLen - TermHeaderLen,
  };

  enum struct Reason : std::uint8_t
  {
    NormalClose = 0,
    TerminationRecvd,
    IdleTimeout,
    RouterShutdown,
    DataPhaseAEADFail,
    IncompatibleOpts,
    IncompatibleSig,
    ClockSkew,
    PaddingViolation,
    AEADFramingError,
    PayloadFormatError,
    SessionRequestError,
    SessionCreatedError,
    SessionConfirmedError,
    ReadTimeout,
    SigVerificationFail,
    InvalidS,
    Banned
  };

  using reason_t = Reason;  //< Termination reason trait alias
  using frames_t = std::uint64_t;  //< Valid frames trait alias
  using ad_t = std::vector<std::uint8_t>;  //< Additional data trait alias

  /// @brief Default ctor
  TerminationBlock() : Block(type_t::Termination, MinTermLen), rsn_(reason_t::NormalClose), valid_frames_(0)
  {
    serialize();
  }

  /// @brief Convert a TerminationBlock from a secure buffer
  explicit TerminationBlock(buffer_t buf) : Block(type_t::Termination, buf.size())
  {
    const exception::Exception ex{"TerminationBlock", __func__};

    check_len(buf.size(), tini2p::under_cast(MinTermLen), tini2p::under_cast(MaxTermLen), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Serialize Termination block to buffer
  void serialize()
  {
    size_ = TermHeaderLen + ad_.size();

    check_params({"TerminationBlock", __func__});

    buf_.resize(HeaderLen + size_);

    tini2p::BytesWriter<buffer_t> writer(buf_);

    write_header(writer);

    writer.write_bytes(valid_frames_, tini2p::Endian::Big);
    writer.write_bytes(rsn_);

    if (ad_.size())
      writer.write_data(ad_);
  }

  /// @brief Deserialize Termination block from buffer
  void deserialize()
  {
    const exception::Exception ex{"TerminationBlock", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::Termination, tini2p::under_cast(MinTermLen), tini2p::under_cast(MaxTermLen), ex);

    reader.read_bytes(valid_frames_, tini2p::Endian::Big);
    reader.read_bytes(rsn_);
    
    check_params(ex);

    if (reader.gcount())
      {
        ad_.resize(reader.gcount());
        ad_.shrink_to_fit();
        reader.read_data(ad_);
      }

    // reclaim unused space
    buf_.resize(reader.count());
    buf_.shrink_to_fit();
  }

  /// @brief Set the Termination message type
  /// @param type Termination message type
  /// @throw Exception on invalid Termination message type
  void reason(const reason_t reason)
  {
    check_reason(reason, {"TerminationBlock", __func__});
    rsn_ = reason;
  }

  /// @brief Get const reference to the Termination reason 
  const reason_t& reason() const noexcept
  {
    return rsn_;
  }

  /// @brief Get a const reference to Termination additional data
  const ad_t& ad() const noexcept
  {
    return ad_;
  }

  /// @brief Get a non-const reference to Termination message data
  ad_t& ad() noexcept
  {
    return ad_;
  }

  /// @brief Equality comparison with another TerminationBlock
  std::uint8_t operator==(const TerminationBlock& oth) const
  {  // attempt constant-time comparison
     const auto& rsn_eq = static_cast<std::uint8_t>(rsn_ == oth.rsn_);
     const auto& frames_eq = static_cast<std::uint8_t>(valid_frames_ == oth.valid_frames_);
     const auto& ad_eq = static_cast<std::uint8_t>(ad_ == oth.ad_);

     return (rsn_eq * frames_eq * ad_eq);
  }

  /// @brief For templated use with I2NP blocks
  std::uint8_t msg_type() const
  {
    const exception::Exception ex{"TerminationBlock", __func__};

    ex.throw_ex<std::logic_error>("invalid call to non-I2NP block");

    return 0;
  }

 private:
  void check_params(const exception::Exception& ex)
  {
    if (type_ != type_t::Termination)
      ex.throw_ex<std::logic_error>("invalid block type.");

    if (size_ < MinTermLen || size_ > MaxTermLen)
      ex.throw_ex<std::length_error>("invalid block size.");

    check_reason(rsn_, ex);
  }

  void check_reason(const reason_t reason, const exception::Exception& ex)
  {
    switch(reason)
    {
      case reason_t::NormalClose:
      case reason_t::TerminationRecvd:
      case reason_t::IdleTimeout:
      case reason_t::RouterShutdown:
      case reason_t::DataPhaseAEADFail:
      case reason_t::IncompatibleOpts:
      case reason_t::IncompatibleSig:
      case reason_t::ClockSkew:
      case reason_t::PaddingViolation:
      case reason_t::AEADFramingError:
      case reason_t::PayloadFormatError:
      case reason_t::SessionRequestError:
      case reason_t::SessionCreatedError:
      case reason_t::SessionConfirmedError:
      case reason_t::ReadTimeout:
      case reason_t::SigVerificationFail:
      case reason_t::InvalidS:
      case reason_t::Banned:
        return;
      default:
        ex.throw_ex<std::logic_error>("invalid termination reason.");
    }
  }

  reason_t rsn_;
  frames_t valid_frames_;
  ad_t ad_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_TERMINATION_H_
