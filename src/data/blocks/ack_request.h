/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_ACK_REQUEST_H_
#define SRC_DATA_BLOCKS_ACK_REQUEST_H_

#include "src/data/blocks/block.h"
#include "src/data/i2np/garlic_delivery.h"

namespace tini2p
{
namespace data
{
class AckRequestBlock : public Block
{
 public:
  enum
  {
    SessionIDLen = 4,  //< 4-bytes big-endian session ID, may change based on final spec
    FlagLen = 1,  //< single byte flag, see spec
    DeliveryLen = 33,  //< Destination garlic delivery instructions, see spec
    AckRequestLen = SessionIDLen + FlagLen + DeliveryLen,
  };

  enum struct Flags : std::uint8_t
  {
    Null,  //< set to zero for future use, see spec
  };

  using session_id_t = std::uint32_t;  //< Session ID trait alias
  using flag_t = Flags;  //< Flag trait alias
  using delivery_t = GarlicDeliveryInstructions;  //< Garlic delivery instructions trait alias

  /// @brief Default ctor, creates a partially initialized AckRequestBlock
  /// @detail Must set garlic delivery instructions `to hash` before serializing
  /// @note Needed for inclusion in std::variant containers
  AckRequestBlock()
      : Block(type_t::AckRequest, tini2p::under_cast(AckRequestLen)), session_id_(), flag_(Flags::Null), delivery_()
  {
  }

  /// @brief Fully-initializing ctor, creates an AckRequestBlock from supplied values
  /// @param id Reverse session ID for the Ack reply
  /// @param to_hash Destination hash for garlic delivery instructions
  AckRequestBlock(session_id_t id, delivery_t::hash_t to_hash)
      : Block(type_t::AckRequest, tini2p::under_cast(AckRequestLen)),
        session_id_(id),
        flag_(Flags::Null),
        delivery_(delivery_t::DeliveryFlags::Destination, std::forward<delivery_t::hash_t>(to_hash))
  {
    serialize();
  }

  /// @brief Deserializing ctor, creates an AckRequestBlock from a secure buffer
  explicit AckRequestBlock(buffer_t buf) : Block(type_t::AckRequest)
  {
    const exception::Exception ex{"AckRequestBlock", __func__};

    check_len(buf.size(), tini2p::under_cast(AckRequestLen), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Serialize the AckRequestBlock to buffer
  void serialize()
  {
    const exception::Exception ex{"AckRequestBlock", __func__};

    if (delivery_.delivery_flags() != delivery_t::DeliveryFlags::Destination || delivery_.to_hash().is_zero())
      ex.throw_ex<std::logic_error>("invalid garlic clove delivery flags and/or null destination hash.");

    tini2p::BytesWriter<buffer_t> writer(buf_);

    write_header(writer);

    writer.write_bytes(session_id_, tini2p::Endian::Big);
    writer.write_bytes(flag_);

    delivery_.serialize();
    writer.write_data(delivery_.buffer());
  }

  /// @brief Deserialize the AckRequestBlock from buffer
  void deserialize()
  {
    const exception::Exception ex{"AckRequestBlock", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::AckRequest, tini2p::under_cast(AckRequestLen), ex);

    reader.read_bytes(session_id_, tini2p::Endian::Big);

    flag_t flag;
    reader.read_bytes(flag);
    check_flag(flag, ex);
    flag_ = flag;

    auto& delivery_buf = delivery_.buffer();
    const auto& dlv_dest_len = tini2p::under_cast(delivery_t::DestinationLen);

    if (delivery_buf.size() != dlv_dest_len)
      delivery_buf.resize(dlv_dest_len);

    reader.read_data(delivery_buf);
    delivery_.deserialize();

    check_delivery(delivery_, ex);
  }

  /// @brief Get a const reference to the session ID
  const session_id_t& session_id() const noexcept
  {
    return session_id_;
  }

  /// @brief Set the session ID
  void session_id(const session_id_t id)
  {
    session_id_ = id;
  }

  /// @brief Get a const reference to the flag
  /// @note Currently unused, see spec
  const flag_t& flag() const noexcept
  {
    return flag_;
  }

  /// @brief Get a const reference to the garlic clove delivery instructions
  const delivery_t& delivery() const noexcept
  {
    return delivery_;
  }

  /// @brief Set the destination hash for the garlic delivery instructions
  /// @param hash Hash of the destination for session binding
  void set_destination_hash(delivery_t::hash_t hash)
  {
    delivery_.set_delivery(delivery_t::DeliveryFlags::Destination, std::forward<delivery_t::hash_t>(hash));
  }

  /// @brief Equality comparison with another AckRequestBlock
  std::uint8_t operator==(const AckRequestBlock& oth) const
  {  // attempt constant-time comparison
    const auto& ses_eq = static_cast<std::uint8_t>(session_id_ == oth.session_id_);
    const auto& flag_eq = static_cast<std::uint8_t>(flag_ == oth.flag_);
    const auto& dlv_eq = static_cast<std::uint8_t>(delivery_ == oth.delivery_);

    return (ses_eq * flag_eq * dlv_eq);
  }

  /// @brief For templated use with I2NP blocks
  std::uint8_t msg_type() const
  {
    const exception::Exception ex{"AckRequestBlock", __func__};

    ex.throw_ex<std::logic_error>("invalid call to non-I2NP block");

    return 0;
  }

 private:
  void check_flag(const flag_t& flag, const exception::Exception& ex)
  {
    if (flag != Flags::Null)
      ex.throw_ex<std::logic_error>("non-null flag.");
  }

  void check_delivery(const delivery_t& delivery, const exception::Exception& ex)
  {
    if (delivery.delivery_flags() != delivery_t::DeliveryFlags::Destination)
      ex.throw_ex<std::logic_error>("garlic delivery instructions must be for a Destination.");

    if (delivery.to_hash().is_zero())
      ex.throw_ex<std::logic_error>("null `to hash`.");
  }

  session_id_t session_id_;
  flag_t flag_;
  delivery_t delivery_;
};
}
}

#endif  // SRC_DATA_BLOCKS_ACK_REQUEST_H_
