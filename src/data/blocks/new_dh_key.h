/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_NEW_DH_KEY_H_
#define SRC_DATA_BLOCKS_NEW_DH_KEY_H_

#include "src/crypto/x25519.h"

#include "src/data/blocks/block.h"

namespace tini2p
{
namespace data
{
class NewDHKeyBlock : public Block
{
 public:
  enum
  {
    KeyIDLen = 2,
    DHKeyLen = crypto::X25519::PublicKeyLen,
    NewDHKeyLen = KeyIDLen + DHKeyLen,
  };

  using key_id_t = std::uint16_t;  //< KeyID trait alias
  using ratchet_key_t = crypto::X25519::pubkey_t;  //< Next DH ratchet key trait alias

  /// @brief Default ctor, creates uninitialized NewDHKeyBlock
  /// @detail Must set ratchet key before serializing to buffer
  NewDHKeyBlock() : Block(type_t::NewDHKey, tini2p::under_cast(NewDHKeyLen)), key_id_(), key_() {}

  /// @brief Fully initializing ctor, creates a NewDHKeyBlock from provided values
  /// @param id Key ID used for ACK messages
  /// @param key New DH ratchet key for next ratchet chain
  NewDHKeyBlock(key_id_t id, ratchet_key_t key)
      : Block(type_t::NewDHKey, tini2p::under_cast(NewDHKeyLen)),
        key_id_(std::forward<key_id_t>(id)),
        key_(std::forward<ratchet_key_t>(key))
  {
    serialize();
  }

  /// @brief Deserializing ctor, creates a NewDHKeyBlock from a secure buffer
  explicit NewDHKeyBlock(buffer_t buf) : Block(type_t::NewDHKey)
  {
    const exception::Exception ex{"NewDHKeyBlock", __func__};

    const auto& buf_len = buf.size();
    check_len(buf_len, tini2p::under_cast(NewDHKeyLen), ex);

    buf_ = std::forward<buffer_t>(buf);
    size_ = buf_len;

    deserialize();
  }

  /// @brief Serialize NewDHKeyBlock to buffer
  void serialize()
  {
    const exception::Exception ex{"NewDHKeyBlock", __func__};

    check_ratchet_key(key_, ex);

    tini2p::BytesWriter<buffer_t> writer(buf_);

    write_header(writer);

    writer.write_bytes(key_id_, tini2p::Endian::Big);
    writer.write_data(key_);
  }

  /// @brief Deserialize NewDHKeyBlock from buffer
  void deserialize()
  {
    const exception::Exception ex{"NewDHKeyBlock", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::NewDHKey, tini2p::under_cast(NewDHKeyLen), ex);

    reader.read_bytes(key_id_, tini2p::Endian::Big);

    ratchet_key_t key;
    reader.read_data(key);
    check_ratchet_key(key, ex);
    key_ = std::move(key);
  }

  /// @brief Get a const reference to the key ID
  const key_id_t& key_id() const noexcept
  {
    return key_id_;
  }

  /// @brief Set the key ID
  void key_id(const key_id_t id)
  {
    key_id_ = id;
  }

  /// @brief Get a const reference to the ratchet key
  const ratchet_key_t& ratchet_key() const noexcept
  {
    return key_;
  }

  /// @brief Set the ratchet key
  void ratchet_key(ratchet_key_t key)
  {
    check_ratchet_key(key, {"NewDHKeyBlock", __func__});

    key_ = std::forward<ratchet_key_t>(key);
  }

  /// @brief Equality comparison with another NewDHKeyBlock
  std::uint8_t operator==(const NewDHKeyBlock& oth) const
  {  // attempt constant-time comparison
    const auto& id_eq = static_cast<std::uint8_t>(key_id_ == oth.key_id_);
    const auto& key_eq = static_cast<std::uint8_t>(key_ == oth.key_);

    return (id_eq * key_eq);
  }

  /// @brief For templated use with I2NP blocks
  std::uint8_t msg_type() const
  {
    const exception::Exception ex{"NewDHKeyBlock", __func__};

    ex.throw_ex<std::logic_error>("invalid call to non-I2NP block");

    return 0;
  }

 private:
  void check_ratchet_key(const ratchet_key_t& key, const exception::Exception& ex)
  {
    if (key.buffer().is_zero())
      ex.throw_ex<std::logic_error>("null ratchet key.");
  }

  key_id_t key_id_;
  ratchet_key_t key_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_NEW_DH_KEY_H_
