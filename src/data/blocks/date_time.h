/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_DATE_TIME_H_
#define SRC_DATA_BLOCKS_DATE_TIME_H_

#include "src/exception/exception.h"

#include "src/bytes.h"
#include "src/time.h"

#include "src/data/blocks/block.h"

namespace tini2p
{
namespace data
{
/// @brief DateTime NTCP2 block
class DateTimeBlock : public Block
{
 public:
  enum : std::uint8_t
  {
    TimestampLen = 4,
    TimestampOffset = 3,
  };

  using timestamp_t = std::uint32_t;  // Timestamp trait alias

  /// @brief Default ctor, creates a fully-initialized DateTimeBlock
  /// @detail Sets timestamp to current time in seconds
  DateTimeBlock() : Block(type_t::DateTime, TimestampLen), timestamp_(time::now_s())
  {
    serialize();
  }

  /// @brief Deserializing ctor, creates a DateTimeBlock from a secure buffer
  DateTimeBlock(buffer_t buf) : Block(type_t::DateTime)
  {
    const exception::Exception ex{"DateTimeBlock", __func__};

    check_len(buf.size(), tini2p::under_cast(TimestampLen), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Serialize DateTime block to buffer
  /// @throw Length error if invalid size
  void serialize()
  {
    const exception::Exception ex{"DateTimeBlock", __func__};

    tini2p::BytesWriter<buffer_t> writer(buf_);

    check_timestamp(timestamp_, ex);

    write_header(writer);

    writer.write_bytes(timestamp_, tini2p::Endian::Big);
  }

  /// @brief Deserialize DateTime block from buffer
  /// @throw Length error if invalid size
  void deserialize()
  {
    const exception::Exception ex{"DateTimeBlock", __func__};
    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::DateTime, tini2p::under_cast(TimestampLen), ex);

    timestamp_t timestamp;
    reader.read_bytes(timestamp, tini2p::Endian::Big);

    check_timestamp(timestamp, ex);

    timestamp_ = timestamp;
  }

  /// @brief Get the timestamp
  timestamp_t timestamp() const noexcept
  {
    return timestamp_;
  }

  /// @brief Set the timestamp
  /// @param timestamp Timestamp to set
  void timestamp(const timestamp_t timestamp)
  {
    const exception::Exception ex{"DateTimeBlock", __func__};

    check_timestamp(timestamp, ex);

    timestamp_ = timestamp;
  }

  /// @brief Equality comparison with another DateTimeBlock
  std::uint8_t operator==(const DateTimeBlock& oth) const
  {
    return static_cast<std::uint8_t>(timestamp_ == oth.timestamp_);
  }

  /// @brief For templated use with I2NP blocks
  std::uint8_t msg_type() const
  {
    const exception::Exception ex{"DateTimeBlock", __func__};

    ex.throw_ex<std::logic_error>("invalid call to non-I2NP block");

    return 0;
  }

 private:
  void check_timestamp(const timestamp_t timestamp, const exception::Exception& ex)
  {
    if (!time::check_lag_s(timestamp))
      ex.throw_ex<std::length_error>("invalid timestamp skew.");
  }

  timestamp_t timestamp_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_DATE_TIME_H_
