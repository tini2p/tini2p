/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 */

#ifndef SRC_DATA_BLOCKS_GARLIC_H_
#define SRC_DATA_BLOCKS_GARLIC_H_

#include "src/time.h"

#include "src/crypto/rand.h"

#include "src/data/blocks/block.h"

#include "src/data/i2np/garlic_delivery.h"
#include "src/data/i2np/i2np_header.h"

namespace tini2p
{
namespace data
{
/// @class GarlicBlock
/// @brief Garlic block format for end-to-end messages
class GarlicBlock : public data::Block
{
 public:
  enum : std::uint16_t
  {
    GarlicHeaderLen = 9,
    MaxBodyLen = 65510,
    MinGarlicLen = 13,  //< BlockHeader(3) + MinDelivery(1) + GarlicHeader(9)
    MaxGarlicLen = 65519,
    DefaultExpiration = 720,  //< in seconds
  };

  /// @brief Default ctor
  GarlicBlock()
      : data::Block(data::Block::type_t::Garlic, MinGarlicLen),
        instructions_(),
        i2np_header_(data::I2NPHeader::Type::Data, {}, data::I2NPHeader::Mode::NTCP2),
        i2np_body_()
  {
    serialize();
  }

  /// @brief Create a Garlic block from a I2NP message type and body buffer
  GarlicBlock(data::I2NPHeader::Type i2np_type, crypto::SecBytes body, std::uint32_t msg_id = 0)
      : Block(type_t::Garlic, MinGarlicLen),
        instructions_(),
        i2np_header_(
            std::forward<data::I2NPHeader::Type>(i2np_type),
            std::forward<std::uint32_t>(msg_id),
            data::I2NPHeader::Mode::NTCP2)
  {
    const exception::Exception ex{"GarlicBlock", __func__};

    check_body(body, ex);

    i2np_body_ = std::forward<crypto::SecBytes>(body);

    serialize();
  }

  /// @brief Create a Garlic block from a I2NP message type and body buffer
  GarlicBlock(
      data::GarlicDeliveryInstructions instructions,
      data::I2NPHeader::Type i2np_type,
      crypto::SecBytes body,
      std::uint32_t msg_id = 0)
      : Block(type_t::Garlic, MinGarlicLen),
        instructions_(std::forward<data::GarlicDeliveryInstructions>(instructions)),
        i2np_header_(
            std::forward<data::I2NPHeader::Type>(i2np_type),
            std::forward<std::uint32_t>(msg_id),
            data::I2NPHeader::Mode::NTCP2)
  {
    const exception::Exception ex{"GarlicBlock", __func__};

    check_body(body, ex);

    i2np_body_ = std::forward<crypto::SecBytes>(body);

    serialize();
  }

  /// @brief Deserlializing ctor, create a GarlicBlock from secure buffer
  GarlicBlock(crypto::SecBytes buf) : Block(type_t::Garlic, MinGarlicLen)
  {
    const exception::Exception ex{"GarlicBlock", __func__};

    check_len(buf.size(), MinGarlicLen, GarlicHeaderLen + MaxBodyLen, ex);

    buf_ = std::forward<crypto::SecBytes>(buf);
    size_ = buf_.size() - HeaderLen;

    deserialize();
  }

  /// @brief Deserialize a GarlicBlock from buffer
  void serialize()
  {
    const exception::Exception ex{"GarlicBlock", __func__};

    check_body(i2np_body_, ex);

    instructions_.serialize();
    i2np_header_.serialize();

    size_ = garlic_size();
    buf_.resize(HeaderLen + size_);

    tini2p::BytesWriter<crypto::SecBytes> writer(buf_);

    write_header(writer);

    // write delivery instructions
    writer.write_data(instructions_.buffer());

    // write Garlic header
    writer.write_data(i2np_header_.buffer());

    // write I2NP message body
    writer.write_data(i2np_body_);
  }

  /// @brief Deserialize a GarlicBlock from buffer
  void deserialize()
  {
    const exception::Exception ex{"GarlicBlock", __func__};

    tini2p::BytesReader<crypto::SecBytes> reader(buf_);

    // read + check block header
    read_header(reader, type_t::Garlic, MinGarlicLen, GarlicHeaderLen + MaxBodyLen, ex);

    // read + check delivery instructions
    data::GarlicDeliveryInstructions instructions;
    read_delivery_instructions(reader, instructions, ex);

    // read + check I2NP header: type || msg_id || expiration
    crypto::SecBytes head_buf;
    head_buf.resize(data::I2NPHeader::NTCP2HeaderLen);
    reader.read_data(head_buf);

    data::I2NPHeader i2np_header(std::move(head_buf), data::I2NPHeader::Mode::NTCP2);
    check_header(i2np_header, ex);

    crypto::SecBytes body;
    body.resize(reader.gcount());

    // read I2NP body
    reader.read_data(body);

    // move temp variables to data members
    instructions_ = std::move(instructions);
    i2np_header_ = std::move(i2np_header);
    i2np_body_ = std::move(body);
  }

  /// @brief Get a const reference to the delivery instructions
  const data::GarlicDeliveryInstructions& instructions() const noexcept
  {
    return instructions_;
  }

  /// @brief Set the delivery instructions
  GarlicBlock& instructions(data::GarlicDeliveryInstructions instructions)
  {
    instructions_ = std::forward<data::GarlicDeliveryInstructions>(instructions);

    return *this;
  }

  /// @brief Get whether the clove is for local delivery
  std::uint8_t is_local() const
  {
    return static_cast<std::uint8_t>(
        instructions_.delivery_flags() == data::GarlicDeliveryInstructions::DeliveryFlags::Local);
  }

  /// @brief Get whether the clove is for router delivery
  std::uint8_t is_router() const
  {
    return static_cast<std::uint8_t>(
        instructions_.delivery_flags() == data::GarlicDeliveryInstructions::DeliveryFlags::Router);
  }

  /// @brief Get whether the clove is for tunnel delivery
  std::uint8_t is_tunnel() const
  {
    return static_cast<std::uint8_t>(
        instructions_.delivery_flags() == data::GarlicDeliveryInstructions::DeliveryFlags::Tunnel);
  }


  /// @brief Get a const reference to the I2NP message type
  const data::I2NPHeader::Type& i2np_type() const noexcept
  {
    return i2np_header_.type();
  }

  /// @brief Set the I2NP message type
  GarlicBlock& i2np_type(data::I2NPHeader::Type type)
  {
    const exception::Exception ex{"GarlicBlock", __func__};

    check_i2np_type(type, ex);
    i2np_header_.type(std::forward<data::I2NPHeader::Type>(type));

    return *this;
  }

  /// @brief Get a const reference to the I2NP message ID
  const std::uint32_t& message_id() const noexcept
  {
    return i2np_header_.message_id();
  }

  /// @brief Set the message ID
  GarlicBlock& message_id(std::uint32_t id)
  {
    i2np_header_.message_id(std::forward<std::uint32_t>(id));

    return *this;
  }

  /// @brief Get a const reference to the I2NP message expiration
  std::uint32_t expiration() const noexcept
  {
    return i2np_header_.expiration();
  }

  /// @brief Set the expiration
  GarlicBlock& expiration(std::uint32_t exp)
  {
    i2np_header_.expiration(std::forward<std::uint32_t>(exp));

    return *this;
  }

  /// @brief Get a const reference to the I2NP header
  const data::I2NPHeader& header() const noexcept
  {
    return i2np_header_;
  }

  /// @brief Set the I2NP header
  GarlicBlock& header(data::I2NPHeader header)
  {
    const exception::Exception ex{"GarlicBlock", __func__};

    check_header(header, ex);

    i2np_header_ = std::forward<data::I2NPHeader>(header);

    return *this;
  }

  /// @brief Get a const reference to the I2NP message body
  const crypto::SecBytes& body() const noexcept
  {
    return i2np_body_;
  }

  /// @brief Set the I2NP message body
  GarlicBlock& body(crypto::SecBytes body)
  {
    const exception::Exception ex{"GarlicBlock", __func__};

    check_body(body, ex);

    i2np_body_ = std::forward<crypto::SecBytes>(body);

    return *this;
  }

  /// @brief Extract the I2NP header
  /// @detail Moves the I2NP header
  data::I2NPHeader extract_header()
  {
    return std::move(i2np_header_);
  }

  /// @brief Extract the I2NP message body
  /// @detail Moves the I2NP message body
  crypto::SecBytes extract_body()
  {
    return std::move(i2np_body_);
  }

  /// @brief Get the size of the Garlic header and body
  std::uint16_t garlic_size() const
  {
    return instructions_.size() + data::I2NPHeader::NTCP2HeaderLen + i2np_body_.size();
  }

  /// @brief Equality comparison with another GarlicBlock
  std::uint8_t operator==(const GarlicBlock& oth) const
  {  // attempt constant-time comparison
    const auto& instr_eq = static_cast<std::uint8_t>(instructions_ == oth.instructions_);
    const auto& head_eq = static_cast<std::uint8_t>(i2np_header_ == oth.i2np_header_);
    const auto& body_eq = static_cast<std::uint8_t>(i2np_body_ == oth.i2np_body_);

    return (instr_eq * head_eq * body_eq);
  }

 private:
  void check_header(const data::I2NPHeader& header, const exception::Exception& ex) const
  {
    check_i2np_type(header.type(), ex);

    if (header.mode() != data::I2NPHeader::Mode::NTCP2)
      ex.throw_ex<std::logic_error>("invalid I2NP mode, expected NTCP2");
  }

  void check_i2np_type(const data::I2NPHeader::Type& type, const exception::Exception& ex) const
  {
    if (type != data::I2NPHeader::Type::Data && type != data::I2NPHeader::Type::DatabaseStore)
      ex.throw_ex<std::logic_error>("invalid Garlic I2NP payload type: " + std::to_string(tini2p::under_cast(type)));
  }

  void check_body(const crypto::SecBytes& body, const exception::Exception& ex) const
  {
    const auto& body_len = body.size();
    const auto& max_len = tini2p::under_cast(MaxBodyLen);

    if (body_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid body length: " + std::to_string(body_len) + ", max: " + std::to_string(max_len));
      }
  }

  void read_delivery_instructions(
      tini2p::BytesReader<crypto::SecBytes>& reader,
      data::GarlicDeliveryInstructions& instructions,
      const exception::Exception& ex) const
  {
    data::GarlicDeliveryInstructions::flags_t flags;
    reader.read_bytes(flags);

    instructions.resize_from_flags(flags);

    reader.skip_back(data::GarlicDeliveryInstructions::FlagsLen);
    reader.read_data(instructions.buffer());

    instructions.deserialize();
  }

  data::GarlicDeliveryInstructions instructions_;
  data::I2NPHeader i2np_header_;
  crypto::SecBytes i2np_body_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_GARLIC_H_
