/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_INFO_H_
#define SRC_DATA_BLOCKS_INFO_H_

#include "src/exception/exception.h"

#include "src/ntcp2/meta.h"
#include "src/time.h"

#include "src/data/router/info.h"

#include "src/data/blocks/block.h"

namespace tini2p
{
namespace data
{
/// @brief RouterInfo NTCP2 block
class InfoBlock : public Block
{
 public:
  enum : std::uint16_t
  {
    FloodFlagLen = 1,
    InfoHeaderLen = HeaderLen + FloodFlagLen,
    MinInfoBlockLen = 440,  //< min RI (439) + flag (1)
    MaxInfoBlockLen = MaxLen,
    MACLen = 16,
    MinPayloadLen = FloodFlagLen + MinInfoBlockLen,
    MaxPayloadLen = FloodFlagLen + MaxInfoBlockLen,
    MinMsg3Pt2Len = MinInfoBlockLen + MACLen,
    MaxMsg3Pt2Len = MaxInfoBlockLen + MACLen,
    MinPaddingLen = 0,
    MaxPaddingLen = MaxMsg3Pt2Len - MinInfoBlockLen,
  };

  enum : std::uint8_t
  {
    FloodFlagOffset = 3,
    RouterInfoOffset,
  };

  enum struct Flag : std::uint8_t
  {
    FloodFlag = 0x01,
  };

  using flag_t = Flag;  //< Flag trait alias
  using info_t = data::Info;  //< RouterInfo trait alias

  /// @brief Default ctor
  InfoBlock() : Block(type_t::Info, FloodFlagLen + MinInfoBlockLen), flag_(flag_t::FloodFlag), info_(nullptr) {}

  /// @brief Copy-ctor
  InfoBlock(const InfoBlock& oth) : Block(type_t::Info, oth.size_), flag_(oth.flag_), info_(oth.info_)
  {
    buf_ = oth.buf_;
  }

  /// @brief Move-ctor
  InfoBlock(InfoBlock&& oth) : Block(type_t::Info, oth.size_), flag_(oth.flag_), info_(oth.info_)
  {
    buf_ = std::move(oth.buf_);
  }

  /// @brief Create a InfoBlock from a RouterInfo
  /// @param info RouterInfo to create the block
  explicit InfoBlock(info_t::shared_ptr info)
      : Block(type_t::Info, FloodFlagLen + MinInfoBlockLen), flag_(flag_t::FloodFlag), info_(info)
  {
    const exception::Exception ex{"InfoBlock", __func__};

    if (!info)
      ex.throw_ex<std::invalid_argument>("null RouterInfo.");

    size_ = FloodFlagLen + info->size();
    buf_.resize(HeaderLen + size_);

    serialize();
  }

  /// @brief Create a InfoBlock from an iterator range
  explicit InfoBlock(buffer_t buf) : Block(type_t::Info)
  {
    const exception::Exception ex{"InfoBlock", __func__};

    check_len(buf.size(), tini2p::under_cast(MinInfoBlockLen), tini2p::under_cast(MaxInfoBlockLen), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  InfoBlock& operator=(InfoBlock oth)
  {
    size_ = std::forward<size_type>(oth.size_);
    flag_ = std::forward<flag_t>(oth.flag_);
    info_ = std::forward<info_t::shared_ptr>(oth.info_);
    buf_ = std::forward<buffer_t>(oth.buf_);

    return *this;
  }

  /// @brief Get a const pointer to the router info
  info_t::const_shared_ptr info() const noexcept
  {
    return info_;
  }

  /// @brief Get a non-const pointer to the router info
  info_t::shared_ptr info() noexcept
  {
    return info_;
  }

  /// @brief Serialize RouterInfo block to buffer
  /// @throw Length error if invalid size
  void serialize()
  {
    const exception::Exception ex{"InfoBlock", __func__};

    buf_.resize(size());

    check_params(ex);

    tini2p::BytesWriter<buffer_t> writer(buf_);

    write_header(writer);
    writer.write_bytes(flag_);

    info_->serialize();
    writer.write_data(info_->buffer());
  }

  /// @brief Deserialize RouterInfo block from buffer
  /// @throw Length error if invalid size
  void deserialize()
  {
    const exception::Exception ex{"InfoBlock", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::Info, tini2p::under_cast(MinInfoBlockLen), tini2p::under_cast(MaxInfoBlockLen), ex);

    reader.read_bytes(flag_);

    Info::buffer_t ri_buf(size_ - tini2p::under_cast(FloodFlagLen));
    reader.read_data(ri_buf);
    info_.reset(new Info(std::move(ri_buf)));

    check_params(ex);
  }

  /// @brief Equality comparison with another InfoBlock
  std::uint8_t operator==(const InfoBlock& oth) const
  {  // unfortunately cannot be constant-time because of pointers
    const auto& flag_eq = static_cast<std::uint8_t>(flag_ == oth.flag_);
    std::uint8_t info_eq(0);
    if (info_ && oth.info_)  // both info pointer set, compare contained info
      info_eq = static_cast<std::uint8_t>(*info_ == *oth.info_);
    else  // one or both info pointers unset, compare both being unset
      info_eq = static_cast<std::uint8_t>(!info_ && !oth.info_);

    return (flag_eq * info_eq);
  }

 private:
  void check_params(const exception::Exception& ex)
  {
    // check if flag contains reserved flag bits
    if (tini2p::under_cast(flag_) >> 1 != 0)
      ex.throw_ex<std::runtime_error>("invalid flood request flag.");

    // check for a valid router info
    if (!info_)
      ex.throw_ex<std::runtime_error>("need a valid RouterInfo.");
  }

  flag_t flag_;
  info_t::shared_ptr info_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_INFO_H_
