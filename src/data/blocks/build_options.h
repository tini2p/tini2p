/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_BUILD_OPTIONS_H_
#define SRC_DATA_BLOCKS_BUILD_OPTIONS_H_

#include <optional>

#include "src/crypto/chacha_poly1305.h"

#include "src/data/blocks/block.h"

namespace tini2p
{
namespace data
{
// TODO(tini2p): impl when Tunnel Build options format is defined
class BuildOptionsBlock : public Block
{
 public:
  enum : std::uint16_t
  {
    FlagsLen = 1,
    ReceiveKeyLen = 32,
    MinOptionsLen = 1,
    ReceiveOptionsLen = 33,
    MaxOptionsLen = 373,
  };

  /// @brief Flags for included options fields
  enum struct Flags : std::uint8_t
  {
    Null = 0x00,          //< Null build options, 0000 0000
    ReceiveKey = 0x01,  //< Receive key included, 0000 0001
    Reserved = 0xFE,     //< Reserved flags mask, 1111 1110
  };

  using aead_t = crypto::ChaChaPoly1305;  //< AEAD encryption trait alias
  using flags_t = Flags;  //< Flags trait alias

  /// @brief Default ctor
  BuildOptionsBlock() : Block(type_t::BuildOptions, tini2p::under_cast(MinOptionsLen)), flags_(flags_t::Null)
  {
    serialize();
  }

  /// @brief Initializing ctor, creates a BuildOptionsBlock with receive key
  explicit BuildOptionsBlock(aead_t::key_t key)
      : Block(type_t::BuildOptions, tini2p::under_cast(ReceiveOptionsLen)), flags_(flags_t::Null), receive_key_()
  {
    receive_key(std::forward<aead_t::key_t>(key));

    serialize();
  }

  /// @brief Deserializing ctor, create BuildOptionsBlock from a secure buffer
  explicit BuildOptionsBlock(buffer_t buf) : Block(type_t::BuildOptions)
  {
    const exception::Exception ex{"BuildOptionsBlock", __func__};

    const auto& buf_len = buf.size();
    check_len(buf_len, tini2p::under_cast(MinOptionsLen), tini2p::under_cast(MaxOptionsLen), ex);

    buf_ = std::forward<buffer_t>(buf);
    size_ = buf_len - tini2p::under_cast(HeaderLen);

    deserialize();
  }

  /// @brief Serialize the BuildOptionsBlock to the buffer
  // TODO(tini2p): expand when spec is finalized
  void serialize()
  {
    const exception::Exception ex{"BuildOptionsBlock", __func__};

    check_type(type_, type_t::BuildOptions, ex);
    check_flags(flags_, ex);
    check_receive_flags(flags_, receive_key_, ex);
   
    const auto& header_len = tini2p::under_cast(HeaderLen);
    const auto& opt_size = options_size();
    buf_.resize(header_len + opt_size);
    size_ = opt_size;

    tini2p::BytesWriter<buffer_t> writer(buf_);
    write_header(writer);

    writer.write_bytes(flags_);

    if (receive_key_ != std::nullopt)
      writer.write_data(*receive_key_);
  }

  /// @brief Deserialize the BuildOptionsBlock from the buffer
  // TODO(tini2p): expand when spec is finalized
  void deserialize()
  {
    const exception::Exception ex{"BuildOptionsBlock", __func__};

    const auto& min_len = tini2p::under_cast(MinOptionsLen);
    const auto& max_len = tini2p::under_cast(MaxOptionsLen);
    const auto& buf_len = buf_.size();

    check_len(buf_len, min_len, max_len, ex);

    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::BuildOptions, min_len, max_len, ex);

    flags_t flags;
    reader.read_bytes(flags);

    check_flags(flags, ex);

    flags_ = std::move(flags);

    if (has_receive_key(flags_))
      {
        aead_t::key_t receive_key;
        reader.read_data(receive_key);
        check_receive_key(receive_key, ex);
        receive_key_.emplace(std::move(receive_key));
      }
  }

  /// @brief Get the AEAD receive key
  const aead_t::key_t& receive_key() const
  {
    const exception::Exception ex{"BuildOptionsBlock", __func__};

    if (receive_key_ == std::nullopt)
      ex.throw_ex<std::runtime_error>("null receive key");

    return *receive_key_;
  }

  /// @brief Set the AEAD receive key
  BuildOptionsBlock& receive_key(aead_t::key_t receive_key)
  {
    const exception::Exception ex{"BuildOptionsBlock", __func__};

    check_receive_key(receive_key, ex);

    add_flags(flags_, flags_t::ReceiveKey, ex);
    receive_key_.emplace(std::forward<aead_t::key_t>(receive_key));

    return *this;
  }

  /// @brief Get the size of the BuildOptions
  size_type options_size() const
  {
    return tini2p::under_cast(FlagsLen) + (has_receive_key(flags_) * tini2p::under_cast(ReceiveKeyLen));
  }

  /// @brief Equality comparison with another BuildOptionsBlock
  // TODO(tini2p): expand when impl is more complete
  std::uint8_t operator==(const BuildOptionsBlock& oth) const
  {  // attempt constant-time comparison
    const auto& type_eq = static_cast<std::uint8_t>(type_ == oth.type_);
    const auto& size_eq = static_cast<std::uint8_t>(size_ == oth.size_);
    const auto& flag_eq = static_cast<std::uint8_t>(flags_ == oth.flags_);
    const auto& recv_eq = static_cast<std::uint8_t>(receive_key_ == oth.receive_key_);
    const auto& buf_eq = static_cast<std::uint8_t>(buf_ == oth.buf_);

    return (type_eq * size_eq * flag_eq * recv_eq * buf_eq);
  }

 private:
  void check_flags(const flags_t& flags, const exception::Exception& ex) const
  {
    const auto& flag_int = tini2p::under_cast(flags);
    const auto& reserved = tini2p::under_cast(flags_t::Reserved);

    if ((flag_int & reserved) > 0)
      {
        ex.throw_ex<std::logic_error>(
            "flags: " + std::to_string(flag_int) + " includes reserved flags: " + std::to_string(reserved));
      }
  }

  void add_flags(flags_t& flags, const flags_t& new_flags, const exception::Exception& ex) const
  {
    check_flags(flags, ex);
    check_flags(new_flags, ex);

    flags = static_cast<flags_t>(tini2p::under_cast(flags) | tini2p::under_cast(new_flags));
  }

  void check_receive_key(const aead_t::key_t& key, const exception::Exception& ex) const
  {
    if (key.is_zero())
      ex.throw_ex<std::logic_error>("null receive key.");
  }

  void check_receive_flags(
      const flags_t& flags,
      const std::optional<aead_t::key_t>& receive_key,
      const exception::Exception& ex) const
  {
    const auto& has_recv_key = has_receive_key(flags_);

    if (has_recv_key && receive_key == std::nullopt)
      ex.throw_ex<std::logic_error>("flags indicate receive key, but receive key is null.");

    if (!has_recv_key && receive_key != std::nullopt)
      ex.throw_ex<std::logic_error>("flags indicate no receive key, but receive key present.");

    if (receive_key != std::nullopt)
      check_receive_key(*receive_key, ex);
  }

  std::uint8_t has_receive_key(const flags_t& flags) const noexcept
  {
    return static_cast<std::uint8_t>((tini2p::under_cast(flags) & tini2p::under_cast(flags_t::ReceiveKey)) > 0);
  }

  flags_t flags_;
  std::optional<aead_t::key_t> receive_key_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_BUILD_OPTIONS_H_
