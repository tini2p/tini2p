/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_DATA_BLOCKS_NEW_SESSION_ACK_H_
#define SRC_DATA_BLOCKS_NEW_SESSION_ACK_H_

#include "src/crypto/sha.h"

#include "src/data/blocks/block.h"

namespace tini2p
{
namespace data
{
/// @class NewSessionAckBlock
/// @brief Creates an Ack to a received, bound ECIES NewSession message
/// @detail Includes Bob's Destination hash for Alice to associate the
///    incoming NewSession message reply with the original outbound session.
class NewSessionAckBlock : public Block
{
 public:
  enum
  {
    NewSessionAckLen = crypto::Sha256::DigestLen,
  };

  using dest_hash_t = crypto::Sha256::digest_t;  //< Destination hash trait alias

  /// @brief Default ctor, creates an uninitialized NewSessionAckBlock
  /// @detail Caller responsible for initializing the Destination hash before serializing
  /// @note Useful for deserializing from the wire
  NewSessionAckBlock() : Block(type_t::NewSessionAck, tini2p::under_cast(NewSessionAckLen)), dest_hash_() {}

  /// @brief Fully-initializing ctor, creates a NewSessionAckBlock from a Destination hash
  explicit NewSessionAckBlock(dest_hash_t dest_hash)
      : Block(type_t::NewSessionAck, tini2p::under_cast(NewSessionAckLen)),
        dest_hash_(std::forward<dest_hash_t>(dest_hash))
  {
    serialize();
  }

  /// @brief Deserializing ctor, creates a NewSessionAckBlock from a secure buffer
  explicit NewSessionAckBlock(buffer_t buf)
      : Block(type_t::NewSessionAck, tini2p::under_cast(NewSessionAckLen)), dest_hash_()
  {
    const exception::Exception ex{"NewSessionAckBlock", __func__};

    check_len(buf.size(), tini2p::under_cast(NewSessionAckLen), ex);

    buf_ = std::forward<buffer_t>(buf);

    deserialize();
  }

  /// @brief Serialize NewSessionAckBlock to buffer
  void serialize()
  {
    const exception::Exception ex{"NewSessionAckBlock", __func__};

    check_len(buf_.size(), tini2p::under_cast(NewSessionAckLen), ex);

    if (dest_hash_.is_zero())
      ex.throw_ex<std::logic_error>("null Destination hash.");

    tini2p::BytesWriter<buffer_t> writer(buf_);

    write_header(writer);

    writer.write_data(dest_hash_);
  }

  /// @brief Deserialize NewSessionAckBlock from buffer
  void deserialize()
  {
    const exception::Exception ex{"NewSessionAckBlock", __func__};

    tini2p::BytesReader<buffer_t> reader(buf_);

    read_header(reader, type_t::NewSessionAck, tini2p::under_cast(NewSessionAckLen), ex);

    dest_hash_t dest_hash;
    reader.read_data(dest_hash);

    if (dest_hash.is_zero())
      ex.throw_ex<std::logic_error>("null Destination hash.");

    dest_hash_ = std::move(dest_hash);
  }

  /// @brief Get a const reference to the Destination hash
  const dest_hash_t& dest_hash() const noexcept
  {
    return dest_hash_;
  }

  /// @brief Set the Destination hash
  void dest_hash(dest_hash_t dest_hash)
  {
    const exception::Exception ex{"NewSessionAckBlock", __func__};

    if (dest_hash.is_zero())
      ex.throw_ex<std::logic_error>("null Destination hash.");

    dest_hash_ = std::forward<dest_hash_t>(dest_hash);

    serialize();
  }

  std::uint8_t operator==(const NewSessionAckBlock& oth) const
  {
    return dest_hash_ == oth.dest_hash_;
  }

 private:
  dest_hash_t dest_hash_;
};
}  // namespace data
}  // namespace tini2p

#endif  // SRC_DATA_BLOCKS_NEW_SESSION_ACK_H_
