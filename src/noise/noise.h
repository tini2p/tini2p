/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_NOISE_NOISE_H_
#define SRC_NOISE_NOISE_H_

#include "src/bytes.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"
#include "src/crypto/x25519.h"

namespace tini2p
{
namespace noise
{
// TODO(tini2p): move all wrapper functions from ntcp2::noise
using curve_t = crypto::X25519;  //< Curve trait alias
using hkdf_t = crypto::HKDF<crypto::HmacSha256>;  //< HKDF-HMAC-SHA256 trait alias
using sha_t = crypto::Sha256;  //< Sha256 trait alias

/// @brief Perform MixHash: h = SHA256(h || data)
/// @param h Noise hash state
/// @param data Input data buffer
template <class TData>
inline static void MixHash(sha_t::digest_t& h, const TData& data)
{
  crypto::SecBytes h_in;
  h_in.resize(sha_t::DigestLen + data.size());
  tini2p::BytesWriter<crypto::SecBytes> ciph_writer(h_in);

  // MixHash(data)
  // h = SHA256(h || data)
  ciph_writer.write_data(h);
  ciph_writer.write_data(data);

  sha_t::Hash(h, h_in);
}

/// @brief Perform MixKey: (chain_key, k) = HKDF(chain_key, keymaterial)
/// @param chain_key Noise chain key
/// @param k Noise symmetric message key
/// @param material Input key material buffer
template <class TKeyMaterial>
inline static void MixKey(curve_t::shrkey_t& chain_key, curve_t::shrkey_t& k, const TKeyMaterial& material)
{
  crypto::FixedSecBytes<64> okm;
  hkdf_t::Derive(okm, chain_key, material);

  tini2p::BytesReader<decltype(okm)> reader(okm);
  reader.read_data(chain_key);
  reader.read_data(k);
}

/// @brief Perform Split: (temp_k1, temp_k2) = HKDF(chain_key, keymaterial)
/// @param chain_key Noise chain key
/// @return Inbound and Outbound cipherstate keys (respectively)
inline static std::pair<curve_t::shrkey_t, curve_t::shrkey_t> Split(curve_t::shrkey_t& chain_key)
{
  crypto::FixedSecBytes<64> okm;
  hkdf_t::Derive(okm, chain_key, crypto::FixedSecBytes<0>());

  curve_t::shrkey_t temp_k1, temp_k2;
  tini2p::BytesReader<decltype(okm)> reader(okm);
  reader.read_data(temp_k1);
  reader.read_data(temp_k2);

  return std::make_pair(temp_k1, temp_k2);
}
}  // namespace noise
}  // namespace tini2p

#endif  // SRC_NOISE_NOISE_H_
