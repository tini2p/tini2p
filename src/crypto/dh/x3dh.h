/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_DH_X3DH_H_
#define SRC_CRYPTO_DH_X3DH_H_

#include "src/bytes.h"

#include "src/crypto/kdf_context.h"

#include "src/crypto/blake.h"
#include "src/crypto/eddsa.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/sha.h"

#include "src/crypto/x25519.h"

namespace tini2p
{
namespace crypto
{
/// @struct X3DH
/// @brief X3DH implementation
/// @tparam Hasher HKDF HMAC-based hashing impl, e.g. HmacSha256, Blake2b, etc.
template <
    class Hasher,
    typename = std::enable_if_t<
        std::is_same<Hasher, HmacSha256>::value
        || std::is_same<Hasher, Blake2b>::value>>
class X3DH
{
 public:
  using curve_t = X25519;  //< Curve trait alias
  using hmac_t = Hasher;  //< HMAC-based hashing function trait alias
  using hkdf_t = HKDF<hmac_t>;  //< HKDF trait alias
  using context_t = KDFContext<hmac_t>; //< KDF context trait alias
  using salt_t = typename hmac_t::salt_t;  //< Salt trait alias

  /// @brief Do a Diffie-Hellman key exchange to derive a shared key
  /// @tparam Output Type for the output buffer (SecureBuffer >= SharedKeyLen)
  template <
      class Output,
      typename = std::enable_if_t<
          std::is_same<Output, SecBytes>::value
          || std::is_same<Output, curve_t::shrkey_t>::value>>
  inline static void DH(
      Output& out,
      const curve_t::pubkey_t& bob_static,
      const curve_t::pubkey_t& bob_ephemeral,
      const curve_t::keypair_t& alice_static,
      const curve_t::keypair_t& alice_ephemeral,
      const context_t& ctx = context_t())
  {
    if (out.size() != curve_t::SharedKeyLen)
      exception::Exception{"X3DH", __func__}.throw_ex<std::invalid_argument>(
          "invalid output key length.");


    // perform DH between Alice (local, private keys) and Bob (remote, public keys)
    curve_t::shrkey_t out1, out2, out3;
    curve_t::DH(out1, alice_static.pvtkey, bob_ephemeral);
    curve_t::DH(out2, alice_ephemeral.pvtkey, bob_static);
    curve_t::DH(out3, alice_ephemeral.pvtkey, bob_ephemeral);

    using kdf_in_t = FixedSecBytes<curve_t::SharedKeyLen * 4>;
    kdf_in_t kdf_in;
    tini2p::BytesWriter<kdf_in_t> kdf_writer(kdf_in);

    // HKDF key-material: (byte[DigestLen](0xFF) || DH(1) || DH(2) || DH(3)), see X3DH spec
    kdf_writer.write_data(std::array<std::uint8_t, Hasher::DigestLen>{
        {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
         0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
         0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}});
    kdf_writer.write_data(out1);
    kdf_writer.write_data(out2);
    kdf_writer.write_data(out3);

    // Derive KDF output w/ empty (all zeroes) salt, see X3DH spec
    hkdf_t::Derive(
        out.data(), out.size(), kdf_in.data(), kdf_in.size(), salt_t{}, ctx);
  }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_DH_X3DH_H_
