/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_CHACHA20_H_
#define SRC_CRYPTO_CHACHA20_H_

#include <sodium.h>

#include "src/crypto/keys.h"
#include "src/crypto/nonce.h"

#include "src/crypto/tunnel/nonce.h"

namespace tini2p
{
namespace crypto
{
/// @class ChaCha20
/// @brief ChaCha20 encryption class
class ChaCha20
{
 public:
  enum
  {
    KeyLen = 32,
    NonceLen = 12,
    TunnelNonceLen = 8,
  };

  struct Key : public crypto::Key<KeyLen>
  {
    using base_t = crypto::Key<KeyLen>;

    Key() : base_t() {}

    Key(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

    Key(const SecBytes& buf) : base_t(buf) {}

    Key(std::initializer_list<std::uint8_t> list) : base_t(list) {}
  };

  using key_t = ChaCha20::Key;  //< Key trait alias
  using nonce_t = Nonce;  //< Nonce trait alias
  using tunnel_nonce_t = TunnelNonce;  //< Tunnel nonce trait alias

  /// @brief ChaCha20 stream + XOR cipher (in-place)
  /// @detail Generates a ChaCha20 keystream from the key and nonce, and XORs with the message in-place
  /// @tparam TNonce Nonce type (full nonce or tunnel nonce)
  /// @param message Message stream to XOR with keystream
  /// @param nonce Nonce to use for keystream generation
  /// @param key Key to use for keystream generation
  template <
      class TMessage,
      class TNonce,
      typename = std::enable_if_t<std::is_same<TNonce, nonce_t>::value || std::is_same<TNonce, tunnel_nonce_t>::value>>
  static void StreamXOR(TMessage& message, const TNonce& nonce, const key_t& key)
  {
    const exception::Exception ex{"ChaCha20", __func__};

    auto msg_ptr = message.data();
    const auto& msg_len = message.size();

    StreamXOR(msg_ptr, msg_len, msg_ptr, msg_len, nonce.data(), key, ex);
  }

  /// @brief ChaCha20 stream + XOR cipher (in-place)
  /// @detail Generates a ChaCha20 keystream from the key and nonce, and XORs with the message in-place
  /// @tparam TNonce Nonce type (full nonce or tunnel nonce)
  /// @param stream_ptr Pointer to the message stream to XOR with keystream
  /// @param stream_len Length of the message stream to XOR with keystream
  /// @param nonce Nonce to use for keystream generation
  /// @param key Key to use for keystream generation
  template <
      class TNonce,
      typename = std::enable_if_t<std::is_same<TNonce, nonce_t>::value || std::is_same<TNonce, tunnel_nonce_t>::value>>
  static void StreamXOR(std::uint8_t* stream_ptr, const std::size_t& stream_len, const TNonce& nonce, const key_t& key)
  {
    const exception::Exception ex{"ChaCha20", __func__};

    StreamXOR(stream_ptr, stream_len, stream_ptr, stream_len, nonce.data(), key, ex);
  }

  /// @brief ChaCha20 stream + XOR cipher (in-place)
  /// @detail Generates a ChaCha20 keystream from the key and nonce, and XORs with the message in-place
  /// @param stream_ptr Pointer to the message stream to XOR with keystream
  /// @param stream_len Length of the message stream to XOR with keystream
  /// @param nonce Nonce to use for keystream generation
  /// @param key Key to use for keystream generation
  static void StreamXOR(
      std::uint8_t* stream_ptr,
      const std::size_t& stream_len,
      const std::uint8_t* nonce_ptr,
      const std::size_t& nonce_len,
      const key_t& key)
  {
    const exception::Exception ex{"ChaCha20", __func__};

    const auto& full_nonce_len = tini2p::under_cast(NonceLen);
    const auto& tun_nonce_len = tini2p::under_cast(TunnelNonceLen);

    tini2p::check_cbuf(nonce_ptr, nonce_len, tun_nonce_len, full_nonce_len, ex);

    StreamXOR(stream_ptr, stream_len, stream_ptr, stream_len, nonce_ptr, key, ex);
  }

  /// @brief Create a random key
  static key_t create_key()
  {
    return key_t(crypto::RandBytes<KeyLen>());
  }

private:
 static void StreamXOR(
     std::uint8_t* stream_ptr,
     const std::size_t& stream_len,
     const std::uint8_t* msg_ptr,
     const std::size_t& msg_len,
     const std::uint8_t* nonce_ptr,
     const key_t& key,
     const exception::Exception& ex)
 {
   if (stream_len != msg_len)
     ex.throw_ex<std::length_error>("ciphertext stream length must equal message length");

   crypto_stream_chacha20_xor(stream_ptr, msg_ptr, msg_len, nonce_ptr, key.data());
 }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_CHACHA20_H_
