/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_BLOOM_FILTER_H_
#define SRC_CRYPTO_BLOOM_FILTER_H_

#include <cmath>
#include <deque>
#include <mutex>
#include <shared_mutex>

#include "src/crypto/bn.h"
#include "src/crypto/sec_bytes.h"
#include "src/crypto/siphash.h"

namespace tini2p
{
namespace crypto
{
/// @brief Constant for log(1 / pow(2, log(2)))
static constexpr double LOG_ONE_DIV_POW2LOG2 = -0.480453;

/// @brief Constant for log(2)
static constexpr double LOG2 = 0.693147;

/// @brief Number of bits and hash rounds in the Bloom filter
class BloomParams
{
 public:
  enum
  {
    MinN = 128,  //< aribitrary minimum elements, any less wouldn't need a Bloom filter
    MaxN = 16777215,  //< arbitrary maximum elements, any more would consume too many resources 
    MinP = 100,  //< arbitrary minimum One-in-P
    MaxP = 10000,  //< arbitrary maximum One-in-P
    MinM = 1225,  //< Min filter bits, based on MinN and MinP w/ k=7
    MaxM = 321621170,  //< Max filter bits, based on MaxN and MaxP w/ k=13
    MinK = 7,  //< Min hash rounds, based on MinN and MinP
    MaxK = 13,  //< Max hash rounds, based on MaxN and MaxP
  };

  using n_t = std::uint32_t;  //< N filter elements trait alias
  using p_t = std::uint16_t;  //< One-in-P false-positives trait alias
  using m_t = std::uint32_t;  //< M filter bits trait alias
  using k_t = std::uint16_t;  //< K hash rounds trait alias

  /// @brief Default ctor
  BloomParams() : n_(), p_(), m_(), k_() {}

  /// @brief Fully initializing ctor, calculate the number of filter bits and hash rounds
  /// @detail Caller can use this ctor to adjust paramters for more or less bits in the filter
  /// @param n N elements in the Bloom filter
  /// @param p One-in-P number of false-positive membership checks
  BloomParams(n_t n, p_t p)
  {
    const exception::Exception ex{"BloomParams", __func__};

    check_n(n, ex);
    check_p(p, ex);

    const double in_p = double(1) / p;
    const auto m = std::ceil((static_cast<const double>(n) * std::log(in_p)) / LOG_ONE_DIV_POW2LOG2);
    const auto k = std::ceil((m / n) * LOG2);

    auto ret_m = static_cast<m_t>(m);
    auto ret_k = static_cast<k_t>(k);

    check_m(ret_m, ex);
    check_k(ret_k, ex);

    n_ = std::forward<n_t>(n);
    p_ = std::forward<p_t>(p);
    m_ = std::move(ret_m);
    k_ = std::move(ret_k);
  }

  /// @brief Get a const reference to the number of elements
  const n_t& n() const noexcept
  {
    return n_;
  }

  /// @brief Get a const reference to the one-in-P number of false-positives
  const p_t& p() const noexcept
  {
    return p_;
  }

  /// @brief Get a const reference to the number of bits
  const m_t& m() const noexcept
  {
    return m_;
  }

  /// @brief Get a const reference to the number of hash rounds
  const k_t& k() const noexcept
  {
    return k_;
  }

 private:
  void check_n(const n_t& n, const exception::Exception& ex) const
  {
    const auto& min_n = tini2p::under_cast(MinN);
    const auto& max_n = tini2p::under_cast(MaxN);

    if (n < min_n || n > max_n)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid number of set elements: " + std::to_string(n) + ", min: " + std::to_string(min_n)
            + ", max: " + std::to_string(max_n));
      }
  }

  void check_p(const p_t& p, const exception::Exception& ex) const
  {
    const auto& min_p = tini2p::under_cast(MinP);
    const auto& max_p = tini2p::under_cast(MaxP);

    if (p < min_p || p > max_p)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid one-in-P number of false positives: " + std::to_string(p) + ", min: " + std::to_string(min_p)
            + ", max: " + std::to_string(max_p));
      }
  }

  void check_m(const m_t& m, const exception::Exception& ex) const
  {
    const auto& min_m = tini2p::under_cast(MinM);
    const auto& max_m = tini2p::under_cast(MaxM);

    if (m < min_m || m > max_m)
      {
        ex.throw_ex<std::logic_error>(
            "invalid number of filter bits: " + std::to_string(m) + ", min: " + std::to_string(min_m)
            + ", max: " + std::to_string(max_m));
      }
  }

  void check_k(const k_t& k, const exception::Exception& ex) const
  {
    const auto& min_k = tini2p::under_cast(MinK);
    const auto& max_k = tini2p::under_cast(MaxK);

    if (k < min_k || k > max_k)
      {
        ex.throw_ex<std::logic_error>(
            "invalid number of hash rounds: " + std::to_string(k) + ", min: " + std::to_string(min_k)
            + ", max: " + std::to_string(max_k));
      }
  }

  n_t n_;
  p_t p_;
  m_t m_;
  k_t k_;
};

/// @class BloomFilter
/// @brief Bloom filter for testing if a member of a set has been seen
/// @detail K number of hash functions approximated with the formula:
///     g(i) = h1(x) + ih2(x) mod m, for i = 0 ... k - 1
///
///     Where h1 and h2 are independent, cryptographically secure hash functions.
///     Here h1 and h2 are separately keyed instantiations of SipHash.
///     Using the above formula, only two applications of the hash function are performed.
///
///     See https://www.eecs.harvard.edu/~michaelm/postscripts/rsa2008.pdf for more information.
class BloomFilter
{
 public:
  using siphash_t = crypto::SipHash;  //< SipHash trait alias
  using params_t = BloomParams;  //< Number bits and hash rounds trait alias
  using filter_t = std::deque<bool>;  //< Filter trait alias
  using positions_t = std::vector<std::uint32_t>; //< Positions trait alias

  /// @brief Default ctor
  BloomFilter() : params_(), h1_key_(), h2_key_(), filter_() {}

  /// @brief Fully-initializing ctor, creates a Bloom filter with n elements and one-in-p false positives
  /// @param n Number of elements in the Bloom filter
  /// @param p One-in-P number of false-positive membership checks
  BloomFilter(params_t::n_t n, params_t::p_t p)
      : params_(std::forward<params_t::n_t>(n), std::forward<params_t::p_t>(p)),
        h1_key_(siphash_t::create_key()),
        h2_key_(siphash_t::create_key()),
        filter_()
  {
    filter_.resize(params_.m());
  }

  /// @brief Copy-ctor
  BloomFilter(const BloomFilter& oth)
      : params_(oth.params_), h1_key_(oth.h1_key_), h2_key_(oth.h2_key_), filter_(oth.filter_)
  {
  }

  /// @brief Move-ctor
  BloomFilter(BloomFilter&& oth)
      : params_(std::move(oth.params_)),
        h1_key_(std::move(oth.h1_key_)),
        h2_key_(std::move(oth.h2_key_)),
        filter_(std::move(oth.filter_))
  {
  }

  /// @brief Forwarding assignment operator
  BloomFilter& operator=(BloomFilter&& oth)
  {
    params_ = std::forward<params_t>(oth.params_);
    h1_key_ = std::forward<siphash_t::key_t>(oth.h1_key_);
    h2_key_ = std::forward<siphash_t::key_t>(oth.h2_key_);

    std::scoped_lock sgd(filter_mutex_);
    filter_ = std::forward<filter_t>(oth.filter_);

    return *this;
  }

  /// @brief Check if an element is present in the Bloom filter
  /// @detail Adds the element to the Bloom filter if no match found
  /// @return One if a match is found, zero if not
  template <std::size_t N>
  std::uint8_t CheckElement(const crypto::FixedSecBytes<N>& element)
  {
    const auto& positions = CalculatePositions(element);

    std::uint8_t match(1);

    {  // lock filter
      std::shared_lock sf(filter_mutex_, std::defer_lock);
      std::scoped_lock sgd(sf);

      // for each position, check if the bit is unset or set
      // any unset bit results in a negative match
      for (const auto& pos : positions)
        match *= static_cast<std::uint8_t>(filter_.at(pos));
    }  // end-filter-lock

    if (match == 0)
      AddElement(positions);

    return match;
  }

  /// @brief Add an element to the Bloom filter
  /// @param element Element to add to the Bloom filter
  template <std::size_t N>
  void AddElement(const crypto::FixedSecBytes<N>& element)
  {
    const auto& positions = CalculatePositions(element);

    AddElement(positions);
  }

  /// @brief Clear the all set Bloom filter bits
  BloomFilter& clear()
  {
    std::scoped_lock sgd(filter_mutex_);

    filter_.clear();
    filter_.resize(params_.m());

    return *this;
  }

  /// @brief Get a const reference to the Bloom filter's number of elements
  const params_t::n_t& n() const noexcept
  {
    return params_.n();
  }

  /// @brief Get a const reference to the Bloom filter's one-in-P number of false-positives
  const params_t::p_t& p() const noexcept
  {
    return params_.p();
  }

  /// @brief Get a const reference to the Bloom filter's number of bits
  const params_t::m_t& m() const noexcept
  {
    return params_.m();
  }

  /// @brief Get a const reference to the Bloom filter's number of hash rounds
  const params_t::k_t& k() const noexcept
  {
    return params_.k();
  }

 private:
  template <std::size_t N>
  positions_t CalculatePositions(const crypto::FixedSecBytes<N>& element) const
  {
    const exception::Exception ex{"BloomFilter", __func__};

    check_key(h1_key_, ex);
    check_key(h2_key_, ex);

    siphash_t::digest_t h1, h2;
    siphash_t::Hash(h1_key_, element, h1);
    siphash_t::Hash(h2_key_, element, h2);

    crypto::BigNum h1_bn(h1), h2_bn(h2), m_bn(params_.m());
    const auto& k = params_.k();
    positions_t positions{};
    positions.reserve(k);

    // g(i) = h1(e) + (i * h2(e)) mod m
    for (std::uint8_t i = 0; i < k; ++i)
      positions.emplace_back(h2_bn.MulAddMod(h1_bn, crypto::BigNum(i), m_bn).uint());

    return positions;
  }

  void AddElement(const positions_t& positions)
  {
    std::scoped_lock sgd(filter_mutex_);
    for (const auto& pos : positions)
      filter_.at(pos) = 1;
  }

  void check_key(const siphash_t::key_t& key, const exception::Exception& ex) const
  {
    if (key.is_zero())
      ex.throw_ex<std::logic_error>("null key.");
  }

  params_t params_;
  siphash_t::key_t h1_key_, h2_key_;

  filter_t filter_;
  std::shared_mutex filter_mutex_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_BLOOM_FILTER_H_
