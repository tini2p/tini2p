/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_X25519_H_
#define SRC_CRYPTO_X25519_H_

#include <sodium.h>

#include "src/crypto/blake.h"
#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/keys.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/rand.h"
#include "src/crypto/sha.h"
#include "src/crypto/signature.h"

namespace tini2p
{
namespace crypto
{
class X25519
{
 protected:
   X25519() = default;  // only allow derived instantiation

 public:
  enum
  {
    KeyLen = 32,
    PublicKeyLen = 32,
    PrivateKeyLen = 32,
    SharedKeyLen = 32,
    SignatureLen = 64,
  };

  struct PublicKey : public Key<PublicKeyLen>
  {
    using base_t = Key<PublicKeyLen>;
    using hasher_t = KeyHasher<PublicKeyLen>;  //< Hasher trait alias

    using pointer = PublicKey*;  //< Non-owning pointer trait alias
    using const_pointer = const PublicKey*;  //< Const non-owning pointer trait alias
    using unique_ptr = std::unique_ptr<PublicKey>;  //< Unique pointer trait alias
    using const_unique_ptr = std::unique_ptr<const PublicKey>;  //< Const unique pointer trait alias
    using shared_ptr = std::shared_ptr<PublicKey>;  //< Shared pointer trait alias
    using const_shared_ptr = std::shared_ptr<const PublicKey>;  //< Const shared pointer trait alias

    PublicKey() : base_t() {}

    PublicKey(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

    PublicKey(const SecBytes& buf) : base_t(buf) {}

    PublicKey(std::initializer_list<std::uint8_t> list) : base_t(list) {}
  };
  
  struct PrivateKey : public Key<PrivateKeyLen>
  {
    using base_t = Key<PrivateKeyLen>;

    PrivateKey() : base_t() {}

    PrivateKey(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

    PrivateKey(const SecBytes& buf) : base_t(buf) {}

    PrivateKey(std::initializer_list<std::uint8_t> list) : base_t(list) {}
  };

  struct SharedKey : public Key<SharedKeyLen>
  {
    using base_t = Key<SharedKeyLen>;

    SharedKey() : base_t() {}

    SharedKey(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

    SharedKey(const SecBytes& buf) : base_t(buf) {}

    SharedKey(std::initializer_list<std::uint8_t> list) : base_t(list) {}

    /// @brief Cast operator to convert to ChaCha20-Poly1305 symmetric key
    explicit operator ChaChaPoly1305::key_t() const
    {
      return ChaChaPoly1305::key_t(base_t::buf_);
    }
  };

  struct Signature : public crypto::Signature<SignatureLen>
  {
    using base_t = crypto::Signature<SignatureLen>;

    Signature() : base_t() {}

    Signature(base_t::buffer_t buf)
        : base_t(std::forward<base_t::buffer_t>(buf))
    {
    }

    Signature(const SecBytes& buf) : base_t(buf) {}

    Signature(std::initializer_list<std::uint8_t> list) : base_t(list) {}
  };

  using curve_t = X25519;  //< Elliptic curve trait alias
  using message_t = SecBytes;  //< Message trait alias
  using ciphertext_t = SecBytes; //< Ciphertext trait alias
  using signature_t = X25519::Signature;  //< Signature trait alias
  using pubkey_t = X25519::PublicKey;  //< Public key trait alias
  using pvtkey_t =  X25519::PrivateKey;  //< Private key trait alias
  using shrkey_t = X25519::SharedKey;  //< Shared key trait alias
  using keypair_t = Keypair<X25519>;  //< Keypair trait alias

  /// @brief Create an X25519 key pair
  /// @return X25519 keypair
  inline static keypair_t create_keys()
  {
    keypair_t k;
    RandBytes(k.pvtkey);
    if(crypto_scalarmult_curve25519_base(k.pubkey.data(), k.pvtkey.data()))
      exception::Exception{"X25519", __func__}.throw_ex<std::runtime_error>(
          "unable to create keys.");

    return k;
  }

  /// @brief Compute public key from an existing private key
  /// @param keys Keypair containing an existing private key
  static void PrivateToPublic(keypair_t& keys)
  {
    const exception::Exception ex{"X25519", __func__};

    if(crypto_scalarmult_curve25519_base(keys.pubkey.data(), keys.pvtkey.data()))
      ex.throw_ex<std::runtime_error>("error deriving public key.");
  }

  /// @brief Comput public key from private key, and compare against existing public key in keypair
  /// @throws Runtime error if public key computation fails
  /// @return Unsigned integer for valid public key (1 valid, 0 invalid)
  static std::uint8_t ValidPublicKey(const keypair_t& keys)
  {
    const exception::Exception ex{"X25519", __func__};

    pubkey_t pubkey;
    if(crypto_scalarmult_curve25519_base(pubkey.data(), keys.pvtkey.data()))
      ex.throw_ex<std::runtime_error>("error deriving public key.");
    
    return keys.pubkey == pubkey;
  }

  /// @brief Diffie-Hellman key exchange over Curve25519
  /// @detail Only calculates the shared secret as the scalar multiplication of the private + public keys.
  ///   For safe use as a key, resulting secret needs to be hashed with an HKDF function. 
  /// @param shrkey Shared key result of DH
  /// @param pvtkey Private key
  /// @param pubkey Public key
  static void DH(Key<KeyLen>& shrkey, const pvtkey_t& pvtkey, const pubkey_t& remote_key)
  {
    const exception::Exception ex{"X25519"};

    if (crypto_scalarmult_curve25519(shrkey.data(), pvtkey.data(), remote_key.data()))
      ex.throw_ex<std::runtime_error>("error computing shared key.");
  }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_X25519_H_
