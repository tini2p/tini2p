/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_SIGNATURE_H_
#define SRC_CRYPTO_SIGNATURE_H_

namespace tini2p
{
namespace crypto
{
/// @brief Base class for signatures
/// @detail Protected ctors to ensure derived instantiation
/// @tparam N Size of the signature
template <std::size_t N>
class Signature
{
 public:
  using buffer_t = FixedSecBytes<N>;  //< Buffer trait alias
  using const_pointer = typename buffer_t::const_pointer;
  using pointer = typename buffer_t::pointer;
  using const_iterator = typename buffer_t::const_iterator;
  using iterator = typename buffer_t::iterator;
  using size_type = typename buffer_t::size_type;

  Signature() : buf_() {}

  Signature(buffer_t buf) : buf_(std::forward<buffer_t>(buf)) {}

  Signature(const SecBytes& buf) : buf_(buf) {}

  Signature(std::initializer_list<std::uint8_t> list) : buf_(list) {}

  const_pointer data() const noexcept
  {
    return buf_.data();
  }

  pointer data() noexcept
  {
    return buf_.data();
  }

  const_iterator begin() const noexcept
  {
    return buf_.begin();
  }

  iterator begin() noexcept
  {
    return buf_.begin();
  }

  const_iterator end() const noexcept
  {
    return buf_.end();
  }

  iterator end() noexcept
  {
    return buf_.end();
  }

  decltype(auto) size() const noexcept
  {
    return buf_.size();
  }

 protected:
  buffer_t buf_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_SIGNATURE_H_
