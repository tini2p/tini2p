/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_TUNNEL_BUILD_REQUEST_H_
#define SRC_CRYPTO_TUNNEL_BUILD_REQUEST_H_

#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/tunnel/nonce.h"

namespace tini2p
{
namespace crypto
{
/// @brief Build Record record and reply key KDF context
const static crypto::KDFContext<crypto::HmacSha256> ECIES_REQUEST_RCRD_CTX(std::string{"RecordReplyGener"});

/// @brief Build Record tunnel layer and randomization key KDF context
const static crypto::KDFContext<crypto::HmacSha256> TUNNEL_LAYER_CTX(std::string{"TunnelLayerRando"});

/// @class TunnelRequest
/// @brief For deriving record and layer keys, and request record encryption
class TunnelRequest
{
 public:
  enum : std::uint16_t
  {
    RecordLen = 528,
    RequestHeaderLen = 48,
    RequestBodyLen = 464,
    RequestCipherLen = 480,
    KeyLen = 32,
    RootInputLen = 64,
    AESKeydataLen = 80,
    ChaChaKeydataLen = 96,
    RecordKeydataLen = 96,
  };

  /// @brief Processing role for the BuildRequestRecord
  enum struct Role : std::uint8_t
  {
    Creator,
    Hop,
  };

  using role_t = Role;
  using curve_t = crypto::X25519;
  using aead_t = crypto::ChaChaPoly1305;
  using aes_t = crypto::AES;
  using chacha_t = crypto::ChaCha20;
  using nonce_t = crypto::TunnelNonce;
  using sha_t = crypto::Sha256;
  using hkdf_t = crypto::HKDF<crypto::HmacSha256>;
  using key_buffer_t = crypto::FixedSecBytes<KeyLen>;

  /// @brief Derive record request and reply keys
  /// @detail Root key and shared secret are used as inputs for deriving layer keys
  /// @returns Record reply key, and key buffers for:
  ///      - reply key
  ///      - root key
  ///      - shared secret
  static std::tuple<aead_t::key_t, key_buffer_t, key_buffer_t, key_buffer_t> DeriveRecordKeys(
      const curve_t::keypair_t& local_keys,
      const curve_t::pubkey_t& remote_key,
      const sha_t::digest_t& ident_hash,
      const role_t& role)
  {
    const exception::Exception ex{"TunnelRequest", __func__};

    curve_t::shrkey_t shared_secret;
    curve_t::DH(shared_secret, local_keys.pvtkey, remote_key);

    crypto::FixedSecBytes<RootInputLen> root_input;

    const auto& hop_key = role == role_t::Creator ? remote_key : local_keys.pubkey;

    // derive the rootKey
    // rootKey = Sha256(sepk || hop_ident_hash)
    tini2p::BytesWriter<crypto::FixedSecBytes<RootInputLen>> writer(root_input);
    writer.write_data(hop_key);
    writer.write_data(ident_hash);

    key_buffer_t root_key;
    crypto::Sha256::Hash(root_key, root_input);

    // derive recordKey and replyKey
    // keydata = HKDF(rootKey, sharedSecret, "RecordReplyGener", 96)
    crypto::FixedSecBytes<RecordKeydataLen> record_keydata;
    hkdf_t::Derive(record_keydata, root_key, shared_secret, ECIES_REQUEST_RCRD_CTX);

    aead_t::key_t record_key;
    key_buffer_t  reply_key;

    tini2p::BytesReader<crypto::FixedSecBytes<RecordKeydataLen>> record_reader(record_keydata);
    record_reader.read_data(root_key);  // update the rootKey, keydata[0:31] 
    record_reader.read_data(record_key);  // AEAD key for record encryption, keydata[32:63]
    record_reader.read_data(reply_key);  // ChaCha20 key for reply record encryption, keydata[64:95]

    return std::make_tuple(
        std::move(record_key), std::move(reply_key), std::move(root_key), std::move(shared_secret.buffer()));
  }

  /// @brief Derive AES tunnel layer keys and reply record IV
  /// @param root_key Root key derived from record key derivation
  /// @param shared_secret Shared DH secret derived from record key derivation
  /// @returns keys buffers for layer and randomization key, and the AES reply record IV
  static std::tuple<key_buffer_t, key_buffer_t, aes_t::iv_t> DeriveAESLayerKeys(
      key_buffer_t root_key,
      key_buffer_t shared_secret)
  {
    // derive tunnel layer and randomization key
    // keydata = HKDF(rootKey, sharedSecret, "TunnelLayerRando", 80)
    crypto::FixedSecBytes<AESKeydataLen> layer_keydata;
    hkdf_t::Derive(layer_keydata, root_key, shared_secret, TUNNEL_LAYER_CTX);

    key_buffer_t layer_key, rand_key;
    aes_t::iv_t reply_iv;

    tini2p::BytesReader<crypto::FixedSecBytes<AESKeydataLen>> layer_reader(layer_keydata);
    layer_reader.read_data(layer_key);  // tunnel layer key, keydata[0:31]
    layer_reader.read_data(rand_key);  // tunnel randomization key, keydata[32:63]
    layer_reader.read_data(reply_iv);  // tunnel reply record IV, keydata[64:79]

    return std::make_tuple(std::move(layer_key), std::move(rand_key), std::move(reply_iv));
  }

  /// @brief Derive ChaCha tunnel layer keys and AEAD send key
  /// @param root_key Root key derived from record key derivation
  /// @param shared_secret Shared DH secret derived from record key derivation
  /// @returns key buffers for:
  ///     - layer key
  ///     - randomization key
  ///     - AEAD send key
  static std::tuple<key_buffer_t, key_buffer_t, key_buffer_t> DeriveChaChaLayerKeys(
      key_buffer_t root_key,
      key_buffer_t shared_secret)
  {
    // derive tunnel layer and randomization key
    // keydata = HKDF(rootKey, sharedSecret, "TunnelLayerRando", 96)
    crypto::FixedSecBytes<ChaChaKeydataLen> layer_keydata;
    hkdf_t::Derive(layer_keydata, root_key, shared_secret, TUNNEL_LAYER_CTX);

    key_buffer_t layer_key, rand_key, send_key;

    tini2p::BytesReader<crypto::FixedSecBytes<ChaChaKeydataLen>> layer_reader(layer_keydata);
    layer_reader.read_data(layer_key);  // tunnel layer key, keydata[0:31]
    layer_reader.read_data(rand_key);  // tunnel randomization key, keydata[32:63]
    layer_reader.read_data(send_key);  // tunnel send aead key, keydata[64:95]

    return std::make_tuple(std::move(layer_key), std::move(rand_key), std::move(send_key));
  }

  /// @brief AEAD encrypt own BuildRequestRecord buffer
  template <class TRecord>
  static void EncryptRecord(const aead_t::key_t& record_key, TRecord& record)
  {
    sha_t::digest_t ad;
    sha_t::Hash(ad, record_key);

    auto record_start = record.data() + tini2p::under_cast(RequestHeaderLen);

    aead_t::AEADEncrypt(
        record_key,
        nonce_t(0),
        record_start,
        tini2p::under_cast(RequestBodyLen),
        ad,
        record_start,
        tini2p::under_cast(RequestCipherLen));
  }

  /// @brief AEAD decrypt own BuildRequestRecord buffer
  template <class TRecord>
  static void DecryptRecord(const aead_t::key_t& record_key, TRecord& record)
  {
    sha_t::digest_t ad;
    sha_t::Hash(ad, record_key);

    auto record_start = record.data() + tini2p::under_cast(RequestHeaderLen);

    aead_t::AEADDecrypt(
        record_key,
        nonce_t(0),
        record_start,
        tini2p::under_cast(RequestBodyLen),
        ad,
        record_start,
        tini2p::under_cast(RequestCipherLen));
  }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_TUNNEL_BUILD_REQUEST_H_
