/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_TUNNEL_CHACHA_H_
#define SRC_CRYPTO_TUNNEL_CHACHA_H_

#include "src/crypto/chacha20.h"
#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/sha.h"

#include "src/crypto/tunnel/build_key_info.h"
#include "src/crypto/tunnel/nonce.h"

namespace tini2p
{
namespace crypto
{
/// @brief Zero-length associated data for AEAD encryption
static const crypto::FixedSecBytes<0> ZERO_AD{};

/// @class TunnelChaCha
/// @brief Tunnel layer encryption using ChaCha
class TunnelChaCha
{
 public:
  enum
  {
    TunnelIDLen = 4,
    RecordLen = 528,
    ReplyBodyLen = 512,
    ReplyCipherLen = 528,
    MessageLen = 1028,
    MessageHeaderLen = 20,
    MessageBodyLen = 992,
    MessageCipherLen = 1008,
  };

  /// @struct Nonces
  /// @brief Container for return nonces
  struct Nonces
  {
    TunnelNonce tunnel_nonce;
    TunnelNonce obfs_nonce;

    std::uint8_t operator==(const Nonces& oth) const
    {
      const auto& tun_eq = static_cast<std::uint8_t>(tunnel_nonce == oth.tunnel_nonce);
      const auto& obfs_eq = static_cast<std::uint8_t>(obfs_nonce == oth.obfs_nonce);

      return (tun_eq * obfs_eq);
    }
  };

  using curve_t = crypto::X25519;  //< Curve trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< ChaCha20-Poly1305-AEAD trait alias
  using chacha_t = crypto::ChaCha20;  //< ChaCha20 trait alias
  using sha_t = crypto::Sha256;  //< Sha256 trait alias
  using sym_t = chacha_t;  //< Symmetric crypto trait alias
  using key_info_t = crypto::BuildKeyInfo<chacha_t>;  //< Tunnel build key information trait alias
  using nonce_t = crypto::TunnelNonce;  //< Tunnel nonce trait alias
  using nonces_t = Nonces;  //< Return tunnel nonces trait alias
  using message_t = crypto::SecBytes;  //< Tunnel message buffer trait alias
  using record_t = crypto::FixedSecBytes<RecordLen>;  //< Build record buffer trait alias

  TunnelChaCha() : keys_() {}

  /// @brief Fully initializing ctor, creates a TunnelChaCha with needed keys
  /// @param nonce_key ChaCha20 key for encrypting tunnel and obfs nonces
  /// @param layer_key ChaCha20 key for encrypting tunnel message
  /// @param hop_key ChaCha20-Poly1305 key for AEAD decrypting the received tunnel message
  /// @param next_hop_key ChaCha20-Poly1305 key for AEAD encrypting the tunnel message for the next hop
  TunnelChaCha(key_info_t key_info)
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    key_info.check_keys(ex);

    keys_ = std::forward<key_info_t>(key_info);
  }

  /// @brief Delete copy-ctor to make TunnelChaCha move-only
  TunnelChaCha(const TunnelChaCha&) = delete;

  /// @brief Delete copy assignment operator to make TunnelChaCha move-only
  void operator=(const TunnelChaCha&) = delete;

  /// @brief Move-ctor
  TunnelChaCha(TunnelChaCha&& oth)
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    oth.keys_.check_keys(ex);

    keys_ = std::move(oth.keys_);
  }

  /// @brief Move-assignment operator
  TunnelChaCha& operator=(TunnelChaCha&& oth)
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    oth.keys_.check_keys(ex);

    keys_ = std::move(oth.keys_);

    return *this;
  }

  /// @brief AEAD encrypt own BuildRequestRecord buffer
  template <class TRecord>
  void EncryptRequestRecord(TRecord& record)
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    check_record(record, ex);

    if (keys_.derive_required())
      keys_.DeriveKeys(key_info_t::role_t::Creator);

    crypto::TunnelRequest::EncryptRecord(keys_.record_key(), record);
  }

  /// @brief Encrypt own BuildResponseRecord buffer
  template <class TRecord>
  void EncryptReplyRecord(TRecord& record) const
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    check_record(record, ex);

    const auto& reply_key = keys_.reply_key();
    sha_t::digest_t ad;
    sha_t::Hash(ad, reply_key);

    aead_t::AEADEncrypt(
        reply_key,
        nonce_t(0),
        record.data(),
        tini2p::under_cast(ReplyBodyLen),
        ad,
        record.data(),
        tini2p::under_cast(ReplyCipherLen));
  }

  /// @brief Encrypt other BuildRequestRecord and BuildResponseRecord buffers
  template <class TRecord>
  void EncryptRecord(TRecord& record, const std::uint8_t& pos, const std::uint8_t& num_records) const
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    check_record(record, ex);

    chacha_t::StreamXOR(
        record.data(), tini2p::under_cast(RecordLen), nonce_t(1 + pos + num_records), keys_.reply_key());
  }

  /// @brief AEAD decrypt own BuildRequestRecord buffer
  template <class TRecord>
  void DecryptRequestRecord(TRecord& record)
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    check_record(record, ex);

    if (keys_.derive_required())
      keys_.DeriveKeys(key_info_t::role_t::Hop);

    crypto::TunnelRequest::DecryptRecord(keys_.record_key(), record);
  }

  /// @brief Decrypt own BuildResponseRecord buffer
  template <class TRecord>
  void DecryptReplyRecord(TRecord& record) const
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    check_record(record, ex);

    const auto & reply_key = keys_.reply_key();
    sha_t::digest_t ad;
    sha_t::Hash(ad, reply_key);

    aead_t::AEADDecrypt(
        reply_key,
        nonce_t(0),
        record.data(),
        tini2p::under_cast(ReplyBodyLen),
        ad,
        record.data(),
        tini2p::under_cast(ReplyCipherLen));
  }

  /// @brief Decrypt other build request/reply record buffers
  template <class TRecord>
  void DecryptRecord(TRecord& record, const std::uint8_t& pos, const std::uint8_t& num_records) const
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    check_record(record, ex);

    chacha_t::StreamXOR(
        record.data(), tini2p::under_cast(RecordLen), nonce_t(1 + pos + num_records), keys_.reply_key());
  }

  /// @brief For templated use with TunnelAES
  template <class TRecord>
  void DecryptRecord(TRecord&) const
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    ex.throw_ex<std::runtime_error>("wrong layer encryption");
  }

  /// @brief Encrypt the tunnel IV and message body
  /// @param message Tunnel message to encrypt
  /// @returns Encrypted tunnel IV
  nonces_t Encrypt(message_t& message)
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    check_message(message, ex);

    auto msg_data = message.data();

    // Set beginning and length of the tunnel nonce and obfs nonce
    auto tun_nonce_ptr = msg_data + tini2p::under_cast(TunnelIDLen);
    const auto& nonce_len = tini2p::under_cast(nonce_t::NonceLen);
    auto obfs_nonce_ptr = tun_nonce_ptr + nonce_len;

    // Check that nonces are unique
    check_nonces(tun_nonce_ptr, obfs_nonce_ptr, ex);

    // ChaCha20 encrypt tunnel nonce before use
    const auto& rand_key = keys_.rand_key();
    chacha_t::StreamXOR(tun_nonce_ptr, nonce_len, obfs_nonce_ptr, nonce_len, rand_key);

    // ChaCha20 encrypt obfs nonce after use
    chacha_t::StreamXOR(obfs_nonce_ptr, nonce_len, tun_nonce_ptr, nonce_len, rand_key);

    nonces_t ret{{tun_nonce_ptr, nonce_len}, {obfs_nonce_ptr, nonce_len}};

    // ChaCha20 encrypt the tunnel message with the tunnel nonce
    chacha_t::StreamXOR(
        msg_data + tini2p::under_cast(MessageHeaderLen),
        tini2p::under_cast(MessageBodyLen),
        ret.tunnel_nonce,
        keys_.layer_key());

    return ret;
  }

  /// @brief AEAD encrypt the tunnel message for the next hop
  /// @detail Call after symmetrically encrypting the message with ChaCha20
  void EncryptAEAD(message_t& message)
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    check_message(message, ex);

    auto msg_data = message.data();

    // Read the encrypted tunnel nonce from the buffer
    nonce_t tunnel_nonce(msg_data + tini2p::under_cast(TunnelIDLen), tini2p::under_cast(nonce_t::NonceLen));

    // Set the beginning of the message body
    auto msg_ptr = msg_data + tini2p::under_cast(MessageHeaderLen);

    // ChaCha20-Poly1305-AEAD encrypt the tunnel message with the tunnel nonce and hop key
    aead_t::AEADEncrypt(
        keys_.send_key(),
        tunnel_nonce,
        msg_ptr,
        tini2p::under_cast(MessageBodyLen),
        ZERO_AD,
        msg_ptr,
        tini2p::under_cast(MessageCipherLen));
  }

  /// @brief Decrypt the tunnel IV and message body
  /// @param message Tunnel message to decrypt
  /// @returns Decrypted tunnel IV
  nonces_t Decrypt(message_t& message)
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    check_message(message, ex);

    auto msg_data = message.data();

    // Set beginning and length of the tunnel nonce and obfs nonce
    auto tun_nonce_ptr = msg_data + tini2p::under_cast(TunnelIDLen);
    const auto& nonce_len = tini2p::under_cast(nonce_t::NonceLen);
    auto obfs_nonce_ptr = tun_nonce_ptr + nonce_len;

    // Check that nonces are unique
    check_nonces(tun_nonce_ptr, obfs_nonce_ptr, ex);

    nonce_t tunnel_nonce(tun_nonce_ptr, nonce_len);

    // ChaCha20 decrypt the tunnel message with the tunnel nonce
    chacha_t::StreamXOR(
        msg_data + tini2p::under_cast(MessageHeaderLen),
        tini2p::under_cast(MessageBodyLen),
        tunnel_nonce,
        keys_.layer_key());

    // ChaCha20 decrypt obfs nonce after use (recovers previous obfs nonce)
    const auto& rand_key = keys_.rand_key();
    chacha_t::StreamXOR(obfs_nonce_ptr, nonce_len, tun_nonce_ptr, nonce_len, rand_key);

    // ChaCha20 decrypt tunnel nonce after use (recovers previous tunnel nonce)
    chacha_t::StreamXOR(tun_nonce_ptr, nonce_len, obfs_nonce_ptr, nonce_len, rand_key);

    return {{tun_nonce_ptr, nonce_len}, {obfs_nonce_ptr, nonce_len}};
  }

  /// @brief AEAD decrypt the incoming tunnel message
  /// @detail Call before:
  ///     - ChaCha20 encrypting the message, as a tunnel participant
  ///     - ChaCha20 decrypting the first layer with the hop's layer key, as an inbound endpoint
  void DecryptAEAD(message_t& message)
  {
    const exception::Exception ex{"TunnelChaCha", __func__};

    check_message(message, ex);

    auto msg_data = message.data();

    // Read the received tunnel nonce from the buffer
    nonce_t tunnel_nonce(msg_data + tini2p::under_cast(TunnelIDLen), tini2p::under_cast(nonce_t::NonceLen));

    // Set the beginning of the message
    auto msg_ptr = msg_data + tini2p::under_cast(MessageHeaderLen);

    // ChaCha20-Poly1305-AEAD decrypt the tunnel message with the tunnel nonce and receive key
    aead_t::AEADDecrypt(
        keys_.receive_key(),
        tunnel_nonce,
        msg_ptr,
        tini2p::under_cast(MessageBodyLen),
        ZERO_AD,
        msg_ptr,
        tini2p::under_cast(MessageCipherLen));
  }

  /// @brief Get a const reference to the hop's key information
  const key_info_t& key_info() const noexcept
  {
    return keys_;
  }

  /// @brief Get a const reference to the hop's key information
  key_info_t& key_info() noexcept
  {
    return keys_;
  }

  /// @brief Create random nonces
  static nonces_t create_nonces()
  {
    return nonces_t{nonce_t::create_random(), nonce_t::create_random()};
  }

  /// @brief Create random nonces, alias for templated use
  static nonces_t create_randomizer()
  {
    return create_nonces();
  }

 private:
  void check_nonces(
      const std::uint8_t* tun_nonce_ptr,
      const std::uint8_t* obfs_nonce_ptr,
      const exception::Exception& ex) const
  {
    const auto& nonce_len = tini2p::under_cast(nonce_t::NonceLen);

    if (nonce_t(tun_nonce_ptr, nonce_len) == nonce_t(obfs_nonce_ptr, nonce_len))
      ex.throw_ex<std::logic_error>("nonces must be unique.");
  }

  void check_nonces(const nonce_t& tunnel_nonce, const nonce_t& obfs_nonce, const exception::Exception& ex) const
  {
    if (tunnel_nonce == obfs_nonce)
      ex.throw_ex<std::logic_error>("nonces must be unique.");
  }

  void check_message(const message_t& message, const exception::Exception& ex) const
  {
    const auto& message_len = message.size();
    const auto& exp_len = tini2p::under_cast(MessageLen);

    if (message_len != exp_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid message length: " + std::to_string(message_len) + ", expected: " + std::to_string(exp_len));
      }
  }

  template <class TRecord>
  void check_record(const TRecord& record, const exception::Exception& ex) const
  {
    const auto& exp_record_len = tini2p::under_cast(RecordLen);
    const auto& record_len = record.size();

    if (record_len != exp_record_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid record length: " + std::to_string(record_len) + ", expected: " + std::to_string(exp_record_len));
      }
  }

  key_info_t keys_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_TUNNEL_CHACHA_H_
