/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_TUNNEL_AES_H_
#define SRC_CRYPTO_TUNNEL_AES_H_

#include "src/crypto/aes.h"

#include "src/crypto/tunnel/build_key_info.h"

namespace tini2p
{
namespace crypto
{
/// @class TunnelAES
/// @brief Tunnel layer encryption using AES
class TunnelAES
{
 public:
  enum
  {
    TunnelIDLen = 4,
    RecordLen = 528,
    ReplyBodyLen = 512,
    ReplyCipherLen = 528,
    MessageLen = 1028,
    MessageHeaderLen = 20,
    MessageBodyLen = 1008,
    MessageCipherLen = 1008,
  };

  using curve_t = crypto::X25519;  //< Curve trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD trait alias
  using sha_t = crypto::Sha256;  //< Sha256 trait alias
  using aes_t = crypto::AES;  //< AES trait alias
  using sym_t = aes_t;  //< Symmetric crypto trait alias
  using key_info_t = crypto::BuildKeyInfo<aes_t>;  //< Tunnel build key information trait alias
  using record_t = crypto::FixedSecBytes<RecordLen>;  //< Tunnel build request/reply record trait alias
  using message_t = crypto::SecBytes;  //< Tunnel message trait alias

  TunnelAES() : iv_aes_(), msg_aes_(), record_aes_(), keys_() {}

  /// @brief Fully-initializing ctor, creates TunnelAES from tunnel build key information
  TunnelAES(key_info_t key_info)
      : iv_aes_(key_info.rand_key()),
        msg_aes_(key_info.layer_key()),
        record_aes_(key_info.reply_key(), key_info.reply_iv())
  {
    const exception::Exception ex{"TunnelAES", __func__};

    key_info.check_keys(ex);

    keys_ = std::forward<key_info_t>(key_info);
  }

  /// @brief Delete copy-ctor to make TunnelAES move-only
  TunnelAES(const TunnelAES&) = delete;

  /// @brief Delete copy assignment operator to make TunnelAES move-only
  void operator=(const TunnelAES&) = delete;

  /// @brief Move-ctor
  TunnelAES(TunnelAES&& oth)
      : iv_aes_(oth.keys_.rand_key()),
        msg_aes_(oth.keys_.layer_key()),
        record_aes_(oth.keys_.reply_key(), oth.keys_.reply_iv())
  {
    const exception::Exception ex{"TunnelAES", __func__};

    oth.keys_.check_keys(ex);

    keys_ = std::move(oth.keys_);
  }

  /// @brief Move-assignment operator
  TunnelAES& operator=(TunnelAES&& oth)
  {
    const exception::Exception ex{"TunnelAES", __func__};

    oth.keys_.check_keys(ex);

    iv_aes_.rekey(oth.keys_.rand_key());
    msg_aes_.rekey(oth.keys_.layer_key());
    record_aes_.rekey(oth.keys_.reply_key(), oth.keys_.reply_iv());

    keys_ = std::move(oth.keys_);

    return *this;
  }

  /// @brief AEAD encrypt own BuildRequestRecord buffer
  template <class TRecord>
  void EncryptRequestRecord(TRecord& record)
  {
    const exception::Exception ex{"TunnelAES", __func__};

    check_record(record, ex);

    if (keys_.derive_required())
      keys_.DeriveKeys(key_info_t::role_t::Creator);

    crypto::TunnelRequest::EncryptRecord(keys_.record_key(), record);
  }

  /// @brief AES256-CBC encrypt the tunnel's BuildRequestRecords and BuildResponseRecords
  /// @detail Alias for templated use
  template <class TRecord>
  void EncryptReplyRecord(TRecord& record)
  {
    const exception::Exception ex{"TunnelAES", __func__};

    check_record(record, ex);

    record_aes_.Encrypt(record.data(), tini2p::under_cast(RecordLen));
  }

  /// @brief AES256-CBC encrypt the tunnel's BuildRequestRecords and BuildResponseRecords
  template <class TRecord>
  void EncryptRecord(TRecord& record)
  {
    const exception::Exception ex{"TunnelAES", __func__};

    check_record(record, ex);

    record_aes_.Encrypt(record.data(), tini2p::under_cast(RecordLen));
  }

  /// @brief AEAD decrypt own BuildRequestRecord buffer
  template <class TRecord>
  void DecryptRequestRecord(TRecord& record)
  {
    const exception::Exception ex{"TunnelAES", __func__};

    check_record(record, ex);

    if (keys_.derive_required())
      keys_.DeriveKeys(key_info_t::role_t::Hop);

    crypto::TunnelRequest::DecryptRecord(keys_.record_key(), record);
  }

  /// @brief AES256-CBC decrypt the tunnel's BuildRequestRecords and BuildResponseRecords
  template <class TRecord>
  void DecryptRecord(TRecord& record)
  {
    const exception::Exception ex{"TunnelAES", __func__};

    check_record(record, ex);

    record_aes_.Decrypt(record.data(), tini2p::under_cast(RecordLen));
  }

  /// @brief For templated use with TunnelChaCha
  template <class TRecord>
  void DecryptRecord(TRecord&, const std::uint8_t&, const std::uint8_t&) const
  {
    const exception::Exception ex{"TunnelAES", __func__};

    ex.throw_ex<std::runtime_error>("wrong layer encryption");
  }

  /// @brief Encrypt the tunnel IV and message body
  /// @param message Tunnel message to encrypt
  /// @returns Encrypted tunnel IV
  aes_t::iv_t Encrypt(message_t& message)
  {
    const exception::Exception ex{"TunnelAES", __func__};

    check_message(message, ex);

    auto msg_data = message.data();

    // Set beginning and length of the tunnel IV
    auto iv_ptr = msg_data + tini2p::under_cast(TunnelIDLen);
    const auto& iv_len = tini2p::under_cast(aes_t::IVLen);

    // ECB encrypt IV before use
    iv_aes_.EncryptECB(iv_ptr, iv_len);

    // Set encrypted IV, and CBC encrypt tunnel message
    msg_aes_.set_iv(iv_ptr, iv_len);
    msg_aes_.Encrypt(msg_data + tini2p::under_cast(MessageHeaderLen), tini2p::under_cast(MessageCipherLen));

    // ECB encrypt IV after use
    iv_aes_.EncryptECB(iv_ptr, iv_len);

    return aes_t::iv_t(iv_ptr, iv_len);
  }

  /// @brief For templated use with TunnelChaCha
  void EncryptAEAD(message_t&)
  {
    const exception::Exception ex{"TunnelAES", __func__};

    ex.throw_ex<std::logic_error>("invalid layer encryption");
  }

  /// @brief Decrypt the tunnel IV and message body
  /// @param message Tunnel message to decrypt
  /// @returns Decrypted tunnel IV
  aes_t::iv_t Decrypt(message_t& message)
  {
    const exception::Exception ex{"TunnelAES", __func__};

    check_message(message, ex);

    auto msg_data = message.data();

    // Set beginning and length of the tunnel IV
    auto iv_ptr = msg_data + tini2p::under_cast(TunnelIDLen);
    const auto& iv_len = tini2p::under_cast(aes_t::IVLen);

    // ECB decrypt IV before use
    iv_aes_.DecryptECB(iv_ptr, iv_len);

    // Set decrypted IV, and CBC decrypt tunnel message
    msg_aes_.set_iv(iv_ptr, iv_len);
    msg_aes_.Decrypt(msg_data + tini2p::under_cast(MessageHeaderLen), tini2p::under_cast(MessageCipherLen));

    // ECB decrypt IV after use
    iv_aes_.DecryptECB(iv_ptr, iv_len);

    return aes_t::iv_t(iv_ptr, iv_len);
  }

  /// @brief For templated use with TunnelChaCha
  void DecryptAEAD(message_t&)
  {
    const exception::Exception ex{"TunnelAES", __func__};

    ex.throw_ex<std::logic_error>("invalid layer encryption");
  }

  /// @brief Get a const reference to the hop's key information
  const key_info_t& key_info() const noexcept
  {
    return keys_;
  }

  /// @brief Get a non-const reference to the hop's key information
  key_info_t& key_info() noexcept
  {
    return keys_;
  }

  /// @brief Create a random IV
  static aes_t::iv_t create_iv()
  {
    return aes_t::create_random_iv();
  }

  /// @brief Create a random IV, alias for templated use
  static aes_t::iv_t create_randomizer()
  {
    return aes_t::create_random_iv();
  }

 private:
  void check_message(const message_t& message, const exception::Exception& ex) const
  {
    const auto& message_len = message.size();
    const auto& exp_len = tini2p::under_cast(MessageLen);

    if (message_len != exp_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid message length: " + std::to_string(message_len) + ", expected: " + std::to_string(exp_len));
      }
  }

  template <class TRecord>
  void check_record(const TRecord& record, const exception::Exception& ex) const
  {
    const auto& exp_record_len = tini2p::under_cast(RecordLen);
    const auto& record_len = record.size();

    if (record_len != exp_record_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid record length: " + std::to_string(record_len) + ", expected: " + std::to_string(exp_record_len));
      }
  }

  aes_t iv_aes_;
  aes_t msg_aes_;
  aes_t record_aes_;
  key_info_t keys_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_TUNNEL_AES_H_
