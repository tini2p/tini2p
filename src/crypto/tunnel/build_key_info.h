/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_TUNNEL_BUILD_KEY_INFO_H_
#define SRC_CRYPTO_TUNNEL_BUILD_KEY_INFO_H_

#include "src/crypto/aes.h"
#include "src/crypto/chacha20.h"
#include "src/crypto/chacha_poly1305.h"
#include "src/crypto/hkdf.h"
#include "src/crypto/kdf_context.h"
#include "src/crypto/sha.h"
#include "src/crypto/x25519.h"

#include "src/crypto/tunnel/build_request.h"

namespace tini2p
{
namespace crypto
{
/// @class BuildKeyInfo
/// @brief Class for storage and derivation of Tunnel Build Request/Reply Record key information
/// @tparam TSym Symmetric crypto type for reply, layer, and randomization keys
// TODO(tini2p): generalize for ElGamal and ECIES-X25519, currently ECIES-X25519 only
template <
    class TSym,
    typename = std::enable_if_t<std::is_same<TSym, crypto::AES>::value || std::is_same<TSym, crypto::ChaCha20>::value>>
class BuildKeyInfo
{
 public:
  enum
  {
    IdentHashLen = 32,  //< Identity hash length
    TruncIdentLen = 16,  //< Truncated Identity hash length, see spec
    RootKeyInputLen = 64,  //< Participant Static Key (32) + Participant Identity Hash (32), see spec 
    RecordKeydataLen = 96,  //< Root key (32) + Record key (32) + Reply key (32)
    AESLayerKeydataLen = 80,  //< Layer key (32) + Randomization key (32) + Reply IV (16)
    ChaChaLayerKeydataLen = 96,  //< Layer key (32) + Randomization key (32) + Send key (32)
    KeyLen = 32,
  };

  /// @brief Requirement flags
  enum struct Reqs : std::uint8_t
  {
    NotRequired,
    Required,
  };

  using sym_t = TSym;  //< Symmetric crypto trait alias
  using sym_key_t = typename TSym::key_t;  //< Symmetric key trait alias
  using curve_t = crypto::X25519;  //< EC curve trait alias
  using aead_t = crypto::ChaChaPoly1305;  //< AEAD trait alias
  using aes_t = crypto::AES;  //< AES trait alias
  using chacha_t = crypto::ChaCha20;  //< ChaCha20 trait alias
  using hkdf_t = crypto::HKDF<crypto::HmacSha256>;  //< HKDF-HMAC-Sha256 trait alias
  using ident_hash_t = crypto::Sha256::digest_t;  //< RouterIdentity trait alias
  using trunc_ident_t = crypto::FixedSecBytes<TruncIdentLen>;  //< Truncated Identity hash trait alias
  using root_key_input_t = crypto::FixedSecBytes<RootKeyInputLen>;  //< Root key input trait alias
  using record_keydata_t = crypto::FixedSecBytes<RecordKeydataLen>;  //< Record keydata trait alias
  using aes_layer_keydata_t = crypto::FixedSecBytes<AESLayerKeydataLen>;  //< AES layer keydata trait alias
  using chacha_layer_keydata_t = crypto::FixedSecBytes<ChaChaLayerKeydataLen>;  //< ChaCha layer keydata trait alias
  using key_buffer_t = crypto::FixedSecBytes<KeyLen>;  //< Key buffer trait alias
  using tunnel_request_t = crypto::TunnelRequest;  //< Tunnel request crypto trait alias
  using role_t = tunnel_request_t::Role;  //< Role trait alias

  /// @brief Default ctor, creates empty BuildKeyInfo
  /// @note Useful for hops to deserialize encrypted BuildRequestRecords
  BuildKeyInfo()
      : creator_keys_(),
        hop_keys_(),
        record_key_(),
        reply_key_(),
        layer_key_(),
        rand_key_(),
        recv_key_(),
        send_key_(),
        derive_req_(Reqs::Required)
  {
  }

  /// @brief Fully initializing ctor, creates a tunnel's OutboundGateway BuildKeyInfo
  /// @detail Creates a new ephemeral keypair for the tunnel creator
  /// @param key Remote static public key of the tunnel hop
  /// @param ident_hash Identity hash of the tunnel hop
  /// @throws Invalid argument for null public key
  /// @throws Invalid argument for null Identity hash
  /// @throws Invalid argument for null truncated Identity hash
  BuildKeyInfo(curve_t::pubkey_t hop_key, ident_hash_t ident_hash)
      : creator_keys_(curve_t::create_keys()),
        record_key_(),
        reply_key_(),
        layer_key_(),
        rand_key_(),
        recv_key_(),
        send_key_(),
        derive_req_(Reqs::Required)
  {
    const exception::Exception ex{"BuildKeyInfo", __func__};

    check_key(hop_key, "hop", ex);
    check_ident_hash(ident_hash, ex);

    hop_keys_.pubkey = std::forward<curve_t::pubkey_t>(hop_key);
    ident_ = std::forward<ident_hash_t>(ident_hash);

    DeriveKeys(role_t::Creator);
  }

  /// @brief Fully initializing ctor, creates a AES tunnel hop's BuildKeyInfo
  /// @detail Caller reads the receive key from BuildRequestRecord options
  /// @param keys Tunnel hop's static keypair
  /// @throws Invalid argument for null hop static private key
  BuildKeyInfo(curve_t::pubkey_t creator_key, curve_t::keypair_t hop_keys, ident_hash_t ident_hash)
      : record_key_(), reply_key_(), layer_key_(), rand_key_(), derive_req_(Reqs::Required)
  {
    const exception::Exception ex{"BuildKeyInfo", __func__};

    if (hop_keys.pvtkey.is_zero())
      ex.throw_ex<std::invalid_argument>("null hop static private key.");

    check_key(creator_key, "creator", ex);
    check_ident_hash(ident_hash, ex);

    creator_keys_.pubkey = std::forward<curve_t::pubkey_t>(creator_key);
    hop_keys_ = std::forward<curve_t::keypair_t>(hop_keys);
    ident_ = std::forward<ident_hash_t>(ident_hash);

    if (hop_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(hop_keys_);

    DeriveKeys(role_t::Hop);
  }

  /// @brief Fully initializing ctor, creates a tunnel creator's BuildKeyInfo for a given ChaCha hop
  /// @detail Creates a new ephemeral keypair for the tunnel creator
  /// @param key Remote static public key of the tunnel hop
  /// @param ident_hash Identity hash of the tunnel hop
  /// @param receive_key AEAD key for the hop to decrypt incoming tunnel messages
  /// @throws Invalid argument for null public key
  /// @throws Invalid argument for null Identity hash
  /// @throws Invalid argument for null truncated Identity hash
  BuildKeyInfo(curve_t::pubkey_t hop_key, ident_hash_t ident_hash, aead_t::key_t receive_key)
      : creator_keys_(curve_t::create_keys()),
        record_key_(),
        reply_key_(),
        layer_key_(),
        rand_key_(),
        send_key_(),
        derive_req_(Reqs::Required)
  {
    const exception::Exception ex{"BuildKeyInfo", __func__};

    check_key(hop_key, "hop", ex);
    check_key(receive_key, "receive", ex);
    check_ident_hash(ident_hash, ex);

    hop_keys_.pubkey = std::forward<curve_t::pubkey_t>(hop_key);
    recv_key_ = std::forward<aead_t::key_t>(receive_key);
    ident_ = std::forward<ident_hash_t>(ident_hash);

    DeriveKeys(role_t::Creator);
  }

  /// @brief Fully initializing ctor, creates a ChaCha tunnel hop's BuildKeyInfo
  /// @detail Caller reads the receive key from BuildRequestRecord options
  /// @param keys Tunnel hop's static keypair
  /// @param receive_key AEAD key for the hop to decrypt incoming tunnel messages
  /// @throws Invalid argument for null hop static private key
  BuildKeyInfo(
      curve_t::pubkey_t creator_key,
      curve_t::keypair_t hop_keys,
      ident_hash_t ident_hash,
      aead_t::key_t receive_key)
      : record_key_(), reply_key_(), layer_key_(), rand_key_(), send_key_(), derive_req_(Reqs::Required)
  {
    const exception::Exception ex{"BuildKeyInfo", __func__};

    if (hop_keys.pvtkey.is_zero())
      ex.throw_ex<std::invalid_argument>("null hop static private key.");

    check_key(creator_key, "creator", ex);
    check_key(receive_key, "receive", ex);
    check_ident_hash(ident_hash, ex);

    creator_keys_.pubkey = std::forward<curve_t::pubkey_t>(creator_key);
    hop_keys_ = std::forward<curve_t::keypair_t>(hop_keys);
    recv_key_ = std::forward<aead_t::key_t>(receive_key);
    ident_ = std::forward<ident_hash_t>(ident_hash);

    if (hop_keys_.pubkey.is_zero())
      curve_t::PrivateToPublic(hop_keys_);

    DeriveKeys(role_t::Hop);
  }

  /// @brief Create a BuildKeyInfo from reply key, root key, and shared secret buffers
  /// @detail For deferred layer key generation when decrypting request records as a TransitTunnel hop
  BuildKeyInfo(key_buffer_t reply_key, const key_buffer_t& root_key, const key_buffer_t& shared_secret)
  {
    const exception::Exception ex{"BuildKeyInfo", __func__};

    check_key(reply_key, "reply", ex);
    check_key(root_key, "root", ex);
    check_key(shared_secret, "shared secret", ex);

    reply_key_ = std::forward<key_buffer_t>(reply_key);

    if (std::is_same<sym_t, aes_t>::value)
      std::tie(layer_key_, rand_key_, reply_iv_) = tunnel_request_t::DeriveAESLayerKeys(root_key, shared_secret);
    else
      std::tie(layer_key_, rand_key_, send_key_) = tunnel_request_t::DeriveChaChaLayerKeys(root_key, shared_secret);
  }

  /// @brief Get a const reference to the creator ephemeral keypair
  const curve_t::keypair_t& creator_keys() const noexcept
  {
    return creator_keys_;
  }

  /// @brief Get a const reference to the creator ephemeral public key
  const curve_t::pubkey_t& creator_pubkey() const noexcept
  {
    return creator_keys_.pubkey;
  }

  /// @brief Set the tunnel creator's ephemeral public key
  void creator_pubkey(curve_t::pubkey_t key)
  {
    const exception::Exception ex{"BuildKeyInfo", __func__};

    check_key(key, "creator", ex);

    creator_keys_.pubkey = std::forward<curve_t::pubkey_t>(key);

    derive_req_ = Reqs::Required;
  }

  /// @brief Get a const reference to the hop static keypair
  const curve_t::keypair_t& hop_keys() const noexcept
  {
    return hop_keys_;
  }

  /// @brief Get a const reference to the hop static public key
  const curve_t::pubkey_t& hop_pubkey() const noexcept
  {
    return hop_keys_.pubkey;
  }

  /// @brief Get a const reference to the request record key
  const aead_t::key_t& record_key() const noexcept
  {
    return record_key_;
  }

  /// @brief Get a const reference to the reply record key
  const sym_key_t& reply_key() const noexcept
  {
    return reply_key_;
  }

  /// @brief Get a const reference to the reply record IV
  const aes_t::iv_t& reply_iv() const noexcept
  {
    return reply_iv_;
  }

  /// @brief Get a const reference to the tunnel layer key
  const sym_key_t& layer_key() const noexcept
  {
    return layer_key_;
  }

  /// @brief Get a const reference to the tunnel randomization key
  const sym_key_t& rand_key() const noexcept
  {
    return rand_key_;
  }

  /// @brief Get a const reference to the tunnel AEAD receive key
  const aead_t::key_t& receive_key() const noexcept
  {
    return recv_key_;
  }

  /// @brief Set the tunnel AEAD receive key
  void receive_key(aead_t::key_t recv_key)
  {
    const exception::Exception ex{"BuildKeyInfo", __func__};

    check_keys(record_key_, reply_key_, layer_key_, rand_key_, recv_key, send_key_, ex);

    recv_key_ = std::forward<aead_t::key_t>(recv_key);
  }

  /// @brief Get a const reference to the tunnel AEAD send key
  const aead_t::key_t& send_key() const noexcept
  {
    return send_key_;
  }

  /// @brief Get a const reference to the Identity hash of the tunnel hop
  const ident_hash_t& ident_hash() const noexcept
  {
    return ident_;
  }

  /// @brief Set the hop's truncated Identity hash
  void ident_hash(ident_hash_t ident)
  {
    const exception::Exception ex{"BuildKeyInfo", __func__};

    check_ident_hash(ident, ex);

    ident_ = std::forward<ident_hash_t>(ident);
    derive_req_ = Reqs::Required;
  }

  /// @brief Get the truncated Identity hash of the tunnel hop
  trunc_ident_t truncated_ident() const noexcept
  {
    return static_cast<trunc_ident_t>(ident_);
  }

  /// @brief Truncate the Identity hash of a tunnel hop
  static trunc_ident_t TruncateIdentHash(
      const ident_hash_t& ident_hash,
      const exception::Exception& ex = {"BuildKeyInfo", "TruncateIdentHash"})
  {
    check_ident_hash(ident_hash, ex);

    return static_cast<trunc_ident_t>(ident_hash);
  }

  /// @brief Check the validity of the BuildKeyInfo
  void check_key_info(const role_t& role, const exception::Exception& ex) const
  {
    if (role == role_t::Creator)
      {
        check_keypair(creator_keys_, ex);
        check_key(hop_keys_.pubkey, "hop", ex);
      }
    else if (role == role_t::Hop)
      {
        check_keypair(hop_keys_, ex);
        check_key(creator_keys_.pubkey, "creator", ex);
      }
    else
      ex.throw_ex<std::invalid_argument>("invalid processing role.");

    check_ident_hash(ident_, ex);
  }

  /// @brief Derive keys for record and layer cryptography
  /// @param role Processing role, tunnel creator or hop
  void DeriveKeys(const role_t& role)
  {
    const exception::Exception ex{"BuildKeyInfo", __func__};

    check_key_info(role, ex);

    key_buffer_t root_key, shared_secret;
    const auto& local_keys = role == role_t::Creator ? creator_keys_ : hop_keys_;
    const auto& remote_key = role == role_t::Creator ? hop_keys_.pubkey : creator_keys_.pubkey;

    std::tie(record_key_, reply_key_, root_key, shared_secret) =
        tunnel_request_t::DeriveRecordKeys(local_keys, remote_key, ident_, role);

    if (std::is_same<sym_t, aes_t>::value)
      std::tie(layer_key_, rand_key_, reply_iv_) = tunnel_request_t::DeriveAESLayerKeys(root_key, shared_secret);
    else if (std::is_same<sym_t, chacha_t>::value)
      std::tie(layer_key_, rand_key_, send_key_) = tunnel_request_t::DeriveChaChaLayerKeys(root_key, shared_secret);
    else
      ex.throw_ex<std::logic_error>("invalid symmetric crypto.");

    derive_req_ = Reqs::NotRequired;
  }

  /// @brief Get whether deriving keys is required
  std::uint8_t derive_required() const noexcept
  {
    return static_cast<std::uint8_t>(derive_req_ == Reqs::Required);
  }

  /// @brief Equality comparison with another BuildKeyInfo
  std::uint8_t operator==(const BuildKeyInfo& oth) const
  {  // attempt constant-time comparison
    const auto& create_eq = static_cast<std::uint8_t>(creator_keys_.pubkey == oth.creator_keys_.pubkey);
    const auto& hop_eq = static_cast<std::uint8_t>(hop_keys_.pubkey == oth.hop_keys_.pubkey);
    const auto& ident_eq = static_cast<std::uint8_t>(ident_ == oth.ident_);
    const auto& rec_eq = static_cast<std::uint8_t>(record_key_ == oth.record_key_);
    const auto& rep_eq = static_cast<std::uint8_t>(reply_key_ == oth.reply_key_);
    const auto& lay_eq = static_cast<std::uint8_t>(layer_key_ == oth.layer_key_);
    const auto& rand_eq = static_cast<std::uint8_t>(rand_key_ == oth.rand_key_);
    const auto& recv_eq = static_cast<std::uint8_t>(recv_key_ == oth.recv_key_);
    const auto& send_eq = static_cast<std::uint8_t>(send_key_ == oth.send_key_);

    return (create_eq * hop_eq * ident_eq * rec_eq * rep_eq * lay_eq * rand_eq * recv_eq * send_eq);
  }

  /// @brief Check if the BuildKeyInformation has valid keys
  void check_keys(const exception::Exception& ex)
  {
    check_keys(record_key_, reply_key_, layer_key_, rand_key_, recv_key_, send_key_, ex);
  }

 private:
  template <class TKey>
  void check_key(const TKey& key, const std::string& key_name, const exception::Exception& ex) const
  {
    if (key.is_zero())
      ex.throw_ex<std::invalid_argument>("null " + key_name + " key.");
  }

  void check_keys(
      const aead_t::key_t& record_key,
      const sym_key_t& reply_key,
      const sym_key_t& layer_key,
      const sym_key_t& rand_key,
      const aead_t::key_t& receive_key,
      const aead_t::key_t& send_key,
      const exception::Exception& ex)
  {
    check_key(reply_key, "reply", ex);
    check_key(layer_key, "layer", ex);
    check_key(rand_key, "randomization", ex);

    if (std::is_same<sym_t, chacha_t>::value)
      check_key(send_key, "send", ex);

    if (std::is_same<sym_t, chacha_t>::value && !record_key.is_zero() && record_key == receive_key)
      ex.throw_ex<std::logic_error>("record key and receive key must be unique.");

    if (std::is_same<sym_t, chacha_t>::value && record_key == send_key)
      ex.throw_ex<std::logic_error>("record key and send key must be unique.");

    if (reply_key == layer_key)
      ex.throw_ex<std::logic_error>("reply key and layer key must be unique.");

    if (reply_key == rand_key)
      ex.throw_ex<std::logic_error>("reply key and randomization key must be unique.");

    if (std::is_same<sym_t, chacha_t>::value && receive_key == send_key)
      ex.throw_ex<std::logic_error>("receive key and send key must be unique.");

    const auto& record_buf = record_key.buffer();
    const auto& reply_buf = reply_key.buffer();
    const auto& layer_buf = layer_key.buffer();
    const auto& rand_buf = rand_key.buffer();
    const auto& receive_buf = receive_key.buffer();
    const auto& send_buf = send_key.buffer();

    // Compare reply key and AEAD keys
    if (reply_buf == record_buf)
      ex.throw_ex<std::logic_error>("reply key and record key must be unique.");

    if (std::is_same<sym_t, chacha_t>::value && reply_buf == receive_buf)
      ex.throw_ex<std::logic_error>("reply key and receive key must be unique.");

    if (std::is_same<sym_t, chacha_t>::value && reply_buf == send_buf)
      ex.throw_ex<std::logic_error>("reply key and send key must be unique.");

    // Compare layer key and AEAD keys
    if (layer_buf == record_buf)
      ex.throw_ex<std::logic_error>("layer key and record key must be unique.");

    if (std::is_same<sym_t, chacha_t>::value && layer_buf == receive_buf)
      ex.throw_ex<std::logic_error>("layer key and receive key must be unique.");

    if (std::is_same<sym_t, chacha_t>::value && layer_buf == send_buf)
      ex.throw_ex<std::logic_error>("layer key and send key must be unique.");

    // Compare randomization key and AEAD keys
    if (rand_buf == record_buf)
      ex.throw_ex<std::logic_error>("randomization key and record key must be unique.");

    if (std::is_same<sym_t, chacha_t>::value && rand_buf == receive_buf)
      ex.throw_ex<std::logic_error>("randomization key and receive key must be unique.");

    if (std::is_same<sym_t, chacha_t>::value && rand_buf == send_buf)
      ex.throw_ex<std::logic_error>("randomization key and send key must be unique.");
  }

  static void check_ident_hash(const ident_hash_t& ident, const exception::Exception& ex)
  {
    if (ident.is_zero())
      ex.throw_ex<std::invalid_argument>("null Identity hash.");

    if (static_cast<trunc_ident_t>(ident).is_zero())
      ex.throw_ex<std::invalid_argument>("null truncated Identity hash.");
  }

  void check_keypair(const curve_t::keypair_t& keys, const exception::Exception& ex) const
  {
    if (keys.pvtkey.is_zero())
      ex.throw_ex<std::logic_error>("null private key.");

    if (keys.pubkey.is_zero())
      ex.throw_ex<std::logic_error>("null public key.");

    if (!curve_t::ValidPublicKey(keys))
      ex.throw_ex<std::logic_error>("invalid public key.");
  }

  curve_t::keypair_t creator_keys_, hop_keys_;
  ident_hash_t ident_;
  aead_t::key_t record_key_;
  sym_key_t reply_key_, layer_key_, rand_key_;
  aes_t::iv_t reply_iv_;
  aead_t::key_t recv_key_, send_key_;

  Reqs derive_req_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_TUNNEL_BUILD_KEY_INFO_H_
