/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_TUNNEL_NONCE_H_
#define SRC_CRYPTO_TUNNEL_NONCE_H_

#include "src/bytes.h"
#include "src/crypto/rand.h"
#include "src/crypto/sec_bytes.h"

namespace tini2p
{
namespace crypto
{
/// @class TunnelNonce
/// @brief Counter-based, nonce for ChaCha20 tunnel layer encryption
class TunnelNonce
{
 public:
  enum
  {
    NonceLen = 8,
  };

  using buffer_t = FixedSecBytes<NonceLen>;  //< Buffer trait alias
  using uint_t = std::uint64_t;  //< Uint trait alias

  TunnelNonce() : buf_{}, n_(0) {}

  /// @brief Create a nonce from a given integer
  /// @brief n Nonce value to set
  explicit TunnelNonce(uint_t n) : buf_{}, n_(std::forward<uint_t>(n))
  {
    tini2p::write_bytes(buf_.data(), n_);
  }

  /// @brief Create a nonce from a secure buffer
  explicit TunnelNonce(buffer_t buf) : buf_{std::forward<buffer_t>(buf)}, n_()
  {
    tini2p::read_bytes(buf_.data(), n_);
  }

  /// @brief Create a nonce from a C-like buffer
  TunnelNonce(const std::uint8_t* buf, const std::size_t buf_len)
  {
    const exception::Exception ex{"TunnelNonce", __func__};

    const auto& nonce_len = tini2p::under_cast(NonceLen);
    tini2p::check_cbuf(buf, buf_len, nonce_len, nonce_len, ex); 

    std::copy_n(buf, buf_len, buf_.data());
    tini2p::read_bytes(buf_.data(), n_);
  }

  /// @brief Set a new nonce value
  /// @param n New nonce value to set
  void operator()(const uint_t& n)
  {
    n_ = n;
    tini2p::write_bytes(buf_.data(), n_);
  }

  /// @brief Pre-increment the nonce by one
  /// @return Nonce value after increment
  uint_t operator++()
  {
    tini2p::write_bytes(buf_.data(), ++n_);
    return n_;
  }

  /// @brief Post-increment the nonce by one
  /// @return Nonce value before increment
  uint_t operator++(int)
  {
    uint_t tmp(n_);
    tini2p::write_bytes(buf_.data(), ++n_);
    return tmp;
  }

  /// @brief Get the nonce as an unsigned int
  explicit operator uint_t() const noexcept
  {
    return n_;
  }

  /// @brief Get the nonce as a buffer
  explicit operator const buffer_t&() const noexcept
  {
    return buf_;
  }

  /// @brief Get a const pointer to the nonce buffer
  const std::uint8_t* data() const noexcept
  {
    return buf_.data();
  }

  /// @brief Get a non-const pointer to the nonce buffer
  std::uint8_t* data() noexcept
  {
    return buf_.data();
  }

  /// @brief Get the size of the nonce buffer
  buffer_t::size_type size() const
  {
    return buf_.size();
  }

  /// @brief Equality comparison with another Nonce
  std::uint8_t operator==(const TunnelNonce& oth) const
  {
    return static_cast<std::uint8_t>(n_ == oth.n_);
  }

  /// @brief Equality comparison with another Nonce
  std::uint8_t operator==(TunnelNonce& oth)
  {
    return static_cast<std::uint8_t>(n_ == oth.n_);
  }

  /// @brief Get whether the nonce is all zeroes
  std::uint8_t is_zero() const
  {
    return buf_.is_zero();
  }

  /// @brief Swap with another Nonce
  TunnelNonce& swap(TunnelNonce& oth)
  {
    buf_.swap(oth.buf_);
    n_ = oth.n_;

    return *this;
  }

  /// @brief Create a random TunnelNonce
  static TunnelNonce create_random()
  {
    return TunnelNonce(crypto::RandBytes<NonceLen>());
  }

 private:
  buffer_t buf_;
  uint_t n_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_TUNNEL_NONCE_H_
