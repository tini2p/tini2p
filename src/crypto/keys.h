/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_KEYS_H_
#define SRC_CRYPTO_KEYS_H_

#include "src/crypto/sec_bytes.h"

namespace tini2p
{
namespace crypto
{
/// @brief Base class for keys
/// @tparam N Size of the key
template <std::size_t N>
class Key
{
 public:
  using buffer_t = FixedSecBytes<N>;  //< Buffer trait alias
  using const_pointer = typename buffer_t::const_pointer;
  using pointer = typename buffer_t::pointer;
  using const_iterator = typename buffer_t::const_iterator;
  using iterator = typename buffer_t::iterator;
  using size_type = typename buffer_t::size_type;

  Key() : buf_() {}

  Key(buffer_t buf) : buf_(std::forward<buffer_t>(buf)) {}

  Key(const SecBytes& buf) : buf_(buf) {}

  Key(std::initializer_list<std::uint8_t> list) : buf_(list) {}

  const_pointer data() const noexcept
  {
    return buf_.data();
  }

  pointer data() noexcept
  {
    return buf_.data();
  }

  const_iterator begin() const noexcept
  {
    return buf_.begin();
  }

  iterator begin() noexcept
  {
    return buf_.begin();
  }

  const_iterator end() const noexcept
  {
    return buf_.end();
  }

  iterator end() noexcept
  {
    return buf_.end();
  }

  decltype(auto) size() const noexcept
  {
    return buf_.size();
  }

  const buffer_t& buffer() const noexcept
  {
    return buf_;
  }

  buffer_t& buffer() noexcept
  {
    return buf_;
  }

  decltype(auto) is_zero() const noexcept
  {
    return buf_.is_zero();
  }

  decltype(auto) operator[](size_type idx) const
  {
    return buf_[idx];
  }

  decltype(auto) operator[](size_type idx)
  {
    return buf_[idx];
  }

  std::uint8_t operator==(const Key& oth) const
  {
    return buf_ == oth.buf_;
  }

  std::uint8_t operator==(Key& oth)
  {
    return buf_ == oth.buf_;
  }

  bool operator<(const Key<N>& oth) const
  {
    return buf_ < oth.buf_;
  }

  Key<N> operator^(const Key<N>& oth) const
  {
    return buf_ ^ oth.buf_;
  }

  void operator^=(const Key<N>& oth)
  {
    buf_ ^= oth.buf_;
  }

  /// @brief Swap with another Key
  Key& swap(Key& oth)
  {
    buf_.swap(oth.buf_);
    return *this;
  }

  /// @brief Zero the key
  void zero()
  {
    buf_.zero();
  }

 protected:
  buffer_t buf_;
};

template <std::size_t N>
struct KeyHasher
{
  std::size_t operator()(const Key<N>& k) const
  {
    std::size_t s(0), i(0);
    for (const auto& kb : k)
      s ^= std::hash<std::uint8_t>()(kb) << i++;
    return s;
  }
};

template <class CryptoImpl>
struct Keypair
{
  typename CryptoImpl::pubkey_t pubkey;
  typename CryptoImpl::pvtkey_t pvtkey;

  /// @brief Equality comparison with another keypair
  std::uint8_t operator==(const Keypair& oth) const
  {  // attempt constant-time comparison
    const auto& pub_eq = static_cast<std::uint8_t>(pubkey == oth.pubkey);
    const auto& pvt_eq = static_cast<std::uint8_t>(pvtkey == oth.pvtkey);

    return (pub_eq * pvt_eq);
  }
};

template <class CryptoImpl>
struct KeyIV
{
  typename CryptoImpl::key_t key;
  typename CryptoImpl::iv_t iv;
};

template <class CryptoImpl>
struct KeyNonce
{
  typename CryptoImpl::key_t key;
  typename CryptoImpl::nonce_t nonce;
};

template <class PublicKeyImpl, class SharedKeyImpl>
struct DHKeys : public Keypair<PublicKeyImpl>, public KeyNonce<SharedKeyImpl>
{
  DHKeys() : Keypair<PublicKeyImpl>(), KeyNonce<SharedKeyImpl>() {}
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_KEYS_H_
