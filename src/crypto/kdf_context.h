/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_KDF_CONTEXT_H_
#define SRC_CRYPTO_KDF_CONTEXT_H_

namespace tini2p
{
namespace crypto
{
/// @class KDFContext
/// @brief Class for HKDF hashing functions
template <class Hasher>
class KDFContext
{
 public:
  using string_t = std::string;
  using buffer_t = std::vector<std::uint8_t>;

  KDFContext() : ctx_(Hasher::DefaultContextLen) {}

  explicit KDFContext(const string_t& info) : ctx_()
  {
    context(info);
  }

  /// @brief Create an KDF_CONTEXT context from an input buffer
  template <std::size_t N>
  explicit KDFContext(const std::array<std::uint8_t, N>& info) : ctx_{}
  {
    context(info);
  }

  explicit KDFContext(const buffer_t& info) : ctx_{}
  {
    context(info);
  }

  /// @brief Set the KDF_CONTEXT context
  /// @param info String containing the KDF_CONTEXT context info
  /// @detail Only copy up to MaxLen bytes from input
  template <class Buffer>
  void context(const Buffer& info)
  {
    if (!ctx_.empty())
      ctx_.clear();

    if (info.size() <= Hasher::MaxContextLen)
      {
        ctx_.insert(ctx_.begin(), info.begin(), info.end());
        ctx_.resize(info.size());
      }
    else
      {
        const auto info_begin = info.begin();
        ctx_.insert(
            ctx_.begin(), info_begin, info_begin + Hasher::MaxContextLen);
      }
  }

  buffer_t::size_type size() const noexcept
  {
    return ctx_.size();
  }

  explicit operator string_t() const
  {
    return std::string(ctx_.begin(), ctx_.end());
  }

  explicit operator const buffer_t&() const noexcept
  {
    return ctx_;
  }

 private:
  buffer_t ctx_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_KDF_CONTEXT_H_
