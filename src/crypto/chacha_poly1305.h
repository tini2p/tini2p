/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_CHACHA_POLY1305_H_
#define SRC_CRYPTO_CHACHA_POLY1305_H_

#include <sodium.h>

#include "src/crypto/chacha20.h"
#include "src/crypto/keys.h"
#include "src/crypto/nonce.h"
#include "src/crypto/poly1305.h"

#include "src/crypto/tunnel/nonce.h"

namespace tini2p
{
namespace crypto
{
/// @class ChaChaPoly1305
/// @brief ChaCha20-Poly1305-AEAD encryption class
class ChaChaPoly1305
{
 public:
  enum : std::uint8_t
  {
    KeyLen = 32,
    MACLen = Poly1305::DigestLen,
  };

  using key_t = ChaCha20::Key;  //< Key trait alias
  using nonce_t = Nonce;  //< Nonce trait alias
  using tunnel_nonce_t = TunnelNonce;  //< Tunnel nonce trait alias
  using mac_t = Poly1305::digest_t;  //< MAC alias trait

  /// @brief AEAD encrypt a message using IETF-AEAD-Chacha20-Poly1305
  /// @tparam Plaintext Plaintext buffer type
  /// @tparam Ciphertext Ciphertext buffer type
  /// @tparam AD Associated data buffer type
  /// @param key Encryption key
  /// @param n Public nonce
  /// @param message Message to encrypt
  /// @param ad Additional data for AEAD
  /// @param ciphertext Buffer for the encryption result: ciphertext || Poly1305MAC
  template <class Message, class Ciphertext, class AD>
  static void
  AEADEncrypt(const key_t& key, const nonce_t& n, const Message& message, const AD& ad, Ciphertext& ciphertext)
  {
    const exception::Exception ex{"ChaChaPoly1305", __func__};

    unsigned long long message_size = message.size();
    if (ciphertext.size() < message_size + Poly1305::DigestLen)
      ciphertext.resize(message_size + Poly1305::DigestLen);

    unsigned long long ciphertext_size = ciphertext.size();
    Encrypt(key, n, message.data(), message_size, ad, ciphertext.data(), ciphertext_size, ex);

    // resize ciphertext to actual length written
    ciphertext.resize(ciphertext_size);
  }

  /// @brief AEAD encrypt a message using IETF-AEAD-Chacha20-Poly1305
  /// @detail Message buffer and ciphertext buffer may overlap.
  ///     Ciphertext buffer must be large enought to contain encrypted ciphertext and Poly1305 MAC
  /// @tparam AD Associated data buffer type
  /// @param key Encryption key
  /// @param n Public nonce
  /// @param msg_ptr Pointer to message buffer to encrypt
  /// @param msg_len Length of the message buffer
  /// @param ad Additional data for AEAD
  /// @param ciph_ptr Pointer to ciphertext buffer for the encryption result: ciphertext || Poly1305MAC
  template <
      class TNonce,
      class AD,
      typename = std::enable_if_t<std::is_same<TNonce, nonce_t>::value || std::is_same<TNonce, tunnel_nonce_t>::value>>
  static void AEADEncrypt(
      const key_t& key,
      const TNonce& n,
      const std::uint8_t* msg_ptr,
      const std::size_t msg_len,
      const AD& ad,
      std::uint8_t* ciph_ptr,
      const std::size_t ciph_len)
  {
    const exception::Exception ex{"ChaChaPoly1305", __func__};

    unsigned long long ciphertext_size = ciph_len;
    Encrypt(key, n, msg_ptr, msg_len, ad, ciph_ptr, ciphertext_size, ex);
  }

  /// @brief AEAD encrypt a message using IETF-AEAD-Chacha20-Poly1305
  /// @tparam Plaintext a plaintext buffer type
  /// @tparam Ciphertext a ciphertext buffer type
  /// @param key Encryption key
  /// @param n Public nonce
  /// @param message Message to encrypt
  /// @param ad Additional data for AEAD
  /// @param ciphertext Buffer for the encryption result: ciphertext || Poly1305MAC
  template <class Message, class Ciphertext, class AD>
  static void
  AEADDecrypt(const key_t& key, const nonce_t& n, Message& message, const AD& ad, const Ciphertext& ciphertext)
  {
    const exception::Exception ex{"ChaChaPoly1305", __func__};

    unsigned long long ciphertext_size = ciphertext.size();
    unsigned long long message_size = ciphertext_size - Poly1305::DigestLen;

    if (message.size() < message_size)
      message.resize(message_size);

    Decrypt(key, n, message.data(), message_size, ad, ciphertext.data(), ciphertext_size, ex);

    // resize message to actual length written
    message.resize(message_size);
  }

  /// @brief AEAD decrypt a message using IETF-AEAD-Chacha20-Poly1305
  /// @detail Message buffer and ciphertext buffer may overlap.
  ///     Ciphertext buffer must be large enought to contain encrypted ciphertext and Poly1305 MAC
  /// @tparam TNonce Nonce type (full or tunnel nonce)
  /// @tparam AD Associated data buffer type
  /// @param key Encryption key
  /// @param n Public nonce
  /// @param msg_ptr Pointer to message buffer to encrypt
  /// @param msg_len Length of the message buffer
  /// @param ad Additional data for AEAD
  /// @param ciph_ptr Pointer to ciphertext buffer for the encryption result: ciphertext || Poly1305MAC
  template <
      class TNonce,
      class AD,
      typename = std::enable_if_t<std::is_same<TNonce, nonce_t>::value || std::is_same<TNonce, tunnel_nonce_t>::value>>
  static void AEADDecrypt(
      const key_t& key,
      const TNonce& n,
      std::uint8_t* msg_ptr,
      const std::size_t msg_len,
      const AD& ad,
      const std::uint8_t* ciph_ptr,
      const std::size_t ciph_len)
  {
    const exception::Exception ex{"ChaChaPoly1305", __func__};

    unsigned long long message_size = msg_len;
    Decrypt(key, n, msg_ptr, message_size, ad, ciph_ptr, ciph_len, ex);
  }

  /// @brief Create a random key
  static key_t create_key()
  {
    return ChaCha20::create_key();
  }

 private:
  template <
      class TNonce,
      class AD,
      typename = std::enable_if_t<std::is_same<TNonce, nonce_t>::value || std::is_same<TNonce, tunnel_nonce_t>::value>>
  static void Encrypt(
      const key_t& key,
      const TNonce& n,
      const std::uint8_t* msg_ptr,
      const std::size_t msg_len,
      const AD& ad,
      std::uint8_t* ciph_ptr,
      unsigned long long& ciph_len,
      const exception::Exception& ex)
  {
    const auto& cipher_mac_len = msg_len + tini2p::under_cast(Poly1305::DigestLen);
    if (ciph_len < cipher_mac_len)
      {
        ex.throw_ex<std::invalid_argument>(
            "ciphertext buffer too small to contain encrypted plaintext and MAC, buffer length: "
            + std::to_string(ciph_len) + ", ciphertext + MAC length: " + std::to_string(cipher_mac_len));
      }

    nonce_t nonce{};
    std::copy_n(n.data(), n.size(), nonce.data());

    if (const int err = crypto_aead_chacha20poly1305_ietf_encrypt(
            ciph_ptr,
            &ciph_len,
            msg_ptr,
            msg_len,
            ad.data(),
            ad.size(),
            nullptr,  // secret nonce (unused)
            nonce.data(),
            key.data()))
      {
        ex.throw_ex<std::runtime_error>("error encrypting message: " + std::to_string(err));
      }
  }

  template <
      class TNonce,
      class AD,
      typename = std::enable_if_t<std::is_same<TNonce, nonce_t>::value || std::is_same<TNonce, tunnel_nonce_t>::value>>
  static void Decrypt(
      const key_t& key,
      const TNonce& n,
      std::uint8_t* msg_ptr,
      unsigned long long& msg_len,
      const AD& ad,
      const std::uint8_t* ciph_ptr,
      const std::size_t ciph_len,
      const exception::Exception& ex)
  {
    const auto& plaintext_len = ciph_len - tini2p::under_cast(Poly1305::DigestLen);
    if (msg_len < plaintext_len)
      {
        ex.throw_ex<std::invalid_argument>(
            "message buffer too small to contain the plaintext, message length: " + std::to_string(msg_len)
            + ", plaintext length: " + std::to_string(plaintext_len));
      }

    nonce_t nonce{};
    std::copy_n(n.data(), n.size(), nonce.data());

    if (const int err = crypto_aead_chacha20poly1305_ietf_decrypt(
            msg_ptr,
            &msg_len,
            nullptr,  // secret nonce (unused)
            ciph_ptr,
            ciph_len,
            ad.data(),
            ad.size(),
            nonce.data(),
            key.data()))
      {
        ex.throw_ex<std::runtime_error>("error decrypting message: " + std::to_string(err));
      }
  }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_CHACHA_POLY1305_H_
