/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_HKDF_H_
#define SRC_CRYPTO_HKDF_H_

#include <sodium.h>

#include "src/bytes.h"

#include "src/exception/exception.h"

#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"

#include "src/crypto/kdf_context.h"

namespace tini2p
{
namespace crypto
{
/// @class HKDF
/// @brief Implementation of HKDF RFC 5689
template <class Hasher>
class HKDF
{
 public:
  enum
  {
    MinKeyLen = 32,
    MaxKeyLen = 64,
  };

  using key_material_t = SecBytes;  //< Key material trait alias
  using context_t = KDFContext<Hasher>;  //< Context trait alias
  using salt_t = typename Hasher::salt_t;  //< Salt trait alias
  using key_t = typename Hasher::key_t;  //< HKDF key trait alias

  /// @brief Generate a new subkey from input key material
  /// @detail KDF context can optionally be empty
  /// @param key_out Output buffer for the resulting subkey
  /// @param salt Salt for generating the pseudo-random key (optional)
  /// @param ctx HKDF context string for unique KDF applications (optional)
  /// @param key_material Input key material buffer
  /// @throw Length errors for invalid key lengths
  template <class TOutKey, class TSalt = salt_t, class TInKey>
  static void Derive(TOutKey& key_out, const TSalt& salt, const TInKey& key_in, const context_t& ctx = context_t())
  {
    Derive(
        reinterpret_cast<std::uint8_t*>(key_out.data()),
        key_out.size(),
        reinterpret_cast<const std::uint8_t*>(salt.data()),
        salt.size(),
        reinterpret_cast<const std::uint8_t*>(key_in.data()),
        key_in.size(),
        ctx);
  }

 private:
  static void Derive(
      std::uint8_t* key_out,
      const std::size_t key_out_len,
      const std::uint8_t* salt_ptr,
      const std::size_t salt_len,
      const std::uint8_t* key_in,
      const std::size_t key_in_len,
      const context_t& ctx = context_t())
  {
    const exception::Exception ex{"HKDF"};

    if (key_in_len < Hasher::MinKeyMaterialLen || key_in_len > Hasher::MaxKeyMaterialLen)
      ex.throw_ex<std::invalid_argument>("invalid input key material length.");

    if (key_out_len < Hasher::MinKeyMaterialLen || key_out_len > Hasher::MaxKeyMaterialLen)
      ex.throw_ex<std::invalid_argument>("invalid output key material length.");

    // derive the pseudo-random key
    key_t prk;
    Hasher::Hash(prk, key_in, key_in_len, salt_ptr, salt_len);

    // expand the key material for a number of rounds, and write to the output buffer
    const auto& digest_len = tini2p::under_cast(Hasher::DigestLen);
    const auto& ko_len = static_cast<std::uint16_t>(key_out_len);
    const auto& rounds = (ko_len / digest_len) + static_cast<std::uint8_t>((ko_len % digest_len) != 0);
    ExpandKeyMaterial(key_out, key_out_len, prk, rounds, ctx);
  }

  static void ExpandKeyMaterial(
      std::uint8_t* key_out,
      const std::size_t key_out_len,
      const key_t& prk,
      const std::uint8_t rounds,
      const context_t& ctx)
  {
    std::vector<typename Hasher::digest_t> digests(rounds);
    const auto ctx_size = ctx.size();
    key_material_t out_ikm(Hasher::DigestLen + ctx_size + 1);
    tini2p::BytesWriter<key_material_t> oikm_writer(out_ikm);

    const std::uint8_t* oikm_ptr(nullptr);
    std::size_t oikm_size(0);

    const std::uint8_t* prk_ptr = prk.data();
    const auto prk_len = prk.size();

    // TODO(tini2p): should this be run for constant set of MAX_ROUNDS for constant-time?
    //    `MAX_ROUNDS - rounds` would do dummy calculations to consume constant resources.
    //     TBD
    // T(0) = empty-string
    // T(N) = H(prk, T(N-1) || info || byte(N))
    // digests(0) = T(1)
    for (std::uint8_t round = 0; round < rounds; ++round)
      {
        using ctx_buf_t = typename context_t::buffer_t;

        if (!round)
          {  // T(0) is empty string, skip it here and when hashing
            oikm_writer.skip_bytes(Hasher::DigestLen);
            oikm_writer.write_data(static_cast<ctx_buf_t>(ctx));
            oikm_ptr = out_ikm.data() + Hasher::DigestLen;
            oikm_size = out_ikm.size() - Hasher::DigestLen;
          }
        else
          {
            oikm_writer.write_data(digests[round - 1]);  // previous round digest, see HKDF spec
            oikm_writer.skip_bytes(ctx_size);  // already wrote info in T(0)
            oikm_ptr = out_ikm.data();
            oikm_size = out_ikm.size();
          }
        oikm_writer.write_bytes<std::uint8_t>(round + 1);  // next round byte, see HKDF spec

        Hasher::Hash(digests[round], oikm_ptr, oikm_size, prk_ptr, prk_len);
        oikm_writer.reset();  // reset writer to beginning of the buffer
      }

    // TODO(tini2p): should this be run for constant set of MAX_ROUNDS for constant-time?
    //    `MAX_ROUNDS - rounds` would do dummy calculations to consume constant resources.
    //     TBD
    // write output key len bytes from expanded key material to the output buffer
    std::size_t written(0), rem(key_out_len);
    const auto is_short = static_cast<std::uint8_t>(key_out_len < rounds * Hasher::DigestLen);
    const auto last_round = rounds - 1;
    for (std::uint8_t round = 0; round < rounds; ++round)
      {
        const std::uint8_t* digest_it = digests[round].data();

        if (is_short == 1 && round == last_round)
          std::copy_n(digest_it, rem, key_out + written);
        else
          std::copy_n(digest_it, Hasher::DigestLen, key_out + written);

        written += Hasher::DigestLen;
        rem -= Hasher::DigestLen;
      }
  }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_HKDF_H_
