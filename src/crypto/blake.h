/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_BLAKE_H_
#define SRC_CRYPTO_BLAKE_H_

#include <sodium.h>

#include "src/bytes.h"

#include "src/exception/exception.h"

#include "src/crypto/kdf_context.h"
#include "src/crypto/rand.h"
#include "src/crypto/sec_bytes.h"

namespace tini2p
{
namespace crypto
{
/// @class Blake2b
class Blake2b
{
 public:
  enum
  {
    DigestLen = 32,
    SaltLen = 16,
    KeyLen = 32,
    MinKeyOutLen = 16,
    MaxKeyOutLen = 64,
    MinKeyMaterialLen = 0,
    MaxKeyMaterialLen = 64,
    DefaultContextLen = 8,
    MaxContextLen = 16,  //< CString[16], based on libsodium
  };

  using salt_t = FixedSecBytes<SaltLen>;  // Salt trait alias
  using key_material_t = SecBytes;  //< Key material trait alias
  using data_t = SecBytes;  //< Data trait alias
  using digest_t = FixedSecBytes<DigestLen>;  //< Digest trait alias
  using context_t = KDFContext<Blake2b>;  //< Context trait alias
  using key_t = FixedSecBytes<KeyLen>;  //< Key trait alias

  /// @brief Blake2b hash with no salt or context
  /// @param key_out Output key material
  /// @param data_ptr Input data buffer pointer
  /// @param data_len Size of the input data buffer
  /// @param key_ptr Key buffer pointer
  /// @param key_len Size of the key buffer
  template <std::size_t N>
  static void Hash(
      FixedSecBytes<N>& key_out,
      const std::uint8_t* data_ptr,
      const std::size_t data_len,
      const std::uint8_t* key_ptr,
      const std::size_t key_len)
  {
    KeyedHash(key_out, data_ptr, data_len, key_ptr, key_len);
  }

  /// @brief Keyed Blake2b-256 hash
  /// @param digest Digest result
  /// @param data Input data to hash
  /// @param key Key for hashing
  template <class TData>
  static void Hash(digest_t& digest, const TData& data, const key_t key)
  {
    KeyedHash(digest, data.data(), data.size(), key.data(), key.size());
  }

  /// @brief Generate a new subkey from a given master key
  /// @tparam N Size of the fixed-length output buffer
  /// @param key_out Output buffer for the resulting subkey
  /// @param nonce Counter-based nonce
  /// @param ctx Blake2b context string for unique KDF applications
  /// @param data Input data to hash
  /// @param key_in Master key buffer
  /// @throw Length errors for invalid key lengths
  template <std::size_t N>
  static void Hash(
      FixedSecBytes<N>& key_out,
      const std::uint8_t* data_ptr,
      const std::size_t data_len,
      const key_t& key_in,
      const salt_t& salt = {},
      const context_t& ctx = context_t())
  {
    Hash(
        key_out.data(),
        N,
        data_ptr,
        data_len,
        key_in.data(),
        KeyLen,
        salt,
        ctx);
  }

  template <std::size_t N>
  static void Hash(
      FixedSecBytes<N>& key_out,
      key_t::const_pointer key_in,
      const key_t::size_type key_in_len,
      const salt_t& salt = {},
      const context_t& ctx = context_t())
  {
    Hash(
        key_out.data(),
        key_out.size(),
        nullptr,
        0,
        key_in,
        key_in_len,
        salt,
        ctx);
  }

  /// @brief Generate a new subkey from a given master key
  /// @param key_out Output buffer for the resulting subkey
  /// @param nonce Counter-based nonce
  /// @param ctx Blake2b context string for unique KDF applications
  /// @param data Input data to hash
  /// @param key_in Master key buffer
  /// @throw Length errors for invalid key lengths
  static void Hash(
      key_material_t& key_out,
      const data_t& data,
      const key_t& key_in,
      const salt_t& salt = {},
      const context_t& ctx = context_t())
  {
    Hash(key_out.data(), key_out.size(), data.data(), data.size(), key_in.data(), key_in.size(), salt, ctx);
  }

  /// @brief Generate a Blake2b-256 digest using a given key and context
  /// @param digest Output buffer for the resulting digest
  /// @param data Input data to hash
  /// @param key Hash key
  /// @param ctx Blake2b context string for unique KDF applications
  static void Hash(digest_t& digest, const data_t& data, const key_t& key, const context_t& ctx)
  {
    Hash(digest.data(), digest.size(), data.data(), data.size(), key.data(), key.size(), {}, ctx);
  }

  /// @brief Create a random key
  static key_t create_key()
  {
    return crypto::RandBytes<KeyLen>();
  }

 private:
  static void Hash(
      std::uint8_t* ko_it,
      const std::size_t ko_size,
      const std::uint8_t* data_ptr,
      const std::size_t data_len,
      const std::uint8_t* key_in,
      const std::size_t key_in_len,
      const salt_t& salt = {},
      const context_t& ctx = context_t())
  {
    const exception::Exception ex{"Blake2b", __func__};

    if (ko_size < MinKeyOutLen || ko_size > MaxKeyOutLen)
      ex.throw_ex<std::invalid_argument>("invalid output key length.");

    crypto_generichash_blake2b_state h;
    crypto_generichash_blake2b_init_salt_personal(
        &h,
        key_in,
        key_in_len,
        ko_size,
        salt.data(),
        static_cast<context_t::buffer_t>(ctx).data());

    if (!data_ptr || !data_len)
      crypto_generichash_blake2b_update(&h, digest_t{}.data(), DigestLen);
    else
      crypto_generichash_blake2b_update(&h, data_ptr, data_len);

    crypto_generichash_blake2b_final(&h, ko_it, ko_size);
  }

  template <std::size_t N>
  static void KeyedHash(
      FixedSecBytes<N>& key_out,
      const std::uint8_t* data_ptr,
      const std::size_t data_len,
      const std::uint8_t* key_ptr,
      const std::size_t key_len)
  {
    const exception::Exception ex{"Blake2b", __func__};

    if (N < MinKeyOutLen || N > MaxKeyOutLen)
      {
        ex.throw_ex<std::length_error>(
            "invalid output key length: " + std::to_string(N)
            + ", min: " + std::to_string(tini2p::under_cast(MinKeyOutLen))
            + ", max: " + std::to_string(tini2p::under_cast(MaxKeyOutLen)));
      }

    if (!data_ptr && data_len != 0)
      ex.throw_ex<std::runtime_error>("null input data provided with non-zero length");

    if (data_len > MaxKeyMaterialLen)
      {
        ex.throw_ex<std::length_error>(
            "invalid key material length: " + std::to_string(data_len)
            + ", max: " + std::to_string(tini2p::under_cast(MaxKeyMaterialLen)));
      }

    if (!key_ptr && key_len != 0)
      ex.throw_ex<std::runtime_error>("null key provided with non-zero length");

    crypto_generichash_blake2b_state h;
    crypto_generichash_blake2b_init(&h, key_ptr, key_len, N);
    crypto_generichash_blake2b_update(&h, data_ptr, data_len);
    crypto_generichash_blake2b_final(&h, key_out.data(), N);
  }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_BLAKE_H_
