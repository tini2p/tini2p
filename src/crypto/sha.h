/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_SHA_H_
#define SRC_CRYPTO_SHA_H_

#include <sodium.h>

#include "src/exception/exception.h"

#include "src/crypto/sec_bytes.h"

namespace tini2p
{
namespace crypto
{
struct Sha256
{
  enum : std::uint16_t
  {
    DigestLen = 32,
    MinInputLen = 0,  //< Arbitrary min
    MaxInputLen = 4096,  //< Arbitrary max
  };

  using digest_t = FixedSecBytes<DigestLen>;  //< digest trait alias

  /// @brief Calculate a SHA256 digest of a given input buffer
  /// @param digest Non-const reference for the SHA256 digest result
  /// @param input Const reference to the input buffer
  template <class TDigest, class Input>
  static void Hash(TDigest& digest, const Input& input)
  {
    Hash(
        static_cast<std::uint8_t*>(digest.data()),
        digest.size(),
        reinterpret_cast<const std::uint8_t*>(input.data()),
        input.size());
  }

  /// @brief Calculate a SHA256 digest of a C-like input buffer
  /// @param digest Non-const reference for the SHA256 digest result
  /// @param input Const reference to the input buffer
  template <class TDigest>
  static void Hash(TDigest& digest, const std::uint8_t* input_ptr, const std::size_t& input_len)
  {
    Hash(static_cast<std::uint8_t*>(digest.data()), digest.size(), input_ptr, input_len);
  }

 private:
  static void Hash(
      std::uint8_t* digest_ptr,
      const std::size_t& digest_len,
      const std::uint8_t* input_ptr,
      const std::size_t& input_len)
  {
    const exception::Exception ex{"Sha256", __func__};

    const auto& exp_len = tini2p::under_cast(DigestLen);

    tini2p::check_cbuf(digest_ptr, digest_len, exp_len, exp_len, ex);
    tini2p::check_cbuf(input_ptr, input_len, tini2p::under_cast(MinInputLen), tini2p::under_cast(MaxInputLen), ex);

    crypto_hash_sha256(digest_ptr, input_ptr, input_len);
  }
};

class HmacSha256
{
 public:
  enum
  {
    DigestLen = Sha256::DigestLen,
    MinSaltLen = 0,
    DefaultSaltLen = Sha256::DigestLen,
    MaxSaltLen = 96,  // 3*Default, need more?
    KeyLen = Sha256::DigestLen,
    MinKeyMaterialLen = 0,
    MaxKeyMaterialLen = Sha256::DigestLen * 16,  // 512 bytes, need more?
    DefaultContextLen = 8,
    MaxContextLen = 96,  //< CString[96], somewhat arbitrary, based on RFC 5869 test vectors
  };

  using salt_t = FixedSecBytes<DefaultSaltLen>;
  using digest_t = FixedSecBytes<DigestLen>;
  using key_material_t = SecBytes;
  using key_t = FixedSecBytes<KeyLen>;

  /// @brief Calculate the HMAC digest of input key material
  /// @param out Output buffer
  /// @param input Input key material
  /// @param key HMAC pseudo-random key
  static void Hash(
      digest_t& out,
      const std::uint8_t* in_ptr,
      const std::size_t in_len,
      const std::uint8_t* key_ptr,
      const std::size_t key_len)
  {
    const exception::Exception ex{"HmacSha256", __func__};

    if (key_ptr == nullptr && key_len != 0)
      ex.throw_ex<std::runtime_error>("null key pointer provided with non-zero length.");

    crypto::FixedSecBytes<1> null_buf{0x00};

    const auto data_ptr = in_ptr == nullptr ? null_buf.data() : in_ptr;
    const auto data_len = in_ptr == nullptr ? 0 : in_len;

    Hash(out.data(), data_ptr, data_len, key_ptr, key_len);
  }

  /// @brief Calculate the HMAC digest of input key material
  /// @param out Output buffer
  /// @param in Input key material
  /// @param key HMAC pseudo-random key
  template <class TKeyMaterial, class TKey = key_t>
  static void Hash(digest_t& out, const TKeyMaterial& in, const TKey& key = key_t{})
  {
    crypto::FixedSecBytes<1> null_buf{0x00};

    const auto null_data = in.data() == nullptr;
    const auto data_ptr = null_data ? null_buf.data() : reinterpret_cast<const std::uint8_t*>(in.data()); 
    const auto data_len = null_data ? 0 : null_buf.size();

    Hash(out.data(), data_ptr, data_len, reinterpret_cast<const std::uint8_t*>(key.data()), key.size());
  }

 private:
  static void Hash(
      std::uint8_t* out_ptr,
      const std::uint8_t* in_ptr,
      const std::size_t in_len,
      const std::uint8_t* key_ptr,
      const std::size_t key_len)
  {
    crypto_auth_hmacsha256_state state;
    crypto_auth_hmacsha256_init(&state, key_ptr, key_len);
    crypto_auth_hmacsha256_update(&state, in_ptr, in_len);
    crypto_auth_hmacsha256_final(&state, out_ptr);
  }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_SHA_H_
