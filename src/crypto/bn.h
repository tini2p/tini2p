/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_BN_H_
#define SRC_CRYPTO_BN_H_

#include <openssl/bn.h>

#include "src/bytes.h"
#include "src/exception/exception.h"

#include "src/crypto/sec_bytes.h"

namespace tini2p
{
namespace crypto
{

/// @class BigNumContext
/// @brief Wrapper for BigNum context pointer
class BigNumContext
{
 public:
  using context_ptr = BN_CTX*;

  /// @brief Default ctor
  BigNumContext() : ctx_(BN_CTX_new()) {}

  BigNumContext(const BigNumContext&) = delete;
  BigNumContext(BigNumContext&&) = delete;

  ~BigNumContext()
  {
    if (ctx_)
      BN_CTX_free(ctx_);
  }

  /// @brief Get a const pointer to the BigNumContext
  const context_ptr pointer() const
  {
    const exception::Exception ex{"BigNumContext", __func__};

    check_context(ctx_, ex);

    return ctx_;
  }

  /// @brief Get a non-const pointer to the BigNumContext
  context_ptr pointer()
  {
    const exception::Exception ex{"BigNumContext", __func__};

    check_context(ctx_, ex);

    return ctx_;
  }

 private:
  void check_context(const context_ptr ctx_ptr, const exception::Exception& ex) const
  {
    if (!ctx_ptr)
      ex.throw_ex<std::runtime_error>("null context pointer");
  }

  context_ptr ctx_;
};

/// @class BigNum
/// @brief Class for operations on BigNums
class BigNum
{
 public:
  enum : std::uint16_t
  {
    MinBufferLen = 1,  //< min one byte buffer representation
    MaxBufferLen = 256,  //< max 256B buffer representation
    ElementLen = 32,  //< Curve25519/Ed25519 element byte length
  };

  using bn_ptr = BIGNUM*;  //< BigNum pointer trait alias
  using uint_t = std::uint32_t;  //< Uint trait alias
  using element_t = crypto::FixedSecBytes<32>;  //< Ed25519 element buffer trait alias

  /// @brief Default ctor
  BigNum() : num_(), bn_(BN_new())
  {
    BN_set_word(bn_, 0);
  }

  /// @brief Create a BigNum from an integer
  explicit BigNum(uint_t num) : num_(std::forward<uint_t>(num)), bn_(BN_new())
  {
    BN_set_word(bn_, num_);
  }

  /// @brief Create a BigNum from a buffer
  /// @detail The buffer will be interpreted as a big-endian BigNum
  template <class TBuffer, class = std::enable_if_t<!std::is_integral_v<TBuffer>>>
  explicit BigNum(const TBuffer& buf, const tini2p::Endian& endian = tini2p::Endian::Big) : bn_(BN_new())
  {
    from_buffer(buf, endian);
  }

  /// @brief Copy-ctor
  BigNum(const BigNum& oth) : bn_(BN_new())
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(oth.bn_, ex);

    const auto& oth_buf = oth.buffer();
    BN_bin2bn(oth_buf.data(), oth_buf.size(), bn_);
  }

  /// @brief Move-ctor
  BigNum(BigNum&& oth)
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(oth.bn_, ex);

    BN_swap(bn_, oth.bn_);
  }

  /// @brief Copy-assignment operator
  BigNum& operator=(const BigNum& oth)
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);
    check_bn(oth.bn_, ex);

    const auto& buf = oth.buffer();
    BN_bin2bn(buf.data(), buf.size(), bn_);

    return *this;
  }

  /// @brief Move-assignment operator
  BigNum& operator=(BigNum&& oth)
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);
    check_bn(oth.bn_, ex);

    BN_swap(bn_, oth.bn_);

    return *this;
  }

  ~BigNum()
  {
    if (bn_)
      BN_clear_free(bn_);
  }

  /// @brief Multiply this BigNum by a multiplier, and add another BigNum, all modulo mod
  /// @param add BigNum to add after multiplication
  /// @param mul BigNum to multiply this BigNum
  /// @param mod Modulus to perform the operations over
  BigNum MulAddMod(const BigNum& add, const BigNum& mul, const BigNum& mod) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto mul_ptr = mul.pointer();
    const auto add_ptr = add.pointer();
    const auto mod_ptr = mod.pointer();

    check_bn(bn_, ex);
    check_bn(add_ptr, ex);
    check_bn(mul_ptr, ex);
    check_bn(mod_ptr, ex);

    // setup
    BigNumContext ctx;
    auto ctx_ptr = ctx.pointer();

    BigNum res;
    auto res_ptr = res.pointer();

    BN_CTX_start(ctx_ptr);

    /// b = multiplier BigNum
    /// c = this BigNum
    /// res = b * c mod m
    BN_mod_mul(res_ptr, mul_ptr, bn_, mod_ptr, ctx_ptr);
    /// a = adder BigNum
    /// res = a + (b * c) mod m
    BN_mod_add(res_ptr, add_ptr, res_ptr, mod_ptr, ctx_ptr);

    BN_CTX_end(ctx_ptr);

    return res;
  }

  /// @brief Multiply this BigNum by a multiplier
  /// @param mul BigNum to multiply this BigNum
  /// @returns A BigNum with the multiplication result
  BigNum Multiply(const BigNum& mul) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    const auto mul_ptr = mul.pointer();

    check_bn(mul_ptr, ex);

    // setup
    BigNumContext ctx;
    auto ctx_ptr = ctx.pointer();

    BigNum res;
    auto res_ptr = res.pointer();

    BN_CTX_start(ctx_ptr);
    BN_mul(res_ptr, bn_, mul_ptr, ctx_ptr);
    BN_CTX_end(ctx_ptr);

    return res;
  }

  /// @brief Multiply this BigNum by a multiplier modulo mod
  /// @param mul BigNum to multiply this BigNum
  /// @param mod Modulus for the multiplication
  /// @returns A BigNum with the multiplication result
  BigNum MultiplyMod(const BigNum& mul, const BigNum& mod) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    const auto mul_ptr = mul.pointer();
    const auto mod_ptr = mod.pointer();

    check_bn(mul_ptr, ex);
    check_bn(mod_ptr, ex);

    // setup
    BigNumContext ctx;
    auto ctx_ptr = ctx.pointer();

    BigNum res;
    auto res_ptr = res.pointer();

    BN_CTX_start(ctx_ptr);
    BN_mod_mul(res_ptr, bn_, mul_ptr, mod_ptr, ctx_ptr);
    BN_CTX_end(ctx_ptr);

    return res;
  }

  /// @brief Divide this BigNum by BigNum divisor
  /// @param div BigNum divisor
  /// @param remainder BigNum division remainder
  /// @return A BigNum containing the division result
  BigNum Divide(const BigNum& div, BigNum& remainder) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto div_ptr = div.pointer();
    auto rem_ptr = remainder.pointer();

    check_bn(bn_, ex);
    check_bn(div_ptr, ex);
    check_bn(rem_ptr, ex);

    if (BN_get_word(div_ptr) == 0)
      ex.throw_ex<std::invalid_argument>("divisor cannot be zero");

    BigNum res;

    BigNumContext ctx;
    auto ctx_ptr = ctx.pointer();

    BN_CTX_start(ctx_ptr);

    BN_CTX_start(ctx_ptr);
    BN_div(res.pointer(), rem_ptr, bn_, div_ptr, ctx_ptr);
    BN_CTX_end(ctx_ptr);

    return res;
  }

  /// @brief Floor-division of this BigNum by BigNum divisor, discards remainder
  /// @param div BigNum divisor
  /// @param remainder BigNum division remainder
  /// @return A BigNum containing the division result
  BigNum Divide(const BigNum& div) const
  {
    BigNum rem;
    return Divide(div, rem);
  }

  /// @brief Divide this BigNum by BigNum divisor modulo another BigNum
  /// @param div BigNum divisor
  /// @param remainder BigNum division remainder
  /// @return A BigNum containing the division result
  BigNum DivideMod(const BigNum& div, const BigNum& mod) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    const auto div_ptr = div.pointer();
    auto mod_ptr = mod.pointer();

    check_bn(div_ptr, ex);
    check_bn(mod_ptr, ex);

    if (BN_get_word(div_ptr) == 0)
      ex.throw_ex<std::invalid_argument>("divisor cannot be zero");

    BigNum res;
    auto res_ptr = res.pointer();

    BigNumContext ctx;
    auto ctx_ptr = ctx.pointer();

    BN_CTX_start(ctx_ptr);

    BN_CTX_start(ctx_ptr);
    BN_mod_inverse(res_ptr, div_ptr, mod_ptr, ctx_ptr);
    BN_mod_mul(res_ptr, bn_, res_ptr, mod_ptr, ctx_ptr);
    BN_CTX_end(ctx_ptr);

    return res;
  }

  /// @brief Add (signed) another BigNum to this BigNum
  /// @param add BigNum to add
  /// @return A BigNum containing the subtraction result
  BigNum Add(const BigNum& add) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto add_ptr = add.pointer();

    check_bn(add_ptr, ex);
    check_bn(bn_, ex);

    BigNum res;

    auto ctx = BN_CTX_new();
    BN_CTX_start(ctx);
    BN_add(res.pointer(), bn_, add_ptr);
    BN_CTX_end(ctx);

    return res;
  }

  /// @brief Add (unsigned) another BigNum to this BigNum
  /// @param add BigNum to add
  /// @return A BigNum containing the subtraction result
  BigNum UAdd(const BigNum& add) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto add_ptr = add.pointer();

    check_bn(add_ptr, ex);
    check_bn(bn_, ex);

    BigNum res;

    auto ctx = BN_CTX_new();
    BN_CTX_start(ctx);
    BN_uadd(res.pointer(), bn_, add_ptr);
    BN_CTX_end(ctx);

    return res;
  }

  /// @brief Add another BigNum to this BigNum modulo mod
  /// @param add BigNum to add to this BigNum
  /// @param mod Modulus for the addition
  /// @returns A BigNum with the addition result
  BigNum AddMod(const BigNum& add, const BigNum& mod) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto add_ptr = add.pointer();
    const auto mod_ptr = mod.pointer();

    check_bn(bn_, ex);
    check_bn(add_ptr, ex);
    check_bn(mod_ptr, ex);

    // setup
    BigNumContext ctx;
    auto ctx_ptr = ctx.pointer();

    BigNum res;
    auto res_ptr = res.pointer();

    BN_CTX_start(ctx_ptr);
    BN_mod_add(res_ptr, bn_, add_ptr, mod_ptr, ctx_ptr);
    BN_CTX_end(ctx_ptr);

    return res;
  }

  /// @brief Subtract (signed) another BigNum from this BigNum
  /// @param sub BigNum to subtract
  /// @return A BigNum containing the subtraction result
  BigNum Subtract(const BigNum& sub) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto sub_ptr = sub.pointer();

    check_bn(sub_ptr, ex);
    check_bn(bn_, ex);

    BigNum res;

    auto ctx = BN_CTX_new();
    BN_CTX_start(ctx);
    BN_sub(res.pointer(), bn_, sub_ptr);
    BN_CTX_end(ctx);

    return res;
  }

  /// @brief Subtract (unsigned) another BigNum from this BigNum
  /// @param sub BigNum to subtract
  /// @return A BigNum containing the subtraction result
  BigNum USubtract(const BigNum& sub) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto sub_ptr = sub.pointer();

    check_bn(sub_ptr, ex);
    check_bn(bn_, ex);

    BigNum res;

    auto ctx = BN_CTX_new();
    BN_CTX_start(ctx);
    BN_usub(res.pointer(), bn_, sub_ptr);
    BN_CTX_end(ctx);

    return res;
  }

  /// @brief Subtract another BigNum from this BigNum modulo mod
  /// @param sub BigNum to subtract from this BigNum
  /// @param mod Modulus for the addition
  /// @returns A BigNum with the addition result
  BigNum SubtractMod(const BigNum& sub, const BigNum& mod) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto sub_ptr = sub.pointer();
    const auto mod_ptr = mod.pointer();

    check_bn(bn_, ex);
    check_bn(sub_ptr, ex);
    check_bn(mod_ptr, ex);

    // setup
    BigNumContext ctx;
    auto ctx_ptr = ctx.pointer();

    BigNum res;
    auto res_ptr = res.pointer();

    BN_CTX_start(ctx_ptr);
    BN_mod_sub(res_ptr, bn_, sub_ptr, mod_ptr, ctx_ptr);
    BN_CTX_end(ctx_ptr);

    return res;
  }

  /// @brief Exponentiate this BigNum by an exponent modulo another BigNum
  /// @param exponent Exponent to raise this BigNum
  /// @param modulus Modulus for the exponentiation
  BigNum ExpMod(const BigNum& exponent, const BigNum& modulus) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto mod_ptr = modulus.pointer();
    const auto exp_ptr = exponent.pointer();

    check_bn(mod_ptr, ex);
    check_bn(exp_ptr, ex);
    check_bn(bn_, ex);

    BigNum res;

    BigNumContext ctx;
    auto ctx_ptr = ctx.pointer();

    BN_CTX_start(ctx_ptr);
    BN_mod_exp(res.pointer(), bn_, exp_ptr, mod_ptr, ctx_ptr);
    BN_CTX_end(ctx_ptr);

    return res;
  }

  /// @brief Exponentiate this BigNum by an exponent
  /// @param exponent Exponent to raise this BigNum
  BigNum Exp(const BigNum& exponent) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto exp_ptr = exponent.pointer();

    check_bn(exp_ptr, ex);
    check_bn(bn_, ex);

    BigNum res;

    BigNumContext ctx;
    auto ctx_ptr = ctx.pointer();

    BN_CTX_start(ctx_ptr);
    BN_exp(res.pointer(), bn_, exp_ptr, ctx_ptr);
    BN_CTX_end(ctx_ptr);

    return res;
  }

  /// @brief Modulate this BigNum by another BigNum
  BigNum Mod(const BigNum& modulus) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto mod_ptr = modulus.pointer();
    check_bn(mod_ptr, ex);
    check_bn(bn_, ex);

    BigNumContext ctx;
    auto ctx_ptr = ctx.pointer();

    BigNum res;
    auto res_ptr = res.pointer();

    BN_CTX_start(ctx_ptr);
    BN_mod(res_ptr, bn_, mod_ptr, ctx_ptr);
    BN_CTX_end(ctx_ptr);

    return res;
  }

  /// @brief Invert this BigNum modulo another BigNum
  /// @param modulus Modulus for the exponentiation
  BigNum ModInverse(const BigNum& modulus) const
  {
    const exception::Exception ex{"BigNum", __func__};

    const auto mod_ptr = modulus.pointer();
    check_bn(mod_ptr, ex);
    check_bn(bn_, ex);

    BigNumContext ctx;
    auto ctx_ptr = ctx.pointer();

    BigNum res;
    auto res_ptr = res.pointer();

    BN_CTX_start(ctx_ptr);
    BN_mod_inverse(res_ptr, bn_, mod_ptr, ctx_ptr);
    BN_CTX_end(ctx_ptr);

    return res;
  }

  /// @brief Negate this BigNum
  BigNum Negate() const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    BigNum res = *this;

    // sets: BigNum to -BigNum or -BigNum to BigNum
    BN_set_negative(res.pointer(), !BN_is_negative(bn_));

    return res;
  }

  /// @brief Left shift this BigNum's value by a number of bits
  /// @param bits number of bits to shift
  BigNum operator<<(const uint_t& bits) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    BigNum res;

    // rshift by shift number of bits
    BN_lshift(res.pointer(), bn_, bits);

    return res;
  }

  /// @brief Right shift this BigNum's value by a number of bits
  /// @param bits number of bits to shift
  BigNum operator>>(const uint_t& bits) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    BigNum res;

    // rshift by shift number of bits
    BN_rshift(res.pointer(), bn_, bits);

    return res;
  }

  /// @brief Mask bits from this BigNum's value
  /// @param mask Bitmask to apply
  uint_t MaskBits(const uint_t& mask) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    auto res = *this;
    auto res_ptr = res.pointer();

    BN_mask_bits(res_ptr, mask);

    return BN_get_word(res_ptr);
  }

  /// @brief Set the BigNum from an unsigned integer
  BigNum& from_uint(uint_t num)
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    BN_set_word(bn_, num);

    return *this;
  }

  /// @brief Set the BigNum from a big-endian buffer 
  template <class TBuffer, class = std::enable_if_t<!std::is_integral_v<TBuffer>>>
  BigNum& from_buffer(const TBuffer& buf, const tini2p::Endian& endian = tini2p::Endian::Big)
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);
    check_buffer(buf, ex);

    if (endian == tini2p::Endian::Big)
      BN_bin2bn(buf.data(), buf.size(), bn_);
    else
      {
        crypto::SecBytes buf_be;
        buf_be.resize(buf.size());

        std::reverse_copy(buf.begin(), buf.end(), buf_be.begin());
        BN_bin2bn(buf_be.data(), buf_be.size(), bn_);
      }

    return *this;
  }

  /// @brief Get the BigNum as an unsigned integer
  uint_t uint() const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    if (BN_num_bits(bn_) > 32)
      ex.throw_ex<std::logic_error>("BigNum too large for unsigned integer representation");

    return BN_get_word(bn_);
  }

  /// @brief Get a big-endian buffer represention of this BigNum
  crypto::SecBytes buffer() const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    crypto::SecBytes buf;
    buf.resize(bits_to_bytes(BN_num_bits(bn_)));

    BN_bn2bin(bn_, buf.data());

    return buf;
  }

  /// @brief Get the BigNum as a Curve25519/Ed25519 element buffer
  /// @param endian Endian-order of the buffer
  // TODO(tini2p): Potentially breaks constant-time when the BigNum buffer is fewer than 32 bytes
  //
  //    In all testing so far, BigNums used in Curve25519 operations only return 31 bytes when the
  //    result is negative.
  //
  //    Is this two byte copy difference (once for extraction, once for endian flip) enough to
  //    detect a timing difference? TBD
  element_t element_buffer() const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    const auto num_bytes = bits_to_bytes(BN_num_bits(bn_));
    if (num_bytes > ElementLen)
      ex.throw_ex<std::runtime_error>("BigNum too large for Ed25519 element representation");

    crypto::SecBytes buf;
    buf.resize(num_bytes);
    BN_bn2bin(bn_, buf.data());

    // convert to little-endian
    element_t elem;
    std::reverse_copy(buf.begin(), buf.end(), elem.begin());

    return elem;
  }

  /// @brief Get a const pointer to the BigNum
  const bn_ptr pointer() const noexcept
  {
    return bn_;
  }

  /// @brief Get a non-const pointer to the BigNum
  bn_ptr pointer() noexcept
  {
    return bn_;
  }

  /// @brief Equality comparison operator
  std::uint8_t operator==(const BigNum& oth) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);
    check_bn(oth.bn_, ex);

    return static_cast<std::uint8_t>(BN_cmp(bn_, oth.bn_) == 0);
  }

  /// @brief Inequality comparison operator
  std::uint8_t operator!=(const BigNum& oth) const
  {
    return static_cast<std::uint8_t>(!(*this == oth));
  }

  /// @brief Less-than comparison operator
  std::uint8_t operator<(const BigNum& oth) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);
    check_bn(oth.bn_, ex);

    return static_cast<std::uint8_t>(BN_cmp(bn_, oth.bn_) == -1);
  }

  /// @brief Less-than-or-equal comparison operator
  std::uint8_t operator<=(const BigNum& oth) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);
    check_bn(oth.bn_, ex);

    return static_cast<std::uint8_t>(BN_cmp(bn_, oth.bn_) <= 0);
  }

  /// @brief Greater-than comparison operator
  std::uint8_t operator>(const BigNum& oth) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);
    check_bn(oth.bn_, ex);

    return static_cast<std::uint8_t>(BN_cmp(bn_, oth.bn_) == 1);
  }

  /// @brief Addition operator
  /// @return BigNum containing the addition result
  BigNum operator+(const BigNum& add) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    const auto add_ptr = add.pointer();
    check_bn(add_ptr, ex);
    
    return (BN_is_negative(bn_) || BN_is_negative(add_ptr)) ? Add(add) : UAdd(add);
  }

  /// @brief Subtraction operator
  /// @return BigNum containing the subtraction result
  BigNum operator-(const BigNum& sub) const
  {
    const exception::Exception ex{"BigNum", __func__};

    check_bn(bn_, ex);

    const auto sub_ptr = sub.pointer();
    check_bn(sub_ptr, ex);

    return (BN_is_negative(bn_) || BN_is_negative(sub_ptr) || BN_cmp(bn_, sub_ptr) == -1) ? Subtract(sub)
                                                                                          : USubtract(sub);
  }

  /// @brief Floor-division operator, discards remainder
  /// @return BigNum containing the division result
  BigNum operator/(const BigNum& divisor) const
  {
    return Divide(divisor);
  }

  /// @brief Convert bits length to bytes length
  uint_t bits_to_bytes(const uint_t& bits) const
  {
    return (bits / 8) + static_cast<uint_t>((bits % 8) != 0);
  }

 private:
  void check_bn(const bn_ptr bn, const exception::Exception& ex) const
  {
    if (bn == nullptr)
      ex.throw_ex<std::runtime_error>("null BigNum pointer");
  }

  template <class TBuffer, class = std::enable_if_t<!std::is_integral_v<TBuffer>>>
  void check_buffer(const TBuffer& buf, const exception::Exception& ex) const
  {
    const auto& buf_len = buf.size();
    const auto& min_len = tini2p::under_cast(MinBufferLen);
    const auto& max_len = tini2p::under_cast(MaxBufferLen);

    if (buf_len < min_len || buf_len > max_len)
      {
        ex.throw_ex<std::length_error>(
            "invalid buffer length: " + std::to_string(buf_len) + ", min: " + std::to_string(min_len)
            + ", max: " + std::to_string(max_len));
      }
  }

  uint_t num_;
  bn_ptr bn_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_BN_H_
