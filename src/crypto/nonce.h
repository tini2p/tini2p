/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_NONCE_H_
#define SRC_CRYPTO_NONCE_H_

#include "src/bytes.h"
#include "src/crypto/sec_bytes.h"

namespace tini2p
{
namespace crypto
{
/// @class Nonce
/// @brief Counter-based, strictly increasing nonce
class Nonce
{
 public:
  enum
  {
    KeyLen = 32,
    NonceLen = 12,
  };

  using buffer_t = FixedSecBytes<NonceLen>;  //< Buffer trait alias
  using uint_t = std::uint16_t;  //< Uint trait alias

  Nonce() : buf_{}, n_(0) {}

  /// @brief Create a nonce from a given integer
  /// @brief n Nonce value to set
  explicit Nonce(uint_t n) : buf_{}, n_(std::forward<uint_t>(n))
  {
    tini2p::write_bytes(buf_.data(), n_);
  }

  /// @brief Create a nonce from a given buffer
  /// @brief n Nonce buffer containing value to set
  explicit Nonce(buffer_t buf) : buf_{std::forward<buffer_t>(buf)}, n_()
  {
    tini2p::read_bytes(buf_.data(), n_);
  }

  /// @brief Set a new nonce value
  /// @param n New nonce value to set
  /// @throw Logic error if new nonce is not greater-than-or-equal-to current nonce
  Nonce& operator()(const uint_t& n)
  {
    const exception::Exception ex{"Nonce", __func__};

    if (n < n_)
      ex.throw_ex<std::logic_error>("nonce is strictly increasing.");

    n_ = n;
    tini2p::write_bytes(buf_.data(), n_);

    return *this;
  }

  /// @brief Pre-increment the nonce by one
  /// @return Nonce value after increment
  uint_t operator++()
  {
    tini2p::write_bytes(buf_.data(), ++n_);
    return n_;
  }

  /// @brief Post-increment the nonce by one
  /// @return Nonce value before increment
  uint_t operator++(int)
  {
    decltype(n_) tmp(n_);
    tini2p::write_bytes(buf_.data(), ++n_);
    return tmp;
  }

  /// @brief Get the nonce as an unsigned int
  explicit operator uint_t() const noexcept
  {
    return n_;
  }

  /// @brief Get the nonce as a buffer
  explicit operator const buffer_t&() const noexcept
  {
    return buf_;
  }

  /// @brief Get a const pointer to the nonce buffer
  const std::uint8_t* data() const noexcept
  {
    return buf_.data();
  }

  /// @brief Get a non-const pointer to the nonce buffer
  std::uint8_t* data() noexcept
  {
    return buf_.data();
  }

  /// @brief Get the size of the nonce buffer
  buffer_t::size_type size() const
  {
    return buf_.size();
  }

  /// @brief Equality comparison with another Nonce
  std::uint8_t operator==(const Nonce& oth) const
  {
    return static_cast<std::uint8_t>(n_ == oth.n_);
  }

  /// @brief Equality comparison with another Nonce
  std::uint8_t operator==(Nonce& oth)
  {
    return static_cast<std::uint8_t>(n_ == oth.n_);
  }

  /// @brief Less-than comparison operator
  std::uint8_t operator<(const Nonce& oth) const
  {
    return static_cast<std::uint8_t>(n_ < oth.n_);
  }

  /// @brief Swap with another Nonce
  Nonce& swap(Nonce& oth)
  {
    buf_.swap(oth.buf_);
    n_ = oth.n_;

    return *this;
  }

  /// @brief Reset the nonce to zero
  Nonce& reset()
  {
    buf_.zero();
    n_ = 0;

    return *this;
  }

 private:
  buffer_t buf_;
  uint_t n_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_NONCE_H_
