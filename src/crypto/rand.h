/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_RAND_H_
#define SRC_CRYPTO_RAND_H_

#include <sodium.h>

#include "src/crypto/sec_bytes.h"

namespace tini2p
{
namespace crypto
{
/// @brief Generate a random block of data with OS RNG
/// @param data Buffer to fill with random data
/// @param size Size of data buffer
inline void RandBytes(uint8_t* data, const std::size_t size)
{
  if (data)
    randombytes_buf(data, size);
}

/// @brief Generate a random block of data with OS RNG
/// @param buf Buffer to fill with random data
template <class Buffer>
inline void RandBytes(Buffer& buf)
{
  if (buf.data())
    randombytes_buf(buf.data(), buf.size());
}

/// @brief Generate a random block of data with OS RNG
/// @param len Length of the buffer to fill with random data
inline SecBytes RandBuffer(const std::size_t len)
{
  SecBytes buf;
  buf.resize(len);
  randombytes_buf(buf.data(), buf.size());
  return buf;
}

/// @brief Generate a random fixed-size block of data with OS RNG
/// @param len Length of the buffer to fill with random data
template <std::size_t N>
inline FixedSecBytes<N> RandBytes()
{
  FixedSecBytes<N> buf;
  randombytes_buf(buf.data(), buf.size());
  return buf;
}

inline decltype(auto) RandInRange(
    const std::uint32_t min = std::numeric_limits<std::uint32_t>::min(),
    const std::uint32_t max = std::numeric_limits<std::uint32_t>::max())
{
  std::uint32_t rand(0);
  do
    {
      rand = randombytes_uniform(max);
    }
  while (rand < min || rand > max);

  return rand;
}

/// @struct RandGenerator
/// @brief Uniform random number generator, primarily used for std::shuffle
struct RandGenerator
{
  using result_type = std::uint32_t;

  static std::uint32_t min()
  {
    return 0;
  }

  static std::uint32_t max()
  {
    return 4294967295;
  }

  std::uint32_t operator()()
  {
    return randombytes_uniform(max());
  }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_RAND_H_
