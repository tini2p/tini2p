/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_HASH_SIPHASH_H_
#define SRC_CRYPTO_HASH_SIPHASH_H_

#include <sodium.h>

#include "src/bytes.h"

#include "src/crypto/keys.h"
#include "src/crypto/rand.h"
#include "src/crypto/sec_bytes.h"

namespace tini2p
{
namespace crypto
{
struct SipHash
{
  enum
  {
    DigestLen = 16,
    KeyLen = 16,
    KeyPartLen = 8,
    IVLen = 8,
  };

  /// @brief Siphash key
  struct Key : public crypto::Key<KeyLen>
  {
    using base_t = crypto::Key<KeyLen>;

    Key() : base_t() {}

    Key(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

    Key(const SecBytes& buf) : base_t(buf) {}

    Key(std::initializer_list<std::uint8_t> list) : base_t(list) {}
  };

  using digest_t = FixedSecBytes<DigestLen>;  //< Digest trait alias
  using key_t = SipHash::Key;  //< Key trait alias
  using key_part_t = FixedSecBytes<KeyPartLen>;  //< Key part trait alias
  using iv_t = FixedSecBytes<IVLen>;  //< IV trait alias

  /// @brief Calculate a SipHash digest using key parts from DataPhase KDF
  /// @param key_pt1 Part one key from NTCP2 DataPhase KDF
  /// @param key_pt2 Part two key from NTCP2 DataPhase KDF
  /// @param iv IV from DataPhase KDF or following message round, see spec
  static void Hash(
      const key_part_t& key_pt1,
      const key_part_t& key_pt2,
      const iv_t& iv,
      digest_t& digest)
  {
    key_t key;
    BytesWriter<key_t> writer(key);
    writer.write_data(key_pt1);
    writer.write_data(key_pt2);

    crypto_shorthash_siphashx24(
        digest.data(), iv.data(), iv.size(), key.data());
  }

  /// @brief Calculate a SipHash digest using a given key
  /// @note Key should be randomly generated, unless protocol specifies a fixed key
  /// @param key SipHash key
  /// @param iv IV from DataPhase KDF or following message round, see spec
  /// @param digest SipHash digest result
  static void Hash(const key_t& key, const iv_t& iv, digest_t& digest)
  {
    crypto_shorthash_siphashx24(digest.data(), iv.data(), iv.size(), key.data());
  }

  /// @brief Calculate a SipHash digest using a given key
  /// @note Key should be randomly generated, unless protocol specifies a fixed key
  /// @tparam TInput Type of input buffer
  /// @param key SipHash key
  /// @param input Input to hash
  /// @param digest SipHash digest result
  template <class TInput>
  static void Hash(const key_t& key, const TInput& input, digest_t& digest)
  {
    crypto_shorthash_siphashx24(digest.data(), input.data(), input.size(), key.data());
  }

  /// @brief Create a random key
  static key_t create_key()
  {
    return key_t(crypto::RandBytes<KeyLen>());
  }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_HASH_SIPHASH_H_
