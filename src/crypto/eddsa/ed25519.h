/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_Ed25519_H_
#define SRC_CRYPTO_Ed25519_H_

#include <sodium.h>

#include "src/exception/exception.h"

#include "src/crypto/keys.h"
#include "src/crypto/rand.h"
#include "src/crypto/sec_bytes.h"
#include "src/crypto/signature.h"

namespace tini2p
{
namespace crypto
{
/// @class Ed25519
/// @brief Base class for Ed25519 based crypto
class Ed25519
{
 protected:
  Ed25519() = default;  // disable direct instantiation

 public:
  enum
  {
    PublicKeyLen = 32,
    PrivateKeyLen = 64,
    SignatureLen = 64,
  };

  struct PublicKey : public Key<PublicKeyLen>
  {
    using base_t = Key<PublicKeyLen>;

    PublicKey() : base_t() {}

    PublicKey(base_t::buffer_t buf)
        : base_t(std::forward<base_t::buffer_t>(buf))
    {
    }

    PublicKey(const SecBytes& buf) : base_t(buf) {}

    PublicKey(std::initializer_list<std::uint8_t> list) : base_t(list) {}
  };

  struct PrivateKey : public Key<PrivateKeyLen>
  {
    using base_t = Key<PrivateKeyLen>;

    PrivateKey() : base_t() {}

    PrivateKey(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

    PrivateKey(const SecBytes& buf) : base_t(buf) {}

    PrivateKey(std::initializer_list<std::uint8_t> list) : base_t(list) {}
  };

  struct Signature : public crypto::Signature<SignatureLen>
  {
    using base_t = crypto::Signature<SignatureLen>;

    Signature() : base_t() {}

    Signature(base_t::buffer_t buf)
        : base_t(std::forward<base_t::buffer_t>(buf))
    {
    }

    Signature(const SecBytes& buf) : base_t(buf) {}

    Signature(std::initializer_list<std::uint8_t> list) : base_t(list) {}
  };

  using curve_t = Ed25519;  //< Curve trait alias
  using message_t = SecBytes;  //< Message trait alias
  using signature_t = Ed25519::Signature;  //< Signature trait alias
  using pubkey_t = Ed25519::PublicKey;  //< Public key trait alias
  using pvtkey_t = Ed25519::PrivateKey;  //< Private key trait alias
  using keypair_t = Keypair<Ed25519>;  //< Keypair trait alias

  /// @brief Create an EdDSA-Ed25519-Sha512 (I2P-variant) key pair
  /// @detail Concatenates public key to the trailing 32 bytes of the private key for I2P signatures.
  ///   See I2P Java impl for details.
  /// @note What kind of interactions does this cause between the private and public keys?
  ///       Does it affect point encoding?
  ///       Does it affect signatures?
  /// @return Ed25519 keypair
  inline static keypair_t create_keys()
  {
    const exception::Exception ex{"Ed25519", __func__};

    keypair_t k;
    if(crypto_sign_keypair(k.pubkey.data(), k.pvtkey.data()))
      ex.throw_ex<std::runtime_error>("could not create keypair.");

    std::copy_n(k.pubkey.data(), PublicKeyLen, k.pvtkey.data() + PublicKeyLen);
    return k;
  }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_EDDSA_Ed25519_H_
