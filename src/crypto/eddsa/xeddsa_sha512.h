/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_EDDSA_XEDDSA_SHA512_H_
#define SRC_CRYPTO_EDDSA_XEDDSA_SHA512_H_

#include <sodium.h>

#include "src/exception/exception.h"

#include "src/crypto/keys.h"
#include "src/crypto/rand.h"
#include "src/crypto/sec_bytes.h"
#include "src/crypto/signature.h"
#include "src/crypto/x25519.h"

namespace tini2p
{
namespace crypto
{
/// @struct XEdDSASha512
/// @brief XEdDSASha512 implementation
struct XEdDSASha512 : public X25519
{
  /// @brief Default ctor, creates new keypair
  XEdDSASha512()
  {
    rekey(create_keys());
  }

  XEdDSASha512(const XEdDSASha512& oth)
      : sk_(std::make_unique<pvtkey_t>(*(oth.sk_))), pk_(oth.pk_)
  {
  }

  /// @brief Create an XEdDSASha512 signer with a private key
  /// @param sk XEdDSASha512 private key
  explicit XEdDSASha512(pvtkey_t sk)
  {
    rekey(std::forward<pvtkey_t>(sk));
  }

  /// @brief create a new EdDSASha512 verifier with a public key
  /// @param sk EdDSASha512 public key
  explicit XEdDSASha512(pubkey_t pk) : pk_(std::forward<pubkey_t>(pk)) {}

  /// @brief create a new EdDSASha512 verifier with a keypair
  /// @param sk EdDSASha512 keypair
  explicit XEdDSASha512(keypair_t keys)
  {
    rekey(std::forward<keypair_t>(keys));
  }

  void operator=(const XEdDSASha512& oth)
  {
    sk_ = std::make_unique<pvtkey_t>(*(oth.sk_));
    pk_ = oth.pk_;
  }

  void Sign(
      message_t::const_pointer msg,
      message_t::size_type msg_len,
      signature_t& sig) const
  {
    const exception::Exception ex{"XEdDSA", __func__};

    if (!sk_)
      ex.throw_ex<std::invalid_argument>("null private key.");

    if (!msg || !msg_len)
      ex.throw_ex<std::invalid_argument>("null message.");

    ex.throw_ex<std::runtime_error>("unimplemented.");
  }

  std::uint8_t Verify(
      message_t::const_pointer msg,
      message_t::size_type msg_len,
      const signature_t& sig) const
  {
    const exception::Exception ex{"XEdDSA", __func__};

    if (!msg || !msg_len)
      ex.throw_ex<std::invalid_argument>("null message.");

    ex.throw_ex<std::runtime_error>("unimplemented.");

    return 0;
  }

  /// @brief Rekey with a private key
  /// @param sk X25519 private key
  void rekey(pvtkey_t sk)
  {
    sk_ = std::make_unique<pvtkey_t>(std::forward<pvtkey_t>(sk));
    crypto_scalarmult_curve25519_base(pk_.data(), sk_->data());
  }

  /// @brief Rekey with a public key
  /// @detail Disables signing functionality, will only verify signatures
  /// @param pk X25519 public key
  void rekey(pubkey_t pk)
  {
    pk_ = std::forward<pubkey_t>(pk);
    sk_.reset(nullptr);
  }

  /// @brief Rekey with a keypair
  /// @param k X25519 keypair
  void rekey(keypair_t k)
  {
    pk_ = std::forward<pubkey_t>(k.pubkey);
    sk_ = std::make_unique<pvtkey_t>(k.pvtkey);
  }

  const pubkey_t& pubkey() const noexcept
  {
    return pk_;
  }

  pubkey_t& pubkey() noexcept
  {
    return pk_;
  }

 private:
  std::unique_ptr<pvtkey_t> sk_;
  pubkey_t pk_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_EDDSA_XEDDSA_SHA512_H_
