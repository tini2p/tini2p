/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_ED25519_SHA512_H_
#define SRC_CRYPTO_ED25519_SHA512_H_

#include <sodium.h>

#include "src/exception/exception.h"

#include "src/crypto/keys.h"
#include "src/crypto/rand.h"
#include "src/crypto/sec_bytes.h"
#include "src/crypto/signature.h"

#include "src/crypto/eddsa/ed25519.h"

namespace tini2p
{
namespace crypto
{
class EdDSASha512 : public Ed25519
{
 public:
  using signature_t = Ed25519::signature_t;  //< Signature trait alias

  /// @brief Default ctor
  EdDSASha512()
  {
    create_own_keys({"EdDSASha512"});
  }

  /// @brief Copy-ctor
  EdDSASha512(const EdDSASha512& oth) : pk_(oth.pk_)
  {
    if (oth.sk_)
      sk_ = std::make_unique<pvtkey_t>(*oth.sk_);
  }

  /// @brief Move-ctor
  EdDSASha512(EdDSASha512&& oth) : sk_(std::move(oth.sk_)), pk_(std::move(oth.pk_)) {}

  /// @brief Create an EdDSASha512 signer with a private key
  /// @param sk EdDSASha512 private key
  explicit EdDSASha512(pvtkey_t sk) { rekey(std::forward<pvtkey_t>(sk)); }

  /// @brief Create a new EdDSASha512 verifier with a public key
  explicit EdDSASha512(pubkey_t pk) : sk_(), pk_(std::forward<pubkey_t>(pk)) {}

  /// @brief Create an EdDSASha512 signer from a keypair
  explicit EdDSASha512(keypair_t keys)
      : sk_(std::make_unique<pvtkey_t>(std::forward<pvtkey_t>(keys.pvtkey))), pk_(std::forward<pubkey_t>(keys.pubkey))
  {
    std::copy_n(pk_.data(), pk_.size(), sk_->data() + PublicKeyLen);  // thanks I2P
  }

  /// @brief Copy-assignment operator
  EdDSASha512& operator=(const EdDSASha512& oth)
  {
    sk_ = std::make_unique<pvtkey_t>(*(oth.sk_));
    pk_ = oth.pk_;

    return *this;
  }

  /// @brief Move-assignment operator
  EdDSASha512& operator=(EdDSASha512&& oth)
  {
    sk_ = std::move(oth.sk_);
    pk_ = std::move(oth.pk_);

    return *this;
  }

  /// @brief Sign a message
  /// @param m Message to sign
  /// @param mlen Length of message to sign
  /// @param signature Buffer for the resulting signature
  void Sign(
      message_t::const_pointer msg,
      const message_t::size_type msg_len,
      signature_t& sig) const
  {
    const exception::Exception ex{"EdDSASha512", __func__};

    if (!sk_)
      ex.throw_ex<std::runtime_error>("null signing key.");

    if (!msg || !msg_len)
      ex.throw_ex<std::invalid_argument>("null message.");

    // Sign message
    if (crypto_sign_detached(sig.data(), nullptr, msg, msg_len, sk_->data()))
      ex.throw_ex<std::runtime_error>("could not sign message.");
  }

  /// @brief Verify an Ed25519 signed message
  /// @param m Signed message to verify
  /// @param mlen Length of the signed message
  /// @param sig Buffer containing the signature
  /// @return True on successful verification
  std::uint8_t Verify(
      message_t::const_pointer msg,
      const message_t::size_type msg_len,
      const signature_t& sig) const
  {
    if (!msg || !msg_len)
      exception::Exception{"EdDSASha512", __func__}
          .throw_ex<std::invalid_argument>("null message.");

    return !crypto_sign_verify_detached(sig.data(), msg, msg_len, pk_.data());
  }

  /// @brief Get a const reference to the public key
  const pubkey_t& pubkey() const noexcept
  {
    return pk_;
  }

  /// @brief Get a non-const reference to the public key
  pubkey_t& pubkey() noexcept
  {
    return pk_;
  }

  /// @brief Rekey the verifier with a new public key
  void rekey(pubkey_t key)
  {
    pk_ = std::forward<pubkey_t>(key);
    sk_.reset(nullptr);
  }

  /// @brief Rekey with a new private key
  /// @param sk Ed25519 private key
  void rekey(pvtkey_t sk)
  {
    create_own_keys({"EdDSASha512", __func__}, std::forward<pvtkey_t>(sk), false);
  }

  /// @brief Rekey with a new private key
  /// @param sk Ed25519 private key
  void rekey(keypair_t k)
  {
    pk_ = std::forward<pubkey_t>(k.pubkey);
    sk_ = std::make_unique<pvtkey_t>(std::forward<pvtkey_t>(k.pvtkey));
  }

 private:
  void create_own_keys(const exception::Exception& ex, pvtkey_t sk = {}, bool empty = true)
  {
    if (empty)
      {
        sk_.reset(new pvtkey_t());
        crypto_sign_keypair(pk_.data(), sk_->data());
      }
    else
      {
        sk_ = std::make_unique<pvtkey_t>(std::forward<pvtkey_t>(sk));

        if (crypto_sign_ed25519_sk_to_pk(pk_.data(), sk_->data()))
          ex.throw_ex<std::runtime_error>("could not create keypair.");
      }

    std::copy_n(pk_.data(), PublicKeyLen, sk_->data() + PublicKeyLen);
  }

  std::unique_ptr<pvtkey_t> sk_;
  pubkey_t pk_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_ED25519_SHA512_H_
