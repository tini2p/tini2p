/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_AES_H_
#define SRC_CRYPTO_AES_H_

#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>

#include <openssl/aes.h>
#include <openssl/evp.h>

#include "src/bytes.h"
#include "src/exception/exception.h"

#include "src/crypto/keys.h"
#include "src/crypto/rand.h"

namespace tini2p
{
namespace crypto
{
class AES
{
 public:
  enum
  {
    KeyLen = 32,
    IVLen = 16,
    BlockLen = 16,
    AESBits = 256,
  };

  using key_t = Key<KeyLen>;  //< Key trait alias
  using iv_t = FixedSecBytes<IVLen>;  //< IV trait alias
  using key_iv_t = KeyIV<AES>;  // KeyIV trait alias
  using block_t = FixedSecBytes<BlockLen>;  //< Block trait alias

  AES() : ctx_(EVP_CIPHER_CTX_new()), key_(), iv_() {}

  /// @brief Create a CBC en/decryption cipher
  /// @param key Cipher key (typically responder router hash)
  /// @param iv Cipher IV
  template <
      class Key,
      class IV,
      typename = std::enable_if_t<
          (std::is_same<Key, key_t>::value || std::is_same<Key, key_t::buffer_t>::value)
          && (std::is_same<IV, iv_t>::value || std::is_same<IV, iv_t::buffer_t>::value)>>
  AES(const Key& key, const IV& iv)
  {
    const exception::Exception& ex{"AES", __func__};

    check_key(key, ex);
    check_iv(iv, ex);

    std::copy_n(key.data(), KeyLen, key_.data());
    std::copy_n(iv.data(), IVLen, iv_.data());

    ctx_ = EVP_CIPHER_CTX_new();
  }

  /// @brief Create a AES en/decryption cipher
  /// @param key Cipher key (typically responder router hash)
  template <
      class Key,
      typename = std::enable_if_t<std::is_same<Key, key_t>::value || std::is_same<Key, key_t::buffer_t>::value>>
  AES(const Key& key)
  {
    const exception::Exception& ex{"AES", __func__};

    check_key(key, ex);

    std::copy_n(key.data(), KeyLen, key_.data());
    ctx_ = EVP_CIPHER_CTX_new();
  }

  ~AES()
  {
    if (ctx_)
      EVP_CIPHER_CTX_free(ctx_);
  }

  /// @brief Delete copy-ctor to avoid segfaults
  AES(const AES&) = delete;

  /// @brief Delete copy assignment operator to avoid segfaults
  void operator=(const AES&) = delete;

  /// @brief Encrypt a buffer of data (in-place)
  /// @param in_out In-place buffer for processing
  /// @param in_out_len In-place buffer length
  void Encrypt(std::uint8_t* in_out, const std::size_t in_out_len)
  {
    ProcessCBC(in_out, in_out_len, Mode::Encrypt, {"AES", __func__});
  }

  /// @brief Decrypt a buffer of data (in-place)
  /// @param out In-place buffer for processing
  /// @param out_len In-place buffer length
  void Decrypt(std::uint8_t* in_out, const std::size_t in_out_len)
  {
    ProcessCBC(in_out, in_out_len, Mode::Decrypt, {"AES", __func__});
  }

  /// @brief Encrypt a buffer using of data (in-place) using ECB mode
  /// @param in_out In-place buffer for processing
  /// @param in_out_len In-place buffer length
  void EncryptECB(std::uint8_t* in_out, const std::size_t in_out_len)
  {
    ProcessECB(in_out, in_out_len, Mode::Encrypt, {"AES", __func__});
  }

  /// @brief Decrypt a buffer of data (in-place) using ECB
  /// @param out In-place buffer for processing
  /// @param out_len In-place buffer length
  void DecryptECB(std::uint8_t* in_out, const std::size_t in_out_len)
  {
    ProcessECB(in_out, in_out_len, Mode::Decrypt, {"AES", __func__});
  }

  /// @brief Set the IV
  /// @throws Invalid argument for wrong IV length or null IV
  void set_iv(const std::uint8_t* iv_ptr, const std::size_t& iv_len)
  {
    const exception::Exception ex{"AES", __func__};

    const auto& exp_iv_len =  tini2p::under_cast(IVLen);
    if (iv_len != exp_iv_len)
      {
        ex.throw_ex<std::invalid_argument>(
            "invalid IV length: " + std::to_string(iv_len) + ", expected: " + std::to_string(exp_iv_len));
      }

    iv_t iv(iv_ptr, iv_len);

    check_iv(iv, ex);

    iv_ = std::move(iv);
  }

  /// @brief Create a CBC en/decryption cipher
  /// @param key Cipher key (typically responder router hash)
  /// @param iv Cipher IV
  template <
      class Key,
      class IV,
      typename = std::enable_if_t<
          (std::is_same<Key, key_t>::value || std::is_same<Key, key_t::buffer_t>::value)
          && (std::is_same<IV, iv_t>::value || std::is_same<IV, iv_t::buffer_t>::value)>>
  void rekey(const Key& key, const IV& iv)
  {
    const exception::Exception ex{"AES", __func__};

    check_key(key, ex);
    check_iv(iv, ex);

    std::copy_n(key.data(), KeyLen, key_.data());
    std::copy_n(iv.data(), IVLen, iv_.data());
  }

  /// @brief Create a CBC en/decryption cipher
  /// @param key Cipher key (typically responder router hash)
  /// @param iv Cipher IV
  template <
      class Key,
      typename = std::enable_if_t<std::is_same<Key, key_t>::value || std::is_same<Key, key_t::buffer_t>::value>>
  void rekey(const Key& key)
  {
    const exception::Exception ex{"AES", __func__};

    check_key(key, ex);

    std::copy_n(key.data(), KeyLen, key_.data());
  }

  /// @brief Create an AES key and IV
  /// @return AES key and IV
  static key_iv_t create_key_iv()
  {
    return key_iv_t{{RandBytes<KeyLen>()}, {RandBytes<IVLen>()}};
  }

  static iv_t create_random_iv()
  {
    return crypto::RandBytes<IVLen>();
  }

 private:
  using ctx_t = EVP_CIPHER_CTX;  //< AES context trait alias

  template <
      class Key,
      typename = std::enable_if_t<std::is_same<Key, key_t>::value || std::is_same<Key, key_t::buffer_t>::value>>
  void check_key(const Key& key, const exception::Exception& ex)
  {
    if (key.is_zero())
      ex.throw_ex<std::logic_error>("null key.");
  }

  template <
      class IV,
      typename = std::enable_if_t<std::is_same<IV, iv_t>::value || std::is_same<IV, iv_t::buffer_t>::value>>
  void check_iv(const IV& iv, const exception::Exception& ex)
  {
    if (iv.is_zero())
      ex.throw_ex<std::logic_error>("null IV.");
  }

  enum struct Mode : int
  {
    Decrypt,
    Encrypt,
  };

  void ProcessCBC(
      std::uint8_t* in_out,
      const std::size_t in_out_len,
      const Mode mode,
      const exception::Exception& ex)
  {
    if (!in_out)
      ex.throw_ex<std::invalid_argument>("null buffers.");

    if (!in_out_len)
      ex.throw_ex<std::invalid_argument>("null buffer lenghts.");

    if (in_out_len % BlockLen != 0)
      ex.throw_ex<std::length_error>("buffer must be a multiple of AES block size.");

    check_key(key_, ex);
    check_iv(iv_, ex);

    // init AES context
    EVP_CipherInit_ex(
      ctx_,
      EVP_aes_256_cbc(),
      NULL,
      key_.data(),
      iv_.data(),
      static_cast<int>(mode));
    EVP_CIPHER_CTX_set_padding(ctx_, 0);  // no padding

    // perform cipher rounds
    const std::uint32_t rounds(in_out_len / BlockLen);
    int out_len(0);
    for (std::uint32_t i = 0; i < rounds; ++i)
      EVP_CipherUpdate(ctx_, in_out + (i * BlockLen), &out_len, in_out + (i * BlockLen), BlockLen); 

    // finalize and reset the context
    EVP_CipherFinal_ex(ctx_, in_out + ((rounds - 1) * BlockLen), &out_len);
    EVP_CIPHER_CTX_reset(ctx_);
  }

  void ProcessECB(
      std::uint8_t* in_out,
      const std::size_t in_out_len,
      const Mode mode,
      const exception::Exception& ex)
  {
    if (!in_out)
      ex.throw_ex<std::invalid_argument>("null buffers.");

    if (!in_out_len)
      ex.throw_ex<std::invalid_argument>("null buffer lenghts.");

    if (in_out_len % BlockLen != 0)
      ex.throw_ex<std::length_error>("buffer must be a multiple of AES block size.");

    // init AES context
    EVP_CipherInit_ex(
      ctx_,
      EVP_aes_256_ecb(),
      NULL,
      key_.data(),
      NULL,
      static_cast<int>(mode));
    EVP_CIPHER_CTX_set_padding(ctx_, 0);  // no padding

    // perform cipher rounds
    const std::uint32_t rounds(in_out_len / BlockLen);
    int out_len(0);
    for (std::uint32_t i = 0; i < rounds; ++i)
      EVP_CipherUpdate(ctx_, in_out + (i * BlockLen), &out_len, in_out + (i * BlockLen), BlockLen); 

    // finalize and reset the context
    EVP_CipherFinal_ex(ctx_, in_out + ((rounds - 1) * BlockLen), &out_len);
    EVP_CIPHER_CTX_reset(ctx_);
  }

  ctx_t* ctx_;
  key_t key_;
  iv_t iv_;
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_AES_H_
