/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_CRYPTO_ELLIGATOR2_H_
#define SRC_CRYPTO_ELLIGATOR2_H_

#include "src/crypto/bn.h"
#include "src/crypto/x25519.h"

namespace tini2p
{
namespace crypto
{
/// @brief BigNum of 2^255 - 19
static const BigNum BN_P25519(crypto::SecBytes{0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                               0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                               0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed});

static const BigNum BN_ZERO(0);

static const BigNum BN_ONE(1);

static const BigNum BN_TWO(2);

static const BigNum BN_U(2);

/// @brief BigNum of 1 / u mod P25519
static const BigNum BN_INVERSE_U(crypto::SecBytes{0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                                  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                                  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf7});

/// @brief BigNum of -1 mod P25519
static const BigNum BN_NEG_ONE(crypto::SecBytes{0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                                0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                                0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xec});

/// @brief BigNum of A: 486662
static const BigNum BN_A(486662);

/// @brief BigNum of -A mod P25519
static const BigNum BN_NEG_A(crypto::SecBytes{0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                              0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                              0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0x92, 0xe7});

/// @brief BigNum for Chi(x) exponent: (P25519 - 1) / 2
/// @detail Also called the legendre symbol, used to calculate the squareness of a number mod P25519
static const BigNum BN_P25519_MIN_ONE_DIV_2(crypto::SecBytes{
    0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf6});

/// @brief BigNum for the squareroot (mod P25519) exponent: (P25519 + 3) / 8
static const BigNum BN_P25519_ADD_3_DIV_8(crypto::SecBytes{
    0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe});

/// @brief BigNum for sqrt(-1) mod P25519: 2^((P25519 - 1) / 4)
/// @detail Algorithm for computing sqrt(-1) mod P25519 originally from Java I2P obfs4 Elligator2 code
///     Ported from the Jan. 13, 2016 C version at https://github.com/Kleshni/Elligator-2
static const BigNum BN_SQRT_NEGATIVE_ONE(crypto::SecBytes{
    0x2b, 0x83, 0x24, 0x80, 0x4f, 0xc1, 0xdf, 0x0b, 0x2b, 0x4d, 0x00, 0x99, 0x3d, 0xfb, 0xd7, 0xa7,
    0x2f, 0x43, 0x18, 0x06, 0xad, 0x2f, 0xe4, 0x78, 0xc4, 0xee, 0x1b, 0x27, 0x4a, 0x0e, 0xa0, 0xb0});

/// @class Elligator2
/// @brief Elligator2 map implementation
class Elligator2
{
 public:
  enum
  {
    EncodedKeyLen = 32,
    PublicKeyLen = 32,
    PrivateKeyLen = 32,
  };

  struct EncodedKey : public Key<PublicKeyLen>
  {
    using base_t = Key<EncodedKeyLen>;
    using hasher_t = KeyHasher<EncodedKeyLen>;  //< Hasher trait alias

    using pointer = EncodedKey*;  //< Non-owning pointer trait alias
    using const_pointer = const EncodedKey*;  //< Const non-owning pointer trait alias
    using unique_ptr = std::unique_ptr<EncodedKey>;  //< Unique pointer trait alias
    using const_unique_ptr = std::unique_ptr<const EncodedKey>;  //< Const unique pointer trait alias
    using shared_ptr = std::shared_ptr<EncodedKey>;  //< Shared pointer trait alias
    using const_shared_ptr = std::shared_ptr<const EncodedKey>;  //< Const shared pointer trait alias

    EncodedKey() : base_t() {}

    EncodedKey(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

    EncodedKey(const SecBytes& buf) : base_t(buf) {}

    EncodedKey(std::initializer_list<std::uint8_t> list) : base_t(list) {}
  };

  struct PublicKey : public Key<PublicKeyLen>
  {
    using base_t = Key<PublicKeyLen>;
    using hasher_t = KeyHasher<PublicKeyLen>;  //< Hasher trait alias
    using curve_t = X25519;  //< Elliptic curve trait alias

    using pointer = PublicKey*;  //< Non-owning pointer trait alias
    using const_pointer = const PublicKey*;  //< Const non-owning pointer trait alias
    using unique_ptr = std::unique_ptr<PublicKey>;  //< Unique pointer trait alias
    using const_unique_ptr = std::unique_ptr<const PublicKey>;  //< Const unique pointer trait alias
    using shared_ptr = std::shared_ptr<PublicKey>;  //< Shared pointer trait alias
    using const_shared_ptr = std::shared_ptr<const PublicKey>;  //< Const shared pointer trait alias

    PublicKey() : base_t() {}

    PublicKey(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

    PublicKey(const SecBytes& buf) : base_t(buf) {}

    PublicKey(std::initializer_list<std::uint8_t> list) : base_t(list) {}

    /// @brief Cast operator to convert decoded public key to Curve25519
    /// @detail For X25519 DH operations
    explicit operator const curve_t::pubkey_t() const noexcept
    {
      return curve_t::pubkey_t(base_t::buf_);
    }
  };
  
  struct PrivateKey : public Key<PrivateKeyLen>
  {
    using base_t = Key<PrivateKeyLen>;
    using curve_t = X25519;

    PrivateKey() : base_t() {}

    PrivateKey(base_t::buffer_t buf) : base_t(std::forward<base_t::buffer_t>(buf)) {}

    PrivateKey(const SecBytes& buf) : base_t(buf) {}

    PrivateKey(std::initializer_list<std::uint8_t> list) : base_t(list) {}

    /// @brief Cast operator to convert private key to Curve25519
    /// @detail For X25519 DH operations
    explicit operator const curve_t::pvtkey_t() const noexcept
    {
      return curve_t::pvtkey_t(base_t::buf_);
    }
  };

  using curve_t = X25519;  //< Elliptic curve trait alias
  using encoded_key_t = EncodedKey;  //< Encoded key trait alias
  using pubkey_t = PublicKey;  //< Decoded public key trait alias
  using pvtkey_t = PrivateKey;  //< Private key trait alias
  using keypair_t = Keypair<Elligator2>;  //< Keypair trait alias

  /// @brief Encode public key to uniform random string
  /// @detail The letter "x" is used for the Montgomery u-coordinate
  ///    to differentiate from the constant "u" (2) used for the Elligator2 map
  /// @param key Public key to encode to a representative
  /// @param high_y Test parameter for manually selecting the parity of y
  static encoded_key_t Encode(const pubkey_t& key, const std::uint8_t& high_y = 0)
  {
    const auto x = DecodeU(key);

    // Randomly choose whether to take y as +/-squareroot(x^3 + x^2 + x)
    // The choice is recoverable from the produced representative
#if defined(TINI2P_TESTS)
    // Only used for testing Kleshni encoding vectors
    const auto y_is_positive = high_y;
#else
    const auto y_is_positive = crypto::RandInRange(0, 1);
#endif
    const auto y_is_negative = static_cast<std::uint8_t>(!y_is_positive);

    // -(x + A)
    // This cool trick is from Java I2P obfs4 / Kleshni code
    // Because (x + A) is multiplied by all arguments, and the negative is transitive,
    //   we only have to compute -(x + A) once
    const auto neg_x_plus_a = Negate(x.AddMod(BN_A, BN_P25519));

    // return value for positive y: squareroot(x^3 + Ax^2 + x)
    // pos_result = sqrt(-x * inv((x + A) * u))
    //            = sqrt(-x / ((x + A) * u))
    const auto r_positive =
        Squareroot(x.MultiplyMod(neg_x_plus_a.MultiplyMod(BN_U, BN_P25519).ModInverse(BN_P25519), BN_P25519));

    // return value for negative y: -squareroot(x^3 + Ax^2 + x)
    // neg_result = sqrt(-(A + x) * inv(u * x)) mod p
    //            = sqrt(-(A + x) / (ux))
    const auto r_negative =
        Squareroot(neg_x_plus_a.MultiplyMod(BN_U.MultiplyMod(x, BN_P25519).ModInverse(BN_P25519), BN_P25519));

    // Multiply the return values by their branch conditions
    // Only one branch condition will be non-zero (1), the other is zero
    // Values are multiplied and added this way to avoid conditional branching, and leaking timing information
    const auto ret_is_positive = r_positive.Multiply(BigNum(y_is_positive));
    const auto ret_is_negative = r_negative.Multiply(BigNum(y_is_negative));

    return encoded_key_t(ret_is_positive.UAdd(ret_is_negative).element_buffer());
  }

  /// @brief Decode public key from uniform random string
  /// @detail The letter "x" is used for the Montgomery u-coordinate
  ///    to differentiate from the constant u (2) used for the Elligator2 map
  static pubkey_t Decode(const encoded_key_t& key)
  {
    const exception::Exception ex{"Elligator2", __func__};

    if (key.is_zero())
      ex.throw_ex<std::invalid_argument>("null encoded key");

    auto buf = key.buffer();

    // Mask significant bits per X25519 decoding specs
    //buf.back() &= 0x7f;

    const BigNum r(buf, tini2p::Endian::Little);

    // x = -A * inv(1 + ur^2) mod p
    //   = -A / (1 + ur^2) mod p
    const auto x = Square(r)
                       .MultiplyMod(BN_U, BN_P25519)
                       .AddMod(BN_ONE, BN_P25519)
                       .ModInverse(BN_P25519)
                       .MultiplyMod(BN_NEG_A, BN_P25519);

    // w = x(x^2 + Ax + 1) mod p
    const auto a_plus_x = BN_A.AddMod(x, BN_P25519);
    const auto w = Square(x).MultiplyMod(a_plus_x, BN_P25519).AddMod(x, BN_P25519);

    const auto w_is_square = Chi(w) == BN_ONE;
    const auto w_is_non_square = static_cast<std::uint8_t>(!w_is_square);

    // Multiply the return values by their branch conditions
    // Only one branch condition will be non-zero (1), the other is zero
    // Values are multiplied and added this way to avoid conditional branching, and leaking timing information

    // x = x
    const auto ret_is_square = x.Multiply(BigNum(w_is_square));

    // x = -x - A
    const auto ret_is_non_square = Negate(x).SubtractMod(BN_A, BN_P25519).Multiply(BigNum(w_is_non_square));

    return EncodeU(ret_is_non_square.UAdd(ret_is_square));
  }

  /// @brief Negate a BigNum modulo 2^255 - 19
  static crypto::BigNum Negate(const crypto::BigNum& num)
  {
    return BN_P25519.SubtractMod(num, BN_P25519);
  }

  /// @brief Square a number modulo 2^255 - 19
  static crypto::BigNum Square(const crypto::BigNum& num)
  {
    return num.MultiplyMod(num, BN_P25519);
  }

  /// @brief Take the squareroot of a square modulo p
  /// @returns Squareroot of the argument, or zero if the argument is non-square
  static crypto::BigNum Squareroot(const crypto::BigNum& num)
  {
    // check if the argument is square modulo q: one if true, zero if not
    const BigNum a_is_square(static_cast<std::uint8_t>(Chi(num) == BN_ONE));

    // Possible return values, all are computed to maintain constant-time
    // b = a^((q + 3) / 8) mod q: the primary squareroot
    // return if a is square, and b < q / 2
    const auto b = num.ExpMod(BN_P25519_ADD_3_DIV_8, BN_P25519);

    // neg_b = -(a^((q + 3) / 8)) mod q: negation of the primary squareroot
    // return if a is square, and b > q - 1 / 2
    const auto neg_b = Negate(b);

    // b_i = b * sqrt(-1) mod q
    // return if b^2 != a, and b_i < q / 2
    const auto b_i = b.MultiplyMod(BN_SQRT_NEGATIVE_ONE, BN_P25519);

    // neg_b_i = -(b * sqrt(-1)) mod q
    // return if b^2 != a, and b_i > q - 1 / 2
    const auto neg_b_i = Negate(b_i);

    const auto b_sqr = Square(b);

    // b^2 == a, branch condition for returning:
    //   - true: b or -b
    //   - false: b*sqrt(-1) or -(b*sqrt(-1))
    const auto b_is_square = b_sqr == num;
    const auto b_not_square = static_cast<std::uint8_t>(!b_is_square);
    const auto b_is_over_half = b > BN_P25519_MIN_ONE_DIV_2;
    const auto b_is_sub_half = static_cast<std::uint8_t>(!b_is_over_half);

    // b * sqrt(-1) < q / 2: branch condition for returning:
    //   - true: b * sqrt(-1) mod P25519
    //   - false: -(b * sqrt(-1)) mod P25519
    const auto b_i_is_over_half = b_i > BN_P25519_MIN_ONE_DIV_2;
    const auto b_i_is_sub_half = static_cast<std::uint8_t>(!b_i_is_over_half);

    // Multiply each return value by it's associated branch condition
    // Only one combination of branch conditions will be non-zero (1),
    //   so only one return value will be non-zero.
    // If a is non-square, returns 0
    return a_is_square.Multiply(b.Multiply(BigNum(b_is_square * b_is_sub_half))
                                    .UAdd(neg_b.Multiply(BigNum(b_is_square * b_is_over_half)))
                                    .UAdd(b_i.Multiply(BigNum(b_not_square * b_i_is_sub_half)))
                                    .UAdd(neg_b_i.Multiply(BigNum(b_not_square * b_i_is_over_half))));
  }

  /// @brief Get whether the argument is square modulo 2^255 - 19
  /// @detail For more details, see section 5.1 of the Elligator2 paper
  /// @return A BigNum representation of: 1 if square, 0 if zero, -1 mod p if non-square
  static BigNum Chi(const BigNum& num)
  {
    return num.ExpMod(BN_P25519_MIN_ONE_DIV_2, BN_P25519);
  }

  /// @brief Create Elligator2 keypair
  static keypair_t create_keys()
  {
    curve_t::keypair_t keys;
    do
      {
        keys = curve_t::create_keys();
      } while(ValidElligator(keys.pubkey) == 0);

    return {keys.pubkey.buffer(), keys.pvtkey.buffer()};
  }

  /// @brief Decode the Curve25519 u-coordinate from a public key
  static pubkey_t EncodeU(const BigNum& u)
  {
    BigNum::element_t enc_buf;

    for (std::uint8_t i = 0; i < 32; ++i)
      enc_buf[i] = (u >> (8*i)).MaskBits(8);

    return pubkey_t(enc_buf);
  }

  /// @brief Decode the Curve25519 u-coordinate from a public key
  template <
      class TKey,
      class = std::enable_if_t<std::is_same<TKey, curve_t::pubkey_t>::value || std::is_same<TKey, pubkey_t>::value>>
  static BigNum DecodeU(const TKey& key)
  {
    auto dec_buf = key.buffer();

    dec_buf.back() &= 0x7f;

    BigNum u_sum(0);
    for (std::uint8_t i = 0; i < 32; ++i)
      u_sum = u_sum.AddMod(BigNum(dec_buf[i]) << 8*i, BN_P25519);

    return u_sum;
  }

  // @brief Return one if the public key contains a valid u-coordinate for Elligator2 encoding
  /// @detail The letter "x" is used for the Montgomery u-coordinate
  ///    to differentiate from the constant u (2) used for the Elligator2 map
  template <
      class TKey,
      class = std::enable_if_t<std::is_same<TKey, pubkey_t>::value || std::is_same<TKey, curve_t::pubkey_t>::value>>
  static std::uint8_t ValidElligator(const TKey& key)
  {
    const auto& x = DecodeU(key);

    // x != -A
    const auto not_neg_a = x != BN_NEG_A;

    // Check Chi(-ux(x + A)) == 1
    const auto is_square =
        Chi(Negate(x.AddMod(BN_A, BN_P25519).MultiplyMod(x, BN_P25519).MultiplyMod(BN_U, BN_P25519))) == BN_ONE;

    // x <= p - 1 / 2
    const auto sub_half = x <= BN_P25519_MIN_ONE_DIV_2;

    return not_neg_a * is_square * sub_half;
  }

  /// @brief Compute public key from an existing private key
  /// @param keys Keypair containing an existing private key
  static void PrivateToPublic(keypair_t& keys)
  {
    const exception::Exception ex{"Elligator2", __func__};

    if(crypto_scalarmult_curve25519_base(keys.pubkey.data(), keys.pvtkey.data()))
      ex.throw_ex<std::runtime_error>("error deriving public key");

    if (ValidElligator(keys.pubkey) == 0)
      ex.throw_ex<std::logic_error>("invalid Elligator2 public key");
  }

  /// @brief Comput public key from private key, and compare against existing public key in keypair
  /// @throws Runtime error if public key computation fails
  /// @return Unsigned integer for valid public key (1 valid, 0 invalid)
  static std::uint8_t ValidPublicKey(const keypair_t& keys)
  {
    const exception::Exception ex{"Elligator2", __func__};

    pubkey_t pubkey;
    if(crypto_scalarmult_curve25519_base(pubkey.data(), keys.pvtkey.data()))
      ex.throw_ex<std::runtime_error>("error deriving public key.");
    
    return (keys.pubkey == pubkey) * ValidElligator(pubkey);
  }
};
}  // namespace crypto
}  // namespace tini2p

#endif  // SRC_CRYPTO_ELLIGATOR2_H_
