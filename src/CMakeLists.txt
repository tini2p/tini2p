# Unlicense
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>

cmake_minimum_required(VERSION 3.10 FATAL_ERROR)
project(tini2p-core CXX)

add_library(tini2p-core
  bytes.h
  time.h

  # Crypto
  crypto/aes.h
  crypto/blake.h
  crypto/chacha_poly1305.h
  crypto/eddsa.h
  crypto/hkdf.h
  crypto/keys.h
  crypto/rand.h
  crypto/sha.h
  crypto/x25519.h
  crypto/dh/x3dh.h

  # Blocks
  data/blocks/blocks.h

  # Router
  data/router/address.h
  data/router/identity.h
  data/router/info.h
  data/router/mapping.h
  data/router/meta.h

  # ECIES
  ecies/x25519.h

  # Exception handling
  exception/exception.h

  # Noise
  noise/noise.h

  # NTCP2
  ntcp2/session_request/session_request.h  # Session Request
  ntcp2/session_created/session_created.h  # Session Created
  ntcp2/session_confirmed/session_confirmed.h  # Session Confirmed
  ntcp2/data_phase/data_phase.h  # Data Phase

  # Session
  ntcp2/session/listener.h
  ntcp2/session/manager.h
  ntcp2/session/session.h)

set_target_properties(tini2p-core PROPERTIES LINKER_LANGUAGE CXX)

target_link_libraries(tini2p-core PRIVATE tini2p-priv)
