/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_NTCP2_DATA_PHASE_MESSAGE_H_
#define SRC_NTCP2_DATA_PHASE_MESSAGE_H_

#include <functional>
#include <mutex>
#include <optional>
#include <shared_mutex>

#include "src/crypto/sec_bytes.h"

#include "src/data/blocks/blocks.h"

namespace tini2p
{
namespace ntcp2
{
/// @class DataPhaseMessage
class DataPhaseMessage
{
 public:
  enum : std::uint32_t
  {
    SizeLen = 2,
    MinLen = SizeLen,
    MaxLen = 65537,  //< Max transport message (SizeLen (2) + uint16_max)
  };

  /// @brief Direction of communication
  enum struct Dir
  {
    AliceToBob,
    BobToAlice
  };

  /// @brief Status of the message
  /// @detail Used by the NTCP2 session handler when reading off the wire
  enum struct Status : bool
  {
    Incomplete,
    Complete,
  };

  using status_t = Status;  //< Status trait alias
  using blocks_t = data::Blocks::data_blocks_t;  //< Blocks trait alias
  using buffer_t = crypto::SecBytes;  //< Buffer trait alias

  /// @brief Default ctor
  DataPhaseMessage() : blocks_(), buffer_(SizeLen), status_(status_t::Incomplete) {}

  /// @brief Copy-ctor
  DataPhaseMessage(const DataPhaseMessage& oth) : blocks_(oth.blocks_), buffer_(oth.buffer_), status_(oth.status_) {}

  /// @brief Move-ctor
  DataPhaseMessage(DataPhaseMessage&& oth)
      : blocks_(std::move(oth.blocks_)), buffer_(std::move(oth.buffer_)), status_(std::move(oth.status_))
  {
  }

  /// @brief Forwarding-assignment operator
  DataPhaseMessage& operator=(DataPhaseMessage oth)
  {
    buffer_ = std::forward<buffer_t>(oth.buffer_);
    status_ = std::forward<status_t>(oth.status_);

    std::scoped_lock sgd(blocks_mutex_);
    blocks_ = std::forward<blocks_t>(oth.blocks_);

    return *this;
  }

  /// @brief Get the size of the DataPhaseMessage
  constexpr std::size_t size() const
  {
    return data::Blocks::TotalSize(blocks_);
  }

  /// @brief Serialize message blocks to buffer
  void serialize()
  {
    const exception::Exception ex{"DataPhase: Message", __func__};

    std::scoped_lock blk_mtx(blocks_mutex_);

    const auto total_size = size();

    if (total_size > MaxLen)
      ex.throw_ex<std::length_error>("invalid total message size.");

    if (buffer_.size() < SizeLen + total_size)
      buffer_.resize(SizeLen + total_size);

    tini2p::BytesWriter<buffer_t> writer(buffer_);
    writer.skip_bytes(SizeLen);  // obfs len written elsewhere

    data::Blocks::CheckBlockOrder(blocks_, ex);

    for (auto& block : blocks_)
      {
        std::visit(
            [&writer](auto& b) {
              b.serialize();
              writer.write_data(b.buffer());
            },
            block);
      }
  }

  /// @brief Deserialize blocks from buffer
  void deserialize()
  {
    const exception::Exception ex{"DataPhase: Message", __func__};

    tini2p::BytesReader<buffer_t> reader(buffer_);
    reader.skip_bytes(SizeLen);

    blocks_t n_blocks;
    while (reader.gcount() > crypto::Poly1305::DigestLen)
      {
          blocks_t::value_type block;
          data::Blocks::ReadToBlock(reader, block, ex);
          data::Blocks::AddBlock(n_blocks, std::move(block), ex);
      }

    std::scoped_lock blk_mtx(blocks_mutex_);
    blocks_.swap(n_blocks);
  }

  template <class TBlock, 
            typename = std::enable_if_t<
                std::is_same<TBlock, data::DateTimeBlock>::value 
                || std::is_same<TBlock, data::I2NPBlock>::value
                || std::is_same<TBlock, data::InfoBlock>::value
                || std::is_same<TBlock, data::NTCP2OptionsBlock>::value
                || std::is_same<TBlock, data::PaddingBlock>::value
                || std::is_same<TBlock, data::TerminationBlock>::value>>
  void add_block(TBlock block)
  {
    const exception::Exception ex{"DataPhase: Message", __func__};

    std::scoped_lock blkg(blocks_mutex_);

    if (data::Blocks::TotalSize(blocks_) + block.size() > MaxLen)
      ex.throw_ex<std::length_error>("invalid total message size.");

    data::Blocks::AddBlock(blocks_, blocks_t::value_type(block), ex);
  }

  /// @brief Get the block at a given index
  /// @throw Invalid argument on out-of-range index
  blocks_t::value_type& get_block(const std::uint16_t index)
  {
    const exception::Exception ex{"DataPhase: Message", __func__};

    if (index >= blocks_.size())
      ex.throw_ex<std::invalid_argument>("index out-of-range.");

    return blocks_[index];
  }

  /// @brief Get the first block of a given type
  /// @param type Type of block to search for
  blocks_t::value_type& get_block(const data::Block::type_t type)
  {
    const exception::Exception ex{"DataPhase: Message", __func__};

    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    const auto it = std::find_if(blocks_.begin(), blocks_.end(), [type](const auto& blk) {
      return std::visit([](const auto& b) { return b.type(); }, blk) == type;
    });

    if (it == blocks_.end())
      {
        ex.throw_ex<std::runtime_error>(
            "DataPhase: Message: no block of search type: " + std::to_string(tini2p::under_cast(type)));
      }

    return *it;
  }

  /// @brief Get whether the message has a block of the given type 
  template <class TBlock>
  std::uint8_t has_block()
  {
    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return has_block<TBlock>(blocks_);
  }

  /// @brief Get a const reference to a message block
  template <class TBlock>
  const TBlock& get_block()
  {
    const exception::Exception ex{"DataPhase: Message", __func__};

    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    return get_block<TBlock>(blocks_, ex);
  }

  /// @brief Extract a block from the message
  template <class TBlock>
  TBlock extract_block()
  {
    const exception::Exception ex{"DataPhase: Message", __func__};

    std::scoped_lock sgd(blocks_mutex_);

    return extract_block<TBlock>(blocks_, ex);
  }

  /// @brief Get a const reference to the message blocks
  const blocks_t& blocks() const noexcept
  {
    return blocks_;
  }

  /// @brief Get a non-const reference to the message blocks
  blocks_t& blocks() noexcept
  {
    return blocks_;
  }

  /// @brief Clear the message blocks
  DataPhaseMessage& clear_blocks()
  {
    std::scoped_lock blkg(blocks_mutex_);
    blocks_.clear();

    return *this;
  }

  /// @brief Get a const reference to the buffer
  const buffer_t& buffer() const noexcept
  {
    return buffer_;
  }

  /// @brief Get a non-const reference to the buffer
  buffer_t& buffer() noexcept
  {
    return buffer_;
  }

  /// @brief Get a const reference to the completion status of the message
  const status_t& status() const noexcept
  {
    return status_;
  }

  /// @brief Set the completion status of the message
  DataPhaseMessage& status(status_t status)
  {
    const exception::Exception ex{"NTCP2: DataPhaseMessage", __func__};

    check_status(status, ex);

    status_ = std::forward<status_t>(status);

    return *this;
  }

  /// @brief Equality comparison with another DataPhaseMessage
  std::uint8_t operator==(const DataPhaseMessage& oth)
  {
    const auto& buf_eq = static_cast<std::uint8_t>(buffer_ == oth.buffer_);
    
    std::shared_lock bs(blocks_mutex_, std::defer_lock);
    std::scoped_lock sgd(bs);

    const auto& block_eq = static_cast<std::uint8_t>(blocks_ == oth.blocks_);

    return (buf_eq * block_eq);
  }

 private:
  void check_status(const status_t& status, const exception::Exception& ex) const
  {
    if (status != status_t::Incomplete && status != status_t::Complete)
      ex.throw_ex<std::invalid_argument>("invalid completion status.");
  }

  template <class TBlock>
  std::uint8_t has_block(const blocks_t& blocks) const
  {
    const auto blocks_end = blocks.end();

    return static_cast<std::uint8_t>(
        std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); })
        != blocks_end);
  }

  template <class TBlock>
  const TBlock& get_block(const blocks_t& blocks, const exception::Exception& ex) const
  {
    const auto blocks_end = blocks.end();

    const auto it =
        std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); });

    if (it == blocks_end)
      ex.throw_ex<std::runtime_error>("no block found for the given type");

    return std::get<TBlock>(*it);
  }

  template <class TBlock>
  TBlock extract_block(blocks_t& blocks, const exception::Exception& ex)
  {
    const auto blocks_end = blocks.end();

    const auto it =
        std::find_if(blocks.begin(), blocks_end, [](const auto& b) { return std::holds_alternative<TBlock>(b); });

    if (it == blocks_end)
      ex.throw_ex<std::runtime_error>("no block found for the given type");

    auto ret_block = std::move(*it);
    blocks.erase(it);

    return std::get<TBlock>(ret_block);
  }

  blocks_t blocks_;
  std::shared_mutex blocks_mutex_;

  buffer_t buffer_;

  status_t status_;
};
}  // namespace ntcp2
}  // namespace tini2p

#endif  // SRC_NTCP2_DATA_PHASE_MESSAGE_H_
