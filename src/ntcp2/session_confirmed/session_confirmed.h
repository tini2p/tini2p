/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_NTCP2_SESSION_CONFIRMED_SESSION_CONFIRMED_H_
#define SRC_NTCP2_SESSION_CONFIRMED_SESSION_CONFIRMED_H_

#include <noise/protocol/handshakestate.h>

#include "src/data/blocks/ntcp2_options.h"
#include "src/data/blocks/padding.h"
#include "src/data/blocks/info.h"

#include "src/ntcp2/session_request/message.h"
#include "src/ntcp2/session_created/message.h"
#include "src/ntcp2/session_confirmed/message.h"

namespace tini2p
{
namespace ntcp2
{
/// @brief Session created message handler
template <class TRole>
class SessionConfirmed
{
 public:
  using role_t = TRole;  //< Role trait alias
  using state_t = noise::HandshakeState;  //< Handshake state trait alias
  using request_msg_t = SessionRequestMessage;  //< SessionRequest message trait alias
  using created_msg_t = SessionCreatedMessage;  //< SessionCreated message trait alias
  using message_t = SessionConfirmedMessage;  //< SessionConfirmed message trait alias
  using kdf_t = SessionCreatedKDF;  //< KDF trait alias

  /// @brief Initialize a session created message handler
  /// @param state Handshake state from successful session requested exchange
  /// @param message SessionCreated message with ciphertext + padding for KDF
  SessionConfirmed(state_t* state, const created_msg_t& message) : state_(state), kdf_(state)
  {
    const exception::Exception ex{"SessionConfirmed", __func__};

    if (!state)
      ex.throw_ex<std::invalid_argument>("null handshake state.");

    kdf_.Derive(message);
  }

  /// @brief Process the session created message based on role
  /// @param message Session created message to process
  /// @throw Runtime error if Noise library returns error
  void ProcessMessage(message_t& message, const request_msg_t::options_t& options)
  {
    if (std::is_same<role_t, Initiator>::value)
      Write(message, options);
    else
      Read(message, options);
  }

 private:
  void Write(message_t& message, const request_msg_t::options_t& options)
  {
    const exception::Exception ex{"SessionConfirmed", __func__};

    NoiseBuffer data /*output*/, payload /*input*/;

    // serialize message blocks to payload buffer
    message.serialize();

    if (options.m3p2_len != message.payload_size())
      ex.throw_ex<std::logic_error>("part two size must equal size sent in SessionRequest.");

    auto& in = message.payload;
    auto& out = message.data;
    const auto& in_size = in.size() - message_t::mac_t::DigestLen;

    noise::RawBuffers bufs{in.data(), in_size, out.data(), out.size()};
    noise::setup_buffers(data, payload, bufs);
    noise::write_message(state_, &data, &payload, ex);
  }

  void Read(message_t& message, const request_msg_t::options_t& options)
  {
    const exception::Exception ex{"SessionConfirmed", __func__};

    NoiseBuffer data /*input*/, payload /*output*/;

    message.payload.resize(static_cast<std::uint16_t>(options.m3p2_len) - message_t::mac_t::DigestLen);

    auto& in = message.data;
    auto& out = message.payload;

    if (in.size() < message_t::MinSize || in.size() > message_t::MaxSize)
      ex.throw_ex<std::length_error>("invalid message size.");

    noise::RawBuffers bufs{in.data(), in.size(), out.data(), out.size()};
    noise::setup_buffers(payload, data, bufs);
    noise::read_message(state_, &data, &payload, ex);

    if (options.m3p2_len != message.payload.size() + message_t::mac_t::DigestLen)
      ex.throw_ex<std::logic_error>("part two size must equal size sent in SessionRequest.");

    // deserialize message blocks from payload buffer
    message.deserialize();
  }

  role_t role_;
  state_t* state_;
  kdf_t kdf_;
};
}  // namespace ntcp2
}  // namespace tini2p

#endif  // SRC_NTCP2_SESSION_CONFIRMED_SESSION_CONFIRMED_H_
