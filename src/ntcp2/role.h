/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 */

#ifndef SRC_NTCP2_ROLE_H_
#define SRC_NTCP2_ROLE_H_

#include <noise/protocol/constants.h>

namespace tini2p
{
namespace ntcp2
{
class Role
{
 public:
  using id_t = int;  //< ID trait alias

  enum struct Initiator
  {
    id = NOISE_ROLE_INITIATOR,
  };

  enum struct Responder
  {
    id = NOISE_ROLE_RESPONDER,
  };

  Role() {}

  virtual ~Role() {}

  /// @brief Get the role ID
  const id_t& id() const noexcept
  {
    return id_;
  }

 protected:
  id_t id_;
};

/// @class Initiator
/// @brief Wrapper for Noise initiator role
class Initiator : public Role
{
 public:
  using role_t = Role::Initiator;  //< Role trait alias

  Initiator()
  {
    id_ = static_cast<id_t>(role_t::id);
  }
};

/// @class Responder
/// @brief Wrapper for Noise responder role
class Responder : public Role
{
 public:
  using role_t = Role::Responder;  //< Role trait alias

  Responder()
  {
    id_ = static_cast<id_t>(role_t::id);
  }
};
}  // namespace ntcp2
}  // namespace tini2p

#endif  // SRC_NTCP2_ROLE_H_
