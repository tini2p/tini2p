/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_NTCP2_SESSION_CREATED_KDF_H_
#define SRC_NTCP2_SESSION_CREATED_KDF_H_

#include "src/ntcp2/session_request/message.h"
#include "src/ntcp2/session_created/message.h"

namespace tini2p
{
namespace ntcp2
{
/// @class SessionCreatedConfirmedKDF
/// @brief Perform key derivation for SessionCreated and SessionConfirmed messages
/// @detail Key derivation is exactly the same for both messages
/// @notes Calls MixHash on the previous message's ciphertext and padding, see spec
class SessionCreatedKDF
{
  noise::HandshakeState* state_;

 public:
  using request_msg_t = SessionRequestMessage;  //< SessionRequest message trait
  using created_msg_t = SessionCreatedMessage;  //< SessionCreated message trait

  SessionCreatedKDF(noise::HandshakeState* state) : state_(state)
  {
    if (!state)
      exception::Exception{"SessionCreatedConfirmedKDF", __func__}
          .throw_ex<std::invalid_argument>("null handshake state.");
  }

  /// @notes Don't free state, handled by owner
  ~SessionCreatedKDF() {}

  /// @brief Derive keys for session created message
  /// @param message Successfully processed session request message
  template <
      class Msg,
      typename = std::enable_if_t<
          std::is_same<Msg, request_msg_t>::value
          || std::is_same<Msg, created_msg_t>::value>>
  void Derive(const Msg& message)
  {
    const exception::Exception ex{"SessionCreatedConfirmedKDF", __func__};

    if (message.ciphertext.empty() || message.padding.empty())
      ex.throw_ex<std::length_error>(
          ("null MixHash parameter(s): ciphertext - "
           + std::to_string(message.ciphertext.size()) + " padding - "
           + std::to_string(message.padding.size()))
              .c_str());

    noise::mix_hash(state_, message.ciphertext, ex);
    noise::mix_hash(state_, message.padding, ex);
  }
};
}  // namespace ntcp2
}  // namespace tini2p

#endif  // SRC_NTCP2_SESSION_CREATED_KDF_H_
