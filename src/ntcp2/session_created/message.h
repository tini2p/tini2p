/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

/// INFO: Separated to avoid forward-declaration of SessionCreatedMessage for SessionCreatedConfirmedKDF

#ifndef SRC_NTCP2_SESSION_CREATED_MESSAGE_H_
#define SRC_NTCP2_SESSION_CREATED_MESSAGE_H_

#include "src/time.h"

#include "src/crypto/poly1305.h"
#include "src/crypto/sec_bytes.h"
#include "src/crypto/x25519.h"

namespace tini2p
{
namespace ntcp2
{
/// @struct SessionCreatedMessage
/// @brief Container for session created message
struct SessionCreatedMessage
{
  enum : std::uint16_t
  {
    OptionsSize = 16,
    YSize = crypto::X25519::PublicKeyLen,
    CiphertextSize = OptionsSize + crypto::Poly1305::DigestLen,
    NoisePayloadSize = YSize + CiphertextSize,
    MinSize = NoisePayloadSize,
    MaxSize = 65535,
    MinPaddingSize = 32,
    MaxPaddingSize = MaxSize - NoisePayloadSize,
  };

  enum : std::uint8_t
  {
    PadLengthOffset = 2,
    TimestampOffset = 8,
    CiphertextOffset = YSize,
    PaddingOffset = NoisePayloadSize,
  };

  /// @brief Container for session request options
  class Options
  {
   public:
    using padlen_t = std::uint16_t;
    using timestamp_t = std::uint32_t;
    using buffer_t = crypto::FixedSecBytes<OptionsSize>;

    padlen_t pad_len;
    timestamp_t timestamp;
    buffer_t buffer;

    Options() : pad_len(crypto::RandInRange(MinPaddingSize, MaxPaddingSize)), timestamp(tini2p::time::now_s())
    {
      serialize();
    }

    Options(const std::uint16_t pad_len) : pad_len(pad_len), timestamp(tini2p::time::now_s())
    {
      serialize();
    }

    /// @brief Updates session created options
    /// @param pad_len Padding length for the session request
    /// @detail As initiator, must call before calling ProcessMessage
    void update(const padlen_t pad_size)
    {
      pad_len = pad_size;
      timestamp = tini2p::time::now_s();
      serialize();
    }

    /// @brief Write request options to buffer
    void serialize()
    {
      check_params({"SessionRequest", __func__});

      tini2p::BytesWriter<buffer_t> writer(buffer);
      writer.write_bytes(pad_len, tini2p::Endian::Big);
      writer.write_bytes(timestamp, tini2p::Endian::Big);
    }

    /// @brief Read request options from buffer
    void deserialize()
    {
      tini2p::BytesReader<buffer_t> reader(buffer);
      reader.read_bytes(pad_len, tini2p::Endian::Big);
      reader.read_bytes(timestamp, tini2p::Endian::Big);

      check_params({"SessionRequest", __func__});
    }

   private:
    void check_params(const exception::Exception& ex)
    {
      if (pad_len > MaxPaddingSize)
        ex.throw_ex<std::length_error>("invalid padding size.");

      if (!time::check_lag_s(timestamp))
        ex.throw_ex<std::runtime_error>("invalid timestamp.");
    }
  };

  using data_t = crypto::SecBytes;  //< Data trait alias
  using padding_t = crypto::SecBytes;  //< Data trait alias
  using options_t = Options;  //< Options trait alias
  using ciphertext_t = crypto::FixedSecBytes<CiphertextSize>;  //< Ciphertext trait alias

  data_t data;
  padding_t padding;
  options_t options;
  ciphertext_t ciphertext;

  /// @brief Create a session created message w/ minimum length
  SessionCreatedMessage() : data(MinSize), options()
  {
    if (options.pad_len)
      {
        padding.resize(options.pad_len);
        crypto::RandBytes(padding.data(), padding.size());
      }
  }

  /// @brief Create a session created message w/ specified padding length
  /// @para pad_len Length of padding to include
  SessionCreatedMessage(const std::uint16_t pad_len) : options(pad_len)
  {
    padding.resize(pad_len);
    crypto::RandBytes(padding.data(), padding.size());
  }
};
}  // namespace ntcp2
}  // namespace tini2p

#endif  // SRC_NTCP2_SESSION_CREATED_MESSAGE_H_
