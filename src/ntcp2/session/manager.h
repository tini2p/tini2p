/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_NTCP2_SESSION_MANAGER_H_
#define SRC_NTCP2_SESSION_MANAGER_H_

#include "src/data/router/info.h"

#include "src/ntcp2/session/session.h"
#include "src/ntcp2/session/listener.h"

namespace tini2p
{
namespace ntcp2
{
/// @class SessionManager
/// @brief Class for managing NTCP2 sessions
class SessionManager
{
 public:
  enum
  {
    MinPort = 9111,
    MaxPort = 30777,
  };

  using info_t = data::Info;  //< RouterInfo trait alias
  using listener_t = SessionListener;  //< Session listener trait alias
  using data_message_t = ntcp2::DataPhaseMessage;  //< DataPhaseMessage trait alias
  using data_messages_t = listener_t::data_messages_t;  //< DataPhaseMessage collection trait alias
  using out_session_t = Session<Initiator>;  //< Outbound session trait alias
  using out_sessions_t = std::vector<out_session_t::shared_ptr>;  //< Outbound session container trait alias
  using port_t = std::uint16_t;  //< Port trait alias

  /// @brief Create a session listener for a given RouterInfo and local endpoints
  /// @param info RouterInfo to receive incoming connections
  /// @param ipv4_ep IPv4 Local ASIO endpoint to listen for connections
  /// @param ipv6_ep IPv6 Local ASIO endpoint to listen for connections
  /// @detail Sessions will be created for both IPv4 and IPv6 routers
  SessionManager(
      info_t::shared_ptr info,
      const listener_t::session_t::tcp_t::endpoint& ipv4_ep,
      const listener_t::session_t::tcp_t::endpoint& ipv6_ep)
      : info_(info)
  {
    const exception::Exception ex{"SessionManager", __func__};

    if (!info)
      ex.throw_ex<std::invalid_argument>("null RouterInfo.");

    if (!ipv4_ep.address().is_v4() || !ipv6_ep.address().is_v6())
      ex.throw_ex<std::invalid_argument>("invalid listener endpoints.");

    check_port(ipv4_ep.port(), ex);
    check_port(ipv6_ep.port(), ex);

    listener_ = std::make_shared<listener_t>(info_, ipv4_ep);
    listener_v6_ = std::make_shared<listener_t>(info_, ipv6_ep);

    listener_->Start();
    listener_v6_->Start();
  }

  /// @brief Create a session listener for a given RouterInfo and local endpoint
  /// @param info RouterInfo to receive incoming connections
  /// @param ep Local ASIO endpoint to listen for connections
  /// @detail Local endpoint can be either IPv4 or IPv6
  /// @detail Incoming sessions will only be created for either IPv4 or IPv6 routers
  /// @detail Outgoing sessions will be created for both IPv4 and IPv6 routers
  SessionManager(info_t::shared_ptr info, const listener_t::session_t::tcp_t::endpoint& ep) : info_(info)
  {
    const exception::Exception ex{"SessionManager", __func__};

    if (!info)
      ex.throw_ex<std::invalid_argument>("null RouterInfo.");

    check_port(ep.port(), ex);

    if (ep.address().is_v4())
      {
        listener_ = std::make_shared<listener_t>(info_, ep);
        listener_->Start();
      }
    else
      {
        listener_v6_ = std::make_shared<listener_t>(info_, ep);
        listener_v6_->Start();
      }
  }

  ~SessionManager()
  {
    Stop();
  }

  /// @brief Stop all in/outbound sessions and listeners
  void Stop()
  {
    try
      {
        {  // clean up open sessions
          std::scoped_lock l(out_sessions_mutex_);
          for (const auto& session : out_sessions_)
            session->Stop();

          out_sessions_.clear();
        }  // end session-lock scope

        if (listener_)
          {
            listener_->Stop();
            listener_.reset();
          }

        if (listener_v6_)
          {
            listener_v6_->Stop();
            listener_v6_.reset();
          }
      }
    catch (const std::exception& ex)
      {
        std::cerr << "SessionManager: " << __func__ << ": " << ex.what();
      }
  }

  /// @brief Has established outbound NTCP2 sessions
  std::uint8_t has_outbound_sessions()
  {
    std::shared_lock os(out_sessions_mutex_, std::defer_lock);
    std::scoped_lock sgd(os);

    return static_cast<std::uint8_t>(!out_sessions_.empty());
  }

  /// @brief Has established inbound NTCP2 sessions
  std::uint8_t has_inbound_sessions()
  {
    const auto& has_v4_sessions = static_cast<std::uint8_t>(listener_ && listener_->has_sessions());
    const auto& has_v6_sessions = static_cast<std::uint8_t>(listener_v6_ && listener_v6_->has_sessions());

    return (has_v4_sessions * has_v6_sessions);
  }

  /// @brief Read any bytes available on listener session sockets
  SessionManager& Read()
  {
    if (listener_)
      listener_->Read();

    if (listener_v6_)
      listener_v6_->Read();

    return *this;
  }

  /// @brief Drain completed inbound session messages
  listener_t::data_messages_t drain_messages()
  {
    data_messages_t messages;

    if (listener_)
       messages = listener_->drain_messages();

    if (listener_v6_)
      {
        if (messages.empty())
          messages = listener_v6_->drain_messages();
        else
          {
            auto msgs = listener_v6_->drain_messages();
            messages.insert(messages.end(), msgs.begin(), msgs.end());
          }
      }

    return messages;
  }

  /// @brief Create a new outbound session to a remote router
  /// @param remote_info Pointer to remote RouterInfo
  /// @return Non-owning ointer to newly created session
  /// @throw Invalid argument if remote router is null
  /// @throw Runtime error if session already exists for the remote router
  out_session_t& session(info_t::shared_ptr remote_info)
  {
    const exception::Exception ex{"SessionManager", __func__};

    if (!remote_info)
      ex.throw_ex<std::invalid_argument>("null RouterInfo.");

    std::scoped_lock l(out_sessions_mutex_);

    // search for existing session to given remote router
    const auto& id_crypto = remote_info->identity().crypto();

    if (blacklisted(id_crypto.pubkey))
      ex.throw_ex<std::runtime_error>("session remote router is blacklisted.");

    const auto it = std::find_if(
        out_sessions_.begin(), out_sessions_.end(), [&id_crypto](const auto& session) -> std::uint8_t {
          return session->key() == id_crypto.pubkey;
        });

    if (it != out_sessions_.end())
      {
        const std::string err_msg("session already exists for key: " + crypto::Base64::Encode((*it)->key()));

        blacklist_.emplace(std::move((*it)->key()));
        blacklist_.emplace(std::move((*it)->connect_key()));

        out_sessions_.erase(it);
        ex.throw_ex<std::runtime_error>(std::move(err_msg));
      }

    out_sessions_.emplace_back(std::make_shared<out_session_t>(remote_info, info_));

    return *out_sessions_.back();
  }

  /// @brief Get whether the manager has an outbound session matching the given key
  /// @param key Static public key for the remote router
  std::uint8_t has_outbound_session(const out_session_t::key_t& key)
  {
    std::shared_lock os(out_sessions_mutex_, std::defer_lock);
    std::scoped_lock l(os);

    return has_session(out_sessions_, key);
  }

  /// @brief Find an outbound session matching the given key
  /// @param key Static public key for the remote router
  /// @throws Runtime error if no session is found
  out_session_t& find_outbound_session(const out_session_t::key_t& key)
  {
    const exception::Exception ex{"NTCP2: SessionManager", __func__};

    std::shared_lock os(out_sessions_mutex_, std::defer_lock);
    std::scoped_lock l(os);

    return find_session(out_sessions_, key, ex);
  }

  /// @brief Get a const shared pointer to a session listener
  /// @param ip IP protocol of the listener to retrieve
  /// @throw Invalid argument if ip is invalid protocol
  const listener_t& listener(const listener_t::session_t::meta_t::IP& ip = listener_t::session_t::meta_t::IP::v4) const
  {
    using IP = listener_t::session_t::meta_t::IP;

    const exception::Exception ex{"SessionManager", __func__};

    if (ip != IP::v4 && ip != IP::v6)
      ex.throw_ex<std::invalid_argument>("invalid listener protocol.");

    if (ip == IP::v4)
      check_listener(listener_, ex);
    else
      check_listener(listener_v6_, ex);

    return ip == IP::v4 ? *listener_ : *listener_v6_;
  }

  /// @brief Get a non-const shared pointer to a session listener
  /// @param ip IP protocol of the listener to retrieve
  /// @throw Invalid argument if ip is invalid protocol
  listener_t& listener(const listener_t::session_t::meta_t::IP& ip = listener_t::session_t::meta_t::IP::v4)
  {
    using IP = listener_t::session_t::meta_t::IP;

    const exception::Exception ex{"SessionManager", __func__};

    if (ip != IP::v4 && ip != IP::v6)
      ex.throw_ex<std::invalid_argument>("invalid listener protocol.");

    if (ip == IP::v4)
      check_listener(listener_, ex);
    else
      check_listener(listener_v6_, ex);

    return ip == IP::v4 ? *listener_ : *listener_v6_;
  }

  /// @brief Get the blacklisted status of a session key
  bool blacklisted(const listener_t::key_t& key) const
  {
    const bool out_bl = blacklist_.find(key) != blacklist_.end();
    const bool listenv4_bl = listener_ && listener_->blacklisted(key);
    const bool listenv6_bl = listener_v6_ && listener_v6_->blacklisted(key);

    return (out_bl || listenv4_bl || listenv6_bl);
  }

 private:
  void check_port(const port_t& port, const exception::Exception& ex) const
  {
    const auto& min_port = tini2p::under_cast(MinPort);
    const auto& max_port = tini2p::under_cast(MaxPort);

    if (port < min_port || port > max_port)
      {
        ex.throw_ex<std::logic_error>(
            "invalid port: " + std::to_string(port) + ", min: " + std::to_string(min_port)
            + ", max: " + std::to_string(max_port));
      }
  }

  std::uint8_t has_session(const out_sessions_t& sessions, const out_session_t::key_t& key) const
  {
    const auto ses_end = sessions.end();

    return static_cast<std::uint8_t>(
        std::find_if(sessions.begin(), ses_end, [&key](const auto& s) { return s && s->key() == key; }) != ses_end);
  }

  out_session_t& find_session(out_sessions_t& sessions, const out_session_t::key_t& key, const exception::Exception& ex) const
  {
    const auto ses_end = sessions.end();
    auto it =  std::find_if(sessions.begin(), ses_end, [&key](const auto& s) { return s && s->key() == key; });

    if (it == ses_end)
      ex.throw_ex<std::runtime_error>("no session found for key: " + tini2p::bin_to_hex(key, ex));

    if (*it == nullptr)
      ex.throw_ex<std::runtime_error>("null session");

    return *(*it);
  }

  void check_listener(const listener_t::shared_ptr& listener, const exception::Exception& ex) const
  {
    if (listener == nullptr)
      ex.throw_ex<std::runtime_error>("null listener");
  }

  info_t::shared_ptr info_;
  listener_t::shared_ptr listener_, listener_v6_;

  out_sessions_t out_sessions_;
  std::shared_mutex out_sessions_mutex_;

  listener_t::blacklist_t blacklist_;
};
}  // namespace ntcp2
}  // namespace tini2p

#endif  // SRC_NTCP2_SESSION_MANAGER_H_
