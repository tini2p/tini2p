/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_NTCP2_SESSION_SESSION_H_
#define SRC_NTCP2_SESSION_SESSION_H_

#include <shared_mutex>
#include <unordered_map>

#include <asio.hpp>

#include "src/data/router/info.h"

#include "src/ntcp2/noise.h"

#include "src/ntcp2/session_request/session_request.h"
#include "src/ntcp2/session_created/session_created.h"
#include "src/ntcp2/session_confirmed/session_confirmed.h"
#include "src/ntcp2/data_phase/data_phase.h"

#include "src/ntcp2/session/key.h"

namespace tini2p
{
namespace ntcp2
{
struct SessionMeta
{
enum : std::uint32_t
{
  CleanTimeout = 5000,  //< in milliseconds
  WaitTimeout = 3000,  //< in milliseconds
  MaxSessions = 3,  //< max sessions in CleanTimeout
  MaxConnections = 11,  //< max connections in CleanTimeout
  ShutdownTimeout = 13, //< in milliseconds, somewhat arbitrary, adjust based on performance tests
};

enum struct IP : bool
{
  v4,
  v6,
};
};

/// @class Session
/// @tparam TRole Noise role for the first data phase message
/// @detail On first data phase message, session initiator will be responder, vice versa
template <class TRole>
class Session
{
 public:
  using role_t = TRole; //< NTCP2 Session role trait alias
  using initiator_r = ntcp2::Initiator;  //< NTCP2 initiator role
  using responder_r = ntcp2::Responder;  //< NTCP2 responder role
  using state_t = NoiseHandshakeState;  //< Handshake state trait alias
  using meta_t = SessionMeta;  //< Meta trait alias
  using info_t = data::Info;  //< RouterInfo trait alias
  using obfse_t = crypto::AES;  //< Obfse crypto trait alias
  using key_t = crypto::X25519::pubkey_t;  //< Key trait alias

  /// @alias created_impl_t
  /// @brief SessionRequest implementation trait
  using request_impl_t =
      std::conditional_t<std::is_same<role_t, Initiator>::value, SessionRequest<Initiator>, SessionRequest<Responder>>;

  /// @alias created_impl_t
  /// @brief SessionCreated implementation trait
  using created_impl_t =
      std::conditional_t<std::is_same<role_t, Initiator>::value, SessionCreated<Responder>, SessionCreated<Initiator>>;

  /// @alias confirmed_impl_t
  /// @brief SessionConfirmed implementation trait
  using confirmed_impl_t = std::
      conditional_t<std::is_same<role_t, Initiator>::value, SessionConfirmed<Initiator>, SessionConfirmed<Responder>>;

  /// @alias data_impl_t
  /// @brief DataPhase implementation trait
  using data_impl_t =
      std::conditional_t<std::is_same<role_t, Initiator>::value, DataPhase<Responder>, DataPhase<Initiator>>;

  using request_msg_t = typename request_impl_t::message_t;  //< SessionRequest message trait alias
  using created_msg_t = typename created_impl_t::message_t;  //< SessionCreated message trait alias
  using confirmed_msg_t = typename confirmed_impl_t::message_t;  //< SessionConfirmed message trait alias
  using data_msg_t = typename data_impl_t::message_t;  //< DataPhase message trait alias
  using data_messages_t = std::vector<data_msg_t>;  //< DataPhase message collection trait alias
  using message_queue_t = std::unordered_map<std::uint32_t, data_msg_t>;  //< DataPhase message queue trait alias

  using context_t = asio::io_context;  //< ASIO context trait alias
  using work_guard_t = asio::executor_work_guard<asio::io_context::executor_type>;  //< Work guard trait alias
  using tcp_t = asio::ip::tcp;  //< TCP trait alias
  using error_c = asio::error_code;  //< Error code trait alias

  using pointer = Session<role_t>*;  //< Non-owning pointer trait alias
  using const_pointer = const Session<role_t>*;  //< Const non-owning pointer trait alias
  using unique_ptr = std::unique_ptr<Session<role_t>>;  //< Unique pointer trait alias
  using const_unique_ptr = std::unique_ptr<const Session<role_t>>;  //< Const unique pointer trait alias
  using shared_ptr = std::shared_ptr<Session<role_t>>;  //< Shared pointer trait alias
  using const_shared_ptr = std::shared_ptr<const Session<role_t>>;  //< Const shared pointer trait alias

  /// @brief Create a session for a remote router
  /// @param remote_info RouterInfo for remote router
  /// @param info RouterInfo for local router
  Session(info_t::shared_ptr remote_info, info_t::shared_ptr info)
      : remote_info_(remote_info),
        info_(info),
        ctx_(),
        work_guard_(std::make_unique<work_guard_t>(asio::make_work_guard(ctx_))),
        sock_(ctx_),
        strand_(ctx_),
        ready_(false)
  {
    const exception::Exception ex{"Session", __func__};

    if (!remote_info_ || !info_)
      ex.throw_ex<std::invalid_argument>("null remote or local RouterInfo.");

    sco_msg_.reset(new confirmed_msg_t(
        info_, crypto::RandInRange(confirmed_msg_t::MinPaddingSize, confirmed_msg_t::MaxPaddingSize - info_->size())));

    noise::init_handshake<Initiator>(&state_, ex);

    std::copy_n(
        crypto::Base64::Decode(remote_info_->options().entry(std::string("s"))).data(),
        remote_key_.size(),
        remote_key_.data());

    std::copy_n(
        crypto::Base64::Decode(remote_info_->options().entry(std::string("i"))).data(), aes_iv_.size(), aes_iv_.data());
  }

  /// @brief Create a session for an incoming connection
  /// @param info RouterInfo for local router
  Session(info_t::shared_ptr info, asio::io_context& ctx)
      : info_(info),
        work_guard_(),
        sock_(ctx),
        strand_(ctx),
        ready_(false)
  {
    const exception::Exception ex{"Session", __func__};

    if (!info_)
      ex.throw_ex<std::invalid_argument>("null RouterInfo.");

    noise::init_handshake<Responder>(&state_, ex);

    std::copy_n(
        crypto::Base64::Decode(info_->options().entry(std::string("i"))).data(), aes_iv_.size(), aes_iv_.data());
  }

  ~Session()
  {
    Stop();
    sock_.close();
    noise::free_handshake(state_);
  }

  /// @brief Start the NTCP2 session
  Session& Start(const meta_t::IP proto = meta_t::IP::v4)
  {
    if (std::is_same<TRole, Initiator>::value)
      Connect(proto);
    else
      HandleSessionRequest();

    return *this;
  }

  /// @brief Wait for session to be ready
  /// @throw Runtime error on handshake timeout
  Session& Wait()
  {
    const exception::Exception ex{"Session", __func__};

    std::unique_lock<std::mutex> l(ready_mutex_);
    if (!cv_.wait_for(l, std::chrono::milliseconds(meta_t::WaitTimeout), [this]() { return ready_; }))
      {
        l.unlock();
        Stop();
        ex.throw_ex<std::runtime_error>("handshake timed out.");
      }
    l.unlock();
    cv_.notify_all();

    return *this;
  }

  /// @brief Stop the session
  Session& Stop()
  {
    try
      {
        if (work_guard_ != nullptr)
          work_guard_->reset();

        ctx_.stop();
        ready_ = false;
        if (thread_)
          {
            thread_->join();
            thread_.reset(nullptr);
          }
      }
    catch (const std::exception& ex)
      {
        std::cerr << "Session: " << __func__ << ": " << ex.what() << std::endl;
      }

    return *this;
  }

  /// @brief Write a data phase message
  /// @param message Data phase message to write
  Session& Write(data_msg_t& message)
  {
    const exception::Exception ex{"Session", __func__};

    if (!ready_)
      ex.throw_ex<std::runtime_error>("session not ready for data phase.");

    check_data_phase(dp_, ex);

    dp_->Write(message);

    auto& msg_buf = message.buffer();

    asio::async_write(sock_, asio::buffer(msg_buf.data(), msg_buf.size()), [&ex](const error_c& ec, std::size_t) {
      if (ec && ec != asio::error::eof)
        ex.throw_ex<std::runtime_error>(ec.message().c_str());
    });

    return *this;
  }

  /// @brief Read a data phase message
  /// @param message Data phase message to store read results
  Session& Read()
  {
    const exception::Exception ex{"Session", __func__};

    if (!ready_)
      ex.throw_ex<std::runtime_error>("session not ready for data phase.");

    check_data_phase(dp_, ex);

    const auto msg_id = crypto::RandInRange();

    const auto read_completion_handler = [this, ex, msg_id](const error_c& ec, std::size_t) {
      if (ec && ec != asio::error::eof)
        ex.throw_ex<std::runtime_error>(ec.message().c_str());

      std::scoped_lock sgd(data_messages_mutex_);

      auto msg_it = data_messages_.find(msg_id);

      if (msg_it == data_messages_.end())
        ex.throw_ex<std::runtime_error>("no message found with ID: " + std::to_string(msg_id));

      auto& msg_buf = msg_it->second.buffer();

      std::uint16_t obfs_len;
      tini2p::read_bytes(msg_buf.data(), obfs_len);  // endian conversion handled by ProcessLength

      // get the direction the message was sent
      const auto dir =
          std::is_same<role_t, initiator_r>::value ? data_msg_t::Dir::BobToAlice : data_msg_t::Dir::AliceToBob;

      dp_->kdf().ProcessLength(obfs_len, dir, tini2p::Endian::Big);

      if (obfs_len)
        {
          dp_xfer_ = 0;
          msg_buf.resize(data_msg_t::SizeLen + obfs_len);

          // read remaing message bytes
          const auto data_completion_handler = [this, ex, obfs_len, msg_id](const error_c& ec, std::size_t) {
            if (ec && ec != asio::error::eof)
              ex.throw_ex<std::runtime_error>(ec.message().c_str());

            std::scoped_lock mgd(data_messages_mutex_);

            auto msg_it = data_messages_.find(msg_id);

            if (msg_it == data_messages_.end())
              ex.throw_ex<std::runtime_error>("no message found with ID: " + std::to_string(msg_id));

            // write deobfuscated length back to message
            tini2p::write_bytes(msg_it->second.buffer().data(), obfs_len);
            dp_->Read(msg_it->second, false /*deobfs len*/);
            msg_it->second.status(data_msg_t::status_t::Complete);
          };

          asio::async_read(
              sock_,
              asio::buffer(msg_buf.data() + tini2p::under_cast(data_msg_t::SizeLen), obfs_len),
              data_completion_handler);
        }
    };

    if (sock_.available() != 0)
      {
        std::scoped_lock dgd(data_messages_mutex_);

        auto inserted = data_messages_.try_emplace(msg_id, data_msg_t());

        // read message length from the socket
        if (inserted.second)
          {
            asio::async_read(
                sock_,
                asio::buffer(inserted.first->second.buffer().data(), data_msg_t::SizeLen),
                read_completion_handler);
          }
        else
          ex.throw_ex<std::runtime_error>("message already exists with ID: " + std::to_string(msg_id));
      }

    return *this;
  }

  /// @brief Get a non-const reference to the socket
  /// @return Reference to session socket
  tcp_t::socket& socket() noexcept
  {
    return sock_;
  }

  /// @brief Get if session is ready for processing data phase messages
  /// @return Session ready status
  bool ready() const noexcept
  {
    return ready_;
  }

  /// @brief Get a const reference to the session key
  /// @detail Keyed under Bob's key for outbound connections, and under Alice's for inbound connections
  const key_t& key() const noexcept
  {
    return remote_key_;
  }

  /// @brief Get a const reference to the connection key
  /// @detail Keyed as a Sha256 hash of initiating endpoint address
  const key_t& connect_key() const noexcept
  {
    return connect_key_;
  }

  /// @brief Get whether the session has DataPhase messages in the queue
  std::uint8_t has_completed_messages()
  {
    std::shared_lock ds(data_messages_mutex_, std::defer_lock);
    std::scoped_lock sgd(ds);

    const auto messages_end = data_messages_.end();
    return static_cast<std::uint8_t>(
        std::find_if(
            data_messages_.begin(),
            messages_end,
            [](const auto& m) { return m.second.status() == data_msg_t::status_t::Complete; })
        != messages_end);
  }

  /// @brief Drain completed DataPhase messages from the queue
  /// @return Collection of DataPhase messages
  decltype(auto) drain_messages()
  {
    data_messages_t ret_msgs;

    {  // lock data messages
      std::scoped_lock sgd(data_messages_mutex_);

      for (auto msg_it = data_messages_.begin(); msg_it != data_messages_.end();)
        {
          if (msg_it->second.status() == data_msg_t::status_t::Complete)
            {
              ret_msgs.emplace_back(std::move(msg_it->second));
              msg_it = data_messages_.erase(msg_it);
            }
          else
            ++msg_it;
        }
    }  // end-data-message-lock

    return ret_msgs;
  }

 private:
  void CalculateConnectKey()
  {
    const auto& host = std::is_same<TRole, Initiator>::value
                           ? sock_.local_endpoint().address().to_string()
                           : sock_.remote_endpoint().address().to_string();

    crypto::Sha256::Hash(connect_key_.buffer(), host);
  }

  void Run()
  {
    const auto func = __func__;
    thread_ = std::make_unique<std::thread>([this, func]() {
      try
        {
          ctx_.run();
        }
      catch (const std::exception& ex)
        {
          std::cerr << "Session: " << func << ": " << ex.what() << std::endl;
          Stop();
        }
    });
  }

  void Connect(const meta_t::IP proto)
  {
    error_c ec;
    remote_host_ = remote_info_->host(static_cast<bool>(proto));

    const exception::Exception ex{"Session", __func__};

    sock_.open(remote_host_.protocol());
    sock_.set_option(tcp_t::socket::reuse_address(true));
    sock_.bind(tcp_t::endpoint(remote_host_.protocol(), 0), ec);
    if (ec)
      ex.throw_ex<std::runtime_error>(ec.message().c_str());

    const auto connect_handler = [this, ex](const error_c& ec) {
      if (ec)
        ex.throw_ex<std::runtime_error>(ec.message().c_str());

      HandleSessionRequest();
    };

    sock_.async_connect(remote_host_, strand_.wrap(connect_handler));

    Run();
  }

  void HandleSessionRequest()
  {

    if (std::is_same<TRole, Initiator>::value)
      DoSessionRequest();
    else
      {
        const exception::Exception ex{"Session", __func__};

        const auto do_session_request = [this, ex](const error_c& ec) {
          if (ec)
            ex.throw_ex<std::runtime_error>(ec.message().c_str());

          DoSessionRequest();
        };
        sock_.async_wait(tcp_t::socket::wait_read, strand_.wrap(do_session_request));
       }

    CalculateConnectKey();
  }

  void DoSessionRequest()
  {
    const exception::Exception ex{"Session", __func__};

    if (std::is_same<TRole, Initiator>::value)
      {
        check_info(info_, ex);
        check_info(remote_info_, ex);

        const auto& ident = remote_info_->identity();
        request_impl_t srq(state_, ident.hash(), aes_iv_);
        auto& kdf = srq.kdf();
        
        kdf.set_local_keys(info_->identity().crypto());
        kdf.Derive(remote_key_);

        std::scoped_lock lg(msg_mutex_);
        srq_msg_ = std::make_unique<request_msg_t>(
            sco_msg_->payload_size(),
            crypto::RandInRange(request_msg_t::MinPaddingSize, request_msg_t::MaxPaddingSize));

        srq.ProcessMessage(*srq_msg_);

        const auto write_completion_handler = [this, ex](const error_c& ec, const std::size_t) {
          if (ec && ec != asio::error::eof)
            ex.throw_ex<std::runtime_error>(ec.message());

          HandleSessionCreated();
        };

        asio::async_write(
            sock_, asio::buffer(srq_msg_->data.data(), srq_msg_->data.size()), strand_.wrap(write_completion_handler));
      }
    else
      {
        std::scoped_lock lg(msg_mutex_);
        srq_msg_ = std::make_unique<request_msg_t>();

        const auto read_completion_handler =
            [this, ex](const error_c& ec, const std::size_t) {
              if (ec && ec != asio::error::eof)
                ex.throw_ex<std::runtime_error>(ec.message().c_str());

              check_info(info_, ex);

              const auto& ident = info_->identity();
              request_impl_t srq(state_, ident.hash(), aes_iv_);
              auto& kdf = srq.kdf();
              kdf.set_local_keys(ident.crypto());
              srq.kdf().Derive();
              srq.ProcessMessage(*srq_msg_);

              if (srq_msg_->options.pad_len)
                {
                  srq_xfer_ = 0;
                  srq_msg_->padding.resize(srq_msg_->options.pad_len);

                  const auto padding_completion_handler = [this, ex](const error_c& ec, const std::size_t) {
                    if (ec && ec != asio::error::eof)
                      ex.throw_ex<std::runtime_error>(ec.message().c_str());

                    HandleSessionCreated();
                  };

                  asio::async_read(
                      sock_,
                      asio::buffer(srq_msg_->padding.data(), srq_msg_->padding.size()),
                      strand_.wrap(padding_completion_handler));
                }
              else
                HandleSessionCreated();
            };

        asio::async_read(
            sock_, asio::buffer(srq_msg_->data.data(), srq_msg_->data.size()), strand_.wrap(read_completion_handler));
      }
  }

  void HandleSessionCreated()
  {
    if (std::is_same<TRole, Initiator>::value)
      {
        const exception::Exception ex{"Session", __func__};
        sock_.async_wait(tcp_t::socket::wait_read, strand_.wrap([this, ex](const error_c& ec) {
          if (ec && ec != asio::error::eof)
            ex.throw_ex<std::runtime_error>(ec.message().c_str());

          DoSessionCreated();
        }));
      }
    else
      DoSessionCreated();
  }

  void DoSessionCreated()
  {
    const exception::Exception ex{"Session", __func__};

    if (std::is_same<TRole, Initiator>::value)
      {
        std::scoped_lock lg(msg_mutex_);
        scr_msg_ = std::make_unique<created_msg_t>();

        const auto read_completion_handler =
            [this, ex](const error_c& ec, const std::size_t bytes_transferred) {
              if (ec && ec != asio::error::eof)
                ex.throw_ex<std::runtime_error>(ec.message().c_str());

              created_impl_t scr(
                  state_, *srq_msg_, remote_info_->identity().hash(), aes_iv_);

              scr.ProcessMessage(*scr_msg_);
              if (scr_msg_->options.pad_len)
                {
                  scr_xfer_ = 0;
                  scr_msg_->padding.resize(scr_msg_->options.pad_len);
                  asio::async_read(
                      sock_,
                      asio::buffer(scr_msg_->padding.data(), scr_msg_->options.pad_len),
                      strand_.wrap([this, ex](const error_c& ec, const std::size_t) {
                        if (ec && ec != asio::error::eof)
                          ex.throw_ex<std::runtime_error>(ec.message().c_str());

                        HandleSessionConfirmed();
                      }));
                }
              else
                ex.throw_ex<std::length_error>("null padding length.");
            };

        asio::async_read(
            sock_,
            asio::buffer(scr_msg_->data.data(), created_msg_t::NoisePayloadSize),
            strand_.wrap(read_completion_handler));
      }
    else
      {
        std::scoped_lock lg(msg_mutex_);
        scr_msg_ = std::make_unique<created_msg_t>();

        created_impl_t scr(
            state_, *srq_msg_, info_->identity().hash(), aes_iv_);

        scr.ProcessMessage(*scr_msg_);
        asio::async_write(
            sock_,
            asio::buffer(scr_msg_->data.data(), scr_msg_->data.size()),
            strand_.wrap([this, ex](const error_c& ec, const std::size_t) {
              if (ec && ec != asio::error::eof)
                ex.throw_ex<std::runtime_error>(ec.message().c_str());

              HandleSessionConfirmed();
            }));
      }
  }

  void HandleSessionConfirmed()
  {
    if (std::is_same<TRole, Initiator>::value)
      DoSessionConfirmed();
    else
      {
        const exception::Exception ex{"Session", __func__};
        sock_.async_wait(tcp_t::socket::wait_read, strand_.wrap([this, ex](const error_c& ec) {
          if (ec && ec != asio::error::eof)
            ex.throw_ex<std::runtime_error>(ec.message().c_str());

          DoSessionConfirmed();
        }));
      }
  }

  void DoSessionConfirmed()
  {
    const exception::Exception ex{"Session", __func__};

    if (std::is_same<TRole, Initiator>::value)
      {
        std::scoped_lock lg(msg_mutex_);
        confirmed_impl_t sco(state_, *scr_msg_);

        sco.ProcessMessage(*sco_msg_, srq_msg_->options);

        asio::async_write(
            sock_,
            asio::buffer(sco_msg_->data.data(), sco_msg_->data.size()),
            strand_.wrap([this, ex](const error_c& ec, std::size_t) {
              if (ec && ec != asio::error::eof)
                ex.throw_ex<std::runtime_error>(ec.message().c_str());

              HandleDataPhase();
            }));
      }
    else
      {
        std::scoped_lock lg(msg_mutex_);
        sco_msg_.reset(new confirmed_msg_t(
            confirmed_msg_t::PartOneSize + srq_msg_->options.m3p2_len));

        const auto read_completion_handler =
            [this, ex](const error_c& ec, std::size_t) {
              if (ec && ec != asio::error::eof)
                ex.throw_ex<std::runtime_error>(ec.message().c_str());

              confirmed_impl_t sco(state_, *scr_msg_);
              sco.ProcessMessage(*sco_msg_, srq_msg_->options);

              // get static key from Alice's RouterInfo
              remote_info_ = sco_msg_->info_block.info();
              const crypto::SecBytes s(crypto::Base64::Decode(remote_info_->options().entry(std::string("s"))));

              // get Alice's static key from first frame
              noise::get_remote_public_key(state_, remote_key_, ex);

              // check static key from first frame matches RouterInfo static key, see spec
              const bool match = std::equal(s.begin(), s.end(), remote_key_.begin());

              if (!match)
                ex.throw_ex<std::logic_error>(
                    "static key does not match initial SessionRequest key.");

              HandleDataPhase();
            };

        asio::async_read(
            sock_,
            asio::buffer(sco_msg_->data.data(), sco_msg_->data.size()),
            strand_.wrap(read_completion_handler));
      }
  }

  void HandleDataPhase()
  {
    if (std::is_same<TRole, Initiator>::value)
      {
        const exception::Exception ex{"Session", __func__};

        sock_.async_wait(tcp_t::socket::wait_read, strand_.wrap([this, ex](const error_c& ec) {
          if (ec && ec != asio::error::eof)
            ex.throw_ex<std::runtime_error>(ec.message().c_str());

          DoDataPhase();
        }));
      }
    else
      DoDataPhase();
  }

  void DoDataPhase()
  {
    const exception::Exception ex{"Session", __func__};
    if (std::is_same<TRole, Initiator>::value)
      {
        std::scoped_lock lg(msg_mutex_);
        dp_msg_ = std::make_unique<data_msg_t>();
        dp_msg_->buffer().resize(data_msg_t::MaxLen);

        const auto read_completion_handler =
            [this, ex](const error_c& ec, std::size_t) {
              if (ec && ec != asio::error::eof)
                ex.throw_ex<std::runtime_error>(ec.message().c_str());

              dp_ = std::make_unique<data_impl_t>(state_);

              std::uint16_t obfs_len;
              tini2p::read_bytes(dp_msg_->buffer().data(), obfs_len);  // endian conversion handled by ProcessLength

              dp_->kdf().ProcessLength(obfs_len, data_msg_t::Dir::BobToAlice, tini2p::Endian::Big);

              if(obfs_len)
                {
                  dp_xfer_ = 0;
                  dp_msg_->buffer().resize(data_msg_t::SizeLen + obfs_len);

                  // read remaing message bytes
                  const auto data_completion_handler =
                      [this, ex, obfs_len](const error_c& ec, std::size_t) {
                        if (ec && ec != asio::error::eof)
                          ex.throw_ex<std::runtime_error>(ec.message().c_str());

                        // write deobfuscated length back to message
                        tini2p::write_bytes(dp_msg_->buffer().data(), obfs_len);
                        dp_->Read(*dp_msg_, false /*deobfs len*/);
                        if (std::get<data::InfoBlock>(dp_msg_->get_block(data::Block::type_t::Info))
                                .info()
                                ->options()
                                .entry(std::string("s"))
                            != remote_info_->options().entry(std::string("s")))
                          ex.throw_ex<std::logic_error>("invalid static key.");
                        //--------------------------------------------
                        {
                          std::scoped_lock l(ready_mutex_);
                          ready_ = true;
                        }
                        cv_.notify_all();
                      };

                  asio::async_read(
                      sock_,
                      asio::buffer(&dp_msg_->buffer()[data_msg_t::SizeLen], obfs_len),
                      strand_.wrap(data_completion_handler));
                }
            };

        // read message length from the socket
        asio::async_read(
            sock_, asio::buffer(dp_msg_->buffer().data(), data_msg_t::SizeLen), strand_.wrap(read_completion_handler));
      }
    else
      {
        std::scoped_lock lg(msg_mutex_);
        dp_ = std::make_unique<data_impl_t>(state_);

        dp_msg_ = std::make_unique<data_msg_t>();
        dp_msg_->add_block(data::InfoBlock(info_));

        dp_->Write(*dp_msg_);

        asio::async_write(
            sock_,
            asio::buffer(dp_msg_->buffer().data(), dp_msg_->buffer().size()),
            strand_.wrap([this, ex](const error_c& ec, std::size_t) {
              if (ec && ec != asio::error::eof)
                ex.throw_ex<std::runtime_error>(ec.message().c_str());
              //--------------------------------------------
              {
                std::scoped_lock l(ready_mutex_);
                ready_ = true;
              }
              cv_.notify_all();
            }));
      }
  }

  void check_data_phase(const std::unique_ptr<data_impl_t>& dp, const exception::Exception& ex) const
  {
    if (dp == nullptr)
      ex.throw_ex<std::runtime_error>("null DataPhase.");
  }

  void check_info(const info_t::shared_ptr& info, const exception::Exception& ex) const
  {
    if (info == nullptr)
      ex.throw_ex<std::runtime_error>("null RouterInfo.");

    if (!info->Verify())
      ex.throw_ex<std::runtime_error>("RouterInfo failed verification.");
  }

  state_t* state_;
  info_t::shared_ptr remote_info_;
  info_t::shared_ptr info_;
  key_t remote_key_, connect_key_;
  obfse_t::iv_t aes_iv_;
  context_t ctx_;
  std::unique_ptr<work_guard_t> work_guard_;
  tcp_t::socket sock_;
  context_t::strand strand_;
  tcp_t::endpoint remote_host_;

  std::unique_ptr<request_msg_t> srq_msg_;
  std::unique_ptr<created_msg_t> scr_msg_;
  std::unique_ptr<confirmed_msg_t> sco_msg_;
  std::unique_ptr<data_msg_t> dp_msg_;
  std::unique_ptr<data_impl_t> dp_;

  message_queue_t data_messages_;
  std::shared_mutex data_messages_mutex_;

  std::size_t srq_xfer_, scr_xfer_, sco_xfer_, dp_xfer_;
  std::condition_variable cv_;
  bool ready_;
  std::mutex ready_mutex_;
  std::mutex msg_mutex_;
  std::unique_ptr<std::thread> thread_;
};
}  // namespace ntcp2
}  // namespace tini2p

#endif  // SRC_NTCP2_SESSION_SESSION_H_
