/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_SESSION_REQUEST_SESSION_REQUEST_H_
#define SRC_SESSION_REQUEST_SESSION_REQUEST_H_

#include <chrono>

#include <noise/protocol/handshakestate.h>

#include "src/crypto/aes.h"
#include "src/crypto/rand.h"
#include "src/crypto/sec_bytes.h"

#include "src/data/router/info.h"

#include "src/ntcp2/session_request/kdf.h"
#include "src/ntcp2/session_request/message.h"

namespace tini2p
{
namespace ntcp2
{
template <class RoleT>
class SessionRequest
{
 public:
  using role_t = RoleT;  //< Role trait alias
  using obfse_t = tini2p::crypto::AES;  //< OBFSE impl trait alias
  using kdf_t = SessionRequestKDF;  //< KDF trait alias
  using message_t = SessionRequestMessage;  //< Message trait alias

  /// @brief Create a SessionRequest processor for a given destination
  /// @param state Pointer to initialized Noise handshake state
  /// @param router_hash Hash of destination RouterIdentity
  /// @param iv Remote AES IV for key obfuscation
  /// @throw Invalid argument on null handshake state
  SessionRequest(
      noise::HandshakeState* state,
      const data::Identity::hash_t& router_hash,
      const obfse_t::iv_t& iv)
      : state_(state),
        kdf_(state_),
        obfse_(static_cast<obfse_t::key_t::buffer_t>(router_hash), iv)
  {
    if (!state)
      exception::Exception{"SessionRequest", __func__}
          .throw_ex<std::invalid_argument>("null handshake state.");
  }

  /// @brief Get a mutable reference to the KDF object
  kdf_t& kdf() noexcept
  {
    return kdf_;
  }

  /// @brief Get a const reference to the KDF object
  const kdf_t& kdf() const noexcept
  {
    return kdf_;
  }

  /// @brief Get a const reference to the OBFSE crypto impl
  const obfse_t& obfse() const noexcept
  {
    return obfse_;
  }

  /// @brief Process session request message based on role
  void ProcessMessage(message_t& message)
  {
    if (std::is_same<role_t, Initiator>::value)
      Write(message);  // write and encrypt message
    else
      Read(message);  // decrypt and read message
  }

 private:
  void Write(message_t& message)
  {
    const exception::Exception ex{"SessionRequest", __func__};

    NoiseBuffer data /*output*/, payload /*input*/;

    // ensure enough room to hold Noise payload + padding
    message.data.resize(message_t::NoisePayloadSize + message.options.pad_len);
    message.options.serialize();

    auto& in = message.options.buffer;
    auto& out = message.data;

    noise::RawBuffers bufs{in.data(), in.size(), out.data(), out.size()};
    noise::setup_buffers(data, payload, bufs);
    noise::write_message(state_, &data, &payload, ex);

    // encrypt ephemeral key in place
    obfse_.Encrypt(out.data(), crypto::X25519::PublicKeyLen);

    // save ciphertext for session created KDF
    std::copy_n(
        &message.data[message_t::CiphertextOffset],
        message_t::CiphertextSize,
        message.ciphertext.data());

    if (message.options.pad_len < message_t::MinPaddingSize
        || message.options.pad_len > message_t::MaxPaddingSize)
      ex.throw_ex<std::length_error>("invalid padding length.");

    std::copy(
        message.padding.begin(),
        message.padding.end(),
        &message.data[message_t::PaddingOffset]);
  }

  void Read(message_t& message)
  {
    const exception::Exception ex{"SessionRequest", __func__};

    NoiseBuffer data /*input*/, payload /*output*/;

    auto& in = message.data;
    auto& out = message.options.buffer;
    const auto& in_size = message_t::NoisePayloadSize;

    if (in.size() < message_t::MinSize || in.size() > message_t::MaxSize)
      ex.throw_ex<std::length_error>("invalid message size.");

    // save ciphertext for session created KDF
    std::copy_n(
        &message.data[message_t::CiphertextOffset],
        message_t::CiphertextSize,
        message.ciphertext.data());

    // decrypt ephemeral key in place
    obfse_.Decrypt(in.data(), crypto::X25519::PublicKeyLen);

    noise::RawBuffers bufs{in.data(), in_size, out.data(), out.size()};
    noise::setup_buffers(payload, data, bufs);
    noise::read_message(state_, &data, &payload, ex);

    // deserialize options from buffer
    message.options.deserialize();

    if (message.options.pad_len < message_t::MinPaddingSize
        || message.options.pad_len > message_t::MaxPaddingSize)
      ex.throw_ex<std::length_error>("invalid padding length.");

    if (message.data.size() - message_t::NoisePayloadSize
        == message.options.pad_len)
      {
        auto& pad = message.padding;
        pad.resize(message.options.pad_len);
        std::copy_n(
            &message.data[message_t::PaddingOffset], pad.size(), pad.data());
      }
  }

  role_t role_;
  noise::HandshakeState* state_;
  kdf_t kdf_;
  obfse_t obfse_;
};
}  // namespace ntcp2
}  // namespace tini2p

#endif  // SRC_SESSION_REQUEST_SESSION_REQUEST_H_
