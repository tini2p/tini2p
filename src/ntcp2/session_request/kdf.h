/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_NTCP2_SESSION_REQUEST_KDF_H_
#define SRC_NTCP2_SESSION_REQUEST_KDF_H_

#include <noise/protocol/handshakestate.h>

#include "src/exception/exception.h"

#include "src/crypto/x25519.h"

#include "src/ntcp2/noise.h"
#include "src/ntcp2/meta.h"
#include "src/ntcp2/role.h"

namespace tini2p
{
namespace ntcp2
{
class SessionRequestKDF
{
  NoiseHandshakeState* state_;

 public:
  SessionRequestKDF(NoiseHandshakeState* state) : state_(state)
  {
    if (!state)
      exception::Exception{"SessionRequestKDF", __func__}
          .throw_ex<std::invalid_argument>("null handshake state.");
  }

  /// @brief Set the responder's remote public key
  /// @param key Remote public key
  /// @detail Validates the public key internally
  void set_remote_key(const crypto::X25519::pubkey_t& key)
  {
    noise::set_remote_public_key(state_, key, {"SessionRequestKDF", __func__});
  }

  /// @brief Get the local static public key
  /// @param key Key to store the local static public key
  void get_local_public_key(crypto::X25519::pubkey_t& key) const
  {
    noise::get_local_public_key(state_, key, {"SessionRequestKDF", __func__});
  }

  /// @brief Set the local static keys
  /// @param keys Keypair to set
  void set_local_keys(const crypto::X25519::keypair_t& keys)
  {
    noise::set_local_keypair(state_, keys, {"SessionRequestKDF", __func__});
  }

  /// @brief Generate local static keys
  void generate_keys()
  {
    noise::generate_keypair(state_, {"SessionRequestKDF", __func__});
  }

  /// @brief Convenience function to derive keys for this session request
  /// @param key Remote static key used for Diffie-Hellman
  void Derive(const crypto::X25519::pubkey_t& key)
  {
    set_remote_key(key);
    Derive();
  }

  /// @brief Performs final steps in key derivation
  /// @detail On success, handshake state is ready to write the first message
  void Derive()
  {
    if (const int err = noise_handshakestate_start(state_))
      exception::Exception{"SessionRequestKDF", __func__}
          .throw_ex<std::runtime_error>(
              "unable to derive session request keys", err);
  }
};
}  // namespace ntcp2
}  // namespace tini2p

#endif  // SRC_NTCP2_SESSION_REQUEST_KDF_H_
