/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#ifndef SRC_SESSION_REQUEST_MESSAGE_H_
#define SRC_SESSION_REQUEST_MESSAGE_H_

#include "src/bytes.h"
#include "src/time.h"

#include "src/crypto/poly1305.h"
#include "src/crypto/rand.h"
#include "src/crypto/sec_bytes.h"
#include "src/crypto/sha.h"
#include "src/crypto/x25519.h"

#include "src/data/router/info.h"

namespace tini2p
{
namespace ntcp2
{
/// @struct SessionRequestMessage
/// @brief For creating and processing NTCP2 SessionRequest messages
// TODO(tini2p): change to a class, make data members private, add getters/setters
struct SessionRequestMessage
{
  enum : std::uint8_t
  {
    CiphertextOffset = 32,
    PaddingOffset = 64,
    VersionOffset = 1,
    PadLengthOffset = 2,
    Msg3Pt2LengthOffset = 4,
    TimestampOffset = 8
  };

  enum
  {
    OptionsSize = 16,
    XSize = crypto::X25519::PublicKeyLen,
    CiphertextSize = OptionsSize + crypto::Poly1305::DigestLen,
    NoisePayloadSize = XSize + CiphertextSize,
    MinSize = NoisePayloadSize,
    MaxSize = 65535,
    MinMsg3Pt2Size = data::Info::MinLen + crypto::Poly1305::DigestLen,  // see spec
    MaxMsg3Pt2Size = 65471,  // see spec
    MinPaddingSize = 32,
    MaxPaddingSize = MaxSize - NoisePayloadSize,
  };

  /// @struct SessionRequestOptions
  /// @brief Container for session request options
  // TODO(tini2p): change to a class, make data members private, add getters/setters
  struct Options
  {
    using buffer_t = crypto::FixedSecBytes<OptionsSize>;
    using id_t = std::uint8_t;
    using version_t = std::uint8_t;
    using padlen_t = std::uint16_t;
    using m3p2len_t = std::uint16_t;
    using timestamp_t = std::uint32_t;

    padlen_t pad_len;
    m3p2len_t m3p2_len;
    timestamp_t timestamp;
    buffer_t buffer;

    /// @brief Default ctor
    Options() : pad_len(crypto::RandInRange(MinPaddingSize, MaxPaddingSize)), m3p2_len(MinMsg3Pt2Size)
    {
      update(m3p2_len, pad_len);
    }

    /// @brief Updates session request options
    /// @param m3p2_len Message 3 Pt. 2 message length, see spec
    /// @param pad_len Padding length for the session request
    /// @detail As initiator, must call before calling ProcessMessage
    Options(const m3p2len_t m3p2_size, const padlen_t pad_size)
    {
      update(m3p2_size, pad_size);
    }

    /// @brief Updates session request options
    /// @param m3p2_len Message 3 Pt. 2 message length, see spec
    /// @param pad_len Padding length for the session request
    /// @detail As initiator, must call before calling ProcessMessage
    void update(const m3p2len_t m3p2_size, const padlen_t pad_size)
    {
      m3p2_len = m3p2_size;
      pad_len = pad_size;
      timestamp = tini2p::time::now_s();

      check_params({"SessionRequest: Options", __func__});

      serialize();
    }

    /// @brief Write request options to buffer
    void serialize()
    {
      check_params({"SessionRequest: Options", __func__});

      tini2p::BytesWriter<buffer_t> writer(buffer);
      writer.write_bytes(data::NET_ID);
      writer.write_bytes(data::V);
      writer.write_bytes(pad_len, tini2p::Endian::Big);
      writer.write_bytes(m3p2_len, tini2p::Endian::Big);
      writer.write_bytes(timestamp, tini2p::Endian::Big);
    }

    /// @brief Read request options from buffer
    void deserialize()
    {
      const exception::Exception ex{"SessionRequest: Options", __func__};

      tini2p::BytesReader<buffer_t> reader(buffer);

      id_t id;
      reader.read_bytes(id);
      check_id(id, ex);

      version_t version;
      reader.read_bytes(version);
      check_version(version, ex);

      padlen_t pad;
      reader.read_bytes(pad, tini2p::Endian::Big);
      check_pad_len(pad, ex);

      m3p2len_t m3p2;
      reader.read_bytes(m3p2, tini2p::Endian::Big);
      check_m3p2_len(m3p2, ex);

      timestamp_t time;
      reader.read_bytes(time, tini2p::Endian::Big);
      check_timestamp(time, ex);

      pad_len = std::move(pad);
      m3p2_len = std::move(m3p2);
      timestamp = std::move(time);
    }

   private:
    void check_id(const id_t& id, const exception::Exception& ex) const
    {
      if (id != data::NET_ID)
        {
          ex.throw_ex<std::logic_error>(
              "invalid netID: " + std::to_string(id) + ", expected: " + std::to_string(data::NET_ID));
        }
    }

    void check_version(const version_t& version, const exception::Exception& ex) const
    {
      if (version != data::V)
        {
          ex.throw_ex<std::runtime_error>(
              "invalid version: " + std::to_string(version) + ", expected: " + std::to_string(data::V));
        }
    }

    void check_pad_len(const padlen_t& pad, const exception::Exception& ex) const
    {
      const auto& max_pad = tini2p::under_cast(MaxPaddingSize);

      if (pad > max_pad)
        {
          ex.throw_ex<std::runtime_error>(
              "invalid padding size: " + std::to_string(pad) + ", max:" + std::to_string(max_pad));
        }
    }

    void check_m3p2_len(const m3p2len_t& m3p2, const exception::Exception& ex) const
    {
      const auto& min_m3p2_len = tini2p::under_cast(MinMsg3Pt2Size);
      const auto& max_m3p2_len = tini2p::under_cast(MaxMsg3Pt2Size);

      if (m3p2 < min_m3p2_len || m3p2 > max_m3p2_len)
        {
          ex.throw_ex<std::runtime_error>(
              "invalid message 3 pt 2 size: " + std::to_string(m3p2) + ", min: " + std::to_string(min_m3p2_len)
              + ", max: " + std::to_string(max_m3p2_len));
        }
    }

    void check_timestamp(const timestamp_t& time, const exception::Exception& ex) const
    {
      if (!time::check_lag_s(time))
        ex.throw_ex<std::runtime_error>("invalid timestamp: " + std::to_string(time) + " lags too far in the past");
    }

    void check_params(const exception::Exception& ex) const
    {
      check_pad_len(pad_len, ex);
      check_m3p2_len(m3p2_len, ex);
      check_timestamp(timestamp, ex);
    }
  };

  using data_t = crypto::SecBytes;  //< Data trait alias
  using padding_t = crypto::SecBytes;  //< Padding trait alias
  using ciphertext_t = crypto::FixedSecBytes<CiphertextSize>;  //< Ciphertext trait alias
  using options_t = Options;  //< Options trait alias

  data_t data;
  padding_t padding;
  ciphertext_t ciphertext;
  options_t options;

  SessionRequestMessage() : data(NoisePayloadSize), options()
  {
    if (options.pad_len)
    {
      padding.resize(options.pad_len);
      crypto::RandBytes(padding);
    }
  }

  SessionRequestMessage(
      const std::uint16_t m3p2_len,
      const std::uint16_t pad_len)
      : data(NoisePayloadSize + pad_len), options(m3p2_len, pad_len)
  {
    if (pad_len)
    {
      padding.resize(pad_len);
      crypto::RandBytes(padding);
    }
  }
};
}  // namespace ntcp2
}  // namespace tini2p

#endif  // SRC_SESSION_REQUEST_MESSAGE_H_
