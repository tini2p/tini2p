# Unlicense
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>

set(NOISEC_ROOT ${PROJECT_SOURCE_DIR}/deps/noise-c)

add_library(NoiseC::NoiseC STATIC IMPORTED)

set(NOISEC_INC ${NOISEC_ROOT}/include)

find_path(NoiseC_INCLUDE_DIR
  NAMES cipherstate.h errors.h handshakestate.h hashstate.h protocol.h
  PATHS ${NOISEC_INC}/noise
  PATH_SUFFIXES keys protocol
  NO_DEFAULT_PATH)

set_target_properties(NoiseC::NoiseC PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${NoiseC_INCLUDE_DIR}")

target_include_directories(NoiseC::NoiseC INTERFACE ${NOISEC_INC})

include(ProcessorCount)
ProcessorCount(ThreadNum)
if (NOT N EQUAL 0)
  set(MAKE_ARGS -j${TheadNum})
endif()

set(NOISEC_BUILD "${NOISEC_ROOT}/build")

set(BYPRODUCT
  ${NOISEC_BUILD}/lib/libnoisekeys.a
  ${NOISEC_BUILD}/lib/libnoiseprotobufs.a
  ${NOISEC_BUILD}/lib/libnoiseprotocol.a)

include(ExternalProject)
ExternalProject_Add(noisec
  SOURCE_DIR ${NOISEC_ROOT}
  BUILD_IN_SOURCE TRUE
  INSTALL_DIR "${NOISEC_BUILD}"
  CONFIGURE_COMMAND autoreconf -i && ./configure --prefix=<INSTALL_DIR>
  BUILD_COMMAND $(MAKE) ${MAKE_ARGS}
  COMMAND ""
  INSTALL_COMMAND $(MAKE) install)

add_dependencies(NoiseC::NoiseC noisec)

set_target_properties(NoiseC::NoiseC PROPERTIES
  IMPORTED_LOCATION "${NOISEC_BUILD}/lib/libnoiseprotocol.a"
  IMPORTED_LINK_INTERFACE_LANGUAGES "C")

target_link_libraries(NoiseC::NoiseC INTERFACE
  ${NOISEC_BUILD}/lib/libnoisekeys.a
  ${NOISEC_BUILD}/lib/libnoiseprotobufs.a
  ${NOISEC_BUILD}/lib/libnoiseprotocol.a)

unset(BYPRODUCT)
unset(ThreadNum)
unset(MAKE_ARGS)
unset(NOISEC_BUILD)
unset(NOISEC_INC)
unset(NOISEC_ROOT)
