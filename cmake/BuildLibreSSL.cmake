# Unlicense
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>

set(LibreSSL_ROOT ${PROJECT_SOURCE_DIR}/deps/libressl)

add_library(LibreSSL::LibreSSL SHARED IMPORTED)

set(LibreSSL_INC ${LibreSSL_ROOT}/include/openssl)

find_path(LibreSSL_INCLUDE_DIR
  NAMES evp.h crypto.h ssl.h
  PATHS ${LibreSSL_INC}
  NO_DEFAULT_PATH)

set_target_properties(LibreSSL::LibreSSL PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${LibreSSL_INCLUDE_DIR}")

target_include_directories(LibreSSL::LibreSSL INTERFACE ${LibreSSL_INCLUDE_DIR})

include(ProcessorCount)
ProcessorCount(ThreadNum)
if (NOT N EQUAL 0)
  set(MAKE_ARGS -j${TheadNum})
endif()

set(LibreSSL_BUILD "${LibreSSL_ROOT}/build")

set(BYPRODUCT
  ${LibreSSL_BUILD}/crypto/libcrypto.a
  ${LibreSSL_BUILD}/ssl/libssl.a
  ${LibreSSL_BUILD}/tls/libtls.a)

include(ExternalProject)
ExternalProject_Add(libressl
  SOURCE_DIR ${LibreSSL_ROOT}
  BUILD_IN_SOURCE TRUE
  INSTALL_DIR "${LibreSSL_BUILD}"
  CONFIGURE_COMMAND cd ${LibreSSL_BUILD} && cmake .. -DENABLE_NC=off -DLIBRESSL_APPS=off -DCMAKE_BUILD_TYPE=Shared
  BUILD_COMMAND $(MAKE) ${MAKE_ARGS}
  COMMAND ""
  INSTALL_COMMAND "")

add_dependencies(LibreSSL::LibreSSL libressl)
