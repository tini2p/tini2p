# Unlicense
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
#
# Parts used from The Kovri I2P Router Project Copyright (c) 2013-2018

SHELL := $(shell which bash)

cmake_target = all

cmake-tini2p  =

cmake-debug      = cmake -D CMAKE_BUILD_TYPE=Debug
cmake-release    = cmake -D CMAKE_BUILD_TYPE=Release
cmake-coverage   = -D WITH_COVERAGE=ON
cmake-tests      = -D WITH_TESTS=ON
cmake-net-tests  = -D WITH_NET_TESTS=ON
cmake-integration-tests  = -D WITH_INTEGRATION_TESTS=ON

noise-c = deps/noise-c
libressl = deps/libressl

build = build/

# cmake builder macro (courtesy of Kovri project)
define CMAKE
  cmake -E make_directory $1
	cmake -E chdir $1 $2 ../
endef

define PREP_NOISE_C
  cd $(noise-c); \
	autoreconf -i; \
	if [[! -d build ]]; then mkdir build; fi;
endef

define PREP_LIBRESSL
  cd $(libressl); \
	if [[! -d build ]]; then mkdir build; fi;
endef

define CLEAN_NOISE_C
  cd $(noise-c); \
  rm -rf build/*; \
	make clean;
endef

define CLEAN_LIBRESSL
  cd $(libressl); \
  rm -rf build/*; \
	make clean;
endef

deps:
	$(call PREP_NOISE_C)
	$(call PREP_LIBRESSL)

all: deps

tests: all
	$(eval cmake-tini2p += $(cmake-debug) $(cmake-tests))
	$(call CMAKE,$(build),$(cmake-tini2p)) && ${MAKE} -C $(build) $(cmake_target)

release-tests: all
	$(eval cmake-tini2p += $(cmake-release) $(cmake-tests))
	$(call CMAKE,$(build),$(cmake-tini2p)) && ${MAKE} -C $(build) $(cmake_target)

net-tests: all
	$(eval cmake-tini2p += $(cmake-debug) $(cmake-tests) $(cmake-net-tests))
	$(call CMAKE,$(build),$(cmake-tini2p)) && ${MAKE} -C $(build) $(cmake_target)

release-net-tests: all
	$(eval cmake-tini2p += $(cmake-release) $(cmake-tests) $(cmake-net-tests))
	$(call CMAKE,$(build),$(cmake-tini2p)) && ${MAKE} -C $(build) $(cmake_target)

int-tests: all
	$(eval cmake-tini2p += $(cmake-debug) $(cmake-tests) $(cmake-integration-tests))
	$(call CMAKE,$(build),$(cmake-tini2p)) && ${MAKE} -C $(build) $(cmake_target)

release-int-tests: all
	$(eval cmake-tini2p += $(cmake-release) $(cmake-tests) $(cmake-integration-tests))
	$(call CMAKE,$(build),$(cmake-tini2p)) && ${MAKE} -C $(build) $(cmake_target)

coverage: all
	$(eval cmake-tini2p += $(cmake-debug) $(cmake-coverage) $(cmake-tests) $(cmake-net-tests))
	$(call CMAKE,$(build),$(cmake-tini2p)) && ${MAKE} -C $(build) $(cmake_target)

clean:
	rm -rf $(build)

clean-deps:
	$(call CLEAN_NOISE_C)
	$(call CLEAN_LIBRESSL)

.PHONY: all tests net-tests coverage clean clean-deps
