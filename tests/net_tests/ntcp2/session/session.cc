/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 */

#include <catch2/catch.hpp>

#include "src/ntcp2/session/session.h"
#include "src/ntcp2/session/listener.h"
#include "src/ntcp2/session/manager.h"

namespace crypto = tini2p::crypto;

using Info = tini2p::data::Info;
using SessionManager = tini2p::ntcp2::SessionManager;
using init_session_t = SessionManager::out_session_t;
using resp_session_t = SessionManager::listener_t::session_t;

struct SessionFixture
{
  SessionFixture()
      : host(resp_session_t::tcp_t::v4(), crypto::RandInRange(SessionManager::MinPort, SessionManager::MaxPort)),
        host_v6(resp_session_t::tcp_t::v6(), crypto::RandInRange(SessionManager::MinPort, SessionManager::MaxPort)),
        remote(new Info(
            Info::identity_t(),
            Info::addresses_t{Info::address_t(host.address().to_string(), host.port()),
                              Info::address_t(host_v6.address().to_string(), host_v6.port())})),
        info(new Info()),
        manager(remote, host, host_v6)
  {
    msg.add_block(tini2p::data::PaddingBlock(32));
    msg.serialize();
  }

  ~SessionFixture()
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    REQUIRE_NOTHROW(manager.Stop());
  }

  resp_session_t::tcp_t::endpoint host, host_v6;
  Info::shared_ptr remote, info;
  SessionManager manager;
  resp_session_t::data_msg_t msg;
};

TEST_CASE_METHOD(SessionFixture, "Manager Session writes and reads after successful connection", "[session]")
{
  auto& mgr_init = manager.session(remote);

  REQUIRE_NOTHROW(mgr_init.Start(init_session_t::meta_t::IP::v6));
  REQUIRE_NOTHROW(mgr_init.Wait());
  REQUIRE(mgr_init.ready());

  auto& remote_v6 = manager.listener(init_session_t::meta_t::IP::v6).session(mgr_init.key());

  REQUIRE_NOTHROW(remote_v6.Wait());
  REQUIRE(remote_v6.ready());

  // Write a message from the local router
  REQUIRE_NOTHROW(mgr_init.Write(msg));
  REQUIRE_NOTHROW(remote_v6.Read());

  // Give Read handler time to completely read the DataPhase message
  std::this_thread::sleep_for(std::chrono::microseconds(500));
  REQUIRE(remote_v6.has_completed_messages());

  // Drain completed DataPhase messages from the queue
  SessionManager::data_messages_t resp_msgs;
  REQUIRE_NOTHROW(resp_msgs = remote_v6.drain_messages());
  REQUIRE(resp_msgs.size() == 1);

  for (auto& data_msg : resp_msgs)
    REQUIRE((data_msg.blocks() == msg.blocks()));
  
  // Write a response message from the remote router
  SessionManager::data_message_t resp_msg;
  resp_msg.add_block(tini2p::data::PaddingBlock(32));

  REQUIRE_NOTHROW(remote_v6.Write(resp_msg));
  REQUIRE_NOTHROW(mgr_init.Read());

  // Give Read handler time to completely read the DataPhase message
  std::this_thread::sleep_for(std::chrono::microseconds(500));
  REQUIRE(mgr_init.has_completed_messages());

  // Drain completed DataPhase messages from the queue
  SessionManager::data_messages_t init_msgs;
  REQUIRE_NOTHROW(init_msgs = mgr_init.drain_messages());
  REQUIRE(init_msgs.size() == 1);

  for (auto& data_msg : init_msgs)
    REQUIRE((data_msg.blocks() == resp_msg.blocks()));
}

TEST_CASE_METHOD(SessionFixture, "SessionManager rejects null RouterInfo for outbound sessions", "[session]")
{
  REQUIRE_THROWS(manager.session(nullptr));
}

TEST_CASE_METHOD(SessionFixture, "Session rejects reading/writing data phase messages w/o valid handshake", "[session]")
{
  init_session_t init(remote, info);
  REQUIRE(!init.ready());
  REQUIRE_THROWS(init.Write(msg));
  REQUIRE_THROWS(init.Read());

  resp_session_t::context_t ctx;
  resp_session_t resp(remote, ctx);

  REQUIRE(!resp.ready());
  REQUIRE_THROWS(resp.Write(msg));
  REQUIRE_THROWS(resp.Read());
}

TEST_CASE_METHOD(SessionFixture, "SessionManager rejects new connection for already existing session", "[session]")
{
  auto& s0 = manager.session(remote);

  REQUIRE_NOTHROW(s0.Start(init_session_t::meta_t::IP::v6));

  REQUIRE_THROWS(manager.session(remote));
  REQUIRE_NOTHROW(init_session_t(remote, info).Start(init_session_t::meta_t::IP::v6));

  REQUIRE(manager.blacklisted(s0.key()));
}
