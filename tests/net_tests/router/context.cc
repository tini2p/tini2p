/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 */

#include <catch2/catch.hpp>

#include "src/router/context.h"

using namespace tini2p::crypto;
namespace data = tini2p::data;

using tini2p::router::Context;
using tini2p::data::I2NPBlock;

struct ContextFixture
{
  ContextFixture() : context(std::string("127.0.0.1"), 9111) {}

  Context context;
};

TEST_CASE_METHOD(ContextFixture, "RouterContext has a RouterInfo", "[context]")
{
  REQUIRE_NOTHROW(context.info());
  REQUIRE(context.info().Verify());

  // Get an owning pointer to the RouterInfo
  auto& info = context.info();

  std::string host_key("host");
  SecBytes test_host{0x74, 0x65, 0x73, 0x74, 0x2e, 0x69, 0x32, 0x70};  // "test.i2p"
  REQUIRE_NOTHROW(info.options().add(host_key, test_host));
  REQUIRE(context.info().options().entry(host_key) == test_host);
}

TEST_CASE_METHOD(ContextFixture, "RouterContext has a NTCP2 SessionManager", "[context]")
{
  REQUIRE_NOTHROW(context.ntcp2_manager());

  auto& ntcp2 = context.ntcp2_manager();

  REQUIRE(!ntcp2.has_outbound_sessions());
  REQUIRE(!ntcp2.has_inbound_sessions());
  REQUIRE_NOTHROW(ntcp2.Stop());
}

TEST_CASE_METHOD(ContextFixture, "RouterContext has a NetDB", "[context]")
{
  REQUIRE_NOTHROW(context.netdb());
  const auto& netdb = context.netdb();

  REQUIRE(netdb.infos().empty());
  REQUIRE(netdb.floodfills().empty());
  REQUIRE(netdb.lease_sets().empty());

  auto& netdb_own = context.netdb();

  REQUIRE_NOTHROW(netdb_own.AddInfo(std::make_shared<Context::info_t>()));
}

TEST_CASE_METHOD(ContextFixture, "RouterContext has a pool managers", "[context]")
{
  REQUIRE_NOTHROW(context.aes_pool_manager());
  REQUIRE_NOTHROW(context.chacha_pool_manager());

  auto& aes_pool_mgr = context.aes_pool_manager();
  auto& chacha_pool_mgr = context.chacha_pool_manager();

  REQUIRE(!aes_pool_mgr.inbound_pool().has_tunnels());
  REQUIRE(!aes_pool_mgr.outbound_pool().has_tunnels());

  REQUIRE(!chacha_pool_mgr.inbound_pool().has_tunnels());
  REQUIRE(!chacha_pool_mgr.outbound_pool().has_tunnels());
}

TEST_CASE_METHOD(ContextFixture, "RouterContext sends an I2NP message over NTCP2", "[context]")
{
  data::I2NPMessage i2np_msg(
      data::I2NPHeader::Type::Data, {}, data::Data(0x32).buffer(), data::I2NPHeader::Mode::NTCP2);

  // Create a mock context for a remote router
  Context remote_context(std::string("127.0.0.1"), 9112);

  // Unrealistic, add external address to the mock RouterInfo
  // Normally done via NTCP2 or SSU discovery
  auto remote_info = remote_context.info_ptr();
  remote_info->add_address(data::Address(std::string("127.0.0.1"), 9112));

  // Add a mock remote router to the NetDB
  REQUIRE_NOTHROW(context.netdb().AddInfo(remote_info));

  // Send the mock I2NP message to the remote router
  REQUIRE_NOTHROW(context.SendI2NPMessage(remote_info->identity().hash(), i2np_msg));

  // Give the listener callback time to fire twice, checks for completed session messages
  std::this_thread::sleep_for(std::chrono::milliseconds(120));

  // Drain the remote context's I2NP Data message queue
  // Somewhat unrealistic to do this manually, normally handled on a timer by an I2CP client handler
  std::vector<data::I2NPMessage> i2np_msgs;
  REQUIRE_NOTHROW(i2np_msgs = remote_context.drain_queue());
  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE(i2np_msgs.front() == i2np_msg);
}

TEST_CASE_METHOD(ContextFixture, "RouterContext creates and sends tunnels messages with Router delivery", "[bad_context]")
{
  data::I2NPMessage i2np_msg(
      data::I2NPHeader::Type::Data, {}, data::Data(0x32).buffer(), data::I2NPHeader::Mode::NTCP2);

  // Create a mock context for a remote router
  Context obep_context(std::string("127.0.0.1"), 9112), remote_context(std::string("127.0.0.1"), 9113);

  // Unrealistic, add external address to the mock RouterInfos
  // Normally done via NTCP2 or SSU discovery
  auto context_info = context.info_ptr();
  context_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9111));

  auto obep_info = obep_context.info_ptr();
  obep_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9112));

  auto remote_info = remote_context.info_ptr();
  remote_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9113));

  // Unrealistic, add a mock routers to each context's NetDB
  // Normally, this would be done via reseeding, DatabaseLookups, and DatabaseStores
  REQUIRE_NOTHROW(context.netdb().AddInfo(obep_info));
  REQUIRE_NOTHROW(obep_context.netdb().AddInfo(context_info).AddInfo(remote_info));

  // Create the outbound tunnel
  REQUIRE_NOTHROW(context.AddOutboundTunnel<TunnelAES>({obep_info}));

  std::this_thread::sleep_for(std::chrono::milliseconds(485));

  // Send an I2NP message through the outbound tunnel to the inbound tunnel
  REQUIRE_NOTHROW(context.SendTunnelMessage(i2np_msg, remote_info->identity().hash()));

  // Give the listener callback time to fire multiple times, checks for completed session messages
  std::this_thread::sleep_for(std::chrono::milliseconds(485));

  // Check that the remote router received I2NP Data message
  // Drain the remote context's I2NP Data message queue
  // Somewhat unrealistic to do this manually, normally handled on a timer by an I2CP client handler
  std::vector<data::I2NPMessage> i2np_msgs;
  REQUIRE_NOTHROW(i2np_msgs = remote_context.drain_queue());
  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE(i2np_msgs.front() == i2np_msg);
}

TEST_CASE_METHOD(ContextFixture, "RouterContext creates and sends AES-to-AES tunnel messages", "[context]")
{
  data::I2NPMessage i2np_msg(data::I2NPHeader::Type::Data, {}, data::Data(0x32).buffer());

  // Create a mock context for a remote router
  Context obep_context(std::string("127.0.0.1"), 9112), remote_ibgw_context(std::string("127.0.0.1"), 9113),
      remote_ibep_context(std::string("127.0.0.1"), 9114);

  // Unrealistic, add external address to the mock RouterInfos
  // Normally done via NTCP2 or SSU discovery
  auto context_info = context.info_ptr();
  context_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9111));

  auto obep_info = obep_context.info_ptr();
  obep_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9112));

  auto remote_ibgw_info = remote_ibgw_context.info_ptr();
  remote_ibgw_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9113));

  auto remote_ibep_info = remote_ibep_context.info_ptr();
  remote_ibep_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9114));

  // Unrealistic, add a mock routers to each context's NetDB
  // Normally, this would be done via reseeding, DatabaseLookups, and DatabaseStores
  REQUIRE_NOTHROW(context.netdb().AddInfo(obep_info));
  REQUIRE_NOTHROW(obep_context.netdb().AddInfo(context_info).AddInfo(remote_ibgw_info));
  REQUIRE_NOTHROW(remote_ibep_context.netdb().AddInfo(remote_ibgw_info));
  REQUIRE_NOTHROW(remote_ibgw_context.netdb().AddInfo(remote_ibep_info));

  // Create the outbound tunnel
  REQUIRE_NOTHROW(context.AddOutboundTunnel<TunnelAES>({obep_info}));

  // Create the inbound tunnel
  REQUIRE_NOTHROW(remote_ibep_context.AddInboundTunnel<TunnelAES>({remote_ibgw_info}));

  std::this_thread::sleep_for(std::chrono::milliseconds(165));

  Context::tunnel_id_t ibgw_tunnel_id;
  REQUIRE_NOTHROW(ibgw_tunnel_id = remote_ibep_context.aes_pool_manager().inbound_pool().tunnel().ibgw_tunnel_id());

  // Send an I2NP message through the outbound tunnel to the inbound tunnel
  REQUIRE_NOTHROW(context.SendTunnelMessage(i2np_msg, remote_ibgw_info->identity().hash(), ibgw_tunnel_id));

  // Give the listener callback time to fire multiple times, checks for completed session messages
  std::this_thread::sleep_for(std::chrono::milliseconds(185));

  // Check that the remote IBEP received I2NP Data message
  // Drain the remote context's I2NP Data message queue
  // Somewhat unrealistic to do this manually, normally handled on a timer by an I2CP client handler
  std::vector<data::I2NPMessage> i2np_msgs;
  REQUIRE_NOTHROW(i2np_msgs = remote_ibep_context.drain_queue());
  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE(i2np_msgs.front() == i2np_msg);
}

TEST_CASE_METHOD(ContextFixture, "RouterContext creates and sends ChaCha-to-ChaCha tunnel messages", "[context]")
{
  data::I2NPMessage i2np_msg(data::I2NPHeader::Type::Data, {}, data::Data(0x32).buffer());

  // Create a mock context for a remote router
  Context obep_context(std::string("127.0.0.1"), 9112), remote_ibgw_context(std::string("127.0.0.1"), 9113),
      remote_ibep_context(std::string("127.0.0.1"), 9114);

  // Unrealistic, add external address to the mock RouterInfos
  // Normally done via NTCP2 or SSU discovery
  auto context_info = context.info_ptr();
  context_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9111));

  auto obep_info = obep_context.info_ptr();
  obep_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9112));

  auto remote_ibgw_info = remote_ibgw_context.info_ptr();
  remote_ibgw_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9113));

  auto remote_ibep_info = remote_ibep_context.info_ptr();
  remote_ibep_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9114));

  // Unrealistic, add a mock routers to each context's NetDB
  // Normally, this would be done via reseeding, DatabaseLookups, and DatabaseStores
  REQUIRE_NOTHROW(context.netdb().AddInfo(obep_info));
  REQUIRE_NOTHROW(obep_context.netdb().AddInfo(context_info).AddInfo(remote_ibgw_info));
  REQUIRE_NOTHROW(remote_ibep_context.netdb().AddInfo(remote_ibgw_info));
  REQUIRE_NOTHROW(remote_ibgw_context.netdb().AddInfo(remote_ibep_info));

  // Create the outbound tunnel
  REQUIRE_NOTHROW(context.AddOutboundTunnel<TunnelChaCha>({obep_info}));

  // Create the inbound tunnel
  REQUIRE_NOTHROW(remote_ibep_context.AddInboundTunnel<TunnelChaCha>({remote_ibgw_info}));

  std::this_thread::sleep_for(std::chrono::milliseconds(165));

  Context::tunnel_id_t ibgw_tunnel_id;
  REQUIRE_NOTHROW(ibgw_tunnel_id = remote_ibep_context.chacha_pool_manager().inbound_pool().tunnel().ibgw_tunnel_id());

  // Send an I2NP message through the outbound tunnel to the inbound tunnel
  REQUIRE_NOTHROW(context.SendTunnelMessage(i2np_msg, remote_ibgw_info->identity().hash(), ibgw_tunnel_id));

  // Give the listener callback time to fire multiple times, checks for completed session messages
  std::this_thread::sleep_for(std::chrono::milliseconds(185));

  // Check that the remote IBEP received I2NP Data message
  // Drain the remote context's I2NP Data message queue
  // Somewhat unrealistic to do this manually, normally handled on a timer by an I2CP client handler
  std::vector<data::I2NPMessage> i2np_msgs;
  REQUIRE_NOTHROW(i2np_msgs = remote_ibep_context.drain_queue());
  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE(i2np_msgs.front() == i2np_msg);
}

TEST_CASE_METHOD(ContextFixture, "RouterContext creates and sends AES-to-ChaCha tunnel messages", "[context]")
{
  data::I2NPMessage i2np_msg(data::I2NPHeader::Type::Data, {}, data::Data(0x32).buffer());

  // Create a mock context for a remote router
  Context obep_context(std::string("127.0.0.1"), 9112), remote_ibgw_context(std::string("127.0.0.1"), 9113),
      remote_ibep_context(std::string("127.0.0.1"), 9114);

  // Unrealistic, add external address to the mock RouterInfos
  // Normally done via NTCP2 or SSU discovery
  auto context_info = context.info_ptr();
  context_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9111));

  auto obep_info = obep_context.info_ptr();
  obep_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9112));

  auto remote_ibgw_info = remote_ibgw_context.info_ptr();
  remote_ibgw_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9113));

  auto remote_ibep_info = remote_ibep_context.info_ptr();
  remote_ibep_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9114));

  // Unrealistic, add a mock routers to each context's NetDB
  // Normally, this would be done via reseeding, DatabaseLookups, and DatabaseStores
  REQUIRE_NOTHROW(context.netdb().AddInfo(obep_info));
  REQUIRE_NOTHROW(obep_context.netdb().AddInfo(context_info).AddInfo(remote_ibgw_info));
  REQUIRE_NOTHROW(remote_ibep_context.netdb().AddInfo(remote_ibgw_info));
  REQUIRE_NOTHROW(remote_ibgw_context.netdb().AddInfo(remote_ibep_info));

  // Create the outbound tunnel
  REQUIRE_NOTHROW(context.AddOutboundTunnel<TunnelAES>({obep_info}));

  // Create the inbound tunnel
  REQUIRE_NOTHROW(remote_ibep_context.AddInboundTunnel<TunnelChaCha>({remote_ibgw_info}));

  std::this_thread::sleep_for(std::chrono::milliseconds(165));

  Context::tunnel_id_t ibgw_tunnel_id;
  REQUIRE_NOTHROW(ibgw_tunnel_id = remote_ibep_context.chacha_pool_manager().inbound_pool().tunnel().ibgw_tunnel_id());

  // Send an I2NP message through the outbound tunnel to the inbound tunnel
  REQUIRE_NOTHROW(context.SendTunnelMessage(i2np_msg, remote_ibgw_info->identity().hash(), ibgw_tunnel_id));

  // Give the listener callback time to fire multiple times, checks for completed session messages
  std::this_thread::sleep_for(std::chrono::milliseconds(185));

  // Check that the remote IBEP received I2NP Data message
  // Drain the remote context's I2NP Data message queue
  // Somewhat unrealistic to do this manually, normally handled on a timer by an I2CP client handler
  std::vector<data::I2NPMessage> i2np_msgs;
  REQUIRE_NOTHROW(i2np_msgs = remote_ibep_context.drain_queue());
  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE(i2np_msgs.front() == i2np_msg);
}

TEST_CASE_METHOD(ContextFixture, "RouterContext creates and sends ChaCha-to-AES tunnel messages", "[context]")
{
  data::I2NPMessage i2np_msg(data::I2NPHeader::Type::Data, {}, data::Data(0x32).buffer());

  // Create a mock context for a remote router
  Context obep_context(std::string("127.0.0.1"), 9112), remote_ibgw_context(std::string("127.0.0.1"), 9113),
      remote_ibep_context(std::string("127.0.0.1"), 9114);

  // Unrealistic, add external address to the mock RouterInfos
  // Normally done via NTCP2 or SSU discovery
  auto context_info = context.info_ptr();
  context_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9111));

  auto obep_info = obep_context.info_ptr();
  obep_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9112));

  auto remote_ibgw_info = remote_ibgw_context.info_ptr();
  remote_ibgw_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9113));

  auto remote_ibep_info = remote_ibep_context.info_ptr();
  remote_ibep_info->add_address(tini2p::data::Address(std::string("127.0.0.1"), 9114));

  // Unrealistic, add a mock routers to each context's NetDB
  // Normally, this would be done via reseeding, DatabaseLookups, and DatabaseStores
  REQUIRE_NOTHROW(context.netdb().AddInfo(obep_info));
  REQUIRE_NOTHROW(obep_context.netdb().AddInfo(context_info).AddInfo(remote_ibgw_info));
  REQUIRE_NOTHROW(remote_ibep_context.netdb().AddInfo(remote_ibgw_info));
  REQUIRE_NOTHROW(remote_ibgw_context.netdb().AddInfo(remote_ibep_info));

  // Create the outbound tunnel
  REQUIRE_NOTHROW(context.AddOutboundTunnel<TunnelChaCha>({obep_info}));

  // Create the inbound tunnel
  REQUIRE_NOTHROW(remote_ibep_context.AddInboundTunnel<TunnelAES>({remote_ibgw_info}));

  std::this_thread::sleep_for(std::chrono::milliseconds(165));

  Context::tunnel_id_t ibgw_tunnel_id;
  REQUIRE_NOTHROW(ibgw_tunnel_id = remote_ibep_context.aes_pool_manager().inbound_pool().tunnel().ibgw_tunnel_id());

  // Send an I2NP message through the outbound tunnel to the inbound tunnel
  REQUIRE_NOTHROW(context.SendTunnelMessage(i2np_msg, remote_ibgw_info->identity().hash(), ibgw_tunnel_id));

  // Give the listener callback time to fire multiple times, checks for completed session messages
  std::this_thread::sleep_for(std::chrono::milliseconds(185));

  // Check that the remote IBEP received I2NP Data message
  // Drain the remote context's I2NP Data message queue
  // Somewhat unrealistic to do this manually, normally handled on a timer by an I2CP client handler
  std::vector<data::I2NPMessage> i2np_msgs;
  REQUIRE_NOTHROW(i2np_msgs = remote_ibep_context.drain_queue());
  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE(i2np_msgs.front() == i2np_msg);
}

TEST_CASE_METHOD(ContextFixture, "RouterContext creates end-to-end ECIES sessions", "[context]")
{
  // Create a mock context for a remote router
  Context obep_context(std::string("127.0.0.1"), 9112), ibgw_context(std::string("127.0.0.1"), 9113),
      remote_ibgw_context(std::string("127.0.0.1"), 9114), remote_ibep_context(std::string("127.0.0.1"), 9115),
      remote_obep_context(std::string("127.0.0.1"), 9116);

  // Unrealistic, add external address to the mock RouterInfos
  // Normally done via NTCP2 or SSU discovery
  auto context_info = context.info_ptr();
  context_info->add_address(data::Address(std::string("127.0.0.1"), 9111)).serialize();

  auto obep_info = obep_context.info_ptr();
  obep_info->add_address(data::Address(std::string("127.0.0.1"), 9112)).serialize();

  auto ibgw_info = ibgw_context.info_ptr();
  ibgw_info->add_address(data::Address(std::string("127.0.0.1"), 9113)).serialize();

  auto remote_ibgw_info = remote_ibgw_context.info_ptr();
  remote_ibgw_info->add_address(data::Address(std::string("127.0.0.1"), 9114)).serialize();

  auto remote_ibep_info = remote_ibep_context.info_ptr();
  remote_ibep_info->add_address(data::Address(std::string("127.0.0.1"), 9115)).serialize();

  auto remote_obep_info = remote_obep_context.info_ptr();
  remote_obep_info->add_address(data::Address(std::string("127.0.0.1"), 9116)).serialize();

  // Unrealistic, add a mock routers to each context's NetDB
  // Normally, this would be done via reseeding, DatabaseLookups, and DatabaseStores
  REQUIRE_NOTHROW(context.netdb().AddInfo(obep_info).AddInfo(ibgw_info));
  REQUIRE_NOTHROW(obep_context.netdb().AddInfo(context_info).AddInfo(remote_ibgw_info));
  REQUIRE_NOTHROW(ibgw_context.netdb().AddInfo(context_info));
  REQUIRE_NOTHROW(remote_ibep_context.netdb().AddInfo(remote_ibgw_info).AddInfo(remote_obep_info));
  REQUIRE_NOTHROW(remote_ibgw_context.netdb().AddInfo(remote_ibep_info));
  REQUIRE_NOTHROW(remote_obep_context.netdb().AddInfo(remote_ibep_info).AddInfo(ibgw_info));

  // Create initiator outbound and inbound tunnel
  REQUIRE_NOTHROW(context.AddOutboundTunnel<TunnelChaCha>({obep_info}));
  REQUIRE_NOTHROW(context.AddInboundTunnel<TunnelChaCha>({ibgw_info}));

  // Create response outbound and inbound tunnel
  REQUIRE_NOTHROW(remote_ibep_context.AddOutboundTunnel<TunnelChaCha>({remote_obep_info}));
  REQUIRE_NOTHROW(remote_ibep_context.AddInboundTunnel<TunnelChaCha>({remote_ibgw_info}));

  std::this_thread::sleep_for(std::chrono::milliseconds(280));

  //const auto& remote_ibgw_hash = remote_ibgw_info->identity().hash();
  Context::tunnel_id_t ibgw_tunnel_id, remote_ibgw_tunnel_id;
  REQUIRE_NOTHROW(ibgw_tunnel_id = context.chacha_pool_manager().inbound_pool().tunnel().ibgw_tunnel_id());
  REQUIRE_NOTHROW(remote_ibgw_tunnel_id = remote_ibep_context.chacha_pool_manager().inbound_pool().tunnel().ibgw_tunnel_id());

  // Create a mock Destination and LeaseSet
  data::Destination dest;
  data::Destination remote_dest(remote_ibep_context.ecies_manager().ecies().id_keys());

  data::LeaseSet2::shared_ptr ls_ptr, remote_ls_ptr;

  REQUIRE_NOTHROW(ls_ptr = context.CreateLeaseSet(dest));
  REQUIRE_NOTHROW(remote_ls_ptr = remote_ibep_context.CreateLeaseSet(remote_dest));
  REQUIRE_NOTHROW(context.netdb().AddLeaseSet(remote_ls_ptr));

  // Send a Garlic message containing a DatabaseStore for the local LeaseSet
  const auto& remote_dest_hash = remote_dest.hash();
  REQUIRE_NOTHROW(context.SendGarlicMessage(dest, remote_dest_hash, data::DatabaseStore(ls_ptr), 0x42));

  // Give the listener callback time to fire multiple times, checks for completed session messages
  std::this_thread::sleep_for(std::chrono::milliseconds(460));

  // Check that the remote router has the local LeaseSet
  const auto& dest_hash = dest.hash();
  REQUIRE_NOTHROW(remote_ibep_context.netdb().find_lease_set(dest_hash));

  // Fully establish the session by sending a NewSessionReply
  REQUIRE_NOTHROW(remote_ibep_context.SendGarlicMessage(remote_dest, dest_hash, data::Data()));

  // Give the listener callback time to fire multiple times
  std::this_thread::sleep_for(std::chrono::milliseconds(240));

  // Check that the local context received the I2NP Data message
  // Drain the remote context's I2NP Data message queue
  // Somewhat unrealistic to do this manually, normally handled on a timer by an I2CP client handler
  std::vector<data::I2NPMessage> i2np_msgs;
  REQUIRE_NOTHROW(i2np_msgs = context.drain_queue());
  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE(i2np_msgs.front().message_buffer() == data::Data().buffer());

  // Send an ExistingSession Garlic message from Alice
  REQUIRE_NOTHROW(context.SendGarlicMessage(dest, remote_dest_hash, data::Data()));

  // Give the listener callback time to fire multiple times
  std::this_thread::sleep_for(std::chrono::milliseconds(240));

  // Check that the remote context received the I2NP Data message
  // Drain the remote context's I2NP Data message queue
  // Somewhat unrealistic to do this manually, normally handled on a timer by an I2CP client handler
  REQUIRE_NOTHROW(i2np_msgs = remote_ibep_context.drain_queue());
  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE(i2np_msgs.front().message_buffer() == data::Data().buffer());
}

TEST_CASE_METHOD(ContextFixture, "RouterContext handles DatabaseLookup messages", "[bad_context]")
{
  // Create a mock context for remote routers
  Context obep_context(std::string("127.0.0.1"), 9112), peer1_context(std::string("127.0.0.1"), 9113),
      peer2_context(std::string("127.0.0.1"), 9114), peer3_context(std::string("127.0.0.1"), 9115),
      peer4_context(std::string("127.0.0.1"), 9116), remote_context(std::string("127.0.0.1"), 9117),
      remote_obep_context(std::string("127.0.0.1"), 9118), remote_ibgw_context(std::string("127.0.0.1"), 9119);

  // Unrealistic, add external address to the mock RouterInfos
  // Normally done via NTCP2 or SSU discovery
  auto context_info = context.info_ptr();
  context_info->add_address(data::Address(std::string("127.0.0.1"), 9111)).serialize();

  auto obep_info = obep_context.info_ptr();
  obep_info->add_address(data::Address(std::string("127.0.0.1"), 9112)).serialize();

  auto peer1_info = peer1_context.info_ptr();
  peer1_info->add_address(data::Address(std::string("127.0.0.1"), 9113)).serialize();

  auto peer2_info = peer2_context.info_ptr();
  peer2_info->add_address(data::Address(std::string("127.0.0.1"), 9114)).serialize();

  auto peer3_info = peer3_context.info_ptr();
  peer3_info->add_address(data::Address(std::string("127.0.0.1"), 9115)).serialize();

  auto peer4_info = peer4_context.info_ptr();
  peer4_info->add_address(data::Address(std::string("127.0.0.1"), 9116)).serialize();

  auto remote_info = remote_context.info_ptr();
  remote_info->add_address(data::Address(std::string("127.0.0.1"), 9117)).serialize();

  auto remote_obep_info = remote_obep_context.info_ptr();
  remote_obep_info->add_address(data::Address(std::string("127.0.0.1"), 9118)).serialize();

  auto remote_ibgw_info = remote_ibgw_context.info_ptr();
  remote_ibgw_info->add_address(data::Address(std::string("127.0.0.1"), 9119)).serialize();

  // Add all-but-one of the peers, and the remote info to the local context
  REQUIRE_NOTHROW(
      context.netdb().AddInfo(obep_info).AddInfo(peer1_info).AddInfo(remote_info).AddInfo(remote_ibgw_info));
  REQUIRE_NOTHROW(obep_context.netdb().AddInfo(context_info).AddInfo(remote_info).AddInfo(remote_ibgw_info));

  // Add the peer not in the local context to the other peer context(s)
  REQUIRE_NOTHROW(peer1_context.netdb().AddInfo(peer2_info).AddInfo(remote_info).AddInfo(remote_ibgw_info));

  // Add the nedded infos to the remote contexts
  REQUIRE_NOTHROW(remote_context.netdb().AddInfo(context_info).AddInfo(remote_obep_info).AddInfo(remote_ibgw_info));
  REQUIRE_NOTHROW(remote_obep_context.netdb()
                      .AddInfo(remote_info)
                      .AddInfo(remote_ibgw_info)
                      .AddInfo(context_info)
                      .AddInfo(peer1_info));
  REQUIRE_NOTHROW(remote_ibgw_context.netdb().AddInfo(remote_info));

  REQUIRE_NOTHROW(context.AddOutboundTunnel<TunnelChaCha>({obep_info}));
  REQUIRE_NOTHROW(remote_context.AddOutboundTunnel<TunnelChaCha>({remote_obep_info}));
  REQUIRE_NOTHROW(remote_context.AddInboundTunnel<TunnelChaCha>({remote_ibgw_info}));

  std::this_thread::sleep_for(std::chrono::milliseconds(710));

  const auto& peer1_hash = peer1_info->identity().hash();
  const auto& remote_hash = remote_info->identity().hash();

  data::DatabaseLookup db_lookup(peer1_hash, remote_hash);

  // Simulate a successful DatabaseLookup by requesting a peer contained in the local context
  // Request the reply is sent directly to the remote router
  REQUIRE_NOTHROW(remote_context.SendTunnelMessage(
      data::I2NPMessage(data::I2NPHeader::Type::DatabaseLookup, 0x22, db_lookup.buffer()),
      context_info->identity().hash()));

  std::this_thread::sleep_for(std::chrono::milliseconds(240));

  REQUIRE(remote_context.netdb().has_info(peer1_hash));

  // Simulate a failed DatabaseLookup by requesting a peer not contained in the local context
  // Request the reply is sent to the remote router's IBGW
  Context::tunnel_id_t remote_ibgw_id;
  REQUIRE_NOTHROW(remote_ibgw_id = remote_context.chacha_pool_manager().inbound_pool().tunnel().ibgw_tunnel_id());

  const auto& obep_hash = obep_info->identity().hash();
  const auto& peer2_hash = peer2_info->identity().hash();
  const auto& remote_ibgw_hash = remote_ibgw_info->identity().hash();

  // Create a DatabaseLookup for a peer not included in the local context
  db_lookup.search_key(peer2_hash)
      .from_key(remote_ibgw_hash)
      .reply_tunnel_id(remote_ibgw_id)
      .excluded_peers({remote_ibgw_hash, remote_hash, obep_hash})
      .serialize();

  REQUIRE_NOTHROW(remote_context.SendTunnelMessage(
      data::I2NPMessage(data::I2NPHeader::Type::DatabaseLookup, 0x23, db_lookup.buffer()),
      context_info->identity().hash()));

  std::this_thread::sleep_for(std::chrono::milliseconds(95));

  // Check that a DatabaseSearchReply was returned, not a DatabaseStore
  REQUIRE(!remote_context.netdb().has_info(peer2_hash));

  // Wait for the lookup sent to the DatabaseSearchReply peers to return a DatabaseStore
  std::this_thread::sleep_for(std::chrono::milliseconds(340));

  // Check that a DatabaseStore was returned from the second lookup
  REQUIRE(remote_context.netdb().has_info(peer2_hash));
}
