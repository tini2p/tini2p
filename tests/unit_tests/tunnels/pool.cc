/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/tunnels/pool.h"

using namespace tini2p::crypto;
namespace data = tini2p::data;
namespace tunnels = tini2p::tunnels;

using data::Info;
using data::Lease2;
using tunnels::CreateReplyType;

/*---------------\
| AES Pool Tests |
\---------------*/

using AESInboundPool = tunnels::Pool<tunnels::InboundTunnel<TunnelAES>>;
using AESOutboundPool = tunnels::Pool<tunnels::OutboundTunnel<TunnelAES>>;

struct AESPoolFixture
{
  AESPoolFixture()
      : creator_info(std::make_shared<Info>()),
        reply_lease(RandBytes<32>()),
        hop_infos{{std::make_shared<Info>()}, {std::make_shared<Info>()}, {std::make_shared<Info>()}}
  {
  }

  Info::shared_ptr creator_info;
  Lease2 reply_lease;
  std::vector<Info::shared_ptr> hop_infos;

  AESInboundPool in_pool;
  AESOutboundPool out_pool;
};

TEST_CASE_METHOD(AESPoolFixture, "AES Pool has valid fields", "[aes_pool]")
{
  REQUIRE(in_pool.tunnels().empty());
  REQUIRE(in_pool.building_tunnels().empty());

  REQUIRE(out_pool.tunnels().empty());
  REQUIRE(out_pool.building_tunnels().empty());
}

TEST_CASE_METHOD(AESPoolFixture, "AES InboundPool can add a tunnel", "[aes_pool]")
{
  REQUIRE_NOTHROW(in_pool.AddTunnel(creator_info, {}, hop_infos));

  // Check that InboundPool adds an building tunnel
  REQUIRE(in_pool.building_tunnels().size() == 1);
  // Exploratory tunnel only moved to established tunnel after receiving accepted VariableTunnelReplyMessage
  REQUIRE(in_pool.tunnels().size() == 0);
}

TEST_CASE_METHOD(AESPoolFixture, "AES OutboundPool can add a tunnel", "[aes_pool]")
{
  REQUIRE_NOTHROW(out_pool.AddTunnel(creator_info, reply_lease, hop_infos));

  // Check that InboundPool adds an building tunnel
  REQUIRE(out_pool.building_tunnels().size() == 1);
  // Exploratory tunnel only moved to established tunnel after receiving accepted VariableTunnelReplyMessage
  REQUIRE(out_pool.tunnels().size() == 0);
}

TEST_CASE_METHOD(AESPoolFixture, "AES InboundPool handles accepted TunnelBuildReply messages", "[aes_pool]")
{
  AESInboundPool::message_id_t msg_id(0);

  // Check that the InboundTunnelPool handles TunnelBuildReply messages
  REQUIRE_NOTHROW(msg_id = in_pool.AddTunnel(creator_info, {}, hop_infos).message_id());

  // Unrealistic, get the building tunnel and create a TunnelBuildReply message
  // Normally, tunnel hops create the message during TunnelBuildMessage processing
  const auto& exp_tunnels = in_pool.building_tunnels();
  REQUIRE(exp_tunnels.size() == 1);

  const auto exp_id = exp_tunnels.front().creator().tunnel_id();
  auto& exp_tunnel = in_pool.building_tunnel(exp_id);
  auto& exp_creator = exp_tunnel.creator();

  // Check that the building tunnel was moved to the established tunnels
  REQUIRE_NOTHROW(in_pool.HandleBuildReply(exp_creator.CreateAESReplyMessage(msg_id, CreateReplyType::AllAccept)));
  REQUIRE(exp_tunnels.size() == 0);
  REQUIRE_THROWS(in_pool.building_tunnel(exp_id));

  REQUIRE(in_pool.tunnels().size() == 1);
  REQUIRE_NOTHROW(in_pool.tunnel(exp_id));
}

TEST_CASE_METHOD(AESPoolFixture, "AES OutboundPool handles accepted TunnelBuildReply messages", "[aes_pool]")
{
  using reply_message_t = AESOutboundPool::reply_message_t;

  AESOutboundPool::message_id_t msg_id(0);

  // Check that the OutboundTunnelPool handles TunnelBuildReply messages
  REQUIRE_NOTHROW(msg_id = out_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  // Unrealistic, get the building tunnel and create a TunnelBuildReply message
  // Normally, tunnel hops create the message during TunnelBuildMessage processing
  const auto& exp_tunnels = out_pool.building_tunnels();
  REQUIRE(exp_tunnels.size() == 1);

  std::unique_ptr<reply_message_t> reply_msg_ptr;
  const auto exp_id = exp_tunnels.front().creator().tunnel_id();
  auto& exp_tunnel = out_pool.building_tunnel(exp_id);
  auto& exp_creator = exp_tunnel.creator();

  // Check that the building tunnel was moved to the established tunnels
  REQUIRE_NOTHROW(out_pool.HandleBuildReply(exp_creator.CreateAESReplyMessage(msg_id, CreateReplyType::AllAccept)));
  REQUIRE(exp_tunnels.size() == 0);
  REQUIRE_THROWS(out_pool.building_tunnel(exp_id));

  REQUIRE(out_pool.tunnels().size() == 1);
  REQUIRE_NOTHROW(out_pool.tunnel(exp_id));
}

TEST_CASE_METHOD(AESPoolFixture, "AES OutboundPool handles creating tunnel messages for an OutboundTunnel", "[aes_pool]")
{
  using reply_message_t = AESOutboundPool::reply_message_t;

  AESOutboundPool::message_id_t msg_id(0);

  // Create an OutboundTunnel for creating TunnelMessages
  REQUIRE_NOTHROW(msg_id = out_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  std::unique_ptr<reply_message_t> reply_msg_ptr;
  const auto out_id = out_pool.building_tunnels().front().creator().tunnel_id();
  auto& exp_tunnel = out_pool.building_tunnel(out_id);
  auto& exp_creator = exp_tunnel.creator();

  // Move the building tunnel to established tunnels
  REQUIRE_NOTHROW(out_pool.HandleBuildReply(exp_creator.CreateAESReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Unrealistic, setup random I2NP buffer and IBGW identifiers
  AESOutboundPool::i2np_buffer_t i2np_buf(RandBuffer(0x23));
  AESOutboundPool::to_hash_t ibgw_to_hash(RandBytes<32>());
  AESOutboundPool::tunnel_id_t ibgw_id(0x41);

  // Create TunnelMessages for the established OutboundTunnel
  std::vector<AESOutboundPool::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = out_pool.HandleI2NPMessage(out_id, i2np_buf, ibgw_to_hash, ibgw_id).second);

  REQUIRE(messages.size() == 1);
}

TEST_CASE_METHOD(AESPoolFixture, "AES InboundPool processes messages created by an IBGW", "[aes_pool]")
{
  AESOutboundPool::message_id_t msg_id(0);

  // Create an InboundTunnel for handling TunnelMessages
  REQUIRE_NOTHROW(msg_id = in_pool.AddTunnel(creator_info, {}, hop_infos).message_id());

  const auto in_id = in_pool.building_tunnels().front().creator().tunnel_id();
  auto& in_tunnel = in_pool.building_tunnel(in_id);
  auto& in_creator = in_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(in_pool.HandleBuildReply(in_creator.CreateAESReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Somewhat unrealistic, setup random I2NP buffer
  // Normally, would come as a TunnelGateway message from an OBEP of another tunnel
  data::I2NPMessage i2np_msg(data::I2NPHeader::Type::Data, 0x42, tini2p::data::Data(32).buffer());

  const auto& hops = in_creator.hops();
  const auto ibgw_hop = hops.begin();

  // Unrealistic, transform the first hop into an IBGW
  // Normally, the IBGW processing would be done by a remote router
  tini2p::tunnels::InboundGateway<TunnelAES> ibgw(
      ibgw_hop->tunnel_id(), ibgw_hop->next_tunnel_id(), std::make_unique<TunnelAES>(ibgw_hop->key_info()));

  // Create TunnelMessages for the established OutboundTunnel to the established InboundTunnel
  std::vector<AESInboundPool::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = ibgw.CreateTunnelMessages(i2np_msg.buffer(), std::nullopt, std::nullopt));

  // Process each outbound TunnelMessage
  AESInboundPool::tunnel_t::creator_t::extracted_i2np_t ret_i2np_msgs;
  for (auto& message : messages)
    {
      // Somewhat unrealistic, simulate InboundTunnel hops encrypting the TunnelMessage
      REQUIRE_NOTHROW(in_creator.IterativeEncryption(message));
      // Process the TunnelMessage as the IBEP
      REQUIRE_NOTHROW(ret_i2np_msgs = in_pool.HandleTunnelMessage(message));
    }

  REQUIRE(ret_i2np_msgs.size() == 1);
  REQUIRE(ret_i2np_msgs.front().second == i2np_msg);
}

TEST_CASE_METHOD(AESPoolFixture, "AES InboundPool creates test messages", "[aes_pool]")
{
  AESInboundPool::message_id_t msg_id(0);

  REQUIRE_NOTHROW(msg_id = in_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  const auto in_id = in_pool.building_tunnels().front().creator().tunnel_id();
  auto& in_tunnel = in_pool.building_tunnel(in_id);
  auto& in_creator = in_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(in_pool.HandleBuildReply(in_creator.CreateAESReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Create test message
  std::pair<tini2p::tunnels::TestInfo, tini2p::data::I2NPMessage> test;
  REQUIRE_NOTHROW(test = in_pool.CreateTestMessage());
  REQUIRE(in_pool.has_test(test.first.message_id));
  REQUIRE(!in_pool.has_test(test.first.message_id + 1));
}

TEST_CASE_METHOD(AESPoolFixture, "AES OutboundPool creates test messages", "[aes_pool]")
{
  AESOutboundPool::message_id_t msg_id(0);

  REQUIRE_NOTHROW(msg_id = out_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  const auto out_id = out_pool.building_tunnels().front().creator().tunnel_id();
  auto& out_tunnel = out_pool.building_tunnel(out_id);
  auto& out_creator = out_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(out_pool.HandleBuildReply(out_creator.CreateAESReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Create ZeroHop tunnels, and a test message
  std::pair<tini2p::tunnels::TestInfo, tini2p::data::I2NPMessage> test;
  REQUIRE_NOTHROW(test = out_pool.CreateZeroHopTunnels(creator_info).CreateTestMessage());
  REQUIRE(out_pool.has_test(test.first.message_id));
  REQUIRE(!out_pool.has_test(test.first.message_id + 1));
}

TEST_CASE_METHOD(AESPoolFixture, "AES Pools reject creating ZeroHop tunnels with a null RouterInfo", "[aes_pool]")
{
  REQUIRE_THROWS(in_pool.CreateZeroHopTunnels(nullptr));
  REQUIRE_THROWS(out_pool.CreateZeroHopTunnels(nullptr));
}

TEST_CASE_METHOD(
    AESPoolFixture,
    "AES InboundPool rejects creating test messages without established tunnels",
    "[aes_pool]")
{
  REQUIRE_THROWS(in_pool.CreateTestMessage());
}

TEST_CASE_METHOD(
    AESPoolFixture,
    "AES OutboundPool rejects creating test messages without established or ZeroHop tunnels",
    "[aes_pool]")
{
  // Check that the pool throws without established tunnels
  REQUIRE_THROWS(out_pool.CreateTestMessage());

  // Establish a tunnel in the pool
  AESOutboundPool::message_id_t msg_id(0);

  REQUIRE_NOTHROW(msg_id = out_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  const auto out_id = out_pool.building_tunnels().front().creator().tunnel_id();
  auto& out_tunnel = out_pool.building_tunnel(out_id);
  auto& out_creator = out_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(out_pool.HandleBuildReply(out_creator.CreateAESReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Check that the pool throws without ZeroHop tunnels
  REQUIRE_THROWS(out_pool.CreateTestMessage());
}

TEST_CASE_METHOD(AESPoolFixture, "AES InboundPool handles test replies", "[aes_pool]")
{
  AESInboundPool::message_id_t msg_id(0);

  REQUIRE_NOTHROW(msg_id = in_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  const auto in_id = in_pool.building_tunnels().front().creator().tunnel_id();
  auto& in_tunnel = in_pool.building_tunnel(in_id);
  auto& in_creator = in_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(in_pool.HandleBuildReply(in_creator.CreateAESReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Create test message
  std::pair<tini2p::tunnels::TestInfo, tini2p::data::I2NPMessage> test;
  REQUIRE_NOTHROW(test = in_pool.CreateTestMessage());

  // Handle test reply
  REQUIRE_NOTHROW(in_pool.HandleTestReply(test.second));
}

TEST_CASE_METHOD(AESPoolFixture, "AES OutboundPool handles test replies", "[aes_pool]")
{
  AESOutboundPool::message_id_t msg_id(0);

  REQUIRE_NOTHROW(msg_id = out_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  const auto out_id = out_pool.building_tunnels().front().creator().tunnel_id();
  auto& out_tunnel = out_pool.building_tunnel(out_id);
  auto& out_creator = out_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(out_pool.HandleBuildReply(out_creator.CreateAESReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Create ZeroHop tunnels, and a test message
  std::pair<tini2p::tunnels::TestInfo, tini2p::data::I2NPMessage> test;
  REQUIRE_NOTHROW(test = out_pool.CreateZeroHopTunnels(creator_info).CreateTestMessage());

  // Handle test reply
  REQUIRE_NOTHROW(out_pool.HandleTestReply(test.second));
}

/*------------------\
| ChaCha Pool Tests |
\------------------*/

using ChaChaInboundPool = tini2p::tunnels::Pool<tini2p::tunnels::InboundTunnel<TunnelChaCha>>;
using ChaChaOutboundPool = tini2p::tunnels::Pool<tini2p::tunnels::OutboundTunnel<TunnelChaCha>>;

struct ChaChaPoolFixture
{
  ChaChaPoolFixture()
      : creator_info(std::make_shared<Info>()),
        reply_lease(RandBytes<32>()),
        hop_infos{{std::make_shared<Info>()}, {std::make_shared<Info>()}, {std::make_shared<Info>()}}
  {
  }

  Info::shared_ptr creator_info;
  Lease2 reply_lease;
  std::vector<Info::shared_ptr> hop_infos;

  ChaChaInboundPool in_pool;
  ChaChaOutboundPool out_pool;
};

TEST_CASE_METHOD(ChaChaPoolFixture, "ChaCha Pool has valid fields", "[chacha_pool]")
{
  REQUIRE(in_pool.tunnels().empty());
  REQUIRE(in_pool.building_tunnels().empty());

  REQUIRE(out_pool.tunnels().empty());
  REQUIRE(out_pool.building_tunnels().empty());
}

TEST_CASE_METHOD(ChaChaPoolFixture, "ChaCha InboundPool can add a tunnel", "[chacha_pool]")
{
  REQUIRE_NOTHROW(in_pool.AddTunnel(creator_info, {}, hop_infos));

  // Check that InboundPool adds an building tunnel
  REQUIRE(in_pool.building_tunnels().size() == 1);
  // Exploratory tunnel only moved to established tunnel after receiving accepted VariableTunnelReplyMessage
  REQUIRE(in_pool.tunnels().size() == 0);
}

TEST_CASE_METHOD(ChaChaPoolFixture, "ChaCha OutboundPool can add a tunnel", "[chacha_pool]")
{
  REQUIRE_NOTHROW(out_pool.AddTunnel(creator_info, reply_lease, hop_infos));

  // Check that InboundPool adds an building tunnel
  REQUIRE(out_pool.building_tunnels().size() == 1);
  // Exploratory tunnel only moved to established tunnel after receiving accepted VariableTunnelReplyMessage
  REQUIRE(out_pool.tunnels().size() == 0);
}

TEST_CASE_METHOD(ChaChaPoolFixture, "ChaCha InboundPool handles accepted TunnelBuildReply messages", "[chacha_pool]")
{
  ChaChaInboundPool::message_id_t msg_id(0);

  // Check that the InboundTunnelPool handles TunnelBuildReply messages
  REQUIRE_NOTHROW(msg_id = in_pool.AddTunnel(creator_info, {}, hop_infos).message_id());

  // Unrealistic, get the building tunnel and create a TunnelBuildReply message
  // Normally, tunnel hops create the message during TunnelBuildMessage processing
  const auto& exp_tunnels = in_pool.building_tunnels();
  REQUIRE(exp_tunnels.size() == 1);

  const auto exp_id = exp_tunnels.front().creator().tunnel_id();
  auto& exp_tunnel = in_pool.building_tunnel(exp_id);
  auto& exp_creator = exp_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(in_pool.HandleBuildReply(exp_creator.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllAccept)));

  REQUIRE(exp_tunnels.size() == 0);
  REQUIRE_THROWS(in_pool.building_tunnel(exp_id));

  REQUIRE(in_pool.tunnels().size() == 1);
  REQUIRE_NOTHROW(in_pool.tunnel(exp_id));
}

TEST_CASE_METHOD(ChaChaPoolFixture, "ChaCha OutboundPool handles accepted TunnelBuildReply messages", "[chacha_pool]")
{
  ChaChaOutboundPool::message_id_t msg_id(0);

  // Check that the OutboundTunnelPool handles TunnelBuildReply messages
  REQUIRE_NOTHROW(msg_id = out_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  // Unrealistic, get the building tunnel and create a TunnelBuildReply message
  // Normally, tunnel hops create the message during TunnelBuildMessage processing
  const auto& exp_tunnels = out_pool.building_tunnels();
  REQUIRE(exp_tunnels.size() == 1);

  const auto exp_id = exp_tunnels.front().creator().tunnel_id();
  auto& exp_tunnel = out_pool.building_tunnel(exp_id);
  auto& exp_creator = exp_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(out_pool.HandleBuildReply(exp_creator.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllAccept)));
  REQUIRE(exp_tunnels.size() == 0);
  REQUIRE_THROWS(out_pool.building_tunnel(exp_id));

  REQUIRE(out_pool.tunnels().size() == 1);
  REQUIRE_NOTHROW(out_pool.tunnel(exp_id));
}

TEST_CASE_METHOD(
    ChaChaPoolFixture,
    "ChaCha OutboundPool handles creating tunnel messages for an OutboundTunnel",
    "[chacha_pool]")
{
  ChaChaOutboundPool::message_id_t msg_id(0);

  // Create an OutboundTunnel for creating TunnelMessages
  REQUIRE_NOTHROW(msg_id = out_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  const auto out_id = out_pool.building_tunnels().front().creator().tunnel_id();
  auto& exp_tunnel = out_pool.building_tunnel(out_id);
  auto& exp_creator = exp_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(out_pool.HandleBuildReply(exp_creator.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Unrealistic, setup random I2NP buffer and IBGW identifiers
  ChaChaOutboundPool::i2np_buffer_t i2np_buf(RandBuffer(0x23));
  ChaChaOutboundPool::to_hash_t ibgw_to_hash(RandBytes<32>());
  ChaChaOutboundPool::tunnel_id_t ibgw_id(0x41);

  // Create TunnelMessages for the established OutboundTunnel
  std::vector<ChaChaOutboundPool::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = out_pool.HandleI2NPMessage(out_id, i2np_buf, ibgw_to_hash, ibgw_id).second);

  REQUIRE(messages.size() == 1);
}

TEST_CASE_METHOD(ChaChaPoolFixture, "ChaCha InboundPool processes messages created by an IBGW", "[chacha_pool]")
{
  ChaChaOutboundPool::message_id_t msg_id(0);

  // Create an InboundTunnel for handling TunnelMessages
  REQUIRE_NOTHROW(msg_id = in_pool.AddTunnel(creator_info, {}, hop_infos).message_id());

  const auto in_id = in_pool.building_tunnels().front().creator().tunnel_id();
  auto& in_tunnel = in_pool.building_tunnel(in_id);
  auto& in_creator = in_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(in_pool.HandleBuildReply(in_creator.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Somewhat unrealistic, setup random I2NP buffer
  // Normally, would come as a TunnelGateway message from an OBEP of another tunnel
  data::I2NPMessage i2np_msg(data::I2NPHeader::Type::Data, 0x42, tini2p::data::Data(32).buffer());

  auto& hops = in_creator.hops();
  const auto ibgw_hop = hops.begin();

  // Unrealistic, transform the first hop into an IBGW
  // Normally, the IBGW processing would be done by a remote router
  tini2p::tunnels::InboundGateway<TunnelChaCha> ibgw(
      ibgw_hop->tunnel_id(), ibgw_hop->next_tunnel_id(), std::make_unique<TunnelChaCha>(ibgw_hop->key_info()));

  // Create TunnelMessages for the established InboundTunnel
  std::vector<ChaChaInboundPool::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = ibgw.CreateTunnelMessages(i2np_msg.buffer(), std::nullopt, std::nullopt));

  // Process each outbound TunnelMessage
  const auto last_hop = --hops.end();
  ChaChaInboundPool::tunnel_t::creator_t::extracted_i2np_t ret_i2np_msgs;
  for (auto& message : messages)
    {
      // Somewhat unrealistic, simulate InboundTunnel hops encrypting the TunnelMessage
      REQUIRE_NOTHROW(in_creator.IterativeEncryption(message));
      REQUIRE_NOTHROW(last_hop->EncryptAEAD(message));
      // Process the TunnelMessage as the IBEP
      REQUIRE_NOTHROW(ret_i2np_msgs = in_pool.HandleTunnelMessage(message));
    }

  REQUIRE(ret_i2np_msgs.size() == 1);
  REQUIRE(ret_i2np_msgs.front().second == i2np_msg);
}

TEST_CASE_METHOD(ChaChaPoolFixture, "ChaCha InboundPool creates test messages", "[chacha_pool]")
{
  ChaChaInboundPool::message_id_t msg_id(0);

  REQUIRE_NOTHROW(msg_id = in_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  const auto in_id = in_pool.building_tunnels().front().creator().tunnel_id();
  auto& in_tunnel = in_pool.building_tunnel(in_id);
  auto& in_creator = in_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(in_pool.HandleBuildReply(in_creator.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Create test message
  std::pair<tini2p::tunnels::TestInfo, tini2p::data::I2NPMessage> test;
  REQUIRE_NOTHROW(test = in_pool.CreateTestMessage());
  REQUIRE(in_pool.has_test(test.first.message_id));
  REQUIRE(!in_pool.has_test(test.first.message_id + 1));
}

TEST_CASE_METHOD(ChaChaPoolFixture, "ChaCha OutboundPool creates test messages", "[chacha_pool]")
{
  ChaChaOutboundPool::message_id_t msg_id(0);

  REQUIRE_NOTHROW(msg_id = out_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  const auto out_id = out_pool.building_tunnels().front().creator().tunnel_id();
  auto& out_tunnel = out_pool.building_tunnel(out_id);
  auto& out_creator = out_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(out_pool.HandleBuildReply(out_creator.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Create ZeroHop tunnels, and a test message
  std::pair<tini2p::tunnels::TestInfo, tini2p::data::I2NPMessage> test;
  REQUIRE_NOTHROW(test = out_pool.CreateZeroHopTunnels(creator_info).CreateTestMessage());
  REQUIRE(out_pool.has_test(test.first.message_id));
  REQUIRE(!out_pool.has_test(test.first.message_id + 1));
}

TEST_CASE_METHOD(ChaChaPoolFixture, "ChaCha Pools reject creating ZeroHop tunnels with a null RouterInfo", "[chacha_pool]")
{
  REQUIRE_THROWS(in_pool.CreateZeroHopTunnels(nullptr));
  REQUIRE_THROWS(out_pool.CreateZeroHopTunnels(nullptr));
}

TEST_CASE_METHOD(
    ChaChaPoolFixture,
    "ChaCha InboundPool rejects creating test messages without established tunnels",
    "[chacha_pool]")
{
  REQUIRE_THROWS(in_pool.CreateTestMessage());
}

TEST_CASE_METHOD(
    ChaChaPoolFixture,
    "ChaCha OutboundPool rejects creating test messages without established or ZeroHop tunnels",
    "[chacha_pool]")
{
  // Check that the pool throws without established tunnels
  REQUIRE_THROWS(out_pool.CreateTestMessage());

  // Establish a tunnel in the pool
  ChaChaOutboundPool::message_id_t msg_id(0);

  REQUIRE_NOTHROW(msg_id = out_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  const auto out_id = out_pool.building_tunnels().front().creator().tunnel_id();
  auto& out_tunnel = out_pool.building_tunnel(out_id);
  auto& out_creator = out_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(out_pool.HandleBuildReply(out_creator.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Check that the pool throws without ZeroHop tunnels
  REQUIRE_THROWS(out_pool.CreateTestMessage());
}

TEST_CASE_METHOD(ChaChaPoolFixture, "ChaCha InboundPool handles test replies", "[chacha_pool]")
{
  ChaChaInboundPool::message_id_t msg_id(0);

  REQUIRE_NOTHROW(msg_id = in_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  const auto in_id = in_pool.building_tunnels().front().creator().tunnel_id();
  auto& in_tunnel = in_pool.building_tunnel(in_id);
  auto& in_creator = in_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(in_pool.HandleBuildReply(in_creator.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Create test message
  std::pair<tini2p::tunnels::TestInfo, tini2p::data::I2NPMessage> test;
  REQUIRE_NOTHROW(test = in_pool.CreateTestMessage());

  // Handle test reply
  REQUIRE_NOTHROW(in_pool.HandleTestReply(test.second));
}

TEST_CASE_METHOD(ChaChaPoolFixture, "ChaCha OutboundPool handles test replies", "[chacha_pool]")
{
  ChaChaOutboundPool::message_id_t msg_id(0);

  REQUIRE_NOTHROW(msg_id = out_pool.AddTunnel(creator_info, reply_lease, hop_infos).message_id());

  const auto out_id = out_pool.building_tunnels().front().creator().tunnel_id();
  auto& out_tunnel = out_pool.building_tunnel(out_id);
  auto& out_creator = out_tunnel.creator();

  // Create a mock VariableTunnelBuildReply message, and move the building tunnel to established tunnels
  REQUIRE_NOTHROW(out_pool.HandleBuildReply(out_creator.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllAccept)));

  // Create ZeroHop tunnels, and a test message
  std::pair<tini2p::tunnels::TestInfo, tini2p::data::I2NPMessage> test;
  REQUIRE_NOTHROW(test = out_pool.CreateZeroHopTunnels(creator_info).CreateTestMessage());

  // Handle test reply
  REQUIRE_NOTHROW(out_pool.HandleTestReply(test.second));
}
