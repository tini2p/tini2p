/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/tunnels/pool_manager.h"

namespace crypto = tini2p::crypto;
namespace data = tini2p::data;

using tini2p::tunnels::CreateReplyType;

/*-----------------------\
| AES Pool Manager Tests |
\-----------------------*/

using AESPoolMgr = tini2p::tunnels::PoolManager<crypto::TunnelAES>;

struct AESPoolMgrFixture
{
  AESPoolMgrFixture() : info(std::make_shared<data::Info>()), manager(info) {}

  decltype(auto) MockLayer()
  {
    const auto& hop_identity = info->identity();
    const auto& hop_hash = hop_identity.hash();

    // Create mock creator layer encryption
    const auto creator_info = std::make_shared<data::Info>();
    return AESPoolMgr::layer_t(AESPoolMgr::layer_t::key_info_t(hop_identity.crypto().pubkey, hop_hash));
  }

  data::Info::shared_ptr info;
  AESPoolMgr manager;
};

TEST_CASE_METHOD(AESPoolMgrFixture, "AES PoolManager handles TunnelBuildMessages", "[aes_pool_mgr]")
{
  const auto& hop_hash = info->identity().hash();
  const AESPoolMgr::tunnel_id_t hop_id(0x42), next_id(0x43);
  const AESPoolMgr::info_t::identity_t::hash_t next_hash(crypto::RandBytes<32>());

  // Create a mock BuildRequestRecord for our router to participate as a tunnel Hop
  AESPoolMgr::build_request_t request({hop_id, crypto::RandBytes<32>(), next_id, next_hash, hop_hash}, crypto::RandBytes<32>());

  AESPoolMgr::build_message_t build_message({request});

  // Handle the TunnelBuildMessage as the PoolManager
  std::pair<AESPoolMgr::tunnel_id_t, AESPoolMgr::info_t::identity_t::hash_t> next_hop;

  // Create mock layer keys, unrealistic
  // Normally layer keys derived during request decryption, see router context
  REQUIRE_NOTHROW(manager.HandleBuildMessage(
      AESPoolMgr::layer_t::key_info_t(crypto::RandBytes<32>(), crypto::RandBytes<32>(), crypto::RandBytes<32>()), request));

  REQUIRE_NOTHROW(manager.find_transit_tunnel(hop_id));
}

TEST_CASE_METHOD(AESPoolMgrFixture, "AES PoolManager handles inbound TunnelBuildReply messages", "[aes_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  REQUIRE_NOTHROW(
      i2np_ptr = std::make_unique<data::I2NPMessage>(manager.AddInboundTunnel({std::make_shared<data::Info>()})));

  auto& in_pool = manager.inbound_pool();
  auto& in_tunnel = in_pool.building_tunnel(in_pool.building_tunnels().front().tunnel_id());

  // Create and process a TunnelBuildReply accepted by all hops
  // Unrealistic, normally each hop creates their reply record, and the TunnelBuildReply is created by the IBEP
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      in_tunnel.creator().CreateAESReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  REQUIRE(in_pool.tunnels().size() == 1);
  REQUIRE(in_pool.building_tunnels().empty());
}

TEST_CASE_METHOD(AESPoolMgrFixture, "AES PoolManager handles outbound TunnelBuildReply messages", "[aes_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  REQUIRE_NOTHROW(
      i2np_ptr = std::make_unique<data::I2NPMessage>(
          manager.AddOutboundTunnel(data::Lease2(crypto::RandBytes<32>()), {std::make_shared<data::Info>()})));

  auto& out_pool = manager.outbound_pool();
  auto& out_tunnel = out_pool.building_tunnel(out_pool.building_tunnels().front().tunnel_id());

  // Create and process a TunnelBuildReply accepted by all hops
  // Unrealistic, normally each hop creates their reply record, and the TunnelBuildReply is created by the IBEP
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      out_tunnel.creator().CreateAESReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  REQUIRE(out_pool.tunnels().size() == 1);
  REQUIRE(out_pool.building_tunnels().empty());
}

TEST_CASE_METHOD(
    AESPoolMgrFixture,
    "AES PoolManager rejects TunnelBuildReply messages for invalid tunnel",
    "[aes_pool_mgr]")
{
  // Check that both pools have no building tunnels
  REQUIRE(manager.inbound_pool().building_tunnels().empty());
  REQUIRE(manager.outbound_pool().building_tunnels().empty());

  // Rejects TunnelBuildReply for build message ID that has no corresponding building tunnel
  REQUIRE_THROWS(manager.HandleBuildReply(
      data::I2NPMessage(data::I2NPMessage::Type::VariableTunnelBuildReply, 0x01, crypto::RandBuffer(529))));
}

TEST_CASE_METHOD(AESPoolMgrFixture, "AES PoolManager adds a tunnel to the InboundPool", "[aes_pool_mgr]")
{
  std::unique_ptr<AESPoolMgr::build_message_t> build_msg_ptr;
  std::vector<data::Info::shared_ptr> hops{std::make_shared<data::Info>()};

  // Check that an inbound tunnel is added
  REQUIRE_NOTHROW(
      build_msg_ptr = std::make_unique<AESPoolMgr::build_message_t>(
          manager.AddInboundTunnel(hops).extract_body(), AESPoolMgr::build_message_t::deserialize_mode_t::Partial));

  // Check that a TunnelBuildMessage is returned with a BuildRequestRecord for the mock Hop
  REQUIRE_NOTHROW(
      build_msg_ptr->find_record(static_cast<data::Identity::trunc_hash_t>(hops.front()->identity().hash())));

  // Check that an building tunnel is added to the InboundPool
  REQUIRE(manager.inbound_pool().building_tunnels().size() == 1);
}

TEST_CASE_METHOD(AESPoolMgrFixture, "AES PoolManager adds a tunnel to the OutboundPool", "[aes_pool_mgr]")
{
  std::unique_ptr<AESPoolMgr::build_message_t> build_msg_ptr;
  std::vector<data::Info::shared_ptr> hops{std::make_shared<data::Info>()};

  // Unrealistic, create a mock Lease2 for a reply InboundGateway
  data::Lease2 lease(crypto::RandBytes<32>());

  // Check that an outbound tunnel is added
  REQUIRE_NOTHROW(
      build_msg_ptr = std::make_unique<AESPoolMgr::build_message_t>(
          manager.AddOutboundTunnel(lease, hops).extract_body(), AESPoolMgr::build_message_t::deserialize_mode_t::Partial));

  // Check that a TunnelBuildMessage is returned with a BuildRequestRecord for the mock Hop
  REQUIRE_NOTHROW(
      build_msg_ptr->find_record(static_cast<data::Identity::trunc_hash_t>(hops.front()->identity().hash())));

  // Check that an building tunnel is added to the OutboundPool
  REQUIRE(manager.outbound_pool().building_tunnels().size() == 1);
}

TEST_CASE_METHOD(AESPoolMgrFixture, "AES PoolManager rejects null tunnel Hops", "[aes_pool_mgr]")
{
  // Unrealistic, create a mock Lease2 for a reply InboundGateway
  data::Lease2 lease(crypto::RandBytes<32>());

  REQUIRE_THROWS(AESPoolMgr(nullptr));
  REQUIRE_THROWS(manager.AddInboundTunnel({}));
  REQUIRE_THROWS(manager.AddInboundTunnel({nullptr}));
  REQUIRE_THROWS(manager.AddOutboundTunnel(lease, {}));
  REQUIRE_THROWS(manager.AddOutboundTunnel(lease, {nullptr}));
}

TEST_CASE_METHOD(AESPoolMgrFixture, "AES PoolManager creates inbound test messages", "[aes_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  // Add an inbound tunnel
  REQUIRE_NOTHROW(
      i2np_ptr = std::make_unique<data::I2NPMessage>(manager.AddInboundTunnel({std::make_shared<data::Info>()})));

  auto& in_pool = manager.inbound_pool();
  auto& in_tunnel = in_pool.building_tunnel(in_pool.building_tunnels().front().tunnel_id());

  // Create a mock AllAccept TunnelBuildReply, and move the tunnel to established tunnels
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      in_tunnel.creator().CreateAESReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  AESPoolMgr::inbound_pool_t::message_id_t msg_id;

  // Create a test message for the established tunnel
  REQUIRE_NOTHROW(msg_id = manager.CreateInboundTestMessage().first.message_id);
  REQUIRE(manager.inbound_pool().has_test(msg_id));
}

TEST_CASE_METHOD(AESPoolMgrFixture, "AES PoolManager creates outbound test messages", "[aes_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  // Add an inbound tunnel
  REQUIRE_NOTHROW(
      i2np_ptr =
          std::make_unique<data::I2NPMessage>(manager.AddOutboundTunnel(data::Lease2(crypto::RandBytes<32>()), {std::make_shared<data::Info>()})));

  auto& out_pool = manager.outbound_pool();
  auto& out_tunnel = out_pool.building_tunnel(out_pool.building_tunnels().front().tunnel_id());

  // Create a mock AllAccept TunnelBuildReply, and move the tunnel to established tunnels
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      out_tunnel.creator().CreateAESReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  AESPoolMgr::inbound_pool_t::message_id_t msg_id;

  // Create a test message for the established tunnel
  REQUIRE_NOTHROW(msg_id = manager.CreateOutboundTestMessage().first.message_id);
  REQUIRE(manager.outbound_pool().has_test(msg_id));
}

TEST_CASE_METHOD(
    AESPoolMgrFixture,
    "AES PoolManager rejects creating test messages for pools with no established tunnels",
    "[aes_pool_mgr]")
{
  REQUIRE_THROWS(manager.CreateInboundTestMessage());
  REQUIRE_THROWS(manager.CreateOutboundTestMessage());
}

TEST_CASE_METHOD(
    AESPoolMgrFixture,
    "AES PoolManager handles an incoming tunnel message for an InboundTunnel",
    "[aes_pool_mgr]")
{
  using delivery_t = AESPoolMgr::tunnel_message_t::first_delivery_t;

  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  REQUIRE_NOTHROW(i2np_ptr = std::make_unique<data::I2NPMessage>(manager.AddInboundTunnel({std::make_shared<data::Info>()})));

  auto& in_pool = manager.inbound_pool();
  const auto in_tunnel_id = in_pool.building_tunnels().front().tunnel_id();
  auto& in_tunnel = in_pool.building_tunnel(in_tunnel_id);

  // Create a mock I2NP Data message
  data::I2NPMessage i2np(data::I2NPMessage::Type::Data, 0x41, data::Data(32).buffer());
  const auto& i2np_buf = i2np.buffer();

  // Create a mock tunnel message
  AESPoolMgr::tunnel_message_t mock_message(
      in_tunnel.creator().tunnel_id(),
      AESPoolMgr::layer_t::create_randomizer(),
      delivery_t(tini2p::data::Layer::AES, delivery_t::mode_t::Unfragmented, i2np_buf.size()),
      i2np_buf);

  // Unrealistic, simulate the IBGW and tunnel hops encrypting the tunnel message
  REQUIRE_NOTHROW(in_tunnel.creator().IterativeEncryption(mock_message));

  // Create an AllAccept TunnelBuildReply, and move the inbound tunnel to established tunnels
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      in_tunnel.creator().CreateAESReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  data::I2NPMessage tun_msg(data::I2NPMessage::Type::TunnelData, 0x41, mock_message.buffer()); 

  // Handle the mock tunnel message destined for the established IBEP
  REQUIRE_NOTHROW(i2np_ptr = std::make_unique<data::I2NPMessage>(manager.HandleTunnelMessage(tun_msg).front().second));
  REQUIRE(*i2np_ptr == i2np);
}

TEST_CASE_METHOD(
    AESPoolMgrFixture,
    "AES PoolManager handles an incoming tunnel message for a Hop TransitTunnel",
    "[aes_pool_mgr]")
{
  using delivery_t = AESPoolMgr::tunnel_message_t::first_delivery_t;

  const auto& hop_hash = info->identity().hash();
  const AESPoolMgr::build_request_t::tunnel_id_t hop_id(0x42);

  // Create a mock BuildRequestRecord for our router to participate as a tunnel Hop
  AESPoolMgr::build_request_t request({hop_id, crypto::RandBytes<32>(), 0x43, crypto::RandBytes<32>(), hop_hash}, crypto::RandBytes<32>());

  // Handle the TunnelBuildMessage as the PoolManager
  // Create mock layer key info, unrealistic
  // Normally, layer keys are derived during record decryption, see router context
  REQUIRE_NOTHROW(manager.HandleBuildMessage(
      AESPoolMgr::layer_t::key_info_t(crypto::RandBytes<32>(), crypto::RandBytes<32>(), crypto::RandBytes<32>()), request));

  // Check that a TransitTunnel was added to the PoolManager
  REQUIRE_NOTHROW(manager.find_transit_tunnel(hop_id));

  // Create a mock I2NP Data message
  data::I2NPMessage i2np(data::I2NPMessage::Type::Data, 0x41, data::Data(32).buffer());
  const auto& i2np_buf = i2np.buffer();

  // Create a mock tunnel message
  AESPoolMgr::tunnel_message_t mock_message(
      hop_id,
      AESPoolMgr::layer_t::create_randomizer(),
      delivery_t(tini2p::data::Layer::AES, delivery_t::mode_t::Unfragmented, i2np_buf.size()),
      i2np_buf);

  data::I2NPMessage tun_msg(data::I2NPMessage::Type::TunnelData, 0x41, mock_message.buffer());

  REQUIRE_NOTHROW(manager.HandleTunnelMessage(tun_msg));
}

TEST_CASE_METHOD(
    AESPoolMgrFixture,
    "AES PoolManager handles an incoming tunnel message for a OBEP TransitTunnel",
    "[aes_pool_mgr]")
{
  using delivery_t = AESPoolMgr::tunnel_message_t::first_delivery_t;

  const auto& hop_hash = info->identity().hash();
  const AESPoolMgr::build_request_t::tunnel_id_t hop_id(0x42);

  // Create a mock BuildRequestRecord for our router to participate as a tunnel OBEP
  AESPoolMgr::build_request_t request(
      {hop_id, crypto::RandBytes<32>(), 0x43, crypto::RandBytes<32>(), hop_hash},
      crypto::RandBytes<32>(),
      AESPoolMgr::build_request_t::flags_t::OBEP);

  // Handle the TunnelBuildMessage as the PoolManager
  // Create mock layer key info, unrealistic
  // Normally, layer keys are derived during record decryption, see router context
  REQUIRE_NOTHROW(manager.HandleBuildMessage(
      AESPoolMgr::layer_t::key_info_t(crypto::RandBytes<32>(), crypto::RandBytes<32>(), crypto::RandBytes<32>()), request));

  // Check that a TransitTunnel was added to the PoolManager
  REQUIRE_NOTHROW(manager.find_transit_tunnel(hop_id));

  // Create a mock I2NP Data message
  data::I2NPMessage i2np(data::I2NPMessage::Type::Data, 0x41, data::Data(32).buffer());
  const auto& i2np_buf = i2np.buffer();

  // Create a mock tunnel message
  AESPoolMgr::tunnel_message_t mock_message(
      hop_id,
      AESPoolMgr::layer_t::create_randomizer(),
      delivery_t(tini2p::data::Layer::AES, delivery_t::mode_t::Unfragmented, i2np_buf.size(), crypto::RandBytes<32>()),
      i2np_buf);

  // Unrealistic, simulate the OBGW iteratively decrypting for the OBEP
  REQUIRE_NOTHROW(std::get<AESPoolMgr::transit_tunnel_t::obep_t>(manager.find_transit_tunnel(hop_id).processor())
                      .Decrypt(mock_message));

  data::I2NPMessage tun_msg(data::I2NPMessage::Type::TunnelData, 0x41, mock_message.buffer());

  // Handle the mock tunnel message destined for the OBEP
  std::unique_ptr<data::I2NPMessage> i2np_ptr;
  REQUIRE_NOTHROW(i2np_ptr = std::make_unique<data::I2NPMessage>(manager.HandleTunnelMessage(tun_msg).front().second));
  REQUIRE(*i2np_ptr == i2np);
}

TEST_CASE_METHOD(
    AESPoolMgrFixture,
    "AES PoolManager rejects incoming tunnel message for invalid tunnel",
    "[aes_pool_mgr]")
{
  // Create a mock tunnel message for a non-existent tunnel
  AESPoolMgr::tunnel_message_t mock_message(
      0x41,
      AESPoolMgr::layer_t::create_randomizer(),
      data::FirstDelivery(data::Layer::AES, data::FirstDelivery::mode_t::Unfragmented, 16, crypto::RandBytes<32>()),
      data::I2NPMessage().buffer());

  data::I2NPMessage tun_msg(data::I2NPMessage::Type::TunnelData, 0x34, mock_message.buffer());

  REQUIRE_THROWS(manager.HandleTunnelMessage(tun_msg));
}

TEST_CASE_METHOD(
    AESPoolMgrFixture,
    "AES PoolManager handles an incoming I2NP message for a IBGW TransitTunnel",
    "[aes_pool_mgr]")
{
  auto creator_layer = MockLayer();
  const auto& creator_key_info = creator_layer.key_info();

  const auto& hop_hash = info->identity().hash();
  const AESPoolMgr::build_request_t::tunnel_id_t hop_id(0x42);

  // Create a mock BuildRequestRecord for our router to participate as a tunnel OBEP
  AESPoolMgr::build_request_t request(
      {hop_id, crypto::RandBytes<32>(), 0x43, crypto::RandBytes<32>(), hop_hash},
      creator_key_info.creator_pubkey(),
      AESPoolMgr::build_request_t::flags_t::IBGW);

  // Handle the TunnelBuildMessage as the PoolManager
  // Create mock layer key info, unrealistic
  // Normally, layer keys are derived during record decryption, see router context
  REQUIRE_NOTHROW(manager.HandleBuildMessage(
      AESPoolMgr::layer_t::key_info_t(crypto::RandBytes<32>(), crypto::RandBytes<32>(), crypto::RandBytes<32>()), request));

  // Check that a TransitTunnel was added to the PoolManager
  REQUIRE_NOTHROW(manager.find_transit_tunnel(hop_id));

  data::I2NPMessage data_block(data::I2NPMessage::Type::Data, 0x41, data::Data(32).buffer());

  // Create a mock I2NP TunnelGateway message
  data::I2NPMessage i2np(data::I2NPMessage::Type::TunnelGateway, 0x41, data::TunnelGateway(hop_id, data_block.buffer()).buffer());

  REQUIRE_NOTHROW(manager.HandleI2NPMessage(i2np));
}

TEST_CASE_METHOD(
    AESPoolMgrFixture,
    "AES PoolManager handles incoming test replies for InboundTunnels",
    "[aes_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  // Add an inbound tunnel
  REQUIRE_NOTHROW(
      i2np_ptr =
          std::make_unique<data::I2NPMessage>(manager.AddInboundTunnel({std::make_shared<data::Info>()})));

  auto& in_pool = manager.inbound_pool();
  auto& in_tunnel = in_pool.building_tunnel(in_pool.building_tunnels().front().tunnel_id());

  // Create a mock AllAccept TunnelBuildReply, and move the tunnel to established tunnels
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      in_tunnel.creator().CreateAESReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  std::pair<AESPoolMgr::inbound_pool_t::test_info_t, data::I2NPMessage> test_pair;

  // Create a test message for the established tunnel
  REQUIRE_NOTHROW(test_pair = manager.CreateInboundTestMessage());
  REQUIRE_NOTHROW(manager.HandleI2NPMessage(test_pair.second));
}

TEST_CASE_METHOD(
    AESPoolMgrFixture,
    "AES PoolManager handles incoming test replies for OutboundTunnels",
    "[aes_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  // Add an inbound tunnel
  REQUIRE_NOTHROW(
      i2np_ptr = std::make_unique<data::I2NPMessage>(
          manager.AddOutboundTunnel(data::Lease2(crypto::RandBytes<32>()), {std::make_shared<data::Info>()})));

  auto& out_pool = manager.outbound_pool();
  const auto out_tunnel_id = out_pool.building_tunnels().front().tunnel_id();
  auto& out_tunnel = out_pool.building_tunnel(out_tunnel_id);

  // Create a mock AllAccept TunnelBuildReply, and move the tunnel to established tunnels
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      out_tunnel.creator().CreateAESReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  // Create a test message for the established tunnel
  std::pair<AESPoolMgr::outbound_pool_t::test_info_t, std::vector<AESPoolMgr::tunnel_message_t>> test_pair;
  REQUIRE_NOTHROW(test_pair = manager.CreateOutboundTestMessage());
  auto& out_creator = out_pool.tunnel(out_tunnel_id).creator();

  // Unrealistic, simulate the outbound hops encrypting and processing the tunnel message(s)
  tini2p::tunnels::OutboundEndpoint<AESPoolMgr::layer_t> obep;
  std::vector<data::I2NPMessage> i2np_msgs;
  for (auto& message : test_pair.second)
    {
      out_creator.IterativeEncryption(message);
      auto i2np_blocks = obep.ExtractI2NPFragments(message);
      for (auto& block : i2np_blocks)
        i2np_msgs.emplace_back(std::move(block.second));
    }

  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE_NOTHROW(manager.HandleI2NPMessage(i2np_msgs.front()));
}

TEST_CASE_METHOD(AESPoolMgrFixture, "AES PoolManager rejects invalid incoming I2NP message types", "[aes_pool_mgr]")
{
  // Inbound I2NP handler requires an I2NP TunnelGateway or Data message, rejecting all others.
  // All other types must be wrapped in a TunnelGateway message to be handled by IBGW TransitTunnels
  crypto::SecBytes bad_buf(crypto::RandBuffer(32));

  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::DatabaseStore, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::DatabaseLookup, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::DatabaseSearchReply, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::DeliveryStatus, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::Garlic, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::TunnelData, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::TunnelBuild, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::TunnelBuildReply, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::VariableTunnelBuild, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::VariableTunnelBuildReply, 0x41, bad_buf)));
}

/*--------------------------\
| ChaCha Pool Manager Tests |
\--------------------------*/

using ChaChaPoolMgr = tini2p::tunnels::PoolManager<crypto::TunnelChaCha>;

struct ChaChaPoolMgrFixture
{
  ChaChaPoolMgrFixture() : info(std::make_shared<data::Info>()), manager(info) {}

  decltype(auto) MockLayer(const data::Info::shared_ptr& creator)
  {
    const auto& hop_identity = creator->identity();
    const auto& hop_hash = hop_identity.hash();

    // Create mock creator layer encryption
    return ChaChaPoolMgr::layer_t(ChaChaPoolMgr::layer_t::key_info_t(hop_identity.crypto().pubkey, hop_hash));
  }

  data::Info::shared_ptr info;
  ChaChaPoolMgr manager;
};

TEST_CASE_METHOD(ChaChaPoolMgrFixture, "ChaCha PoolManager handles TunnelBuildMessages", "[chacha_pool_mgr]")
{
  const auto& hop_hash = info->identity().hash();
  const ChaChaPoolMgr::tunnel_id_t hop_id(0x42), next_id(0x43);
  const data::Identity::hash_t next_hash(crypto::RandBytes<32>());

  // Create a mock BuildRequestRecord for our router to participate as a tunnel Hop
  ChaChaPoolMgr::build_request_t request(
      {hop_id, crypto::RandBytes<32>(), next_id, next_hash, hop_hash},
      crypto::RandBytes<32>(),
      ChaChaPoolMgr::layer_t::aead_t::create_key());

  // Handle the TunnelBuildMessage as the PoolManager
  // Create mock layer key info, unrealistic
  // Normally, layer keys are derived during record decryption, see router context
  REQUIRE_NOTHROW(manager.HandleBuildMessage(
      ChaChaPoolMgr::layer_t::key_info_t(crypto::RandBytes<32>(), crypto::RandBytes<32>(), crypto::RandBytes<32>()), request));

  // Check that a TransitTunnel was added to the PoolManager
  REQUIRE_NOTHROW(manager.find_transit_tunnel(hop_id));
}

TEST_CASE_METHOD(ChaChaPoolMgrFixture, "ChaCha PoolManager adds a tunnel to the InboundPool", "[chacha_pool_mgr]")
{
  std::unique_ptr<ChaChaPoolMgr::build_message_t> build_msg_ptr;
  std::vector<data::Info::shared_ptr> hops{std::make_shared<data::Info>()};

  // Check that an inbound tunnel is added
  REQUIRE_NOTHROW(
      build_msg_ptr = std::make_unique<ChaChaPoolMgr::build_message_t>(
          manager.AddInboundTunnel(hops).extract_body(), ChaChaPoolMgr::build_message_t::deserialize_mode_t::Partial));

  // Check that a TunnelBuildMessage is returned with a BuildRequestRecord for the mock Hop
  REQUIRE_NOTHROW(
      build_msg_ptr->find_record(static_cast<data::Identity::trunc_hash_t>(hops.front()->identity().hash())));

  // Check that an building tunnel is added to the InboundPool
  REQUIRE(manager.inbound_pool().building_tunnels().size() == 1);
}

TEST_CASE_METHOD(ChaChaPoolMgrFixture, "ChaCha PoolManager adds a tunnel to the OutboundPool", "[chacha_pool_mgr]")
{
  std::unique_ptr<ChaChaPoolMgr::build_message_t> build_msg_ptr;
  std::vector<data::Info::shared_ptr> hops{std::make_shared<data::Info>()};

  // Unrealistic, create a mock Lease2 for a reply InboundGateway
  data::Lease2 lease(crypto::RandBytes<32>());

  // Check that an outbound tunnel is added
  REQUIRE_NOTHROW(
      build_msg_ptr = std::make_unique<ChaChaPoolMgr::build_message_t>(
          manager.AddOutboundTunnel(lease, hops).extract_body(),
          ChaChaPoolMgr::build_message_t::deserialize_mode_t::Partial));

  // Check that a TunnelBuildMessage is returned with a BuildRequestRecord for the mock Hop
  REQUIRE_NOTHROW(
      build_msg_ptr->find_record(static_cast<data::Identity::trunc_hash_t>(hops.front()->identity().hash())));

  // Check that an building tunnel is added to the OutboundPool
  REQUIRE(manager.outbound_pool().building_tunnels().size() == 1);
}

TEST_CASE_METHOD(
    ChaChaPoolMgrFixture,
    "ChaCha PoolManager handles inbound TunnelBuildReply messages",
    "[chacha_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  REQUIRE_NOTHROW(
      i2np_ptr = std::make_unique<data::I2NPMessage>(manager.AddInboundTunnel({std::make_shared<data::Info>()})));

  auto& in_pool = manager.inbound_pool();
  auto& in_tunnel = in_pool.building_tunnel(in_pool.building_tunnels().front().tunnel_id());

  // Create and process a TunnelBuildReply accepted by all hops
  // Unrealistic, normally each hop creates their reply record, and the TunnelBuildReply is created by the IBEP
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      in_tunnel.creator().CreateChaChaReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  REQUIRE(in_pool.tunnels().size() == 1);
  REQUIRE(in_pool.building_tunnels().empty());
}

TEST_CASE_METHOD(
    ChaChaPoolMgrFixture,
    "ChaCha PoolManager handles outbound TunnelBuildReply messages",
    "[chacha_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  REQUIRE_NOTHROW(
      i2np_ptr = std::make_unique<data::I2NPMessage>(
          manager.AddOutboundTunnel(data::Lease2(crypto::RandBytes<32>()), {std::make_shared<data::Info>()})));

  auto& out_pool = manager.outbound_pool();
  auto& out_tunnel = out_pool.building_tunnel(out_pool.building_tunnels().front().tunnel_id());

  // Create and process a TunnelBuildReply accepted by all hops
  // Unrealistic, normally each hop creates their reply record, and the TunnelBuildReply is created by the IBEP
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      out_tunnel.creator().CreateChaChaReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  REQUIRE(out_pool.tunnels().size() == 1);
  REQUIRE(out_pool.building_tunnels().empty());
}

TEST_CASE_METHOD(
    ChaChaPoolMgrFixture,
    "ChaCha PoolManager rejects TunnelBuildReply messages for invalid tunnel",
    "[chacha_pool_mgr]")
{
  // Check that both pools have no building tunnels
  REQUIRE(manager.inbound_pool().building_tunnels().empty());
  REQUIRE(manager.outbound_pool().building_tunnels().empty());

  // Rejects TunnelBuildReply for build message ID that has no corresponding building tunnel
  REQUIRE_THROWS(manager.HandleBuildReply(data::I2NPMessage(
      data::I2NPMessage::Type::VariableTunnelBuildReply, 0x01, crypto::RandBuffer(529))));
}

TEST_CASE_METHOD(ChaChaPoolMgrFixture, "ChaCha PoolManager rejects null tunnel Hops", "[chacha_pool_mgr]")
{
  // Unrealistic, create a mock Lease2 for a reply InboundGateway
  data::Lease2 lease(crypto::RandBytes<32>());

  REQUIRE_THROWS(ChaChaPoolMgr(nullptr));
  REQUIRE_THROWS(manager.AddInboundTunnel({}));
  REQUIRE_THROWS(manager.AddInboundTunnel({nullptr}));
  REQUIRE_THROWS(manager.AddOutboundTunnel(lease, {}));
  REQUIRE_THROWS(manager.AddOutboundTunnel(lease, {nullptr}));
}

TEST_CASE_METHOD(ChaChaPoolMgrFixture, "ChaCha PoolManager creates inbound test messages", "[chacha_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  // Add an inbound tunnel
  REQUIRE_NOTHROW(
      i2np_ptr = std::make_unique<data::I2NPMessage>(manager.AddInboundTunnel({std::make_shared<data::Info>()})));

  auto& in_pool = manager.inbound_pool();
  auto& in_tunnel = in_pool.building_tunnel(in_pool.building_tunnels().front().tunnel_id());

  // Create a mock AllAccept TunnelBuildReply, and move the tunnel to established tunnels
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      in_tunnel.creator().CreateChaChaReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  ChaChaPoolMgr::inbound_pool_t::message_id_t msg_id;

  // Create a test message for the established tunnel
  REQUIRE_NOTHROW(msg_id = manager.CreateInboundTestMessage().first.message_id);
  REQUIRE(manager.inbound_pool().has_test(msg_id));
}

TEST_CASE_METHOD(ChaChaPoolMgrFixture, "ChaCha PoolManager creates outbound test messages", "[chacha_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  // Add an inbound tunnel
  REQUIRE_NOTHROW(
      i2np_ptr = std::make_unique<data::I2NPMessage>(
          manager.AddOutboundTunnel(data::Lease2(crypto::RandBytes<32>()), {std::make_shared<data::Info>()})));

  auto& out_pool = manager.outbound_pool();
  auto& out_tunnel = out_pool.building_tunnel(out_pool.building_tunnels().front().tunnel_id());

  // Create a mock AllAccept TunnelBuildReply, and move the tunnel to established tunnels
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      out_tunnel.creator().CreateChaChaReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  ChaChaPoolMgr::inbound_pool_t::message_id_t msg_id;

  // Create a test message for the established tunnel
  REQUIRE_NOTHROW(msg_id = manager.CreateOutboundTestMessage().first.message_id);
  REQUIRE(manager.outbound_pool().has_test(msg_id));
}

TEST_CASE_METHOD(
    ChaChaPoolMgrFixture,
    "ChaCha PoolManager rejects creating test messages for pools with no established tunnels",
    "[chacha_pool_mgr]")
{
  REQUIRE_THROWS(manager.CreateInboundTestMessage());
  REQUIRE_THROWS(manager.CreateOutboundTestMessage());
}

TEST_CASE_METHOD(
    ChaChaPoolMgrFixture,
    "ChaCha PoolManager handles an incoming tunnel message for an InboundTunnel",
    "[chacha_pool_mgr]")
{
  using delivery_t = ChaChaPoolMgr::tunnel_message_t::first_delivery_t;

  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  REQUIRE_NOTHROW(
      i2np_ptr = std::make_unique<data::I2NPMessage>(manager.AddInboundTunnel({std::make_shared<data::Info>()})));

  auto& in_pool = manager.inbound_pool();
  const auto in_tunnel_id = in_pool.building_tunnels().front().tunnel_id();
  auto& in_tunnel = in_pool.building_tunnel(in_tunnel_id);

  // Create a mock I2NP Data message
  data::I2NPMessage i2np(data::I2NPMessage::Type::Data, 0x41, data::Data(32).buffer());
  const auto& i2np_buf = i2np.buffer();

  // Create a mock tunnel message
  ChaChaPoolMgr::tunnel_message_t mock_message(
      in_tunnel.creator().tunnel_id(),
      ChaChaPoolMgr::layer_t::create_randomizer(),
      delivery_t(tini2p::data::Layer::ChaCha, delivery_t::mode_t::Unfragmented, i2np_buf.size()),
      i2np_buf);

  // Unrealistic, simulate the IBGW and tunnel hops encrypting the tunnel message
  REQUIRE_NOTHROW(in_tunnel.creator().IterativeEncryption(mock_message));

  // Unrealistic, simulate the last hop AEAD encrypting the mock message
  REQUIRE_NOTHROW(in_tunnel.creator().hops().back().EncryptAEAD(mock_message));

  // Create an AllAccept TunnelBuildReply, and move the inbound tunnel to established tunnels
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      in_tunnel.creator().CreateChaChaReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  data::I2NPMessage tun_msg(data::I2NPMessage::Type::TunnelData, 0x41, mock_message.buffer());

  // Handle the mock tunnel message destined for the established IBEP
  REQUIRE_NOTHROW(i2np_ptr = std::make_unique<data::I2NPMessage>(manager.HandleTunnelMessage(tun_msg).front().second));
  REQUIRE(*i2np_ptr == i2np);
}

TEST_CASE_METHOD(
    ChaChaPoolMgrFixture,
    "ChaCha PoolManager handles an incoming tunnel message for a Hop TransitTunnel",
    "[chacha_pool_mgr]")
{
  using delivery_t = ChaChaPoolMgr::tunnel_message_t::first_delivery_t;

  auto creator_info = std::make_shared<data::Info>();
  auto creator_layer = MockLayer(creator_info);
  const auto& creator_key_info = creator_layer.key_info();

  // Simulate the tunnel creator making a new set of build keys for our hop
  auto hop_layer = MockLayer(info);
  const auto& hop_key_info = hop_layer.key_info();

  const auto& hop_hash = info->identity().hash();
  const ChaChaPoolMgr::build_request_t::tunnel_id_t hop_id(0x42);

  // Create a mock BuildRequestRecord for our router to participate as a tunnel Hop
  ChaChaPoolMgr::build_request_t request(
      {hop_id, crypto::RandBytes<32>(), 0x43, crypto::RandBytes<32>(), hop_hash},
      hop_key_info.creator_pubkey(),
      creator_key_info.send_key(),
      ChaChaPoolMgr::build_request_t::flags_t::ChaChaHop);

  // Handle the TunnelBuildMessage as the PoolManager
  // Create mock layer key info, unrealistic
  // Normally, layer keys are derived during record decryption, see router context
  REQUIRE_NOTHROW(manager.HandleBuildMessage(
      ChaChaPoolMgr::layer_t::key_info_t(crypto::RandBytes<32>(), crypto::RandBytes<32>(), crypto::RandBytes<32>()), request));

  // Check that a TransitTunnel was added to the PoolManager
  REQUIRE_NOTHROW(manager.find_transit_tunnel(hop_id));

  // Create a mock I2NP Data message
  data::I2NPMessage i2np(data::I2NPMessage::Type::Data, 0x41, data::Data(32).buffer());
  const auto& i2np_buf = i2np.buffer();

  // Create a mock tunnel message
  ChaChaPoolMgr::tunnel_message_t mock_message(
      hop_id,
      ChaChaPoolMgr::layer_t::create_randomizer(),
      delivery_t(tini2p::data::Layer::ChaCha, delivery_t::mode_t::Unfragmented, i2np_buf.size()),
      i2np_buf);

  // Simulate the previous tunnel hop (here, the creator) AEAD encrypting the message for the hop
  REQUIRE_NOTHROW(creator_layer.EncryptAEAD(mock_message.buffer()));

  data::I2NPMessage tun_msg(data::I2NPMessage::Type::TunnelData, 0x41, mock_message.buffer());

  // Handle the mock tunnel message destined for the Hop
  REQUIRE_NOTHROW(manager.HandleTunnelMessage(tun_msg));
}

TEST_CASE_METHOD(
    ChaChaPoolMgrFixture,
    "ChaCha PoolManager handles an incoming tunnel message for a OBEP TransitTunnel",
    "[chacha_pool_mgr]")
{
  using delivery_t = ChaChaPoolMgr::tunnel_message_t::first_delivery_t;

  auto creator_info = std::make_shared<data::Info>();
  auto creator_layer = MockLayer(creator_info);
  const auto& creator_key_info = creator_layer.key_info();

  // Simulate the tunnel creator making a new set of build keys for our hop
  auto hop_layer = MockLayer(info);
  const auto& hop_key_info = hop_layer.key_info();

  const auto& hop_hash = info->identity().hash();
  const ChaChaPoolMgr::build_request_t::tunnel_id_t hop_id(0x42);

  // Create a mock BuildRequestRecord for our router to participate as a tunnel OBEP
  ChaChaPoolMgr::build_request_t request(
      {hop_id, crypto::RandBytes<32>(), 0x43, crypto::RandBytes<32>(), hop_hash},
      hop_key_info.creator_pubkey(),
      creator_key_info.send_key(),
      ChaChaPoolMgr::build_request_t::flags_t::OBEP);

  // Create mock layer key info, unrealistic
  // Normally, layer keys are derived during record decryption, see router context
  auto key_info = ChaChaPoolMgr::layer_t::key_info_t(crypto::RandBytes<32>(), crypto::RandBytes<32>(), crypto::RandBytes<32>());
  key_info.receive_key(creator_key_info.send_key());

  // Handle the TunnelBuildMessage as the PoolManager
  REQUIRE_NOTHROW(manager.HandleBuildMessage(std::move(key_info), request));

  // Check that a TransitTunnel was added to the PoolManager
  REQUIRE_NOTHROW(manager.find_transit_tunnel(hop_id));

  // Create a mock I2NP Data message
  data::I2NPMessage i2np(data::I2NPMessage::Type::Data, 0x41, data::Data(32).buffer());
  const auto& i2np_buf = i2np.buffer();

  // Create a mock tunnel message
  ChaChaPoolMgr::tunnel_message_t mock_message(
      hop_id,
      ChaChaPoolMgr::layer_t::create_randomizer(),
      delivery_t(tini2p::data::Layer::ChaCha, delivery_t::mode_t::Unfragmented, i2np_buf.size(), crypto::RandBytes<32>()),
      i2np_buf);

  // Unrealistic, simulate the OBGW iteratively decrypting for the OBEP
  auto& obep = std::get<ChaChaPoolMgr::transit_tunnel_t::obep_t>(manager.find_transit_tunnel(hop_id).processor());
  REQUIRE_NOTHROW(obep.Decrypt(mock_message));

  // Simulate the previous tunnel hop (here, the creator) AEAD encrypting the message for the OBEP
  REQUIRE_NOTHROW(creator_layer.EncryptAEAD(mock_message.buffer()));

  data::I2NPMessage tun_msg(data::I2NPMessage::Type::TunnelData, 0x41, mock_message.buffer());

  // Handle the mock tunnel message destined for the OBEP
  std::unique_ptr<data::I2NPMessage> i2np_ptr;
  REQUIRE_NOTHROW(
      i2np_ptr = std::make_unique<data::I2NPMessage>(manager.HandleTunnelMessage(tun_msg).front().second));
  REQUIRE(*i2np_ptr == i2np);
}

TEST_CASE_METHOD(
    ChaChaPoolMgrFixture,
    "ChaCha PoolManager rejects incoming tunnel message for invalid tunnel",
    "[chacha_pool_mgr]")
{
  // Create a mock tunnel message for a non-existent tunnel
  ChaChaPoolMgr::tunnel_message_t mock_message(
      0x41,
      ChaChaPoolMgr::layer_t::create_randomizer(),
      data::FirstDelivery(
          data::FirstDelivery::layer_t::ChaCha, data::FirstDelivery::mode_t::Unfragmented, 16, crypto::RandBytes<32>()),
      data::I2NPMessage().buffer());

  data::I2NPMessage tun_msg(data::I2NPMessage::Type::TunnelData, 0x41, mock_message.buffer());

  REQUIRE_THROWS(manager.HandleTunnelMessage(tun_msg));
}

TEST_CASE_METHOD(
    ChaChaPoolMgrFixture,
    "ChaCha PoolManager handles an incoming I2NP message for a IBGW TransitTunnel",
    "[chacha_pool_mgr]")
{
  const auto& hop_hash = info->identity().hash();
  const ChaChaPoolMgr::build_request_t::tunnel_id_t hop_id(0x42);

  // Create a mock BuildRequestRecord for our router to participate as a tunnel OBEP
  ChaChaPoolMgr::build_request_t request(
      {hop_id, crypto::RandBytes<32>(), 0x43, crypto::RandBytes<32>(), hop_hash},
      crypto::RandBytes<32>(),
      ChaChaPoolMgr::build_request_t::flags_t::IBGW);

  // Handle the TunnelBuildMessage as the PoolManager
  // Create mock layer key info, unrealistic
  // Normally, layer keys are derived during record decryption, see router context
  REQUIRE_NOTHROW(manager.HandleBuildMessage(
      ChaChaPoolMgr::layer_t::key_info_t(crypto::RandBytes<32>(), crypto::RandBytes<32>(), crypto::RandBytes<32>()), request));

  // Check that a TransitTunnel was added to the PoolManager
  REQUIRE_NOTHROW(manager.find_transit_tunnel(hop_id));

  data::I2NPMessage data_msg(data::I2NPMessage::Type::Data, 0x41, data::Data(32).buffer());

  // Create a mock I2NP TunnelGateway message
  data::I2NPMessage i2np(
      data::I2NPMessage::Type::TunnelGateway, 0x41, data::TunnelGateway(hop_id, data_msg.buffer()).buffer());

  REQUIRE_NOTHROW(manager.HandleI2NPMessage(i2np));
}

TEST_CASE_METHOD(
    ChaChaPoolMgrFixture,
    "ChaCha PoolManager handles incoming test replies for InboundTunnels",
    "[chacha_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  // Add an inbound tunnel
  REQUIRE_NOTHROW(
      i2np_ptr =
          std::make_unique<data::I2NPMessage>(manager.AddInboundTunnel({std::make_shared<data::Info>()})));

  auto& in_pool = manager.inbound_pool();
  auto& in_tunnel = in_pool.building_tunnel(in_pool.building_tunnels().front().tunnel_id());

  // Create a mock AllAccept TunnelBuildReply, and move the tunnel to established tunnels
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      in_tunnel.creator().CreateChaChaReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  std::pair<ChaChaPoolMgr::inbound_pool_t::test_info_t, data::I2NPMessage> test_pair;

  // Create a test message for the established tunnel
  REQUIRE_NOTHROW(test_pair = manager.CreateInboundTestMessage());
  REQUIRE_NOTHROW(manager.HandleI2NPMessage(test_pair.second));
}

TEST_CASE_METHOD(
    ChaChaPoolMgrFixture,
    "ChaCha PoolManager handles incoming test replies for OutboundTunnels",
    "[chacha_pool_mgr]")
{
  std::unique_ptr<data::I2NPMessage> i2np_ptr;

  // Add an inbound tunnel
  REQUIRE_NOTHROW(
      i2np_ptr = std::make_unique<data::I2NPMessage>(
          manager.AddOutboundTunnel(data::Lease2(crypto::RandBytes<32>()), {std::make_shared<data::Info>()})));

  auto& out_pool = manager.outbound_pool();
  const auto out_tunnel_id = out_pool.building_tunnels().front().tunnel_id();
  auto& out_tunnel = out_pool.building_tunnel(out_tunnel_id);

  // Create a mock AllAccept TunnelBuildReply, and move the tunnel to established tunnels
  REQUIRE_NOTHROW(manager.HandleBuildReply(
      out_tunnel.creator().CreateChaChaReplyMessage(i2np_ptr->message_id(), CreateReplyType::AllAccept)));

  // Create a test message for the established tunnel
  std::pair<ChaChaPoolMgr::outbound_pool_t::test_info_t, std::vector<ChaChaPoolMgr::tunnel_message_t>> test_pair;
  REQUIRE_NOTHROW(test_pair = manager.CreateOutboundTestMessage());
  auto& out_est_tunnel = out_pool.tunnel(out_tunnel_id);
  auto& out_creator = out_est_tunnel.creator();

  // Unrealistic, simulate the outbound hops encrypting and processing the tunnel message(s)
  tini2p::tunnels::OutboundEndpoint<ChaChaPoolMgr::layer_t> obep;
  std::vector<data::I2NPMessage> i2np_msgs;
  for (auto& message : test_pair.second)
    {
      out_creator.IterativeEncryption(message);
      auto i2np_blocks = obep.ExtractI2NPFragments(message);
      for (auto& block : i2np_blocks)
        i2np_msgs.emplace_back(std::move(block.second));
    }

  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE_NOTHROW(manager.HandleI2NPMessage(i2np_msgs.front()));
}

TEST_CASE_METHOD(ChaChaPoolMgrFixture, "ChaCha PoolManager rejects invalid incoming I2NP message types", "[chacha_pool_mgr]")
{
  // Inbound I2NP handler requires an I2NP TunnelGateway or Data message, rejecting all others.
  // All other types must be wrapped in a TunnelGateway message to be handled by IBGW TransitTunnels
  crypto::SecBytes bad_buf(crypto::RandBuffer(32));

  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::DatabaseStore, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::DatabaseLookup, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::DatabaseSearchReply, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::DeliveryStatus, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::Garlic, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::TunnelData, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::TunnelBuild, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::TunnelBuildReply, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::VariableTunnelBuild, 0x41, bad_buf)));
  REQUIRE_THROWS(manager.HandleI2NPMessage(data::I2NPMessage(data::I2NPHeader::Type::VariableTunnelBuildReply, 0x41, bad_buf)));
}
