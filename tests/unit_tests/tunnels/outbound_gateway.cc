/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/data.h"

#include "src/tunnels/outbound_gateway.h"

using namespace tini2p::crypto;

using tini2p::tunnels::CreateReplyType;

/*---------------------------\
| AES Outbound Gateway Tests |
\---------------------------*/

using AESOutboundGateway = tini2p::tunnels::OutboundGateway<TunnelAES>;

struct AESOBGWFixture
{
  AESOBGWFixture()
      : loc_info(std::make_shared<AESOutboundGateway::info_t>()),
        reply_lease(RandBytes<32>()),
        hop_infos{std::make_shared<AESOutboundGateway::info_t>(),
                  std::make_shared<AESOutboundGateway::info_t>(),
                  std::make_shared<AESOutboundGateway::info_t>()},
        obgw(loc_info, reply_lease, hop_infos)
  {
  }

  AESOutboundGateway::info_t::shared_ptr loc_info;
  AESOutboundGateway::lease_t reply_lease;
  std::vector<AESOutboundGateway::info_t::shared_ptr> hop_infos;
  AESOutboundGateway obgw;
};

TEST_CASE_METHOD(AESOBGWFixture, "AES OBGW has a local RouterInfo and tunnel hops", "[aes_obgw]")
{
  REQUIRE(obgw.info() == *loc_info);
  REQUIRE(obgw.reply_lease() == reply_lease);
  
  const auto& hops = obgw.hops();
  const auto& hops_len = hop_infos.size();
  REQUIRE(hops.size() == hops_len);

  const auto hop_infos_beg = hop_infos.begin();
  const auto hop_infos_end = hop_infos.end();

  const auto hops_end = hops.end();
  const auto last_hop = --hops.end();

  for (auto hop_it = hops.begin(); hop_it != hops_end; ++hop_it)
    {
      REQUIRE(
          std::find_if(hop_infos_beg, hop_infos_end, [&hop_it](const auto& h) { return *h == hop_it->info(); })
          != hop_infos_end);

      if (hop_it != last_hop)
        {  // check next ID is next hop's ID
          auto next_hop = hop_it;
          REQUIRE(hop_it->next_tunnel_id() == (++next_hop)->tunnel_id());
        }
      else
        {  // check ID is previous hop's next ID
          auto prev_hop = hop_it;
          REQUIRE(hop_it->tunnel_id() == (--prev_hop)->next_tunnel_id());
        }
    }
}

TEST_CASE_METHOD(AESOBGWFixture, "AES OBGW creates BuildRequestRecords for each hop", "[aes_obgw]")
{
  AESOutboundGateway::build_message_t build_message;

  // Create and encrypt the BuildRequestRecords for each hop
  REQUIRE_NOTHROW(build_message = obgw.CreateBuildMessage());

  auto& hops = obgw.hops();
  const auto& records = build_message.records();
  const auto& reqs_len = records.size();
  REQUIRE((reqs_len > hops.size() && reqs_len <= tini2p::under_cast(AESOutboundGateway::MaxHops)));

  // Check each hop record has valid fields, and the hop decrypts record
  const auto last_hop = --hops.end();
  const auto hops_begin = hops.begin();
  const auto hops_end = hops.end();
  for (std::decay_t<decltype(hops)>::iterator hop_it = hops.begin(); hop_it != hops_end; ++hop_it)
    {
      const auto& req = records.at(std::distance(hops_begin, hop_it));

      REQUIRE(req.tunnel_id() == hop_it->tunnel_id());
      REQUIRE(req.local_ident_hash() == loc_info->identity().hash());
      REQUIRE(req.next_tunnel_id() == hop_it->next_tunnel_id());

      if (hop_it != last_hop)
        {
          REQUIRE(req.next_ident_hash() == (++hop_it)->info().identity().hash());
          --hop_it;
        }
      else
        REQUIRE(req.next_ident_hash() == reply_lease.tunnel_gateway());

      // Deserialize and decrypt the record as the tunnel hop
      auto hop_req_buf = req.buffer();
      REQUIRE_NOTHROW(hop_it->DecryptRequestRecord(hop_req_buf));
      AESOutboundGateway::build_request_t hop_req(hop_req_buf);

      REQUIRE(hop_req == req);
    }
}

TEST_CASE_METHOD(AESOBGWFixture, "AES OBGW preprocesses BuildRequestRecords for each hop", "[aes_obgw]")
{
  AESOutboundGateway::build_message_t build_message;

  REQUIRE_NOTHROW(build_message = obgw.CreateBuildMessage());
  const auto build_copy = build_message;

  // Pre-process the message, iteratively decrypting the records
  REQUIRE_NOTHROW(obgw.PreprocessAESBuildMessage(build_message));

  // Post-process the message, and check records are revealed
  // Somewhat unrealistic, normally tunnel hops each perform one round
  //   of encryption, revealing one record at a time during the tunnel build
  auto& records = build_message.records();
  const auto& records_len = records.size();

  const auto& copy_records = build_copy.records();

  auto& hops = obgw.hops();

  REQUIRE((records_len > hops.size() && records_len <= tini2p::under_cast(AESOutboundGateway::MaxHops)));

  const auto hops_begin = hops.begin();
  const auto hops_end = hops.end();

  const auto record_begin = records.begin();
  const auto record_end = records.end();

  for (auto hop_it = hops.begin(); hop_it != hops_end; ++hop_it)
    {
      const auto& hop_trunc_ident = hop_it->key_info().truncated_ident();

      const auto rec_it = std::find_if(record_begin, record_end, [&hop_trunc_ident](const auto& r) {
        return r.truncated_ident() == hop_trunc_ident;
      });

      REQUIRE(rec_it != record_end);

      // Check that the next record is revealed
      REQUIRE(rec_it->buffer() == copy_records.at(std::distance(hops_begin, hop_it)).buffer());

      // Check that the record can be found with corresponding hop's truncated Identity hash
      REQUIRE(build_message.find_record(hop_trunc_ident) == *rec_it);

      for (auto check_it = hops.begin(); check_it != hops_end; ++check_it)
        {
          // Check that all other records are encrypted in the message
          if (check_it != hop_it)
            {
              const auto& check_ident = check_it->key_info().truncated_ident();
              const auto record_check_it = std::find_if(record_begin, record_end, [&check_ident](const auto& r) {
                return r.truncated_ident() == check_ident;
              });

              REQUIRE(!(record_check_it->buffer() == copy_records.at(std::distance(hops_begin, check_it)).buffer()));
            }
        }

      // Simulate the record hop encrypting its reply record and the other records
      for (auto enc_it = records.begin(); enc_it != record_end; ++enc_it)
        hop_it->EncryptRecord(enc_it->buffer());
    }
}

TEST_CASE_METHOD(AESOBGWFixture, "AES OBGW handles BuildResponseRecords for each hop", "[aes_obgw]")
{
  using reply_message_t = AESOutboundGateway::reply_message_t;
  
  std::unique_ptr<reply_message_t> reply_msg_ptr;
  std::uint32_t msg_id(0x01);
  const auto des_mode = reply_message_t::deserialize_mode_t::Partial;
  std::uint8_t accepted(0);

  // Simulate each hop accepting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          obgw.CreateAESReplyMessage(msg_id, CreateReplyType::AllAccept).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = obgw.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 1);

  // Simulate one hop accepting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          obgw.CreateAESReplyMessage(msg_id, CreateReplyType::OneAccept).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = obgw.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);

  // Simulate each hop rejecting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          obgw.CreateAESReplyMessage(msg_id, CreateReplyType::AllReject).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = obgw.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);

  // Simulate one hop rejecting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          obgw.CreateAESReplyMessage(msg_id, CreateReplyType::OneReject).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = obgw.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);
}

TEST_CASE_METHOD(AESOBGWFixture, "AES OBGW creates tunnel messages from I2NP message", "[aes_obgw]")
{
  using FirstDelivery = tini2p::data::FirstDelivery;
  using i2np_block_t = AESOutboundGateway::i2np_block_t;

  const auto& max_fragment_len =
      FirstDelivery::max_fragment_size(FirstDelivery::flags_t::FragTunnel, tini2p::data::Layer::AES);
  const auto& max_follow_len = tini2p::data::FollowDelivery::max_fragment_size(tini2p::data::Layer::AES);

  // Create random I2NP message buffer
  i2np_block_t i2np_block(i2np_block_t::msg_type_t::Data, 0x41, tini2p::data::Data(max_fragment_len - 16).buffer());
  auto& i2np_buf = i2np_block.buffer();

  // Create random InboundGateway to hash
  AESOutboundGateway::to_hash_t to_hash(RandBytes<32>());
  // Create random InboundGateway tunnel ID
  std::optional<AESOutboundGateway::tunnel_id_t> ibgw_id(19);

  // Check that an unfragmented I2NP message returns one tunnel message
  std::vector<AESOutboundGateway::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = obgw.CreateTunnelMessages(i2np_buf, to_hash, ibgw_id));
  REQUIRE(messages.size() == 1);

  // Check that a fragmented I2NP message returns multiple tunnel messages
  REQUIRE_NOTHROW(i2np_buf.resize(max_fragment_len + max_follow_len));
  REQUIRE_NOTHROW(messages = obgw.CreateTunnelMessages(i2np_buf, to_hash, ibgw_id));
  REQUIRE(messages.size() == 2);

  REQUIRE_NOTHROW(i2np_buf.resize(max_follow_len * 2));
  REQUIRE_NOTHROW(messages = obgw.CreateTunnelMessages(i2np_buf, to_hash, ibgw_id));
  REQUIRE(messages.size() == 3);
}

TEST_CASE_METHOD(AESOBGWFixture, "AES OBGW preprocesses tunnel messages", "[aes_obgw]")
{
  using i2np_block_t = AESOutboundGateway::i2np_block_t;

  // Create random I2NP message buffer
  i2np_block_t i2np_block(i2np_block_t::msg_type_t::Data, 0x41, tini2p::data::Data(2048).buffer());
  // Create random InboundGateway to hash
  AESOutboundGateway::to_hash_t to_hash(RandBytes<32>());
  // Create random InboundGateway tunnel ID
  std::optional<AESOutboundGateway::tunnel_id_t> ibgw_id(19);

  // Create tunnel messages
  std::vector<AESOutboundGateway::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = obgw.CreateTunnelMessages(i2np_block.buffer(), to_hash, ibgw_id));
  REQUIRE(messages.size() > 1);

  // Copy messages for comparison
  const auto messages_copy = messages;

  // Preprocess tunnel messages
  for (std::size_t i = 0; i < messages.size(); ++i)
    {
      auto& message = messages.at(i);
      REQUIRE_NOTHROW(obgw.IterativeDecryption(message));
      REQUIRE(!(message == messages_copy.at(i)));
    }

  const auto& msg_offset = tini2p::under_cast(AESOutboundGateway::aes_layer_t::MessageHeaderLen);
  const auto& msg_len = tini2p::under_cast(AESOutboundGateway::aes_layer_t::MessageCipherLen);
  // Simulate tunnel hops processing tunnel messages
  for (std::size_t i = 0; i < messages.size(); ++i)
    {
      auto& message = messages.at(i);
      REQUIRE_NOTHROW(obgw.IterativeEncryption(message));

      REQUIRE(
          SecBytes(message.buffer().data() + msg_offset, msg_len)
          == SecBytes(messages_copy.at(i).buffer().data() + msg_offset, msg_len));
    }
}

/*------------------------------\
| ChaCha Outbound Gateway Tests |
\------------------------------*/

using ChaChaOutboundGateway = tini2p::tunnels::OutboundGateway<TunnelChaCha>;

struct ChaChaOBGWFixture
{
  ChaChaOBGWFixture()
      : loc_info(std::make_shared<ChaChaOutboundGateway::info_t>()),
        reply_lease(RandBytes<32>()),
        hop_infos{std::make_shared<ChaChaOutboundGateway::info_t>(),
                  std::make_shared<ChaChaOutboundGateway::info_t>(),
                  std::make_shared<ChaChaOutboundGateway::info_t>()},
        obgw(loc_info, reply_lease, hop_infos)
  {
  }

  ChaChaOutboundGateway::info_t::shared_ptr loc_info;
  ChaChaOutboundGateway::lease_t reply_lease;
  std::vector<ChaChaOutboundGateway::info_t::shared_ptr> hop_infos;
  ChaChaOutboundGateway obgw;
};

TEST_CASE_METHOD(ChaChaOBGWFixture, "ChaCha OBGW has a local RouterInfo and tunnel hops", "[chacha_obgw]")
{
  REQUIRE(obgw.info() == *loc_info);
  REQUIRE(obgw.reply_lease() == reply_lease);
  
  const auto& hops = obgw.hops();
  const auto& hops_len = hop_infos.size();
  REQUIRE(hops.size() == hops_len);

  const auto hop_infos_beg = hop_infos.begin();
  const auto hop_infos_end = hop_infos.end();

  const auto hops_end = hops.end();
  const auto last_hop = --hops.end();

  for (auto hop_it = hops.begin(); hop_it != hops_end; ++hop_it)
    {
      REQUIRE(
          std::find_if(hop_infos_beg, hop_infos_end, [&hop_it](const auto& h) { return *h == hop_it->info(); })
          != hop_infos_end);

      if (hop_it != last_hop)
        {  // check next ID is next hop's ID
          auto next_hop = hop_it;
          REQUIRE(hop_it->next_tunnel_id() == (++next_hop)->tunnel_id());
        }
      else
        {  // check ID is previous hop's next ID
          auto prev_hop = hop_it;
          REQUIRE(hop_it->tunnel_id() == (--prev_hop)->next_tunnel_id());
        }
    }
}

TEST_CASE_METHOD(ChaChaOBGWFixture, "ChaCha OBGW creates BuildRequestRecords for each hop", "[chacha_obgw]")
{
  ChaChaOutboundGateway::build_message_t build_message;

  // Create and encrypt the BuildRequestRecords for each hop
  REQUIRE_NOTHROW(build_message = obgw.CreateBuildMessage());

  auto& hops = obgw.hops();
  const auto& hops_len = hops.size();
  const auto& records = build_message.records();
  const auto& reqs_len = records.size();
  REQUIRE((reqs_len > hops_len && reqs_len < tini2p::under_cast(ChaChaOutboundGateway::MaxHops)));

  // Check each hop record has valid fields, and the hop decrypts record
  auto hops_begin = hops.begin();
  const auto hops_end = hops.end();
  const auto last_hop = --hops.end();

  for (auto hop_it = hops_begin; hop_it != hops_end; ++hop_it)
    {
      const auto pos = std::distance(hops_begin, hop_it);
      const auto& req = records.at(pos);

      REQUIRE(req.tunnel_id() == hop_it->tunnel_id());
      REQUIRE(req.local_ident_hash() == loc_info->identity().hash());
      REQUIRE(req.next_tunnel_id() == hop_it->next_tunnel_id());

      // Deserialize and decrypt the record as the tunnel hop
      auto hop_req_buf = req.buffer();
      REQUIRE_NOTHROW(hop_it->DecryptRequestRecord(hop_req_buf));
      ChaChaOutboundGateway::build_request_t hop_req(hop_req_buf);

      REQUIRE(hop_req == req);

      if (hop_it != last_hop)
        {
          auto next_hop = hop_it;
          REQUIRE(req.next_ident_hash() == (++next_hop)->info().identity().hash());
        }
      else
        REQUIRE(req.next_ident_hash() == reply_lease.tunnel_gateway());
    }
}

TEST_CASE_METHOD(ChaChaOBGWFixture, "ChaCha OBGW preprocesses BuildRequestRecords for each hop", "[chacha_obgw]")
{
  ChaChaOutboundGateway::build_message_t build_message;

  REQUIRE_NOTHROW(build_message = obgw.CreateBuildMessage());

  // Copy records for comparison, will still be in order (first index, first hop)
  const auto build_copy = build_message;
  const auto& copy_records = build_copy.records();

  // Pre-process the message, iteratively decrypting the records
  REQUIRE_NOTHROW(obgw.PreprocessChaChaBuildMessage(build_message));

  // Post-process the message, and check records are revealed
  // Somewhat unrealistic, normally tunnel hops each perform one round
  //   of encryption, revealing one record at a time during the tunnel build
  auto& records = build_message.records();
  const auto& records_len = records.size();
  auto& hops = obgw.hops();
  const auto& hops_len = hops.size();

  REQUIRE((records_len > hops_len && records_len < tini2p::under_cast(ChaChaOutboundGateway::MaxHops)));

  auto hops_begin = hops.begin();
  const auto hops_end = hops.end();

  const auto record_begin = records.begin();
  const auto record_end = records.end();

  for (auto hop_it = hops_begin; hop_it != hops_end; ++hop_it)
    {
      const auto& hop_ident = hop_it->key_info().truncated_ident();

      const auto record_it = std::find_if(
          record_begin, record_end, [&hop_ident](const auto& r) { return r.truncated_ident() == hop_ident; });

      // Check that the next record is revealed
      REQUIRE(record_it != record_end);

      REQUIRE(record_it->buffer() == copy_records.at(std::distance(hops_begin, hop_it)).buffer());

      // Check that the record can be found with corresponding hop's truncated Identity hash
      REQUIRE(build_message.find_record(hop_ident) == *record_it);

      for (auto check_it = hops.begin(); check_it != hops_end; ++check_it)
        {
          // Check that all other records are encrypted in the message
          if (check_it != hop_it)
            {
              const auto& check_ident = check_it->key_info().truncated_ident();
              const auto record_check_it = std::find_if(record_begin, record_end, [&check_ident](const auto& r) {
                return r.truncated_ident() == check_ident;
              });

              REQUIRE(!(record_check_it->buffer() == copy_records.at(std::distance(hops_begin, check_it)).buffer()));
            }
        }

      // Simulate the tunnel hop encrypting its own and all other records
      for (auto enc_it = records.begin(); enc_it != record_end; ++enc_it)
        hop_it->EncryptRecord(enc_it->buffer(), std::distance(record_begin, enc_it), records_len);
    }
}

TEST_CASE_METHOD(ChaChaOBGWFixture, "ChaCha OBGW handles BuildResponseRecords for each hop", "[aes_obgw]")
{
  using reply_message_t = ChaChaOutboundGateway::reply_message_t;

  std::unique_ptr<reply_message_t> reply_msg_ptr;
  std::uint32_t msg_id(0x01);
  const auto des_mode = reply_message_t::deserialize_mode_t::Partial;
  std::uint8_t accepted(0);

  // Simulate each hop accepting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          obgw.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllAccept).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = obgw.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 1);

  // Simulate one hop accepting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          obgw.CreateChaChaReplyMessage(msg_id, CreateReplyType::OneAccept).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = obgw.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);

  // Simulate each hop rejecting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          obgw.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllReject).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = obgw.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);

  // Simulate one hop rejecting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          obgw.CreateChaChaReplyMessage(msg_id, CreateReplyType::OneReject).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = obgw.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);
}

TEST_CASE_METHOD(ChaChaOBGWFixture, "ChaCha OBGW creates tunnel messages from I2NP message", "[chacha_obgw]")
{
  using FirstDelivery = tini2p::data::FirstDelivery;
  using i2np_block_t = ChaChaOutboundGateway::i2np_block_t;

  const auto& max_fragment_len =
      FirstDelivery::max_fragment_size(FirstDelivery::flags_t::FragTunnel, tini2p::data::Layer::ChaCha);
  const auto& max_follow_len = tini2p::data::FollowDelivery::max_fragment_size(tini2p::data::Layer::ChaCha);

  // Create random I2NP message buffer
  i2np_block_t i2np_block(i2np_block_t::msg_type_t::Data, 0x41, tini2p::data::Data(max_fragment_len - 16).buffer());
  auto& i2np_buf = i2np_block.buffer();

  // Create random InboundGateway to hash
  ChaChaOutboundGateway::to_hash_t to_hash(RandBytes<32>());
  // Create random InboundGateway tunnel ID
  std::optional<ChaChaOutboundGateway::tunnel_id_t> ibgw_id(19);

  // Check that an unfragmented I2NP message returns one tunnel message
  std::vector<ChaChaOutboundGateway::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = obgw.CreateTunnelMessages(i2np_buf, to_hash, ibgw_id));
  REQUIRE(messages.size() == 1);

  // Check that a fragmented I2NP message returns multiple tunnel messages
  REQUIRE_NOTHROW(i2np_buf.resize(max_fragment_len + max_follow_len));
  REQUIRE_NOTHROW(messages = obgw.CreateTunnelMessages(i2np_buf, to_hash, ibgw_id));
  REQUIRE(messages.size() == 2);

  REQUIRE_NOTHROW(i2np_buf.resize(max_follow_len * 2));
  REQUIRE_NOTHROW(messages = obgw.CreateTunnelMessages(i2np_buf, to_hash, ibgw_id));
  REQUIRE(messages.size() == 3);
}

TEST_CASE_METHOD(ChaChaOBGWFixture, "ChaCha OBGW preprocesses tunnel messages", "[chacha_obgw]")
{
  using i2np_block_t = ChaChaOutboundGateway::i2np_block_t;

  // Create random I2NP message buffer
  i2np_block_t i2np_block(i2np_block_t::msg_type_t::Data, 0x41, tini2p::data::Data(2048).buffer());
  // Create random InboundGateway to hash
  ChaChaOutboundGateway::to_hash_t to_hash(RandBytes<32>());
  // Create random InboundGateway tunnel ID
  std::optional<ChaChaOutboundGateway::tunnel_id_t> ibgw_id(19);
  // Create tunnel messages
  std::vector<ChaChaOutboundGateway::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = obgw.CreateTunnelMessages(i2np_block.buffer(), to_hash, ibgw_id));
  REQUIRE(messages.size() > 1);

  // Copy messages for comparison
  const auto messages_copy = messages;

  // Preprocess tunnel messages
  for (std::size_t i = 0; i < messages.size(); ++i)
    {
      auto& message = messages.at(i);
      REQUIRE_NOTHROW(obgw.IterativeDecryption(message));
      REQUIRE(!(message == messages_copy.at(i)));

      // AEAD encrypt the messages for the first hop
      REQUIRE_NOTHROW(obgw.EncryptAEAD(message));
    }

  const auto& msg_offset = tini2p::under_cast(ChaChaOutboundGateway::chacha_layer_t::MessageHeaderLen);
  const auto& msg_len = tini2p::under_cast(ChaChaOutboundGateway::chacha_layer_t::MessageBodyLen);
  auto& first_hop = obgw.hops().front();
  // Simulate tunnel hops processing tunnel messages
  for (std::size_t i = 0; i < messages.size(); ++i)
    {
      auto& message = messages.at(i);

      // AEAD decrypt the message as the first tunnel hop
      REQUIRE_NOTHROW(first_hop.DecryptAEAD(message));

      REQUIRE_NOTHROW(obgw.IterativeEncryption(message));

      REQUIRE(
          SecBytes(message.buffer().data() + msg_offset, msg_len)
          == SecBytes(messages_copy.at(i).buffer().data() + msg_offset, msg_len));
    }
}
