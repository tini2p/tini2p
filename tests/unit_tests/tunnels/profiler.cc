/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/tunnels/profiler.h"

using namespace tini2p::crypto;

using tini2p::tunnels::Profiler;

struct ProfilerFixture
{
  ProfilerFixture() : peers{{RandBytes<32>()}}, profiler() {}

  Profiler::peer_hashes_t peers;
  Profiler profiler;
};

TEST_CASE_METHOD(ProfilerFixture, "Profiler adds peers", "[profiler]")
{
  REQUIRE_NOTHROW(profiler.add_peer(RandBytes<32>()));
  REQUIRE_NOTHROW(profiler.add_peers(peers));

  for (const auto& peer : peers)
    REQUIRE(profiler.find_profile(peer).ident_hash() == peer);
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler returns a list of peer IDs for peers", "[profiler]")
{
  REQUIRE_NOTHROW(profiler.add_peers(peers));
  REQUIRE(!profiler.get_peer_ids(peers).empty());
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler updates peer speed", "[profiler]")
{
  const Profiler::peer_profile_t::bytes_t bytes(1729);
  const Profiler::peer_profile_t::secs_t secs(29);
  const auto& exp_speed = (bytes / secs) + (bytes % secs);

  REQUIRE_NOTHROW(profiler.add_peers(peers));
  REQUIRE_NOTHROW(profiler.update_peer_speed(peers, bytes, secs));

  for (const auto& peer : peers)
    REQUIRE(profiler.find_profile(peer).speed() == exp_speed);
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler adds peer build success", "[profiler]")
{
  REQUIRE_NOTHROW(profiler.add_peers(peers));
  REQUIRE_NOTHROW(profiler.add_peer_build_success(peers));

  for (const auto& peer : peers)
    REQUIRE(profiler.find_profile(peer).build_success() == 1);
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler adds peer build failure", "[profiler]")
{
  REQUIRE_NOTHROW(profiler.add_peers(peers));
  REQUIRE_NOTHROW(profiler.add_peer_build_fail(peers));

  for (const auto& peer : peers)
    REQUIRE(profiler.find_profile(peer).build_fail() == 1);
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler adds peer build drop", "[profiler]")
{
  REQUIRE_NOTHROW(profiler.add_peers(peers));
  REQUIRE_NOTHROW(profiler.add_peer_build_drop(peers));

  for (const auto& peer : peers)
    REQUIRE(profiler.find_profile(peer).build_drop() == 1);
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler adds peer test success", "[profiler]")
{
  REQUIRE_NOTHROW(profiler.add_peers(peers));
  REQUIRE_NOTHROW(profiler.add_peer_test_success(peers));

  for (const auto& peer : peers)
    REQUIRE(profiler.find_profile(peer).test_success() == 1);
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler adds peer test failure", "[profiler]")
{
  REQUIRE_NOTHROW(profiler.add_peers(peers));
  REQUIRE_NOTHROW(profiler.add_peer_test_fail(peers));

  for (const auto& peer : peers)
    REQUIRE(profiler.find_profile(peer).test_fail() == 1);
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler adds peer test drop", "[profiler]")
{
  REQUIRE_NOTHROW(profiler.add_peers(peers));
  REQUIRE_NOTHROW(profiler.add_peer_test_drop(peers));

  for (const auto& peer : peers)
    REQUIRE(profiler.find_profile(peer).test_drop() == 1);
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler updates peer groups", "[profiler]")
{
  Profiler::peer_hashes_t fast_peers, high_cap_peers, standard_peers;

  // Add maximum number of fast peers to the fast, high capacity and standard peers
  for (auto i = 0; i < Profiler::MaxFastPeers; ++i)
    {
      fast_peers.emplace_back(RandBytes<32>());
      high_cap_peers.emplace_back(RandBytes<32>());
      standard_peers.emplace_back(RandBytes<32>());
      REQUIRE_NOTHROW(
          profiler.add_peer(fast_peers.back()).add_peer(high_cap_peers.back()).add_peer(standard_peers.back()));
    }

  // Add maximum number of high capacity peers to the high capacity peers
  for (auto i = tini2p::under_cast(Profiler::MaxFastPeers); i < Profiler::MaxHighCapPeers; ++i)
    {
      high_cap_peers.emplace_back(RandBytes<32>());
      REQUIRE_NOTHROW(profiler.add_peer(high_cap_peers.back()));
    }

  // Add successful builds for fast and high cap peers
  for (auto i = 0; i < 10; ++i)
    REQUIRE_NOTHROW(profiler.add_peer_build_success(fast_peers).add_peer_build_success(high_cap_peers));

  // Update the speed for fast peers
  REQUIRE_NOTHROW(profiler.update_peer_speed(fast_peers, 100, 1));

  // Update speed for high capacity peers, but slower than fast peers
  REQUIRE_NOTHROW(profiler.update_peer_speed(high_cap_peers, 50, 1));

  // Update speed for standard peers (make them really slow)
  REQUIRE_NOTHROW(profiler.update_peer_speed(standard_peers, 5, 1));

  // Update the peer groups
  REQUIRE_NOTHROW(profiler.update_peer_groups());

  Profiler::peer_hash_t fast_peer, high_cap_peer;

  const auto fast_begin = fast_peers.begin();
  const auto fast_end = fast_peers.end();

  const auto high_begin = high_cap_peers.begin();
  const auto high_end = high_cap_peers.end();

  const auto standard_begin = standard_peers.begin();
  const auto standard_end = standard_peers.end();

  // Get a fast peer
  REQUIRE_NOTHROW(fast_peer = profiler.get_fast_peer());

  // Check that a fast peer is returned from the peers with fastest speed
  REQUIRE(std::find(fast_begin, fast_end, fast_peer) != fast_end);

  // Check that the fast peer group only contains the fastest peers
  REQUIRE(std::find(high_begin, high_end, fast_peer) == high_end);

  // Check that standard peers aren't added to the fast group
  REQUIRE(std::find(standard_begin, standard_end, fast_peer) == standard_end);

  // Get a high capacity peer
  REQUIRE_NOTHROW(high_cap_peer = profiler.get_high_cap_peer());

  // Check that a high capacity peer could come from the fastest peer group or the high capacity peer group
  REQUIRE(
      (std::find(fast_begin, fast_end, high_cap_peer) != fast_end
       || std::find(high_begin, high_end, high_cap_peer) != high_end));

  // Check that standard peers aren't added to the high capacity group
  REQUIRE(std::find(standard_begin, standard_end, high_cap_peer) == standard_end);
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects adding a peer that already exists", "[profiler]")
{
  REQUIRE_NOTHROW(profiler.add_peers(peers));

  // Rejects explicitly adding a duplicate peer
  REQUIRE_THROWS(profiler.add_peer(peers.front()));

  // Does not throw when adding a list of peers, but ignores duplicates
  REQUIRE_NOTHROW(profiler.add_peers(peers));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects adding null peers", "[profiler]")
{
  REQUIRE_THROWS(profiler.add_peer({}));
  REQUIRE_THROWS(profiler.add_peers({}));
  REQUIRE_THROWS(profiler.add_peers({Profiler::peer_hash_t{}}));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects getting peer IDs for non-existent peers", "[profiler]")
{
  REQUIRE_THROWS(profiler.get_peer_ids(peers));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects finding a non-existent peer", "[profiler]")
{
  REQUIRE_THROWS(profiler.find_profile(peers.front()));
  REQUIRE_THROWS(profiler.find_profile(0));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects updating the speed of a non-existent peer", "[profiler]")
{
  REQUIRE_THROWS(profiler.update_peer_speed(peers, 1, 1));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects updating the speed with invalid sampling interval", "[profiler]")
{
  REQUIRE_NOTHROW(profiler.add_peers(peers));
  REQUIRE_THROWS(profiler.update_peer_speed(peers, 1, 0));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects adding a build success for a non-existent peer", "[profiler]")
{
  REQUIRE_THROWS(profiler.add_peer_build_success(peers));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects adding a build fail for a non-existent peer", "[profiler]")
{
  REQUIRE_THROWS(profiler.add_peer_build_fail(peers));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects adding a build drop for a non-existent peer", "[profiler]")
{
  REQUIRE_THROWS(profiler.add_peer_build_drop(peers));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects adding a test success for a non-existent peer", "[profiler]")
{
  REQUIRE_THROWS(profiler.add_peer_test_success(peers));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects adding a test fail for a non-existent peer", "[profiler]")
{
  REQUIRE_THROWS(profiler.add_peer_test_fail(peers));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects adding a test drop for a non-existent peer", "[profiler]")
{
  REQUIRE_THROWS(profiler.add_peer_test_drop(peers));
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects getting a fast peer when none exist", "[profiler]")
{
  REQUIRE_THROWS(profiler.get_fast_peer());
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects getting a high capacity peer when none exist", "[profiler]")
{
  REQUIRE_THROWS(profiler.get_high_cap_peer());
}

TEST_CASE_METHOD(ProfilerFixture, "Profiler rejects getting a standard peer when none exist", "[profiler]")
{
  REQUIRE_THROWS(profiler.get_standard_peer());
}
