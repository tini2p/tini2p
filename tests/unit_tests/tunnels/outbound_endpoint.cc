/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/data.h"

#include "src/tunnels/hop.h"
#include "src/tunnels/outbound_gateway.h"
#include "src/tunnels/outbound_endpoint.h"

using namespace tini2p::crypto;
namespace data = tini2p::data;

using tini2p::data::TunnelGateway;
using tini2p::data::FirstDelivery;

/*----------------------------------\
| AES Tunnel OutboundEndpoint Tests |
\----------------------------------*/

using AESOutboundGateway = tini2p::tunnels::OutboundGateway<TunnelAES>;
using AESOutboundEndpoint = tini2p::tunnels::OutboundEndpoint<TunnelAES>;

struct AESOutboundEndpointFixture
{
  AESOutboundEndpointFixture()
      : info_ptr(std::make_shared<AESOutboundEndpoint::info_t>()),
        id(0x19),
        next_id(0x23),
        iv(RandBytes<AESOutboundEndpoint::aes_layer_t::aes_t::IVLen>()),
        i2np_msg_len(16),
        message(
            0x42,
            iv,
            FirstDelivery(
                FirstDelivery::layer_t::AES,
                FirstDelivery::mode_t::Unfragmented,
                i2np_msg_len,
                RandBytes<32>()),
            data::I2NPMessage().buffer()),
                
        obep(id, next_id, info_ptr)
  {
  }

  AESOutboundEndpoint::info_t::shared_ptr info_ptr;
  AESOutboundEndpoint::tunnel_id_t id, next_id;
  AESOutboundEndpoint::aes_layer_t::aes_t::iv_t iv;
  std::uint16_t i2np_msg_len;
  AESOutboundEndpoint::tunnel_message_t message;
  AESOutboundEndpoint obep;
};

TEST_CASE_METHOD(AESOutboundEndpointFixture, "AES OBEP encrypts a message", "[aes_obep]")
{
  AESOutboundEndpoint::aes_layer_t::aes_t::iv_t ret_iv;
  const auto msg_copy = message;

  REQUIRE_NOTHROW(ret_iv = obep.Encrypt(message));

  REQUIRE(!(ret_iv == iv));

  const auto message_eq = message == msg_copy;
  REQUIRE(!message_eq);

  AESOutboundEndpoint::tunnel_id_t read_next_id;
  REQUIRE_NOTHROW(tini2p::read_bytes(message.buffer().data(), read_next_id, tini2p::Endian::Big));
  REQUIRE(read_next_id == next_id);
}

TEST_CASE_METHOD(AESOutboundEndpointFixture, "AES OBEP encrypts and decrypts back to original message", "[aes_obep]")
{
  AESOutboundEndpoint::aes_layer_t::aes_t::iv_t ret_iv;
  auto msg_copy = message;

  // Simulate OBGW preprocessing
  REQUIRE_NOTHROW(ret_iv = obep.Decrypt(message));

  REQUIRE(!(ret_iv == iv));

  auto message_eq = message == msg_copy;
  REQUIRE(!(message_eq));

  // Encrypt the message as the OBEP
  REQUIRE_NOTHROW(ret_iv = obep.Encrypt(message));
  REQUIRE(ret_iv == iv);

  // Write next ID to message copy for comparison
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_copy.buffer().data(), next_id, tini2p::Endian::Big));

  REQUIRE((message == msg_copy));
}

TEST_CASE_METHOD(
    AESOutboundEndpointFixture,
    "AES OBEP extracts I2NP message fragments from tunnel message",
    "[aes_obep]")
{
  const auto fragments = message.fragments();
  data::I2NPMessage i2np_msg(fragments.front());

  AESOutboundEndpoint::extracted_i2np_t i2np_msgs;
  REQUIRE_NOTHROW(i2np_msgs = obep.ExtractI2NPFragments(message));

  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE(i2np_msgs.front().second == i2np_msg);

  // Create a random I2NP buffer that fragments into multiple TunnelMessages
  i2np_msg_len = 1730;
  REQUIRE_NOTHROW(i2np_msg.message_buffer(tini2p::data::Data(i2np_msg_len).buffer()));
  REQUIRE_NOTHROW(i2np_msg.serialize());

  // Create a mock OutboundGateway for creating TunnelMessages
  AESOutboundGateway obgw(
      std::make_shared<AESOutboundEndpoint::info_t>(), AESOutboundEndpoint::lease_t(RandBytes<32>()), {info_ptr});

  // Create random InboundGateway to hash
  AESOutboundGateway::to_hash_t to_hash(RandBytes<32>());

  // Create random InboundGateway tunnel ID
  std::optional<AESOutboundGateway::tunnel_id_t> ibgw_id(0x42);

  std::vector<AESOutboundEndpoint::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = obgw.CreateTunnelMessages(i2np_msg.buffer(), to_hash, ibgw_id));

  REQUIRE_NOTHROW(i2np_msgs.clear());
  for (auto& message : messages)
    {
      REQUIRE(i2np_msgs.empty());
      REQUIRE_NOTHROW(i2np_msgs = obep.ExtractI2NPFragments(message));
    }
  REQUIRE(i2np_msgs.size() == 1);

  TunnelGateway gw(i2np_msgs.front().second.message_buffer());
  REQUIRE(gw.i2np_header() == i2np_msg.header());
  REQUIRE(gw.i2np_body() == i2np_msg.message_buffer());
}

TEST_CASE_METHOD(AESOutboundEndpointFixture, "AES OBEP rejects identical tunnel IDs", "[aes_obep]")
{
  REQUIRE_THROWS(AESOutboundEndpoint(id, id, info_ptr));
}

/*-------------------------------------\
| ChaCha Tunnel OutboundEndpoint Tests |
\-------------------------------------*/

using ChaChaHop = tini2p::tunnels::Hop<TunnelChaCha>;
using ChaChaOutboundEndpoint = tini2p::tunnels::OutboundEndpoint<TunnelChaCha>;
using ChaChaOutboundGateway = tini2p::tunnels::OutboundGateway<TunnelChaCha>;

struct ChaChaOutboundEndpointFixture
{
  ChaChaOutboundEndpointFixture()
      : hop_info_ptr(std::make_shared<ChaChaOutboundEndpoint::info_t>()),
        obep_info_ptr(std::make_shared<ChaChaOutboundEndpoint::info_t>()),
        hop_receive_key(RandBytes<ChaChaOutboundEndpoint::chacha_layer_t::aead_t::KeyLen>()),
        hop_id(0x17),
        obep_id(0x19),
        obep_next_id(0x23),
        nonces(ChaChaOutboundEndpoint::chacha_layer_t::create_nonces()),
        i2np_msg_len(16),
        message(
            0x42,
            nonces,
            FirstDelivery(
                FirstDelivery::layer_t::ChaCha,
                FirstDelivery::mode_t::Unfragmented,
                i2np_msg_len,
                RandBytes<32>()),
            data::I2NPMessage().buffer()),
        peer_key(ChaChaHop::blake_t::create_key()),
        hop(hop_id, obep_id, hop_info_ptr, hop_receive_key, peer_key),
        obep(obep_id, obep_next_id, obep_info_ptr, hop.key_info().send_key())
  {
  }

  ChaChaOutboundEndpoint::info_t::shared_ptr hop_info_ptr, obep_info_ptr;
  ChaChaOutboundEndpoint::chacha_layer_t::aead_t::key_t hop_receive_key;
  ChaChaOutboundEndpoint::tunnel_id_t hop_id, obep_id, obep_next_id;
  ChaChaOutboundEndpoint::chacha_layer_t::nonces_t nonces;
  std::uint16_t i2np_msg_len;
  ChaChaOutboundEndpoint::tunnel_message_t message;
  ChaChaHop::peer_key_t peer_key;
  ChaChaHop hop;
  ChaChaOutboundEndpoint obep;
};

TEST_CASE_METHOD(ChaChaOutboundEndpointFixture, "ChaCha OBEP encrypts a message", "[chacha_obep]")
{
  ChaChaOutboundEndpoint::chacha_layer_t::nonces_t ret_nonces;
  const auto msg_copy = message;

  // ChaCha20 encrypt the tunnel message
  REQUIRE_NOTHROW(ret_nonces = obep.Encrypt(message));

  REQUIRE(!(ret_nonces == nonces));

  const auto message_eq = message == msg_copy;
  REQUIRE(!message_eq);

  ChaChaOutboundEndpoint::tunnel_id_t read_next_id;
  REQUIRE_NOTHROW(tini2p::read_bytes(message.buffer().data(), read_next_id, tini2p::Endian::Big));
  REQUIRE(read_next_id == obep_next_id);
}

TEST_CASE_METHOD(
    ChaChaOutboundEndpointFixture,
    "ChaCha OBEP encrypts and decrypts back to original message",
    "[chacha_obep]")
{
  ChaChaOutboundEndpoint::chacha_layer_t::nonces_t ret_nonces;
  auto msg_copy = message;

  // ChaCha20 encrypt the tunnel message
  // Simulates OBGW preprocessing
  REQUIRE_NOTHROW(ret_nonces = obep.Decrypt(message));

  REQUIRE(!(ret_nonces == nonces));

  auto message_eq = message == msg_copy;
  REQUIRE(!(message_eq));

  // ChaCha20 encrypt the tunnel message
  REQUIRE_NOTHROW(ret_nonces = obep.Encrypt(message));
  REQUIRE(ret_nonces == nonces);

  // Write next ID to message copy for comparison
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_copy.buffer().data(), obep_next_id, tini2p::Endian::Big));

  message_eq = message == msg_copy;
  REQUIRE(message_eq);
}

TEST_CASE_METHOD(
    ChaChaOutboundEndpointFixture,
    "ChaCha OBEP AEAD encrypts and decrypts back to original message",
    "[chacha_obep]")
{
  ChaChaOutboundEndpoint::chacha_layer_t::nonces_t ret_nonces;
  auto msg_copy = message;

  // Decrypt the tunnel message as the OBEP
  // Simulates OBGW preprocessing
  REQUIRE_NOTHROW(ret_nonces = obep.Decrypt(message));

  REQUIRE(!(ret_nonces == nonces));

  auto message_eq = message == msg_copy;
  REQUIRE(!(message_eq));

  // AEAD encrypt the tunnel message for the OBEP
  REQUIRE_NOTHROW(hop.EncryptAEAD(message));

  // AEAD decrypt the tunnel message from the previous hop 
  REQUIRE_NOTHROW(obep.DecryptAEAD(message));

  // Encrypt the tunnel message as the OBEP
  REQUIRE_NOTHROW(ret_nonces = obep.Encrypt(message));
  REQUIRE(ret_nonces == nonces);

  // Write next ID to message copy for comparison
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_copy.buffer().data(), obep_next_id, tini2p::Endian::Big));

  const auto& chacha_msg_len = tini2p::under_cast(ChaChaOutboundEndpoint::chacha_layer_t::MessageHeaderLen)
                               + tini2p::under_cast(ChaChaOutboundEndpoint::chacha_layer_t::MessageBodyLen);

  // Resize to avoid comparing the MAC added during AEAD encryption
  REQUIRE_NOTHROW(message.buffer().resize(chacha_msg_len));
  REQUIRE_NOTHROW(msg_copy.buffer().resize(chacha_msg_len));

  REQUIRE((message == msg_copy));
}

TEST_CASE_METHOD(
    ChaChaOutboundEndpointFixture,
    "ChaCha OBEP extracts I2NP message fragments from tunnel message",
    "[chacha_obep]")
{
  const auto fragments = message.fragments();
  data::I2NPMessage i2np_msg(fragments.front());

  ChaChaOutboundEndpoint::extracted_i2np_t i2np_msgs;
  REQUIRE_NOTHROW(i2np_msgs = obep.ExtractI2NPFragments(message));

  REQUIRE(i2np_msgs.size() == 1);
  REQUIRE(i2np_msgs.front().second == i2np_msg);

  // Create a random I2NP buffer that fragments into multiple TunnelMessages
  i2np_msg_len = 1730;
  REQUIRE_NOTHROW(i2np_msg.message_buffer(tini2p::data::Data(i2np_msg_len).buffer()));
  REQUIRE_NOTHROW(i2np_msg.serialize());

  // Create a mock OutboundGateway for creating TunnelMessages
  ChaChaOutboundGateway obgw(
      std::make_shared<ChaChaOutboundEndpoint::info_t>(),
      ChaChaOutboundEndpoint::lease_t(RandBytes<32>()),
      {obep_info_ptr});

  // Create random InboundGateway to hash
  ChaChaOutboundGateway::to_hash_t to_hash(RandBytes<32>());
  // Create random InboundGateway tunnel ID
  std::optional<ChaChaOutboundGateway::tunnel_id_t> ibgw_id(0x42);

  // Create tunnel messages with Tunnel delivery type
  std::vector<ChaChaOutboundEndpoint::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = obgw.CreateTunnelMessages(i2np_msg.buffer(), to_hash, ibgw_id));

  REQUIRE_NOTHROW(i2np_msgs.clear());
  for (auto& message : messages)
    {
      REQUIRE(i2np_msgs.empty());
      REQUIRE_NOTHROW(i2np_msgs = obep.ExtractI2NPFragments(message));
    }
  REQUIRE(i2np_msgs.size() == 1);

  TunnelGateway gw(i2np_msgs.front().second.message_buffer());
  REQUIRE(gw.i2np_header() == i2np_msg.header());
  REQUIRE(gw.i2np_body() == i2np_msg.message_buffer());
}

TEST_CASE_METHOD(ChaChaOutboundEndpointFixture, "ChaCha OBEP rejects identical tunnel IDs", "[chacha_obep]")
{
  REQUIRE_THROWS(ChaChaOutboundEndpoint(obep_id, obep_id, obep_info_ptr, hop_receive_key));
}

TEST_CASE_METHOD(ChaChaOutboundEndpointFixture, "ChaCha OBEP rejects identical nonces", "[chacha_obep]")
{
  tini2p::BytesWriter<ChaChaOutboundEndpoint::tunnel_message_t::buffer_t> writer(message.buffer());

  // Write obfs nonce over the tunnel nonce for identical nonces
  REQUIRE_NOTHROW(writer.skip_bytes(ChaChaOutboundEndpoint::chacha_layer_t::TunnelIDLen));
  REQUIRE_NOTHROW(
      writer.write_data(static_cast<ChaChaOutboundEndpoint::chacha_layer_t::nonce_t::buffer_t>(nonces.obfs_nonce)));

  REQUIRE_THROWS(obep.Decrypt(message));
}
