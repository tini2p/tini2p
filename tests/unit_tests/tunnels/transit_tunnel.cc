/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/i2np_message.h"
#include "src/tunnels/transit_tunnel.h"

namespace crypto = tini2p::crypto;
namespace data = tini2p::data;

using tini2p::data::TunnelGateway;
using tini2p::data::FirstDelivery;

/*------------------------\
| AES TransitTunnel Tests |
\------------------------*/

using AESTransitTunnel = tini2p::tunnels::TransitTunnel<crypto::TunnelAES>;

struct AESTransitFixture
{
  AESTransitFixture()
      : hop_info(),
        creator_info(),
        build_params{0x42,
                     creator_info.identity().hash(),
                     0x43,
                     crypto::RandBytes<data::Identity::HashLen>(),
                     hop_info.identity().hash()}
  {
  }

  /// @brief Create a mock VariableTunnelBuildMessage
  /// @param flags Flags for the type of BuildRequestRecord
  void CreateBuildMessage(const AESTransitTunnel::build_request_t::flags_t& flags)
  {
    using key_info_t = AESTransitTunnel::layer_t::key_info_t;

    const auto& hop_identity = hop_info.identity();
    const auto& hop_keys = hop_identity.crypto();
    const auto& hop_ident = hop_identity.hash();

    // Create new tunnel creator layer encryption (new ephemeral keys)
    REQUIRE_NOTHROW(
        creator_layer_ptr = std::make_unique<AESTransitTunnel::layer_t>(key_info_t(hop_keys.pubkey, hop_ident)));

    const auto& creator_key = creator_layer_ptr->key_info().creator_pubkey();

    /// Create hop layer encryption from the creator pubkey and hop keys
    /// Somewhat unrealistic, normally the creator key is read from the BuildRequestRecord
    REQUIRE_NOTHROW(
        hop_layer_ptr = std::make_unique<AESTransitTunnel::layer_t>(key_info_t(creator_key, hop_keys, hop_ident)));

    // Create a BuildRequestRecord with the given flags and creator pubkey
    REQUIRE_NOTHROW(record_ptr = std::make_unique<AESTransitTunnel::build_request_t>(build_params, creator_key, flags));

    // Encrypt the record as the tunnel creator
    REQUIRE_NOTHROW(creator_layer_ptr->EncryptRequestRecord(record_ptr->buffer()));
  }

  data::Info hop_info, creator_info;
  std::unique_ptr<AESTransitTunnel::layer_t> creator_layer_ptr, hop_layer_ptr;
  AESTransitTunnel::build_request_t::params_t build_params;
  std::unique_ptr<AESTransitTunnel::build_request_t> record_ptr;
  std::unique_ptr<AESTransitTunnel> tunnel_ptr;
};

TEST_CASE_METHOD(AESTransitFixture, "AES TransitTunnel is created for valid tunnel processors", "[aes_transit]")
{
  // Create and encrypt a build message with a Hop BuildRequestRecord
  CreateBuildMessage(AESTransitTunnel::build_request_t::flags_t::Hop);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<AESTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  REQUIRE(std::holds_alternative<AESTransitTunnel::hop_t>(tunnel_ptr->processor()));

  // Create and encrypt a build message with an InboundGateway BuildRequestRecord
  CreateBuildMessage(AESTransitTunnel::build_request_t::flags_t::IBGW);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<AESTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  // Check the TransitTunnel processor is an InboundGateway
  REQUIRE(std::holds_alternative<AESTransitTunnel::ibgw_t>(tunnel_ptr->processor()));

  // Create and encrypt a build message with an OutboundEndpoint BuildRequestRecord
  CreateBuildMessage(AESTransitTunnel::build_request_t::flags_t::OBEP);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<AESTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  // Check the TransitTunnel processor is an OutboundEndpoint
  REQUIRE(std::holds_alternative<AESTransitTunnel::obep_t>(tunnel_ptr->processor()));
}

TEST_CASE_METHOD(AESTransitFixture, "AES TransitTunnel handles Hop TunnelMessages", "[aes_transit]")
{
  // Create and encrypt a build message with a Hop BuildRequestRecord
  CreateBuildMessage(AESTransitTunnel::build_request_t::flags_t::Hop);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<AESTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  const auto& hop_id = std::visit([](const auto& p) { return p.tunnel_id(); }, tunnel_ptr->processor());
  AESTransitTunnel::tunnel_message_t tun_msg(
      hop_id, AESTransitTunnel::layer_t::create_iv(), FirstDelivery(), AESTransitTunnel::i2np_block_t().buffer());

  REQUIRE_NOTHROW(tunnel_ptr->HandleTunnelMessage(tun_msg));
}

TEST_CASE_METHOD(AESTransitFixture, "AES TransitTunnel handles OutboundEndpoint TunnelMessages", "[aes_transit]")
{
  // Create and encrypt a build message with a OutboundEndpoint BuildRequestRecord
  CreateBuildMessage(AESTransitTunnel::build_request_t::flags_t::OBEP);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<AESTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  auto& obep = std::get<AESTransitTunnel::obep_t>(tunnel_ptr->processor());
  data::I2NPMessage i2np_msg(data::I2NPHeader::Type::Data, {}, data::Data(0x42).buffer());

  // Create a tunnel message with Tunnel delivery type
  AESTransitTunnel::tunnel_message_t tun_msg(
      obep.tunnel_id(),
      AESTransitTunnel::layer_t::create_iv(),
      FirstDelivery(
          FirstDelivery::layer_t::AES,
          FirstDelivery::mode_t::Unfragmented,
          i2np_msg.size(),
          FirstDelivery::tunnel_id_t(0x44),
          crypto::RandBytes<32>()),
      i2np_msg.buffer());

  const auto tun_msg_copy = tun_msg;

  // Mock the OutboundGateway iteratively decrypting the tunnel message, and outbound Hop processing
  REQUIRE_NOTHROW(obep.Decrypt(tun_msg));

  // Handle the tunnel message as the OutboundEndpoint
  std::vector<std::pair<std::optional<AESTransitTunnel::ident_hash_t>, data::I2NPMessage>> i2np_msgs;
  REQUIRE_NOTHROW(i2np_msgs = tunnel_ptr->HandleTunnelMessage(tun_msg));

  REQUIRE(i2np_msgs.size() == 1);

  TunnelGateway gw_msg(i2np_msgs.front().second.message_buffer());

  REQUIRE(data::I2NPMessage(gw_msg.extract_header(), gw_msg.extract_body()) == i2np_msg);
}

TEST_CASE_METHOD(AESTransitFixture, "AES TransitTunnel handles I2NP messages as InboundGateway", "[aes_transit]")
{
  // Create and encrypt a build message with a InboundGateway BuildRequestRecord
  CreateBuildMessage(AESTransitTunnel::build_request_t::flags_t::IBGW);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<AESTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  crypto::SecBytes i2np_buf(crypto::RandBuffer(0x71));

  // Handle the tunnel message as the OutboundEndpoint
  std::vector<AESTransitTunnel::tunnel_message_t> tun_msgs;
  REQUIRE_NOTHROW(tun_msgs = tunnel_ptr->HandleI2NPMessage(i2np_buf));

  REQUIRE(tun_msgs.size() == 1);
}

TEST_CASE_METHOD(AESTransitFixture, "AES TransitTunnel rejects null layer encryption", "[aes_transit]")
{
  REQUIRE_THROWS(AESTransitTunnel(nullptr, *record_ptr));
}

TEST_CASE_METHOD(
    AESTransitFixture,
    "AES TransitTunnel rejects handling I2NP messages as Hop and OutboundEndpoint",
    "[aes_transit]")
{
  // Create and encrypt a build message with a Hop BuildRequestRecord
  CreateBuildMessage(AESTransitTunnel::build_request_t::flags_t::Hop);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<AESTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  AESTransitTunnel::i2np_buffer_t i2np_buf(crypto::RandBuffer(1));

  REQUIRE_THROWS(tunnel_ptr->HandleI2NPMessage(i2np_buf));

  // Create and encrypt a build message with an OutboundEndpoint BuildRequestRecord
  CreateBuildMessage(AESTransitTunnel::build_request_t::flags_t::OBEP);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<AESTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  REQUIRE_THROWS(tunnel_ptr->HandleI2NPMessage(i2np_buf));
}

TEST_CASE_METHOD(
    AESTransitFixture,
    "AES TransitTunnel rejects handling tunnel messages as InboundGateway",
    "[aes_transit]")
{
  // Create and encrypt a build message with a InboundGateway BuildRequestRecord
  CreateBuildMessage(AESTransitTunnel::build_request_t::flags_t::IBGW);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<AESTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  const data::I2NPMessage i2np_msg;
  AESTransitTunnel::tunnel_message_t tun_msg(
      0x42,
      AESTransitTunnel::layer_t::create_iv(),
      FirstDelivery(
          FirstDelivery::layer_t::AES, FirstDelivery::mode_t::Unfragmented, i2np_msg.size(), crypto::RandBytes<32>()),
      i2np_msg.buffer());

  REQUIRE_THROWS(tunnel_ptr->HandleTunnelMessage(tun_msg));
}

TEST_CASE_METHOD(AESTransitFixture, "AES Hop TransitTunnel rejects duplicate IVs", "[aes_transit]")
{
  // Create and encrypt a build message with a Hop BuildRequestRecord
  CreateBuildMessage(AESTransitTunnel::build_request_t::flags_t::Hop);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<AESTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  data::I2NPMessage i2np_msg;
  AESTransitTunnel::tunnel_message_t tun_msg(
      0x42,
      AESTransitTunnel::layer_t::create_iv(),
      FirstDelivery(
          FirstDelivery::layer_t::AES, FirstDelivery::mode_t::Unfragmented, i2np_msg.size(), crypto::RandBytes<32>()),
      i2np_msg.buffer());

  // Rejects handling the same IV twice
  REQUIRE_NOTHROW(tunnel_ptr->HandleTunnelMessage(tun_msg));
  REQUIRE_THROWS(tunnel_ptr->HandleTunnelMessage(tun_msg));
}

TEST_CASE_METHOD(AESTransitFixture, "AES OBEP TransitTunnel rejects duplicate IVs", "[aes_transit]")
{
  // Create and encrypt a build message with a OutboundEndpoint BuildRequestRecord
  CreateBuildMessage(AESTransitTunnel::build_request_t::flags_t::OBEP);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<AESTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  data::I2NPMessage i2np_msg;
  AESTransitTunnel::tunnel_message_t tun_msg(
      0x42,
      AESTransitTunnel::layer_t::create_iv(),
      FirstDelivery(
          FirstDelivery::layer_t::AES, FirstDelivery::mode_t::Unfragmented, i2np_msg.size(), crypto::RandBytes<32>()),
      i2np_msg.buffer());

  // Unrealistic, simulate the OBGW iteratively decrypting the message
  REQUIRE_NOTHROW(std::get<AESTransitTunnel::obep_t>(tunnel_ptr->processor()).Decrypt(tun_msg));

  // Rejects handling the same IV twice
  REQUIRE_NOTHROW(tunnel_ptr->HandleTunnelMessage(tun_msg));
  REQUIRE_THROWS(tunnel_ptr->HandleTunnelMessage(tun_msg));
}

/*---------------------------\
| ChaCha TransitTunnel Tests |
\---------------------------*/

using ChaChaTransitTunnel = tini2p::tunnels::TransitTunnel<crypto::TunnelChaCha>;

struct ChaChaTransitFixture
{
  ChaChaTransitFixture()
      : hop_info(),
        creator_info(),
        build_params{0x42,
                     creator_info.identity().hash(),
                     0x43,
                     crypto::RandBytes<data::Identity::HashLen>(),
                     hop_info.identity().hash()},
        peer_key(crypto::Blake2b::create_key()),
        prev_hop(
            0x41,
            build_params.tunnel_id,
            std::make_shared<ChaChaTransitTunnel::info_t>(),
            ChaChaTransitTunnel::layer_t::aead_t::create_key(),
            peer_key)
  {
  }

  /// @brief Create a mock VariableTunnelBuildMessage
  /// @param flags Flags for the type of BuildRequestRecord
  void CreateBuildMessage(const ChaChaTransitTunnel::build_request_t::flags_t& flags)
  {
    using key_info_t = ChaChaTransitTunnel::layer_t::key_info_t;
    using flags_t = ChaChaTransitTunnel::build_request_t::flags_t;

    const auto& hop_identity = hop_info.identity();
    const auto& hop_keys = hop_identity.crypto();
    const auto& hop_ident = hop_identity.hash();

    // Create new tunnel creator layer encryption (new ephemeral keys)
    REQUIRE_NOTHROW(
        creator_layer_ptr = std::make_unique<ChaChaTransitTunnel::layer_t>(key_info_t(hop_keys.pubkey, hop_ident)));

    const auto& creator_key = creator_layer_ptr->key_info().creator_pubkey();

    /// Create hop layer encryption from the creator pubkey and hop keys
    /// Somewhat unrealistic, normally the creator key is read from the BuildRequestRecord
    REQUIRE_NOTHROW(
        hop_layer_ptr = std::make_unique<ChaChaTransitTunnel::layer_t>(key_info_t(creator_key, hop_keys, hop_ident)));

    // Create a BuildRequestRecord with the given flags and creator pubkey
    if (flags == flags_t::ChaChaHop || flags == flags_t::ChaChaOBEP)
      {
        REQUIRE_NOTHROW(
            record_ptr = std::make_unique<ChaChaTransitTunnel::build_request_t>(
                build_params, creator_key, prev_hop.key_info().send_key(), flags));
      }
    else
      {
        REQUIRE_NOTHROW(
            record_ptr = std::make_unique<ChaChaTransitTunnel::build_request_t>(build_params, creator_key, flags));
      }

    // Encrypt the record as the tunnel creator
    REQUIRE_NOTHROW(creator_layer_ptr->EncryptRequestRecord(record_ptr->buffer()));
  }

  ChaChaTransitTunnel::info_t hop_info, creator_info;
  ChaChaTransitTunnel::build_request_t::params_t build_params;
  crypto::Blake2b::key_t peer_key;
  tini2p::tunnels::Hop<crypto::TunnelChaCha> prev_hop;
  std::unique_ptr<ChaChaTransitTunnel::layer_t> creator_layer_ptr, hop_layer_ptr;
  std::unique_ptr<ChaChaTransitTunnel::build_request_t> record_ptr;
  std::unique_ptr<ChaChaTransitTunnel> tunnel_ptr;
};

TEST_CASE_METHOD(
    ChaChaTransitFixture,
    "ChaCha TransitTunnel is created for valid tunnel processors",
    "[chacha_transit]")
{
  // Create and encrypt a build message with a Hop BuildRequestRecord
  CreateBuildMessage(ChaChaTransitTunnel::build_request_t::flags_t::Hop);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<ChaChaTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  REQUIRE(std::holds_alternative<ChaChaTransitTunnel::hop_t>(tunnel_ptr->processor()));

  // Create and encrypt a build message with an InboundGateway BuildRequestRecord
  CreateBuildMessage(ChaChaTransitTunnel::build_request_t::flags_t::IBGW);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<ChaChaTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  // Check the TransitTunnel processor is an InboundGateway
  REQUIRE(std::holds_alternative<ChaChaTransitTunnel::ibgw_t>(tunnel_ptr->processor()));

  // Create and encrypt a build message with an OutboundEndpoint BuildRequestRecord
  CreateBuildMessage(ChaChaTransitTunnel::build_request_t::flags_t::OBEP);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<ChaChaTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  // Check the TransitTunnel processor is an OutboundEndpoint
  REQUIRE(std::holds_alternative<ChaChaTransitTunnel::obep_t>(tunnel_ptr->processor()));
}

TEST_CASE_METHOD(ChaChaTransitFixture, "ChaCha TransitTunnel handles Hop TunnelMessages", "[chacha_transit]")
{
  // Create and encrypt a build message with a Hop BuildRequestRecord
  CreateBuildMessage(ChaChaTransitTunnel::build_request_t::flags_t::ChaChaHop);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<ChaChaTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  const auto& hop_id = std::visit([](const auto& p) { return p.tunnel_id(); }, tunnel_ptr->processor());
  ChaChaTransitTunnel::tunnel_message_t tun_msg(
      hop_id,
      ChaChaTransitTunnel::layer_t::create_nonces(),
      FirstDelivery(FirstDelivery::layer_t::ChaCha),
      ChaChaTransitTunnel::i2np_block_t().buffer());

  // AEAD encrypt the message as the mock previous hop
  REQUIRE_NOTHROW(prev_hop.EncryptAEAD(tun_msg));

  // Handle the tunnel message as the Hop
  REQUIRE_NOTHROW(tunnel_ptr->HandleTunnelMessage(tun_msg));
}

TEST_CASE_METHOD(
    ChaChaTransitFixture,
    "ChaCha TransitTunnel handles OutboundEndpoint TunnelMessages",
    "[chacha_transit]")
{
  // Create and encrypt a build message with a OutboundEndpoint BuildRequestRecord
  CreateBuildMessage(ChaChaTransitTunnel::build_request_t::flags_t::ChaChaOBEP);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<ChaChaTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  auto& obep = std::get<ChaChaTransitTunnel::obep_t>(tunnel_ptr->processor());
  data::I2NPMessage i2np_msg(data::I2NPHeader::Type::Data, {}, data::Data(0x32).buffer());

  // Create an outbound tunnel message with Tunnel delivery type
  ChaChaTransitTunnel::tunnel_message_t tun_msg(
      obep.tunnel_id(),
      ChaChaTransitTunnel::layer_t::create_nonces(),
      FirstDelivery(
          FirstDelivery::layer_t::ChaCha,
          FirstDelivery::mode_t::Unfragmented,
          i2np_msg.size(),
          FirstDelivery::tunnel_id_t(0x44),
          crypto::RandBytes<32>()),
      i2np_msg.buffer());

  const auto tun_msg_copy = tun_msg;

  // Mock the OutboundGateway iteratively decrypting the tunnel message, and outbound Hop processing
  REQUIRE_NOTHROW(obep.Decrypt(tun_msg));

  // AEAD encrypt the message as the mock previous hop
  REQUIRE_NOTHROW(prev_hop.EncryptAEAD(tun_msg));

  // Handle the tunnel message as the OutboundEndpoint
  ChaChaTransitTunnel::obep_t::extracted_i2np_t i2np_msgs;
  REQUIRE_NOTHROW(i2np_msgs = tunnel_ptr->HandleTunnelMessage(tun_msg));

  REQUIRE(i2np_msgs.size() == 1);

  TunnelGateway gw_msg(i2np_msgs.front().second.extract_body());
  REQUIRE(data::I2NPMessage(gw_msg.extract_header(), gw_msg.extract_body()) == i2np_msg);
}

TEST_CASE_METHOD(
    ChaChaTransitFixture,
    "ChaCha TransitTunnel handles I2NP messages as InboundGateway",
    "[chacha_transit]")
{
  // Create and encrypt a build message with a InboundGateway BuildRequestRecord
  CreateBuildMessage(ChaChaTransitTunnel::build_request_t::flags_t::IBGW);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<ChaChaTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  ChaChaTransitTunnel::i2np_buffer_t i2np_buf(crypto::RandBuffer(0x71));

  // Handle the tunnel message as the OutboundEndpoint
  std::vector<ChaChaTransitTunnel::tunnel_message_t> tun_msgs;
  REQUIRE_NOTHROW(tun_msgs = tunnel_ptr->HandleI2NPMessage(i2np_buf));

  REQUIRE(tun_msgs.size() == 1);
}

TEST_CASE_METHOD(ChaChaTransitFixture, "ChaCha TransitTunnel rejects null layer encryption", "[aes_transit]")
{
  REQUIRE_THROWS(ChaChaTransitTunnel(nullptr, *record_ptr));
}

TEST_CASE_METHOD(
    ChaChaTransitFixture,
    "ChaCha TransitTunnel rejects handling I2NP messages as Hop and OutboundEndpoint",
    "[chacha_transit]")
{
  // Create and encrypt a build message with a Hop BuildRequestRecord
  CreateBuildMessage(ChaChaTransitTunnel::build_request_t::flags_t::Hop);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<ChaChaTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  ChaChaTransitTunnel::i2np_buffer_t i2np_buf(crypto::RandBuffer(1));

  REQUIRE_THROWS(tunnel_ptr->HandleI2NPMessage(i2np_buf));

  // Create and encrypt a build message with an OutboundEndpoint BuildRequestRecord
  CreateBuildMessage(ChaChaTransitTunnel::build_request_t::flags_t::OBEP);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<ChaChaTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  REQUIRE_THROWS(tunnel_ptr->HandleI2NPMessage(i2np_buf));
}

TEST_CASE_METHOD(
    ChaChaTransitFixture,
    "ChaCha TransitTunnel rejects handling tunnel messages as InboundGateway",
    "[chacha_transit]")
{
  // Create and encrypt a build message with a InboundGateway BuildRequestRecord
  CreateBuildMessage(ChaChaTransitTunnel::build_request_t::flags_t::IBGW);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<ChaChaTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  ChaChaTransitTunnel::tunnel_message_t tun_msg(
      0x42,
      ChaChaTransitTunnel::layer_t::create_nonces(),
      FirstDelivery(FirstDelivery::layer_t::ChaCha),
      ChaChaTransitTunnel::i2np_block_t().buffer());

  REQUIRE_THROWS(tunnel_ptr->HandleTunnelMessage(tun_msg));
}

TEST_CASE_METHOD(ChaChaTransitFixture, "ChaCha Hop TransitTunnel rejects duplicate nonces", "[chacha_transit]")
{
  // Create and encrypt a build message with a Hop BuildRequestRecord
  CreateBuildMessage(ChaChaTransitTunnel::build_request_t::flags_t::ChaChaHop);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<ChaChaTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  ChaChaTransitTunnel::tunnel_message_t tun_msg(
      0x42,
      ChaChaTransitTunnel::layer_t::create_nonces(),
      FirstDelivery(FirstDelivery::layer_t::ChaCha, FirstDelivery::mode_t::Unfragmented, 12, crypto::RandBytes<32>()),
      ChaChaTransitTunnel::i2np_block_t().buffer());

  // Rejects handling the same IV twice
  REQUIRE_NOTHROW(prev_hop.EncryptAEAD(tun_msg));
  REQUIRE_NOTHROW(tunnel_ptr->HandleTunnelMessage(tun_msg));
  REQUIRE_THROWS(tunnel_ptr->HandleTunnelMessage(tun_msg));
}

TEST_CASE_METHOD(ChaChaTransitFixture, "ChaCha OBEP TransitTunnel rejects duplicate nonces", "[chacha_transit]")
{
  // Create and encrypt a build message with a OutboundEndpoint BuildRequestRecord
  CreateBuildMessage(ChaChaTransitTunnel::build_request_t::flags_t::ChaChaOBEP);

  // Create a new TransitTunnel from the message
  REQUIRE_NOTHROW(tunnel_ptr = std::make_unique<ChaChaTransitTunnel>(std::move(hop_layer_ptr), *record_ptr));

  ChaChaTransitTunnel::tunnel_message_t tun_msg(
      0x42,
      ChaChaTransitTunnel::layer_t::create_nonces(),
      FirstDelivery(FirstDelivery::layer_t::ChaCha, FirstDelivery::mode_t::Unfragmented, 16, crypto::RandBytes<32>()),
      data::I2NPMessage().buffer());

  // Unrealistic, simulate the OBGW iteratively decrypting the message
  REQUIRE_NOTHROW(std::get<ChaChaTransitTunnel::obep_t>(tunnel_ptr->processor()).Decrypt(tun_msg));

  // AEAD encrypt the message as the preceding hop
  REQUIRE_NOTHROW(prev_hop.EncryptAEAD(tun_msg));

  // Rejects handling the same IV twice
  REQUIRE_NOTHROW(tunnel_ptr->HandleTunnelMessage(tun_msg));
  REQUIRE_THROWS(tunnel_ptr->HandleTunnelMessage(tun_msg));
}
