/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/tunnels/hop.h"
#include "src/tunnels/inbound_gateway.h"

using namespace tini2p::crypto;

/*--------------------------\
| AES Inbound Gateway Tests |
\--------------------------*/

using AESInboundGateway = tini2p::tunnels::InboundGateway<TunnelAES>;

struct AESIBGWFixture
{
  AESIBGWFixture()
      : ibgw_id(0x23),
        ibgw_next_id(0x29),
        ibgw_info(std::make_shared<AESInboundGateway::info_t>()),
        iv(AESInboundGateway::aes_layer_t::create_iv()),
        message(
            0x42,
            iv,
            AESInboundGateway::tunnel_message_t::first_delivery_t{},
            AESInboundGateway::i2np_block_t().buffer()),
        ibgw(ibgw_id, ibgw_next_id, ibgw_info)
  {
  }

  AESInboundGateway::tunnel_id_t ibgw_id, ibgw_next_id;
  AESInboundGateway::info_t::shared_ptr ibgw_info;
  AESInboundGateway::aes_layer_t::aes_t::iv_t iv;
  AESInboundGateway::tunnel_message_t message;
  AESInboundGateway ibgw;
};

TEST_CASE_METHOD(AESIBGWFixture, "AES IBGW creates tunnel messages from I2NP message", "[aes_ibgw]")
{
  using FirstDelivery = tini2p::data::FirstDelivery;

  const auto& max_fragment_len =
      FirstDelivery::max_fragment_size(FirstDelivery::flags_t::FragTunnel, tini2p::data::Layer::AES);
  const auto& max_follow_len = tini2p::data::FollowDelivery::max_fragment_size(tini2p::data::Layer::AES);

  // Create random I2NP message buffer
  AESInboundGateway::i2np_block_t::buffer_t i2np_buf(RandBuffer(max_fragment_len));
  // Create random OutboundGateway to hash
  AESInboundGateway::to_hash_t to_hash(RandBytes<32>());

  // Check that an unfragmented I2NP message returns one tunnel message
  std::vector<AESInboundGateway::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = ibgw.CreateTunnelMessages(i2np_buf, to_hash, std::nullopt));
  REQUIRE(messages.size() == 1);

  // Check that a fragmented I2NP message returns multiple tunnel messages
  REQUIRE_NOTHROW(i2np_buf.resize(max_fragment_len + max_follow_len));
  REQUIRE_NOTHROW(messages = ibgw.CreateTunnelMessages(i2np_buf, to_hash, std::nullopt));
  REQUIRE(messages.size() == 2);

  REQUIRE_NOTHROW(i2np_buf.resize(max_follow_len * 2));
  REQUIRE_NOTHROW(messages = ibgw.CreateTunnelMessages(i2np_buf, to_hash, std::nullopt));
  REQUIRE(messages.size() == 3);
}

TEST_CASE_METHOD(
    AESIBGWFixture,
    "AES IBGW rejects duplicate nonces when creating tunnel messages from I2NP message",
    "[aes_ibgw]")
{
  AESInboundGateway::i2np_block_t i2np_block;
  const auto& i2np_buf = i2np_block.buffer();

  // Create random OutboundGateway to hash
  AESInboundGateway::to_hash_t to_hash(RandBytes<32>());

  std::vector<AESInboundGateway::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = ibgw.CreateTunnelMessages(i2np_buf, to_hash, std::nullopt));
}

TEST_CASE_METHOD(AESIBGWFixture, "AES IBGW encrypts a message", "[aes_ibgw]")
{
  AESInboundGateway::aes_layer_t::aes_t::iv_t ret_iv;
  const auto msg_copy = message;

  REQUIRE_NOTHROW(ret_iv = ibgw.Encrypt(message));

  REQUIRE(!(ret_iv == iv));

  REQUIRE(!(message == msg_copy));

  AESInboundGateway::tunnel_id_t read_next_id;
  REQUIRE_NOTHROW(tini2p::read_bytes(message.buffer().data(), read_next_id, tini2p::Endian::Big));
  REQUIRE(read_next_id == ibgw_next_id);
}

TEST_CASE_METHOD(AESIBGWFixture, "AES IBGW encrypts and decrypts back to original message", "[aes_ibgw]")
{
  AESInboundGateway::aes_layer_t::aes_t::iv_t ret_iv;
  auto msg_copy = message;

  REQUIRE_NOTHROW(ret_iv = ibgw.Encrypt(message));

  REQUIRE(!(ret_iv == iv));

  REQUIRE(!(message == msg_copy));

  AESInboundGateway::tunnel_id_t read_next_id;
  REQUIRE_NOTHROW(tini2p::read_bytes(message.buffer().data(), read_next_id, tini2p::Endian::Big));
  REQUIRE(read_next_id == ibgw_next_id);

  // Write next ID to message copy for comparison
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_copy.buffer().data(), read_next_id, tini2p::Endian::Big));

  // Simulate the IBEP decrypting back to original
  REQUIRE_NOTHROW(ret_iv = ibgw.Decrypt(message));
  REQUIRE(ret_iv == iv);

  REQUIRE((message == msg_copy));
}

TEST_CASE_METHOD(AESIBGWFixture, "AES IBGW rejects identical tunnel IDs", "[aes_ibgw]")
{
  REQUIRE_THROWS(AESInboundGateway(ibgw_id, ibgw_id, ibgw_info));
  REQUIRE_THROWS(AESInboundGateway(ibgw_next_id, ibgw_next_id, ibgw_info));
}

/*-----------------------------\
| ChaCha Inbound Gateway Tests |
\-----------------------------*/

using ChaChaHop = tini2p::tunnels::Hop<TunnelChaCha>;
using ChaChaInboundGateway = tini2p::tunnels::InboundGateway<TunnelChaCha>;

struct ChaChaIBGWFixture
{
  ChaChaIBGWFixture()
      : ibgw_id(0x23),
        ibgw_next_id(0x29),
        hop_next_id(0x31),
        ibgw_info(std::make_shared<ChaChaInboundGateway::info_t>()),
        hop_info(std::make_shared<ChaChaInboundGateway::info_t>()),
        nonces(ChaChaInboundGateway::chacha_layer_t::create_nonces()),
        message(
            0x42,
            nonces,
            ChaChaInboundGateway::tunnel_message_t::first_delivery_t(
                ChaChaInboundGateway::tunnel_message_t::delivery_layer_t::ChaCha),
            ChaChaInboundGateway::i2np_block_t().buffer()),
        peer_key(ChaChaInboundGateway::blake_t::create_key()),
        ibgw(ibgw_id, ibgw_next_id, ibgw_info),
        hop(ibgw_next_id, hop_next_id, hop_info, ibgw.key_info().send_key(), peer_key)
  {
  }

  ChaChaInboundGateway::tunnel_id_t ibgw_id, ibgw_next_id, hop_next_id;
  ChaChaInboundGateway::info_t::shared_ptr ibgw_info, hop_info;
  ChaChaInboundGateway::chacha_layer_t::nonces_t nonces;
  ChaChaInboundGateway::tunnel_message_t message;
  ChaChaInboundGateway::peer_key_t peer_key;
  ChaChaInboundGateway ibgw;
  ChaChaHop hop;
};

TEST_CASE_METHOD(ChaChaIBGWFixture, "ChaCha IBGW creates tunnel messages from I2NP message", "[chacha_ibgw]")
{
  using FirstDelivery = tini2p::data::FirstDelivery;

  const auto& max_fragment_len =
      FirstDelivery::max_fragment_size(FirstDelivery::flags_t::FragTunnel, tini2p::data::Layer::ChaCha);
  const auto& max_follow_len = tini2p::data::FollowDelivery::max_fragment_size(tini2p::data::Layer::ChaCha);

  // Create random I2NP message buffer
  ChaChaInboundGateway::i2np_block_t::buffer_t i2np_buf(RandBuffer(max_fragment_len));
  // Create random OutboundGateway to hash
  ChaChaInboundGateway::to_hash_t to_hash(RandBytes<32>());

  // Check that an unfragmented I2NP message returns one tunnel message
  std::vector<ChaChaInboundGateway::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = ibgw.CreateTunnelMessages(i2np_buf, to_hash, std::nullopt));
  REQUIRE(messages.size() == 1);

  // Check that a fragmented I2NP message returns multiple tunnel messages
  REQUIRE_NOTHROW(i2np_buf.resize(max_fragment_len + max_follow_len));
  REQUIRE_NOTHROW(messages = ibgw.CreateTunnelMessages(i2np_buf, to_hash, std::nullopt));
  REQUIRE(messages.size() == 2);

  REQUIRE_NOTHROW(i2np_buf.resize(max_follow_len * 2));
  REQUIRE_NOTHROW(messages = ibgw.CreateTunnelMessages(i2np_buf, to_hash, std::nullopt));
  REQUIRE(messages.size() == 3);
}

TEST_CASE_METHOD(ChaChaIBGWFixture, "ChaCha IBGW encrypts a message", "[aes_ibgw]")
{
  ChaChaInboundGateway::chacha_layer_t::nonces_t ret_nonces;
  const auto msg_copy = message;

  REQUIRE_NOTHROW(ret_nonces = ibgw.Encrypt(message));

  REQUIRE(!(ret_nonces == nonces));

  REQUIRE(!(message == msg_copy));

  ChaChaInboundGateway::tunnel_id_t read_next_id;
  REQUIRE_NOTHROW(tini2p::read_bytes(message.buffer().data(), read_next_id, tini2p::Endian::Big));
  REQUIRE(read_next_id == ibgw_next_id);
}

TEST_CASE_METHOD(ChaChaIBGWFixture, "ChaCha IBGW encrypts and decrypts back to original message", "[chacha_ibgw]")
{
  ChaChaInboundGateway::chacha_layer_t::nonces_t ret_nonces;
  auto msg_copy = message;

  // Encrypt the message as the IBGW
  REQUIRE_NOTHROW(ret_nonces = ibgw.Encrypt(message));

  REQUIRE(!(ret_nonces == nonces));

  REQUIRE(!(message == msg_copy));

  ChaChaInboundGateway::tunnel_id_t read_next_id;
  REQUIRE_NOTHROW(tini2p::read_bytes(message.buffer().data(), read_next_id, tini2p::Endian::Big));
  REQUIRE(read_next_id == ibgw_next_id);

  // Write next ID to message copy for comparison
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_copy.buffer().data(), read_next_id, tini2p::Endian::Big));

  // AEAD encrypt for the next hop
  REQUIRE_NOTHROW(ibgw.EncryptAEAD(message));

  // AEAD decrypt as the next hop
  REQUIRE_NOTHROW(hop.DecryptAEAD(message));

  // Simulate the IBEP decrypting back to original
  REQUIRE_NOTHROW(ret_nonces = ibgw.Decrypt(message));
  REQUIRE(ret_nonces == nonces);

  const auto msg_offset = tini2p::under_cast(ChaChaInboundGateway::layer_t::MessageHeaderLen);
  const auto msg_len = tini2p::under_cast(ChaChaInboundGateway::layer_t::MessageBodyLen);

  REQUIRE(
      SecBytes(message.buffer().data() + msg_offset, msg_len)
      == SecBytes(msg_copy.buffer().data() + msg_offset, msg_len));
}

TEST_CASE_METHOD(ChaChaIBGWFixture, "ChaCha IBGW rejects identical tunnel IDs", "[chacha_ibgw]")
{
  REQUIRE_THROWS(ChaChaInboundGateway(ibgw_id, ibgw_id, ibgw_info));
  REQUIRE_THROWS(ChaChaInboundGateway(ibgw_next_id, ibgw_next_id, ibgw_info));
}

TEST_CASE_METHOD(ChaChaIBGWFixture, "ChaCha IBGW rejects identical nonces", "[chacha_ibgw]")
{
  tini2p::BytesWriter<ChaChaInboundGateway::tunnel_message_t::buffer_t> writer(message.buffer());

  // Write obfs nonce over the tunnel nonce for identical nonces
  REQUIRE_NOTHROW(writer.skip_bytes(ChaChaInboundGateway::chacha_layer_t::TunnelIDLen));
  REQUIRE_NOTHROW(writer.write_data(static_cast<ChaChaInboundGateway::chacha_layer_t::nonce_t::buffer_t>(nonces.obfs_nonce)));

  REQUIRE_THROWS(ibgw.Encrypt(message));
}
