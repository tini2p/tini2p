/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/data.h"

#include "src/tunnels/inbound_gateway.h"
#include "src/tunnels/inbound_endpoint.h"

using namespace tini2p::crypto;

using tini2p::tunnels::CreateReplyType;

/*---------------------------\
| AES Inbound Endpoint Tests |
\---------------------------*/

using AESInboundEndpoint = tini2p::tunnels::InboundEndpoint<TunnelAES>;

struct AESIBEPFixture
{
  AESIBEPFixture()
      : loc_info(std::make_shared<AESInboundEndpoint::info_t>()),
        hop_infos{std::make_shared<AESInboundEndpoint::info_t>(),
                  std::make_shared<AESInboundEndpoint::info_t>(),
                  std::make_shared<AESInboundEndpoint::info_t>()},
        ibep(loc_info, {}, hop_infos)
  {
    REQUIRE_NOTHROW(reply_lease = ibep.reply_lease());
  }

  AESInboundEndpoint::info_t::shared_ptr loc_info;
  std::vector<AESInboundEndpoint::info_t::shared_ptr> hop_infos;
  AESInboundEndpoint ibep;
  AESInboundEndpoint::lease_t reply_lease;
};

TEST_CASE_METHOD(AESIBEPFixture, "AES IBEP has a local RouterInfo and tunnel hops", "[aes_ibep]")
{
  REQUIRE(ibep.info() == *loc_info);
  
  const auto& hops = ibep.hops();
  const auto& hops_len = hop_infos.size();
  REQUIRE(hops.size() == hops_len);
  REQUIRE(ibep.reply_lease().tunnel_gateway() == hops.front().info().identity().hash());

  auto hops_begin = hops.begin();
  const auto hops_end = hops.end();
  const auto last_hop = --hops.end();

  const auto hop_infos_beg = hop_infos.begin();
  const auto hop_infos_end = hop_infos.end();

  for (auto hop_it = hops_begin; hop_it != hops_end; ++hop_it)
    {
      REQUIRE(
          std::find_if(hop_infos_beg, hop_infos_end, [&hop_it](const auto& h) { return *h == hop_it->info(); })
          != hop_infos_end);

      if (hop_it != last_hop)
        {  // check next ID is next hop's ID
          auto next_hop = hop_it;
          REQUIRE(hop_it->next_tunnel_id() == (++next_hop)->tunnel_id());
        }
      else
        {  // check ID is previous hop's next ID
          auto prev_hop = hop_it;
          REQUIRE(hop_it->tunnel_id() == (--prev_hop)->next_tunnel_id());
        }
    }
}

TEST_CASE_METHOD(AESIBEPFixture, "AES IBEP creates BuildRequestRecords for each hop", "[aes_ibep]")
{
  AESInboundEndpoint::build_message_t build_message;

  // Create and encrypt the BuildRequestRecords for each hop
  REQUIRE_NOTHROW(build_message = ibep.CreateBuildMessage());

  auto& hops = ibep.hops();
  const auto& records = build_message.records();
  const auto& reqs_len = records.size();
  REQUIRE((reqs_len > hops.size() && reqs_len <= tini2p::under_cast(AESInboundEndpoint::MaxHops)));

  auto hops_begin = hops.begin();
  auto hops_end = hops.end();
  const auto last_hop = --hops.end();

  // Check each hop record has valid fields, and the hop decrypts record
  for (auto hop_it = hops.begin(); hop_it != hops_end; ++hop_it)
    {
      const auto& req = records.at(std::distance(hops_begin, hop_it));

      REQUIRE(req.tunnel_id() == hop_it->tunnel_id());
      REQUIRE(req.local_ident_hash() == loc_info->identity().hash());
      REQUIRE(req.next_tunnel_id() == hop_it->next_tunnel_id());

      if (hop_it != last_hop)
        {
          auto next_hop = hop_it;
          REQUIRE(req.next_ident_hash() == (++next_hop)->info().identity().hash());
        }
      else  // last hop is the InboundEndpoint (us)
        REQUIRE(req.next_ident_hash() == loc_info->identity().hash());

      // Decrypt and deserialize the record as the tunnel hop
      auto hop_req_buf = req.buffer();
      REQUIRE_NOTHROW(hop_it->DecryptRequestRecord(hop_req_buf));
      AESInboundEndpoint::build_request_t hop_req(hop_req_buf);

      REQUIRE(hop_req == req);
    }
}

TEST_CASE_METHOD(AESIBEPFixture, "AES IBEP preprocesses BuildRequestRecords for each hop", "[aes_ibep]")
{
  AESInboundEndpoint::build_message_t build_message;

  REQUIRE_NOTHROW(build_message = ibep.CreateBuildMessage());
  const auto build_copy = build_message;

  REQUIRE_NOTHROW(ibep.PreprocessAESBuildMessage(build_message));

  // Post-process the message, and check records are revealed
  // Somewhat unrealistic, normally tunnel hops each perform one round
  //   of encryption, revealing one record at a time during the tunnel build
  auto& records = build_message.records();
  const auto& copy_records = build_copy.records();
  const auto& records_len = records.size();
  auto& hops = ibep.hops();

  REQUIRE((records_len > hops.size() && records_len <= tini2p::under_cast(AESInboundEndpoint::MaxHops)));

  auto hops_begin = hops.begin();
  auto hops_end = hops.end();

  const auto record_begin = records.begin();
  const auto record_end = records.end();

  for (auto hop_it = hops_begin; hop_it != hops_end; ++hop_it)
    {
      const auto& hop_trunc_ident = hop_it->key_info().truncated_ident();
      const auto find_record = [&hop_trunc_ident](const auto& r) { return r.truncated_ident() == hop_trunc_ident; };

      const auto rec_it = std::find_if(record_begin, record_end, find_record);

      REQUIRE(rec_it != record_end);

      // Check that the next record is revealed
      REQUIRE(rec_it->buffer() == copy_records.at(std::distance(hops_begin, hop_it)).buffer());

      // Check that the record can be found with corresponding hop's truncated Identity hash
      REQUIRE(build_message.find_record(hop_trunc_ident) == *rec_it);

      for (auto check_it = hops.begin(); check_it != hops_end; ++check_it)
        {
          // Check that all other records are encrypted in the message
          if (check_it != hop_it)
            {
              const auto& check_ident = check_it->key_info().truncated_ident();
              const auto record_check_it = std::find_if(record_begin, record_end, [&check_ident](const auto& r) {
                return r.truncated_ident() == check_ident;
              });

              REQUIRE(!(record_check_it->buffer() == copy_records.at(std::distance(hops_begin, check_it)).buffer()));
            }
        }

      // Simulate the record hop encrypting its reply record and the other records
      for (auto enc_it = record_begin; enc_it != record_end; ++enc_it)
        hop_it->EncryptRecord(enc_it->buffer());
    }
}

TEST_CASE_METHOD(AESIBEPFixture, "AES IBEP handles BuildResponseRecords for each hop", "[aes_ibep]")
{
  using reply_message_t = AESInboundEndpoint::reply_message_t;

  std::unique_ptr<reply_message_t> reply_msg_ptr;
  AESInboundEndpoint::message_id_t msg_id(0x01);
  std::uint8_t accepted(0);
  const auto des_mode = reply_message_t::deserialize_mode_t::Partial;

  // Simulate each hop accepting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          ibep.CreateAESReplyMessage(msg_id, CreateReplyType::AllAccept).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = ibep.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 1);

  // Simulate one hop accepting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          ibep.CreateAESReplyMessage(msg_id, CreateReplyType::OneAccept).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = ibep.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);

  // Simulate each hop rejecting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          ibep.CreateAESReplyMessage(msg_id, CreateReplyType::AllReject).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = ibep.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);

  // Simulate one hop rejecting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          ibep.CreateAESReplyMessage(msg_id, CreateReplyType::OneReject).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = ibep.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);
}

TEST_CASE_METHOD(AESIBEPFixture, "AES IBEP iteratively decrypts received tunnel messages", "[aes_ibep]")
{
  using i2np_block_t = AESInboundEndpoint::i2np_block_t;

  // Create random I2NP message buffer
  i2np_block_t i2np_block(i2np_block_t::msg_type_t::Data, 0x41, tini2p::data::Data(2048).buffer());
  // Create random OutboundGateway to hash
  AESInboundEndpoint::to_hash_t to_hash(RandBytes<32>());

  // Create a mock InboundGateway
  tini2p::tunnels::InboundGateway<TunnelAES> ibgw(0x29, 0x31, hop_infos.front());

  // Create tunnel messages
  std::vector<AESInboundEndpoint::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = ibgw.CreateTunnelMessages(i2np_block.buffer(), to_hash, std::nullopt));
  REQUIRE(messages.size() > 1);

  // Copy messages for comparison
  const auto messages_copy = messages;

  // Simulate tunnel hops encrypting tunnel messages
  for (std::size_t i = 0; i < messages.size(); ++i)
    {
      auto& message = messages.at(i);
      REQUIRE_NOTHROW(ibep.IterativeEncryption(message));
      REQUIRE(!(message == messages_copy.at(i)));
    }

  const auto& msg_offset = tini2p::under_cast(AESInboundEndpoint::aes_layer_t::MessageHeaderLen);
  const auto& msg_len = tini2p::under_cast(AESInboundEndpoint::aes_layer_t::MessageCipherLen);
  // Decrypt the tunnel messages as the InboundEndpoint
  for (std::size_t i = 0; i < messages.size(); ++i)
    {
      auto& message = messages.at(i);
      REQUIRE_NOTHROW(ibep.IterativeDecryption(message));

      REQUIRE(
          SecBytes(message.buffer().data() + msg_offset, msg_len)
          == SecBytes(messages_copy.at(i).buffer().data() + msg_offset, msg_len));
    }
}

/*------------------------------\
| ChaCha Inbound Endpoint Tests |
\------------------------------*/

using ChaChaInboundEndpoint = tini2p::tunnels::InboundEndpoint<TunnelChaCha>;

struct ChaChaIBEPFixture
{
  ChaChaIBEPFixture()
      : loc_info(std::make_shared<ChaChaInboundEndpoint::info_t>()),
        hop_infos{std::make_shared<ChaChaInboundEndpoint::info_t>(),
                  std::make_shared<ChaChaInboundEndpoint::info_t>(),
                  std::make_shared<ChaChaInboundEndpoint::info_t>()},
        ibep(loc_info, {}, hop_infos)
  {
    REQUIRE_NOTHROW(reply_lease = ibep.reply_lease());
  }

  ChaChaInboundEndpoint::info_t::shared_ptr loc_info;
  ChaChaInboundEndpoint::lease_t reply_lease;
  std::vector<ChaChaInboundEndpoint::info_t::shared_ptr> hop_infos;
  ChaChaInboundEndpoint ibep;
};

TEST_CASE_METHOD(ChaChaIBEPFixture, "ChaCha IBEP has a local RouterInfo and tunnel hops", "[chacha_ibep]")
{
  REQUIRE(ibep.info() == *loc_info);
  
  const auto& hops = ibep.hops();
  const auto& hops_len = hop_infos.size();
  REQUIRE(hops.size() == hops_len);

  REQUIRE(ibep.reply_lease().tunnel_gateway() == hops.front().info().identity().hash());

  const auto hops_end = hops.end();
  const auto last_hop = --hops.end();

  const auto hop_infos_beg = hop_infos.begin();
  const auto hop_infos_end = hop_infos.end();

  for (auto hop_it = hops.begin(); hop_it != hops_end; ++hop_it)
    {
      REQUIRE(
          std::find_if(hop_infos_beg, hop_infos_end, [&hop_it](const auto& h) { return *h == hop_it->info(); })
          != hop_infos_end);

      if (hop_it != last_hop)
        {  // check next ID is next hop's ID
          auto next_hop = hop_it;
          REQUIRE(hop_it->next_tunnel_id() == (++next_hop)->tunnel_id());
        }
      else
        {  // check ID is previous hop's next ID
          auto prev_hop = hop_it;
          REQUIRE(hop_it->tunnel_id() == (--prev_hop)->next_tunnel_id());
        }
    }
}

TEST_CASE_METHOD(ChaChaIBEPFixture, "ChaCha IBEP creates BuildRequestRecords for each hop", "[chacha_ibep]")
{
  ChaChaInboundEndpoint::build_message_t build_message;

  // Create and encrypt the BuildRequestRecords for each hop
  REQUIRE_NOTHROW(build_message = ibep.CreateBuildMessage());

  auto& hops = ibep.hops();
  const auto& records = build_message.records();
  const auto& reqs_len = records.size();
  REQUIRE((reqs_len > hops.size() && reqs_len <= tini2p::under_cast(ChaChaInboundEndpoint::MaxHops)));

  const auto hops_begin = hops.begin();
  const auto hops_end = hops.end();
  const auto last_hop = --hops.end();

  // Check each hop record has valid fields, and the hop decrypts record
  for (auto hop_it = hops.begin(); hop_it != hops_end; ++hop_it)
    {
      const auto& req = records.at(std::distance(hops_begin, hop_it));

      REQUIRE(req.tunnel_id() == hop_it->tunnel_id());
      REQUIRE(req.local_ident_hash() == loc_info->identity().hash());
      REQUIRE(req.next_tunnel_id() == hop_it->next_tunnel_id());

      if (hop_it != last_hop)
        {
          auto next_hop = hop_it;
          REQUIRE(req.next_ident_hash() == (++next_hop)->info().identity().hash());
        }
      else  // last hop is the InboundEndpoint (us)
        REQUIRE(req.next_ident_hash() == loc_info->identity().hash());

      // Decrypt and deserialize the record as the tunnel hop
      auto hop_req_buf = req.buffer(); 
      REQUIRE_NOTHROW(hop_it->DecryptRequestRecord(hop_req_buf));
      ChaChaInboundEndpoint::build_request_t hop_req(hop_req_buf);

      REQUIRE(hop_req == req);
    }
}

TEST_CASE_METHOD(ChaChaIBEPFixture, "ChaCha IBEP preprocesses BuildRequestRecords for each hop", "[chacha_ibep]")
{
  ChaChaInboundEndpoint::build_message_t build_message;

  REQUIRE_NOTHROW(build_message = ibep.CreateBuildMessage());

  // Copy records for comparison, will still be in order (first index, first hop)
  const auto build_copy = build_message;
  const auto& copy_records = build_copy.records();

  // Pre-process the message, iteratively decrypting the records
  REQUIRE_NOTHROW(ibep.PreprocessChaChaBuildMessage(build_message));

  // Post-process the message, and check records are revealed
  // Somewhat unrealistic, normally tunnel hops each perform one round
  //   of encryption, revealing one record at a time during the tunnel build
  auto& records = build_message.records();
  const auto& records_len = records.size();
  auto& hops = ibep.hops();

  REQUIRE((records_len > hops.size() && records_len <= tini2p::under_cast(ChaChaInboundEndpoint::MaxHops)));

  const auto hops_begin = hops.begin();
  const auto hops_end = hops.end();

  const auto record_begin = records.begin();
  const auto record_end = records.end();

  for (auto hop_it = hops.begin(); hop_it != hops_end; ++hop_it)
    {
      const auto& hop_ident = hop_it->key_info().truncated_ident();

      const auto record_it = std::find_if(
          record_begin, record_end, [&hop_ident](const auto& r) { return r.truncated_ident() == hop_ident; });

      // Check that the next record is revealed
      REQUIRE(record_it != record_end);

      REQUIRE(record_it->buffer() == copy_records.at(std::distance(hops_begin, hop_it)).buffer());

      // Check that the record can be found with corresponding hop's truncated Identity hash
      REQUIRE(build_message.find_record(hop_ident) == *record_it);

      for (auto check_it = hops.begin(); check_it != hops_end; ++check_it)
        {
          // Check that all other records are encrypted in the message
          if (check_it != hop_it)
            {
              const auto& check_ident = check_it->key_info().truncated_ident();
              const auto record_check_it = std::find_if(record_begin, record_end, [&check_ident](const auto& r) {
                return r.truncated_ident() == check_ident;
              });

              REQUIRE(!(record_check_it->buffer() == copy_records.at(std::distance(hops_begin, check_it)).buffer()));
            }
        }

      // Simulate the tunnel hop encrypting its own and all other records
      for (auto enc_it = records.begin(); enc_it != record_end; ++enc_it)
        hop_it->EncryptRecord(enc_it->buffer(), std::distance(record_begin, enc_it), records_len);
    }
}

TEST_CASE_METHOD(ChaChaIBEPFixture, "ChaCha IBEP handles BuildResponseRecords for each hop", "[chacha_ibep]")
{
  using reply_message_t = ChaChaInboundEndpoint::reply_message_t;

  std::unique_ptr<reply_message_t> reply_msg_ptr;
  ChaChaInboundEndpoint::message_id_t msg_id(0x01);
  std::uint8_t accepted(0);
  const auto des_mode = reply_message_t::deserialize_mode_t::Partial;

  // Simulate each hop accepting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          ibep.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllAccept).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = ibep.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 1);

  // Simulate one hop accepting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          ibep.CreateChaChaReplyMessage(msg_id, CreateReplyType::OneAccept).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = ibep.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);

  // Simulate each hop rejecting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          ibep.CreateChaChaReplyMessage(msg_id, CreateReplyType::AllReject).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = ibep.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);

  // Simulate one hop rejecting the tunnel
  REQUIRE_NOTHROW(
      reply_msg_ptr = std::make_unique<reply_message_t>(
          ibep.CreateChaChaReplyMessage(msg_id, CreateReplyType::OneReject).extract_body(), des_mode));
  REQUIRE_NOTHROW(accepted = ibep.HandleBuildReply(*reply_msg_ptr));
  REQUIRE(accepted == 0);
}

TEST_CASE_METHOD(ChaChaIBEPFixture, "ChaCha IBEP iteratively decrypts received tunnel messages", "[chacha_ibep]")
{
  using i2np_block_t = ChaChaInboundEndpoint::i2np_block_t;

  // Create random I2NP message buffer
  i2np_block_t i2np_block(i2np_block_t::msg_type_t::Data, 0x41, tini2p::data::Data(2048).buffer());
  // Create random OutboundGateway to hash
  ChaChaInboundEndpoint::to_hash_t to_hash(RandBytes<32>());

  // Create mock InboundGateway
  tini2p::tunnels::InboundGateway<TunnelChaCha> ibgw(0x29, 0x31, hop_infos.front());

  // Create tunnel messages
  std::vector<ChaChaInboundEndpoint::tunnel_message_t> messages;
  REQUIRE_NOTHROW(messages = ibgw.CreateTunnelMessages(i2np_block.buffer(), to_hash, std::nullopt));
  REQUIRE(messages.size() > 1);

  // Copy messages for comparison
  const auto messages_copy = messages;

  // Simulate tunnel hops encrypting tunnel messages
  auto& last_hop = ibep.hops().back();
  for (std::size_t i = 0; i < messages.size(); ++i)
    {
      auto& message = messages.at(i);
      REQUIRE_NOTHROW(ibep.IterativeEncryption(message));
      REQUIRE(!(message == messages_copy.at(i)));

      // AEAD encrypt the messages for the first hop
      REQUIRE_NOTHROW(last_hop.EncryptAEAD(message));
    }

  const auto& msg_offset = tini2p::under_cast(ChaChaInboundEndpoint::chacha_layer_t::MessageHeaderLen);
  const auto& msg_len = tini2p::under_cast(ChaChaInboundEndpoint::chacha_layer_t::MessageBodyLen);
  // Decrypt the tunnel messages as the InboundEndpoint
  for (std::size_t i = 0; i < messages.size(); ++i)
    {
      auto& message = messages.at(i);

      // AEAD decrypt the message as the first tunnel hop
      REQUIRE_NOTHROW(ibep.DecryptAEAD(message));

      REQUIRE_NOTHROW(ibep.IterativeDecryption(message));

      REQUIRE(
          SecBytes(message.buffer().data() + msg_offset, msg_len)
          == SecBytes(messages_copy.at(i).buffer().data() + msg_offset, msg_len));
    }
}
