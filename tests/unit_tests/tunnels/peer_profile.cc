/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/tunnels/peer_profile.h"

using namespace tini2p::crypto;

using tini2p::tunnels::PeerProfile;

struct ProfileFixture
{
  ProfileFixture() : ident(RandBytes<32>()), profile(ident) {}

  PeerProfile::ident_hash_t ident;
  PeerProfile profile;
};

TEST_CASE_METHOD(ProfileFixture, "PeerProfile has valid fields", "[peer_profile]")
{
  REQUIRE(profile.speed() == 0);
  REQUIRE(profile.capacity() == 0);

  REQUIRE(profile.build_success() == 0);
  REQUIRE(profile.build_fail() == 0);
  REQUIRE(profile.build_drop() == 0);

  REQUIRE(profile.test_success() == 0);
  REQUIRE(profile.test_fail() == 0);
  REQUIRE(profile.test_drop() == 0);
}

TEST_CASE_METHOD(ProfileFixture, "PeerProfile updates build statistics", "[peer_profile]")
{
  PeerProfile::capacity_t capacity(0);

  REQUIRE_NOTHROW(capacity = profile.capacity());

  // Add a successful tunnel build
  REQUIRE_NOTHROW(profile.add_build_success());

  // Check capacity is updated
  REQUIRE(profile.capacity() > capacity);
  REQUIRE_NOTHROW(capacity = profile.capacity());

  // Add a failed tunnel build
  REQUIRE_NOTHROW(profile.add_build_fail());

  // Check capacity is updated
  REQUIRE(profile.capacity() < capacity);
  REQUIRE_NOTHROW(capacity = profile.capacity());

  // Add a dropped tunnel build
  REQUIRE_NOTHROW(profile.add_build_drop());

  // Check capacity is updated
  REQUIRE(profile.capacity() < capacity);
  REQUIRE_NOTHROW(capacity = profile.capacity());
}

TEST_CASE_METHOD(ProfileFixture, "PeerProfile updates test statistics", "[peer_profile]")
{
  PeerProfile::capacity_t capacity(0);

  REQUIRE_NOTHROW(capacity = profile.capacity());

  // Add a successful tunnel test
  REQUIRE_NOTHROW(profile.add_test_success());

  // Check capacity is updated
  REQUIRE(profile.capacity() > capacity);
  REQUIRE_NOTHROW(capacity = profile.capacity());

  // Add a failed tunnel test
  REQUIRE_NOTHROW(profile.add_test_fail());

  // Check capacity is updated
  REQUIRE(profile.capacity() < capacity);
  REQUIRE_NOTHROW(capacity = profile.capacity());

  // Add a dropped tunnel test
  REQUIRE_NOTHROW(profile.add_test_drop());

  // Check capacity is updated
  REQUIRE(profile.capacity() < capacity);
  REQUIRE_NOTHROW(capacity = profile.capacity());
}

TEST_CASE_METHOD(ProfileFixture, "PeerProfile updates transfer speed", "[peer_profile]")
{
  // update speed w/ default time interval (60s)
  REQUIRE_NOTHROW(profile.speed(100));

  // update speed w/ custom time interval
  REQUIRE_NOTHROW(profile.speed(200, 30));
}

TEST_CASE_METHOD(ProfileFixture, "PeerProfile rejects null Identity hash", "[peer_profile]")
{
  REQUIRE_THROWS(PeerProfile(PeerProfile::ident_hash_t{}));
}

TEST_CASE_METHOD(ProfileFixture, "PeerProfile rejects invalid speed sampling interval", "[peer_profile]")
{
  REQUIRE_THROWS(profile.speed(1, 0));
}
