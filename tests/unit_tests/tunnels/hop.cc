/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/tunnels/hop.h"

using namespace tini2p::crypto;

using AESHop = tini2p::tunnels::Hop<TunnelAES>;

/*---------------------\
| AES Tunnel Hop Tests |
\---------------------*/

struct AESHopFixture
{
  AESHopFixture()
      : info_ptr(std::make_shared<AESHop::info_t>()),
        id(0x17),
        next_id(0x19),
        iv(AESHop::aes_layer_t::create_iv()),
        message(0x42, iv, AESHop::tunnel_message_t::first_delivery_t{}, AESHop::i2np_block_t().buffer()),
        peer_key(AESHop::blake_t::create_key()),
        hop(id, next_id, info_ptr, peer_key)
  {
  }

  AESHop::info_t::shared_ptr info_ptr;
  AESHop::tunnel_id_t id, next_id;
  AESHop::aes_layer_t::aes_t::iv_t iv;
  AESHop::tunnel_message_t message;
  AESHop::peer_key_t peer_key;
  AESHop hop;
};

TEST_CASE_METHOD(AESHopFixture, "AES hop encrypts a message", "[aes_hop]")
{
  AESHop::aes_layer_t::aes_t::iv_t ret_iv;
  const auto msg_copy = message;

  REQUIRE_NOTHROW(ret_iv = hop.Encrypt(message));

  REQUIRE(!(ret_iv == iv));

  REQUIRE(!(message == msg_copy));

  AESHop::tunnel_id_t read_next_id;
  REQUIRE_NOTHROW(tini2p::read_bytes(message.buffer().data(), read_next_id, tini2p::Endian::Big));
  REQUIRE(read_next_id == next_id);
}

TEST_CASE_METHOD(AESHopFixture, "AES hop encrypts and decrypts back to original message", "[aes_hop]")
{
  AESHop::aes_layer_t::aes_t::iv_t ret_iv;
  auto msg_copy = message;

  REQUIRE_NOTHROW(ret_iv = hop.Encrypt(message));

  REQUIRE(!(ret_iv == iv));

  REQUIRE(!(message == msg_copy));

  AESHop::tunnel_id_t read_next_id;
  REQUIRE_NOTHROW(tini2p::read_bytes(message.buffer().data(), read_next_id, tini2p::Endian::Big));
  REQUIRE(read_next_id == next_id);

  // Write next ID to message copy for comparison
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_copy.buffer().data(), read_next_id, tini2p::Endian::Big));

  REQUIRE_NOTHROW(ret_iv = hop.Decrypt(message));
  REQUIRE(ret_iv == iv);

  REQUIRE((message == msg_copy));
}

TEST_CASE_METHOD(AESHopFixture, "AES hop rejects identical tunnel IDs", "[aes_hop]")
{
  REQUIRE_THROWS(AESHop(id, id, info_ptr, peer_key));
  REQUIRE_THROWS(AESHop(next_id, next_id, info_ptr, peer_key));
}

/*------------------------\
| ChaCha Tunnel Hop Tests |
\------------------------*/

using ChaChaHop = tini2p::tunnels::Hop<TunnelChaCha>;

struct ChaChaHopFixture
{
  ChaChaHopFixture()
      : info_ptr(std::make_shared<ChaChaHop::info_t>()),
        next_info_ptr(std::make_shared<ChaChaHop::info_t>()),
        hop_receive_key(RandBytes<ChaChaHop::chacha_layer_t::aead_t::KeyLen>()),
        id(0x17),
        next_id(0x19),
        next_next_id(0x23),
        nonces(ChaChaHop::chacha_layer_t::create_nonces()),
        message(
            0x42,
            nonces,
            ChaChaHop::tunnel_message_t::first_delivery_t(ChaChaHop::tunnel_message_t::delivery_layer_t::ChaCha),
            ChaChaHop::i2np_block_t().buffer()),
        peer_key(ChaChaHop::blake_t::create_key()),
        hop(id, next_id, info_ptr, hop_receive_key, peer_key),
        next_hop(next_id, next_next_id, next_info_ptr, hop.key_info().send_key(), peer_key)
  {
  }

  ChaChaHop::info_t::shared_ptr info_ptr, next_info_ptr;
  ChaChaHop::chacha_layer_t::aead_t::key_t hop_receive_key;
  ChaChaHop::tunnel_id_t id, next_id, next_next_id;
  ChaChaHop::chacha_layer_t::nonces_t nonces;
  ChaChaHop::tunnel_message_t message;
  ChaChaHop::peer_key_t peer_key;
  ChaChaHop hop, next_hop;
};

TEST_CASE_METHOD(ChaChaHopFixture, "ChaCha hop encrypts a message", "[chacha_hop]")
{
  ChaChaHop::chacha_layer_t::nonces_t ret_nonces;
  const auto msg_copy = message;

  // ChaCha20 encrypt the tunnel message
  REQUIRE_NOTHROW(ret_nonces = hop.Encrypt(message));

  REQUIRE(!(ret_nonces == nonces));

  REQUIRE(!(message == msg_copy));

  ChaChaHop::tunnel_id_t read_next_id;
  REQUIRE_NOTHROW(tini2p::read_bytes(message.buffer().data(), read_next_id, tini2p::Endian::Big));
  REQUIRE(read_next_id == next_id);

  // AEAD encrypt the tunnel message for the next hop
  REQUIRE_NOTHROW(hop.EncryptAEAD(message));
}

TEST_CASE_METHOD(ChaChaHopFixture, "ChaCha hop encrypts and decrypts back to original message", "[chacha_hop]")
{
  ChaChaHop::chacha_layer_t::nonces_t ret_nonces;
  auto msg_copy = message;

  // ChaCha20 encrypt the tunnel message
  REQUIRE_NOTHROW(ret_nonces = hop.Encrypt(message));

  REQUIRE(!(ret_nonces == nonces));

  REQUIRE(!(message == msg_copy));

  // Read the tunnel ID, and check it matches the next ID
  ChaChaHop::tunnel_id_t read_next_id;
  REQUIRE_NOTHROW(tini2p::read_bytes(message.buffer().data(), read_next_id, tini2p::Endian::Big));
  REQUIRE(read_next_id == next_id);

  // Write next ID to message copy for comparison
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_copy.buffer().data(), read_next_id, tini2p::Endian::Big));

  // ChaCha20 decrypt the tunnel message
  // Unrealistic, this step would be performed by the inbound endpoint
  REQUIRE_NOTHROW(ret_nonces = hop.Decrypt(message));
  REQUIRE(ret_nonces == nonces);

  REQUIRE((message == msg_copy));
}

TEST_CASE_METHOD(ChaChaHopFixture, "ChaCha hop AEAD encrypts and decrypts back to original message", "[chacha_hop]")
{
  ChaChaHop::chacha_layer_t::nonces_t ret_nonces;
  auto msg_copy = message;

  // Encrypt the tunnel message
  REQUIRE_NOTHROW(ret_nonces = hop.Encrypt(message));

  REQUIRE(!(ret_nonces == nonces));

  REQUIRE(!(message == msg_copy));

  // Read the tunnel ID, and check it matches the next ID
  ChaChaHop::tunnel_id_t read_next_id;
  REQUIRE_NOTHROW(tini2p::read_bytes(message.buffer().data(), read_next_id, tini2p::Endian::Big));
  REQUIRE(read_next_id == next_id);

  // Write next ID to message copy for comparison
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_copy.buffer().data(), read_next_id, tini2p::Endian::Big));

  // AEAD encrypt the tunnel message for the next hop
  REQUIRE_NOTHROW(hop.EncryptAEAD(message));

  // AEAD decrypt the tunnel message from the previous hop
  REQUIRE_NOTHROW(next_hop.DecryptAEAD(message));

  // Decrypt the tunnel message as the first hop
  // Somewhat unrealistic, this step would be performed by the inbound endpoint
  REQUIRE_NOTHROW(ret_nonces = hop.Decrypt(message));
  REQUIRE(ret_nonces == nonces);

  const auto& chacha_msg_len = tini2p::under_cast(ChaChaHop::chacha_layer_t::MessageHeaderLen)
                               + tini2p::under_cast(ChaChaHop::chacha_layer_t::MessageBodyLen);

  // Resize to avoid comparing the MAC added during AEAD encryption
  REQUIRE_NOTHROW(message.buffer().resize(chacha_msg_len));
  REQUIRE_NOTHROW(msg_copy.buffer().resize(chacha_msg_len));

  REQUIRE((message == msg_copy));
}

TEST_CASE_METHOD(ChaChaHopFixture, "ChaCha hop rejects identical tunnel IDs", "[chacha_hop]")
{
  REQUIRE_THROWS(ChaChaHop(id, id, info_ptr, hop_receive_key, peer_key));
}

TEST_CASE_METHOD(ChaChaHopFixture, "ChaCha hop rejects identical nonces", "[chacha_hop]")
{
  tini2p::BytesWriter<ChaChaHop::tunnel_message_t::buffer_t> writer(message.buffer());

  // Write obfs nonce over the tunnel nonce for identical nonces
  REQUIRE_NOTHROW(writer.skip_bytes(ChaChaHop::chacha_layer_t::TunnelIDLen));
  REQUIRE_NOTHROW(writer.write_data(static_cast<ChaChaHop::chacha_layer_t::nonce_t::buffer_t>(nonces.obfs_nonce)));

  REQUIRE_THROWS(hop.Encrypt(message));
}
