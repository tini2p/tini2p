/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/router/context.h"

using namespace tini2p::crypto;
namespace data = tini2p::data;

using tini2p::router::Context;
using tini2p::data::BuildRequestRecord;
using AESBuildResponse = tini2p::data::BuildResponseRecord<tini2p::crypto::AES>;
using ChaChaBuildResponse = tini2p::data::BuildResponseRecord<tini2p::crypto::ChaCha20>;
using tini2p::data::TunnelBuildMessage;

struct ContextFixture
{
  Context context;
};

TEST_CASE_METHOD(ContextFixture, "RouterContext accepts AES TunnelBuildMessages", "[context]")
{
  const auto& local_identity = context.info().identity();

  // Create a mock creator layer encryption paired with our local router
  tini2p::crypto::TunnelAES creator_layer(
      tini2p::crypto::TunnelAES::key_info_t(local_identity.crypto().pubkey, local_identity.hash()));

  const auto& creator_key_info = creator_layer.key_info();
  const auto& creator_pubkey = creator_key_info.creator_pubkey();

  const auto& hop_hash = local_identity.hash();
  const BuildRequestRecord::tunnel_id_t hop_id(0x42), next_id(0x43);
  const Context::info_t::identity_t::hash_t next_hash(RandBytes<32>());
  const BuildRequestRecord::params_t build_params{hop_id, RandBytes<32>(), next_id, next_hash, hop_hash};

  // Create a mock BuildRequestRecord for our router to participate as a tunnel Hop
  auto request_ptr = std::make_unique<BuildRequestRecord>(build_params, creator_pubkey);

  REQUIRE_NOTHROW(creator_layer.EncryptRequestRecord(request_ptr->buffer()));
  auto build_msg_ptr = std::make_unique<TunnelBuildMessage>(TunnelBuildMessage::records_t{*request_ptr});

  data::I2NPMessage build_msg(data::I2NPMessage::Type::VariableTunnelBuild, 0x02, build_msg_ptr->buffer());

  // Handle the TunnelBuildMessage as the PoolManager
  Context::info_t::identity_t::hash_t reply_hash;
  data::I2NPMessage reply_msg;
  REQUIRE_NOTHROW(std::tie(reply_hash, reply_msg) = context.HandleTunnelBuildMessage(build_msg));
  REQUIRE(reply_hash == next_hash);
  REQUIRE_NOTHROW(context.aes_pool_manager().find_transit_tunnel(hop_id));

  TunnelBuildMessage reply_build_msg(reply_msg.extract_body(), TunnelBuildMessage::deserialize_mode_t::Partial);

  // Somewhat unrealistic, here we know the index of the record directly.
  // Normally, the creator keeps track of the hop's record index when creating the TunnelBuildMessage
  AESBuildResponse reply(reply_build_msg.find_record(0).buffer(), AESBuildResponse::deserialize_mode_t::Partial);

  // Decrypt the reply as the mock creator
  // Somewhat unrealistic, BuildResponseRecord would normally be deserialized from a TunnelBuildReply
  REQUIRE_NOTHROW(creator_layer.DecryptRecord(reply.buffer()));
  REQUIRE_NOTHROW(reply.deserialize());

  // Check the tunnel was rejected
  REQUIRE(reply.flags() == AESBuildResponse::flags_t::Accept);
}

TEST_CASE_METHOD(
    ContextFixture,
    "RouterContext rejects duplicate AES TunnelBuildMessages from the same creator",
    "[context]")
{
  const auto& local_identity = context.info().identity();

  // Create a mock creator layer encryption paired with our local router
  tini2p::crypto::TunnelAES creator_layer(
      tini2p::crypto::TunnelAES::key_info_t(local_identity.crypto().pubkey, local_identity.hash()));

  const auto& creator_key_info = creator_layer.key_info();
  const auto& creator_pubkey = creator_key_info.creator_pubkey();

  const auto& hop_hash = local_identity.hash();
  const BuildRequestRecord::tunnel_id_t hop_id(0x42), next_id(0x43);
  const Context::info_t::identity_t::hash_t next_hash(RandBytes<32>());
  const BuildRequestRecord::params_t build_params{hop_id, RandBytes<32>(), next_id, next_hash, hop_hash};

  // Create a mock BuildRequestRecord for our router to participate as a tunnel Hop
  auto request_ptr = std::make_unique<BuildRequestRecord>(build_params, creator_pubkey);

  REQUIRE_NOTHROW(creator_layer.EncryptRequestRecord(request_ptr->buffer()));
  auto build_msg_ptr = std::make_unique<TunnelBuildMessage>(TunnelBuildMessage::records_t{*request_ptr});

  data::I2NPMessage build_msg(data::I2NPMessage::Type::VariableTunnelBuild, 0x02, build_msg_ptr->buffer());

  // Handle the TunnelBuildMessage as the PoolManager
  REQUIRE_NOTHROW(context.HandleTunnelBuildMessage(build_msg));

  // Create a duplicate mock BuildRequestRecord for our router to participate as a tunnel Hop
  request_ptr = std::make_unique<BuildRequestRecord>(build_params, creator_pubkey);

  // Encrypt the request as the mock creator
  REQUIRE_NOTHROW(creator_layer.EncryptRequestRecord(request_ptr->buffer()));

  build_msg_ptr = std::make_unique<TunnelBuildMessage>(TunnelBuildMessage::records_t{*request_ptr});
  build_msg.message_buffer(build_msg_ptr->buffer());

  // Handle the TunnelBuildMessage as the router context
  Context::info_t::identity_t::hash_t reply_hash;
  data::I2NPMessage reply_msg;
  REQUIRE_NOTHROW(std::tie(reply_hash, reply_msg) = context.HandleTunnelBuildMessage(build_msg));
  REQUIRE(reply_hash == next_hash);

  TunnelBuildMessage reply_build_msg(reply_msg.extract_body(), TunnelBuildMessage::deserialize_mode_t::Partial);

  // Somewhat unrealistic, here we know the index of the record directly.
  // Normally, the creator keeps track of the hop's record index when creating the TunnelBuildMessage
  AESBuildResponse reply(reply_build_msg.find_record(0).buffer(), AESBuildResponse::deserialize_mode_t::Partial);

  // Decrypt the reply as the mock creator
  // Somewhat unrealistic, BuildResponseRecord would normally be deserialized from a TunnelBuildReply
  REQUIRE_NOTHROW(creator_layer.DecryptRecord(reply.buffer()));
  REQUIRE_NOTHROW(reply.deserialize());

  // Check the tunnel was rejected
  REQUIRE(reply.flags() == AESBuildResponse::flags_t::Reject);
}

TEST_CASE_METHOD(ContextFixture, "RouterContext accepts ChaCha TunnelBuildMessages", "[context]")
{
  const auto& local_identity = context.info().identity();

  // Create a mock creator layer encryption paired with our local router
  tini2p::crypto::TunnelChaCha creator_layer(
      tini2p::crypto::TunnelChaCha::key_info_t(local_identity.crypto().pubkey, local_identity.hash()));

  const auto& creator_key_info = creator_layer.key_info();
  const auto& creator_pubkey = creator_key_info.creator_pubkey();

  const auto& hop_hash = local_identity.hash();
  const BuildRequestRecord::tunnel_id_t hop_id(0x42), next_id(0x43);
  const Context::info_t::identity_t::hash_t next_hash(RandBytes<32>());
  const BuildRequestRecord::params_t build_params{hop_id, RandBytes<32>(), next_id, next_hash, hop_hash};

  // Create a mock BuildRequestRecord for our router to participate as a tunnel Hop
  auto request_ptr = std::make_unique<BuildRequestRecord>(
      build_params, creator_pubkey, RandBytes<32>(), BuildRequestRecord::flags_t::ChaChaHop);

  REQUIRE_NOTHROW(creator_layer.EncryptRequestRecord(request_ptr->buffer()));
  auto build_msg_ptr = std::make_unique<TunnelBuildMessage>(TunnelBuildMessage::records_t{*request_ptr});

  data::I2NPMessage build_msg(data::I2NPMessage::Type::VariableTunnelBuild, 0x02, build_msg_ptr->buffer());

  // Handle the TunnelBuildMessage as the router context
  Context::info_t::identity_t::hash_t reply_hash;
  data::I2NPMessage reply_msg;
  REQUIRE_NOTHROW(std::tie(reply_hash, reply_msg) = context.HandleTunnelBuildMessage(build_msg));
  REQUIRE(reply_hash == next_hash);
  REQUIRE_NOTHROW(context.chacha_pool_manager().find_transit_tunnel(hop_id));

  TunnelBuildMessage reply_build_msg(reply_msg.extract_body(), TunnelBuildMessage::deserialize_mode_t::Partial);

  // Somewhat unrealistic, here we know the index of the record directly.
  // Normally, the creator keeps track of the hop's record index when creating the TunnelBuildMessage
  ChaChaBuildResponse reply(reply_build_msg.find_record(0).buffer(), ChaChaBuildResponse::deserialize_mode_t::Partial);

  // Decrypt the reply as the mock creator
  // Somewhat unrealistic, BuildResponseRecord would normally be deserialized from a TunnelBuildReply
  REQUIRE_NOTHROW(creator_layer.DecryptReplyRecord(reply.buffer()));
  REQUIRE_NOTHROW(reply.deserialize());

  // Check the tunnel was rejected
  REQUIRE(reply.flags() == ChaChaBuildResponse::flags_t::Accept);
}

TEST_CASE_METHOD(
    ContextFixture,
    "RouterContext rejects duplicate ChaCha TunnelBuildMessages from the same creator",
    "[context]")
{
  const auto& local_identity = context.info().identity();

  // Create a mock creator layer encryption paired with our local router
  tini2p::crypto::TunnelChaCha creator_layer(
      tini2p::crypto::TunnelChaCha::key_info_t(local_identity.crypto().pubkey, local_identity.hash()));

  const auto& creator_key_info = creator_layer.key_info();
  const auto& creator_pubkey = creator_key_info.creator_pubkey();

  const auto& hop_hash = local_identity.hash();
  const BuildRequestRecord::tunnel_id_t hop_id(0x42), next_id(0x43);
  const Context::info_t::identity_t::hash_t next_hash(RandBytes<32>());
  const BuildRequestRecord::params_t build_params{hop_id, RandBytes<32>(), next_id, next_hash, hop_hash};

  // Create a mock BuildRequestRecord for our router to participate as a tunnel Hop
  auto request_ptr =
      std::make_unique<BuildRequestRecord>(build_params, creator_pubkey, RandBytes<32>(), BuildRequestRecord::flags_t::ChaChaHop);

  REQUIRE_NOTHROW(creator_layer.EncryptRequestRecord(request_ptr->buffer()));
  auto build_msg_ptr = std::make_unique<TunnelBuildMessage>(TunnelBuildMessage::records_t{*request_ptr});

  data::I2NPMessage build_msg(data::I2NPHeader::Type::VariableTunnelBuild, 0x02, build_msg_ptr->buffer());

  // Handle the TunnelBuildMessage as the PoolManager
  REQUIRE_NOTHROW(context.HandleTunnelBuildMessage(build_msg));

  // Create a duplicate mock BuildRequestRecord for our router to participate as a tunnel Hop
  request_ptr =
      std::make_unique<BuildRequestRecord>(build_params, creator_pubkey, BuildRequestRecord::flags_t::ChaChaHop);

  // Encrypt the request as the mock creator
  REQUIRE_NOTHROW(creator_layer.EncryptRequestRecord(request_ptr->buffer()));

  build_msg_ptr = std::make_unique<TunnelBuildMessage>(TunnelBuildMessage::records_t{*request_ptr});
  build_msg.message_buffer(build_msg_ptr->buffer());

  // Handle the TunnelBuildMessage as the router context
  Context::info_t::identity_t::hash_t reply_hash;
  data::I2NPMessage reply_msg;
  REQUIRE_NOTHROW(std::tie(reply_hash, reply_msg) = context.HandleTunnelBuildMessage(build_msg));
  REQUIRE(reply_hash == next_hash);

  TunnelBuildMessage reply_build_msg(reply_msg.extract_body(), TunnelBuildMessage::deserialize_mode_t::Partial);

  // Somewhat unrealistic, here we know the index of the record directly.
  // Normally, the creator keeps track of the hop's record index when creating the TunnelBuildMessage
  ChaChaBuildResponse reply(reply_build_msg.find_record(0).buffer(), ChaChaBuildResponse::deserialize_mode_t::Partial);

  // Decrypt the reply as the mock creator
  // Somewhat unrealistic, BuildResponseRecord would normally be deserialized from a TunnelBuildReply
  REQUIRE_NOTHROW(creator_layer.DecryptReplyRecord(reply.buffer()));
  REQUIRE_NOTHROW(reply.deserialize());

  // Check the tunnel was rejected
  REQUIRE(reply.flags() == ChaChaBuildResponse::flags_t::Reject);
}
