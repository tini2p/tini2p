/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/crypto/chacha20.h"

using ChaCha = tini2p::crypto::ChaCha20;
using namespace tini2p::crypto;

TEST_CASE("ChaCha20 encrypts and decrypts a message", "[chacha]")
{
  ChaCha::key_t key(RandBytes<ChaCha::KeyLen>());
  ChaCha::nonce_t nonce(RandBytes<ChaCha::nonce_t::NonceLen>());

  tini2p::crypto::SecBytes message(RandBuffer(20));

  const auto msg_copy = message;  // copy message for comparison

  // encrypt & decrypt a message (in-place)
  REQUIRE_NOTHROW(ChaCha::StreamXOR(message, nonce, key));
  REQUIRE(!(message == msg_copy));

  REQUIRE_NOTHROW(ChaCha::StreamXOR(message, nonce, key));
  REQUIRE(message == msg_copy);

  auto msg_ptr = message.data();
  const auto& msg_len = message.size();

  // encrypt & decrypt a C-like message
  REQUIRE_NOTHROW(ChaCha::StreamXOR(msg_ptr, msg_len, nonce, key));
  REQUIRE(!(message == msg_copy));

  REQUIRE_NOTHROW(ChaCha::StreamXOR(msg_ptr, msg_len, nonce, key));
  REQUIRE(message == msg_copy);
}
