/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/crypto/siphash.h"

namespace crypto = tini2p::crypto;

struct SipHashFixture
{
  const crypto::SipHash::key_part_t key_pt1{
      {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07}};

  const crypto::SipHash::key_part_t key_pt2{
      {0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F}};

  const crypto::SipHash::iv_t iv{{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07}};

  const crypto::SipHash::digest_t digest{{0x3B, 0x62, 0xA9, 0xBA, 0x62, 0x58, 0xF5,
                                          0x61, 0x0F, 0x83, 0xE2, 0x64, 0xF3, 0x14,
                                          0x97, 0xB4}};
};

TEST_CASE_METHOD(SipHashFixture, "SipHash calculates digest", "[sip]")
{
  using Catch::Matchers::Equals;
  using vec = std::vector<std::uint8_t>;

  crypto::SipHash::digest_t result;
  REQUIRE_NOTHROW(crypto::SipHash::Hash(key_pt1, key_pt2, iv, result));
  REQUIRE_THAT(vec(result.begin(), result.end()),
               Equals(vec(digest.begin(), digest.end())));
}
