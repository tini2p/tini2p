/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/aes.h"

namespace crypto = tini2p::crypto;

struct AESFixture
{
  AESFixture() : k(crypto::AES::create_key_iv()), aes(k.key, k.iv)
  {
    crypto::RandBytes(in);
  }

  crypto::AES::key_iv_t k;
  crypto::FixedSecBytes<crypto::AES::BlockLen * 3> in, out;
  crypto::AES aes;
};

TEST_CASE("AES creates key and IV")
{
  crypto::AES::key_iv_t k;
  REQUIRE(k.key.size() == crypto::AES::KeyLen);
  REQUIRE(k.iv.size() == crypto::AES::IVLen);
}

TEST_CASE_METHOD(AESFixture, "AES ECB sets key", "[aes]")
{
  REQUIRE_NOTHROW(crypto::AES(k.key));
  REQUIRE_NOTHROW(crypto::AES(k.key).rekey(k.key));
}

TEST_CASE_METHOD(AESFixture, "AES CBC sets key and IV", "[aes]")
{
  REQUIRE_NOTHROW(crypto::AES(k.key, k.iv));
  REQUIRE_NOTHROW(crypto::AES(k.key, k.iv).rekey(k.key, k.iv));
}

TEST_CASE_METHOD(AESFixture, "AES ECB encrypts block(s) of data", "[aes]")
{
  REQUIRE_NOTHROW(aes.EncryptECB(in.data(), in.size()));
}

TEST_CASE_METHOD(AESFixture, "AES ECB decrypts a block(s) of data", "[aes]")
{
  REQUIRE_NOTHROW(aes.DecryptECB(in.data(), in.size()));
}

TEST_CASE_METHOD(AESFixture, "AES ECB encrypts and decrypts back to plaintext", "[aes]")
{
  const auto t_in = in;

  // encrypt/decrypt buffer in-place
  REQUIRE_NOTHROW(aes.EncryptECB(in.data(), in.size()));
  REQUIRE(!(in == t_in));

  REQUIRE_NOTHROW(aes.DecryptECB(in.data(), in.size()));
  REQUIRE(in == t_in);
}

TEST_CASE_METHOD(AESFixture, "AES CBC encrypts block(s) of data", "[aes]")
{
  REQUIRE_NOTHROW(aes.Encrypt(in.data(), in.size()));
}

TEST_CASE_METHOD(AESFixture, "AES CBC decrypts a block(s) of data", "[aes]")
{
  REQUIRE_NOTHROW(aes.Decrypt(in.data(), in.size()));
}

TEST_CASE_METHOD(AESFixture, "AES CBC encrypts and decrypts back to plaintext", "[aes]")
{
  const auto t_in = in;

  // encrypt/decrypt buffer in-place
  REQUIRE_NOTHROW(aes.Encrypt(in.data(), in.size()));
  REQUIRE(!(in == t_in));

  REQUIRE_NOTHROW(aes.Decrypt(in.data(), in.size()));
  REQUIRE(in == t_in);
}

TEST_CASE_METHOD(AESFixture, "AES ECB and CBC rejects null buffer", "[aes]")
{
  REQUIRE_THROWS(aes.EncryptECB(nullptr, in.size()));
  REQUIRE_THROWS(aes.DecryptECB(nullptr, in.size()));

  REQUIRE_THROWS(aes.Encrypt(nullptr, in.size()));
  REQUIRE_THROWS(aes.Decrypt(nullptr, in.size()));
}

TEST_CASE_METHOD(AESFixture, "AES ECB and CBC rejects invalid sizes", "[aes]")
{
  REQUIRE_THROWS(aes.EncryptECB(in.data(), 0));
  REQUIRE_THROWS(aes.DecryptECB(in.data(), 0));

  REQUIRE_THROWS(aes.Encrypt(in.data(), 0));
  REQUIRE_THROWS(aes.Decrypt(in.data(), 0));

  REQUIRE_THROWS(aes.EncryptECB(in.data(), crypto::AES::BlockLen - 1));
  REQUIRE_THROWS(aes.DecryptECB(in.data(), crypto::AES::BlockLen + 1));

  REQUIRE_THROWS(aes.Encrypt(in.data(), crypto::AES::BlockLen - 1));
  REQUIRE_THROWS(aes.Decrypt(in.data(), crypto::AES::BlockLen + 1));
}
