/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/crypto/tunnel/chacha.h"

using namespace tini2p::crypto;

struct TunnelChaChaFixture
{
  TunnelChaChaFixture()
      : hop_keys(TunnelChaCha::key_info_t::curve_t::create_keys()),
        next_hop_keys(TunnelChaCha::key_info_t::curve_t::create_keys()),
        next_next_hop_keys(TunnelChaCha::key_info_t::curve_t::create_keys()),
        hop_ident(RandBytes<TunnelChaCha::key_info_t::IdentHashLen>()),
        next_hop_ident(RandBytes<TunnelChaCha::key_info_t::IdentHashLen>()),
        next_next_hop_ident(RandBytes<TunnelChaCha::key_info_t::IdentHashLen>()),
        hop_key_info(hop_keys.pubkey, hop_ident),
        next_hop_key_info(next_hop_keys.pubkey, next_hop_ident, hop_key_info.send_key()),
        next_next_hop_key_info(next_next_hop_keys.pubkey, next_next_hop_ident, next_hop_key_info.send_key()),
        message(RandBuffer(TunnelChaCha::MessageLen)),
        record(RandBuffer(TunnelChaCha::RecordLen)),
        nonces{TunnelChaCha::nonce_t(RandBytes<TunnelChaCha::nonce_t::NonceLen>()),
               TunnelChaCha::nonce_t(RandBytes<TunnelChaCha::nonce_t::NonceLen>())},
        chacha(hop_key_info),
        next_chacha(next_hop_key_info),
        next_next_chacha(next_hop_key_info)
  {
    // Write random nonces to the message buffer
    tini2p::BytesWriter<TunnelChaCha::message_t> writer(message);

    REQUIRE_NOTHROW(writer.skip_bytes(tini2p::under_cast(TunnelChaCha::TunnelIDLen)));
    REQUIRE_NOTHROW(writer.write_data(static_cast<TunnelChaCha::nonce_t::buffer_t>(nonces.tunnel_nonce)));
    REQUIRE_NOTHROW(writer.write_data(static_cast<TunnelChaCha::nonce_t::buffer_t>(nonces.obfs_nonce)));
  }

  typename TunnelChaCha::key_info_t::curve_t::keypair_t hop_keys, next_hop_keys, next_next_hop_keys;
  typename TunnelChaCha::key_info_t::ident_hash_t hop_ident, next_hop_ident, next_next_hop_ident;
  typename TunnelChaCha::key_info_t hop_key_info, next_hop_key_info, next_next_hop_key_info;
  std::unique_ptr<TunnelChaCha::nonces_t> nonces_ptr;

  TunnelChaCha::message_t message;
  TunnelChaCha::record_t record;
  TunnelChaCha::nonces_t nonces;

  TunnelChaCha chacha, next_chacha, next_next_chacha;
};

TEST_CASE_METHOD(TunnelChaChaFixture, "TunnelChaCha encrypts a message", "[tunnel_chacha]")
{
  const auto msg_copy = message;

  REQUIRE_NOTHROW(nonces_ptr = std::make_unique<TunnelChaCha::nonces_t>(chacha.Encrypt(message)));

  // Check the message no longer matches the original
  REQUIRE(!(message == msg_copy));

  // Check that the returned IV is encrypted
  REQUIRE(!(*nonces_ptr == nonces));
}

TEST_CASE_METHOD(
    TunnelChaChaFixture,
    "TunnelChaCha AEAD encrypts and decrypts a message back to orignal",
    "[tunnel_chacha]")
{
  using Catch::Matchers::Equals;

  auto msg_copy = message;

  // AEAD encrypt the message
  REQUIRE_NOTHROW(chacha.EncryptAEAD(message));
  REQUIRE(!(message == msg_copy));

  // Offset for the end of the ChaCha encrypted portion of the TunnelMessage
  const auto& chacha_msg_len =
      tini2p::under_cast(TunnelChaCha::MessageHeaderLen) + tini2p::under_cast(TunnelChaCha::MessageBodyLen);

  // Copy the Poly1305 MAC for comparison
  std::string mac(message.begin() + chacha_msg_len, message.end());

  // AEAD decrypt the message as the next hop
  REQUIRE_NOTHROW(next_chacha.DecryptAEAD(message));

  // Copy the Poly1305 MAC to ensure decryption didn't change it
  std::string copy_mac(message.begin() + chacha_msg_len, message.end());

  // Resize message to avoid comparing the MAC
  // Message before AEAD encryption will not have the MAC
  message.resize(chacha_msg_len);
  msg_copy.resize(chacha_msg_len);

  // Check that the message matches original
  REQUIRE(message == msg_copy);

  // Check that the MAC matches the one calculated during encryption
  REQUIRE_THAT(mac, Equals(copy_mac));
}

TEST_CASE_METHOD(
    TunnelChaChaFixture,
    "TunnelChaCha ChaCha20 encrypts and decrypts a message back to original",
    "[tunnel_chacha]")
{
  const auto msg_copy = message;

  REQUIRE_NOTHROW(nonces_ptr = std::make_unique<TunnelChaCha::nonces_t>(chacha.Encrypt(message)));

  // Check the message no longer matches the original
  REQUIRE(!(message == msg_copy));

  // Check that the returned nonces are encrypted
  REQUIRE(!(*nonces_ptr == nonces));

  REQUIRE_NOTHROW(nonces_ptr = std::make_unique<TunnelChaCha::nonces_t>(chacha.Decrypt(message)));

  // Check the message matches the original
  REQUIRE(message == msg_copy);

  // Check that the returned nonces match the original
  REQUIRE(*nonces_ptr == nonces);
}

TEST_CASE_METHOD(
    TunnelChaChaFixture,
    "TunnelChaCha ChaCha20 encrypts and decrypts a record back to original with multiple layers",
    "[tunnel_chacha]")
{
  const auto record_copy = record;

  // Iteratively encrypt: hop1, hop0 (simulates inbound tunnel processing)
  REQUIRE_NOTHROW(chacha.EncryptRecord(record, 0, 2));
  REQUIRE_NOTHROW(next_chacha.EncryptRecord(record, 0, 2));

  // Check the record no longer matches the original
  REQUIRE(!(record == record_copy));

  // Iteratively decrypt: hop0, hop1 (simulates inbound endpoint processing)
  REQUIRE_NOTHROW(next_chacha.DecryptRecord(record, 0, 2));
  REQUIRE_NOTHROW(chacha.DecryptRecord(record, 0, 2));

  // Check the record matches the original
  REQUIRE(record == record_copy);

  // Iteratively decrypt: hop1, hop0 (simulates outbound gateway processing)
  REQUIRE_NOTHROW(next_chacha.DecryptRecord(record, 0, 2));
  REQUIRE_NOTHROW(chacha.DecryptRecord(record, 0, 2));

  // Check the record no longer matches the original
  REQUIRE(!(record == record_copy));

  // Iteratively encrypt: hop1, hop0 (simulates outbound tunnel processing)
  REQUIRE_NOTHROW(chacha.EncryptRecord(record, 0, 2));
  REQUIRE_NOTHROW(next_chacha.EncryptRecord(record, 0, 2));

  // Check the record matches the original
  REQUIRE(record == record_copy);

  // Iteratively decrypt: hop0, hop1
  // Order doesn't matter, because the key stream is XOR-ed out
  REQUIRE_NOTHROW(chacha.DecryptRecord(record, 0, 2));
  REQUIRE_NOTHROW(next_chacha.DecryptRecord(record, 0, 2));

  // Check the record no longer matches the original
  REQUIRE(!(record == record_copy));

  // Iteratively encrypt: hop0, hop1
  // Order doesn't matter, because the key stream is XOR-ed out
  REQUIRE_NOTHROW(chacha.EncryptRecord(record, 0, 2));
  REQUIRE_NOTHROW(next_chacha.EncryptRecord(record, 0, 2));

  // Check the record matches the original
  REQUIRE(record == record_copy);
}

TEST_CASE_METHOD(
    TunnelChaChaFixture,
    "TunnelChaCha ChaCha20 encrypts and decrypts a tunnel message back to original with multiple layers",
    "[tunnel_chacha]")
{
  const auto message_copy = message;

  // Simulate inbound tunnel message processing
  REQUIRE_NOTHROW(chacha.Encrypt(message));
  REQUIRE_NOTHROW(next_chacha.Encrypt(message));
  REQUIRE_NOTHROW(next_next_chacha.Encrypt(message));
  REQUIRE_NOTHROW(next_next_chacha.Decrypt(message));
  REQUIRE_NOTHROW(next_chacha.Decrypt(message));
  REQUIRE_NOTHROW(chacha.Decrypt(message));
  REQUIRE(message == message_copy);

  // Simulate outbound tunnel message processing
  REQUIRE_NOTHROW(next_next_chacha.Decrypt(message));
  REQUIRE_NOTHROW(next_chacha.Decrypt(message));
  REQUIRE_NOTHROW(chacha.Decrypt(message));
  REQUIRE_NOTHROW(chacha.Encrypt(message));
  REQUIRE_NOTHROW(next_chacha.Encrypt(message));
  REQUIRE_NOTHROW(next_next_chacha.Encrypt(message));
  REQUIRE(message == message_copy);
}

TEST_CASE_METHOD(TunnelChaChaFixture, "TunnelChaCha rejects identical nonces", "[tunnel_chacha]")
{
  // Write random nonces to the message buffer
  tini2p::BytesWriter<TunnelChaCha::message_t> writer(message);

  REQUIRE_NOTHROW(writer.skip_bytes(tini2p::under_cast(TunnelChaCha::TunnelIDLen)));
  REQUIRE_NOTHROW(writer.write_data(static_cast<TunnelChaCha::nonce_t::buffer_t>(nonces.tunnel_nonce)));
  REQUIRE_NOTHROW(writer.write_data(static_cast<TunnelChaCha::nonce_t::buffer_t>(nonces.tunnel_nonce)));

  REQUIRE_THROWS(chacha.Encrypt(message));
  REQUIRE_THROWS(chacha.Decrypt(message));
}
