/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/tunnel/aes.h"

using namespace tini2p::crypto;

struct TunnelAESFixture
{
  TunnelAESFixture()
      : hop_keys(TunnelAES::key_info_t::curve_t::create_keys()),
        next_hop_keys(TunnelAES::key_info_t::curve_t::create_keys()),
        next_next_hop_keys(TunnelAES::key_info_t::curve_t::create_keys()),
        hop_ident(RandBytes<TunnelAES::key_info_t::IdentHashLen>()),
        next_hop_ident(RandBytes<TunnelAES::key_info_t::IdentHashLen>()),
        next_next_hop_ident(RandBytes<TunnelAES::key_info_t::IdentHashLen>()),
        key_info(hop_keys.pubkey, hop_ident),
        next_key_info(next_hop_keys.pubkey, next_hop_ident),
        next_next_key_info(next_next_hop_keys.pubkey, next_next_hop_ident),
        message(RandBuffer(TunnelAES::MessageLen)),
        record(RandBuffer(TunnelAES::RecordLen)),
        iv(message.data() + TunnelAES::TunnelIDLen, TunnelAES::aes_t::IVLen),
        aes(key_info),
        next_aes(next_key_info),
        next_next_aes(next_next_key_info)
  {
  }

  typename TunnelAES::key_info_t::curve_t::keypair_t hop_keys, next_hop_keys, next_next_hop_keys;
  typename TunnelAES::key_info_t::ident_hash_t hop_ident, next_hop_ident, next_next_hop_ident;
  typename TunnelAES::key_info_t key_info, next_key_info, next_next_key_info;
  std::unique_ptr<TunnelAES::aes_t::iv_t> iv_ptr;
  TunnelAES::message_t message;
  TunnelAES::record_t record;
  TunnelAES::aes_t::iv_t iv;

  TunnelAES aes, next_aes, next_next_aes;
};

TEST_CASE_METHOD(TunnelAESFixture, "TunnelAES encrypts a message", "[tunnel_aes]")
{
  const auto msg_copy = message;

  REQUIRE_NOTHROW(iv_ptr = std::make_unique<TunnelAES::aes_t::iv_t>(aes.Encrypt(message)));

  // Check the message no longer matches the original
  REQUIRE(!(message == msg_copy));

  // Check that the returned IV is encrypted
  REQUIRE(!(*iv_ptr == iv));
}

TEST_CASE_METHOD(TunnelAESFixture, "TunnelAES decrypts a message", "[tunnel_aes]")
{
  const auto msg_copy = message;

  REQUIRE_NOTHROW(iv_ptr = std::make_unique<TunnelAES::aes_t::iv_t>(aes.Decrypt(message)));

  // Check the message no longer matches the original
  REQUIRE(!(message == msg_copy));

  // Check that the returned IV does not match the original
  REQUIRE(!(*iv_ptr == iv));
}

TEST_CASE_METHOD(TunnelAESFixture, "TunnelAES encrypts and decrypts a message back to plaintext", "[tunnel_aes]")
{
  const auto msg_copy = message;

  REQUIRE_NOTHROW(iv_ptr = std::make_unique<TunnelAES::aes_t::iv_t>(aes.Encrypt(message)));

  // Check the message no longer matches the original
  REQUIRE(!(message == msg_copy));

  // Check that the returned IV is encrypted
  REQUIRE(!(*iv_ptr == iv));

  REQUIRE_NOTHROW(iv_ptr = std::make_unique<TunnelAES::aes_t::iv_t>(aes.Decrypt(message)));

  // Check the record matches the original
  REQUIRE(message == msg_copy);

  // Check that the returned IV matches the original
  REQUIRE(*iv_ptr == iv);
}

TEST_CASE_METHOD(TunnelAESFixture, "TunnelAES encrypts and decrypts a record back to plaintext", "[tunnel_aes]")
{
  const auto record_copy = record;

  REQUIRE_NOTHROW(aes.EncryptRecord(record));

  // Check the record no longer matches the original
  REQUIRE(!(record == record_copy));

  REQUIRE_NOTHROW(aes.DecryptRecord(record));

  // Check the record matches the original
  REQUIRE(record == record_copy);

  // Preemptively decrypt the record
  REQUIRE_NOTHROW(aes.DecryptRecord(record));

  // Check the record no longer matches the original
  REQUIRE(!(record == record_copy));

  // Encrypt the record
  REQUIRE_NOTHROW(aes.EncryptRecord(record));

  // Check the record matches the original
  REQUIRE(record == record_copy);
}

TEST_CASE_METHOD(
    TunnelAESFixture,
    "TunnelAES encrypts and decrypts a record back to original with multiple layers",
    "[tunnel_aes]")
{
  const auto record_copy = record;

  // Order is important for AES-CBC, because of how the mode mixes the keystream with input.
  // One must "peel off" the layers of encryption in the reverse order they are applied.

  // Iteratively encrypt the record: hop0, hop1 (simulates inbound tunnel processing)
  REQUIRE_NOTHROW(aes.EncryptRecord(record));
  REQUIRE_NOTHROW(next_aes.EncryptRecord(record));

  // Check the record no longer matches the original
  REQUIRE(!(record == record_copy));

  // Iteratively decrypt the record: hop1, hop0 (simulates inbound endpoint processing)
  REQUIRE_NOTHROW(next_aes.DecryptRecord(record));
  REQUIRE_NOTHROW(aes.DecryptRecord(record));

  // Check the record matches the original
  REQUIRE(record == record_copy);

  // Preemptively decrypt the record: hop1, hop0 (simulates outbound gateway processing)
  REQUIRE_NOTHROW(next_aes.DecryptRecord(record));
  REQUIRE_NOTHROW(aes.DecryptRecord(record));

  // Check the record no longer matches the original
  REQUIRE(!(record == record_copy));

  // Encrypt the record: hop0, hop1 (simulates outbound tunnel processing)
  REQUIRE_NOTHROW(aes.EncryptRecord(record));
  REQUIRE_NOTHROW(next_aes.EncryptRecord(record));

  // Check the record matches the original
  REQUIRE(record == record_copy);
}

TEST_CASE_METHOD(
    TunnelAESFixture,
    "TunnelAES AES encrypts and decrypts a tunnel message back to original with multiple layers",
    "[tunnel_aes]")
{
  const auto message_copy = message;

  // Simulate inbound tunnel message processing
  REQUIRE_NOTHROW(aes.Encrypt(message));
  REQUIRE_NOTHROW(next_aes.Encrypt(message));
  REQUIRE_NOTHROW(next_next_aes.Encrypt(message));
  REQUIRE_NOTHROW(next_next_aes.Decrypt(message));
  REQUIRE_NOTHROW(next_aes.Decrypt(message));
  REQUIRE_NOTHROW(aes.Decrypt(message));
  REQUIRE(message == message_copy);

  // Simulate outbound tunnel message processing
  REQUIRE_NOTHROW(next_next_aes.Decrypt(message));
  REQUIRE_NOTHROW(next_aes.Decrypt(message));
  REQUIRE_NOTHROW(aes.Decrypt(message));
  REQUIRE_NOTHROW(aes.Encrypt(message));
  REQUIRE_NOTHROW(next_aes.Encrypt(message));
  REQUIRE_NOTHROW(next_next_aes.Encrypt(message));
  REQUIRE(message == message_copy);
}
