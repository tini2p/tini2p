/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/bloom_filter.h"

namespace crypto = tini2p::crypto;

using BloomFilter = tini2p::crypto::BloomFilter;
using BloomParams = tini2p::crypto::BloomParams;

struct BloomFixture
{
  BloomFixture() : n(614400), p(1024), bloom(n, p) {}

  BloomParams::n_t n;
  BloomParams::p_t p;
  BloomFilter bloom;
};

TEST_CASE_METHOD(BloomFixture, "BloomFilter has valid parameters", "[bloom]")
{
  REQUIRE(bloom.n() == 614400);
  REQUIRE(bloom.p() == 1024);
  REQUIRE(bloom.m() == 8863919);
  REQUIRE(bloom.k() == 10);
}

TEST_CASE_METHOD(BloomFixture, "BloomFilter checks and adds elements", "[bloom]")
{
  crypto::FixedSecBytes<8> element(crypto::RandBytes<8>()), oth_element(crypto::RandBytes<8>());

  // Check that element is not in an empty Bloom filter
  REQUIRE(bloom.CheckElement(element) == 0);

  // Check that the element was added to the Bloom filter
  REQUIRE(bloom.CheckElement(element) == 1);

  // Add another element to the Bloom filter
  REQUIRE_NOTHROW(bloom.AddElement(oth_element));

  // Check that the other element was added to the Bloom filter
  REQUIRE(bloom.CheckElement(oth_element) == 1);
}

TEST_CASE_METHOD(BloomFixture, "BloomFilter rejects invalid parameters", "[bloom]")
{
  const auto& min_n = tini2p::under_cast(BloomParams::MinN);
  const auto& max_n = tini2p::under_cast(BloomParams::MaxN);

  const auto& min_p = tini2p::under_cast(BloomParams::MinP);
  const auto& max_p = tini2p::under_cast(BloomParams::MaxP);

  REQUIRE_THROWS(BloomFilter(min_n - 1, min_p));
  REQUIRE_THROWS(BloomFilter(min_n - 1, min_p));

  REQUIRE_THROWS(BloomFilter(min_n, min_p - 1));
  REQUIRE_THROWS(BloomFilter(min_n, max_p + 1));

  REQUIRE_THROWS(BloomFilter(max_n + 1, max_p));
  REQUIRE_THROWS(BloomFilter(max_n, max_p + 1));

  REQUIRE_THROWS(BloomParams(min_n - 1, min_p));
  REQUIRE_THROWS(BloomParams(min_n - 1, min_p));

  REQUIRE_THROWS(BloomParams(min_n, min_p - 1));
  REQUIRE_THROWS(BloomParams(min_n, max_p + 1));

  REQUIRE_THROWS(BloomParams(max_n + 1, max_p));
  REQUIRE_THROWS(BloomParams(max_n, max_p + 1));
}
