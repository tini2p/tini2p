/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/bn.h"

using tini2p::crypto::BigNum;

struct BigNumFixture
{
  BigNum bn;
};

TEST_CASE_METHOD(BigNumFixture, "BigNum divides by another BigNum", "[bn]")
{
  REQUIRE_NOTHROW(bn.from_uint(16));
  BigNum two(2);

  // Check for the expected result
  BigNum remainder;
  BigNum exp_result(8);
  REQUIRE_NOTHROW(bn.Divide(BigNum(2), remainder) == exp_result);
  REQUIRE_NOTHROW(bn.Divide(BigNum(2)) == exp_result);
  REQUIRE(bn.uint() == 16);

  // Check that the BigNums divide evenly, i.e. no remainder
  REQUIRE(remainder == BigNum(0));

  // Check that division by a non-multiple results in the correct remainder
  REQUIRE(bn.Divide(BigNum(3), remainder).uint() == 5);
  REQUIRE(remainder.uint() == 1);

  REQUIRE((bn / BigNum(2)) == exp_result);

  REQUIRE_NOTHROW(exp_result.from_uint(16));
  REQUIRE(bn == exp_result);
}

TEST_CASE_METHOD(BigNumFixture, "BigNum rejects dividing by zero", "[bn]")
{
  BigNum rem, zero(0);

  REQUIRE_THROWS(bn.Divide(zero, rem));
  REQUIRE_THROWS(bn.Divide(zero));
  REQUIRE_THROWS(bn / zero);
}

TEST_CASE_METHOD(BigNumFixture, "BigNum adds another BigNum", "[bn]")
{
  // Check that unsigned addition returns the expected result
  REQUIRE(bn.UAdd(BigNum(3)).uint() == 3);

  // Check that signed addition returns expected result
  REQUIRE_NOTHROW(bn.from_uint(3));
  REQUIRE(bn.Add(BigNum(2).Negate()).uint() == 1);

  REQUIRE((bn + bn).uint() == 6);

  REQUIRE(bn.AddMod(bn, BigNum(4)).uint() == 2);
}

TEST_CASE_METHOD(BigNumFixture, "BigNum subtracts another BigNum", "[bn]")
{
  // Check that unsigned addition returns the expected result
  REQUIRE_NOTHROW(bn.from_uint(3));
  REQUIRE(bn.USubtract(BigNum(3)).uint() == 0);

  // Check that signed addition returns expected result
  REQUIRE(bn.Subtract(BigNum(2).Negate()).uint() == 5);

  REQUIRE((bn - bn).uint() == 0);
  REQUIRE((bn - BigNum(4)) == BigNum(1).Negate());

  REQUIRE(BigNum(10).SubtractMod(BigNum(1), BigNum(7)).uint() == 2);
}

TEST_CASE_METHOD(BigNumFixture, "BigNum exponentiates modulus a BigNum", "[bn]")
{
  REQUIRE_NOTHROW(bn.from_uint(2));
  REQUIRE(bn.ExpMod(BigNum(4), BigNum(20)).uint() == 16);
  REQUIRE(bn.ExpMod(BigNum(4), BigNum(16)).uint() == 0);
}

TEST_CASE_METHOD(BigNumFixture, "BigNum inverts modulus a BigNum", "[bn]")
{
  REQUIRE_NOTHROW(bn.from_uint(20));

  REQUIRE_NOTHROW(bn.ModInverse(BigNum(1)) == BigNum(1).Divide(bn));
}

TEST_CASE_METHOD(BigNumFixture, "BigNum exponetiates by another BigNum", "[bn]")
{
  BigNum p25519 = BigNum(2).Exp(BigNum(255)).Subtract(BigNum(19));

  REQUIRE(p25519.Mod(BigNum(8)).uint() == 5);
}

TEST_CASE_METHOD(BigNumFixture, "BigNum bitshifts left by a number of bits", "[bn]")
{
  REQUIRE((BigNum(0xfeedab) << 8) == BigNum(0xfeedab00));
}

TEST_CASE_METHOD(BigNumFixture, "BigNum bitshifts right by a number of bits", "[bn]")
{
  REQUIRE((BigNum(0xfeedab) >> 8) == BigNum(0xfeed));
}

TEST_CASE_METHOD(BigNumFixture, "BigNum masks bits", "[bn]")
{
  BigNum p25519(tini2p::crypto::SecBytes{0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                       0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                       0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xed});

  REQUIRE(p25519.MaskBits(8) == 0xed);
  REQUIRE(p25519.MaskBits(32) == 0xffffffed);
}
