/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/crypto/chacha_poly1305.h"

using ChaCha = tini2p::crypto::ChaChaPoly1305;

using namespace tini2p::crypto;

TEST_CASE("ChaChaPoly1305 encrypts and decrypts a message", "[chacha]")
{
  ChaCha::key_t key(RandBytes<ChaCha::KeyLen>());
  ChaCha::nonce_t nonce;

  tini2p::crypto::SecBytes message(RandBuffer(20)), ciphertext{}, ad(RandBuffer(32));

  const auto msg_copy = message;  // copy message for comparison

  // encrypt & decrypt a message
  REQUIRE_NOTHROW(ChaCha::AEADEncrypt(key, nonce, message, ad, ciphertext));
  REQUIRE(!(ciphertext == msg_copy));

  REQUIRE_NOTHROW(ChaCha::AEADDecrypt(key, nonce, message, ad, ciphertext));
  REQUIRE(message == msg_copy);

  // encrypt & decrypt a message (in-place)
  REQUIRE_NOTHROW(ChaCha::AEADEncrypt(key, nonce, message, ad, message));
  REQUIRE(!(message == msg_copy));

  REQUIRE_NOTHROW(ChaCha::AEADDecrypt(key, nonce, message, ad, message));
  REQUIRE(message == msg_copy);
}

TEST_CASE("ChaChaPoly1305 encrypts and decrypts a message with a random tunnel nonce", "[chacha]")
{
  ChaCha::key_t key(RandBytes<ChaCha::KeyLen>());
  ChaCha::tunnel_nonce_t nonce(RandBytes<ChaCha::tunnel_nonce_t::NonceLen>());

  tini2p::crypto::SecBytes message(RandBuffer(20)), ciphertext(36), ad(RandBuffer(32));

  const auto msg_copy = message;  // copy message for comparison

  auto msg_ptr = message.data();
  const auto& msg_len = message.size();

  auto ciph_ptr = ciphertext.data();
  const auto& ciph_len = ciphertext.size();

  // encrypt & decrypt a message
  REQUIRE_NOTHROW(ChaCha::AEADEncrypt(key, nonce, msg_ptr, msg_len, ad, ciph_ptr, ciph_len));
  REQUIRE(!(ciphertext == msg_copy));

  REQUIRE_NOTHROW(ChaCha::AEADDecrypt(key, nonce, msg_ptr, msg_len, ad, ciph_ptr, ciph_len));
  REQUIRE(message == msg_copy);
}
