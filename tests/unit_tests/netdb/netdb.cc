/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/netdb/netdb.h"

using Catch::Matchers::Equals;
using vec = std::vector<std::uint8_t>;

namespace crypto = tini2p::crypto;
namespace data = tini2p::data;

using tini2p::netdb::NetDB;

struct NetDBFixture
{
  NetDBFixture() : netdb(std::make_shared<data::Info>()) {}

  NetDB netdb;
  std::unique_ptr<NetDB::db_lookup_t> lookup_msg;
  std::unique_ptr<NetDB::db_store_t> store_msg;
  tini2p::crypto::Sha256::digest_t search_key, from_key;
  data::I2NPMessage lookup_ret;
};

TEST_CASE_METHOD(NetDBFixture, "NetDB handles DatabaseStore messages", "[netdb]")
{
  auto info_ptr = std::make_shared<data::Info>();

  REQUIRE_NOTHROW(store_msg = std::make_unique<data::DatabaseStore>(info_ptr));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));
  REQUIRE(netdb.floodfills().size() == 1);
  REQUIRE(*(netdb.floodfills().front()) == *info_ptr);

  auto ls2_ptr = std::make_shared<data::LeaseSet2>();

  REQUIRE_NOTHROW(store_msg = std::make_unique<data::DatabaseStore>(ls2_ptr));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));
  REQUIRE(netdb.lease_sets().size() == 1);
  REQUIRE(*(netdb.lease_sets().front()) == *ls2_ptr);
}

TEST_CASE_METHOD(NetDBFixture, "NetDB handles DatabaseLookup messages", "[netdb]")
{
  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(search_key));  // randomize search key, unrealistic
  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(from_key));  // randomize from key, unrealistic
  REQUIRE_NOTHROW(lookup_msg = std::make_unique<data::DatabaseLookup>(search_key, from_key));

  REQUIRE_NOTHROW(lookup_ret = netdb.HandleDatabaseLookup(*lookup_msg));

  // NetDB empty, should be no match
  REQUIRE(lookup_ret.type() == data::I2NPHeader::Type::DatabaseSearchReply);
  REQUIRE(data::DatabaseSearchReply(lookup_ret.message_buffer()).peer_keys().empty());
}

TEST_CASE_METHOD(NetDBFixture, "NetDB finds entries after local storage", "[netdb]")
{
  auto info_ptr = std::make_shared<data::Info>();

  // store RouterInfo
  REQUIRE_NOTHROW(store_msg = std::make_unique<data::DatabaseStore>(info_ptr));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));
  REQUIRE(netdb.floodfills().size() == 1);
  REQUIRE(*(netdb.floodfills().front()) == *info_ptr);

  auto ls2_ptr = std::make_shared<data::LeaseSet2>();

  // store LeaseSet2
  REQUIRE_NOTHROW(store_msg = std::make_unique<data::DatabaseStore>(ls2_ptr));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));
  REQUIRE(netdb.lease_sets().size() == 1);
  REQUIRE(*(netdb.lease_sets().front()) == *ls2_ptr);

  search_key = info_ptr->hash();
  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(from_key));

  // lookup RouterInfo entry
  REQUIRE_NOTHROW(lookup_msg = std::make_unique<data::DatabaseLookup>(search_key, from_key));
  REQUIRE_NOTHROW(lookup_ret = netdb.HandleDatabaseLookup(*lookup_msg));
  REQUIRE(lookup_ret.type() == data::I2NPHeader::Type::DatabaseStore);

  // lookup LeaseSet2 entry
  search_key = ls2_ptr->hash();
  REQUIRE_NOTHROW(lookup_msg = std::make_unique<data::DatabaseLookup>(search_key, from_key));
  REQUIRE_NOTHROW(lookup_ret = netdb.HandleDatabaseLookup(*lookup_msg));
  REQUIRE(lookup_ret.type() == data::I2NPHeader::Type::DatabaseStore);

  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(search_key));  // randomize search key, unrealistic
  REQUIRE_NOTHROW(lookup_msg = std::make_unique<data::DatabaseLookup>(search_key, from_key));
  REQUIRE_NOTHROW(lookup_ret = netdb.HandleDatabaseLookup(*lookup_msg));

  // should be no local match, and return a DatabaseSearchReply w/ one peer key
  REQUIRE(lookup_ret.type() == data::I2NPHeader::Type::DatabaseSearchReply);
  REQUIRE(data::DatabaseSearchReply(lookup_ret.message_buffer()).peer_keys().size() == 1);
}

TEST_CASE_METHOD(NetDBFixture, "NetDB returns at most default_num peer keys in a DatabaseSearchReply", "[netdb]")
{
  // add DefaultPeersLen + 1 RouterInfos to the NetDB
  const auto& default_peers = tini2p::under_cast(NetDB::DefaultPeersLen);
  const auto& total_peers = default_peers + 1;
  for (std::uint8_t i = 0; i < total_peers; ++i)
    {
      REQUIRE_NOTHROW(store_msg = std::make_unique<data::DatabaseStore>(std::make_shared<data::Info>()));
      REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));
    }

  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(search_key));  // randomize search key, unrealistic
  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(from_key));  // randomize from key, unrealistic
  REQUIRE_NOTHROW(lookup_msg = std::make_unique<data::DatabaseLookup>(search_key, from_key));
  REQUIRE_NOTHROW(lookup_ret = netdb.HandleDatabaseLookup(*lookup_msg));

  // should be no local match, and return a DatabaseSearchReply w/ one peer key
  REQUIRE(lookup_ret.type() == data::I2NPHeader::Type::DatabaseSearchReply);
  REQUIRE(data::DatabaseSearchReply(lookup_ret.message_buffer()).peer_keys().size() == default_peers);
}

TEST_CASE_METHOD(NetDBFixture, "NetDB finds closest peers based on lookup flag", "[netdb]")
{
  // store a floodfill peer
  const auto ff_peer = std::make_shared<data::Info>();
  REQUIRE_NOTHROW(store_msg = std::make_unique<data::DatabaseStore>(ff_peer));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));

  // store a non-floodfill peer
  auto nonff_peer = std::make_shared<data::Info>();
  nonff_peer->options().add(std::string("caps"), std::string("OR"));

  REQUIRE_NOTHROW(store_msg = std::make_unique<data::DatabaseStore>(nonff_peer));
  REQUIRE_NOTHROW(netdb.HandleDatabaseStore(*store_msg));

  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(search_key));  // randomize search key, unrealistic

  NetDB::peers_t peers;
  // perform an Exploratory lookup, should only return non-ff peer
  REQUIRE_NOTHROW(peers = netdb.ClosestPeers(search_key, data::DatabaseLookup::LookupFlags::Explore));

  for (const auto& peer : peers)
    REQUIRE(peer == nonff_peer->hash());

  // perform a non-Exploratory lookup, should return both stored peers
  REQUIRE_NOTHROW(peers = netdb.ClosestPeers(search_key, data::DatabaseLookup::LookupFlags::Explore));

  for (const auto& peer : peers)
    {
      const bool has_both = peer == nonff_peer->hash() || peer == ff_peer->hash();
      REQUIRE(has_both);
    }
}

TEST_CASE_METHOD(NetDBFixture, "NetDB finds a RouterInfo", "[netdb]")
{
  auto info = std::make_shared<NetDB::info_t>();
  const auto& ident_hash = info->identity().hash();

  REQUIRE_NOTHROW(netdb.AddInfo(info));
  REQUIRE(netdb.has_info(ident_hash));
  REQUIRE(*netdb.find_info(ident_hash) == *info);
}

TEST_CASE_METHOD(NetDBFixture, "NetDB does not find a RouterInfo that doesn't exist", "[netdb]")
{
  // Create a mock Identity hash
  const NetDB::info_t::identity_t::hash_t& ident_hash(tini2p::crypto::RandBytes<32>());

  REQUIRE(!netdb.has_info(ident_hash));
  REQUIRE_THROWS(netdb.find_info(ident_hash));
}

TEST_CASE_METHOD(NetDBFixture, "NetDB rejects null RouterInfo", "[netdb]")
{
  REQUIRE_THROWS(NetDB(nullptr));
  REQUIRE_THROWS(netdb.AddInfo(nullptr));
}
