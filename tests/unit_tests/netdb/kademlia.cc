/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/netdb/kademlia.h"

using Catch::Matchers::Equals;
using vec = std::vector<std::uint8_t>;

using Kademlia = tini2p::netdb::Kademlia;

struct KademliaFixture
{
  Kademlia::search_key_t search_key{{0x99, 0x81, 0xaf, 0xcf, 0x9a, 0x57, 0x1f, 0x51, 0x25, 0x3a, 0x1a,
                              0x33, 0xe6, 0xef, 0x2d, 0x6f, 0xd8, 0x7a, 0x1e, 0x12, 0x13, 0x0f,
                              0x65, 0x97, 0x02, 0xfb, 0x22, 0x8a, 0x0f, 0x5e, 0xfa, 0x3f}};

  Kademlia::routing_key_t routing_key{{0x4d, 0x16, 0xf0, 0xd6, 0x9b, 0x2e, 0x32, 0xed, 0xda, 0x0f, 0x32,
                                       0x60, 0xdc, 0x27, 0x03, 0x51, 0xd5, 0xa7, 0x17, 0x84, 0xb9, 0x2a,
                                       0x0e, 0x0a, 0x5f, 0xdb, 0x9f, 0x58, 0x3c, 0x48, 0xab, 0xd8}};

  Kademlia::metric_t metric{{0xd4, 0x97, 0x5f, 0x19, 0x01, 0x79, 0x2d, 0xbc, 0xff, 0x35, 0x28,
                             0x53, 0x3a, 0xc8, 0x2e, 0x3e, 0x0d, 0xdd, 0x09, 0x96, 0xaa, 0x25,
                             0x6b, 0x9d, 0x5d, 0x20, 0xbd, 0xd2, 0x33, 0x16, 0x51, 0xe7}};
};

TEST_CASE_METHOD(KademliaFixture, "Kademlia creates a routing key", "[netdb]")
{
  Kademlia::routing_key_t t_routing_key;
  Kademlia::CreateRoutingKey(search_key, t_routing_key);

  REQUIRE_THAT(vec(t_routing_key.begin(), t_routing_key.end()), Equals(vec(routing_key.begin(), routing_key.end())));
}

TEST_CASE_METHOD(KademliaFixture, "Kademlia calculates XOR distance metric", "[netdb]")
{
  Kademlia::metric_t t_metric;
  Kademlia::XOR(routing_key, search_key, t_metric);

  REQUIRE_THAT(vec(t_metric.begin(), t_metric.end()), Equals(vec(metric.begin(), metric.end())));
}
