/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/ecies/existing_session.h"
#include "src/ecies/keys.h"

using tini2p::ecies::EciesInitiator;
using tini2p::ecies::EciesResponder;
using tini2p::ecies::ExistingSessionMessage;
using tini2p::ecies::DHState;
using tini2p::ecies::Keys;
using OutCipherState = tini2p::ecies::CipherState<EciesInitiator>;
using InCipherState = tini2p::ecies::CipherState<EciesResponder>;

struct ExistingSessionFixture
{
  ExistingSessionFixture()
      : dh_state(),
        remote_dh_state(),
        ecies_state(),
        remote_ecies_state(),
        ecies_keys(Keys::curve_t::create_keys(), Keys::curve_t::create_keys()),
        remote_ecies_keys(Keys::curve_t::create_keys(), Keys::curve_t::create_keys()),
        local_id_keys(ecies_keys.id_keys()),
        remote_id_keys(remote_ecies_keys.id_keys()),
        local_ep_keys(ecies_keys.ep_keys()),
        remote_ep_keys(remote_ecies_keys.ep_keys()),
        existing_msg()
  {
    // Set remote keys for local ECIES state, unrealistic.
    // Realistically, set while processing NewSessionMessage.
    REQUIRE_NOTHROW(ecies_keys.remote_rekey(remote_id_keys.pubkey, remote_ep_keys.pubkey));

    tini2p::crypto::X25519::shrkey_t tag_key, sym_key;

    // Setup local ECIES state as the sender
    REQUIRE_NOTHROW(std::tie(tag_key, sym_key) = dh_state.Ratchet(local_ep_keys, remote_ep_keys.pubkey));
    REQUIRE_NOTHROW(ecies_state.UpdateChainKeys(std::move(tag_key), std::move(sym_key)));

    // Set remote keys for remote ECIES state, unrealistic.
    // Realistically, set while processing NewSessionMessage.
    REQUIRE_NOTHROW(remote_ecies_keys.remote_rekey(local_id_keys.pubkey, local_ep_keys.pubkey));

    // Setup remote ECIES state as the receiver
    REQUIRE_NOTHROW(std::tie(tag_key, sym_key) = remote_dh_state.Ratchet(remote_ep_keys, local_ep_keys.pubkey));
    REQUIRE_NOTHROW(remote_ecies_state.UpdateChainKeys(std::move(tag_key), std::move(sym_key)));
  }

  DHState dh_state, remote_dh_state;
  OutCipherState ecies_state;
  InCipherState remote_ecies_state;
  Keys ecies_keys, remote_ecies_keys;
  Keys::curve_t::keypair_t local_id_keys, remote_id_keys;
  Keys::curve_t::keypair_t local_ep_keys, remote_ep_keys;
  ExistingSessionMessage existing_msg;
};

TEST_CASE_METHOD(
    ExistingSessionFixture,
    "ExistingSessionMessage fails to serialize without a session tag",
    "[ecies_x25519]")
{
  REQUIRE_THROWS(existing_msg.serialize(ecies_state));
}

TEST_CASE_METHOD(
    ExistingSessionFixture,
    "ExistingSessionMessage de/serializes after setting a session tag",
    "[ecies_x25519]")
{
  REQUIRE_NOTHROW(existing_msg.tag(ecies_state.tag()));
  const auto existing_copy = existing_msg;  // create copy for comparison

  REQUIRE_NOTHROW(existing_msg.serialize(ecies_state));
  REQUIRE_NOTHROW(existing_msg.deserialize(remote_ecies_state));

  REQUIRE((existing_msg == existing_copy));
}

TEST_CASE_METHOD(
    ExistingSessionFixture,
    "ExistingSessionMessage de/serializes w/ payload after setting a session tag",
    "[ecies_x25519]")
{
  REQUIRE_NOTHROW(existing_msg.tag(ecies_state.tag()));
  REQUIRE_NOTHROW(existing_msg.blocks(ExistingSessionMessage::payload_blocks_t{tini2p::data::PaddingBlock(0x42)}));
  const auto existing_copy = existing_msg;  // create copy for comparison

  REQUIRE_NOTHROW(existing_msg.serialize(ecies_state));
  REQUIRE_NOTHROW(existing_msg.deserialize(remote_ecies_state));

  REQUIRE((existing_msg == existing_copy));
}

TEST_CASE_METHOD(ExistingSessionFixture, "ExistingSessionMessage rejects null session tag", "[ecies_x25519]")
{
  ExistingSessionMessage::session_tag_t null_tag;

  REQUIRE_THROWS(existing_msg.tag(null_tag));

  REQUIRE_NOTHROW(existing_msg.tag(ecies_state.tag()));
  REQUIRE_NOTHROW(existing_msg.serialize(ecies_state));

  tini2p::BytesWriter<ExistingSessionMessage::buffer_t> writer(existing_msg.buffer());
  REQUIRE_NOTHROW(writer.write_data(null_tag));

  REQUIRE_THROWS(existing_msg.deserialize(remote_ecies_state));
}

TEST_CASE_METHOD(ExistingSessionFixture, "ExistingSessionMessage accepts in-order blocks", "[ecies_x25519]")
{
  tini2p::data::PaddingBlock pad_block(0x42);
  tini2p::data::TerminationBlock term_block;

  // Create payload blocks with Termination block following Padding block
  ExistingSessionMessage::payload_blocks_t blocks{pad_block};
  REQUIRE_NOTHROW(existing_msg.blocks(blocks));

  REQUIRE_NOTHROW(blocks.emplace_back(term_block));
  REQUIRE_NOTHROW(existing_msg.blocks(blocks));

  REQUIRE_NOTHROW(blocks.clear());

  // There is no particular order for the following set of blocks
  // None are required, and each permutation can be followed by a Padding block, a Termination block, both, or neither
  // There are hundreds of thousands of permutations possible, we'll test 4
  tini2p::data::AckBlock ack_block(0x1, 0x1);
  tini2p::data::AckRequestBlock::delivery_t::hash_t to_hash;
  tini2p::crypto::RandBytes(to_hash);  // create random "to hash", unrealistic
  tini2p::data::AckRequestBlock ack_req_block(0x1, to_hash);
  tini2p::data::DateTimeBlock date_block;
  tini2p::data::ECIESOptionsBlock opt_block;
  tini2p::data::GarlicBlock garlic_block;
  tini2p::data::MessageNumberBlock num_block;
  tini2p::data::NewDHKeyBlock key_block(0x1, static_cast<tini2p::crypto::X25519::pubkey_t>(local_ep_keys.pubkey));

  REQUIRE_NOTHROW(blocks.clear());
  
  // Add the blocks in alphabetical order
  REQUIRE_NOTHROW(blocks = {ack_block, ack_req_block, date_block, opt_block, garlic_block, num_block, key_block});
  REQUIRE_NOTHROW(existing_msg.blocks(blocks));

  // Reverse the block order
  REQUIRE_NOTHROW(std::reverse(blocks.begin(), blocks.end()));
  REQUIRE_NOTHROW(existing_msg.blocks(blocks));

  // Add a Padding block
  REQUIRE_NOTHROW(blocks.emplace_back(pad_block));
  REQUIRE_NOTHROW(existing_msg.blocks(blocks));

  // Add a Termination block
  REQUIRE_NOTHROW(blocks.emplace_back(term_block));
  REQUIRE_NOTHROW(existing_msg.blocks(blocks));

  // Serialize and encrypt the message
  REQUIRE_NOTHROW(existing_msg.tag(ecies_state.tag()));
  REQUIRE_NOTHROW(existing_msg.serialize(ecies_state));

  // Deserialize and decrypt the message
  REQUIRE_NOTHROW(existing_msg.deserialize(remote_ecies_state));

  // Check the message blocks match the last set blocks
  for (const auto& block : blocks)
  {
    std::visit(
        [this](const auto& b) {
          using block_type = std::decay_t<decltype(b)>;

          REQUIRE(existing_msg.has_block<block_type>());
          REQUIRE(existing_msg.get_block<block_type>() == b);
        },
        block);
  }
}

TEST_CASE_METHOD(ExistingSessionFixture, "ExistingSessionMessage rejects out-of-order blocks", "[ecies_x25519]")
{
  tini2p::data::TerminationBlock term_block;
  tini2p::data::PaddingBlock pad_block(0x42);

  // Create payload blocks with Padding block following Termination block
  ExistingSessionMessage::payload_blocks_t blocks{{term_block}, {pad_block}};
  REQUIRE_THROWS(existing_msg.blocks(blocks));

  // Add multiple termination blocks
  REQUIRE_NOTHROW(blocks.pop_back());
  REQUIRE_NOTHROW(blocks.emplace_back(term_block));

  REQUIRE_THROWS(existing_msg.blocks(blocks));

  // Clear, and add multiple padding blocks
  REQUIRE_NOTHROW(blocks.clear());
  REQUIRE_NOTHROW(blocks.emplace_back(pad_block));
  REQUIRE_NOTHROW(blocks.emplace_back(pad_block));

  REQUIRE_THROWS(existing_msg.blocks(blocks));

  // Rejects any block (besides Termination block) following Padding block
  REQUIRE_NOTHROW(blocks.pop_back());
  REQUIRE_NOTHROW(blocks.emplace_back(tini2p::data::AckBlock()));

  REQUIRE_THROWS(existing_msg.blocks(blocks));

  REQUIRE_NOTHROW(blocks.pop_back());
  REQUIRE_NOTHROW(blocks.emplace_back(tini2p::data::AckRequestBlock()));

  REQUIRE_THROWS(existing_msg.blocks(blocks));

  REQUIRE_NOTHROW(blocks.pop_back());
  REQUIRE_NOTHROW(blocks.emplace_back(tini2p::data::ECIESOptionsBlock()));

  REQUIRE_THROWS(existing_msg.blocks(blocks));

  REQUIRE_NOTHROW(blocks.pop_back());
  REQUIRE_NOTHROW(blocks.emplace_back(tini2p::data::GarlicBlock()));

  REQUIRE_THROWS(existing_msg.blocks(blocks));

  REQUIRE_NOTHROW(blocks.pop_back());
  REQUIRE_NOTHROW(blocks.emplace_back(tini2p::data::MessageNumberBlock()));

  REQUIRE_THROWS(existing_msg.blocks(blocks));

  REQUIRE_NOTHROW(blocks.pop_back());
  REQUIRE_NOTHROW(blocks.emplace_back(tini2p::data::NewDHKeyBlock()));

  REQUIRE_THROWS(existing_msg.blocks(blocks));
}
