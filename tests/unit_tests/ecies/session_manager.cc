/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/ecies/session_manager.h"

using tini2p::ecies::SessionManager;
using tini2p::ecies::PayloadSection;
using TagStatus = tini2p::ecies::EciesX25519::TagStatus;
using DecryptionStatus = tini2p::ecies::EciesX25519::DecryptionStatus;
using SessionMode = tini2p::ecies::EciesX25519::SessionMode;

struct EciesSessionManagerFixture
{
  SessionManager manager;
};

TEST_CASE_METHOD(EciesSessionManagerFixture, "Creates a new outbound session for a Destination", "[ecies_mgr]")
{
  // Create mock remote ECIES instance
  SessionManager::ecies_x25519_t remote_ecies;

  // Create mock payload
  tini2p::ecies::PayloadSection payload;

  // Create a mock outbound session
  const auto& id_key = remote_ecies.id_keys().pubkey;
  REQUIRE_NOTHROW(manager.CreateOutboundSession(id_key, payload));
}

TEST_CASE_METHOD(EciesSessionManagerFixture, "Handles trial-decrypting a NewSessionMessage", "[ecies_mgr]")
{
  SessionManager remote_manager;

  auto& rem_ecies = remote_manager.ecies();

  // Create mock LeaseSet2
  SessionManager::destination_t::eddsa_t rem_signer;
  SessionManager::destination_t rem_dest(rem_ecies.id_keys(), rem_signer);
  tini2p::data::LeaseSet2 rem_lease_set(rem_dest);

  // Create mock DatabaseStore message for a mock LeaseSet2
  tini2p::data::GarlicBlock garlic_block(tini2p::data::I2NPHeader::Type::DatabaseStore, rem_lease_set.buffer());

  // Create mock payload section containing the DatabaseStore message
  tini2p::ecies::PayloadSection payload(garlic_block);

  auto& loc_ecies = manager.ecies();

  // Create mock local destination
  SessionManager::destination_t::eddsa_t loc_signer;
  SessionManager::destination_t loc_dest(loc_ecies.id_keys(), loc_signer);

  std::optional<SessionManager::ecies_x25519_t::new_message_t> new_msg;

  // Create mock remote outbound session, and encrypt the remote NewSessionMessage
  const auto& loc_id_key = loc_ecies.id_keys().pubkey;
  REQUIRE_NOTHROW(new_msg.emplace(remote_manager.CreateOutboundSession(loc_id_key, payload)));
  REQUIRE(new_msg != std::nullopt);

  DecryptionStatus status;
  SessionMode mode;

  // Trial decrypt the NewSession message
  REQUIRE_NOTHROW(std::tie(status, mode) = manager.TrialDecrypt(*new_msg));

  // Check that trial decryption succeeds, and returns the correct session mode
  REQUIRE(status == DecryptionStatus::Successful);
  REQUIRE(mode == SessionMode::Bound);
}

TEST_CASE_METHOD(
    EciesSessionManagerFixture,
    "Handles extracting DatabaseStore from decrypted NewSessionMessage",
    "[ecies_mgr]")
{
  const auto& loc_ecies = manager.ecies();

  // Create mock LeaseSet2
  SessionManager::destination_t::eddsa_t loc_signer;
  SessionManager::destination_t loc_dest(loc_ecies.id_keys(), loc_signer);
  auto loc_lease_set = std::make_shared<tini2p::data::LeaseSet2>(loc_dest);

  // Create mock DatabaseStore message for a mock LeaseSet2
  tini2p::data::DatabaseStore db_store(loc_lease_set);
  tini2p::data::GarlicBlock garlic_block(tini2p::data::I2NPHeader::Type::DatabaseStore, db_store.buffer());

  // Create mock payload section containing the DatabaseStore message
  std::unique_ptr<PayloadSection> pay_ptr;
  REQUIRE_NOTHROW(pay_ptr = std::make_unique<PayloadSection>(garlic_block));

  // Create mock NewSessionMessage, unrealistic.
  // Realistically, the message would be trial-decrypted from a new inbound session at the remote router
  SessionManager::ecies_x25519_t::new_message_t new_msg(loc_ecies.id_keys().pubkey, *pay_ptr);
}

TEST_CASE_METHOD(
    EciesSessionManagerFixture,
    "Handles creating new inbound/outbound sessions from trial-decrypted NewSessionMessage",
    "[ecies_mgr]")
{
  SessionManager remote_manager;

  auto& rem_ecies = remote_manager.ecies();
  rem_ecies.ep_keys(tini2p::crypto::X25519::create_keys());

  // Create mock LeaseSet2
  SessionManager::destination_t::eddsa_t rem_signer;
  SessionManager::destination_t rem_dest(rem_ecies.id_keys(), rem_signer);
  auto rem_lease_set = std::make_shared<tini2p::data::LeaseSet2>(rem_dest);

  // Create mock DatabaseStore message for a mock LeaseSet2
  tini2p::data::DatabaseStore db_store(rem_lease_set);
  tini2p::data::GarlicBlock garlic_block(tini2p::data::I2NPHeader::Type::DatabaseStore, db_store.buffer());

  // Create mock payload section containing the DatabaseStore message
  tini2p::ecies::PayloadSection payload(garlic_block);

  const auto& loc_ecies = manager.ecies();

  // Create mock local destination
  SessionManager::destination_t::eddsa_t loc_signer;
  SessionManager::destination_t loc_dest(loc_ecies.id_keys(), loc_signer);

  std::optional<SessionManager::ecies_x25519_t::new_message_t> new_msg;

  // Create mock remote outbound session, and encrypt the remote NewSessionMessage
  REQUIRE_NOTHROW(new_msg.emplace(remote_manager.CreateOutboundSession(loc_ecies.id_keys().pubkey, payload)));
  REQUIRE(new_msg != std::nullopt);

  DecryptionStatus status;
  SessionMode mode;

  // Trial-decrypt the NewSession message
  REQUIRE_NOTHROW(std::tie(status, mode) = manager.TrialDecrypt(*new_msg));

  // Check that trial decryption succeeds, and returns the correct session mode
  REQUIRE(status == DecryptionStatus::Successful);
  REQUIRE(mode == SessionMode::Bound);

  std::optional<SessionManager::new_reply_t> new_reply;
  SessionManager::new_reply_t::payload_t reply_payload;

  // Create a NewSessionReply for the remote Destination
  REQUIRE_NOTHROW(new_reply.emplace(manager.CreateNewSessionReply(rem_ecies.id_keys().pubkey, reply_payload)));

  REQUIRE_NOTHROW(remote_manager.HandleNewSessionReply(*new_reply));
}

TEST_CASE_METHOD(
    EciesSessionManagerFixture,
    "Handles decrypting an ExistingSessionMessage for established inbound and outbound sessions",
    "[ecies_mgr]")
{
  SessionManager remote_manager;

  const auto& loc_ecies = manager.ecies();

  tini2p::ecies::PayloadSection payload;

  std::optional<SessionManager::ecies_x25519_t::new_message_t> new_msg;

  // Create mock remote outbound session, and encrypt the remote NewSessionMessage
  const auto& loc_id_key = loc_ecies.id_keys().pubkey;
  REQUIRE_NOTHROW(new_msg.emplace(remote_manager.CreateOutboundSession(loc_id_key, payload)));
  REQUIRE(new_msg != std::nullopt);

  DecryptionStatus status;
  SessionMode mode;

  // Trial-decrypt the NewSession message
  REQUIRE_NOTHROW(std::tie(status, mode) = manager.TrialDecrypt(*new_msg));

  // Check that trial decryption succeeds, and returns the correct session mode
  REQUIRE(status == DecryptionStatus::Successful);
  REQUIRE(mode == SessionMode::Bound);

  std::optional<SessionManager::new_reply_t> new_reply;
  SessionManager::new_reply_t::payload_t reply_payload;

  // Create a NewSessionReply for the remote Destination
  REQUIRE_NOTHROW(
      new_reply.emplace(manager.CreateNewSessionReply(remote_manager.ecies().id_keys().pubkey, reply_payload)));

  // Decrypt the NewSessionReply message, and create an established outbound session
  REQUIRE_NOTHROW(remote_manager.HandleNewSessionReply(*new_reply));

  SessionManager::existing_message_t existing_message(
      SessionManager::existing_message_t::payload_blocks_t{tini2p::data::PaddingBlock(0x42)});

  auto existing_msg_copy = existing_message;

  // Encrypt a remote outbound ExistingSessionMessage to fully confirm the session
  REQUIRE_NOTHROW(remote_manager.HandleOutboundExistingMessage(loc_id_key, existing_message));

  // Copy tag after its set during encryption
  existing_msg_copy.tag(existing_message.tag());

  // Decrypt the ExistingSessionMessage on the local router
  REQUIRE_NOTHROW(manager.HandleInboundExistingMessage(existing_message));
  REQUIRE((existing_message == existing_msg_copy));
}

TEST_CASE_METHOD(
    EciesSessionManagerFixture,
    "Handles trial-decrypting Garlic message with a NewSession message",
    "[ecies_mgr]")
{
  SessionManager remote_manager;

  auto& loc_ecies = manager.ecies();

  // Create a pending outbound session as the remote Destination
  std::optional<SessionManager::new_message_t> new_msg;
  REQUIRE_NOTHROW(new_msg.emplace(remote_manager.CreateOutboundSession(loc_ecies.id_keys().pubkey, {})));

  tini2p::data::I2NPMessage i2np_garlic(
      tini2p::data::I2NPMessage::Type::Garlic,
      0x02,
      new_msg->buffer(),
      tini2p::data::I2NPMessage::Mode::Garlic);

  // Check that the local Destination successfully decrypts the NewSession message
  SessionManager::message_v ret_msg;
  REQUIRE_NOTHROW(ret_msg = manager.HandleI2NPMessage(i2np_garlic));
  REQUIRE(std::holds_alternative<SessionManager::new_message_t>(ret_msg));
}

TEST_CASE_METHOD(
    EciesSessionManagerFixture,
    "Handles decrypting Garlic message with a NewSessionReply for a pending outbound session",
    "[ecies_mgr]")
{
  SessionManager remote_manager;

  auto& loc_ecies = manager.ecies();
  auto& rem_ecies = remote_manager.ecies();

  // Create a pending outbound session as the remote Destination
  std::optional<SessionManager::new_message_t> new_msg;
  REQUIRE_NOTHROW(new_msg.emplace(remote_manager.CreateOutboundSession(loc_ecies.id_keys().pubkey, {})));

  DecryptionStatus status;
  SessionMode mode;

  // Trial-decrypt the NewSession message
  REQUIRE_NOTHROW(std::tie(status, mode) = manager.TrialDecrypt(*new_msg));

  // Check that trial decryption succeeds, and returns the correct session mode
  REQUIRE(status == DecryptionStatus::Successful);
  REQUIRE(mode == SessionMode::Bound);

  // Create and encrypt a NewSessionReply as the local Destination to confirm the session
  std::optional<SessionManager::new_reply_t> new_reply;
  REQUIRE_NOTHROW(new_reply.emplace(manager.CreateNewSessionReply(rem_ecies.id_keys().pubkey, {})));

  tini2p::data::I2NPMessage i2np_garlic(
      tini2p::data::I2NPMessage::Type::Garlic,
      0x02,
      new_reply->buffer(),
      tini2p::data::I2NPMessage::Mode::Garlic);

  REQUIRE(remote_manager.FindPendingOutboundTag(new_reply->reply_tag()) == TagStatus::Found);

  // Check that the remote Destination successfully decrypts the NewSessionReply from the local Destination
  SessionManager::message_v ret_msg;
  REQUIRE_NOTHROW(ret_msg = remote_manager.HandleI2NPMessage(i2np_garlic));
  REQUIRE(std::holds_alternative<SessionManager::new_reply_t>(ret_msg));
}

TEST_CASE_METHOD(
    EciesSessionManagerFixture,
    "Uses unique tags for multiple NewSessionReply messages to the same Destination",
    "[ecies_mgr]")
{
  SessionManager remote_manager;

  auto& loc_ecies = manager.ecies();
  auto& rem_ecies = remote_manager.ecies();

  // Create a pending outbound session as the remote Destination
  std::optional<SessionManager::new_message_t> new_msg;
  REQUIRE_NOTHROW(new_msg.emplace(remote_manager.CreateOutboundSession(loc_ecies.id_keys().pubkey, {})));

  DecryptionStatus status;
  SessionMode mode;

  // Trial-decrypt the NewSession message
  REQUIRE_NOTHROW(std::tie(status, mode) = manager.TrialDecrypt(*new_msg));

  // Check that trial decryption succeeds, and returns the correct session mode
  REQUIRE(status == DecryptionStatus::Successful);
  REQUIRE(mode == SessionMode::Bound);

  // Create and encrypt a NewSessionReply as the local Destination to confirm the session
  std::optional<SessionManager::new_reply_t> new_reply, new_reply_two;
  REQUIRE_NOTHROW(new_reply.emplace(manager.CreateNewSessionReply(rem_ecies.id_keys().pubkey, {})));
  REQUIRE_NOTHROW(new_reply_two.emplace(manager.CreateNewSessionReply(rem_ecies.id_keys().pubkey, {})));

  REQUIRE(!(new_reply->reply_tag() == new_reply_two->reply_tag()));
}

TEST_CASE_METHOD(
    EciesSessionManagerFixture,
    "Handles decrypting Garlic message with an ExistingSessionMessage for a pending inbound session",
    "[ecies_mgr]")
{
  SessionManager remote_manager;

  auto& loc_ecies = manager.ecies();
  const auto& loc_id_key = loc_ecies.id_keys().pubkey;
  auto& rem_ecies = remote_manager.ecies();

  // Create a pending outbound session as the remote Destination
  std::optional<SessionManager::new_message_t> new_msg;
  REQUIRE_NOTHROW(new_msg.emplace(remote_manager.CreateOutboundSession(loc_id_key, {})));

  DecryptionStatus status;
  SessionMode mode;

  // Trial-decrypt the NewSession message
  REQUIRE_NOTHROW(std::tie(status, mode) = manager.TrialDecrypt(*new_msg));

  // Check that trial decryption succeeds, and returns the correct session mode
  REQUIRE(status == DecryptionStatus::Successful);
  REQUIRE(mode == SessionMode::Bound);

  // Create and encrypt a NewSessionReply as the local Destination to confirm the session
  std::optional<SessionManager::new_reply_t> new_reply;
  REQUIRE_NOTHROW(new_reply.emplace(manager.CreateNewSessionReply(rem_ecies.id_keys().pubkey, {})));

  REQUIRE(remote_manager.FindPendingOutboundTag(new_reply->reply_tag()) == TagStatus::Found);

  // Check the remote Destination successfully decrypts the NewSessionReply
  REQUIRE_NOTHROW(remote_manager.HandleNewSessionReply(*new_reply));

  SessionManager::existing_message_t existing_msg(
      SessionManager::existing_message_t::payload_blocks_t{tini2p::data::PaddingBlock(0x42)});

  // Create and encrypt an outbound ExistingSession message as the remote Destination to fully confirm the session
  REQUIRE_NOTHROW(remote_manager.HandleOutboundExistingMessage(loc_id_key, existing_msg));

  tini2p::data::I2NPMessage i2np_garlic(
      tini2p::data::I2NPMessage::Type::Garlic,
      0x02,
      existing_msg.buffer(),
      tini2p::data::I2NPMessage::Mode::Garlic);

  REQUIRE(manager.FindPendingInboundTag(existing_msg.tag()) == TagStatus::Found);

  // Check that the local Destination successfully decrypts the inbound ExistingSession message
  SessionManager::message_v ret_msg;
  REQUIRE_NOTHROW(ret_msg = manager.HandleI2NPMessage(i2np_garlic));
  REQUIRE(std::holds_alternative<SessionManager::existing_message_t>(ret_msg));
}

TEST_CASE_METHOD(
    EciesSessionManagerFixture,
    "Handles decrypting Garlic message with an ExistingSessionMessage for an established inbound session",
    "[ecies_mgr]")
{
  SessionManager remote_manager;

  auto& loc_ecies = manager.ecies();
  const auto& loc_id_key = loc_ecies.id_keys().pubkey;
  auto& rem_ecies = remote_manager.ecies();

  // Create a pending outbound session as the remote Destination
  std::optional<SessionManager::new_message_t> new_msg;
  REQUIRE_NOTHROW(new_msg.emplace(remote_manager.CreateOutboundSession(loc_id_key, {})));

  DecryptionStatus status;
  SessionMode mode;

  // Trial-decrypt the NewSession message
  REQUIRE_NOTHROW(std::tie(status, mode) = manager.TrialDecrypt(*new_msg));

  // Check that trial decryption succeeds, and returns the correct session mode
  REQUIRE(status == DecryptionStatus::Successful);
  REQUIRE(mode == SessionMode::Bound);

  // Create and encrypt a NewSessionReply as the local Destination to confirm the session
  std::optional<SessionManager::new_reply_t> new_reply;
  REQUIRE_NOTHROW(new_reply.emplace(manager.CreateNewSessionReply(rem_ecies.id_keys().pubkey, {})));

  REQUIRE(remote_manager.FindPendingOutboundTag(new_reply->reply_tag()) == TagStatus::Found);

  // Check the remote Destination successfully decrypts the NewSessionReply
  REQUIRE_NOTHROW(remote_manager.HandleNewSessionReply(*new_reply));

  SessionManager::existing_message_t existing_msg(
      SessionManager::existing_message_t::payload_blocks_t{tini2p::data::PaddingBlock(0x42)});

  // Create and encrypt an outbound ExistingSession message as the remote Destination to fully confirm the session
  REQUIRE_NOTHROW(remote_manager.HandleOutboundExistingMessage(loc_id_key, existing_msg));

  const auto tag = existing_msg.tag();

  REQUIRE_NOTHROW(manager.HandleInboundExistingMessage(existing_msg));

  // Encrypt the same message again, unrealistic, but uses a different session tag
  REQUIRE_NOTHROW(remote_manager.HandleOutboundExistingMessage(loc_id_key, existing_msg));

  REQUIRE(!(existing_msg.tag() == tag));

  tini2p::data::I2NPMessage i2np_garlic(
      tini2p::data::I2NPMessage::Type::Garlic,
      0x02,
      existing_msg.buffer(),
      tini2p::data::I2NPMessage::Mode::Garlic);

  REQUIRE(manager.FindInboundTag(existing_msg.tag()) == TagStatus::Found);
  REQUIRE(manager.FindPendingInboundTag(existing_msg.tag()) == TagStatus::NotFound);

  // Check the local Destination successfully decrypts the ExistingSession message for the established inbound session
  SessionManager::message_v ret_msg;
  REQUIRE_NOTHROW(ret_msg = manager.HandleI2NPMessage(i2np_garlic));
  REQUIRE(std::holds_alternative<SessionManager::existing_message_t>(ret_msg));
}

TEST_CASE_METHOD(
    EciesSessionManagerFixture,
    "Rejects multiple NewSession messages using the same ephemeral key",
    "[ecies_mgr]")
{
  SessionManager remote_manager;
  auto& loc_ecies = manager.ecies();

  // Create a pending outbound session as the remote Destination
  std::optional<SessionManager::new_message_t> new_msg;
  REQUIRE_NOTHROW(new_msg.emplace(remote_manager.CreateOutboundSession(loc_ecies.id_keys().pubkey, {})));

  // Copy for simulating a replay attack
  auto new_msg_replay = *new_msg;

  DecryptionStatus status;
  SessionMode mode;

  // Trial-decrypt the NewSession message
  REQUIRE_NOTHROW(std::tie(status, mode) = manager.TrialDecrypt(*new_msg));

  // Check that trial decryption succeeds, and returns the correct session mode
  REQUIRE(status == DecryptionStatus::Successful);
  REQUIRE(mode == SessionMode::Bound);

  // Check the SessionManager rejects another NewSession message with the same ephemeral key
  // Ephemeral keys must be unique to prevent derived key re-use and replay attacks
  REQUIRE_THROWS(manager.TrialDecrypt(new_msg_replay));
}

TEST_CASE_METHOD(
    EciesSessionManagerFixture,
    "Rejects multiple NewSessionReply messages using the same ephemeral key",
    "[ecies_mgr]")
{
  SessionManager remote_manager;

  auto& loc_ecies = manager.ecies();
  auto& rem_ecies = remote_manager.ecies();

  // Create a pending outbound session as the remote Destination
  std::optional<SessionManager::new_message_t> new_msg;
  REQUIRE_NOTHROW(new_msg.emplace(remote_manager.CreateOutboundSession(loc_ecies.id_keys().pubkey, {})));

  DecryptionStatus status;
  SessionMode mode;

  // Trial-decrypt the NewSession message
  REQUIRE_NOTHROW(std::tie(status, mode) = manager.TrialDecrypt(*new_msg));

  // Check that trial decryption succeeds, and returns the correct session mode
  REQUIRE(status == DecryptionStatus::Successful);
  REQUIRE(mode == SessionMode::Bound);

  // Create and encrypt a NewSessionReply as the local Destination to confirm the session
  std::optional<SessionManager::new_reply_t> new_reply;
  REQUIRE_NOTHROW(new_reply.emplace(manager.CreateNewSessionReply(rem_ecies.id_keys().pubkey, {})));

  // Copy for simulating replay attack
  auto new_reply_replay = *new_reply;

  // Check that the initial NewSessionReply message decryption succeeds
  REQUIRE_NOTHROW(remote_manager.HandleNewSessionReply(*new_reply));

  // Check the SessionManager rejects another NewSessionReply message with the same ephemeral key
  // Ephemeral keys must be unique to prevent derived key re-use and replay attacks
  REQUIRE_THROWS(remote_manager.HandleNewSessionReply(new_reply_replay));
}
