/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/ecies/new_session_reply.h"

using tini2p::ecies::EciesInitiator;
using tini2p::ecies::EciesResponder;
using tini2p::ecies::Keys;
using tini2p::ecies::EphemeralKeySection;
using tini2p::ecies::StaticKeySection;
using tini2p::ecies::PayloadSection;
using tini2p::ecies::NewSessionMessage;

struct NewSessionReplyFixture
{
  NewSessionFixture()
      : ecies_keys(Keys::curve_t::create_keys(), Keys::elligator_t::create_keys()),
        remote_ecies_keys(Keys::curve_t::create_keys(), Keys::elligator_t::create_keys()),
        local_id_keys(ecies_keys.id_keys()),
        remote_id_keys(remote_ecies_keys.id_keys()),
        local_ep_keys(ecies_keys.ep_keys()),
        remote_ep_keys(remote_ecies_keys.ep_keys()),
        reply_tag(RandBytes<8>()),
        chain_key(RandBytes<32>()),
        h(RandBytes<32>()),
        payload(),
        new_reply(reply_tag, chain_key, h, payload)
  {
    // Set Bob's remote keys manually (unrealistic)
    // Realistically, Bob sets remote static (id) and ephemeral key (ep) from Alice's NewSessionMessage.
    REQUIRE_NOTHROW(ecies_keys.remote_rekey(remote_id_keys.pubkey, remote_ep_keys.pubkey));

    // Set Alice's remote keys manually (unrealistic)
    // Realistically, Alice sets remote static key from Bob's LeaseSet2,
    //   and Bob's ephemeral key from processing the NewSessionReplyMessage.
    REQUIRE_NOTHROW(remote_ecies_keys.remote_rekey(local_id_keys.pubkey, local_ep_keys.pubkey));
  }

  NewSessionReplyMessage::ecies_keys_t ecies_keys, remote_ecies_keys;
  NewSessionReplyMessage::ecies_keys_t::curve_t::keypair_t local_id_keys, remote_id_keys;
  NewSessionReplyMessage::ecies_keys_t::curve_t::keypair_t local_ep_keys, remote_ep_keys;
  NewSessionReplyMessage::tag_state_t::tag_t reply_tag;
  NewSessionReplyMessage::key_info_t::chain_key_t chain_key;
  NewSessionReplyMessage::key_info_t::h_t h;
  NewSessionReplyMessage::payload_t payload;
  NewSessionReplyMessage new_reply;
};

TEST_CASE_METHOD(NewSessionReplyFixture, "NewSessionReplyMessage processes a FlagsKey section", "[ecies_x25519]")
{
  // Set Alice's remote keys manually (unrealistic)
  // Realistically, Alice sets remote static keys from Bob's LeaseSet2.
  REQUIRE_NOTHROW(remote_ecies_keys.remote_id_key(local_id_keys.pubkey));

  auto& fk_section = new_reply.flags_key_section();
  const auto& fk_buf = fk_section.buffer();
  const auto copy_buf = fk_buf;

  // Encrypt the reply payload as Bob
  REQUIRE_NOTHROW(new_reply.ProcessPayloadSection<EciesInitiator>(payload, fk_section, ecies_keys));

  // section buffer should no longer match the copy
  REQUIRE_THAT(!(fk_buf == copy_buf));

  // decrypt the payload section as Alice
  REQUIRE_NOTHROW(new_reply.ProcessStaticKeySection<EciesResponder>(payload, fk_section, remote_ecies_keys));

  // section buffer should now match the copy again
  REQUIRE_THAT(fk_buf == copy_buf);
}

TEST_CASE_METHOD(NewSessionReplyFixture, "NewSessionReplyMessage processes a ReplyPayload section", "[ecies_x25519]")
{
  // Create EphemeralKeySection w/ ReplyPayload flag
  StaticKeySection fk_section(StaticKeySection::Flags::Reply);

  const auto& pay_buf = payload.buffer();
  const auto copy_buf = pay_buf;

  // Encrypt the reply payload as Bob
  REQUIRE_NOTHROW(new_reply.ProcessPayloadSection<EciesInitiator>(payload, fk_section, ecies_keys));

  // section buffer should no longer match the copy
  REQUIRE_THAT(!(pay_buf == copy_buf));

  // decrypt the payload section as Alice
  REQUIRE_NOTHROW(new_reply.ProcessPayloadSection<EciesResponder>(payload, fk_section, remote_ecies_keys));

  // section buffer should now match the copy again
  REQUIRE_THAT(pay_buf == copy_buf);
}

TEST_CASE_METHOD(NewSessionReplyFixture, "NewSessionReplyMessage serializes and deserializes from a buffer", "[ecies_x25519]")
{
  auto& new_buf = new_reply.buffer();
  const auto new_copy = new_buf;

  // Encrypt and serialize the NewSessionReply message to buffer
  REQUIRE_NOTHROW(new_reply.serialize(ecies_keys));

  // message buffer should no longer match the copy
  REQUIRE(!(new_buf == new_copy));

  // Deserialize and decrypt the NewSessionReply message from buffer
  REQUIRE_NOTHROW(new_reply.deserialize(remote_ecies_keys));

  // message buffer should now match the copy again
  REQUIRE(new_buf == new_copy);

  // Check that Alice's remote ephemeral key matches Bob's ephemeral public key
  REQUIRE(remote_ecies_keys.remote_ep_key() == ecies_keys.ep_keys().pubkey);
}
