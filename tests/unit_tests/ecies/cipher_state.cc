/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/codecs.h"

#include "src/ecies/cipher_state.h"

using namespace tini2p::crypto;

using tini2p::ecies::EciesInitiator;
using tini2p::ecies::EciesResponder;
using tini2p::ecies::Keys;
using tini2p::ecies::DHState;
using TagState = tini2p::ecies::TagState<EciesResponder>;
using SymmetricState = tini2p::ecies::SymmetricState<EciesResponder>;

struct CipherStateFixture
{
  DHState dh_state;
};

TEST_CASE_METHOD(CipherStateFixture, "Generates tags and symmetric keys", "[cipherstate]")
{
  auto root_key = Base64::Decode(std::string("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA="));
  auto sess_key = Base64::Decode(std::string("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA="));

  std::vector<tini2p::ecies::Tag> exp_tags{Base64::Decode(std::string("tZBi3Iima44=")),
                                           Base64::Decode(std::string("pvZgZVsP8B4=")),
                                           Base64::Decode(std::string("kkwd3WR8xRo=")),
                                           Base64::Decode(std::string("hiPihQVcC3E=")),
                                           Base64::Decode(std::string("pw~NRAhRnvM="))};

  std::vector<X25519::shrkey_t> exp_keys{
      Base64::Decode(std::string("s2pYZqn-uJouvR-ZPiKmJeZrCxjvjhgJ-ox74AvX-6w=")),
      Base64::Decode(std::string("PX6c3Awn0--e~PdYT~Tlhf2OgBYbMe0ia30rtYi2aqw=")),
      Base64::Decode(std::string("DNTokRiiIwDUeVvZ0ffAm6xrQo7RR5NcIxktVwt71ok=")),
      Base64::Decode(std::string("lm1ACcC~YULRajHwZbgQ-b3z7naKVt4-t2xQ0pQ~sVQ=")),
      Base64::Decode(std::string("i7ZWlP-Hdv3Qm0WHqKR7IKf72r0rMssHunpErIvVJGI="))};

  X25519::shrkey_t tag_ck, sym_ck;
  std::tie(tag_ck, sym_ck) = dh_state.Ratchet(root_key, sess_key);

  TagState tag_state(tag_ck);
  SymmetricState sym_state(sym_ck);

  std::uint8_t tag_idx(0);
  const auto& tags = tag_state.tags();

  for (const auto& tag : exp_tags)
    REQUIRE(tags.at(tag_idx++) == tag);

  std::uint8_t key_idx(0);
  const auto& keys = sym_state.keys();

  for (const auto& key : exp_keys)
    REQUIRE(keys.at(key_idx++) == key);
}
