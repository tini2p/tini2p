/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <iostream>

#include <catch2/catch.hpp>

#include "src/ecies/message_sections.h"

using tini2p::data::NewDHKeyBlock;

using tini2p::ecies::SectionKeyInfo;
using tini2p::ecies::EphemeralKeySection;
using tini2p::ecies::StaticKeySection;
using tini2p::ecies::PayloadSection;
using tini2p::ecies::ReplyPayloadSection;

TEST_CASE("SectionKeyInfo has valid initial chain key and h", "[ecies_sections]")
{
  SectionKeyInfo::chain_key_t chain_key;
  SectionKeyInfo::h_t h;

  REQUIRE_NOTHROW(std::tie(chain_key, h) = SectionKeyInfo::InitializeChainKey());
  REQUIRE(chain_key == tini2p::ecies::ECIES_INITIAL_CHAIN_KEY);
  REQUIRE(h == tini2p::ecies::ECIES_INITIAL_H);
}

/*----------------------------\
| Ephemeral Key Section Tests |
\----------------------------*/

struct EphemeralKeySectionFixture
{
  EphemeralKeySectionFixture() : key(tini2p::crypto::Elligator2::create_keys().pubkey) {}

  EphemeralKeySection::key_t null_key, key;
  EphemeralKeySection ep_section;
  std::unique_ptr<EphemeralKeySection> ep_ptr;
};

TEST_CASE_METHOD(EphemeralKeySectionFixture, "EphemeralKeySection has valid fields", "[ecies]")
{
  REQUIRE(ep_section.key() == null_key);
  // must set non-null key before serializing
  REQUIRE_THROWS(ep_section.serialize());
}

TEST_CASE_METHOD(EphemeralKeySectionFixture, "EphemeralKeySection sets valid fields", "[ecies]")
{
  REQUIRE_NOTHROW(ep_section.key(key));
  REQUIRE(ep_section.key() == key);
}

TEST_CASE_METHOD(EphemeralKeySectionFixture, "EphemeralKeySection can be fully-initialized", "[ecies]")
{
  REQUIRE_NOTHROW(ep_ptr = std::make_unique<EphemeralKeySection>());
  REQUIRE_NOTHROW(ep_ptr->key(key));
  REQUIRE(ep_ptr->key() == key);
}

TEST_CASE_METHOD(EphemeralKeySectionFixture, "EphemeralKeySection serializes and deserializes from buffer", "[ecies]")
{
  // must set non-null key before serializing
  REQUIRE_NOTHROW(ep_section.key(key));
  REQUIRE_NOTHROW(ep_section.serialize());
  REQUIRE_NOTHROW(ep_section.deserialize());
  REQUIRE(ep_section.key() == key);

  REQUIRE_NOTHROW(ep_ptr = std::make_unique<EphemeralKeySection>(ep_section.buffer()));
  REQUIRE(ep_section == *ep_ptr);
}

TEST_CASE_METHOD(EphemeralKeySectionFixture, "EphemeralKeySection rejects setting null key", "[ecies]")
{
  REQUIRE_THROWS(ep_section.key(null_key));
}

TEST_CASE_METHOD(EphemeralKeySectionFixture, "EphemeralKeySection rejects deserializing invalid buffer", "[ecies]")
{
  REQUIRE_THROWS(ep_section.deserialize());
  REQUIRE_THROWS(EphemeralKeySection(ep_section.buffer()));
}

/*----------------------------\
| Static Key Section Tests    |
\----------------------------*/

struct StaticKeySectionFixture
{
  StaticKeySectionFixture() : key(tini2p::crypto::RandBytes<32>()), fk_section()
  {
  }

  StaticKeySection::key_t null_key, key;
  StaticKeySection fk_section;
  std::unique_ptr<StaticKeySection> fk_ptr;
};

TEST_CASE_METHOD(StaticKeySectionFixture, "StaticKeySection has valid fields", "[ecies]")
{
  // static key zeroed for a OneTime/Unbound message
  REQUIRE(fk_section.key() == null_key);
}

TEST_CASE_METHOD(StaticKeySectionFixture, "StaticKeySection sets valid fields", "[ecies]")
{
  // set the key and StaticKey flag
  REQUIRE_NOTHROW(fk_section.key(key));
  REQUIRE(fk_section.key() == key);
}

TEST_CASE_METHOD(StaticKeySectionFixture, "StaticKeySection can be fully initialized", "[ecies]")
{
  // Create a FlagsKey section with a static key (bound)
  REQUIRE_NOTHROW(fk_ptr = std::make_unique<StaticKeySection>(key));
  REQUIRE(fk_ptr->key() == key);
}

TEST_CASE_METHOD(StaticKeySectionFixture, "StaticKeySection serializes and deserializes from buffer", "[ecies]")
{
  REQUIRE_NOTHROW(fk_section.serialize());
  REQUIRE_NOTHROW(fk_section.deserialize());

  // create new section, deserializing from buffer (OneTime/Unbound)
  REQUIRE_NOTHROW(fk_ptr = std::make_unique<StaticKeySection>(fk_section.buffer()));
  REQUIRE(fk_section == *fk_ptr);

  // set the static key
  REQUIRE_NOTHROW(fk_section.key(key));
  REQUIRE(fk_section.key() == key);

  // create new section, deserializing from buffer (Bound)
  REQUIRE_NOTHROW(fk_ptr = std::make_unique<StaticKeySection>(fk_section.buffer()));
  REQUIRE(fk_section == *fk_ptr);
}

/*----------------------------\
| Payload Section Tests       |
\----------------------------*/

struct PayloadSectionFixture
{
  PayloadSectionFixture() : pay_section() {}

  PayloadSection pay_section;
};

TEST_CASE_METHOD(PayloadSectionFixture, "PayloadSection has valid fields", "[ecies]")
{
  // ensure blocks are in required order, see spec
  REQUIRE(pay_section.has_block<tini2p::data::DateTimeBlock>());
  REQUIRE(pay_section.has_block<tini2p::data::PaddingBlock>());
}

TEST_CASE_METHOD(PayloadSectionFixture, "PayloadSection sets valid fields", "[ecies]")
{
  const PayloadSection::payload_blocks_t blocks{{tini2p::data::DateTimeBlock()},
                                                {tini2p::data::ECIESOptionsBlock()},
                                                {tini2p::data::GarlicBlock()},
                                                {tini2p::data::PaddingBlock(0x42)}};
  REQUIRE_NOTHROW(pay_section.blocks(blocks));

  for (const auto& block : blocks)
    {
      std::visit(
          [this](const auto& b) {
            using block_type = std::decay_t<decltype(b)>;

            REQUIRE(pay_section.has_block<block_type>());
            REQUIRE(pay_section.get_block<block_type>() == b);
          },
          block);
    }
}

TEST_CASE_METHOD(PayloadSectionFixture, "PayloadSection can be created from an Garlic block", "[ecies]")
{
  std::unique_ptr<PayloadSection> pay_ptr;
  tini2p::data::GarlicBlock garlic_block;

  REQUIRE_NOTHROW(pay_ptr = std::make_unique<PayloadSection>(garlic_block));

  REQUIRE(pay_ptr->has_block<tini2p::data::GarlicBlock>());
  REQUIRE(pay_ptr->get_block<tini2p::data::GarlicBlock>() == garlic_block);

  // Padding block added automatically
  REQUIRE(pay_ptr->has_block<tini2p::data::PaddingBlock>());
}

TEST_CASE_METHOD(PayloadSectionFixture, "PayloadSection can add an Garlic, ECIES Options or Padding block", "[ecies]")
{
  tini2p::data::GarlicBlock garlic_block;

  REQUIRE_NOTHROW(pay_section.add_block(garlic_block));

  // Garlic block is the first additional block
  REQUIRE(pay_section.has_block<tini2p::data::GarlicBlock>());
  REQUIRE(pay_section.get_block<tini2p::data::GarlicBlock>() == garlic_block);

  // can remove Padding block
  REQUIRE_NOTHROW(pay_section.remove_padding());

  // can add Padding block if none present
  tini2p::data::PaddingBlock pad_block(0x42);
  REQUIRE_NOTHROW(pay_section.add_block(pad_block));

  // rejects adding Padding block if one is present
  REQUIRE_THROWS(pay_section.add_block(pad_block));

  // First padding block added is now the last block
  REQUIRE(pay_section.has_block<tini2p::data::PaddingBlock>());
  REQUIRE(pay_section.get_block<tini2p::data::PaddingBlock>() == pad_block);

  // can add multiple Garlic blocks
  REQUIRE_NOTHROW(pay_section.add_block(garlic_block));

  // remove the padding block
  REQUIRE_NOTHROW(pay_section.remove_padding());
  REQUIRE(!pay_section.has_block<tini2p::data::PaddingBlock>());
}

TEST_CASE_METHOD(PayloadSectionFixture, "PayloadSection serializes and deserializes from buffer", "[ecies]")
{
  REQUIRE_NOTHROW(pay_section.serialize());
  REQUIRE_NOTHROW(pay_section.deserialize());

  std::unique_ptr<PayloadSection> pay_ptr;
  REQUIRE_NOTHROW(pay_ptr = std::make_unique<PayloadSection>(pay_section.buffer()));
  REQUIRE((*pay_ptr == pay_section));
}

TEST_CASE_METHOD(PayloadSectionFixture, "PayloadSection rejects setting invalid blocks", "[ecies]")
{
  // rejects empty blocks
  REQUIRE_THROWS(pay_section.blocks({}));
  
  // rejects undersized blocks
  PayloadSection::payload_blocks_t blocks{};
  REQUIRE_THROWS(pay_section.blocks(blocks));

  // rejects out-of-order blocks
  blocks.emplace_back(tini2p::data::ECIESOptionsBlock());
  REQUIRE_THROWS(pay_section.blocks(blocks));

  // rejects Garlic & Padding blocks in required blocks
  blocks.pop_back();
  blocks.emplace_back(tini2p::data::GarlicBlock());
  REQUIRE_THROWS(pay_section.blocks(blocks));

  blocks.pop_back();
  blocks.emplace_back(tini2p::data::PaddingBlock(0x42));
  REQUIRE_THROWS(pay_section.blocks(blocks));

  // create valid required blocks
  blocks.pop_back();
  blocks.emplace_back(tini2p::data::DateTimeBlock());
  REQUIRE_NOTHROW(pay_section.blocks(blocks));

  // rejects invalid additional blocks
  // only Garlic, Options & Padding blocks allowed
  blocks.emplace_back(tini2p::data::DateTimeBlock());
  REQUIRE_THROWS(pay_section.blocks(blocks));

  // rejects multiple padding blocks
  blocks.pop_back();
  blocks.emplace_back(tini2p::data::PaddingBlock(0x42));
  blocks.emplace_back(tini2p::data::PaddingBlock(0x42));
  REQUIRE_THROWS(pay_section.blocks(blocks));

  // Padding block must be last
  blocks.pop_back();
  blocks.emplace_back(tini2p::data::GarlicBlock());
  REQUIRE_THROWS(pay_section.blocks(blocks));
}

TEST_CASE_METHOD(PayloadSectionFixture, "PayloadSection rejects deserializing invalid required blocks", "[ecies]")
{
  // ensure valid starting state
  REQUIRE_NOTHROW(pay_section.deserialize());

  tini2p::data::DateTimeBlock date_block;
  tini2p::data::ECIESOptionsBlock opt_block;

  tini2p::BytesWriter<PayloadSection::buffer_t> writer(pay_section.buffer());

  // write out-of-order blocks
  writer.write_data(opt_block.buffer());
  writer.write_data(date_block.buffer());

  // rejects out-of-order 
  REQUIRE_THROWS(pay_section.deserialize());
}

TEST_CASE_METHOD(PayloadSectionFixture, "PayloadSection rejects deserializing invalid additional blocks", "[ecies]")
{
  tini2p::data::DateTimeBlock date_block;
  tini2p::data::ECIESOptionsBlock opt_block;
  tini2p::data::GarlicBlock garlic_block;
  tini2p::data::PaddingBlock pad_block(0x42);

  tini2p::BytesWriter<PayloadSection::buffer_t> writer(pay_section.buffer());

  // rejects pad block in required blocks
  writer.write_data(pad_block.buffer());
  REQUIRE_THROWS(pay_section.deserialize());

  // rejects Garlic block in required blocks
  writer.skip_back(pad_block.size());
  writer.write_data(garlic_block.buffer());
  REQUIRE_THROWS(pay_section.deserialize());

  // create buffer with enough size for valid required blocks, and bad additional blocks
  const auto& full_len = date_block.size() + opt_block.size() + (pad_block.size() * 2);
  PayloadSection::buffer_t bad_buf(full_len);

  tini2p::BytesWriter<PayloadSection::buffer_t> bad_writer(bad_buf);

  // write valid required blocks, ensure valid state
  bad_writer.write_data(date_block.buffer());
  bad_writer.write_data(opt_block.buffer());
  bad_buf.resize(bad_writer.count());
  REQUIRE_NOTHROW(PayloadSection(bad_buf));

  // rejects additional blocks that aren't Garlic or pad blocks
  // rejects DateTime block
  bad_writer.skip_back(opt_block.size());
  bad_writer.write_data(date_block.buffer());
  bad_buf.resize(bad_writer.count());
  REQUIRE_THROWS(PayloadSection(bad_buf));

  // rejects multiple pad blocks
  bad_buf.resize(full_len);
  bad_writer.write_data(pad_block.buffer());
  bad_writer.write_data(pad_block.buffer());
  REQUIRE_THROWS(PayloadSection(bad_buf));

  // rejects Garlic block following pad block
  bad_writer.skip_back(pad_block.size());
  bad_writer.write_data(garlic_block.buffer());
  REQUIRE_THROWS(PayloadSection(bad_buf));
}

/*----------------------------\
| Reply Payload Section Tests |
\----------------------------*/

struct ReplyPayloadSectionFixture
{
  ReplyPayloadSectionFixture()
  {
    repay_ptr = std::make_unique<ReplyPayloadSection>();
  }

  std::unique_ptr<ReplyPayloadSection> repay_ptr;
};

TEST_CASE_METHOD(ReplyPayloadSectionFixture, "ReplyPayloadSection has valid fields", "[ecies]")
{
  REQUIRE(repay_ptr->has_block<tini2p::data::DateTimeBlock>());
  REQUIRE(repay_ptr->has_block<tini2p::data::ECIESOptionsBlock>());
}

TEST_CASE_METHOD(ReplyPayloadSectionFixture, "ReplyPayloadSection sets valid fields", "[ecies]")
{
  const ReplyPayloadSection::payload_blocks_t blocks{{tini2p::data::DateTimeBlock()},
                                                     {tini2p::data::ECIESOptionsBlock()},
                                                     {tini2p::data::GarlicBlock()},
                                                     {tini2p::data::PaddingBlock(0x42)}};
  REQUIRE_NOTHROW(repay_ptr->blocks(blocks));

  for (const auto& block : blocks)
    {
      std::visit(
          [this](const auto& b) {
            using block_type = std::decay_t<decltype(b)>;

            REQUIRE(repay_ptr->has_block<block_type>());
            REQUIRE(repay_ptr->get_block<block_type>() == b);
          },
          block);
    }
}

TEST_CASE_METHOD(
    ReplyPayloadSectionFixture,
    "ReplyPayloadSection can be created from a MessageNumber and Garlic block",
    "[ecies]")
{
  std::unique_ptr<ReplyPayloadSection> pay_ptr;
  tini2p::data::GarlicBlock garlic_block;

  REQUIRE_NOTHROW(pay_ptr.reset(new ReplyPayloadSection(garlic_block)));
}

TEST_CASE_METHOD(ReplyPayloadSectionFixture, "ReplyPayloadSection can add an Garlic or Padding block", "[ecies]")
{
  tini2p::data::GarlicBlock garlic_block;

  REQUIRE_NOTHROW(repay_ptr->add_block(garlic_block));

  // can add multiple Garlic blocks
  REQUIRE_NOTHROW(repay_ptr->add_block(garlic_block));
  REQUIRE(repay_ptr->has_block<tini2p::data::GarlicBlock>());
  REQUIRE(repay_ptr->get_block<tini2p::data::GarlicBlock>() == garlic_block);

  // rejects adding Padding block if one is present
  tini2p::data::PaddingBlock pad_block(0x42);
  REQUIRE_NOTHROW(repay_ptr->add_block(pad_block));
  REQUIRE_THROWS(repay_ptr->add_block(pad_block));

  // can remove Padding block
  REQUIRE_NOTHROW(repay_ptr->remove_padding());

  // Garlic block is now the last block
  REQUIRE(repay_ptr->has_block<tini2p::data::GarlicBlock>());
  REQUIRE(repay_ptr->get_block<tini2p::data::GarlicBlock>() == garlic_block);

  // can add Padding block if none present
  REQUIRE_NOTHROW(repay_ptr->add_block(pad_block));
  REQUIRE(repay_ptr->has_block<tini2p::data::PaddingBlock>());
  REQUIRE(repay_ptr->get_block<tini2p::data::PaddingBlock>() == pad_block);
}

TEST_CASE_METHOD(ReplyPayloadSectionFixture, "ReplyPayloadSection serializes and deserializes from buffer", "[ecies]")
{
  REQUIRE_NOTHROW(repay_ptr->serialize());
  REQUIRE_NOTHROW(repay_ptr->deserialize());
  REQUIRE_NOTHROW(ReplyPayloadSection(repay_ptr->buffer()));
}

TEST_CASE_METHOD(ReplyPayloadSectionFixture, "ReplyPayloadSection rejects setting invalid blocks", "[ecies]")
{
  // rejects empty blocks
  REQUIRE_THROWS(repay_ptr->blocks({}));
  
  // rejects undersized blocks
  ReplyPayloadSection::payload_blocks_t blocks{};
  REQUIRE_THROWS(repay_ptr->blocks(blocks));

  // rejects out-of-order blocks
  blocks.emplace_back(tini2p::data::ECIESOptionsBlock());
  blocks.emplace_back(tini2p::data::DateTimeBlock());
  REQUIRE_THROWS(repay_ptr->blocks(blocks));

  // rejects Garlic & Padding blocks in required blocks
  blocks.pop_back();
  blocks.emplace_back(tini2p::data::GarlicBlock());
  REQUIRE_THROWS(repay_ptr->blocks(blocks));

  blocks.pop_back();
  blocks.emplace_back(tini2p::data::PaddingBlock(0x42));
  REQUIRE_THROWS(repay_ptr->blocks(blocks));

  // create valid required blocks
  blocks.clear();
  blocks.emplace_back(tini2p::data::DateTimeBlock());
  blocks.emplace_back(tini2p::data::ECIESOptionsBlock());
  REQUIRE_NOTHROW(repay_ptr->blocks(blocks));

  // rejects invalid additional blocks
  // only Garlic & Padding blocks allowed
  blocks.emplace_back(tini2p::data::DateTimeBlock());
  REQUIRE_THROWS(repay_ptr->blocks(blocks));

  blocks.clear();
  blocks.emplace_back(tini2p::data::ECIESOptionsBlock());
  REQUIRE_THROWS(repay_ptr->blocks(blocks));

  // rejects multiple padding blocks
  blocks.pop_back();
  blocks.emplace_back(tini2p::data::PaddingBlock(0x42));
  blocks.emplace_back(tini2p::data::PaddingBlock(0x42));
  REQUIRE_THROWS(repay_ptr->blocks(blocks));

  // Garlic block must be last
  blocks.pop_back();
  blocks.emplace_back(tini2p::data::GarlicBlock());
  REQUIRE_THROWS(repay_ptr->blocks(blocks));
}

TEST_CASE_METHOD(ReplyPayloadSectionFixture, "ReplyPayloadSection rejects deserializing invalid buffer", "[ecies]")
{
  // ensure valid starting state
  REQUIRE_NOTHROW(repay_ptr->deserialize());

  tini2p::data::DateTimeBlock date_block;
  tini2p::data::ECIESOptionsBlock opt_block;
  tini2p::data::GarlicBlock garlic_block;
  tini2p::data::PaddingBlock pad_block(0x42);

  ReplyPayloadSection::payload_blocks_t blocks{{date_block, opt_block, garlic_block, pad_block}};
  REQUIRE_NOTHROW(repay_ptr->blocks(blocks));

  tini2p::BytesWriter<ReplyPayloadSection::buffer_t> writer(repay_ptr->buffer());

  // write out-of-order blocks
  writer.write_data(opt_block.buffer());
  writer.write_data(date_block.buffer());

  // rejects out-of-order 
  REQUIRE_THROWS(repay_ptr->deserialize());

  // rewrite valid buffer
  writer.reset();
  writer.write_data(date_block.buffer());
  writer.write_data(opt_block.buffer());
  REQUIRE_NOTHROW(repay_ptr->deserialize());

  // rejects pad block in required blocks
  writer.skip_back(opt_block.size());
  writer.write_data(pad_block.buffer());
  REQUIRE_THROWS(repay_ptr->deserialize());

  // rejects Garlic block in required blocks
  writer.skip_back(pad_block.size());
  writer.write_data(garlic_block.buffer());
  REQUIRE_THROWS(repay_ptr->deserialize());

  // create buffer with enough size for valid required blocks, and bad additional blocks
  ReplyPayloadSection::buffer_t bad_buf(date_block.size() + opt_block.size() + (pad_block.size() * 2));

  tini2p::BytesWriter<ReplyPayloadSection::buffer_t> bad_writer(bad_buf);

  // write valid required blocks, ensure valid state
  bad_writer.write_data(date_block.buffer());
  bad_writer.write_data(opt_block.buffer());
  bad_buf.resize(bad_writer.count());
  REQUIRE_NOTHROW(ReplyPayloadSection(bad_buf));

  bad_buf.resize(bad_buf.size() + (pad_block.size() * 2));

  // rejects additional blocks that aren't Garlic or pad blocks
  // rejects DateTime block
  bad_writer.write_data(date_block.buffer());
  REQUIRE_THROWS(ReplyPayloadSection(bad_buf));

  // rejects ECIESOptions block
  bad_writer.skip_back(date_block.size());
  bad_writer.write_data(opt_block.buffer());
  REQUIRE_THROWS(ReplyPayloadSection(bad_buf));

  // rejects multiple pad blocks
  bad_writer.skip_back(opt_block.size());
  bad_writer.write_data(pad_block.buffer());
  bad_writer.write_data(pad_block.buffer());
  REQUIRE_THROWS(ReplyPayloadSection(bad_buf));

  // rejects Garlic block following pad block
  bad_writer.skip_back(pad_block.size());
  bad_writer.write_data(garlic_block.buffer());
  REQUIRE_THROWS(ReplyPayloadSection(bad_buf));
}
