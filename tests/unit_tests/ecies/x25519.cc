/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/ecies/x25519.h"

using tini2p::crypto::X25519;
using tini2p::crypto::Elligator2;

using tini2p::ecies::EciesX25519;

struct EciesX25519Fixture
{
  EciesX25519Fixture()
      : ecies(X25519::create_keys(), Elligator2::create_keys()),
        remote_ecies(X25519::create_keys(), Elligator2::create_keys())
  {
  }

  EciesX25519 ecies, remote_ecies;
};

TEST_CASE_METHOD(EciesX25519Fixture, "EciesX25519 has valid key lengths", "[ecies_x25519]")
{
  REQUIRE(ecies.pubkey().size() == X25519::PublicKeyLen);
  REQUIRE(EciesX25519::curve_t::pubkey_t().size() == X25519::PublicKeyLen);
  REQUIRE(EciesX25519::curve_t::pvtkey_t().size() == X25519::PrivateKeyLen);
  REQUIRE(EciesX25519::curve_t::shrkey_t().size() == X25519::SharedKeyLen);
}

TEST_CASE_METHOD(EciesX25519Fixture, "EciesX25519 encrypts + decrypts NewSessionMessage", "[ecies_x25519]")
{
  EciesX25519::new_message_t new_message;

  // Unrealistic, set Alice's remote static public key manually.
  // Realistically, Alice sets Bob's static key from the LeaseSet2.
  ecies.remote_id_key(remote_ecies.id_keys().pubkey);

  // Encrypt the message from Alice to Bob
  REQUIRE_NOTHROW(ecies.Encrypt(new_message));

  // Decrypt the message from Alice as Bob
  REQUIRE_NOTHROW(remote_ecies.Decrypt(new_message));

  auto new_message_copy = new_message;  // copy for comparison

  // Encrypt the message from Alice to Bob
  REQUIRE_NOTHROW(ecies.Encrypt(new_message));

  REQUIRE(!(new_message == new_message_copy));

  // Decrypt the message from Alice as Bob
  REQUIRE_NOTHROW(remote_ecies.Decrypt(new_message));

  REQUIRE((new_message == new_message_copy));
}

TEST_CASE_METHOD(EciesX25519Fixture, "EciesX25519 encrypts + decrypts a bound NewSessionMessage", "[bad_ecies_x25519]")
{
  EciesX25519::new_message_t new_message(ecies.id_keys().pubkey, EciesX25519::new_message_t::payload_t());

  // unrealistic, set Alice's remote static public key manually.
  // Realistically, Alice sets Bob's static key from the LeaseSet2.
  ecies.remote_id_key(remote_ecies.id_keys().pubkey);

  // Encrypt the message from Alice to Bob
  REQUIRE_NOTHROW(ecies.Encrypt(new_message));

  // Decrypt the message from Alice as Bob
  REQUIRE_NOTHROW(remote_ecies.Decrypt(new_message));

  auto new_message_copy = new_message;  // copy for comparison

  // Encrypt and decrypt again to compare the decrypted serialized buffer

  // Encrypt the message from Alice to Bob
  REQUIRE_NOTHROW(ecies.Encrypt(new_message));

  REQUIRE(!(new_message == new_message_copy));

  // Decrypt the message from Alice as Bob
  REQUIRE_NOTHROW(remote_ecies.Decrypt(new_message));

  REQUIRE((new_message == new_message_copy));
}

TEST_CASE_METHOD(EciesX25519Fixture, "EciesX25519 encrypts + decrypts NewSessionReplyMessage", "[ecies_x25519]")
{
  EciesX25519::new_reply_t::payload_t reply_pay;

  // Create random reply tag, unrealistic
  // Normally, the reply tag is derived from Alice's original NewSessionMessage
  EciesX25519::new_reply_t::tag_t reply_tag;
  tini2p::crypto::RandBytes(reply_tag);

  // Create NewSession reply message w/ Bob's init + ephemeral public keys and reply payload
  EciesX25519::new_reply_t new_reply(reply_tag, reply_pay);

  // Unrealistic, set Alice's static + ephemeral public key manually in Bob's ECIES state.
  // Realistically, Bob sets Alice's keys from the the initiating NewSessionMessage.
  remote_ecies.remote_id_key(ecies.id_keys().pubkey);
  remote_ecies.remote_ep_key(ecies.ep_keys().pubkey);

  // Simulate the chain key and chain hash from NewSessionMessage processing, unrealistic
  REQUIRE_NOTHROW(remote_ecies.chain_key(tini2p::crypto::RandBytes<32>()).h(tini2p::crypto::RandBytes<32>()));

  const auto chain_key = remote_ecies.chain_key();
  const auto h = remote_ecies.h();

  // Encrypt the NewSession reply as Bob
  REQUIRE_NOTHROW(remote_ecies.Encrypt(new_reply));

  // Simulate the chain key and chain hash from NewSessionMessage processing, unrealistic
  REQUIRE_NOTHROW(ecies.chain_key(chain_key).h(h));

  // Decrypt the NewSession reply as Alice
  REQUIRE_NOTHROW(ecies.Decrypt(new_reply));

  auto new_reply_copy = new_reply;  // copy for comparison

  // ensure Alice now has Bob's ephemeral public key
  REQUIRE(ecies.remote_ep_key() == remote_ecies.ep_keys().pubkey);

  // Encrypt and decrypt again to compare the decrypted serialized buffer

  // Encrypt the NewSession reply as Bob
  REQUIRE_NOTHROW(remote_ecies.Encrypt(new_reply));

  // Decrypt the NewSession reply as Alice
  REQUIRE_NOTHROW(ecies.Decrypt(new_reply));

  // ensure Alice succesfully decrypts the NewSession message
  REQUIRE((new_reply == new_reply_copy));
}

TEST_CASE_METHOD(
    EciesX25519Fixture,
    "EciesX25519 encrypts + decrypts ExistingSessionMessage when remote public keys are set",
    "[ecies_x25519]")
{
  // Manually set remote keys (unrealistic).
  // Realistically performed during NewSessionMessage processing.
  REQUIRE_NOTHROW(ecies.remote_id_key(remote_ecies.id_keys().pubkey));
  REQUIRE_NOTHROW(ecies.remote_ep_key(remote_ecies.ep_keys().pubkey));
  REQUIRE_NOTHROW(remote_ecies.remote_rekey(ecies.id_keys().pubkey, ecies.ep_keys().pubkey));

  // Perform initial ratchets (unrealistic).
  // Realistically performed during NewSessionMessage processing.
  REQUIRE_NOTHROW(ecies.DHRatchet<EciesX25519::initiator_r>());
  REQUIRE_NOTHROW(ecies.CheckRatchets());

  // Create empty ExistingSessionMessage w/ random session tag
  EciesX25519::existing_message_t existing_msg(ecies.tag(), {});

  const auto existing_msg_copy = existing_msg;  // copy for comparison

  REQUIRE_NOTHROW(ecies.Encrypt(existing_msg));
  REQUIRE(!(existing_msg.buffer() == existing_msg_copy.buffer()));

  // Perform initial ratchets (unrealistic).
  // Realistically performed during NewSessionMessage processing.
  REQUIRE_NOTHROW(remote_ecies.DHRatchet<EciesX25519::responder_r>());
  REQUIRE_NOTHROW(remote_ecies.CheckRatchets());

  REQUIRE_NOTHROW(remote_ecies.Decrypt(existing_msg));
  REQUIRE((existing_msg == existing_msg_copy));
}

TEST_CASE_METHOD(EciesX25519Fixture, "EciesX25519 handles trial decryption of NewSessionMessage", "[ecies_x25519]")
{
  tini2p::ecies::ReplyPayloadSection reply_pay;

  // Create random reply tag, unrealistic
  // Normally, the reply tag is derived from Alice's original NewSessionMessage
  EciesX25519::new_reply_t::tag_t reply_tag;
  tini2p::crypto::RandBytes(reply_tag);

  // Create NewSession reply message w/ Bob's init + ephemeral public keys and reply payload
  EciesX25519::new_reply_t new_reply(reply_tag, reply_pay);

  // Unrealistic, set Alice's static + ephemeral public key manually in Bob's ECIES state.
  // Realistically, Bob sets Alice's keys from the the initiating NewSessionMessage.
  REQUIRE_NOTHROW(remote_ecies.remote_id_key(ecies.id_keys().pubkey));
  REQUIRE_NOTHROW(remote_ecies.remote_ep_key(ecies.ep_keys().pubkey));

  // Simulate the chain key and chain hash from NewSessionMessage processing, unrealistic
  REQUIRE_NOTHROW(remote_ecies.chain_key(tini2p::crypto::RandBytes<32>()).h(tini2p::crypto::RandBytes<32>()));

  const auto chain_key = remote_ecies.chain_key();
  const auto h = remote_ecies.h();

  // Encrypt NewSessionMessage as Bob
  REQUIRE_NOTHROW(remote_ecies.Encrypt(new_reply));

  // Simulate the chain key and chain hash from NewSessionMessage processing, unrealistic
  REQUIRE_NOTHROW(ecies.chain_key(chain_key).h(h));

  // Decrypt NewSessionMessage as Alice
  REQUIRE_NOTHROW(ecies.Decrypt(new_reply));

  // Re-Encrypt NewSessionMessage as Bob, with a different ID key for Alice
  EciesX25519::curve_t::pubkey_t new_id_key;
  tini2p::crypto::RandBytes(new_id_key);
  REQUIRE_NOTHROW(remote_ecies.remote_id_key(new_id_key));
  REQUIRE_NOTHROW(remote_ecies.Encrypt(new_reply));

  // Ensure decrypting NewSessionMessage as Alice fails
  REQUIRE_THROWS(ecies.Decrypt(new_reply));
}
