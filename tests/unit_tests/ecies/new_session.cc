/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/ecies/new_session.h"

using tini2p::crypto::HmacSha256;

using tini2p::ecies::EciesInitiator;
using tini2p::ecies::EciesResponder;
using tini2p::ecies::Keys;
using tini2p::ecies::EphemeralKeySection;
using tini2p::ecies::StaticKeySection;
using tini2p::ecies::PayloadSection;
using tini2p::ecies::NewSessionMessage;

struct NewSessionFixture
{
  NewSessionFixture()
      : ecies_keys(Keys::curve_t::create_keys(), Keys::elligator_t::create_keys()),
        remote_ecies_keys(Keys::curve_t::create_keys(), Keys::elligator_t::create_keys()),
        local_id_keys(ecies_keys.id_keys()),
        remote_id_keys(remote_ecies_keys.id_keys()),
        local_ep_keys(ecies_keys.ep_keys()),
        remote_ep_keys(remote_ecies_keys.ep_keys()),
        new_msg()
  {
    // Set Alice's remote key manually (unrealistic)
    // Realistically, Alice sets remote static (id) key from LeaseSet2.
    REQUIRE_NOTHROW(ecies_keys.remote_id_key(remote_id_keys.pubkey));
  }

  NewSessionMessage::ecies_keys_t ecies_keys, remote_ecies_keys;
  NewSessionMessage::ecies_keys_t::curve_t::keypair_t local_id_keys, remote_id_keys;
  NewSessionMessage::ecies_keys_t::curve_t::keypair_t local_ep_keys, remote_ep_keys;
  NewSessionMessage new_msg;
};

TEST_CASE_METHOD(NewSessionFixture, "NewSessionMessage processes a StaticKeySection", "[new_session]")
{
  // create mock ephemeral key section
  auto ep_keys = EphemeralKeySection::elligator_t::create_keys();
  EphemeralKeySection ep_section;
  ep_section.key(ep_keys.pubkey);

  // Convert the Elligator2 keys to X25519 keys
  ecies_keys.ep_keys(std::move(ep_keys));

  // Set Bob's remote ephemeral key manually (unrealistic)
  // Realistically, Bob sets remote ephemeral key processing NewSessionMessage.
  remote_ecies_keys.remote_ep_key(ecies_keys.ep_keys().pubkey);

  // create static key section
  StaticKeySection fk_section(local_id_keys.pubkey);
  const auto fk_copy = fk_section;  // copy for comparison

  // encrypt the FlagsKey section as Alice
  REQUIRE_NOTHROW(new_msg.ProcessStaticKeySection<EciesInitiator>(fk_section, ecies_keys));
  // section buffer should no longer match the copy
  REQUIRE(!(fk_section.buffer() == fk_copy.buffer()));

  // decrypt the FlagsKey section as Bob
  REQUIRE_NOTHROW(new_msg.ProcessStaticKeySection<EciesResponder>(fk_section, remote_ecies_keys));
  // section buffer should now match the copy again
  REQUIRE(fk_section.buffer() == fk_copy.buffer());

  // remote ECIES (Bob) should now have the local ECIES (Alice) static public key
  REQUIRE(remote_ecies_keys.remote_id_key() == local_id_keys.pubkey);
}

TEST_CASE_METHOD(NewSessionFixture, "NewSessionMessage processes a PayloadSection", "[new_session]")
{
  // Set Bob's remote keys manually (unrealistic)
  // Realistically, Bob sets remote ephemeral and static keys processing NewSessionMessage.
  REQUIRE_NOTHROW(remote_ecies_keys.remote_rekey(local_id_keys.pubkey, local_ep_keys.pubkey));

  // create mock static key section
  StaticKeySection fk_section(local_id_keys.pubkey);

  // create payload section
  // performs DH ratchet b/w local (Alice) reusable ephemeral private key, and remote (Bob) static public key
  // DH root key is used as the key in the NewDHKey block
  NewSessionMessage::payload_t pay_section;
  const auto& pay_buf = pay_section.buffer();
  const auto copy_buf = pay_buf;

  // Simulate resulting chain key and chain hash from FlagsKey section
  ecies_keys.chain_key(tini2p::crypto::RandBytes<32>()).h(tini2p::crypto::RandBytes<32>());
  const auto chain_key = ecies_keys.chain_key();
  const auto h = ecies_keys.h();

  // encrypt the payload section as Alice
  REQUIRE_NOTHROW(new_msg.ProcessPayloadSection<EciesInitiator>(pay_section, fk_section, ecies_keys));

  // section buffer should no longer match the copy
  REQUIRE(!(pay_buf == copy_buf));

  // Simulate resulting chain key and chain hash from FlagsKey section
  remote_ecies_keys.chain_key(chain_key).h(h);

  // decrypt the payload section as Bob
  REQUIRE_NOTHROW(new_msg.ProcessPayloadSection<EciesResponder>(pay_section, fk_section, remote_ecies_keys));

  // section buffer should now match the copy again
  REQUIRE(pay_buf == copy_buf);
}

TEST_CASE_METHOD(NewSessionFixture, "NewSessionMessage serializes and deserializes from a buffer", "[new_session]")
{
  new_msg.static_section().key(ecies_keys.id_keys().pubkey);

  auto& new_buf = new_msg.buffer();
  auto new_copy = new_buf;

  // Encrypt and serialize the NewSessionReply message to buffer
  REQUIRE_NOTHROW(new_msg.serialize(ecies_keys));

  // message buffer should no longer match the copy
  REQUIRE(!(new_buf == new_copy));

  // Deserialize and decrypt the NewSessionReply message from buffer
  REQUIRE_NOTHROW(new_msg.deserialize(remote_ecies_keys));

  new_copy = new_buf;

  // Encrypt and serialize the NewSessionReply message to buffer
  REQUIRE_NOTHROW(new_msg.serialize(ecies_keys));

  // message buffer should no longer match the copy
  //REQUIRE(!(new_buf == new_copy));

  // Deserialize and decrypt the NewSessionReply message from buffer
  REQUIRE_NOTHROW(new_msg.deserialize(remote_ecies_keys));

  REQUIRE(new_buf == new_copy);

  // Check that Bob's remote ephemeral key matches Alice's ephemeral public key
  REQUIRE(remote_ecies_keys.remote_ep_key() == ecies_keys.ep_keys().pubkey);
}
