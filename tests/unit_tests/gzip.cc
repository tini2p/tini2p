/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/gzip.h"

#include <iostream>
using Catch::Matchers::Equals;
using vec = std::vector<std::uint8_t>;

TEST_CASE("Gzip compresses a buffer", "[gzip]")
{
  vec in(tini2p::Gzip::MaxLen);
  vec out;

  REQUIRE_NOTHROW(tini2p::Gzip::Compress(in.data(), tini2p::Gzip::MaxLen, out));
  REQUIRE(out.size() < tini2p::Gzip::MaxLen);
  REQUIRE(out.size() > tini2p::Gzip::MinLen);
}

TEST_CASE("Gzip decompresses a buffer", "[gzip]")
{
  vec in{{0x78, 0x5e, 0x63, 0x60, 0xa0, 0x0c, 0x00, 0x00, 0x00, 0x40, 0x00, 0x01}};
  vec out;
  vec exp_out(64);  // 64 zeros

  REQUIRE_NOTHROW(tini2p::Gzip::Decompress(in.data(), tini2p::Gzip::MaxLen, out));
  REQUIRE(out.size() <= tini2p::Gzip::MaxLen);
  REQUIRE(out.size() > tini2p::Gzip::MinLen);
  REQUIRE_THAT(out, Equals(exp_out));
}

TEST_CASE("Gzip rejects null buffers", "[gzip]")
{
  vec out{};

  REQUIRE_THROWS(tini2p::Gzip::Compress(nullptr, tini2p::Gzip::MinLen, out));
  REQUIRE_THROWS(tini2p::Gzip::Compress(out.data(), 0, out));

  REQUIRE_THROWS(tini2p::Gzip::Decompress(nullptr, tini2p::Gzip::MinLen, out));
  REQUIRE_THROWS(tini2p::Gzip::Decompress(out.data(), 0, out));
}

TEST_CASE("Gzip rejects oversize buffers", "[gzip]")
{
  vec out{};

  REQUIRE_THROWS(tini2p::Gzip::Compress(out.data(), tini2p::Gzip::MaxLen + 1, out));
  REQUIRE_THROWS(tini2p::Gzip::Decompress(out.data(), tini2p::Gzip::MaxLen + 1, out));
}
