/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/i2np/tunnel_gateway.h"

namespace crypto = tini2p::crypto;
namespace data = tini2p::data;

using tini2p::data::TunnelGateway;

struct TunnelGatewayMsgFixture
{
  TunnelGatewayMsgFixture()
      : tunnel_id(0x42),
        i2np_data_len(23),
        i2np_header(data::I2NPHeader::Type::Data, {}, data::I2NPHeader::Mode::NTCP2),
        i2np_body(crypto::RandBuffer(i2np_data_len)),
        gateway_msg(tunnel_id, i2np_header, i2np_body)
  {
  }

  TunnelGateway::tunnel_id_t tunnel_id;
  TunnelGateway::length_t i2np_data_len;
  data::I2NPHeader i2np_header;
  crypto::SecBytes i2np_body;
  TunnelGateway gateway_msg;
};

TEST_CASE_METHOD(TunnelGatewayMsgFixture, "TunnelGateway message has valid fields", "[i2np_gw]")
{
  REQUIRE(gateway_msg.tunnel_id() == tunnel_id);
  REQUIRE(gateway_msg.size() == tini2p::under_cast(TunnelGateway::HeaderLen) + i2np_header.size() + i2np_body.size());
  REQUIRE(gateway_msg.i2np_header() == i2np_header);
  REQUIRE(gateway_msg.i2np_body() == i2np_body);
}

TEST_CASE_METHOD(TunnelGatewayMsgFixture, "TunnelGateway serializes and deserializes from buffer", "[i2np_gw]")
{
  REQUIRE_NOTHROW(gateway_msg.serialize());

  std::unique_ptr<TunnelGateway> gw_ptr;
  REQUIRE_NOTHROW(gw_ptr = std::make_unique<TunnelGateway>(gateway_msg.buffer(), gateway_msg.i2np_header().mode()));

  REQUIRE(*gw_ptr == gateway_msg);
}

TEST_CASE_METHOD(TunnelGatewayMsgFixture, "TunnelGateway rejects null tunnel ID", "[i2np_gw]")
{
  REQUIRE_THROWS(TunnelGateway(0, i2np_header, i2np_body));
  REQUIRE_THROWS(gateway_msg.tunnel_id(0));

  // Write a null tunnel ID to the message buffer
  REQUIRE_NOTHROW(tini2p::write_bytes(gateway_msg.buffer().data(), TunnelGateway::tunnel_id_t(0), tini2p::Endian::Big));

  // Check the message throws when deserializing a null tunnel ID
  REQUIRE_THROWS(gateway_msg.deserialize());
}

TEST_CASE_METHOD(TunnelGatewayMsgFixture, "TunnelGateway rejects null I2NP message", "[i2np_gw]")
{
  // Check that TunnelGateway message is not created from a zeroed I2NP block buffer
  REQUIRE_NOTHROW(i2np_body.zero());
  REQUIRE_THROWS(TunnelGateway(tunnel_id, i2np_header, i2np_body));
  REQUIRE_THROWS(gateway_msg.i2np_body(i2np_body));

  tini2p::BytesWriter<TunnelGateway::buffer_t> writer(gateway_msg.buffer());

  // Write zero length to the message buffer
  REQUIRE_NOTHROW(writer.skip_bytes(tini2p::under_cast(TunnelGateway::TunnelIDLen)));
  REQUIRE_NOTHROW(writer.write_bytes(TunnelGateway::length_t(0), tini2p::Endian::Big));

  // Check that the message throws deserializing a null I2NP block length
  REQUIRE_THROWS(gateway_msg.deserialize());

  // Write original block length back to the message buffer
  const auto& orig_block_len = gateway_msg.i2np_header().size() + gateway_msg.i2np_body().size();
  REQUIRE_NOTHROW(writer.skip_back(tini2p::under_cast(TunnelGateway::LengthLen)));
  REQUIRE_NOTHROW(writer.write_bytes(TunnelGateway::length_t(orig_block_len), tini2p::Endian::Big));

  // Zero the I2NP block
  REQUIRE_NOTHROW(i2np_body.resize(orig_block_len));
  REQUIRE_NOTHROW(i2np_body.zero());
  REQUIRE_NOTHROW(writer.write_data(i2np_body));

  // Check that deserializing the TunnelGateway message throws
  REQUIRE_THROWS(gateway_msg.deserialize());
}
