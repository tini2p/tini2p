/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/database_lookup.h"

using tini2p::data::DatabaseLookup;

struct DatabaseLookupFixture
{
  DatabaseLookupFixture() : dbl(search_key, from_key) {}

  tini2p::crypto::Sha256::digest_t search_key{{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a,
                                               0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15,
                                               0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f}};

  tini2p::crypto::Sha256::digest_t from_key{{0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a,
                                             0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35,
                                             0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f}};

  DatabaseLookup dbl;
};

TEST_CASE_METHOD(DatabaseLookupFixture, "DatabaseLookup message has valid fields", "[i2np]")
{
  REQUIRE(dbl.search_key() == search_key);
  REQUIRE(dbl.from_key() == from_key);

  DatabaseLookup::flags_t expected_flags(0);  // DirectDelivery | Unencrypted | NormalLookup

  REQUIRE(dbl.flags() == expected_flags);

  std::unique_ptr<DatabaseLookup> dbl_ptr;
  REQUIRE_NOTHROW(dbl_ptr = std::make_unique<DatabaseLookup>(search_key, from_key, 0, DatabaseLookup::LookupFlags::RI));
  REQUIRE(dbl_ptr->flags() == tini2p::under_cast(DatabaseLookup::LookupFlags::RI));

  REQUIRE_NOTHROW(dbl_ptr = std::make_unique<DatabaseLookup>(search_key, from_key, 0, DatabaseLookup::LookupFlags::LS));
  REQUIRE(dbl_ptr->flags() == tini2p::under_cast(DatabaseLookup::LookupFlags::LS));

  REQUIRE_NOTHROW(dbl_ptr = std::make_unique<DatabaseLookup>(search_key, from_key, 0, DatabaseLookup::LookupFlags::Explore));
  REQUIRE(dbl_ptr->flags() == tini2p::under_cast(DatabaseLookup::LookupFlags::Explore));
}

TEST_CASE_METHOD(DatabaseLookupFixture, "DatabaseLookup deserializes from a buffer", "[i2np]")
{
  std::unique_ptr<DatabaseLookup> dbl_des;

  REQUIRE_NOTHROW(dbl_des = std::make_unique<DatabaseLookup>(dbl.buffer()));
  REQUIRE(*dbl_des == dbl);
}

TEST_CASE_METHOD(DatabaseLookupFixture, "DatabaseLookup adds excluded peer(s)", "[i2np]")
{
  DatabaseLookup::ex_peer_key_t peer, null_peer;
  tini2p::crypto::RandBytes(peer);  // unrealistic, simulate key with random data
  const auto explore_flag = tini2p::under_cast(DatabaseLookup::LookupFlags::Explore);

  // add non-zero peer key, and check that lookup type is not exploratory
  REQUIRE_NOTHROW(dbl.add_excluded_peer(peer));

  // check that exploratory lookup flag is unset
  auto lookup_flag = dbl.flags() & explore_flag;
  REQUIRE(lookup_flag == 0);

  // add zero'ed peer
  REQUIRE_NOTHROW(dbl.add_excluded_peer(null_peer));

  // check that exploratory lookup flag is set
  lookup_flag = dbl.flags() & explore_flag;
  REQUIRE(lookup_flag == explore_flag);

  // add another non-zero peer key
  tini2p::crypto::RandBytes(peer);
  REQUIRE_NOTHROW(dbl.add_excluded_peer(peer));

  // check that exploratory lookup flag is still set
  lookup_flag = dbl.flags() & explore_flag;
  REQUIRE(lookup_flag == explore_flag);

  REQUIRE_NOTHROW(dbl.serialize());
}

TEST_CASE_METHOD(DatabaseLookupFixture, "DatabaseLookup sets encrypted data", "[i2np]")
{
  DatabaseLookup::reply_key_t key;
  DatabaseLookup::reply_tag_t tag;
  tini2p::crypto::RandBytes(key);  // unrealistic, simulate key with random data
  tini2p::crypto::RandBytes(tag);  // simulate tag with random data

  // set encrypted data
  REQUIRE_NOTHROW(dbl.set_encrypted_data(key, {tag}));

  // check encrypted flag is set
  const auto enc_flag = tini2p::under_cast(DatabaseLookup::EncryptionFlags::Encrypted);
  const auto flags = dbl.flags() & enc_flag;
  REQUIRE(flags == enc_flag);

  REQUIRE_NOTHROW(dbl.serialize());
}

TEST_CASE_METHOD(DatabaseLookupFixture, "DatabaseLookup rejects excessive excluded peers", "[i2np]")
{
  // create Max + 1 peer keys
  for (DatabaseLookup::ex_peers_size_t i = 0; i < DatabaseLookup::MaxExcludedPeers; ++i)
    REQUIRE_NOTHROW(dbl.add_excluded_peer(DatabaseLookup::ex_peer_key_t()));

  REQUIRE_THROWS(dbl.add_excluded_peer(DatabaseLookup::ex_peer_key_t()));
}

TEST_CASE_METHOD(DatabaseLookupFixture, "DatabaseLookup rejects excessive reply tags", "[i2np]")
{
  DatabaseLookup::reply_key_t key;
  tini2p::crypto::RandBytes(key);  // unrealistic, simulate key with random data

  // create Max + 1 reply tags
  std::vector<DatabaseLookup::reply_tag_t> tags(DatabaseLookup::MaxReplyTags + 1);
  for (auto& tag : tags)
    tini2p::crypto::RandBytes(tag);

  REQUIRE_THROWS(dbl.set_encrypted_data(key, tags));
}

TEST_CASE_METHOD(DatabaseLookupFixture, "DatabaseLookup rejects null reply tag(s)", "[i2np]")
{
  DatabaseLookup::reply_key_t key;
  tini2p::crypto::RandBytes(key);  // unrealistic, simulate key with random data
  const DatabaseLookup::reply_tag_t null_tag{};

  REQUIRE_THROWS(dbl.set_encrypted_data(key, {}));
  REQUIRE_THROWS(dbl.set_encrypted_data(key, {null_tag}));
}

TEST_CASE("DatabaseLookup rejects invalid buffer", "[i2np]")
{
  DatabaseLookup::buffer_t buf;
  REQUIRE_THROWS(DatabaseLookup(nullptr, DatabaseLookup::MinLen));
  REQUIRE_THROWS(DatabaseLookup(buf.data(), DatabaseLookup::MinLen - 1));

  buf.resize(DatabaseLookup::MaxLen + 1);
  REQUIRE_THROWS(DatabaseLookup(buf.data(), buf.size()));
}
