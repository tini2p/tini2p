/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/tunnel_delivery.h"

using namespace tini2p::crypto;

using FirstDelivery = tini2p::data::FirstDelivery;
using FirstFlags = FirstDelivery::Flags;
using Mode = FirstDelivery::Mode;
using Layer = tini2p::data::Layer;

struct FirstDeliveryFixture
{
  FirstDeliveryFixture()
      : tun_id(41),
        to_hash(RandBytes<FirstDelivery::ToHashLen>()),
        msg_id(42),
        min_frag_len(tini2p::under_cast(FirstDelivery::MinFragmentLen))
  {
  }

  std::optional<FirstDelivery::tunnel_id_t> tun_id;
  std::optional<FirstDelivery::to_hash_t> to_hash;
  std::optional<FirstDelivery::message_id_t> msg_id;
  FirstDelivery::size_type min_frag_len, max_frag_len;
  FirstDelivery fragment;
  std::unique_ptr<FirstDelivery> frag_ptr;
};

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery has valid fields", "[tun_delivery]")
{
  const auto& frag_flag_int = tini2p::under_cast(FirstFlags::Fragmented);

  // Default FirstDelivery unfragmented w/ local delivery instructions
  REQUIRE(fragment.flags() == FirstFlags::Local);
  REQUIRE(fragment.tunnel_id() == std::nullopt);
  REQUIRE(fragment.to_hash_opt() == std::nullopt);
  REQUIRE(fragment.message_id() == std::nullopt);
  REQUIRE(fragment.size() == FirstDelivery::UnfragLocalLen);
  REQUIRE(fragment.fragment_size() == min_frag_len);

  auto exp_flag = static_cast<FirstFlags>(tini2p::under_cast(FirstFlags::Local) | frag_flag_int);

  // Fragmented Local FirstDelivery
  REQUIRE_NOTHROW(frag_ptr = std::make_unique<FirstDelivery>(Layer::AES, Mode::Fragmented, min_frag_len, msg_id));
  REQUIRE(frag_ptr->flags() == exp_flag);
  REQUIRE(frag_ptr->tunnel_id() == std::nullopt);
  REQUIRE(frag_ptr->to_hash_opt() == std::nullopt);
  REQUIRE(frag_ptr->message_id() == msg_id);
  REQUIRE(frag_ptr->size() == FirstDelivery::FragLocalLen);
  REQUIRE(frag_ptr->fragment_size() == min_frag_len);

  // Unfragmented Router FirstDelivery
  REQUIRE_NOTHROW(frag_ptr = std::make_unique<FirstDelivery>(Layer::AES, Mode::Unfragmented, min_frag_len, *to_hash));
  REQUIRE(frag_ptr->flags() == FirstFlags::Router);
  REQUIRE(frag_ptr->tunnel_id() == std::nullopt);
  REQUIRE(frag_ptr->to_hash_opt() == to_hash);
  REQUIRE(frag_ptr->to_hash() == *to_hash);
  REQUIRE(frag_ptr->message_id() == std::nullopt);
  REQUIRE(frag_ptr->size() == FirstDelivery::UnfragRouterLen);
  REQUIRE(frag_ptr->fragment_size() == min_frag_len);

  // Fragmented Router FirstDelivery
  exp_flag = static_cast<FirstFlags>(tini2p::under_cast(FirstFlags::Router) | frag_flag_int);
  REQUIRE_NOTHROW(frag_ptr = std::make_unique<FirstDelivery>(Layer::AES, Mode::Fragmented, min_frag_len, *to_hash, msg_id));
  REQUIRE(frag_ptr->flags() == exp_flag);
  REQUIRE(frag_ptr->tunnel_id() == std::nullopt);
  REQUIRE(frag_ptr->to_hash_opt() == to_hash);
  REQUIRE(frag_ptr->to_hash() == *to_hash);
  REQUIRE(frag_ptr->message_id() == msg_id);
  REQUIRE(frag_ptr->size() == FirstDelivery::FragRouterLen);
  REQUIRE(frag_ptr->fragment_size() == min_frag_len);

  // Unfragmented Tunnel FirstDelivery
  REQUIRE_NOTHROW(frag_ptr = std::make_unique<FirstDelivery>(Layer::AES, Mode::Unfragmented, min_frag_len, *tun_id, *to_hash));
  REQUIRE(frag_ptr->flags() == FirstFlags::Tunnel);
  REQUIRE(frag_ptr->tunnel_id() == tun_id);
  REQUIRE(frag_ptr->to_hash_opt() == to_hash);
  REQUIRE(frag_ptr->to_hash() == *to_hash);
  REQUIRE(frag_ptr->message_id() == std::nullopt);
  REQUIRE(frag_ptr->size() == FirstDelivery::UnfragTunnelLen);
  REQUIRE(frag_ptr->fragment_size() == min_frag_len);

  // Fragmented Tunnel FirstDelivery
  exp_flag = static_cast<FirstFlags>(tini2p::under_cast(FirstFlags::Tunnel) | frag_flag_int);
  REQUIRE_NOTHROW(frag_ptr = std::make_unique<FirstDelivery>(Layer::AES, Mode::Fragmented, min_frag_len, *tun_id, *to_hash, msg_id));
  REQUIRE(frag_ptr->flags() == exp_flag);
  REQUIRE(frag_ptr->tunnel_id() == tun_id);
  REQUIRE(frag_ptr->to_hash_opt() == to_hash);
  REQUIRE(frag_ptr->to_hash() == *to_hash);
  REQUIRE(frag_ptr->message_id() == msg_id);
  REQUIRE(frag_ptr->size() == FirstDelivery::FragTunnelLen);
  REQUIRE(frag_ptr->fragment_size() == min_frag_len);
}

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery serializes and deserializes from buffer", "[tun_delivery]")
{
  // Serializes and deserializes default FirstDelivery
  REQUIRE_NOTHROW(fragment.serialize());
  REQUIRE_NOTHROW(frag_ptr = std::make_unique<FirstDelivery>(Layer::AES, fragment.buffer()));
  REQUIRE(*frag_ptr == fragment);

  // Serializes and deserializes fragmented tunnel FirstDelivery (contains all fields)
  REQUIRE_NOTHROW(frag_ptr = std::make_unique<FirstDelivery>(Layer::AES, Mode::Fragmented, min_frag_len, *tun_id, *to_hash, msg_id));

  const auto frag_copy = *frag_ptr;
  REQUIRE_NOTHROW(frag_ptr = std::make_unique<FirstDelivery>(Layer::AES, frag_ptr->buffer()));
  REQUIRE(*frag_ptr == frag_copy);
}

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery rejects message ID for unfragmented instructions", "[tun_delivery]")
{
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Unfragmented, min_frag_len, msg_id));
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Unfragmented, min_frag_len, *to_hash, msg_id));
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Unfragmented, min_frag_len, *tun_id, *to_hash, msg_id));
}

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery rejects fragmented instructions without message ID", "[tun_delivery]")
{
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Fragmented, min_frag_len));
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Fragmented, min_frag_len, *to_hash));
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Fragmented, min_frag_len, *tun_id, *to_hash));
}

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery rejects invalid AES fragment sizes", "[tun_delivery]")
{
  // Invalid min/max unfragmented local
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::Local, Layer::AES);
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Unfragmented, min_frag_len - 1));
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Unfragmented, max_frag_len + 1));

  // Invalid min/max unfragmented router
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::Router, Layer::AES);
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Unfragmented, min_frag_len - 1, *to_hash));
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Unfragmented, max_frag_len + 1, *to_hash));

  // Invalid min/max unfragmented tunnel
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::Tunnel, Layer::AES);
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Unfragmented, min_frag_len - 1, *tun_id, *to_hash));
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Unfragmented, max_frag_len + 1, *tun_id, *to_hash));

  // Invalid max fragmented local
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::FragLocal, Layer::AES);
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Fragmented, max_frag_len + 1, msg_id));

  // Invalid max fragmented router
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::FragRouter, Layer::AES);
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Fragmented, max_frag_len + 1, *to_hash, msg_id));

  // Invalid max fragmented tunnel
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::FragTunnel, Layer::AES);
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Fragmented, max_frag_len + 1, *tun_id, *to_hash, msg_id));
}

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery rejects invalid ChaCha fragment sizes", "[tun_delivery]")
{
  // Invalid min/max unfragmented local
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::Local, Layer::ChaCha);
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Unfragmented, min_frag_len - 1));
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Unfragmented, max_frag_len + 1));

  // Invalid min/max unfragmented router
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::Router, Layer::ChaCha);
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Unfragmented, min_frag_len - 1, *to_hash));
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Unfragmented, max_frag_len + 1, *to_hash));

  // Invalid min/max unfragmented tunnel
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::Tunnel, Layer::ChaCha);
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Unfragmented, min_frag_len - 1, *tun_id, *to_hash));
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Unfragmented, max_frag_len + 1, *tun_id, *to_hash));

  // Invalid max fragmented local
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::FragLocal, Layer::ChaCha);
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Fragmented, max_frag_len + 1, msg_id));

  // Invalid max fragmented router
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::FragRouter, Layer::ChaCha);
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Fragmented, max_frag_len + 1, *to_hash, msg_id));

  // Invalid max fragmented tunnel
  max_frag_len = FirstDelivery::max_fragment_size(FirstFlags::FragTunnel, Layer::ChaCha);
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Fragmented, max_frag_len + 1, *tun_id, *to_hash, msg_id));
}

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery rejects null to hashes", "[tun_delivery]")
{
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Unfragmented, min_frag_len, FirstDelivery::to_hash_t{}));
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Unfragmented, min_frag_len, *tun_id, FirstDelivery::to_hash_t{}));
}

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery rejects null tunnel ID", "[tun_delivery]")
{
  REQUIRE_THROWS(FirstDelivery(Layer::AES, Mode::Unfragmented, min_frag_len, 0, *to_hash));
  REQUIRE_THROWS(FirstDelivery(Layer::ChaCha, Mode::Unfragmented, min_frag_len, 0, *to_hash));
}

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery rejects deserializing invalid flags", "[tun_delivery]")
{
  auto& buf = fragment.buffer();

  // Rejects unsupported flags
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), FirstFlags::Invalid));
  REQUIRE_THROWS(fragment.deserialize());

  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), FirstFlags::Extended));
  REQUIRE_THROWS(fragment.deserialize());

  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), FirstFlags::Delay));
  REQUIRE_THROWS(fragment.deserialize());
}

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery rejects deserializing invalid local instructions", "[tun_delivery]")
{
  auto& buf = fragment.buffer();

  // Fragmented local delivery requires message ID
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), FirstFlags::FragLocal));
  REQUIRE_THROWS(fragment.deserialize());

  // Resize to fragmented local delivery size
  REQUIRE_NOTHROW(buf.clear());
  REQUIRE_NOTHROW(buf.resize(tini2p::under_cast(FirstDelivery::FragLocalLen)));

  // Should throw because of extra remaining length
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), FirstFlags::Local));
  REQUIRE_THROWS(fragment.deserialize());
}

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery rejects deserializing invalid router instructions", "[tun_delivery]")
{
  auto& buf = fragment.buffer();

  REQUIRE_NOTHROW(buf.clear());
  REQUIRE_NOTHROW(buf.resize(tini2p::under_cast(FirstDelivery::UnfragRouterLen)));

  // Unfragmented router requires to hash
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), FirstFlags::Router));
  REQUIRE_THROWS(fragment.deserialize());

  REQUIRE_NOTHROW(buf.clear());
  REQUIRE_NOTHROW(buf.resize(tini2p::under_cast(FirstDelivery::FragRouterLen)));

  // Fragmented router requires to hash and message ID
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), FirstFlags::FragRouter));
  REQUIRE_THROWS(fragment.deserialize());
}

TEST_CASE_METHOD(FirstDeliveryFixture, "FirstDelivery rejects deserializing invalid tunnel instructions", "[tun_delivery]")
{
  auto& buf = fragment.buffer();

  REQUIRE_NOTHROW(buf.clear());
  REQUIRE_NOTHROW(buf.resize(tini2p::under_cast(FirstDelivery::UnfragTunnelLen)));

  // Unfragmented tunnel requires tunnel ID and to hash
  // Should throw for null tunnel ID
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), FirstFlags::Tunnel));
  REQUIRE_THROWS(fragment.deserialize());

  const auto& flag_len =  tini2p::under_cast(FirstDelivery::FlagsLen);

  // Write the tunnel ID, should still throw for null to hash
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data() + flag_len, *tun_id, tini2p::Endian::Big));
  REQUIRE_THROWS(fragment.deserialize());

  // Fragmented tunnel requires tunnel ID, to hash, and message ID
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), FirstFlags::FragTunnel));
  // Write non-null first byte of the to hash
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data() + flag_len + tini2p::under_cast(FirstDelivery::TunnelIDLen), 0x42));
  // Should throw for missing message ID
  REQUIRE_THROWS(fragment.deserialize());

  // Should throw for invalid (null) fragment size
  REQUIRE_NOTHROW(buf.resize(tini2p::under_cast(FirstDelivery::FragTunnelLen)));
  REQUIRE_THROWS(fragment.deserialize());
}

using FollowDelivery = tini2p::data::FollowDelivery;
using FollowFlags = FollowDelivery::Flags;

struct FollowDeliveryFixture
{
  FollowDeliveryFixture() : msg_id(42) {}

  FollowDelivery::message_id_t msg_id;
  FollowDelivery fragment;
  std::unique_ptr<FollowDelivery> frag_ptr;
};

TEST_CASE_METHOD(FollowDeliveryFixture, "FollowDelivery has valid fields", "[tun_delivery]")
{
  const auto& exp_flags =
      fragment.FragNumToFlags(FollowFlags::Follow, tini2p::under_cast(FollowDelivery::MinFragmentNum), {});

  REQUIRE(fragment.flags() == exp_flags);
  REQUIRE(fragment.fragment_number() == FollowDelivery::MinFragmentNum);
  REQUIRE(!fragment.is_last());
  REQUIRE(fragment.message_id() == 0);
  REQUIRE(fragment.fragment_size() == FollowDelivery::MinFragmentLen);
  REQUIRE(fragment.size() == FollowDelivery::HeaderLen);
}

TEST_CASE_METHOD(FollowDeliveryFixture, "FollowDelivery serializes and deserializes from buffer", "[tun_delivery]")
{
  REQUIRE_NOTHROW(fragment.serialize());
  REQUIRE_NOTHROW(frag_ptr = std::make_unique<FollowDelivery>(Layer::AES, fragment.buffer()));
  REQUIRE(*frag_ptr == fragment);
}

TEST_CASE_METHOD(FollowDeliveryFixture, "FollowDelivery rejects invalid follow flag", "[tun_delivery]")
{
  auto& buf = fragment.buffer();
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), 0));

  REQUIRE_THROWS(fragment.deserialize());
}

TEST_CASE_METHOD(FollowDeliveryFixture, "FollowDelivery rejects invalid fragment numbers", "[tun_delivery]")
{
  const auto& bad_min_num = tini2p::under_cast(FollowDelivery::MinFragmentNum) - 1;
  const auto& bad_max_num = tini2p::under_cast(FollowDelivery::MaxFragmentNum) + 1;

  REQUIRE_THROWS(fragment.fragment_number(bad_min_num));
  REQUIRE_THROWS(fragment.fragment_number(bad_max_num));

  auto& buf = fragment.buffer();
  const auto& bad_min_flag = static_cast<FollowFlags>(tini2p::under_cast(FollowFlags::Follow) | (bad_min_num << 1));
  const auto& bad_max_flag = static_cast<FollowFlags>(tini2p::under_cast(FollowFlags::Follow) | (bad_max_num << 1));

  // Should throw for invalid minimum fragment number
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), bad_min_flag));
  REQUIRE_THROWS(fragment.deserialize());

  // Should throw for invalid maximum fragment number
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), bad_max_flag));
  REQUIRE_THROWS(fragment.deserialize());
}

TEST_CASE_METHOD(FollowDeliveryFixture, "FollowDelivery rejects invalid last fragment", "[tun_delivery]")
{
  const auto& max_frag_num = tini2p::under_cast(FollowDelivery::MaxFragmentNum);
  const auto& invalid_last_frag =
      static_cast<FollowDelivery::is_last_t>(tini2p::under_cast(FollowDelivery::is_last_t::True) + 1);

  // Require the max fragment number to be the last fragment
  REQUIRE_NOTHROW(fragment.fragment_number(max_frag_num, FollowDelivery::is_last_t::True));

  // Reject max fragment number that is not set as the last fragment
  REQUIRE_THROWS(fragment.fragment_number(max_frag_num));

  // Rejects invalid last fragment flag
  REQUIRE_THROWS(FollowDelivery(Layer::AES, max_frag_num - 1, msg_id, tini2p::under_cast(FollowDelivery::MinFragmentLen), invalid_last_frag));
  REQUIRE_THROWS(fragment.fragment_number(max_frag_num - 1, invalid_last_frag));

  auto& buf = fragment.buffer();

  // Require the max fragment number to be the last fragment
  const auto& bad_last_flags = static_cast<FollowFlags>(tini2p::under_cast(FollowFlags::Follow) | (max_frag_num << 1));
  REQUIRE_NOTHROW(tini2p::write_bytes(buf.data(), bad_last_flags));
}

TEST_CASE_METHOD(FollowDeliveryFixture, "FollowDelivery rejects invalid fragment sizes", "[tun_delivery]")
{
  const auto& min_frag_num = tini2p::under_cast(FollowDelivery::MinFragmentNum);
  const auto& bad_min_len = tini2p::under_cast(FollowDelivery::MinFragmentLen) - 1;
  const auto& bad_max_len = FollowDelivery::max_fragment_size(Layer::AES) + 1;

  REQUIRE_THROWS(FollowDelivery(Layer::AES, min_frag_num, msg_id, bad_min_len));
  REQUIRE_THROWS(FollowDelivery(Layer::AES, min_frag_num, msg_id, bad_max_len));

  REQUIRE_THROWS(fragment.fragment_size(bad_min_len));
  REQUIRE_THROWS(fragment.fragment_size(bad_max_len));
}
