/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/build_request_record.h"

/*-----------------------------\
| AES BuildRequestRecord Tests |
\-----------------------------*/

using AESBuildRequestRecord = tini2p::data::BuildRequestRecord;

struct AESBuildRequestFixture
{
  AESBuildRequestFixture()
      : params{0x42,
               tini2p::crypto::RandBytes<AESBuildRequestRecord::ident_t::HashLen>(),
               0x43,
               tini2p::crypto::RandBytes<AESBuildRequestRecord::ident_t::HashLen>(),
               tini2p::crypto::RandBytes<AESBuildRequestRecord::ident_t::HashLen>()},
        hop_trunc_ident(static_cast<AESBuildRequestRecord::trunc_ident_t>(params.hop_ident)),
        hop_keys(AESBuildRequestRecord::curve_t::create_keys()),
        creator_keys(AESBuildRequestRecord::curve_t::create_keys()),
        creator_key(creator_keys.pubkey),
        record(params, creator_keys.pubkey)
  {
  }

  AESBuildRequestRecord::params_t params;
  AESBuildRequestRecord::trunc_ident_t hop_trunc_ident;
  AESBuildRequestRecord::curve_t::keypair_t hop_keys;
  AESBuildRequestRecord::curve_t::keypair_t creator_keys;
  AESBuildRequestRecord::curve_t::pubkey_t creator_key;
  AESBuildRequestRecord record;
};

TEST_CASE_METHOD(AESBuildRequestFixture, "AES BuildRequestRecord has valid fields", "[build_request]")
{
  REQUIRE(record.truncated_ident() == hop_trunc_ident);
  REQUIRE(record.creator_key() == creator_keys.pubkey);
  REQUIRE(record.tunnel_id() == params.tunnel_id);
  REQUIRE(record.local_ident_hash() == params.local_ident);
  REQUIRE(record.next_tunnel_id() == params.next_tunnel_id);
  REQUIRE(record.next_ident_hash() == params.next_ident);
  REQUIRE(record.flags() == AESBuildRequestRecord::Flags::Hop);
  REQUIRE(record.request_time() == tini2p::time::now_min());
  REQUIRE(record.expiration() == tini2p::time::now_min() + tini2p::under_cast(AESBuildRequestRecord::Expiration));
  REQUIRE(record.options() == AESBuildRequestRecord::options_t());

  const auto& padding  = record.padding();
  REQUIRE(padding.size() == AESBuildRequestRecord::OptionsPaddingLen - record.options().size());
  REQUIRE(!padding.buffer().is_zero());
}

TEST_CASE_METHOD(AESBuildRequestFixture, "AES BuildRequestRecord serializes and deserializes from a buffer", "[build_request]")
{
  std::unique_ptr<AESBuildRequestRecord> record_ptr;
  REQUIRE_NOTHROW(record.serialize());
  REQUIRE_NOTHROW(record_ptr = std::make_unique<AESBuildRequestRecord>(record.buffer()));
  REQUIRE(*record_ptr == record);
}

TEST_CASE_METHOD(AESBuildRequestFixture, "AES BuildRequestRecord partially deserializes from a buffer", "[build_request]")
{
  std::unique_ptr<AESBuildRequestRecord> record_ptr;

  // Only copy the record buffer, and read the hop's truncated Identity hash from the record buffer
  // Useful for searching encrypted records during AESBuildRequestRecord processing by hops
  REQUIRE_NOTHROW(
      record_ptr = std::make_unique<AESBuildRequestRecord>(record.buffer(), AESBuildRequestRecord::DeserializeMode::Partial));
  REQUIRE(record_ptr->truncated_ident() == hop_trunc_ident);
  REQUIRE(record_ptr->buffer() == record.buffer());
}

TEST_CASE_METHOD(AESBuildRequestFixture, "AES BuildRequestRecord rejects invalid tunnel IDs", "[build_request]")
{

  // Rejects null tunnel IDs
  params.tunnel_id = 0;
  REQUIRE_THROWS(AESBuildRequestRecord(params, creator_key));

  params.tunnel_id = 0x42;
  params.next_tunnel_id = 0;
  REQUIRE_THROWS(AESBuildRequestRecord(params, creator_key));

  // Rejects same tunnel ID and next tunnel ID
  params.next_tunnel_id = params.tunnel_id;
  REQUIRE_THROWS(AESBuildRequestRecord(params, creator_key));
}

TEST_CASE_METHOD(AESBuildRequestFixture, "AES BuildRequestRecord rejects invalid Identity hashes", "[build_request]")
{
  // Rejects null Identity hashes
  REQUIRE_NOTHROW(params.local_ident.zero());
  REQUIRE_THROWS(AESBuildRequestRecord(params, creator_key));

  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(params.local_ident));
  REQUIRE_NOTHROW(params.next_ident.zero());
  REQUIRE_THROWS(AESBuildRequestRecord(params, creator_key));

  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(params.next_ident));
  REQUIRE_NOTHROW(params.hop_ident.zero());
  REQUIRE_THROWS(AESBuildRequestRecord(params, creator_key));

  // Create an Identity hash with leading 16 bytes all zeroes
  AESBuildRequestRecord::ident_hash_t null_trunc_hash{{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                              0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
                                              0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01}};

  // Rejects null truncated Identity hash
  params.hop_ident = null_trunc_hash;
  REQUIRE_THROWS(AESBuildRequestRecord(params, creator_key));

  // Rejects local Identity hash matching hop hash, zero-hop tunnels don't use AESBuildRequestRecords
  params.hop_ident = params.local_ident;
  REQUIRE_THROWS(AESBuildRequestRecord(params, creator_key));

  // Accepts local Identity hash matching next hash, when building inbound tunnels
  // InboundEndpoint is the tunnel creator (us), and is the last hop's next hop
  params.next_ident = params.local_ident;
  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(params.hop_ident));
  REQUIRE_NOTHROW(AESBuildRequestRecord(params, creator_key));

  // Rejects next Identity hash matching hop hash, tunnel hop cannot send to itself
  params.next_ident = params.hop_ident;
  REQUIRE_THROWS(AESBuildRequestRecord(params, creator_key));
}

TEST_CASE_METHOD(AESBuildRequestFixture, "AES BuildRequestRecord rejects null public key", "[build_request]")
{
  REQUIRE_THROWS(AESBuildRequestRecord(params, {}));
}

/*--------------------------------\
| ChaCha BuildRequestRecord Tests |
\--------------------------------*/

using ChaChaBuildRequestRecord = tini2p::data::BuildRequestRecord;

struct ChaChaBuildRequestFixture
{
  ChaChaBuildRequestFixture()
      : params{0x42,
               tini2p::crypto::RandBytes<ChaChaBuildRequestRecord::ident_t::HashLen>(),
               0x43,
               tini2p::crypto::RandBytes<ChaChaBuildRequestRecord::ident_t::HashLen>(),
               tini2p::crypto::RandBytes<ChaChaBuildRequestRecord::ident_t::HashLen>()},
        hop_trunc_ident(static_cast<ChaChaBuildRequestRecord::trunc_ident_t>(params.hop_ident)),
        hop_keys(ChaChaBuildRequestRecord::curve_t::create_keys()),
        creator_keys(ChaChaBuildRequestRecord::curve_t::create_keys()),
        creator_key(creator_keys.pubkey),
        receive_key(ChaChaBuildRequestRecord::aead_t::create_key()),
        record(params, creator_keys.pubkey, receive_key)
  {
  }

  ChaChaBuildRequestRecord::params_t params;
  ChaChaBuildRequestRecord::trunc_ident_t hop_trunc_ident;
  ChaChaBuildRequestRecord::curve_t::keypair_t hop_keys;
  ChaChaBuildRequestRecord::curve_t::keypair_t creator_keys;
  ChaChaBuildRequestRecord::curve_t::pubkey_t creator_key;
  ChaChaBuildRequestRecord::aead_t::key_t receive_key;
  ChaChaBuildRequestRecord record;
};

TEST_CASE_METHOD(ChaChaBuildRequestFixture, "ChaCha BuildRequestRecord has valid fields", "[build_request]")
{
  REQUIRE(record.truncated_ident() == hop_trunc_ident);
  REQUIRE(record.creator_key() == creator_keys.pubkey);
  REQUIRE(record.tunnel_id() == params.tunnel_id);
  REQUIRE(record.local_ident_hash() == params.local_ident);
  REQUIRE(record.next_tunnel_id() == params.next_tunnel_id);
  REQUIRE(record.next_ident_hash() == params.next_ident);
  REQUIRE(record.flags() == ChaChaBuildRequestRecord::Flags::Hop);
  REQUIRE(record.request_time() == tini2p::time::now_min());
  REQUIRE(record.expiration() == tini2p::time::now_min() + tini2p::under_cast(ChaChaBuildRequestRecord::Expiration));
  REQUIRE(record.options() == ChaChaBuildRequestRecord::options_t(receive_key));

  const auto& padding  = record.padding();
  REQUIRE(padding.size() == ChaChaBuildRequestRecord::OptionsPaddingLen - record.options().size());
  REQUIRE(!padding.buffer().is_zero());
}

TEST_CASE_METHOD(ChaChaBuildRequestFixture, "ChaCha BuildRequestRecord serializes and deserializes from a buffer", "[build_request]")
{
  std::unique_ptr<ChaChaBuildRequestRecord> record_ptr;
  REQUIRE_NOTHROW(record.serialize());
  REQUIRE_NOTHROW(record_ptr = std::make_unique<ChaChaBuildRequestRecord>(record.buffer()));
  REQUIRE(*record_ptr == record);
}

TEST_CASE_METHOD(ChaChaBuildRequestFixture, "ChaCha BuildRequestRecord partially deserializes from a buffer", "[build_request]")
{
  std::unique_ptr<ChaChaBuildRequestRecord> record_ptr;

  // Only copy the record buffer, and read the hop's truncated Identity hash from the record buffer
  // Useful for searching encrypted records during ChaChaBuildRequestRecord processing by hops
  REQUIRE_NOTHROW(
      record_ptr = std::make_unique<ChaChaBuildRequestRecord>(record.buffer(), ChaChaBuildRequestRecord::DeserializeMode::Partial));
  REQUIRE(record_ptr->truncated_ident() == hop_trunc_ident);
  REQUIRE(record_ptr->buffer() == record.buffer());
}

TEST_CASE_METHOD(ChaChaBuildRequestFixture, "ChaCha BuildRequestRecord rejects invalid tunnel IDs", "[build_request]")
{

  // Rejects null tunnel IDs
  params.tunnel_id = 0;
  REQUIRE_THROWS(ChaChaBuildRequestRecord(params, creator_key));

  params.tunnel_id = 0x42;
  params.next_tunnel_id = 0;
  REQUIRE_THROWS(ChaChaBuildRequestRecord(params, creator_key));

  // Rejects same tunnel ID and next tunnel ID
  params.next_tunnel_id = params.tunnel_id;
  REQUIRE_THROWS(ChaChaBuildRequestRecord(params, creator_key));
}

TEST_CASE_METHOD(ChaChaBuildRequestFixture, "ChaCha BuildRequestRecord rejects invalid Identity hashes", "[build_request]")
{
  // Rejects null Identity hashes
  REQUIRE_NOTHROW(params.local_ident.zero());
  REQUIRE_THROWS(ChaChaBuildRequestRecord(params, creator_key));

  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(params.local_ident));
  REQUIRE_NOTHROW(params.next_ident.zero());
  REQUIRE_THROWS(ChaChaBuildRequestRecord(params, creator_key));

  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(params.next_ident));
  REQUIRE_NOTHROW(params.hop_ident.zero());
  REQUIRE_THROWS(ChaChaBuildRequestRecord(params, creator_key));

  // Create an Identity hash with leading 16 bytes all zeroes
  ChaChaBuildRequestRecord::ident_hash_t null_trunc_hash{{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                              0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
                                              0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01}};

  // Rejects null truncated Identity hash
  params.hop_ident = null_trunc_hash;
  REQUIRE_THROWS(ChaChaBuildRequestRecord(params, creator_key));

  // Rejects local Identity hash matching hop hash, zero-hop tunnels don't use ChaChaBuildRequestRecords
  params.hop_ident = params.local_ident;
  REQUIRE_THROWS(ChaChaBuildRequestRecord(params, creator_key));

  // Accepts local Identity hash matching next hash, when building inbound tunnels
  // InboundEndpoint is the tunnel creator (us), and is the last hop's next hop
  params.next_ident = params.local_ident;
  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(params.hop_ident));
  REQUIRE_NOTHROW(ChaChaBuildRequestRecord(params, creator_key));

  // Rejects next Identity hash matching hop hash, tunnel hop cannot send to itself
  params.next_ident = params.hop_ident;
  REQUIRE_THROWS(ChaChaBuildRequestRecord(params, creator_key));
}

TEST_CASE_METHOD(ChaChaBuildRequestFixture, "ChaCha BuildRequestRecord rejects null keys", "[build_request]")
{
  REQUIRE_THROWS(ChaChaBuildRequestRecord(params, {}));
  REQUIRE_THROWS(ChaChaBuildRequestRecord(params, creator_key, {}, ChaChaBuildRequestRecord::flags_t::Hop));
}
