/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/tunnel_build_reply.h"

using namespace tini2p::crypto;

using AESBuildResponseRecord = tini2p::data::BuildResponseRecord<AES>;
using AESTunnelBuildReply = tini2p::data::TunnelBuildReply<AES>;

struct AESBuildReplyFixture
{
  AESBuildReplyFixture() : records{AESBuildResponseRecord(), AESBuildResponseRecord()}, message(records) {}

  AESTunnelBuildReply::records_t records;
  AESTunnelBuildReply message;
};

TEST_CASE_METHOD(AESBuildReplyFixture, "AES TunnelBuildReply adds records", "[build_msg]")
{
  REQUIRE_NOTHROW(message.add_record(AESBuildResponseRecord()));
  REQUIRE_NOTHROW(message.add_record(AESBuildResponseRecord(AESBuildResponseRecord::flags_t::Reject)));
}

TEST_CASE_METHOD(AESBuildReplyFixture, "AES TunnelBuildReply rejects invalid number of records", "[build_msg]")
{
  const auto& min_records = tini2p::under_cast(AESTunnelBuildReply::MinRecords);
  const auto& max_records = tini2p::under_cast(AESTunnelBuildReply::MaxRecords);

  // Rejects serializing from empty records
  REQUIRE_THROWS(AESTunnelBuildReply().serialize());

  // Rejects creating from empty records
  REQUIRE_THROWS(AESTunnelBuildReply(AESTunnelBuildReply::records_t{}));

  // Unrealistic, create max + 1 random records
  records.clear();
  for (AESTunnelBuildReply::num_records_t i = 0; i < max_records + 1; ++i)
    REQUIRE_NOTHROW(records.emplace_back(AESBuildResponseRecord()));

  // Rejects creating from more than max records
  REQUIRE_THROWS(AESTunnelBuildReply(records));

  auto& msg_buf = message.buffer();

  // Write min - 1 to the number of records byte
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_buf.data(), AESTunnelBuildReply::num_records_t(min_records - 1)));

  // Rejects deserializing less than the minimum number of records
  REQUIRE_THROWS(message.deserialize());

  // Write max + 1 to the number of records byte
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_buf.data(), AESTunnelBuildReply::num_records_t(max_records + 1)));

  // Rejects deserializing more than the maximum number of records
  REQUIRE_THROWS(message.deserialize());

  // Write max to the number of records byte
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_buf.data(), AESTunnelBuildReply::num_records_t(max_records)));

  // Rejects deserializing invalid buffer lengths
  // Rejects min_len - 1 buffer size
  REQUIRE_NOTHROW(msg_buf.resize(tini2p::under_cast(AESTunnelBuildReply::MinLen) - 1));
  REQUIRE_THROWS(message.deserialize());

  // Rejects max_len + 1 buffer size
  REQUIRE_NOTHROW(msg_buf.resize(tini2p::under_cast(AESTunnelBuildReply::MaxLen) + 1));
  REQUIRE_THROWS(message.deserialize());

  // Expects maximum number of records, buffer length is short by one byte
  REQUIRE_NOTHROW(msg_buf.resize(tini2p::under_cast(AESTunnelBuildReply::MaxLen) - 1));
  REQUIRE_THROWS(message.deserialize());
}
