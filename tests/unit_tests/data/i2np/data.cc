/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/data.h"

using tini2p::data::Data;

struct I2NPDataFixture
{
  Data data;
};

TEST_CASE_METHOD(I2NPDataFixture, "I2NP Data stores arbitrary data", "[i2np]")
{
  Data::data_t rand_data(Data::MinDataLen);
  tini2p::crypto::RandBytes(rand_data);

  REQUIRE_NOTHROW(data.data(rand_data));
  REQUIRE(data.data() == rand_data);

  rand_data.resize(Data::MaxDataLen);
  tini2p::crypto::RandBytes(rand_data);

  REQUIRE_NOTHROW(data.data(rand_data));
  REQUIRE(data.data() == rand_data);

  REQUIRE(data.size() == Data::LengthLen + rand_data.size());
}

TEST_CASE_METHOD(I2NPDataFixture, "I2NP Data serializes and deserializes from buffer", "[i2np]")
{
  auto data_copy = data;

  REQUIRE_NOTHROW(data.serialize());
  REQUIRE_NOTHROW(data.deserialize());
  REQUIRE(data == data_copy);

  Data::data_t rand_data(Data::MinDataLen + 0x42);
  tini2p::crypto::RandBytes(rand_data);

  REQUIRE_NOTHROW(data.data(rand_data));

  REQUIRE_NOTHROW(data.serialize());
  REQUIRE(!(data == data_copy));

  data_copy = data;

  REQUIRE_NOTHROW(data.deserialize());
  REQUIRE(data == data_copy);

  REQUIRE(data.data() == rand_data);
}

TEST_CASE_METHOD(I2NPDataFixture, "I2NP Data rejects invalid sizes", "[i2np]")
{
  REQUIRE_THROWS(Data(Data::buffer_t(Data::MinLen - 1)));
  REQUIRE_THROWS(Data(Data::buffer_t(Data::MaxLen + 1)));

  REQUIRE_THROWS(data.data(Data::data_t(Data::MinDataLen - 1)));
  REQUIRE_THROWS(data.data(Data::data_t(Data::MaxDataLen + 1)));

  REQUIRE_NOTHROW(data.serialize());

  tini2p::BytesWriter<Data::buffer_t> writer(data.buffer());
  writer.write_bytes(Data::MaxDataLen + 1, tini2p::Endian::Big);

  REQUIRE_THROWS(data.deserialize());
}
