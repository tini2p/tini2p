/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/i2np/delivery_status.h"

using tini2p::data::DeliveryStatus;

struct DeliveryStatusFixture
{
  DeliveryStatusFixture() : msg_id(tini2p::crypto::RandInRange()), ds_ptr(new DeliveryStatus(msg_id)) {}

  DeliveryStatus::message_id_t msg_id;
  DeliveryStatus::timestamp_t time;
  std::unique_ptr<DeliveryStatus> ds_ptr;
};

TEST_CASE_METHOD(DeliveryStatusFixture, "DeliveryStatus has valid fields", "[i2np]")
{
  REQUIRE(ds_ptr->message_id() == msg_id);
  REQUIRE(ds_ptr->timestamp() == tini2p::time::now_s());

  time = tini2p::time::now_s() - 500;  // set timestamp to arbitrary point in the past
  REQUIRE_NOTHROW(ds_ptr.reset(new DeliveryStatus(msg_id, time)));

  REQUIRE(ds_ptr->message_id() == msg_id);
  REQUIRE(ds_ptr->timestamp() == time);
}

TEST_CASE_METHOD(DeliveryStatusFixture, "DeliveryStatus deserializes from buffer", "[i2np]")
{
  std::unique_ptr<DeliveryStatus> ds_des_ptr;
  REQUIRE_NOTHROW(ds_des_ptr.reset(new DeliveryStatus(ds_ptr->buffer())));
  REQUIRE(*ds_des_ptr == *ds_ptr);

  REQUIRE_NOTHROW(ds_des_ptr.reset(new DeliveryStatus(ds_ptr->buffer().data(), DeliveryStatus::MessageLen)));
  REQUIRE(*ds_des_ptr == *ds_ptr);
}

TEST_CASE_METHOD(DeliveryStatusFixture, "DeliveryStatus rejects invalid buffer", "[i2np]")
{
  REQUIRE_THROWS(DeliveryStatus(nullptr, DeliveryStatus::MessageLen));
  REQUIRE_THROWS(DeliveryStatus(ds_ptr->buffer().data(), DeliveryStatus::MessageLen - 1));
  REQUIRE_THROWS(DeliveryStatus(ds_ptr->buffer().data(), DeliveryStatus::MessageLen + 1));
}
