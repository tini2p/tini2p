/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 */

#include <catch2/catch.hpp>

#include "src/data/i2np/build_response_record.h"

/*---------------------------\
| AES BuildResponseRecord Tests |
\---------------------------*/

using AESBuildResponseRecord = tini2p::data::BuildResponseRecord<tini2p::crypto::AES>;

struct AESBuildResponseRecordFixture
{
  AESBuildResponseRecord record;
};

TEST_CASE_METHOD(AESBuildResponseRecordFixture, "AES BuildResponseRecord has valid fields", "[build_reply]")
{
  REQUIRE(record.flags() == AESBuildResponseRecord::Flags::Accept);
  REQUIRE(record.options() == AESBuildResponseRecord::options_t());
  REQUIRE(record.padding().size() == tini2p::under_cast(AESBuildResponseRecord::AESOptPadLen) - record.options().size());

  // Sha256 checksum only in AES BuildResponseRecords, Poly1305 MAC takes its place for ChaCha BuildResponseRecords
  REQUIRE(
      record.checksum() == AESBuildResponseRecord::CalculateChecksum(record.flags(), record.options(), record.padding()));
}

TEST_CASE_METHOD(
    AESBuildResponseRecordFixture,
    "AES BuildResponseRecord serializes and deserializes from the buffer",
    "[build_reply]")
{
  // Serialize and encrypt the record to buffer
  std::unique_ptr<AESBuildResponseRecord> record_ptr;
  REQUIRE_NOTHROW(record.serialize());
  REQUIRE_NOTHROW(record_ptr = std::make_unique<AESBuildResponseRecord>(record.buffer()));
  REQUIRE(*record_ptr == record);
}

TEST_CASE_METHOD(AESBuildResponseRecordFixture, "AES BuildResponseRecord rejects setting invalid flags", "[build_reply]")
{
  using Flags = AESBuildResponseRecord::Flags;

  // Check that valid flags don't throw
  REQUIRE_NOTHROW(AESBuildResponseRecord(Flags::Accept));
  REQUIRE_NOTHROW(AESBuildResponseRecord(Flags::Reject));

  // Check that invalid flags throw
  // Any flags that aren't Accept or Reject are invalid (254 total, we test 4)
  const Flags& low_bad_accept = static_cast<Flags>(tini2p::under_cast(Flags::Accept) - 1);
  const Flags& hi_bad_accept = static_cast<Flags>(tini2p::under_cast(Flags::Accept) + 1);
  const Flags& low_bad_reject = static_cast<Flags>(tini2p::under_cast(Flags::Reject) - 1);
  const Flags& hi_bad_reject = static_cast<Flags>(tini2p::under_cast(Flags::Reject) + 1);

  REQUIRE_THROWS(AESBuildResponseRecord(low_bad_accept));
  REQUIRE_THROWS(AESBuildResponseRecord(hi_bad_accept));
  REQUIRE_THROWS(AESBuildResponseRecord(low_bad_reject));
  REQUIRE_THROWS(AESBuildResponseRecord(hi_bad_reject));

  REQUIRE_THROWS(record.flags(low_bad_accept));
  REQUIRE_THROWS(record.flags(hi_bad_accept));
  REQUIRE_THROWS(record.flags(low_bad_reject));
  REQUIRE_THROWS(record.flags(hi_bad_reject));
}

TEST_CASE_METHOD(AESBuildResponseRecordFixture, "AES BuildResponseRecord rejects invalid checksum", "[build_reply]")
{
  auto& record_buf = record.buffer();
  std::uint8_t first_check_byte;

  // Read first checksum byte, and write an invalid value
  REQUIRE_NOTHROW(tini2p::read_bytes(record_buf.data(), first_check_byte));
  REQUIRE_NOTHROW(tini2p::write_bytes(record_buf.data(), std::uint8_t(first_check_byte + 1)));

  // Ensure the checksum fails validation
  REQUIRE_THROWS(record.deserialize());

  // Reset to original checksum
  REQUIRE_NOTHROW(tini2p::write_bytes(record_buf.data(), first_check_byte));

  // Ensure valid state
  REQUIRE_NOTHROW(record.deserialize());

  // Randomize the options and padding data
  REQUIRE_NOTHROW(tini2p::crypto::RandBytes(
      record_buf.data() + tini2p::under_cast(AESBuildResponseRecord::sha_t::DigestLen),
      tini2p::under_cast(AESBuildResponseRecord::AESOptPadLen)));

  // Ensure the checksum fails validation
  REQUIRE_THROWS(record.deserialize());
}

/*------------------------------\
| ChaCha BuildResponseRecord Tests |
\------------------------------*/

using ChaChaBuildResponseRecord = tini2p::data::BuildResponseRecord<tini2p::crypto::ChaCha20>;

struct ChaChaBuildResponseRecordFixture
{
  ChaChaBuildResponseRecord record;
};

TEST_CASE_METHOD(ChaChaBuildResponseRecordFixture, "ChaCha BuildResponseRecord has valid fields", "[build_reply]")
{
  REQUIRE(record.flags() == ChaChaBuildResponseRecord::Flags::Accept);
  REQUIRE(record.options() == ChaChaBuildResponseRecord::options_t());
  REQUIRE(
      record.padding().size() == tini2p::under_cast(ChaChaBuildResponseRecord::ChaChaOptPadLen) - record.options().size());
}

TEST_CASE_METHOD(
    ChaChaBuildResponseRecordFixture,
    "ChaCha BuildResponseRecord serializes and deserializes from the buffer",
    "[build_reply]")
{
  // Serialize and encrypt the record to buffer
  std::unique_ptr<ChaChaBuildResponseRecord> record_ptr;
  REQUIRE_NOTHROW(record.serialize());
  REQUIRE_NOTHROW(record_ptr = std::make_unique<ChaChaBuildResponseRecord>(record.buffer()));
  REQUIRE(*record_ptr == record);
}

TEST_CASE_METHOD(
    ChaChaBuildResponseRecordFixture,
    "ChaCha BuildResponseRecord rejects setting invalid flags",
    "[build_reply]")
{
  using Flags = ChaChaBuildResponseRecord::Flags;

  // Check that valid flags don't throw
  REQUIRE_NOTHROW(ChaChaBuildResponseRecord(Flags::Accept));
  REQUIRE_NOTHROW(ChaChaBuildResponseRecord(Flags::Reject));

  // Check that invalid flags throw
  // Any flags that aren't Accept or Reject are invalid (254 total, we test 4)
  const Flags& low_bad_accept = static_cast<Flags>(tini2p::under_cast(Flags::Accept) - 1);
  const Flags& hi_bad_accept = static_cast<Flags>(tini2p::under_cast(Flags::Accept) + 1);
  const Flags& low_bad_reject = static_cast<Flags>(tini2p::under_cast(Flags::Reject) - 1);
  const Flags& hi_bad_reject = static_cast<Flags>(tini2p::under_cast(Flags::Reject) + 1);

  REQUIRE_THROWS(ChaChaBuildResponseRecord(low_bad_accept));
  REQUIRE_THROWS(ChaChaBuildResponseRecord(hi_bad_accept));
  REQUIRE_THROWS(ChaChaBuildResponseRecord(low_bad_reject));
  REQUIRE_THROWS(ChaChaBuildResponseRecord(hi_bad_reject));

  REQUIRE_THROWS(record.flags(low_bad_accept));
  REQUIRE_THROWS(record.flags(hi_bad_accept));
  REQUIRE_THROWS(record.flags(low_bad_reject));
  REQUIRE_THROWS(record.flags(hi_bad_reject));
}

// ChaCha BuildResponseRecord integrity is protected by ChaCha20-Poly1305 AEAD encryption
