/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/tunnel_build_message.h"

using AESBuildRequestRecord = tini2p::data::BuildRequestRecord;
using AESTunnelBuildMessage = tini2p::data::TunnelBuildMessage;

using namespace tini2p::crypto;

struct AESBuildMessageFixture
{
  AESBuildMessageFixture()
      : records{AESBuildRequestRecord(
                    RandBytes<AESTunnelBuildMessage::RecordLen>(),
                    AESBuildRequestRecord::deserialize_mode_t::Partial)
                    .flags(AESBuildRequestRecord::flags_t::Hop),
                AESBuildRequestRecord(
                    RandBytes<AESTunnelBuildMessage::RecordLen>(),
                    AESBuildRequestRecord::deserialize_mode_t::Partial)
                    .flags(AESBuildRequestRecord::flags_t::Hop)},
        message(records)
  {
  }

  AESTunnelBuildMessage::records_t records;
  AESTunnelBuildMessage message;
};

TEST_CASE_METHOD(AESBuildMessageFixture, "AES TunnelBuildMessage adds records", "[build_msg]")
{
  // Unrealistic, create a BuildRequestRecord with a random buffer
  AESBuildRequestRecord record(
      RandBytes<AESTunnelBuildMessage::RecordLen>(),
      AESBuildRequestRecord::deserialize_mode_t::Partial);

  record.flags(AESBuildRequestRecord::flags_t::Hop);

  REQUIRE_NOTHROW(message.add_record(record));
  REQUIRE(message.find_record(record.truncated_ident()) == record);
}

TEST_CASE_METHOD(AESBuildMessageFixture, "AES TunnelBuildMessage rejects duplicate records", "[build_msg]")
{
  // Unrealistic, create a random hop public key
  AESBuildRequestRecord::curve_t::pubkey_t hop_key;
  REQUIRE_NOTHROW(RandBytes(hop_key));

  const auto& record = records.front();
  // Rejects creating message with duplicate records
  REQUIRE_THROWS(AESTunnelBuildMessage(AESTunnelBuildMessage::records_t{record, record}));

  // Rejects adding duplicate record
  REQUIRE_THROWS(message.add_record(record));

  // Write duplicate record to buffer
  auto& msg_buf = message.buffer();
  tini2p::BytesWriter<AESTunnelBuildMessage::buffer_t> writer(msg_buf);
  REQUIRE_NOTHROW(writer.skip_bytes(
      tini2p::under_cast(AESTunnelBuildMessage::SizeLen) + tini2p::under_cast(AESTunnelBuildMessage::RecordLen)));
  REQUIRE_NOTHROW(writer.write_data(record.buffer()));

  // Rejects deserializing duplicate record
  REQUIRE_THROWS(message.deserialize());
}

TEST_CASE_METHOD(AESBuildMessageFixture, "AES TunnelBuildMessage rejects invalid number of records", "[build_msg]")
{
  const auto& min_records = tini2p::under_cast(AESTunnelBuildMessage::MinRecords);
  const auto& max_records = tini2p::under_cast(AESTunnelBuildMessage::MaxRecords);

  // Rejects serializing from empty records
  REQUIRE_THROWS(AESTunnelBuildMessage().serialize());

  // Rejects creating from empty records
  REQUIRE_THROWS(AESTunnelBuildMessage(AESTunnelBuildMessage::records_t{}));

  // Unrealistic, create max + 1 random records
  records.clear();
  for (AESTunnelBuildMessage::num_records_t i = 0; i < max_records + 1; ++i)
    {
      AESBuildRequestRecord::buffer_t record_buf;
      REQUIRE_NOTHROW(RandBytes(record_buf));
      REQUIRE_NOTHROW(records.emplace_back(record_buf, AESBuildRequestRecord::deserialize_mode_t::Partial));
    }

  // Rejects creating from more than max records
  REQUIRE_THROWS(AESTunnelBuildMessage(records));

  auto& msg_buf = message.buffer();

  // Write min - 1 to the number of records byte
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_buf.data(), AESTunnelBuildMessage::num_records_t(min_records - 1)));

  // Rejects deserializing less than the minimum number of records
  REQUIRE_THROWS(message.deserialize());

  // Write max + 1 to the number of records byte
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_buf.data(), AESTunnelBuildMessage::num_records_t(max_records + 1)));

  // Rejects deserializing more than the maximum number of records
  REQUIRE_THROWS(message.deserialize());

  // Write max to the number of records byte
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_buf.data(), AESTunnelBuildMessage::num_records_t(max_records)));

  // Rejects deserializing invalid buffer lengths
  // Rejects min_len - 1 buffer size
  REQUIRE_NOTHROW(msg_buf.resize(tini2p::under_cast(AESTunnelBuildMessage::MinLen) - 1));
  REQUIRE_THROWS(message.deserialize());

  // Rejects max_len + 1 buffer size
  REQUIRE_NOTHROW(msg_buf.resize(tini2p::under_cast(AESTunnelBuildMessage::MaxLen) + 1));
  REQUIRE_THROWS(message.deserialize());

  // Expects maximum number of records, buffer length is short by one byte
  REQUIRE_NOTHROW(msg_buf.resize(tini2p::under_cast(AESTunnelBuildMessage::MaxLen) - 1));
  REQUIRE_THROWS(message.deserialize());
}
