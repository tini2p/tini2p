/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/garlic.h"

namespace data = tini2p::data;

struct GarlicFixture
{
  data::Garlic garlic;
};

TEST_CASE_METHOD(GarlicFixture, "data::Garlic has valid fields", "[garlic]")
{
  REQUIRE(garlic.empty_cloves());
  REQUIRE(garlic.size() == data::Garlic::MetadataLen);
  REQUIRE(garlic.certificate() == tini2p::data::Certificate());
  REQUIRE(garlic.expiration() > tini2p::time::now_ms());
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic rejects serializing empty cloves", "[garlic]")
{
  REQUIRE(garlic.empty_cloves());
  REQUIRE_THROWS(garlic.serialize());
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic adds cloves", "[garlic]")
{
  data::Garlic::clove_t clove;
  data::Garlic::cloves_t cloves{clove};

  REQUIRE_NOTHROW(garlic.add_clove(cloves.front()));
  REQUIRE(garlic.extract_cloves() == cloves);
  REQUIRE(garlic.empty_cloves());

  cloves.clear();

  // Add max number of cloves individually
  for (std::uint8_t i = 0; i < data::Garlic::MaxCloves; ++i)
    {
      data::Garlic::clove_t clove;
      cloves.emplace_back(clove);
      REQUIRE_NOTHROW(garlic.add_clove(std::move(clove)));
    }

  // Extract the cloves, and check they equal the original collection
  REQUIRE(garlic.has_max_cloves());
  REQUIRE(garlic.extract_cloves() == cloves);

  // Add max number of cloves
  REQUIRE_NOTHROW(garlic.cloves(cloves));
  REQUIRE(garlic.has_max_cloves());
  REQUIRE(!garlic.has_max_cloves_len());
  REQUIRE(garlic.extract_cloves() == cloves);

  REQUIRE_NOTHROW(data::Garlic(cloves));
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic sets message ID", "[garlic]")
{
  data::Garlic::clove_t clove;
  data::Garlic msg;
  data::Garlic::message_id_t msg_id(0x01);

  REQUIRE_NOTHROW(msg = data::Garlic({clove}, msg_id));
  REQUIRE_NOTHROW(garlic.message_id(msg_id));

  REQUIRE(msg.message_id() == msg_id);
  REQUIRE(garlic.message_id() == msg_id);
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic rejects setting null message ID", "[garlic]")
{
  REQUIRE_THROWS(garlic.message_id(0));
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic sets expiration", "[garlic]")
{
  data::Garlic::expiration_t expiration(tini2p::time::now_ms() + data::Garlic::DefaultExpiration);

  REQUIRE_NOTHROW(garlic.expiration(expiration));
  REQUIRE(garlic.expiration() == expiration);
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic rejects setting expired expiration", "[garlic]")
{
  REQUIRE_THROWS(garlic.expiration(tini2p::time::now_ms() - 1));
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic serializes and deserializes from buffer", "[garlic]")
{
  REQUIRE_NOTHROW(garlic.add_clove(data::Garlic::clove_t({}, tini2p::data::DatabaseStore(std::make_shared<tini2p::data::Info>()))));
  REQUIRE_NOTHROW(garlic.serialize());
  REQUIRE_NOTHROW(garlic.deserialize());

  data::Garlic de_msg;
  REQUIRE_NOTHROW(de_msg = data::Garlic(garlic.buffer()));
  REQUIRE((de_msg == garlic));
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic rejects deserializing invalid number of cloves", "[garlic]")
{
  // Ensure valid starting state
  REQUIRE_NOTHROW(garlic.add_clove(data::Garlic::clove_t()));
  REQUIRE_NOTHROW(garlic.serialize());
  REQUIRE_NOTHROW(garlic.deserialize());

  auto& garlic_buf = garlic.buffer();
  tini2p::BytesWriter<data::Garlic::buffer_t> writer(garlic_buf);

  // Check that no cloves throws
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::num_t(0)));
  REQUIRE_THROWS(garlic.deserialize());
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic rejects deserializing invalid clove delivery instructions", "[garlic]")
{
  // Ensure valid starting state
  REQUIRE_NOTHROW(garlic.add_clove(data::Garlic::clove_t()));
  REQUIRE_NOTHROW(garlic.serialize());
  REQUIRE_NOTHROW(garlic.deserialize());

  auto& garlic_buf = garlic.buffer();
  tini2p::BytesWriter<data::Garlic::buffer_t> writer(garlic_buf);

  // Check that invalid delivery instructions flags throws
  REQUIRE_NOTHROW(writer.skip_bytes(data::Garlic::NumLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::clove_t::instructions_t::ReservedMask));
  REQUIRE_THROWS(garlic.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::clove_t::instructions_t::FlagsLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::clove_t::instructions_t::UnusedFlags::Encrypted));
  REQUIRE_THROWS(garlic.deserialize());

  // Check that router delivery for a local clove throws
  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::clove_t::instructions_t::FlagsLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::clove_t::instructions_t::DeliveryFlags::Router));
  REQUIRE_THROWS(garlic.deserialize());

  // Check that router delivery for a local clove throws
  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::clove_t::instructions_t::FlagsLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::clove_t::instructions_t::DeliveryFlags::Tunnel));
  REQUIRE_THROWS(garlic.deserialize());
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic rejects deserializing invalid clove I2NP message", "[garlic]")
{
  // Ensure valid starting state
  REQUIRE_NOTHROW(garlic.add_clove(data::Garlic::clove_t()));
  REQUIRE_NOTHROW(garlic.serialize());
  REQUIRE_NOTHROW(garlic.deserialize());

  auto& garlic_buf = garlic.buffer();
  tini2p::BytesWriter<data::Garlic::buffer_t> writer(garlic_buf);

  // Check that invalid clove sizes throw
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::MinClovesLen - 1, tini2p::Endian::Little));
  REQUIRE_THROWS(garlic.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(data::I2NPHeader::SizeLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::MaxClovesLen + 1, tini2p::Endian::Little));
  REQUIRE_THROWS(garlic.deserialize());
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic rejects deserializing invalid clove expiration", "[garlic]")
{
  data::Garlic::clove_t clove;
  REQUIRE_NOTHROW(garlic.add_clove(clove));
  REQUIRE_NOTHROW(garlic.serialize());
  REQUIRE_NOTHROW(garlic.deserialize());

  auto& garlic_buf = garlic.buffer();
  tini2p::BytesWriter<data::Garlic::buffer_t> writer(garlic_buf);

  REQUIRE_NOTHROW(writer.skip_bytes(
      data::Garlic::NumLen + data::Garlic::clove_t::instructions_t::FlagsLen + clove.message_size()
      + data::Garlic::clove_t::CloveIDLen));

  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::clove_t::expiration_t(tini2p::time::now_ms()), tini2p::Endian::Big));
  REQUIRE_THROWS(garlic.deserialize());
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic rejects deserializing invalid clove certificate", "[garlic]")
{
  data::Garlic::clove_t clove;
  REQUIRE_NOTHROW(garlic.add_clove(clove));
  REQUIRE_NOTHROW(garlic.serialize());
  REQUIRE_NOTHROW(garlic.deserialize());

  auto& garlic_buf = garlic.buffer();
  tini2p::BytesWriter<data::Garlic::buffer_t> writer(garlic_buf);

  REQUIRE_NOTHROW(writer.skip_bytes(
      data::Garlic::NumLen + data::Garlic::clove_t::instructions_t::FlagsLen + clove.message_size()
      + data::Garlic::clove_t::CloveIDLen + data::Garlic::clove_t::ExpirationLen));

  // Check that only null certificate accepted
  // note: HashCash may be accepted in the future
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::HashCashCert));
  REQUIRE_THROWS(garlic.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::certificate_t::CertTypeLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::HiddenCert));
  REQUIRE_THROWS(garlic.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::certificate_t::CertTypeLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::SignedCert));
  REQUIRE_THROWS(garlic.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::certificate_t::CertTypeLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::MultipleCert));
  REQUIRE_THROWS(garlic.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::certificate_t::CertTypeLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::KeyCert));
  REQUIRE_THROWS(garlic.deserialize());

  // Reset to valid state
  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::certificate_t::CertTypeLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::NullCert));
  REQUIRE_NOTHROW(garlic.deserialize());

  // Check that non-null certificate length throws
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::length_t(1), tini2p::Endian::Big));
  REQUIRE_THROWS(garlic.deserialize());
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic rejects deserializing invalid message certificate", "[garlic]")
{
  data::Garlic::clove_t clove;
  REQUIRE_NOTHROW(garlic.add_clove(clove));
  REQUIRE_NOTHROW(garlic.serialize());
  REQUIRE_NOTHROW(garlic.deserialize());

  auto& garlic_buf = garlic.buffer();
  tini2p::BytesWriter<data::Garlic::buffer_t> writer(garlic_buf);

  REQUIRE_NOTHROW(writer.skip_bytes(data::Garlic::NumLen + clove.size()));

  // Check that only null certificate accepted
  // note: HashCash may be accepted in the future
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::HashCashCert));
  REQUIRE_THROWS(garlic.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::certificate_t::CertTypeLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::HiddenCert));
  REQUIRE_THROWS(garlic.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::certificate_t::CertTypeLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::SignedCert));
  REQUIRE_THROWS(garlic.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::certificate_t::CertTypeLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::MultipleCert));
  REQUIRE_THROWS(garlic.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::certificate_t::CertTypeLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::KeyCert));
  REQUIRE_THROWS(garlic.deserialize());

  // Reset to valid state
  REQUIRE_NOTHROW(writer.skip_back(data::Garlic::certificate_t::CertTypeLen));
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::cert_type_t::NullCert));
  REQUIRE_NOTHROW(garlic.deserialize());

  // Check that non-null certificate length throws
  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::certificate_t::length_t(1), tini2p::Endian::Big));
  REQUIRE_THROWS(garlic.deserialize());
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic rejects deserializing invalid message ID", "[garlic]")
{
  data::Garlic::clove_t clove;
  REQUIRE_NOTHROW(garlic.add_clove(clove));
  REQUIRE_NOTHROW(garlic.serialize());
  REQUIRE_NOTHROW(garlic.deserialize());

  auto& garlic_buf = garlic.buffer();
  tini2p::BytesWriter<data::Garlic::buffer_t> writer(garlic_buf);

  REQUIRE_NOTHROW(writer.skip_bytes(data::Garlic::NumLen + clove.size() + garlic.certificate().size()));

  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::message_id_t(0), tini2p::Endian::Big));
  REQUIRE_THROWS(garlic.deserialize());
}

TEST_CASE_METHOD(GarlicFixture, "data::Garlic rejects deserializing invalid message expiration", "[garlic]")
{
  data::Garlic::clove_t clove;
  REQUIRE_NOTHROW(garlic.add_clove(clove));
  REQUIRE_NOTHROW(garlic.serialize());
  REQUIRE_NOTHROW(garlic.deserialize());

  auto& garlic_buf = garlic.buffer();
  tini2p::BytesWriter<data::Garlic::buffer_t> writer(garlic_buf);

  REQUIRE_NOTHROW(
      writer.skip_bytes(data::Garlic::NumLen + clove.size() + garlic.certificate().size() + data::Garlic::MessageIDLen));

  REQUIRE_NOTHROW(writer.write_bytes(data::Garlic::expiration_t(tini2p::time::now_ms()), tini2p::Endian::Big));
  REQUIRE_THROWS(garlic.deserialize());
}
