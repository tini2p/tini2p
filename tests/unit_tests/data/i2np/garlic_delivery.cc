/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/i2np/garlic_delivery.h"

using GDI = tini2p::data::GarlicDeliveryInstructions;

struct GDIFixture
{
  GDIFixture()
  {
    tini2p::crypto::RandBytes(to_hash);  // simulate "to hash", unrealistic
  }

  GDI gd;
  GDI::hash_t to_hash;
};

TEST_CASE_METHOD(GDIFixture, "GarlicDeliveryInstructions have valid default fields", "[i2np]")
{
  REQUIRE(gd.flags() == tini2p::under_cast(GDI::DeliveryFlags::Local));
  REQUIRE(gd.to_hash().is_zero());
  REQUIRE(gd.tunnel_id() == 0);
  REQUIRE(gd.delay() == 0);
  REQUIRE(gd.size() == GDI::FlagsLen);
}

TEST_CASE_METHOD(GDIFixture, "GarlicDeliveryInstructions creates fully initialized instructions", "[i2np]")
{
  REQUIRE_NOTHROW(GDI(GDI::DeliveryFlags::Destination, to_hash));
  REQUIRE_NOTHROW(GDI(GDI::DeliveryFlags::Router, to_hash));
  REQUIRE_NOTHROW(GDI(GDI::DeliveryFlags::Tunnel, to_hash, GDI::tunnel_id_t(0x42)));
}

TEST_CASE_METHOD(GDIFixture, "GarlicDeliveryInstructions deserializes from a buffer", "[i2np]")
{
  REQUIRE_NOTHROW(gd = GDI(GDI::DeliveryFlags::Tunnel, to_hash, GDI::tunnel_id_t(0x42)));
  std::unique_ptr<GDI> gd_ptr;

  REQUIRE_NOTHROW(gd_ptr.reset(new GDI(gd.buffer().data(), gd.size())));
  REQUIRE(*gd_ptr == gd);

  REQUIRE_NOTHROW(gd_ptr.reset(new GDI(gd.buffer())));
  REQUIRE(*gd_ptr == gd);

  REQUIRE_NOTHROW(gd_ptr->from_buffer(gd.buffer()));
  REQUIRE(*gd_ptr == gd);
}

TEST_CASE_METHOD(GDIFixture, "GarlicDeliveryInstructions rejects invalid full init ctor", "[i2np]")
{
  REQUIRE_THROWS(GDI(GDI::DeliveryFlags::Destination, {}));  // rejects null "to hash"
  REQUIRE_THROWS(GDI(GDI::DeliveryFlags::Local, to_hash));  // rejects Local delivery w/ a "to hash"
  REQUIRE_THROWS(GDI(GDI::DeliveryFlags::Tunnel, to_hash, 0));  // rejects null tunnel ID w/ Tunnel delivery
}

TEST_CASE_METHOD(GDIFixture, "GarlicDeliveryInstructions rejects invalid deserialize ctor", "[i2np]")
{
  REQUIRE_THROWS(GDI(nullptr, GDI::MinLen));
  REQUIRE_THROWS(GDI(gd.buffer().data(), 0));
  REQUIRE_THROWS(GDI(gd.buffer().data(), GDI::MinLen - 1));
  REQUIRE_THROWS(GDI(gd.buffer().data(), GDI::MaxLen + 1));
}
