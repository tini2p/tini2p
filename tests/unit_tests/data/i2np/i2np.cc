/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/i2np.h"

namespace crypto = tini2p::crypto;
namespace data = tini2p::data;

struct I2NPMessageFixture
{
  data::I2NPMessage message;
};

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage has valid fields", "[i2np]")
{
  REQUIRE(message.type() == data::I2NPMessage::Type::Data);
  REQUIRE(message.message_id() != 0);
  REQUIRE(message.expiration() == tini2p::time::now_ms() + data::I2NPMessage::ExpirationMS);
  REQUIRE(message.message_size() == 0);
  REQUIRE(message.checksum() == 0xe3);
  REQUIRE(message.message_buffer().empty());
  REQUIRE(message.size() == data::I2NPMessage::HeaderLen);
  REQUIRE(message.buffer().size() == data::I2NPMessage::HeaderLen);
  REQUIRE(message.mode() == data::I2NPMessage::Mode::Normal);
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage sets the message mode", "[i2np]")
{
  REQUIRE_NOTHROW(message.mode(data::I2NPMessage::Mode::Garlic));
  REQUIRE_NOTHROW(message.mode() == data::I2NPMessage::Mode::Garlic);

  REQUIRE_NOTHROW(message.mode(data::I2NPMessage::Mode::Normal));
  REQUIRE_NOTHROW(message.mode() == data::I2NPMessage::Mode::Normal);

  REQUIRE_NOTHROW(message.mode(data::I2NPMessage::Mode::NTCP2));
  REQUIRE_NOTHROW(message.mode() == data::I2NPMessage::Mode::NTCP2);

  REQUIRE_NOTHROW(message.mode(data::I2NPMessage::Mode::SSU));
  REQUIRE_NOTHROW(message.mode() == data::I2NPMessage::Mode::SSU);
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage serializes and deserializes from buffer", "[i2np]")
{
  // Check default constructed I2NP message serializes and deserializes
  REQUIRE_NOTHROW(message.serialize());
  REQUIRE_NOTHROW(message.deserialize());

  // Check I2NP message w/ message ID and random payload serializes and deserializes
  REQUIRE_NOTHROW(message.message_id(0x42));
  REQUIRE_NOTHROW(message.message_buffer(crypto::RandBuffer(0x20)));

  REQUIRE_NOTHROW(message.serialize());
  REQUIRE_NOTHROW(message.deserialize());

  // Check that an I2NP message can be constructed from a buffer w/ a serialized I2NP message
  std::unique_ptr<data::I2NPMessage> oth_msg;
  REQUIRE_NOTHROW(oth_msg = std::make_unique<data::I2NPMessage>(message.buffer(), message.mode()));
  REQUIRE(*oth_msg == message);
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage serializes a normal message", "[i2np]")
{
  REQUIRE_NOTHROW(message.mode(data::I2NPMessage::Mode::Normal));
  REQUIRE_NOTHROW(message.message_buffer(crypto::RandBuffer(0x20)));
  REQUIRE_NOTHROW(message.serialize());

  auto& msg_buf = message.buffer();
  REQUIRE(msg_buf.size() == data::I2NPMessage::HeaderLen + message.message_size());

  tini2p::BytesReader<crypto::SecBytes> reader(msg_buf);

  data::I2NPMessage::Type type;
  reader.read_bytes(type);
  REQUIRE(type == data::I2NPMessage::Type::Data);

  std::uint32_t msg_id(0);
  reader.read_bytes(msg_id, tini2p::Endian::Big);
  REQUIRE(msg_id == message.message_id());

  std::uint64_t exp(0);
  reader.read_bytes(exp, tini2p::Endian::Big);
  REQUIRE(exp == message.expiration());

  std::uint16_t msg_len(0);
  reader.read_bytes(msg_len, tini2p::Endian::Big);
  REQUIRE(msg_len == msg_buf.size() - (data::I2NPMessage::HeaderLen));

  std::uint8_t checksum(0);
  reader.read_bytes(checksum);
  REQUIRE(checksum == message.checksum());
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage serializes a Garlic message", "[i2np]")
{
  REQUIRE_NOTHROW(message.mode(data::I2NPMessage::Mode::Garlic));
  REQUIRE_NOTHROW(message.message_buffer(crypto::RandBuffer(0x20)));
  REQUIRE_NOTHROW(message.serialize());

  auto& msg_buf = message.buffer();
  REQUIRE(msg_buf.size() == data::I2NPMessage::HeaderLen + data::I2NPMessage::GarlicSizeLen + message.message_size());

  tini2p::BytesReader<crypto::SecBytes> reader(msg_buf);

  data::I2NPMessage::Type type;
  reader.read_bytes(type);
  REQUIRE(type == data::I2NPMessage::Type::Data);

  std::uint32_t msg_id(0);
  reader.read_bytes(msg_id, tini2p::Endian::Big);
  REQUIRE(msg_id == message.message_id());

  std::uint64_t exp(0);
  reader.read_bytes(exp, tini2p::Endian::Big);
  REQUIRE(exp == message.expiration());

  std::uint16_t msg_len(0);
  reader.read_bytes(msg_len, tini2p::Endian::Big);
  REQUIRE(msg_len == msg_buf.size() - data::I2NPMessage::HeaderLen);

  std::uint8_t checksum(0);
  reader.read_bytes(checksum);
  REQUIRE(checksum == message.checksum());

  std::uint32_t enc_len(0);
  reader.read_bytes(enc_len, tini2p::Endian::Big);
  REQUIRE(enc_len == message.message_size());
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage serializes a NTCP2 message", "[i2np]")
{
  REQUIRE_NOTHROW(message.mode(data::I2NPMessage::Mode::NTCP2));
  REQUIRE_NOTHROW(message.serialize());

  auto& msg_buf = message.buffer();
  REQUIRE(msg_buf.size() == data::I2NPMessage::NTCP2HeaderLen + message.message_size());

  tini2p::BytesReader<crypto::SecBytes> reader(msg_buf);

  data::I2NPMessage::Type type;
  reader.read_bytes(type);
  REQUIRE(type == data::I2NPMessage::Type::Data);

  std::uint32_t msg_id(0);
  reader.read_bytes(msg_id, tini2p::Endian::Big);
  REQUIRE(msg_id == message.message_id());

  std::uint32_t exp(0);
  reader.read_bytes(exp, tini2p::Endian::Big);
  REQUIRE(exp == message.expiration());
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage serializes a SSU message", "[i2np]")
{
  REQUIRE_NOTHROW(message.mode(data::I2NPMessage::Mode::SSU));
  REQUIRE_NOTHROW(message.serialize());

  auto& msg_buf = message.buffer();
  REQUIRE(msg_buf.size() == data::I2NPMessage::SSUHeaderLen + message.message_size());

  tini2p::BytesReader<crypto::SecBytes> reader(msg_buf);

  data::I2NPMessage::Type type;
  reader.read_bytes(type);
  REQUIRE(type == data::I2NPMessage::Type::Data);

  std::uint32_t exp(0);
  reader.read_bytes(exp, tini2p::Endian::Big);
  REQUIRE(exp == message.expiration());
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage creates an inner message from buffer", "[i2np]")
{
  data::Data data(0x20);
  const auto data_copy = data;

  REQUIRE_NOTHROW(message.message_buffer(data.buffer()));

  REQUIRE_NOTHROW(data = message.to_message<data::Data>());
  REQUIRE(data == data_copy);
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage rejects setting an invalid type", "[i2np]")
{
  REQUIRE_THROWS(message.type(data::I2NPMessage::Type::Reserved));
  REQUIRE_THROWS(message.type(data::I2NPMessage::Type::FutureReserved));
  REQUIRE_THROWS(message.type(data::I2NPMessage::Type::TunnelBuild));
  REQUIRE_THROWS(message.type(data::I2NPMessage::Type::TunnelBuildReply));
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage rejects setting an invalid mode", "[i2np]")
{
  const auto bad_low_mode =
      static_cast<data::I2NPMessage::Mode>(tini2p::under_cast(data::I2NPMessage::Mode::Normal) - 1);

  const auto bad_hi_mode =
      static_cast<data::I2NPMessage::Mode>(tini2p::under_cast(data::I2NPMessage::Mode::SSU) + 1);

  REQUIRE_THROWS(message.mode(bad_low_mode));
  REQUIRE_THROWS(message.mode(bad_hi_mode));

  REQUIRE_THROWS(data::I2NPMessage(data::Data(), bad_low_mode));
  REQUIRE_THROWS(data::I2NPMessage(data::Data(), bad_hi_mode));
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage rejects setting an invalid expiration", "[i2np]")
{
  REQUIRE_THROWS(message.expiration(0));

  // Check invalid expiration for mode(s) w/ 64-bit expiration
  REQUIRE_THROWS(message.expiration(tini2p::time::now_ms()));

  // Check invalid expiration for mode(s) w/ 32-bit expiration
  REQUIRE_NOTHROW(message.mode(data::I2NPMessage::Mode::NTCP2));
  REQUIRE_THROWS(message.expiration(tini2p::time::now_s()));
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage rejects deserializing invalid type", "[i2np]")
{
  REQUIRE_NOTHROW(message.serialize());

  tini2p::BytesWriter<crypto::SecBytes> writer(message.buffer());

  // write invalid type to the buffer
  REQUIRE_NOTHROW(writer.write_bytes(data::I2NPMessage::Type::Reserved));

  REQUIRE_THROWS(message.deserialize());
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage rejects deserializing invalid expiration", "[i2np]")
{
  REQUIRE_NOTHROW(message.serialize());

  tini2p::BytesWriter<crypto::SecBytes> writer(message.buffer());

  // write null expiration
  REQUIRE_NOTHROW(writer.skip_bytes(data::I2NPMessage::TypeLen + data::I2NPMessage::MessageIDLen));
  REQUIRE_NOTHROW(writer.write_bytes(std::uint64_t(0)));

  REQUIRE_THROWS(message.deserialize());
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage rejects deserializing invalid checksum", "[i2np]")
{
  REQUIRE_NOTHROW(message.serialize());

  tini2p::BytesWriter<crypto::SecBytes> writer(message.buffer());

  // write null checksum
  REQUIRE_NOTHROW(writer.skip_bytes(
      data::I2NPMessage::TypeLen +
      data::I2NPMessage::MessageIDLen +
      data::I2NPMessage::ExpirationSLen +
      data::I2NPMessage::SizeLen));

  REQUIRE_NOTHROW(writer.write_bytes(std::uint8_t(0)));

  REQUIRE_THROWS(message.deserialize());
}

TEST_CASE_METHOD(I2NPMessageFixture, "I2NPMessage rejects deserializing invalid message length", "[i2np]")
{
  REQUIRE_NOTHROW(message.serialize());

  tini2p::BytesWriter<crypto::SecBytes> writer(message.buffer());

  // write invalid message length
  REQUIRE_NOTHROW(writer.skip_bytes(
      data::I2NPMessage::TypeLen +
      data::I2NPMessage::MessageIDLen +
      data::I2NPMessage::ExpirationSLen));

  REQUIRE_NOTHROW(writer.write_bytes(std::uint16_t(0xFFFF), tini2p::Endian::Big));

  REQUIRE_THROWS(message.deserialize());
}
