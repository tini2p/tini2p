/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/database_store.h"

TEST_CASE("DatabaseStore does not verify a default constructed message")
{
  tini2p::data::DatabaseStore dbs;
  REQUIRE(!dbs.Verify());
}

TEST_CASE("DatabaseStore message has valid fields", "[i2np]")
{
  /*----------------------------------------------------------------------\
  | RouterInfo DatabaseStore message (no reply)                           |
  \----------------------------------------------------------------------*/
  tini2p::data::Info::shared_ptr info(new tini2p::data::Info());
  tini2p::data::DatabaseStore dbs_ri(info);

  REQUIRE(dbs_ri.search_key() == info->hash());
  REQUIRE(dbs_ri.type() == tini2p::data::DatabaseStore::Type::RouterInfo);
  REQUIRE(dbs_ri.reply_token() == 0);
  REQUIRE(dbs_ri.reply_tunnel_id() == 0);
  REQUIRE(dbs_ri.reply_gateway() == tini2p::crypto::Sha256::digest_t());
  REQUIRE(*(std::get<tini2p::data::Info::shared_ptr>(dbs_ri.entry_data())) == *info);

  /*----------------------------------------------------------------------\
  | LeaseSet2 DatabaseStore message (no reply)                            |
  \----------------------------------------------------------------------*/
  tini2p::data::LeaseSet2::shared_ptr ls(new tini2p::data::LeaseSet2());
  tini2p::data::DatabaseStore dbs_ls(ls);

  REQUIRE(dbs_ls.search_key() == ls->hash());
  REQUIRE(dbs_ls.type() == tini2p::data::DatabaseStore::Type::LeaseSet2);
  REQUIRE(dbs_ls.reply_token() == 0);
  REQUIRE(dbs_ls.reply_tunnel_id() == 0);
  REQUIRE(dbs_ls.reply_gateway() == tini2p::crypto::Sha256::digest_t());
  REQUIRE(*(std::get<tini2p::data::LeaseSet2::shared_ptr>(dbs_ls.entry_data())) == *ls);

  /*----------------------------------------------------------------------\
  | Reply DatabaseStore message                                           |
  \----------------------------------------------------------------------*/
  tini2p::data::DatabaseStore::reply_tunnel_id_t tunnel_id(0x42);
  tini2p::data::Info reply_ri;
  tini2p::data::DatabaseStore::reply_gateway_t gateway(reply_ri.hash());
  REQUIRE_NOTHROW(dbs_ri.reply(tunnel_id, gateway));

  REQUIRE(dbs_ri.search_key() == info->hash());
  REQUIRE(dbs_ri.type() == tini2p::data::DatabaseStore::Type::RouterInfo);
  REQUIRE(dbs_ri.reply_token() != 0);
  REQUIRE(dbs_ri.reply_tunnel_id() == tunnel_id);
  REQUIRE(dbs_ri.reply_gateway() == gateway);
  REQUIRE(*(std::get<tini2p::data::Info::shared_ptr>(dbs_ri.entry_data())) == *info);
}

TEST_CASE("DatabaseStore deserializes from a buffer", "[i2np]")
{
  /*----------------------------------------------------------------------\
  | RouterInfo DatabaseStore message deserialization                      |
  \----------------------------------------------------------------------*/
  tini2p::data::Info::shared_ptr info(new tini2p::data::Info());
  auto dbs = std::make_unique<tini2p::data::DatabaseStore>(info);

  const auto& dbs_buf = dbs->buffer();

  REQUIRE_NOTHROW(tini2p::data::DatabaseStore(dbs_buf));
  REQUIRE_NOTHROW(tini2p::data::DatabaseStore(dbs_buf.data(), dbs_buf.size()));

  /*----------------------------------------------------------------------\
  | LeaseSet2 DatabaseStore message deserialization                       |
  \----------------------------------------------------------------------*/
  tini2p::data::LeaseSet2::shared_ptr ls(new tini2p::data::LeaseSet2());
  dbs.reset(new tini2p::data::DatabaseStore(ls));

  REQUIRE_NOTHROW(tini2p::data::DatabaseStore(dbs->buffer()));
}

TEST_CASE("DatabaseStore rejects invalid buffer", "[i2np]")
{
  tini2p::data::DatabaseStore::buffer_t buf;
  REQUIRE_THROWS(tini2p::data::DatabaseStore(nullptr, tini2p::data::DatabaseStore::MinLen));
  REQUIRE_THROWS(tini2p::data::DatabaseStore(buf.data(), 0));

  buf.resize(tini2p::data::DatabaseStore::MaxLen + 1);
  REQUIRE_THROWS(tini2p::data::DatabaseStore(buf.data(), buf.size()));
}
