/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/i2np/tunnel_message.h"

using namespace tini2p::crypto;

using AESMessage = tini2p::data::TunnelMessage<TunnelAES>;
using ChaChaMessage = tini2p::data::TunnelMessage<TunnelChaCha>;
using Layer = tini2p::data::Layer;

/*-------------------------\
| AES Tunnel Message Tests |
\-------------------------*/

struct AESMessageFixture
{
  AESMessageFixture()
      : tunnel_id(0x42),
        iv(RandBytes<AESMessage::aes_layer_t::aes_t::IVLen>()),
        first_frag_len(66),
        msg_id(13),
        first_dlv(Layer::AES, AESMessage::first_delivery_t::mode_t::Fragmented, first_frag_len, msg_id),
        frag_num(1),
        follow_frag_len(99),
        follow_dlv(Layer::AES, frag_num, *msg_id, follow_frag_len),
        first_fragment(RandBuffer(first_frag_len)),
        follow_fragment(RandBuffer(follow_frag_len)),
        message(tunnel_id, iv, first_dlv, first_fragment)
  {
    REQUIRE_NOTHROW(message.add_fragment(follow_dlv, follow_fragment));
  }

  AESMessage::tunnel_id_t tunnel_id;
  AESMessage::iv_t iv;
  AESMessage::first_delivery_t::size_type first_frag_len;
  std::optional<AESMessage::first_delivery_t::message_id_t> msg_id;
  AESMessage::first_delivery_t first_dlv;

  AESMessage::follow_delivery_t::frag_num_t frag_num;
  AESMessage::follow_delivery_t::size_type follow_frag_len;
  AESMessage::follow_delivery_t follow_dlv;
  AESMessage::fragment_t first_fragment, follow_fragment;

  AESMessage message;
  std::unique_ptr<AESMessage> message_ptr;
};

TEST_CASE_METHOD(AESMessageFixture, "AES TunnelMessage has valid fields", "[aes_tun_msg]")
{
  REQUIRE(message.hop_tunnel_id() == tunnel_id);
  REQUIRE(message.iv() == iv);

  const auto& deliveries = message.deliveries();
  const auto& fragments = message.fragments();

  REQUIRE(deliveries.size() == 2);
  REQUIRE(fragments.size() == 2);

  REQUIRE(std::get<AESMessage::first_delivery_t>(deliveries[0]) == first_dlv);
  REQUIRE(fragments[0] == first_fragment);

  REQUIRE(std::get<AESMessage::follow_delivery_t>(deliveries[1]) == follow_dlv);
  REQUIRE(fragments[1] == follow_fragment);

  // Serialize message to generate checksum and random padding
  REQUIRE_NOTHROW(message.serialize());

  // Check that checksum is calculated correctly
  REQUIRE(message.checksum() == message.CalculateChecksum(deliveries, fragments, iv));

  const auto& padding = message.padding();
  const auto& pad_delim = tini2p::under_cast(AESMessage::PadDelim);

  // Check that padding is non-zero
  for (const auto& pad : padding)
    REQUIRE(pad != pad_delim);
}

TEST_CASE_METHOD(AESMessageFixture, "AES TunnelMessage serializes and deserializes from buffer", "[aes_tun_msg]")
{
  REQUIRE_NOTHROW(message.serialize());
  REQUIRE_NOTHROW(message_ptr = std::make_unique<AESMessage>(message.buffer()));
  const auto& msg_eq = *message_ptr == message;
  REQUIRE(msg_eq);
}

TEST_CASE_METHOD(AESMessageFixture, "AES TunnelMessage adds a fragment", "[aes_tun_msg]")
{
  msg_id.emplace(*msg_id + 1);

  // Simulate adding a first fragment from another message
  AESMessage::first_delivery_t first(Layer::AES, AESMessage::first_delivery_t::mode_t::Fragmented, first_frag_len, msg_id);

  REQUIRE_NOTHROW(first_fragment = RandBuffer(first_frag_len));
  REQUIRE_NOTHROW(message.add_fragment(first, first_fragment));

  // Simulate adding a follow-on fragment from the same message as above
  AESMessage::follow_delivery_t follow(Layer::AES, frag_num, *msg_id, follow_frag_len);

  REQUIRE_NOTHROW(follow_fragment = RandBuffer(follow_frag_len));
  REQUIRE_NOTHROW(message.add_fragment(follow, follow_fragment));
}

TEST_CASE_METHOD(AESMessageFixture, "AES TunnelMessage rejects null tunnel ID", "[aes_tun_msg]")
{
  REQUIRE_THROWS(AESMessage(0, iv, first_dlv, first_fragment));
  REQUIRE_THROWS(AESMessage(0, iv, follow_dlv, follow_fragment));

  auto& msg_buf = message.buffer();
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_buf.data(), AESMessage::tunnel_id_t(0)));
  REQUIRE_THROWS(message.deserialize());
}

TEST_CASE_METHOD(AESMessageFixture, "AES TunnelMessage rejects null tunnel IV", "[aes_tun_msg]")
{
  REQUIRE_THROWS(AESMessage(tunnel_id, {}, first_dlv, first_fragment));
  REQUIRE_THROWS(AESMessage(tunnel_id, {}, follow_dlv, follow_fragment));

  auto& msg_buf = message.buffer();
  AESMessage::iv_t null_iv{};

  tini2p::BytesWriter<AESMessage::buffer_t> writer(msg_buf);
  REQUIRE_NOTHROW(writer.skip_bytes(tini2p::under_cast(AESMessage::TunnelIDLen)));
  REQUIRE_NOTHROW(writer.write_data(null_iv));
  REQUIRE_THROWS(message.deserialize());
}

TEST_CASE_METHOD(AESMessageFixture, "AES TunnelMessage rejects invalid checksum", "[aes_tun_msg]")
{
  auto& msg_buf = message.buffer();

  const auto& check_offset = tini2p::under_cast(AESMessage::ClearHeaderLen);
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_buf.data() + check_offset, message.checksum() + 1));
  REQUIRE_THROWS(message.deserialize());
}

TEST_CASE_METHOD(AESMessageFixture, "AES TunnelMessage rejects invalid delivery instructions", "[aes_tun_msg]")
{
  // Rejects duplicate fragments
  REQUIRE_THROWS(message.add_fragment(first_dlv, first_fragment));
  REQUIRE_THROWS(message.add_fragment(follow_dlv, follow_fragment));

  // Add non-duplicate fragment
  AESMessage::follow_delivery_t dup_follow(Layer::AES, frag_num + 1, *msg_id, follow_frag_len);
  REQUIRE_NOTHROW(message.add_fragment(dup_follow, follow_fragment));

  // Serialize to recalculate padding and checksum
  REQUIRE_NOTHROW(message.serialize());

  auto& msg_buf = message.buffer();
  const auto& pad_delim = tini2p::under_cast(AESMessage::PadDelim);

  tini2p::BytesWriter<AESMessage::buffer_t> writer(msg_buf);
  writer.skip_bytes(AESMessage::ClearHeaderLen + AESMessage::ChecksumLen);

  // Find end of padding
  const auto it = std::find_if(
      msg_buf.begin() + writer.count(), msg_buf.end(), [&pad_delim](const auto& b) { return b == pad_delim; });

  // Go to end of first two valid fragments
  const auto body_start = it - msg_buf.begin();
  writer.reset();
  writer.skip_bytes(body_start + first_dlv.size() + first_frag_len + follow_dlv.size() + follow_frag_len);

  // Overwrite second follow-on instructions with duplicate info to throw during deserialization
  writer.write_data(follow_dlv.buffer());

  // Recalculate checksum to get past check in deserialization
  AESMessage::deliveries_t dup_dlvs{{first_dlv, follow_dlv, follow_dlv}};
  AESMessage::fragments_t dup_frags{{first_fragment, follow_fragment, follow_fragment}};
  const auto dup_checksum = message.CalculateChecksum(dup_dlvs, dup_frags, iv);

  // Write checksum for duplicate fragments back to buffer
  writer.reset();
  writer.skip_bytes(AESMessage::ClearHeaderLen);
  writer.write_bytes(dup_checksum);
  
  // Rejects the duplicate fragment
  REQUIRE_THROWS(message.deserialize());

  const auto& max_fragment_len = AESMessage::follow_delivery_t::max_fragment_size(Layer::AES);
  AESMessage::follow_delivery_t follow(Layer::AES, frag_num + 1, *msg_id, max_fragment_len);
  REQUIRE_NOTHROW(follow_fragment = RandBuffer(max_fragment_len));

  // Check message doesn't throw on empty message when adding max fragment length
  REQUIRE_NOTHROW(AESMessage(tunnel_id, iv, follow, follow_fragment));

  // Rejects adding a fragment when adding it exceeds max message body length
  REQUIRE_THROWS(message.add_fragment(follow, follow_fragment));
}

/*----------------------------\
| ChaCha Tunnel Message Tests |
\----------------------------*/

struct ChaChaMessageFixture
{
  ChaChaMessageFixture()
      : tunnel_id(0x42),
        nonces(ChaChaMessage::chacha_layer_t::create_nonces()),
        first_frag_len(66),
        msg_id(13),
        first_dlv(Layer::ChaCha, ChaChaMessage::first_delivery_t::mode_t::Fragmented, first_frag_len, msg_id),
        frag_num(1),
        follow_frag_len(99),
        follow_dlv(Layer::ChaCha, frag_num, *msg_id, follow_frag_len),
        first_fragment(RandBuffer(first_frag_len)),
        follow_fragment(RandBuffer(follow_frag_len)),
        message(tunnel_id, nonces, first_dlv, first_fragment)
  {
    REQUIRE_NOTHROW(message.add_fragment(follow_dlv, follow_fragment));
  }

  ChaChaMessage::tunnel_id_t tunnel_id;
  ChaChaMessage::nonces_t nonces;
  ChaChaMessage::first_delivery_t::size_type first_frag_len;
  std::optional<ChaChaMessage::first_delivery_t::message_id_t> msg_id;
  ChaChaMessage::first_delivery_t first_dlv;

  ChaChaMessage::follow_delivery_t::frag_num_t frag_num;
  ChaChaMessage::follow_delivery_t::size_type follow_frag_len;
  ChaChaMessage::follow_delivery_t follow_dlv;
  ChaChaMessage::fragment_t first_fragment, follow_fragment;

  ChaChaMessage message;
  std::unique_ptr<ChaChaMessage> message_ptr;
};

TEST_CASE_METHOD(ChaChaMessageFixture, "ChaCha TunnelMessage has valid fields", "[chacha_tun_msg]")
{
  REQUIRE(message.hop_tunnel_id() == tunnel_id);
  REQUIRE(message.tunnel_nonce() == nonces.tunnel_nonce);
  REQUIRE(message.obfs_nonce() == nonces.obfs_nonce);

  const auto& deliveries = message.deliveries();
  const auto& fragments = message.fragments();

  REQUIRE(deliveries.size() == 2);
  REQUIRE(fragments.size() == 2);

  REQUIRE(std::get<ChaChaMessage::first_delivery_t>(deliveries[0]) == first_dlv);
  REQUIRE(fragments[0] == first_fragment);

  REQUIRE(std::get<ChaChaMessage::follow_delivery_t>(deliveries[1]) == follow_dlv);
  REQUIRE(fragments[1] == follow_fragment);

  // Serialize message to generate checksum and random padding
  REQUIRE_NOTHROW(message.serialize());

  // Check that checksum is calculated correctly
  REQUIRE(message.checksum() == message.CalculateChecksum(deliveries, fragments, nonces));

  const auto& padding = message.padding();
  const auto& pad_delim = tini2p::under_cast(ChaChaMessage::PadDelim);

  // Check that padding is non-zero
  for (const auto& pad : padding)
    REQUIRE(pad != pad_delim);
}

TEST_CASE_METHOD(
    ChaChaMessageFixture,
    "ChaCha TunnelMessage serializes and deserializes from buffer",
    "[chacha_tun_msg]")
{
  REQUIRE_NOTHROW(message.serialize());
  REQUIRE_NOTHROW(message_ptr = std::make_unique<ChaChaMessage>(message.buffer()));

  const auto& msg_eq = *message_ptr == message;
  REQUIRE(msg_eq);
}

TEST_CASE_METHOD(ChaChaMessageFixture, "ChaCha TunnelMessage adds a fragment", "[chacha_tun_msg]")
{
  msg_id.emplace(*msg_id + 1);

  // Simulate adding a first fragment from another message
  ChaChaMessage::first_delivery_t first(Layer::ChaCha, ChaChaMessage::first_delivery_t::mode_t::Fragmented, first_frag_len, msg_id);

  REQUIRE_NOTHROW(first_fragment = RandBuffer(first_frag_len));
  REQUIRE_NOTHROW(message.add_fragment(first, first_fragment));

  // Simulate adding a follow-on fragment from the same message as above
  ChaChaMessage::follow_delivery_t follow(Layer::ChaCha, frag_num, *msg_id, follow_frag_len);

  REQUIRE_NOTHROW(follow_fragment = RandBuffer(follow_frag_len));
  REQUIRE_NOTHROW(message.add_fragment(follow, follow_fragment));
}

TEST_CASE_METHOD(ChaChaMessageFixture, "ChaCha TunnelMessage rejects null tunnel ID", "[chacha_tun_msg]")
{
  REQUIRE_THROWS(ChaChaMessage(0, nonces, first_dlv, first_fragment));
  REQUIRE_THROWS(ChaChaMessage(0, nonces, follow_dlv, follow_fragment));

  auto& msg_buf = message.buffer();
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_buf.data(), ChaChaMessage::tunnel_id_t(0)));
  REQUIRE_THROWS(message.deserialize());
}

TEST_CASE_METHOD(ChaChaMessageFixture, "ChaCha TunnelMessage rejects invalid tunnel nonces", "[chacha_tun_msg]")
{
  // Rejects null nonces
  REQUIRE_THROWS(ChaChaMessage(tunnel_id, ChaChaMessage::nonces_t{{}, nonces.obfs_nonce}, first_dlv, first_fragment));
  REQUIRE_THROWS(
      ChaChaMessage(tunnel_id, ChaChaMessage::nonces_t{nonces.tunnel_nonce, {}}, follow_dlv, follow_fragment));

  // Rejects duplicate nonces
  REQUIRE_THROWS(ChaChaMessage(
      tunnel_id, ChaChaMessage::nonces_t{nonces.tunnel_nonce, nonces.tunnel_nonce}, follow_dlv, follow_fragment));

  REQUIRE_THROWS(ChaChaMessage(
      tunnel_id, ChaChaMessage::nonces_t{nonces.obfs_nonce, nonces.obfs_nonce}, follow_dlv, follow_fragment));

  auto& msg_buf = message.buffer();
  ChaChaMessage::nonce_t null_nonce{};

  tini2p::BytesWriter<ChaChaMessage::buffer_t> writer(msg_buf);

  // Rejects deserializing null nonces
  REQUIRE_NOTHROW(writer.skip_bytes(tini2p::under_cast(ChaChaMessage::TunnelIDLen)));
  REQUIRE_NOTHROW(writer.write_data(static_cast<typename ChaChaMessage::nonce_t::buffer_t>(null_nonce)));
  REQUIRE_THROWS(message.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(null_nonce.size()));
  REQUIRE_NOTHROW(writer.write_data(static_cast<typename ChaChaMessage::nonce_t::buffer_t>(nonces.tunnel_nonce)));
  REQUIRE_NOTHROW(writer.write_data(static_cast<typename ChaChaMessage::nonce_t::buffer_t>(null_nonce)));
  REQUIRE_THROWS(message.deserialize());

  // Rejects deserializing duplicate nonces
  REQUIRE_NOTHROW(writer.skip_back(null_nonce.size()));
  REQUIRE_NOTHROW(writer.write_data(static_cast<typename ChaChaMessage::nonce_t::buffer_t>(nonces.tunnel_nonce)));
  REQUIRE_THROWS(message.deserialize());
}

TEST_CASE_METHOD(ChaChaMessageFixture, "ChaCha TunnelMessage rejects invalid checksum", "[chacha_tun_msg]")
{
  auto& msg_buf = message.buffer();

  const auto& check_offset = tini2p::under_cast(ChaChaMessage::ClearHeaderLen);
  REQUIRE_NOTHROW(tini2p::write_bytes(msg_buf.data() + check_offset, message.checksum() + 1));
  REQUIRE_THROWS(message.deserialize());
}

TEST_CASE_METHOD(ChaChaMessageFixture, "ChaCha TunnelMessage rejects invalid delivery instructions", "[chacha_tun_msg]")
{
  // Rejects duplicate fragments
  REQUIRE_THROWS(message.add_fragment(first_dlv, first_fragment));
  REQUIRE_THROWS(message.add_fragment(follow_dlv, follow_fragment));

  // Add non-duplicate fragment
  ChaChaMessage::follow_delivery_t dup_follow(Layer::ChaCha, frag_num + 1, *msg_id, follow_frag_len);
  REQUIRE_NOTHROW(message.add_fragment(dup_follow, follow_fragment));

  // Serialize to recalculate padding and checksum
  REQUIRE_NOTHROW(message.serialize());

  auto& msg_buf = message.buffer();
  const auto& pad_delim = tini2p::under_cast(ChaChaMessage::PadDelim);

  tini2p::BytesWriter<ChaChaMessage::buffer_t> writer(msg_buf);
  writer.skip_bytes(ChaChaMessage::ClearHeaderLen + ChaChaMessage::ChecksumLen);

  // Find end of padding
  const auto it = std::find_if(
      msg_buf.begin() + writer.count(), msg_buf.end(), [&pad_delim](const auto& b) { return b == pad_delim; });

  // Go to end of first two valid fragments
  const auto body_start = it - msg_buf.begin();
  writer.reset();
  writer.skip_bytes(body_start + first_dlv.size() + first_frag_len + follow_dlv.size() + follow_frag_len);

  // Overwrite second follow-on instructions with duplicate info to throw during deserialization
  writer.write_data(follow_dlv.buffer());

  // Recalculate checksum to get past check in deserialization
  ChaChaMessage::deliveries_t dup_dlvs{{first_dlv, follow_dlv, follow_dlv}};
  ChaChaMessage::fragments_t dup_frags{{first_fragment, follow_fragment, follow_fragment}};
  const auto dup_checksum = message.CalculateChecksum(dup_dlvs, dup_frags, nonces);

  // Write checksum for duplicate fragments back to buffer
  writer.reset();
  writer.skip_bytes(ChaChaMessage::ClearHeaderLen);
  writer.write_bytes(dup_checksum);
  
  // Rejects the duplicate fragment
  REQUIRE_THROWS(message.deserialize());

  const auto& max_fragment_len = ChaChaMessage::follow_delivery_t::max_fragment_size(Layer::ChaCha);
  ChaChaMessage::follow_delivery_t follow(Layer::ChaCha, frag_num + 1, *msg_id, max_fragment_len);
  REQUIRE_NOTHROW(follow_fragment = RandBuffer(max_fragment_len));

  // Check message doesn't throw on empty message when adding max fragment length
  REQUIRE_NOTHROW(ChaChaMessage(tunnel_id, nonces, follow, follow_fragment));

  // Rejects adding a fragment when adding it exceeds max message body length
  REQUIRE_THROWS(message.add_fragment(follow, follow_fragment));
}

/*--------------------------------\
| Tunnel Message Fragmenter Tests |
\--------------------------------*/

using MessageBody = tini2p::data::TunnelMessageBody;
using FirstDelivery = tini2p::data::FirstDelivery;
using FollowDelivery = tini2p::data::FollowDelivery;

using AESFragmenter = tini2p::data::TunnelMessageFragmenter<TunnelAES>;

struct AESFragmenterFixture
{
  AESFragmenterFixture() : i2np_buf(RandBuffer(63744)), tunnel_id(42), to_hash(RandBytes<FirstDelivery::ToHashLen>()) {}

  typename AESFragmenter::i2np_buffer_t i2np_buf;
  std::optional<typename AESFragmenter::tunnel_id_t> tunnel_id;
  std::optional<typename AESFragmenter::to_hash_t> to_hash;
  MessageBody body;
};

TEST_CASE_METHOD(AESFragmenterFixture, "AES Tunnel fragmenter creates tunnel fragments from I2NP buffer", "[aes_frag]")
{
  REQUIRE_NOTHROW(body = AESFragmenter::FragmentI2NP(FirstDelivery::Delivery::Local, i2np_buf));

  // Max follow-on fragments, and the first fragment
  const auto& max_fragments = tini2p::under_cast(FollowDelivery::MaxFragmentNum) + 1;

  REQUIRE(body.deliveries.size() == max_fragments);
  REQUIRE(body.fragments.size() == max_fragments);

  // Resize for max fragmented router total length
  REQUIRE_NOTHROW(i2np_buf.resize(63712));
  REQUIRE_NOTHROW(body = AESFragmenter::FragmentI2NP(FirstDelivery::Delivery::Router, i2np_buf, to_hash));

  REQUIRE(body.deliveries.size() == max_fragments);
  REQUIRE(body.fragments.size() == max_fragments);

  // Resize for max fragmented tunnel delivery total length
  REQUIRE_NOTHROW(i2np_buf.resize(63708));
  REQUIRE_NOTHROW(body = AESFragmenter::FragmentI2NP(FirstDelivery::Delivery::Tunnel, i2np_buf, to_hash, tunnel_id));

  // Check that there are max follow-on fragments + first fragment
  REQUIRE(body.deliveries.size() == max_fragments);
  REQUIRE(body.fragments.size() == max_fragments);
}

TEST_CASE_METHOD(AESFragmenterFixture, "AES Tunnel fragmenter rejects an I2NP buffer that is too large", "[aes_frag]")
{
  // Max fragmented local delivery total length + 1
  REQUIRE_NOTHROW(i2np_buf = RandBuffer(63745));
  REQUIRE_THROWS(body = AESFragmenter::FragmentI2NP(FirstDelivery::Delivery::Local, i2np_buf));

  // Max fragmented router delivery total length + 1
  REQUIRE_NOTHROW(i2np_buf.resize(63713));
  REQUIRE_THROWS(body = AESFragmenter::FragmentI2NP(FirstDelivery::Delivery::Router, i2np_buf, to_hash));

  // Max fragmented tunnel delivery total length + 1
  REQUIRE_NOTHROW(i2np_buf.resize(63709));
  REQUIRE_THROWS(body = AESFragmenter::FragmentI2NP(FirstDelivery::Delivery::Tunnel, i2np_buf, to_hash, tunnel_id));
}

using ChaChaFragmenter = tini2p::data::TunnelMessageFragmenter<TunnelChaCha>;

struct ChaChaFragmenterFixture
{
  ChaChaFragmenterFixture() : i2np_buf(RandBuffer(62720)), tunnel_id(42), to_hash(RandBytes<FirstDelivery::ToHashLen>())
  {
  }

  typename ChaChaFragmenter::i2np_buffer_t i2np_buf;
  std::optional<typename ChaChaFragmenter::tunnel_id_t> tunnel_id;
  std::optional<typename ChaChaFragmenter::to_hash_t> to_hash;
  MessageBody body;
};

TEST_CASE_METHOD(
    ChaChaFragmenterFixture,
    "ChaCha Tunnel fragmenter creates tunnel fragments from I2NP buffer",
    "[chacha_frag]")
{
  REQUIRE_NOTHROW(body = ChaChaFragmenter::FragmentI2NP(FirstDelivery::Delivery::Local, i2np_buf));

  // Max follow-on fragments, and the first fragment
  const auto& max_fragments = tini2p::under_cast(FollowDelivery::MaxFragmentNum) + 1;

  REQUIRE(body.deliveries.size() == max_fragments);
  REQUIRE(body.fragments.size() == max_fragments);

  // Resize for max fragmented router total length
  REQUIRE_NOTHROW(i2np_buf.resize(62688));
  REQUIRE_NOTHROW(body = ChaChaFragmenter::FragmentI2NP(FirstDelivery::Delivery::Router, i2np_buf, to_hash));

  REQUIRE(body.deliveries.size() == max_fragments);
  REQUIRE(body.fragments.size() == max_fragments);

  // Resize for max fragmented tunnel delivery total length
  REQUIRE_NOTHROW(i2np_buf.resize(62684));
  REQUIRE_NOTHROW(body = ChaChaFragmenter::FragmentI2NP(FirstDelivery::Delivery::Tunnel, i2np_buf, to_hash, tunnel_id));

  // Check that there are max follow-on fragments + first fragment
  REQUIRE(body.deliveries.size() == max_fragments);
  REQUIRE(body.fragments.size() == max_fragments);
}

TEST_CASE_METHOD(
    ChaChaFragmenterFixture,
    "ChaCha Tunnel fragmenter rejects an I2NP buffer that is too large",
    "[chacha_frag]")
{
  // Max fragmented local delivery total length + 1
  REQUIRE_NOTHROW(i2np_buf = RandBuffer(62721));
  REQUIRE_THROWS(body = ChaChaFragmenter::FragmentI2NP(FirstDelivery::Delivery::Local, i2np_buf));

  // Max fragmented router delivery total length + 1
  REQUIRE_NOTHROW(i2np_buf.resize(62689));
  REQUIRE_THROWS(body = ChaChaFragmenter::FragmentI2NP(FirstDelivery::Delivery::Router, i2np_buf, to_hash));

  // Max fragmented tunnel delivery total length + 1
  REQUIRE_NOTHROW(i2np_buf.resize(62685));
  REQUIRE_THROWS(body = ChaChaFragmenter::FragmentI2NP(FirstDelivery::Delivery::Tunnel, i2np_buf, to_hash, tunnel_id));
}
