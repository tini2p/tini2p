/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/i2np/garlic_clove.h"
#include "src/data/i2np/i2np.h"

using GarlicClove = tini2p::data::GarlicClove;
using GDI = tini2p::data::GarlicDeliveryInstructions;

// supported I2NP messages
using tini2p::data::Data;
using tini2p::data::DatabaseStore;
using tini2p::data::DeliveryStatus;

struct GarlicCloveFixture
{
  GDI instructions;
  std::unique_ptr<GarlicClove> clove_ptr;
};

TEST_CASE_METHOD(GarlicCloveFixture, "GarlicClove created from valid instruction + I2NP message", "[clove]")
{
  const auto check_fields = [this](const auto& msg) {
    using msg_t = std::decay_t<decltype(msg)>;

    REQUIRE(clove_ptr->instructions() == instructions);
    REQUIRE(clove_ptr->extract_message<msg_t>() == msg);
    REQUIRE(clove_ptr->id() != GarlicClove::NullID);
    REQUIRE(clove_ptr->expiration() >= tini2p::time::now_ms());
    REQUIRE(clove_ptr->cert() == tini2p::data::Certificate(tini2p::data::Certificate::cert_type_t::NullCert));
  };

  const Data data;
  REQUIRE_NOTHROW(clove_ptr = std::make_unique<GarlicClove>(instructions, data));
  check_fields(data);

  const DatabaseStore dbs(std::make_shared<tini2p::data::Info>());
  REQUIRE_NOTHROW(clove_ptr = std::make_unique<GarlicClove>(instructions, dbs));
  check_fields(dbs);

  const DeliveryStatus dlv(0x42);
  instructions.set_delivery(GarlicClove::instructions_t::delivery_t::Router, tini2p::crypto::RandBytes<32>());
  REQUIRE_NOTHROW(clove_ptr = std::make_unique<GarlicClove>(instructions, dlv));
  check_fields(dlv);
}

TEST_CASE_METHOD(GarlicCloveFixture, "GarlicClove created from deserializing a valid buffer", "[clove]")
{
  DatabaseStore dbs(std::make_shared<tini2p::data::LeaseSet2>());
  REQUIRE_NOTHROW(clove_ptr = std::make_unique<GarlicClove>(instructions, dbs));

  std::unique_ptr<GarlicClove> n_clove_ptr;
  REQUIRE_NOTHROW(n_clove_ptr = std::make_unique<GarlicClove>(clove_ptr->buffer()));
  REQUIRE(*n_clove_ptr == *clove_ptr);
}

TEST_CASE_METHOD(GarlicCloveFixture, "GarlicClove rejects creation from invalid I2NP message", "[clove]")
{
  // no need to test invalid serializing ctor, will fail to compile if passed invalid variant alternative
  REQUIRE_NOTHROW(clove_ptr = std::make_unique<GarlicClove>(instructions, Data()));

  /*-----------------------------------------------\
   | Reset I2NP message type to unsupported types  |
   *----------------------------------------------*/
  const auto i2np_type_offset = instructions.size();

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPMessage::Type::DatabaseSearchReply);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPMessage::Type::DatabaseLookup);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPMessage::Type::Garlic);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPMessage::Type::TunnelData);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPMessage::Type::TunnelGateway);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPMessage::Type::TunnelBuild);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPMessage::Type::TunnelBuildReply);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPMessage::Type::VariableTunnelBuild);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPMessage::Type::VariableTunnelBuildReply);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPMessage::Type::Reserved);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));

  clove_ptr->buffer()[i2np_type_offset] = tini2p::under_cast(tini2p::data::I2NPMessage::Type::FutureReserved);
  REQUIRE_THROWS(GarlicClove(clove_ptr->buffer()));
}
