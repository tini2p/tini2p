/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/i2np/database_search_reply.h"

using tini2p::data::DatabaseSearchReply;

struct DatabaseSearchReplyFixture
{
  DatabaseSearchReplyFixture()
  {
    // mock random keys, unrealistic
    tini2p::crypto::RandBytes(search_key);
    tini2p::crypto::RandBytes(peer_key);
    tini2p::crypto::RandBytes(from_key);
  }

  DatabaseSearchReply::search_key_t search_key;
  DatabaseSearchReply::peer_key_t peer_key;
  DatabaseSearchReply::from_key_t from_key;

  std::unique_ptr<DatabaseSearchReply> dbsr_ptr;
};

TEST_CASE_METHOD(DatabaseSearchReplyFixture, "DatabaseSearchReply has valid fields", "[i2np]")
{
  REQUIRE_NOTHROW(dbsr_ptr.reset(new DatabaseSearchReply(search_key, from_key)));
  REQUIRE(dbsr_ptr->search_key() == search_key);
  REQUIRE(dbsr_ptr->peer_keys().empty());
  REQUIRE(dbsr_ptr->from_key() == from_key);

  REQUIRE_NOTHROW(dbsr_ptr.reset(new DatabaseSearchReply(search_key, from_key, {peer_key})));
  REQUIRE(dbsr_ptr->search_key() == search_key);
  REQUIRE(dbsr_ptr->peer_keys().size() == 1);
  REQUIRE(dbsr_ptr->from_key() == from_key);
}

TEST_CASE_METHOD(DatabaseSearchReplyFixture, "DatabaseSearchReply deserializes from buffer", "[i2np]")
{
  std::unique_ptr<DatabaseSearchReply> dbsr_des_ptr;
  REQUIRE_NOTHROW(dbsr_ptr.reset(new DatabaseSearchReply(search_key, from_key)));
  REQUIRE_NOTHROW(dbsr_des_ptr.reset(new DatabaseSearchReply(dbsr_ptr->buffer())));

  REQUIRE(*dbsr_ptr == *dbsr_des_ptr);
}

TEST_CASE_METHOD(DatabaseSearchReplyFixture, "DatabaseSearchReply rejects null keys", "[i2np]")
{
  REQUIRE_THROWS(DatabaseSearchReply({}, from_key));
  REQUIRE_THROWS(DatabaseSearchReply(search_key, {}));

  peer_key.zero();  // zero the peer key, closest router hash will never be null
  REQUIRE_THROWS(DatabaseSearchReply(search_key, from_key, {peer_key}));
  REQUIRE_THROWS(DatabaseSearchReply(search_key, from_key).peer_keys({peer_key}));
}

TEST_CASE_METHOD(DatabaseSearchReplyFixture, "DatabaseSearchReply rejects excessive peer keys", "[i2np]")
{
  std::vector<DatabaseSearchReply::peer_key_t> peer_keys(DatabaseSearchReply::MaxPeers + 1);
  for (auto& peer : peer_keys)
    tini2p::crypto::RandBytes(peer);

  REQUIRE_THROWS(DatabaseSearchReply(search_key, from_key, peer_keys));
  REQUIRE_THROWS(DatabaseSearchReply(search_key, from_key).peer_keys(peer_keys));
}
