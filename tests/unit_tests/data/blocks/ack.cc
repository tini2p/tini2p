/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/blocks/ack.h"

using tini2p::data::AckBlock;

struct AckBlockFixture
{
  AckBlock block;
};

TEST_CASE_METHOD(AckBlockFixture, "AckBlock has a valid fields", "[block]")
{
  REQUIRE(block.type() == AckBlock::type_t::Ack);
  REQUIRE(block.size() == AckBlock::HeaderLen);
  REQUIRE(block.data_size() == 0);
  REQUIRE(block.acks().empty());
}

TEST_CASE_METHOD(AckBlockFixture, "AckBlock creates fully-initialized block", "[block]")
{
  AckBlock::key_id_t key_id(0x41), key_id1(0x42);
  AckBlock::n_t n(0x1), n1(0x2);
  AckBlock::acks_t acks{{key_id, n}, {key_id1, n1}};

  std::unique_ptr<AckBlock> block_ptr;
  REQUIRE_NOTHROW(block_ptr.reset(new AckBlock(key_id, n)));
  REQUIRE(block_ptr->acks().at(key_id) == n);
  
  REQUIRE_NOTHROW(block_ptr.reset(new AckBlock(acks)));
  REQUIRE(block_ptr->acks() == acks);
}

TEST_CASE_METHOD(AckBlockFixture, "AckBlock serializes and deserializes from a buffer", "[block]")
{
  // block must have at least one Ack to serialize
  REQUIRE_THROWS(block.serialize());

  // add an Ack for an arbitrary key ID and N
  AckBlock::key_id_t key_id(0x42);
  AckBlock::n_t n(0x1);
  AckBlock::acks_t acks{{key_id, n}};
  REQUIRE_NOTHROW(block.add_ack(key_id, n));
  REQUIRE(block.acks().at(key_id) == n);

  REQUIRE_NOTHROW(block.serialize());
  REQUIRE_NOTHROW(block.deserialize());

  REQUIRE(block.acks() == acks);

  std::unique_ptr<AckBlock> exp_block;
  REQUIRE_NOTHROW(exp_block.reset(new AckBlock(block.buffer())));
  REQUIRE(block == *exp_block);
}

TEST_CASE_METHOD(AckBlockFixture, "AckBlock rejects invalid number of acks", "[block]")
{
  AckBlock::acks_t acks;
  // rejects setting empty Acks container (can only be empty with default ctor)
  REQUIRE_THROWS(block.acks(acks));

  AckBlock::key_id_t i = 0;
  for (; i < tini2p::under_cast(AckBlock::MaxAcksNum); ++i)
    acks[i] = i;

  // add the max number of acks
  REQUIRE_NOTHROW(block.acks(acks));

  // rejects adding one more Ack
  ++i;
  REQUIRE_THROWS(block.add_ack(i, i));

  // rejects setting max + 1 Acks
  acks[i] = i;
  REQUIRE_THROWS(block.acks(acks));
}

TEST_CASE_METHOD(AckBlockFixture, "AckBlock rejects deserializing invalid buffer", "[block]")
{
  AckBlock::acks_t acks{{0, 0}, {1, 1}, {2, 2}};
  AckBlock bad_block(acks);
  REQUIRE_NOTHROW(bad_block.deserialize());

  const auto& ack_len = tini2p::under_cast(AckBlock::AckLen);
  const auto& size_len = tini2p::under_cast(AckBlock::SizeLen);
  
  tini2p::BytesWriter<AckBlock::buffer_t> writer(bad_block.buffer());

  // write under-sized Acks length
  REQUIRE_NOTHROW(writer.skip_bytes(tini2p::under_cast(AckBlock::TypeLen)));
  REQUIRE_NOTHROW(writer.write_bytes((acks.size() - 1) * ack_len, tini2p::Endian::Big));
  REQUIRE_THROWS(bad_block.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(size_len));
  REQUIRE_NOTHROW(writer.write_bytes(tini2p::under_cast(AckBlock::MinAcksLen) - 1, tini2p::Endian::Big));
  REQUIRE_THROWS(bad_block.deserialize());

  // write over-sized Acks length
  REQUIRE_NOTHROW(writer.skip_back(size_len));
  REQUIRE_NOTHROW(writer.write_bytes<AckBlock::size_type>((acks.size() + 1) * ack_len, tini2p::Endian::Big));
  REQUIRE_THROWS(bad_block.deserialize());

  REQUIRE_NOTHROW(writer.skip_back(size_len));
  REQUIRE_NOTHROW(writer.write_bytes(tini2p::under_cast(AckBlock::MaxAcksLen) + 1, tini2p::Endian::Big));
  REQUIRE_THROWS(bad_block.deserialize());
}

TEST_CASE_METHOD(AckBlockFixture, "AckBlock rejects deserializing duplicate Acks", "[block]")
{
  AckBlock::acks_t acks{{0, 0}, {1, 1}};
  AckBlock bad_block(acks);

  // ensure bad block is in a valid initial state
  REQUIRE_NOTHROW(bad_block.deserialize());
  REQUIRE(bad_block.data_size() == acks.size() * tini2p::under_cast(AckBlock::AckLen));

  // write duplicate acks
  tini2p::BytesWriter<AckBlock::buffer_t> writer(bad_block.buffer());
  REQUIRE_NOTHROW(writer.skip_bytes(tini2p::under_cast(AckBlock::HeaderLen)));
  REQUIRE_NOTHROW(writer.write_bytes(AckBlock::key_id_t(1), tini2p::Endian::Big));
  REQUIRE_NOTHROW(writer.skip_bytes(tini2p::under_cast(AckBlock::NLen)));
  REQUIRE_NOTHROW(writer.write_bytes(AckBlock::key_id_t(1), tini2p::Endian::Big));
  REQUIRE_THROWS(bad_block.deserialize());
}
