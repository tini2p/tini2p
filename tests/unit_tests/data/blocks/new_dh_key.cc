/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/blocks/new_dh_key.h"

using tini2p::data::NewDHKeyBlock;

struct NewDHKeyBlockFixture
{
  NewDHKeyBlock block;
};

TEST_CASE_METHOD(NewDHKeyBlockFixture, "NewDHKeyBlock has a valid fields", "[block]")
{
  REQUIRE(block.type() == NewDHKeyBlock::type_t::NewDHKey);
  REQUIRE(block.size() == NewDHKeyBlock::HeaderLen + NewDHKeyBlock::NewDHKeyLen);
  REQUIRE(block.data_size() == NewDHKeyBlock::NewDHKeyLen);
  REQUIRE(block.key_id() == 0);
  REQUIRE(block.ratchet_key() == NewDHKeyBlock::ratchet_key_t());
}

TEST_CASE_METHOD(NewDHKeyBlockFixture, "NewDHKeyBlock creates a fully-initialized block", "[block]")
{
  NewDHKeyBlock::key_id_t id(0x42);
  NewDHKeyBlock::ratchet_key_t key;
  tini2p::crypto::RandBytes(key);  // set random pubkey, unrealistic

  std::unique_ptr<NewDHKeyBlock> block_ptr;
  REQUIRE_NOTHROW(block_ptr.reset(new NewDHKeyBlock(id, key)));

  REQUIRE(block_ptr->key_id() == id);
  REQUIRE(block_ptr->ratchet_key() == key);
}

TEST_CASE_METHOD(NewDHKeyBlockFixture, "NewDHKeyBlock serializes and deserializes from buffer", "[block]")
{
  // must set public key before serializing
  NewDHKeyBlock::ratchet_key_t key;
  tini2p::crypto::RandBytes(key);  // set random pubkey, unrealistic
  REQUIRE_NOTHROW(block.ratchet_key(key));

  REQUIRE_NOTHROW(block.serialize());
  REQUIRE_NOTHROW(block.deserialize());

  std::unique_ptr<NewDHKeyBlock> exp_block;
  REQUIRE_NOTHROW(exp_block.reset(new NewDHKeyBlock(block.buffer())));
  REQUIRE(block == *exp_block);
}

TEST_CASE_METHOD(NewDHKeyBlockFixture, "NewDHKeyBlock rejects deserializing from invalid buffer", "[block]")
{
  // create under-sized buffer
  NewDHKeyBlock::buffer_t bad_buf(tini2p::under_cast(NewDHKeyBlock::NewDHKeyLen) - 1);
  REQUIRE_THROWS(NewDHKeyBlock(bad_buf));

  // resize to over-sized buffer
  bad_buf.resize(bad_buf.size() + 2);
  REQUIRE_THROWS(NewDHKeyBlock(bad_buf));

  // must set public key before serializing
  NewDHKeyBlock::ratchet_key_t key;
  tini2p::crypto::RandBytes(key);  // set random pubkey, unrealistic
  REQUIRE_NOTHROW(block.ratchet_key(key));
  // serialize to get a valid buffer
  REQUIRE_NOTHROW(block.serialize());
  // deserialize to ensure valid buffer
  REQUIRE_NOTHROW(block.deserialize());
  // invalidate buffer by overwriting key section with all zeros
  const auto& key_offset = tini2p::under_cast(NewDHKeyBlock::HeaderLen) + tini2p::under_cast(NewDHKeyBlock::KeyIDLen);
  tini2p::BytesWriter<NewDHKeyBlock::buffer_t> writer(block.buffer());
  writer.skip_bytes(key_offset);
  writer.write_data(NewDHKeyBlock::ratchet_key_t());
  REQUIRE_THROWS(block.deserialize());
}

TEST_CASE_METHOD(NewDHKeyBlockFixture, "NewDHKeyBlock rejects setting an invalid key", "[block]")
{
  NewDHKeyBlock::ratchet_key_t null_key;
  REQUIRE_THROWS(NewDHKeyBlock(0, null_key));
  REQUIRE_THROWS(block.ratchet_key(null_key));
}
