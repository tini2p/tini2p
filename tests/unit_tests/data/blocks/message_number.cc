/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/blocks/message_number.h"

using tini2p::data::MessageNumberBlock;

struct MessageNumberBlockFixture
{
  MessageNumberBlock block;
};

TEST_CASE_METHOD(MessageNumberBlockFixture, "MessageNumberBlock has valid fields", "[block]")
{
  REQUIRE(block.type() == MessageNumberBlock::Type::MessageNumber);
  REQUIRE(block.size() == MessageNumberBlock::HeaderLen + MessageNumberBlock::MsgNumBlockLen);
  REQUIRE(block.data_size() == MessageNumberBlock::MsgNumBlockLen);
  REQUIRE(block.key_id() == 0);
  REQUIRE(block.pn() == 0);
  REQUIRE(block.n() == 0);
}

TEST_CASE_METHOD(MessageNumberBlockFixture, "MessageNumberBlock serializes and deserializes from buffer", "[block]")
{
  auto exp_block = std::make_unique<MessageNumberBlock>(block);  // copy block for comparison

  REQUIRE_NOTHROW(block.serialize());
  REQUIRE_NOTHROW(block.deserialize());
  REQUIRE(block == *exp_block);

  // set random test values, maybe realistic
  const MessageNumberBlock::key_id_t test_id = 0x42;
  const MessageNumberBlock::pn_t test_pn = 0x64;
  const MessageNumberBlock::n_t test_n = 0x17;

  REQUIRE_NOTHROW(block.key_id(test_id));
  REQUIRE_NOTHROW(block.pn(test_pn));
  REQUIRE_NOTHROW(block.n(test_n));

  REQUIRE(block.key_id() == test_id);
  REQUIRE(block.pn() == test_pn);
  REQUIRE(block.n() == test_n);

  REQUIRE_NOTHROW(block.serialize());
  REQUIRE_NOTHROW(exp_block.reset(new MessageNumberBlock(block.buffer())));  // copy updated block for comparison
  REQUIRE(block == *exp_block);
}
