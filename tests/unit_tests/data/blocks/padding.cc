/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/blocks/padding.h"

using tini2p::data::PaddingBlock;

struct PaddingBlockFixture
{
  PaddingBlock block;
};

TEST_CASE_METHOD(PaddingBlockFixture, "PaddingBlock has a valid block ID", "[block]")
{
  REQUIRE(block.type() == PaddingBlock::type_t::Padding);
}

TEST_CASE_METHOD(PaddingBlockFixture, "PaddingBlock has a valid size", "[block]")
{
  REQUIRE(block.size() == block.buffer().size());
  REQUIRE(block.data_size() <= PaddingBlock::MaxPaddingLen);
  REQUIRE(block.data_size() == block.padding().size());
}

TEST_CASE_METHOD(
    PaddingBlockFixture,
    "PaddingBlock serializes and deserializes",
    "[block]")
{
  // serialize to buffer
  REQUIRE_NOTHROW(block.serialize());

  // deserialize from buffer
  REQUIRE_NOTHROW(block.deserialize());

  REQUIRE(block.type() == PaddingBlock::type_t::Padding);
  REQUIRE(block.size() == block.buffer().size());
  REQUIRE(block.data_size() <= PaddingBlock::MaxPaddingLen);
  REQUIRE(block.data_size() == block.padding().size());
}

TEST_CASE_METHOD(
    PaddingBlockFixture,
    "PaddingBlock fails to deserialize invalid ID",
    "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the block ID
  ++block.buffer()[PaddingBlock::TypeOffset];
  REQUIRE_THROWS(block.deserialize());

  block.buffer()[PaddingBlock::TypeOffset] -= 2;
  REQUIRE_THROWS(block.deserialize());
}

TEST_CASE_METHOD(
    PaddingBlockFixture,
    "PaddingBlock fails to deserialize invalid size",
    "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the size
  REQUIRE_NOTHROW(tini2p::write_bytes(
      &block.buffer()[PaddingBlock::SizeOffset], PaddingBlock::MaxPaddingLen + 1));

  REQUIRE_THROWS(block.deserialize());
}
