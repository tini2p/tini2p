/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/blocks/ntcp2_options.h"

using OptionsBlock = tini2p::data::NTCP2OptionsBlock;

struct NTCP2OptionsBlockFixture
{
  OptionsBlock block;
};

TEST_CASE_METHOD(NTCP2OptionsBlockFixture, "NTCP2OptionsBlock has a block ID", "[block]")
{
  REQUIRE(block.type() == OptionsBlock::type_t::NTCP2Options);
}

TEST_CASE_METHOD(NTCP2OptionsBlockFixture, "NTCP2OptionsBlock has a block size", "[block]")
{
  REQUIRE(block.data_size() == OptionsBlock::NTCP2OptionsLen);
  REQUIRE(block.size() == OptionsBlock::HeaderLen + OptionsBlock::NTCP2OptionsLen);
  REQUIRE(block.size() == block.buffer().size());
}

TEST_CASE_METHOD(NTCP2OptionsBlockFixture, "NTCP2OptionsBlock serializes a valid block", "[block]")
{
  REQUIRE_NOTHROW(block.serialize());

  // check min padding ratios
  REQUIRE_NOTHROW(block.tmin(block.min_padding_ratio()));
  REQUIRE_NOTHROW(block.tmax(block.min_padding_ratio()));
  REQUIRE_NOTHROW(block.rmin(block.min_padding_ratio()));
  REQUIRE_NOTHROW(block.rmax(block.min_padding_ratio()));
  REQUIRE_NOTHROW(block.serialize());

  // check max padding ratios
  REQUIRE_NOTHROW(block.tmax(block.max_padding_ratio()));
  REQUIRE_NOTHROW(block.tmin(block.max_padding_ratio()));
  REQUIRE_NOTHROW(block.rmax(block.max_padding_ratio()));
  REQUIRE_NOTHROW(block.rmin(block.max_padding_ratio()));
  REQUIRE_NOTHROW(block.serialize());
}

TEST_CASE_METHOD(NTCP2OptionsBlockFixture, "NTCP2OptionsBlock serializes and deserializes a valid block", "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());
  REQUIRE_NOTHROW(block.deserialize());
}

TEST_CASE_METHOD(NTCP2OptionsBlockFixture, "NTCP2OptionsBlock fails to deserialize invalid ID", "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the block ID
  ++block.buffer()[OptionsBlock::TypeOffset];

  REQUIRE_THROWS(block.deserialize());

  block.buffer()[OptionsBlock::TypeOffset] -= 2;

  REQUIRE_THROWS(block.deserialize());
}

TEST_CASE_METHOD(NTCP2OptionsBlockFixture, "NTCP2OptionsBlock fails to deserialize invalid size", "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the size 
  ++block.buffer()[OptionsBlock::SizeOffset];

  REQUIRE_THROWS(block.deserialize());

  // invalidate the size 
  block.buffer()[OptionsBlock::SizeOffset] -= 2;

  REQUIRE_THROWS(block.deserialize());
}

TEST_CASE_METHOD(NTCP2OptionsBlockFixture, "NTCP2OptionsBlock fails to serialize invalid parameters", "[block]")
{
  // check invalid parameters (lowerbound)
  REQUIRE_THROWS(block.tmin(block.min_padding_ratio() - 1));
  REQUIRE_THROWS(block.tmax(block.min_padding_ratio() - 1));
  REQUIRE_THROWS(block.rmin(block.min_padding_ratio() - 1));
  REQUIRE_THROWS(block.rmax(block.min_padding_ratio() - 1));

  // min cannot be larger than max
  REQUIRE_NOTHROW(block.tmax(block.min_padding_ratio()));
  REQUIRE_THROWS(block.tmin(block.min_padding_ratio() + 1));

  // min cannot be larger than max
  REQUIRE_NOTHROW(block.rmax(block.min_padding_ratio()));
  REQUIRE_THROWS(block.rmin(block.min_padding_ratio() + 1));

  // check invalid parameters (upperbound)
  REQUIRE_THROWS(block.tmin(block.max_padding_ratio() + 1));
  REQUIRE_THROWS(block.tmax(block.max_padding_ratio() + 1));
  REQUIRE_THROWS(block.rmin(block.max_padding_ratio() + 1));
  REQUIRE_THROWS(block.rmax(block.max_padding_ratio() + 1));

  // min cannot be larger than max
  REQUIRE_NOTHROW(block.tmax(block.max_padding_ratio() - 1));
  REQUIRE_THROWS(block.tmin(block.max_padding_ratio()));

  // min cannot be larger than max
  REQUIRE_NOTHROW(block.rmax(block.max_padding_ratio() - 1));
  REQUIRE_THROWS(block.rmin(block.max_padding_ratio()));
}

TEST_CASE_METHOD(NTCP2OptionsBlockFixture, "NTCP2OptionsBlock fails to deserialize invalid parameters", "[block]")
{
  // Testing invalid header values done above.
  //
  // Parameters cannot be deserialized out of range.
  //   The only ranged parameters are cast as `(std::uint8_t)val / 16.0` (all in-range float values).
  //   All invalid values just wrap-around, i.e. uint8_MAX + 1 = uint8_MIN = 0
}
