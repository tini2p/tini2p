/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/blocks/ecies_options.h"

using OptionsBlock = tini2p::data::ECIESOptionsBlock;

struct ECIESOptionsBlockFixture
{
  OptionsBlock block;
};

TEST_CASE_METHOD(ECIESOptionsBlockFixture, "ECIESOptionsBlock has a valid fields", "[block]")
{
  REQUIRE(block.type() == OptionsBlock::type_t::ECIESOptions);
  REQUIRE(block.size() == OptionsBlock::HeaderLen + OptionsBlock::ECIESOptionsLen);
  REQUIRE(block.data_size() == OptionsBlock::ECIESOptionsLen);
  REQUIRE(block.tag_len() == OptionsBlock::DefaultTagLen);
  REQUIRE(block.out_tag_window() == OptionsBlock::DefaultTagWindow);
  REQUIRE(block.timeout() == OptionsBlock::DefaultTimeout);
  REQUIRE(block.in_tag_window() == OptionsBlock::DefaultTagWindow);
  REQUIRE(block.flag() == OptionsBlock::Flags::NullFlag);
  REQUIRE(block.misc() == 0);
}

TEST_CASE_METHOD(ECIESOptionsBlockFixture, "ECIESOptionsBlock serializes and deserializes from buffer", "[block]")
{
  REQUIRE_NOTHROW(block.serialize());
  REQUIRE_NOTHROW(block.deserialize());

  std::unique_ptr<OptionsBlock> exp_block;
  REQUIRE_NOTHROW(exp_block.reset(new OptionsBlock(block.buffer())));
  REQUIRE(block == *exp_block);
}

TEST_CASE_METHOD(ECIESOptionsBlockFixture, "ECIESOptionsBlock rejects deserializing from invalid buffer", "[block]")
{
  // create an undersized buffer
  OptionsBlock::buffer_t bad_buf(OptionsBlock::HeaderLen + OptionsBlock::ECIESOptionsLen - 1);
  REQUIRE_THROWS(OptionsBlock(bad_buf));

  // resize for oversized buffer
  bad_buf.resize(bad_buf.size() + 2);
  REQUIRE_THROWS(OptionsBlock(bad_buf));
}

TEST_CASE_METHOD(ECIESOptionsBlockFixture, "ECIESOptionsBlock rejects setting invalid tag length", "[block]")
{
  REQUIRE_THROWS(block.tag_len(tini2p::under_cast(OptionsBlock::MinTagLen) - 1));
  REQUIRE_THROWS(block.tag_len(tini2p::under_cast(OptionsBlock::MaxTagLen) + 1));
}

TEST_CASE_METHOD(ECIESOptionsBlockFixture, "ECIESOptionsBlock rejects setting invalid tag window", "[block]")
{
  REQUIRE_THROWS(block.out_tag_window(tini2p::under_cast(OptionsBlock::MinTagWindow) - 1));
  REQUIRE_THROWS(block.out_tag_window(tini2p::under_cast(OptionsBlock::MaxTagWindow) + 1));

  REQUIRE_THROWS(block.in_tag_window(tini2p::under_cast(OptionsBlock::MinTagWindow) - 1));
  REQUIRE_THROWS(block.in_tag_window(tini2p::under_cast(OptionsBlock::MaxTagWindow) + 1));
}

TEST_CASE_METHOD(ECIESOptionsBlockFixture, "ECIESOptionsBlock rejects setting invalid timeout", "[block]")
{
  REQUIRE_THROWS(block.timeout(tini2p::under_cast(OptionsBlock::MinTimeout) - 1));
  REQUIRE_THROWS(block.timeout(tini2p::under_cast(OptionsBlock::MaxTimeout) + 1));
}

TEST_CASE_METHOD(ECIESOptionsBlockFixture, "ECIESOptionsBlock rejects setting invalid flag", "[block]")
{
  // set invalid flag, adjust as more flags are added
  REQUIRE_THROWS(
      block.flag(static_cast<OptionsBlock::Flags>(tini2p::under_cast(OptionsBlock::Flags::RequestRatchet) + 1)));
}
