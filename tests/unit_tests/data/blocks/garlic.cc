/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/blocks/garlic.h"

namespace data = tini2p::data;

using tini2p::data::GarlicBlock;
using tini2p::data::GarlicDeliveryInstructions;

struct GarlicBlockFixture
{
  GarlicDeliveryInstructions instructions;
  GarlicBlock block;
};

TEST_CASE_METHOD(GarlicBlockFixture, "GarlicBlock has valid fields", "[garlic_block]")
{
  REQUIRE(block.type() == GarlicBlock::type_t::Garlic);
  REQUIRE(block.instructions() == tini2p::data::GarlicDeliveryInstructions());
  REQUIRE(block.i2np_type() == data::I2NPHeader::Type::Data);
  REQUIRE(block.message_id() != 0);
  REQUIRE(block.expiration() != 0);
}

TEST_CASE_METHOD(GarlicBlockFixture, "GarlicBlock converts I2NP message", "[garlic_block]")
{
  REQUIRE_NOTHROW(block = GarlicBlock(data::I2NPHeader::Type::Data, tini2p::crypto::RandBuffer(32)));
  REQUIRE_NOTHROW(block = GarlicBlock(data::I2NPHeader::Type::DatabaseStore, tini2p::crypto::RandBuffer(32)));
}

TEST_CASE_METHOD(GarlicBlockFixture, "GarlicBlock serializes and deserializes from buffer", "[garlic_block]")
{
  REQUIRE_NOTHROW(block.message_id(0x42));
  REQUIRE_NOTHROW(block.expiration(tini2p::time::now_s() + GarlicBlock::DefaultExpiration));
  REQUIRE_NOTHROW(block.body(tini2p::crypto::RandBuffer(32)));
  REQUIRE_NOTHROW(block.serialize());

  REQUIRE(block.buffer().size());
  REQUIRE(block.type() == GarlicBlock::type_t::Garlic);

  REQUIRE(GarlicBlock(block.buffer()) == block);
}

TEST_CASE_METHOD(GarlicBlockFixture, "GarlicBlock rejects setting invalid message ID", "[garlic_block]")
{
  REQUIRE_THROWS(block.message_id(0x00));
}

TEST_CASE_METHOD(GarlicBlockFixture, "GarlicBlock rejects setting invalid expiration", "[garlic_block]")
{
  REQUIRE_THROWS(block.expiration(tini2p::time::now_s()));
}

TEST_CASE_METHOD(GarlicBlockFixture, "GarlicBlock rejects setting invalid I2NP message type", "[garlic_block]")
{
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::Reserved));
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::FutureReserved));
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::DatabaseSearchReply));
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::DatabaseLookup));
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::DeliveryStatus));
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::Garlic));
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::TunnelData));
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::TunnelGateway));
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::TunnelBuild));
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::TunnelBuildReply));
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::VariableTunnelBuild));
  REQUIRE_THROWS(block.i2np_type(data::I2NPHeader::Type::VariableTunnelBuildReply));
}

TEST_CASE_METHOD(GarlicBlockFixture, "GarlicBlock rejects creation from invalid I2NP message", "[garlic_block]")
{
  // no need to test invalid serializing ctor, will fail to compile if passed invalid variant alternative
  REQUIRE_NOTHROW(block = GarlicBlock(data::I2NPHeader::Type::Data, tini2p::crypto::RandBuffer(32)));

  /*-----------------------------------------------\
   | Reset I2NP message type to unsupported types  |
   *----------------------------------------------*/
  const auto i2np_type_offset =  tini2p::data::GarlicBlock::HeaderLen + instructions.size();

  auto& block_buf = block.buffer();
  auto& type_byte = block_buf[i2np_type_offset];

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::DatabaseSearchReply);
  REQUIRE_THROWS(GarlicBlock(block_buf));

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::DatabaseLookup);
  REQUIRE_THROWS(GarlicBlock(block_buf));

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::DeliveryStatus);
  REQUIRE_THROWS(GarlicBlock(block_buf));

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::Garlic);
  REQUIRE_THROWS(GarlicBlock(block_buf));

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::TunnelData);
  REQUIRE_THROWS(GarlicBlock(block_buf));

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::TunnelGateway);
  REQUIRE_THROWS(GarlicBlock(block_buf));

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::TunnelBuild);
  REQUIRE_THROWS(GarlicBlock(block_buf));

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::TunnelBuildReply);
  REQUIRE_THROWS(GarlicBlock(block_buf));

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::VariableTunnelBuild);
  REQUIRE_THROWS(GarlicBlock(block_buf));

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::VariableTunnelBuildReply);
  REQUIRE_THROWS(GarlicBlock(block_buf));

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::Reserved);
  REQUIRE_THROWS(GarlicBlock(block_buf));

  type_byte = tini2p::under_cast(data::I2NPHeader::Type::FutureReserved);
  REQUIRE_THROWS(GarlicBlock(block_buf));
}
