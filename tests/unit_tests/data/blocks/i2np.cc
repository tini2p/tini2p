/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/blocks/i2np.h"

using tini2p::data::I2NPBlock;

struct I2NPBlockFixture
{
  I2NPBlock block;
};

TEST_CASE_METHOD(I2NPBlockFixture, "I2NPBlock has a block ID", "[block]")
{
  REQUIRE(block.type() == I2NPBlock::Type::I2NP);
}

TEST_CASE_METHOD(I2NPBlockFixture, "I2NPBlock has a size", "[block]")
{
  REQUIRE(block.data_size() >= I2NPBlock::MinMsgLen);
  REQUIRE(block.data_size() <= I2NPBlock::MaxMsgLen);
  REQUIRE(block.size() == I2NPBlock::HeaderLen + block.data_size());
  REQUIRE(block.size() == block.buffer().size());
}

TEST_CASE_METHOD(I2NPBlockFixture, "I2NPBlock has an expiration", "[block]")
{
  REQUIRE(block.expiration() > tini2p::time::now_s());
}

TEST_CASE_METHOD(I2NPBlockFixture, "I2NPBlock serializes and deserializes from the buffer", "[block]")
{
  // serialize to buffer
  REQUIRE_NOTHROW(block.serialize());

  // create from a secure buffer
  REQUIRE_NOTHROW(I2NPBlock(block.buffer()));

  // deserialize from buffer
  REQUIRE_NOTHROW(block.deserialize());

  REQUIRE(block.type() == I2NPBlock::Type::I2NP);
  REQUIRE(block.message_id() != 0);
  REQUIRE(block.size() == I2NPBlock::HeaderLen + block.data_size());
  REQUIRE(block.buffer().size() == block.size());
}

TEST_CASE_METHOD(I2NPBlockFixture, "I2NPBlock fails to deserialize invalid ID", "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the block ID
  ++block.buffer()[I2NPBlock::TypeOffset];
  REQUIRE_THROWS(block.deserialize());

  block.buffer()[I2NPBlock::TypeOffset] -= 2;
  REQUIRE_THROWS(block.deserialize());
}

TEST_CASE_METHOD(I2NPBlockFixture, "I2NPBlock fails to deserialize invalid size", "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the size 
  tini2p::write_bytes(&block.buffer()[I2NPBlock::SizeOffset], I2NPBlock::MinMsgLen - 1);
  REQUIRE_THROWS(block.deserialize());

  tini2p::write_bytes(&block.buffer()[I2NPBlock::SizeOffset], I2NPBlock::MaxMsgLen + 1);
  REQUIRE_THROWS(block.deserialize());
}

TEST_CASE_METHOD(I2NPBlockFixture, "I2NPBlock fails to deserialize invalid expiration", "[block]")
{
  using tini2p::time::now_s;
  using expiration_t = I2NPBlock::expiration_t;

  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the expiration (at expiration)
  tini2p::write_bytes(&block.buffer()[I2NPBlock::ExpirationOffset], expiration_t(now_s()), tini2p::Endian::Big);
  REQUIRE_THROWS(block.deserialize());

  // invalidate the expiration (past expiration)
  tini2p::write_bytes(&block.buffer()[I2NPBlock::ExpirationOffset], expiration_t(now_s() - 1), tini2p::Endian::Big);
  REQUIRE_THROWS(block.deserialize());
}
