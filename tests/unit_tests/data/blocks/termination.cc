/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/blocks/termination.h"

using tini2p::data::TerminationBlock;

struct TerminationBlockFixture
{
  TerminationBlock block;
};

TEST_CASE_METHOD(
    TerminationBlockFixture,
    "TerminationBlock has a block ID",
    "[block]")
{
  REQUIRE(block.type() == TerminationBlock::type_t::Termination);
}

TEST_CASE_METHOD(TerminationBlockFixture, "TerminationBlock has a size", "[block]")
{
  REQUIRE(block.data_size() >= TerminationBlock::MinTermLen);
  REQUIRE(block.data_size() <= TerminationBlock::MaxTermLen);
  REQUIRE(block.size() == TerminationBlock::HeaderLen + block.data_size());
  REQUIRE(block.size() == block.buffer().size());
}

TEST_CASE_METHOD(
    TerminationBlockFixture,
    "TerminationBlock serializes and deserializes from the buffer",
    "[block]")
{
  // serialize to buffer
  REQUIRE_NOTHROW(block.serialize());

  // create from a valid buffer
  REQUIRE_NOTHROW(TerminationBlock(block.buffer()));

  // deserialize from buffer
  REQUIRE_NOTHROW(block.deserialize());

  REQUIRE(block.type() == TerminationBlock::type_t::Termination);
  REQUIRE(block.size() == TerminationBlock::HeaderLen + block.data_size());
  REQUIRE(block.reason() == TerminationBlock::reason_t::NormalClose);
  REQUIRE(block.buffer().size() == block.size());
}

TEST_CASE_METHOD(
    TerminationBlockFixture,
    "TerminationBlock fails to deserialize invalid ID",
    "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the block ID
  ++block.buffer()[TerminationBlock::TypeOffset];
  REQUIRE_THROWS(block.deserialize());

  block.buffer()[TerminationBlock::TypeOffset] -= 2;
  REQUIRE_THROWS(block.deserialize());
}

TEST_CASE_METHOD(
    TerminationBlockFixture,
    "TerminationBlock fails to deserialize invalid size",
    "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the size
  tini2p::write_bytes(
      &block.buffer()[TerminationBlock::SizeOffset], TerminationBlock::MinTermLen - 1, tini2p::Endian::Big);
  REQUIRE_THROWS(block.deserialize());

  tini2p::write_bytes(
      &block.buffer()[TerminationBlock::SizeOffset], TerminationBlock::MaxTermLen + 1, tini2p::Endian::Big);
  REQUIRE_THROWS(block.deserialize());
}
