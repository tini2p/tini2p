/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/blocks/ack_request.h"

using tini2p::data::AckRequestBlock;

struct AckRequestBlockFixture
{
  AckRequestBlock block;
};

TEST_CASE_METHOD(AckRequestBlockFixture, "AckRequestBlock has valid fields", "[block]")
{
  REQUIRE(block.type() == AckRequestBlock::type_t::AckRequest);
  REQUIRE(block.size() == AckRequestBlock::HeaderLen + AckRequestBlock::AckRequestLen);
  REQUIRE(block.data_size() == AckRequestBlock::AckRequestLen);
  REQUIRE(block.session_id() == 0);
  REQUIRE(block.flag() == AckRequestBlock::Flags::Null);
  // unset for inclusion in std::variant container, must set before serializing
  REQUIRE(block.delivery() == AckRequestBlock::delivery_t());
}

TEST_CASE_METHOD(AckRequestBlockFixture, "AckRequestBlock creates a fully initialized block", "[block]")
{
  std::unique_ptr<AckRequestBlock> block_ptr;
  AckRequestBlock::session_id_t session_id(0x42);
  AckRequestBlock::delivery_t::hash_t to_hash;
  tini2p::crypto::RandBytes(to_hash);  // set random "to hash", unrealistic

  REQUIRE_NOTHROW(block_ptr.reset(new AckRequestBlock(session_id, to_hash)));

  // check session ID is set properly
  REQUIRE(block_ptr->session_id() == session_id);

  // check flag is null (may change if used in spec)
  REQUIRE(block_ptr->flag() == AckRequestBlock::Flags::Null);

  // check garlic clove delivery instructions are set properly
  const auto& block_delivery = block_ptr->delivery();
  REQUIRE(block_delivery.delivery_flags() == AckRequestBlock::delivery_t::DeliveryFlags::Destination);
  REQUIRE(block_delivery.to_hash() == to_hash);
}

TEST_CASE_METHOD(AckRequestBlockFixture, "AckRequestBlock serializes and deserializes from buffer", "[block]")
{
  // must set garlic clove delivery instructions destination hash
  REQUIRE_THROWS(block.serialize());

  AckRequestBlock::delivery_t::hash_t to_hash;
  tini2p::crypto::RandBytes(to_hash);  // set random "to hash", unrealistic

  REQUIRE_NOTHROW(block.set_destination_hash(to_hash));

  // check garlic clove delivery instructions are set properly
  const auto& block_delivery = block.delivery();
  REQUIRE(block_delivery.delivery_flags() == AckRequestBlock::delivery_t::DeliveryFlags::Destination);
  REQUIRE(block_delivery.to_hash() == to_hash);

  REQUIRE_NOTHROW(block.serialize());
  REQUIRE_NOTHROW(block.deserialize());

  std::unique_ptr<AckRequestBlock> exp_block;
  REQUIRE_NOTHROW(exp_block.reset(new AckRequestBlock(block.buffer())));
  REQUIRE(block == *exp_block);
}

TEST_CASE_METHOD(AckRequestBlockFixture, "AckRequestBlock rejects null destination hash", "[block]")
{
  AckRequestBlock::delivery_t::hash_t null_hash;

  REQUIRE_THROWS(block.set_destination_hash(null_hash));
  REQUIRE_THROWS(AckRequestBlock({}, null_hash));
}

// may change depending on spec
TEST_CASE_METHOD(AckRequestBlockFixture, "AckRequestBlock rejects deserializing non-null flag", "[block]")
{
  AckRequestBlock::delivery_t::hash_t to_hash;
  tini2p::crypto::RandBytes(to_hash);  // set random "to hash", unrealistic

  // setup valid block buffer
  REQUIRE_NOTHROW(block.set_destination_hash(to_hash));
  REQUIRE_NOTHROW(block.serialize());

  tini2p::BytesWriter<AckRequestBlock::buffer_t> writer(block.buffer());
  writer.skip_bytes(AckRequestBlock::HeaderLen + AckRequestBlock::SessionIDLen);
  // write non-null flag to block buffer
  writer.write_bytes<std::uint8_t>(0x42);
  REQUIRE_THROWS(block.deserialize());
}

TEST_CASE_METHOD(
    AckRequestBlockFixture,
    "AckRequestBlock rejects deserializing invalid delivery instructions",
    "[block]")
{
  AckRequestBlock::delivery_t::hash_t null_hash, to_hash;
  tini2p::crypto::RandBytes(to_hash);  // set random "to hash", unrealistic

  // setup valid block buffer
  REQUIRE_NOTHROW(block.set_destination_hash(to_hash));
  REQUIRE_NOTHROW(block.serialize());

  tini2p::BytesWriter<AckRequestBlock::buffer_t> writer(block.buffer());

  // skip to destination hash
  writer.skip_bytes(
      AckRequestBlock::HeaderLen + AckRequestBlock::SessionIDLen + AckRequestBlock::FlagLen
      + AckRequestBlock::delivery_t::FlagsLen);

  // write null destination hash
  writer.write_data(null_hash);

  // block buffer now invalid because of null hash
  REQUIRE_THROWS(block.deserialize());

  // skip back to overwrite garlic delivery instructions flag
  writer.skip_back(AckRequestBlock::delivery_t::FlagsLen + AckRequestBlock::delivery_t::HashLen);
  // set invalid flag
  writer.write_bytes(AckRequestBlock::delivery_t::DeliveryFlags::Local);

  // block now invalid because of non-Destination delivery
  REQUIRE_THROWS(block.deserialize());
}
