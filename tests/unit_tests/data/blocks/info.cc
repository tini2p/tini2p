/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/blocks/info.h"

using tini2p::data::InfoBlock;

struct InfoBlockFixture
{
  InfoBlockFixture() : info(new tini2p::data::Info()), block(info) {}

  tini2p::data::Info::shared_ptr info;
  InfoBlock block;
};

TEST_CASE_METHOD(InfoBlockFixture, "InfoBlock has a block ID", "[block]")
{
  REQUIRE(block.type() == InfoBlock::Type::Info);
}

TEST_CASE_METHOD(InfoBlockFixture, "InfoBlock has a block size", "[block]")
{
  REQUIRE(block.data_size() >= InfoBlock::MinInfoBlockLen);
  REQUIRE(block.data_size() <= InfoBlock::MaxInfoBlockLen);
}

TEST_CASE_METHOD(InfoBlockFixture, "InfoBlock serializes and deserializes a valid block", "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());
  REQUIRE_NOTHROW(block.deserialize());

  std::unique_ptr<InfoBlock> des_block;
  REQUIRE_NOTHROW(des_block.reset(new InfoBlock(block.buffer())));
  REQUIRE(block == *des_block);
}

TEST_CASE_METHOD(InfoBlockFixture, "InfoBlock fails to deserialize invalid ID", "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the block ID
  ++block.buffer()[InfoBlock::TypeOffset];
  REQUIRE_THROWS(block.deserialize());

  block.buffer()[InfoBlock::TypeOffset] -= 2;
  REQUIRE_THROWS(block.deserialize());
}

TEST_CASE_METHOD(InfoBlockFixture, "InfoBlock fails to deserialize invalid size", "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the size
  tini2p::write_bytes(&block.buffer()[InfoBlock::SizeOffset], InfoBlock::MaxInfoBlockLen + 1);
  REQUIRE_THROWS(block.deserialize());

  tini2p::write_bytes(&block.buffer()[InfoBlock::SizeOffset], InfoBlock::MinInfoBlockLen - 1);
  REQUIRE_THROWS(block.deserialize());
}
