/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/blocks/date_time.h"

using tini2p::time::now_s;
using tini2p::data::DateTimeBlock;

struct DateTimeBlockFixture
{
  DateTimeBlock block;
};

TEST_CASE_METHOD(DateTimeBlockFixture, "DateTimeBlock has a block ID", "[block]")
{
  REQUIRE(block.type() == DateTimeBlock::Type::DateTime);
}

TEST_CASE_METHOD(DateTimeBlockFixture, "DateTimeBlock has a size", "[block]")
{
  REQUIRE(block.data_size() == DateTimeBlock::TimestampLen);
  REQUIRE(
      block.size() == DateTimeBlock::HeaderLen + DateTimeBlock::TimestampLen);
  REQUIRE(block.size() == block.buffer().size());
}

TEST_CASE_METHOD(DateTimeBlockFixture, "DateTimeBlock has a timestamp", "[block]")
{
  REQUIRE(block.timestamp());
  REQUIRE(tini2p::time::check_lag_s(block.timestamp()));
}

TEST_CASE_METHOD(DateTimeBlockFixture, "DateTimeBlock serializes and deserializes from the buffer", "[block]")
{
  // set valid DateTime block parameters
  const auto tmp_ts = now_s();

  // serialize to buffer
  REQUIRE_NOTHROW(block.serialize());

  // deserialize from buffer
  REQUIRE_NOTHROW(block.deserialize());

  REQUIRE(block.type() == DateTimeBlock::Type::DateTime);
  REQUIRE(block.data_size() == DateTimeBlock::TimestampLen);
  REQUIRE(block.timestamp() == tmp_ts);
  REQUIRE(
      block.size() == DateTimeBlock::HeaderLen + DateTimeBlock::TimestampLen);
  REQUIRE(block.buffer().size() == block.size());
}

TEST_CASE_METHOD(DateTimeBlockFixture, "DateTimeBlock fails to deserialize invalid ID", "[block]")
{
  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the block ID
  ++block.buffer()[DateTimeBlock::TypeOffset];
  REQUIRE_THROWS(block.deserialize());

  block.buffer()[DateTimeBlock::TypeOffset] -= 2;
  REQUIRE_THROWS(block.deserialize());
}

TEST_CASE_METHOD(DateTimeBlockFixture, "DateTimeBlock fails to deserialize invalid size", "[block]")
{
  // invalidate the size 
  ++block.buffer()[DateTimeBlock::SizeOffset];
  REQUIRE_THROWS(block.deserialize());

  block.buffer()[DateTimeBlock::SizeOffset] -= 2;
  REQUIRE_THROWS(block.deserialize());
}

TEST_CASE_METHOD(DateTimeBlockFixture, "DateTimeBlock fails to deserialize invalid timestamp", "[block]")
{
  using tini2p::time::MaxLagDelta;

  // serialize a valid block
  REQUIRE_NOTHROW(block.serialize());

  // invalidate the timestamp (lowerbound) 
  block.buffer()[DateTimeBlock::TimestampOffset] -= MaxLagDelta + 1;
  REQUIRE_THROWS(block.deserialize());

  // invalidate the timestamp (upperbound)
  tini2p::write_bytes(
      &block.buffer()[DateTimeBlock::TimestampOffset], DateTimeBlock::timestamp_t(now_s() + (MaxLagDelta + 1)));
  REQUIRE_THROWS(block.deserialize());
}
