/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/router/destination.h"

namespace crypto = tini2p::crypto;

using Catch::Matchers::Equals;
using vec = std::vector<std::uint8_t>;

using Destination = tini2p::data::Destination;
using crypto_t = Destination::crypto_t;
using signing_t = Destination::eddsa_t;


struct DestinationFixture
{
  DestinationFixture() : destination() {}

  Destination destination;
};

TEST_CASE_METHOD(DestinationFixture, "Destination has a crypto public key", "[ident]")
{
  REQUIRE(destination.crypto().pubkey.size() == crypto_t::PublicKeyLen);
  REQUIRE(destination.crypto_pubkey_len() == crypto_t::PublicKeyLen);
}

TEST_CASE_METHOD(DestinationFixture, "Destination has a signing public key", "[ident]")
{
  std::visit(
      [](const auto& s) { REQUIRE(s.pubkey().size() == std::decay_t<decltype(s)>::PublicKeyLen); }, destination.signing());
  REQUIRE_NOTHROW(destination.signing_pubkey_len() == signing_t::PublicKeyLen);
}

TEST_CASE_METHOD(DestinationFixture, "Destination has a cert", "[ident]")
{
  REQUIRE_NOTHROW(destination.cert());

  const auto& cert = destination.cert();
  REQUIRE(cert.cert_type == Destination::cert_t::cert_type_t::KeyCert);
  REQUIRE(cert.length == Destination::cert_t::KeyCertLen);
  REQUIRE(cert.sign_type == Destination::cert_t::sign_type_t::EdDSA);
  REQUIRE(cert.crypto_type == Destination::cert_t::crypto_type_t::EciesX25519);
}

TEST_CASE_METHOD(DestinationFixture, "Destination has a valid size", "[ident]")
{
  REQUIRE_NOTHROW(destination.size());

  const auto keys_len = crypto_t::PublicKeyLen + signing_t::PublicKeyLen;
  const auto expected_len =
      keys_len + (Destination::KeysPaddingLen - keys_len) + Destination::cert_t::HeaderLen + Destination::cert_t::KeyCertLen;

  REQUIRE(destination.size() == expected_len);
}

TEST_CASE_METHOD(DestinationFixture, "Destination serializes and deserializes a valid router destination", "[ident]")
{
  REQUIRE_NOTHROW(destination.serialize());
  REQUIRE_NOTHROW(destination.deserialize());
  REQUIRE_NOTHROW(Destination(destination.buffer()));

  Destination dest_copy(destination.buffer());

  REQUIRE_THAT(static_cast<vec>(dest_copy.buffer()), Equals(static_cast<vec>(destination.buffer())));
  REQUIRE_THAT(static_cast<vec>(dest_copy.padding()), Equals(static_cast<vec>(destination.padding())));

  const auto& signing = std::get<signing_t>(destination.signing());
  const auto& sigkey0 = signing.pubkey();
  const auto& sigkey1 = signing.pubkey();

  REQUIRE_THAT(vec(sigkey0.begin(), sigkey0.end()), Equals(vec(sigkey1.begin(), sigkey1.end())));
}

TEST_CASE_METHOD(DestinationFixture, "Destination signs and verifies a message", "[ident]")
{
  std::array<std::uint8_t, 13> msg{};
  Destination::eddsa_t::signature_t signature;

  Destination::signature_v sig;

  REQUIRE_NOTHROW(sig = destination.Sign(msg.data(), msg.size()));
  REQUIRE(destination.Verify(msg.data(), msg.size(), sig));
}

TEST_CASE_METHOD(DestinationFixture, "Destination rejects deserializing an invalid cert", "[ident]")
{
  // invalidate the cert length
  tini2p::BytesWriter<Destination::buffer_t> writer(destination.buffer());
  writer.skip_bytes(Destination::CertOffset + Destination::cert_t::LengthOffset);
  writer.write_bytes<Destination::cert_t::length_t>(0x42, tini2p::Endian::Big);

  REQUIRE_THROWS(destination.deserialize());

  // reset length, overwrite signing + crypto types with random data
  writer.skip_back(Destination::cert_t::LengthLen);
  writer.write_bytes(Destination::cert_t::length_t(Destination::cert_t::KeyCertLen), tini2p::Endian::Big);
  writer.write_bytes(tini2p::crypto::RandInRange());

  REQUIRE_NOTHROW(destination.deserialize());
  REQUIRE(destination.cert().locally_unreachable());
}

TEST_CASE_METHOD(DestinationFixture, "Destination rejects signing without a private key", "[ident]")
{
  Destination ident;
  ident.rekey<signing_t>(signing_t::pubkey_t{});

  std::array<std::uint8_t, 7> msg{};
  Destination::signature_v sig;

  REQUIRE_THROWS(sig = ident.Sign(msg.data(), msg.size()));
}
