/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/router/lease_set.h"

/*--------------------------------------------\
| LeaseSet2 Header tests                       |
\--------------------------------------------*/
using LeaseSet2Header = tini2p::data::LeaseSet2Header;

struct LeaseSet2HeaderFixture
{
  LeaseSet2Header ls_header;
};

TEST_CASE_METHOD(LeaseSet2HeaderFixture, "LeaseSet2Header has valid fields", "[ls]")
{
  REQUIRE(ls_header.ts() + ls_header.expires() == tini2p::time::now_s() + LeaseSet2Header::Timeout);

  const auto flags = tini2p::under_cast(ls_header.flags());
  const auto online_keys_f = tini2p::under_cast(LeaseSet2Header::Flag::OnlineKeys);
  const auto published_f = tini2p::under_cast(LeaseSet2Header::Flag::Published);

  REQUIRE((flags & online_keys_f) == online_keys_f);
  REQUIRE((flags >> 1 | published_f) == published_f);
}

TEST_CASE_METHOD(LeaseSet2HeaderFixture, "LeaseSet2Header serializes + signs with initializing ctor", "[ls]")
{
  REQUIRE_NOTHROW(LeaseSet2Header(LeaseSet2Header::destination_t()));
  REQUIRE(LeaseSet2Header(LeaseSet2Header::destination_t()).Verify());
}

TEST_CASE_METHOD(LeaseSet2HeaderFixture, "LeaseSet2Header deserializes from a buffer + verifies", "[ls]")
{
  std::unique_ptr<LeaseSet2Header> ls_ptr;
  REQUIRE_NOTHROW(ls_ptr.reset(new LeaseSet2Header(LeaseSet2Header::destination_t())));

  bool valid(false);
  REQUIRE_NOTHROW(valid = LeaseSet2Header(ls_ptr->buffer()).Verify());
  REQUIRE(valid);
}

TEST_CASE_METHOD(LeaseSet2HeaderFixture, "LeaseSet2Header assigns from another header", "[ls]")
{
  const auto header = ls_header;
}

/*--------------------------------------------\
| Key Section tests                           |
\--------------------------------------------*/
using KeySection = tini2p::data::KeySection;

struct KeySectionFixture
{
  KeySectionFixture() : key()
  {
    /// create random mock key, unrealistic
    tini2p::crypto::RandBytes(key);
    ks_ptr.reset(new KeySection(key));
  }

  KeySection::key_t key;
  std::unique_ptr<KeySection> ks_ptr;
};

TEST_CASE_METHOD(KeySectionFixture, "KeySection has valid fields", "[ls]")
{
  REQUIRE(ks_ptr->type == KeySection::Type::X25519);
  REQUIRE(ks_ptr->key_len == tini2p::crypto::X25519::PublicKeyLen);
  REQUIRE(ks_ptr->key.size() == tini2p::crypto::X25519::PublicKeyLen);
}

TEST_CASE_METHOD(KeySectionFixture, "KeySection serializes to a buffer", "[ls]")
{
  REQUIRE_NOTHROW(ks_ptr->serialize());
}

TEST_CASE_METHOD(KeySectionFixture, "KeySection deserializes from buffer", "[ls]")
{
  REQUIRE_NOTHROW(KeySection(ks_ptr->buffer.data(), ks_ptr->buffer.size()));
}

TEST_CASE_METHOD(KeySectionFixture, "KeySection rejects null buffer", "[ls]")
{
  REQUIRE_THROWS(KeySection(nullptr, ks_ptr->buffer.size()));
  REQUIRE_THROWS(KeySection(ks_ptr->buffer.data(), 0));
}

TEST_CASE_METHOD(KeySectionFixture, "KeySection rejects serializing and deserializing null key", "[ls]")
{
  KeySection ks;

  REQUIRE_THROWS(ks.serialize());

  // ensure valid state
  REQUIRE_NOTHROW(ks_ptr->deserialize());

  // write null key to valid buffer
  tini2p::BytesWriter<KeySection::buffer_t> writer(ks_ptr->buffer);
  REQUIRE_NOTHROW(writer.write_data(ks.key));

  // ensure throws deserializing null key
  REQUIRE_THROWS(ks_ptr->deserialize());
}

/*--------------------------------------------\
| Lease tests                                 |
\--------------------------------------------*/
using Lease2 = tini2p::data::Lease2;

struct Lease2Fixture
{
  Lease2Fixture() : lease(tini2p::crypto::RandBytes<32>()) {}

  Lease2 lease;
};

TEST_CASE_METHOD(Lease2Fixture, "Lease2 has valid fields", "[ls]")
{
  REQUIRE(lease.tunnel_gateway().size() == LeaseSet2Header::destination_t::HashLen);
  REQUIRE(lease.tunnel_id() != 0);
  REQUIRE(lease.expiration() >= tini2p::time::now_s() + Lease2::Timeout);
}

TEST_CASE_METHOD(Lease2Fixture, "Lease2 serializes to a buffer", "[ls]")
{
  REQUIRE_NOTHROW(lease.serialize());
}

TEST_CASE_METHOD(Lease2Fixture, "Lease2 deserializes from buffer", "[ls]")
{
  const auto& lease_buf = lease.buffer();

  REQUIRE_NOTHROW(Lease2(lease_buf));
  REQUIRE_NOTHROW(Lease2(lease_buf.data(), lease_buf.size()));
}

TEST_CASE_METHOD(Lease2Fixture, "Lease2 rejects null buffer", "[ls]")
{
  const auto& lease_buf = lease.buffer();

  REQUIRE_THROWS(Lease2(nullptr, lease_buf.size()));
  REQUIRE_THROWS(Lease2(lease_buf.data(), 0));
}

/*--------------------------------------------\
| LeaseSet2 tests                              |
\--------------------------------------------*/
using LeaseSet2 = tini2p::data::LeaseSet2;

struct LeaseSet2Fixture
{
  LeaseSet2 ls;
};

TEST_CASE_METHOD(LeaseSet2Fixture, "LeaseSet2 has valid fields", "[ls]")
{
  REQUIRE_NOTHROW(ls.properties());
  REQUIRE(ls.key_sections().size() * KeySection::MinLen == ls.key_sections_len());
  REQUIRE(ls.leases().size() * LeaseSet2::lease_t::Len == ls.leases_len());
}

TEST_CASE_METHOD(LeaseSet2Fixture, "LeaseSet2 serializes to a buffer", "[ls]")
{
  REQUIRE_NOTHROW(ls.serialize());
  REQUIRE_NOTHROW(ls.deserialize());
  REQUIRE(ls.Verify());
}

TEST_CASE_METHOD(LeaseSet2Fixture, "LeaseSet2 deserializes from buffer", "[ls]")
{
  const auto& ls_buf = ls.buffer();

  REQUIRE_NOTHROW(LeaseSet2(ls_buf.data(), ls_buf.size()));
  REQUIRE(LeaseSet2(ls_buf.data(), ls_buf.size()).Verify());

  REQUIRE_NOTHROW(LeaseSet2(ls_buf));
  REQUIRE(LeaseSet2(ls_buf).Verify());
}

TEST_CASE_METHOD(LeaseSet2Fixture, "LeaseSet2 rejects null buffer", "[ls]")
{
  REQUIRE_THROWS(LeaseSet2(nullptr, ls.buffer().size()));
  REQUIRE_THROWS(LeaseSet2(ls.buffer().data(), 0));
}

TEST_CASE_METHOD(LeaseSet2Fixture, "LeaseSet2 rejects invalid KeySection", "[ls]")
{
  REQUIRE_NOTHROW(ls.serialize());

  tini2p::BytesWriter<LeaseSet2::buffer_t> writer(ls.buffer());
  // skip to first key section key length offset
  writer.skip_bytes(ls.header().size() + ls.properties().size() + LeaseSet2::KeySectionNumLen + KeySection::TypeLen);

  writer.write_bytes(KeySection::MinKeyLen - 1);  // invalidate the key length (lower)
  REQUIRE_THROWS(ls.deserialize());

  writer.skip_back(KeySection::SizeLen);
  writer.write_bytes(KeySection::MaxKeyLen + 1);  // invalidate the key length (upper)
  REQUIRE_THROWS(ls.deserialize());
}

TEST_CASE_METHOD(LeaseSet2Fixture, "LeaseSet2 has valid signature", "[ls]")
{
  REQUIRE(ls.Verify());
}

// TODO(tini2p): implement and test blinded signing
