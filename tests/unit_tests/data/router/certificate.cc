/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/router/certificate.h"

using tini2p::data::Certificate;

struct RouterCertificateFixture
{
  RouterCertificateFixture() : cert(Certificate::cert_type_t::KeyCert) {}

  Certificate cert;
};

TEST_CASE_METHOD(
    RouterCertificateFixture,
    "RouterCertificate has a valid default construction",
    "[cert]")
{
  REQUIRE(cert.cert_type == Certificate::cert_type_t::KeyCert);
  REQUIRE(cert.sign_type == Certificate::sign_type_t::EdDSA);
  REQUIRE(cert.crypto_type == Certificate::crypto_type_t::EciesX25519);
}

TEST_CASE_METHOD(
    RouterCertificateFixture,
    "RouterCertificate serializes a valid certificate",
    "[cert]")
{
  REQUIRE_NOTHROW(cert.serialize());
}

TEST_CASE_METHOD(
    RouterCertificateFixture,
    "RouterCertificate deserializes a valid certificate",
    "[cert]")
{
  REQUIRE_NOTHROW(cert.serialize());
  REQUIRE_NOTHROW(cert.deserialize());
}
