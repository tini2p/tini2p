/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/router/mapping.h"

using Mapping = tini2p::data::Mapping;

struct MappingFixture
{
  MappingFixture()
  {
    min_kv.fill(0xbe);
    max_kv.fill(0xbe);
  }

  tini2p::crypto::FixedSecBytes<Mapping::MinKVLen> min_kv;
  tini2p::crypto::FixedSecBytes<Mapping::MaxKVLen> max_kv;
  tini2p::data::Mapping map;
};

TEST_CASE_METHOD(MappingFixture, "Mapping validates entry sizes", "[map]")
{
  const std::string kv_str(Mapping::MaxKVLen, 0xF);

  REQUIRE_NOTHROW(map.size());
  REQUIRE_NOTHROW(map.add(kv_str, kv_str));
  REQUIRE_NOTHROW(map.add(min_kv, min_kv));
  REQUIRE_NOTHROW(map.add(min_kv, max_kv));
  REQUIRE_NOTHROW(map.add(max_kv, min_kv));
  REQUIRE_NOTHROW(map.add(max_kv, max_kv));
}

TEST_CASE_METHOD(MappingFixture, "Mapping rejects invalid entry sizes", "[map]")
{
  const std::string kv_str(Mapping::MaxKVLen + 1, 0xF);

  REQUIRE_THROWS(map.add(min_kv, kv_str));
  REQUIRE_THROWS(map.add(kv_str, min_kv));
  REQUIRE_THROWS(map.add(min_kv, std::vector<std::uint8_t>{}));
  REQUIRE_THROWS(map.add(std::vector<std::uint8_t>{}, min_kv));
}

TEST_CASE_METHOD(MappingFixture, "Mapping rejects too many entries", "[map]")
{
  const auto& entry_len = map.entry_size(max_kv, max_kv);
  while(entry_len + map.size() < tini2p::under_cast(Mapping::MaxLen))
    {  // generate random key for unique entries
      REQUIRE_NOTHROW(map.add(max_kv, max_kv));
      tini2p::crypto::RandBytes(max_kv);
    }
  REQUIRE_THROWS(map.add(max_kv, max_kv));
}

TEST_CASE_METHOD(MappingFixture, "Mapping serializes an empty mapping", "[map]")
{
  REQUIRE_NOTHROW(map.serialize());
}

TEST_CASE_METHOD(MappingFixture, "Mapping serializes a non-empty mapping", "[map]")
{
  for (std::uint8_t cnt = 0; cnt < 3; ++cnt)
    map.add(max_kv, max_kv);

  REQUIRE_NOTHROW(map.serialize());
}

TEST_CASE_METHOD(MappingFixture, "Mapping deserializes an empty mapping", "[map]")
{
  REQUIRE_NOTHROW(map.serialize());
  REQUIRE_NOTHROW(map.deserialize());
}

TEST_CASE_METHOD(MappingFixture, "Mapping deserializes a non-empty mapping", "[map]")
{
  constexpr std::uint8_t num_entry = 3;
  for (std::uint8_t cnt = 0; cnt < num_entry; ++cnt)
    {
      min_kv[0] = cnt;
      map.add(min_kv, min_kv);
    }

  REQUIRE_NOTHROW(map.serialize());
  REQUIRE_NOTHROW(map.deserialize());

  REQUIRE(map.size() == Mapping::SizeLen + (num_entry * map.entry_size(min_kv, min_kv)));
}

TEST_CASE_METHOD(MappingFixture, "Mapping fails to deserialize an invalid key length", "[map]")
{
  REQUIRE_NOTHROW(map.add(min_kv, min_kv));
  REQUIRE_NOTHROW(map.serialize());

  // will overrun the buffer if unchecked
  map.buffer()[Mapping::KeySizeOffset] = 0xFF;
  REQUIRE_THROWS(map.deserialize());
}

TEST_CASE_METHOD(MappingFixture, "Mapping fails to deserialize an invalid value length", "[map]")
{
  REQUIRE_NOTHROW(map.add(min_kv, min_kv));
  REQUIRE_NOTHROW(map.serialize());

  // will overrun the buffer if unchecked
  const auto& value_offset = Mapping::SizeLen + min_kv.size() + Mapping::KVDelimLen;
  map.buffer()[value_offset] = 0xFF;
  REQUIRE_THROWS(map.deserialize());
}

TEST_CASE_METHOD(MappingFixture, "Mapping fails to deserialize an invalid buffer length", "[map]")
{
  REQUIRE_NOTHROW(map.serialize());

  // invalidate the buffer size
  REQUIRE_NOTHROW(map.buffer().resize(map.size() - 0x1));
  REQUIRE_THROWS(map.deserialize());
}

TEST_CASE_METHOD(MappingFixture, "Mapping fails to deserialize an invalid mapping length", "[map]")
{
  // invalidate the mapping size
  auto& buf = map.buffer();
  buf.resize(1);
  buf[Mapping::SizeOffset] = 0xFF;

  REQUIRE_THROWS(map.deserialize());
}

TEST_CASE_METHOD(MappingFixture, "Mapping fails to deserialize missing key-value delimiter", "[map]")
{
  REQUIRE_NOTHROW(map.add(min_kv, min_kv));
  REQUIRE_NOTHROW(map.serialize());

  const std::uint8_t kv_delim = 0x3D;
  auto& buf = map.buffer();

  // find and remove the key-value delimiter ("=")
  buf.erase(std::remove(buf.begin(), buf.end(), kv_delim));

  REQUIRE_THROWS(map.deserialize());
}
