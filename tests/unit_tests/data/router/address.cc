/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/router/address.h"

using Address = tini2p::data::Address;

struct RouterAddressFixture
{
  RouterAddressFixture() : address(host, port) {}

  const std::string host{"13.37.4.2"};
  const std::uint16_t port{9111};
  Address address;
};

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress has a cost", "[address]")
{
  REQUIRE(address.cost == Address::DefaultCost);
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress has a transport", "[address]")
{
  using Catch::Matchers::Equals;

  const std::string ntcp2_str("ntcp2");

  const std::string transport_str(address.transport.begin(), address.transport.end());

  REQUIRE_THAT(transport_str, Equals(ntcp2_str));
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress serializes a valid address", "[address]")
{
  REQUIRE_NOTHROW(address.serialize());
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress deserializes a valid address", "[address]")
{
  REQUIRE_NOTHROW(address.serialize());
  REQUIRE_NOTHROW(address.deserialize());
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress fails to serialize invalid expiration", "[address]")
{
  // any non-zero expiration is invalid
  ++address.expiration;

  REQUIRE_THROWS(address.serialize());
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress fails to serialize invalid transport", "[address]")
{
  // invalidate the transport length
  address.transport.push_back(0x2B);

  REQUIRE_THROWS(address.serialize());

  address.transport.pop_back();

  // invalidate the transport name
  ++address.transport.front();

  REQUIRE_THROWS(address.serialize());
}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress fails to deserialize invalid expiration", "[address]")
{
  REQUIRE_NOTHROW(address.serialize());

  // any non-zero expiration is invalid
  ++address.buffer[Address::ExpirationOffset];

  REQUIRE_THROWS(address.deserialize());

}

TEST_CASE_METHOD(RouterAddressFixture, "RouterAddress fails to deserialize invalid transport", "[address]")
{
  REQUIRE_NOTHROW(address.serialize());

  // invalidate the transport length
  ++address.buffer[Address::TransportOffset];

  REQUIRE_THROWS(address.deserialize());

  --address.buffer[Address::TransportOffset];

  // invalidate the transport name
  ++address.buffer[Address::TransportOffset + 1];

  REQUIRE_THROWS(address.deserialize());
}
