/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/crypto/rand.h"

#include "src/data/router/info.h"

using tini2p::data::Info;
using crypto_t = Info::identity_t::crypto_t;
using signing_t = Info::identity_t::eddsa_t;

struct RouterInfoFixture
{
  RouterInfoFixture() : info() {}

  Info info;
};

TEST_CASE_METHOD(RouterInfoFixture, "RouterInfo has valid router identity", "[ri]")
{
  REQUIRE(info.identity().size() == Info::identity_t::DefaultLen);
  REQUIRE(!info.identity().cert().locally_unreachable());
}

TEST_CASE_METHOD(RouterInfoFixture, "RouterInfo has valid date", "[ri]")
{
  REQUIRE(info.date());
}

TEST_CASE_METHOD(RouterInfoFixture, "RouterInfo has valid addresses", "[ri]")
{
  REQUIRE(info.addresses().empty());
}

TEST_CASE_METHOD(RouterInfoFixture, "RouterInfo has valid options", "[ri]")
{
  using Catch::Matchers::Equals;

  const auto noise_i = info.options().entry(std::string("i"));
  const auto iv = info.iv();

  REQUIRE_THAT(
      std::string(noise_i.begin(), noise_i.end()), Equals(tini2p::crypto::Base64::Encode(iv.data(), iv.size())));
}

TEST_CASE_METHOD(RouterInfoFixture, "RouterInfo has valid signature", "[ri]")
{
  const auto& sig = info.signature();
  const auto& ident = info.identity();
  const auto& sig_len = std::visit([](const auto& v) { return v.size(); }, sig);

  REQUIRE(sig_len == ident.sig_len());
  REQUIRE(ident.Verify(info.buffer().data(), info.size() - sig_len, sig));
  REQUIRE(info.Verify());
}

TEST_CASE_METHOD(RouterInfoFixture, "RouterInfo has serializes and deserializes empty addresses + options", "[ri]")
{
  REQUIRE_NOTHROW(info.serialize());
  REQUIRE_NOTHROW(info.deserialize());

  REQUIRE_NOTHROW(Info(info.buffer()));

  Info info_copy(info.buffer());
  REQUIRE(info_copy.Verify());
}

TEST_CASE_METHOD(RouterInfoFixture, "RouterInfo has serializes and deserializes non-empty addresses + options", "[ri]")
{
  using Catch::Matchers::Equals;
  using vec = std::vector<std::uint8_t>;

  info.add_address(tini2p::data::Address(std::string("127.0.0.1"), 9111));
  info.options().add(std::string("host"), std::string("127.0.0.1"));

  REQUIRE_NOTHROW(info.serialize());
  REQUIRE_NOTHROW(info.deserialize());

  REQUIRE_NOTHROW(Info(info.buffer()));
  Info info_copy(info.buffer());

  REQUIRE_THAT(static_cast<vec>(info_copy.buffer()), Equals(static_cast<vec>(info.buffer())));

  const auto& sigkey0 = std::get<signing_t>(info.identity().signing()).pubkey();
  const auto& sigkey1 = std::get<signing_t>(info_copy.identity().signing()).pubkey();

  REQUIRE_THAT(vec(sigkey0.begin(), sigkey0.end()), Equals(vec(sigkey1.begin(), sigkey1.end())));

  const auto& sig0 = std::get<signing_t::signature_t>(info.signature());
  const auto& sig1 = std::get<signing_t::signature_t>(info_copy.signature());

  REQUIRE_THAT(vec(sig0.begin(), sig0.end()), Equals(vec(sig1.begin(), sig1.end())));
  REQUIRE(info_copy.identity().Verify(info_copy.buffer().data(), info_copy.size() - sig1.size(), sig1));
  REQUIRE(info_copy.Verify());
}

TEST_CASE_METHOD(RouterInfoFixture, "RouterInfo can sign a message with its router identity", "[ri]")
{
  Info::signature_v sig;
  signing_t::message_t msg(19);
  tini2p::crypto::RandBytes(msg);

  const auto& ident = info.identity();

  REQUIRE_NOTHROW(sig = ident.Sign(msg.data(), msg.size()));
  REQUIRE(ident.Verify(msg.data(), msg.size(), sig));
}
