/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/data/router/identity.h"

namespace crypto = tini2p::crypto;

using Catch::Matchers::Equals;
using vec = std::vector<std::uint8_t>;

using Identity = tini2p::data::Identity;
using crypto_t = Identity::crypto_t;
using signing_t = Identity::eddsa_t;


struct RouterIdentityFixture
{
  RouterIdentityFixture() : identity() {}

  Identity identity;
};

TEST_CASE_METHOD(RouterIdentityFixture, "RouterIdentity has a crypto public key", "[ident]")
{
  REQUIRE(identity.crypto().pubkey.size() == crypto_t::PublicKeyLen);
  REQUIRE(identity.crypto_pubkey_len() == crypto_t::PublicKeyLen);
}

TEST_CASE_METHOD(RouterIdentityFixture, "RouterIdentity has a signing public key", "[ident]")
{
  std::visit(
      [](const auto& s) { REQUIRE(s.pubkey().size() == std::decay_t<decltype(s)>::PublicKeyLen); }, identity.signing());
  REQUIRE_NOTHROW(identity.signing_pubkey_len() == signing_t::PublicKeyLen);
}

TEST_CASE_METHOD(RouterIdentityFixture, "RouterIdentity has a cert", "[ident]")
{
  REQUIRE_NOTHROW(identity.cert());

  const auto& cert = identity.cert();
  REQUIRE(cert.cert_type == Identity::cert_t::cert_type_t::KeyCert);
  REQUIRE(cert.length == Identity::cert_t::KeyCertLen);
  REQUIRE(cert.sign_type == Identity::cert_t::sign_type_t::EdDSA);
  REQUIRE(cert.crypto_type == Identity::cert_t::crypto_type_t::EciesX25519);
}

TEST_CASE_METHOD(RouterIdentityFixture, "RouterIdentity has a valid size", "[ident]")
{
  REQUIRE_NOTHROW(identity.size());

  const auto keys_len = crypto_t::PublicKeyLen + signing_t::PublicKeyLen;
  const auto expected_len =
      keys_len + (Identity::KeysPaddingLen - keys_len) + Identity::cert_t::HeaderLen + Identity::cert_t::KeyCertLen;

  REQUIRE(identity.size() == expected_len);
}

TEST_CASE_METHOD(RouterIdentityFixture, "RouterIdentity serializes and deserializes a valid router identity", "[ident]")
{
  REQUIRE_NOTHROW(identity.serialize());
  REQUIRE_NOTHROW(identity.deserialize());
  REQUIRE_NOTHROW(Identity(identity.buffer()));

  Identity ident_copy(identity.buffer());

  REQUIRE_THAT(static_cast<vec>(ident_copy.buffer()), Equals(static_cast<vec>(identity.buffer())));
  REQUIRE_THAT(static_cast<vec>(ident_copy.padding()), Equals(static_cast<vec>(identity.padding())));

  const auto& signing = std::get<signing_t>(identity.signing());
  const auto& sigkey0 = signing.pubkey();
  const auto& sigkey1 = signing.pubkey();

  REQUIRE_THAT(vec(sigkey0.begin(), sigkey0.end()), Equals(vec(sigkey1.begin(), sigkey1.end())));
}

TEST_CASE_METHOD(RouterIdentityFixture, "RouterIdentity signs and verifies a message", "[ident]")
{
  std::array<std::uint8_t, 13> msg{};
  Identity::eddsa_t::signature_t signature;

  Identity::signature_v sig;

  REQUIRE_NOTHROW(sig = identity.Sign(msg.data(), msg.size()));
  REQUIRE(identity.Verify(msg.data(), msg.size(), sig));
}

TEST_CASE_METHOD(RouterIdentityFixture, "RouterIdentity rejects deserializing an invalid cert", "[ident]")
{
  // invalidate the cert length
  tini2p::BytesWriter<Identity::buffer_t> writer(identity.buffer());
  writer.skip_bytes(Identity::CertOffset + Identity::cert_t::LengthOffset);
  writer.write_bytes<Identity::cert_t::length_t>(0x42, tini2p::Endian::Big);

  REQUIRE_THROWS(identity.deserialize());

  // reset length, overwrite signing + crypto types with random data
  writer.skip_back(Identity::cert_t::LengthLen);
  writer.write_bytes(Identity::cert_t::length_t(Identity::cert_t::KeyCertLen), tini2p::Endian::Big);
  writer.write_bytes(tini2p::crypto::RandInRange());

  REQUIRE_NOTHROW(identity.deserialize());
  REQUIRE(identity.cert().locally_unreachable());
}

TEST_CASE_METHOD(RouterIdentityFixture, "RouterIdentity rejects signing without a private key", "[ident]")
{
  Identity ident;
  ident.rekey<signing_t>(signing_t::pubkey_t{});

  std::array<std::uint8_t, 7> msg{};
  Identity::signature_v sig;

  REQUIRE_THROWS(sig = ident.Sign(msg.data(), msg.size()));
}
