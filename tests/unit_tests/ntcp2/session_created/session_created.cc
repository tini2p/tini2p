/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "tests/unit_tests/mock/handshake.h"

struct SessionCreatedFixture : public MockHandshake
{
  SessionCreatedFixture()
  {
    ValidSessionRequest();
    InitializeSessionCreated();
  }
};

TEST_CASE_METHOD(
    SessionCreatedFixture,
    "SessionCreated initiator writes message",
    "[scr]")
{
  REQUIRE_NOTHROW(scr_initiator->ProcessMessage(scr_message));
}

TEST_CASE_METHOD(
    SessionCreatedFixture,
    "SessionCreated responder reads a written message",
    "[scr]")
{
  REQUIRE_NOTHROW(scr_initiator->ProcessMessage(scr_message));
  REQUIRE_NOTHROW(scr_responder->ProcessMessage(scr_message));
}

TEST_CASE_METHOD(
    SessionCreatedFixture,
    "SessionCreated rejects null handshake state",
    "[scr]")
{
  using hash_t = tini2p::data::Identity::hash_t;

  REQUIRE_THROWS(sess_init_t::created_impl_t(
      nullptr, request_msg_t{}, hash_t{}, obfse_t::iv_t{}));

  REQUIRE_THROWS(sess_resp_t::created_impl_t(
      nullptr, request_msg_t{}, hash_t{}, obfse_t::iv_t{}));
}
