/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/ntcp2/session_request/kdf.h"

using tini2p::ntcp2::Initiator;
using tini2p::ntcp2::Responder;
using tini2p::ntcp2::SessionRequestKDF;

struct SessionRequestKDFFixture
{
  SessionRequestKDFFixture()
  {
    const tini2p::exception::Exception ex{"SessionRequestKDFFixture", __func__};

    tini2p::ntcp2::noise::init_handshake<Initiator>(&initiator_state, ex);
    tini2p::ntcp2::noise::init_handshake<Responder>(&responder_state, ex);

    initiator_kdf = std::make_unique<SessionRequestKDF>(initiator_state);
    responder_kdf = std::make_unique<SessionRequestKDF>(responder_state);
  }

  NoiseHandshakeState *initiator_state, *responder_state;
  std::unique_ptr<SessionRequestKDF> initiator_kdf, responder_kdf;
  tini2p::crypto::X25519::pubkey_t key;
};

TEST_CASE_METHOD(
    SessionRequestKDFFixture,
    "SessionRequestKDF sets remote public key",
    "[srq_kdf]")
{
  REQUIRE_NOTHROW(initiator_kdf->set_remote_key(key));
}

TEST_CASE_METHOD(
    SessionRequestKDFFixture,
    "SessionRequestKDF generates local keypair",
    "[srq_kdf]")
{
  REQUIRE_NOTHROW(initiator_kdf->generate_keys());
}

TEST_CASE_METHOD(
    SessionRequestKDFFixture,
    "SessionRequestKDF derives initiator session request keys",
    "[srq_kdf]")
{
  REQUIRE_NOTHROW(initiator_kdf->generate_keys());
  REQUIRE_NOTHROW(initiator_kdf->Derive(key));
}

TEST_CASE_METHOD(
    SessionRequestKDFFixture,
    "SessionRequestKDF derives responder session request keys",
    "[srq_kdf]")
{
  REQUIRE_NOTHROW(responder_kdf->generate_keys());
  REQUIRE_NOTHROW(responder_kdf->Derive());
}

TEST_CASE_METHOD(
    SessionRequestKDFFixture,
    "SessionRequestKDF fails to derive initiator keys without remote static key",
    "[srq_kdf]")
{
  REQUIRE_NOTHROW(initiator_kdf->generate_keys());
  REQUIRE_THROWS(initiator_kdf->Derive());
}

TEST_CASE_METHOD(
    SessionRequestKDFFixture,
    "SessionRequestKDF fails to derive initiator keys without local keypair",
    "[srq_kdf]")
{
  REQUIRE_NOTHROW(initiator_kdf->set_remote_key(key));
  REQUIRE_THROWS(initiator_kdf->Derive());
}

TEST_CASE_METHOD(
    SessionRequestKDFFixture,
    "SessionRequestKDF fails to derive responder keys without local keypair",
    "[srq_kdf]")
{
  REQUIRE_THROWS(responder_kdf->Derive());
}

TEST_CASE("SessionRequestKDF rejects null handshake state", "[srq_kdf]")
{
  REQUIRE_THROWS(SessionRequestKDF(nullptr));
}
