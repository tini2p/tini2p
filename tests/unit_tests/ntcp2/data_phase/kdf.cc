/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "tests/unit_tests/mock/handshake.h"

struct DataPhaseKDFFixture : public MockHandshake
{
  DataPhaseKDFFixture()
  {
    ValidSessionRequest();
    ValidSessionCreated();
    ValidSessionConfirmed();

    // Switch roles according to spec
    initiator =
        std::make_unique<sess_init_t::data_impl_t::kdf_t>(responder_state);

    responder =
        std::make_unique<sess_resp_t::data_impl_t::kdf_t>(initiator_state);
  }

  std::unique_ptr<sess_init_t::data_impl_t::kdf_t> initiator;
  std::unique_ptr<sess_resp_t::data_impl_t::kdf_t> responder;
};

TEST_CASE_METHOD(DataPhaseKDFFixture, "DataPhaseKDF generates keys", "[dpkdf]")
{
  using Catch::Matchers::Equals;
  using vec = std::vector<std::uint8_t>;
  using Dir = sess_init_t::data_msg_t::Dir;

  std::uint16_t tmp = 17, msg_len = 17;

  const auto& init_hash = initiator->hash();
  const auto& resp_hash = responder->hash();
  REQUIRE_THAT(vec(init_hash.begin(), init_hash.end()), Equals(vec(resp_hash.begin(), resp_hash.end())));

  // check initial length is obfuscated then deobfuscated correctly
  REQUIRE_NOTHROW(initiator->ProcessLength(msg_len, Dir::BobToAlice));
  REQUIRE(msg_len != tmp);

  REQUIRE_NOTHROW(responder->ProcessLength(msg_len, Dir::BobToAlice, tini2p::Endian::Big));
  REQUIRE(msg_len == tmp);

  // check response length is obfuscated then deobfuscated correctly
  REQUIRE_NOTHROW(responder->ProcessLength(msg_len, Dir::AliceToBob));
  REQUIRE(msg_len != tmp);

  REQUIRE_NOTHROW(initiator->ProcessLength(msg_len, Dir::AliceToBob, tini2p::Endian::Big));
  REQUIRE(msg_len == tmp);

  // check follow-on length is obfuscated then deobfuscated correctly
  REQUIRE_NOTHROW(initiator->ProcessLength(msg_len, Dir::BobToAlice));
  REQUIRE(msg_len != tmp);

  REQUIRE_NOTHROW(responder->ProcessLength(msg_len, Dir::BobToAlice, tini2p::Endian::Big));
  REQUIRE(msg_len == tmp);
}

TEST_CASE_METHOD(
    DataPhaseKDFFixture,
    "DataPhaseKDF rejects null handshake state",
    "[dp]")
{
  REQUIRE_THROWS(sess_init_t::data_impl_t::kdf_t(nullptr));
  REQUIRE_THROWS(sess_resp_t::data_impl_t::kdf_t(nullptr));
}
