/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "tests/unit_tests/mock/handshake.h"

struct DataPhaseFixture : public MockHandshake
{
  using Info = tini2p::data::Info;

  using DateTimeBlock = tini2p::data::DateTimeBlock;
  using PaddingBlock = tini2p::data::PaddingBlock;
  using I2NPBlock = tini2p::data::I2NPBlock;
  using InfoBlock = tini2p::data::InfoBlock;
  using TerminationBlock = tini2p::data::TerminationBlock;

  DataPhaseFixture()
  {
    ValidSessionRequest();
    ValidSessionCreated();
    ValidSessionConfirmed();
    InitializeDataPhase();
  }

  Info::shared_ptr ri;
};

TEST_CASE_METHOD(
    DataPhaseFixture,
    "DataPhase initiator and responder encrypt and decrypt a message",
    "[dp]")
{
  dp_message.add_block(DateTimeBlock());

  REQUIRE_NOTHROW(dp_initiator->Write(dp_message));
  REQUIRE_NOTHROW(dp_responder->Read(dp_message));

  REQUIRE_NOTHROW(dp_responder->Write(dp_message));
  REQUIRE_NOTHROW(dp_initiator->Read(dp_message));
}

TEST_CASE_METHOD(
    DataPhaseFixture,
    "DataPhase responder encrypts and decrypts a message with blocks",
    "[dp]")
{
  ri = std::make_shared<Info>();
  dp_message.add_block(DateTimeBlock());
  dp_message.add_block(InfoBlock(ri));
  dp_message.add_block(PaddingBlock(17));

  REQUIRE_NOTHROW(dp_initiator->Write(dp_message));
  REQUIRE_NOTHROW(dp_responder->Read(dp_message));
}

TEST_CASE_METHOD(
    DataPhaseFixture,
    "DataPhase initiator and responder reject writing empty messages",
    "[dp]")
{
  REQUIRE_THROWS(dp_initiator->Write(dp_message));
  REQUIRE_THROWS(dp_responder->Write(dp_message));
}

TEST_CASE_METHOD(
    DataPhaseFixture,
    "DataPhase responder rejects invalid MAC",
    "[dp]")
{
  using Catch::Matchers::Equals;
  using vec = std::vector<std::uint8_t>;

  dp_message.add_block(DateTimeBlock());
  dp_message.serialize();

  const auto& buf = dp_message.buffer();
  vec exp_msg(buf.begin(), buf.end());
  REQUIRE_NOTHROW(dp_initiator->Write(dp_message));

  // invalidate ciphertext
  dp_message.buffer().zero();
  REQUIRE_NOTHROW(dp_responder->Read(dp_message));
  REQUIRE_THAT(static_cast<vec>(buf), !Equals(exp_msg));
}

TEST_CASE_METHOD(DataPhaseFixture, "DataPhase initiator rejects invalid MAC", "[dp]")
{
  using Catch::Matchers::Equals;
  using vec = std::vector<std::uint8_t>;

  dp_message.add_block(DateTimeBlock());
  dp_message.serialize();

  const auto& buf = dp_message.buffer();
  const vec exp_msg(buf.begin(), buf.end());
  REQUIRE_NOTHROW(dp_responder->Write(dp_message));

  // invalidate ciphertext
  dp_message.buffer().zero();
  REQUIRE_NOTHROW(dp_initiator->Read(dp_message));
  REQUIRE_THAT(static_cast<vec>(buf), !Equals(exp_msg));
}

TEST_CASE_METHOD(
    DataPhaseFixture,
    "DataPhase message rejects invalid block order",
    "[dp]")
{
  // invalid order, padding must be last block
  dp_message.add_block(PaddingBlock(3));
  REQUIRE_THROWS(dp_message.add_block(DateTimeBlock()));

  dp_message.clear_blocks();

  // invalid order, termination must only be followed by padding block
  dp_message.add_block(TerminationBlock());
  REQUIRE_THROWS(dp_message.add_block(DateTimeBlock()));
}

TEST_CASE_METHOD(
    DataPhaseFixture,
    "DataPhase message rejects invalid size",
    "[dp]")
{
  using Catch::Matchers::Equals;

  // add blocks to make message oversized
  I2NPBlock i2np_msg;
  i2np_msg.resize_message(I2NPBlock::MaxMsgLen);
  dp_message.add_block(std::move(i2np_msg));

  REQUIRE_THROWS(
      dp_message.add_block(PaddingBlock(PaddingBlock::MaxPaddingLen)));
}

TEST_CASE_METHOD(
    DataPhaseFixture,
    "DataPhase rejects null handshake state",
    "[dp]")
{
  REQUIRE_THROWS(sess_init_t::data_impl_t(nullptr));
  REQUIRE_THROWS(sess_resp_t::data_impl_t(nullptr));
}
