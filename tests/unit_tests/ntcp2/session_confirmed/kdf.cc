/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "src/ntcp2/noise.h"
#include "src/ntcp2/session_created/kdf.h"

#include "tests/unit_tests/mock/handshake.h"

struct SessionConfirmedKDFFixture : public MockHandshake
{
  SessionConfirmedKDFFixture()
  {
    ValidSessionRequest();
    ValidSessionCreated();

    // switch roles from SessionRequest
    initiator_kdf =
        std::make_unique<sess_init_t::confirmed_impl_t::kdf_t>(responder_state);

    responder_kdf =
        std::make_unique<sess_resp_t::confirmed_impl_t::kdf_t>(initiator_state);
  }

  std::unique_ptr<sess_init_t::confirmed_impl_t::kdf_t> initiator_kdf;
  std::unique_ptr<sess_resp_t::confirmed_impl_t::kdf_t> responder_kdf;
};

TEST_CASE_METHOD(
    SessionConfirmedKDFFixture,
    "Session confirmed initiator derives keys",
    "[scr_kdf]")
{
  // derive keys with SessionRequest ciphertext + padding
  REQUIRE_NOTHROW(initiator_kdf->Derive(srq_message));
}

TEST_CASE_METHOD(
    SessionConfirmedKDFFixture,
    "Session confirmed responder derives keys",
    "[scr_kdf]")
{
  // derive keys with SessionRequest ciphertext + padding
  REQUIRE_NOTHROW(responder_kdf->Derive(srq_message));
}
