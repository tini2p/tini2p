/* Unlicense
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
*/

#include <catch2/catch.hpp>

#include "tests/unit_tests/mock/handshake.h"

struct SessionConfirmedFixture : public MockHandshake
{
  SessionConfirmedFixture()
  {
    ValidSessionRequest();
    ValidSessionCreated();
    InitializeSessionConfirmed();
  }
};

TEST_CASE_METHOD(SessionConfirmedFixture, "SessionConfirmed initiator writes message", "[sco]")
{
  REQUIRE_NOTHROW(sco_initiator->ProcessMessage(sco_message, srq_message.options));
}

TEST_CASE_METHOD(SessionConfirmedFixture, "SessionConfirmed responder reads a written message", "[sco]")
{
  using crypto_t = tini2p::data::Identity::crypto_t;

  auto ri = sco_message.info_block.info();
  REQUIRE_NOTHROW(sco_initiator->ProcessMessage(sco_message, srq_message.options));
  REQUIRE_NOTHROW(sco_responder->ProcessMessage(sco_message, srq_message.options));
  auto ret_ri = sco_message.info_block.info();

  REQUIRE(ri->identity().hash() == ret_ri->identity().hash());

  const auto& r0_key = ri->identity().crypto().pubkey;
  const auto& r1_key = ret_ri->identity().crypto().pubkey;
  REQUIRE(r0_key == r1_key);

  const crypto_t::pubkey_t& s(tini2p::crypto::Base64::Decode(ret_ri->options().entry(std::string("s"))));

  REQUIRE(ret_ri->identity().crypto().pubkey == s);
}

TEST_CASE_METHOD(SessionConfirmedFixture, "SessionConfirmed initiator rejects invalid message size", "[sco]")
{
  // Part two message size must equal value sent in initial SessionRequest
  ++srq_message.options.m3p2_len;
  REQUIRE_THROWS(sco_initiator->ProcessMessage(sco_message, srq_message.options));

  srq_message.options.m3p2_len -= 2;
  REQUIRE_THROWS(sco_initiator->ProcessMessage(sco_message, srq_message.options));
}

TEST_CASE_METHOD(SessionConfirmedFixture, "SessionConfirmed responder rejects invalid message size", "[sco]")
{
  // Part two message size must equal value sent in initial SessionRequest
  ++srq_message.options.m3p2_len;
  REQUIRE_THROWS(sco_responder->ProcessMessage(sco_message, srq_message.options));

  srq_message.options.m3p2_len -= 2;
  REQUIRE_THROWS(sco_responder->ProcessMessage(sco_message, srq_message.options));
}

TEST_CASE_METHOD(SessionConfirmedFixture, "SessionConfirmed responder rejects invalid MAC", "[sco]")
{
  // Write a valid message
  REQUIRE_NOTHROW(sco_initiator->ProcessMessage(sco_message, srq_message.options));

  // Invalidate the ciphertext to fail MAC verification
  ++sco_message.data[0];
  REQUIRE_THROWS(sco_responder->ProcessMessage(sco_message, srq_message.options));

  --sco_message.data[0];  // reset

  // Invalidate part two
  ++sco_message.data[confirmed_msg_t::PartOneSize];
  REQUIRE_THROWS(sco_responder->ProcessMessage(sco_message, srq_message.options));
}

TEST_CASE_METHOD(SessionConfirmedFixture, "SessionConfirmed rejects null handshake state", "[sco]")
{
  REQUIRE_THROWS(sess_init_t::confirmed_impl_t(nullptr, created_msg_t{}));
  REQUIRE_THROWS(sess_resp_t::confirmed_impl_t(nullptr, created_msg_t{}));
}
